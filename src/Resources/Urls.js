const url = 'http://192.168.110.12';
const cdnurl = 'http://192.168.110.13';
const baseAdminPanelUrl = 'http://localhost';
const port = '4060';
const cdnport = '7080';
const baseAdminPanelPort = '3000';
const baseUrl = `${url}:${port}`;
module.exports = {
  // <editor-fold dsc="CDN">
  BASE_CDN_URL: `${cdnurl}:${cdnport}`,
  // </editor-fold>

  // <editor-fold dsc="Admin Panel">
  BASE_ADMIN_PANEL: `${baseAdminPanelUrl}:${baseAdminPanelPort}`,
  // </editor-fold>

  // <editor-fold dsc="Identity">
  USER_ROLES: `${baseUrl}/Identity/Role/Roles`,
  USERS: `${baseUrl}/Identity/User/Users`,
  IDENTITY_CHANGE_PASSWORD: `${baseUrl}/Identity/User/ChangePassword`,
  IDENTITY_USER_ROLE: `${baseUrl}/Identity/UserRole/UserRoles`,

  // </editor-fold>

  // <editor-fold dsc="Catalog">
  CATEGORIES: `${baseUrl}/Catalog/Category/Categories`,
  PRODUCTTYPES: `${baseUrl}/Catalog/ProductType/ProductTypes`,
  PRODUCTS: `${baseUrl}/Catalog/Product/Products`,
  // </editor-fold>

  // <editor-fold dsc="Vendor">
  VENDOR_BRANCHES: `${baseUrl}/Vendor/VendorBranch/VendorBranches`,
  VENDOR_BRANCHES_USER: `${baseUrl}/Vendor/VendorBranchUser/VendorBranchUserByVendorId`,
  AGENTS: `${baseUrl}/Vendor/Agent/Agents`,
  // </editor-fold>

  // <editor-fold dsc="Common">
  CITIES: `${baseUrl}/Common/City/Cities`,
  DISTRICTS: `${baseUrl}/Common/District/Districts`,
  // </editor-fold>

  // <editor-fold dsc="Promotion">
  PROMOTIONS: `${baseUrl}/SAM/Promotion/Promotions`,
  PROMOTION_TYPES: `${baseUrl}/SAM/Promotion/PromotionType/PromotionTypes`,
  // </editor-fold>

  // <editor-fold dsc="Commission">
  COMMISSION: `${baseUrl}/SAM/VendorCategoryCommission/VendorCategoryCommissions`,
  // </editor-fold>

  // <editor-fold dsc="Facility">
  FACILITY: `${baseUrl}/Vendor/Facility/Facilities`,
  VENDOR_FACILITY: `${baseUrl}/Vendor/VendorBranchFacility/VendorBranchFacilities`,
  // </editor-fold>

  // <editor-fold dsc="vendorBranchOffShift">
  VENDORBRANCH_OFFSHIFT: `${baseUrl}/Vendor/VendorBranchOffShift/VendorBranchOffShifts`,
  // </editor-fold>

  // <editor-fold dsc="vendorBranchUser">
  VENDORBRANCH_USER: `${baseUrl}/Vendor/VendorBranchUser/VendorBranchUsers`,
  // </editor-fold>
};
