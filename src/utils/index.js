import coreFetch from '../core/fetch';
import { loginUrl } from '../variables';

export function createConstants(...constants) {
  return constants.reduce((acc, constant) => {
    acc[constant] = constant;
    return acc;
  }, {});
}
export function createReducer(initialState, reducerMap) {
  return (state = initialState, action) => {
    const reducer = reducerMap[action.type];
    return reducer ? reducer(state, action.payload) : state;
  };
}
export function checkHttpStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}
export function parseJSON(response) {
  return response.json();
}

export function setCookie(name, value, hours) {
  if (process.env.BROWSER) {
    let expires = '';
    if (hours) {
      const date = new Date();
      date.setTime(date.getTime() + hours * 60 * 60 * 1000);
      expires = `; expires=${date.toGMTString()}`;
    }
    document.cookie = `${name}=${value}${expires}; path=/`;
  }
}

export function readCookieFrom(cookies, name) {
  const nameEQ = `${name}=`;
  const ca = cookies.split(';');
  let i = 0;
  const length = ca.length;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

export function getCookie(name, cookie = null) {
  if (!cookie && process.env.BROWSER) {
    return readCookieFrom(document.cookie, name);
  } else if (cookie) {
    return readCookieFrom(cookie, name);
  }
  return null;
}

export function deleteCookie(name) {
  setCookie(name, '', -1);
}

export const FetchData = async (url, fetch = coreFetch) => {
  const defaults = {
    method: 'GET',
  };
  const resp = await fetch(url, defaults);
  const data = await resp.json();
  if (data.code === 401 || data.code === 403) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
  }
  return data;
};

export const FetchDataAuth = async (url, token, fetch = coreFetch) => {
  const defaults = {
    method: 'GET',
    headers: {
      Authorization: `bearer ${token}`,
    },
  };
  const resp = await fetch(url, defaults, fetch);
  const data = await resp.json();
  if (data.code === 401 || data.code === 403) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
  }
  return data;
};

export const DeleteData = async (url, fetch = coreFetch) => {
  const defaults = {
    method: 'DELETE',
  };
  const response = await fetch(url, defaults, fetch);
  let data;

  if (response.status === 200 || response.status === 201) {
    data = await response.json();
    return data;
  } else if (response.status === 403) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 401) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 404) {
    data = await response.json();
    return data;
  }
  return response;
};

export const AuthDeleteData = async (url, token = '', fetch = coreFetch) => {
  const defaults = {
    method: 'DELETE',
    headers: {
      Authorization: `bearer ${token}`,
    },
  };
  const response = await fetch(url, defaults, fetch);
  let data;

  if (response.status === 200 || response.status === 201) {
    data = await response.json();
    return data;
  } else if (response.status === 403) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 401) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 404) {
    data = await response.json();
    return data;
  }
  return response;
};

export const PostData = async (url, body, fetch = coreFetch) => {
  const defaults = {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  };

  const response = await fetch(url, defaults);
  let data;
  if (response.status === 200 || response.status === 201) {
    data = await response.json();
    return data;
  } else if (response.status === 403) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 401) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 404) {
    data = await response.json();
    return data;
  }
  return response;
};

export const AuthPostData = async (url, body, token, fetch = coreFetch) => {
  const defaults = {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `bearer ${token}`,
    },
    body: JSON.stringify(body),
  };

  const response = await fetch(url, defaults);
  let data;
  if (response.status === 200 || response.status === 201) {
    data = await response.json();
    return data;
  } else if (response.status === 403) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 401) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 404) {
    data = await response.json();
    return data;
  }
  return response;
};

export const PutData = async (url, body, fetch = coreFetch) => {
  const defaults = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  };
  const response = await fetch(url, defaults);

  let data;
  if (response.status === 200 || response.status === 201) {
    data = await response.json();
    return data;
  } else if (response.status === 403) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 401) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 404) {
    data = await response.json();
    return data;
  }
  return response;
};

export const AuthPutData = async (url, body, token, fetch = coreFetch) => {
  const defaults = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `bearer ${token}`,
    },
    body: JSON.stringify(body),
  };
  const response = await fetch(url, defaults);
  let data;
  if (response.status === 200 || response.status === 201) {
    data = await response.json();
    return data;
  } else if (response.status === 403) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 401) {
    if (process.env.BROWSER) {
      window.location = loginUrl;
    }
    data = await response.json();
    return data;
  } else if (response.status === 404) {
    data = await response.json();
    return data;
  }
  return response;
};

export function isNumber(number) {
  const isnum = /^\d+$/;
  return isnum.test(number);
}

export function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
