import React from 'react';
import firebase from 'firebase';
import { notification } from 'antd';
import Link from '../../components/Link';
import { authPostAppUserInfo } from '../../services/identityApi';
import { BaseCRUDDtoBuilder } from '../../dtos/dtoBuilder';
import { UserAppInfo } from '../../dtos/identityDtos';
import { getCookie } from '../index';

export function initializePush() {
  const messaging = firebase.messaging();
  messaging.usePublicVapidKey(
    'BPJ9syq7vmIjvkDSNjToZKhDdfnupjJJnZgNJ9mL5Xs14ObNOzlue8WzuteBNQeb-CP201HdHx4T2MJzgFMc-iQ',
  );
  messaging
    .requestPermission()
    .then(() => messaging.getToken())
    .then(token => {
      const adminToken = getCookie('token');
      const appType = getCookie('appType');

      authPostAppUserInfo(
        new BaseCRUDDtoBuilder()
          .dto(new UserAppInfo({ token, os: 'web', appType }))
          .build(),
        adminToken,
      );
    })
    .catch(error => {
      if (error.code === 'messaging/permission-blocked') {
        console.log('Please Unblock Notification Request Manually');
      } else {
        console.log('Error Occurred', error);
      }
    });

  messaging.onMessage(payload => {
    console.log('payload', payload);
    let title = '';
    const notif = JSON.parse(payload.data.custom_notification);
    if (notif.type === 'Message')
      title = (
        <Link
          to={`/message/list?messageId=${notif.meta_data.messageId}&mode=${
            notif.meta_data.mode
          }`}
        >
          {notif.title}
        </Link>
      );
    else if (notif.type === 'NewBill')
      title = <Link to="/order/list">{notif.title}</Link>;

    notification.config({
      placement: 'bottomRight',
      bottom: 50,
      duration: 300,
    });
    notification.open({
      message: title,
      description: notif.body,
    });
  });
}
