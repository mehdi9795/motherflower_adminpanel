module.exports = {
  getResponseModel: {
    items: [],
    count: 0,
  },
  treeModel: {
    items: [],
    allowLink: false,
    allowLastLeafSelect: false,
  },

  treeSelectModel: {
    items: [],
  },
};
