import { notification } from 'antd';

export function checkPermission(name, flatPermission) {
  let result = '';
  try {
    Object.keys(flatPermission).map(item => {
      if (flatPermission[item].resourceValue === name) {
        // if (flatPermission[item].permissionAttribute === 'Readonly') {
        //   console.log('item',item);
        // } else if (flatPermission[item].permissionAttribute === 'Hide') {
        //   console.log('item',item);
        // }
        result = 'hide';
        throw result;
      }
      return null;
    });
    throw result;
  } catch (e) {
    return e;
  }
}

export function generateArrayQueryString(items, pattern) {
  let output = `[]`;
  if (items.length > 0) {
    output = ``;
    items.map(item => {
      output = `${output}${pattern.replace(`0`, item)},`;
    });
    output = output.slice(0, -1);
  }
  return output;
}

export const getDtoQueryString = getDto => {
  const output = getDto.replace(/['"]+/g, '');
  return output;
};

export const showNotification = (type, title, description, duration = 100) => {
  notification[type]({
    bottom: 15,
    duration,
    className: 'successBox',
    placement: 'bottomRight',
    message: title,
    description,
  });
};

export const pressEnterAndCallApplySearch = applySearch => {
  const inputs = document.getElementsByClassName('searchBox')[0];
  if (inputs) {
    const inputsFinal = inputs.getElementsByTagName('input');
    if (inputsFinal) {
      for (let i = 0; i < inputsFinal.length; i += 1) {
        if (
          inputsFinal[i].type.toLowerCase() === 'text' ||
          inputsFinal[i].type.toLowerCase() === 'tel'
        )
          inputsFinal[i].onkeypress = e => {
            if (e.key === 'Enter') applySearch();
          };
      }
    }
  }
};
