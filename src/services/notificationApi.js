import {
  BaseAuthDelete,
  BaseAuthGet,
  BaseAuthPost,
  BaseAuthPut,
} from './baseApi';

export const GetNotificationApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/UserMessage/UserMessages', dto);
export const PostNotificationApi = (token, dto) =>
  BaseAuthPost(token, '/Notification/Message/Messages', dto);
export const PutNotificationApi = (token, dto) =>
  BaseAuthPut(token, '/Notification/Message/Messages', dto);
export const DeleteNotificationApi = (token, id) =>
  BaseAuthDelete(token, `/Notification/Message/Messages/${id}`);

export const GetRequestTypeApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/RequestType/RequestTypes', dto);


export const GetPushApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/Push/Pushes', dto);
export const PostPushApi = (token, dto) =>
  BaseAuthPost(token, '/Notification/Push/Pushes', dto);
export const PutPushApi = (token, dto) =>
  BaseAuthPut(token, '/Notification/Push/Pushes', dto);
export const DeletePushApi = (token, id) =>
  BaseAuthDelete(token, `/Notification/Push/Pushes/${id}`);

export const GetPushStatusApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/Push/PushStatus', dto);

export const GetPushTestGroupApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/PushTestGroup/PushTestGroups', dto);
export const PostPushTestGroupApi = (token, dto) =>
  BaseAuthPost(token, '/Notification/PushTestGroup/PushTestGroups', dto);
export const PutPushTestGroupApi = (token, dto) =>
  BaseAuthPut(token, '/Notification/PushTestGroup/PushTestGroups', dto);
export const DeletePushTestGroupApi = (token, dto) =>
  BaseAuthDelete(token, `/Notification/PushTestGroup/PushTestGroups?dtos=${dto}`);


export const PostSendPushApi = (token, dto) =>
  BaseAuthPost(token, '/Notification/Push/Send', dto);

export const GetSmsBulkApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/SMSBulk/SMSBulks', dto);
export const PostSmsBulkApi = (token, dto) =>
  BaseAuthPost(token, '/Notification/SMSBulk/SMSBulks', dto);
export const PutSmsBulkApi = (token, dto) =>
  BaseAuthPut(token, '/Notification/SMSBulk/SMSBulks', dto);
export const DeleteSmsBulkApi = (token, id) =>
  BaseAuthDelete(token, `/Notification/SMSBulk/SMSBulks/${id}`);

export const GetSmsStatusApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/SMSBulk/SMSBulkStatus', dto);

export const SendSmsBulkApi = (token, dto) =>
  BaseAuthPost(token, '/Notification/SMSBulk/Send', dto);
