import {
  BaseAuthGet,
  BaseAuthDelete,
  BaseAuthPut,
  BaseAuthPost,
} from './baseApi';

// <editor-fold dsc="Product Apis">
export const getProductApi = (token, dto) =>
  BaseAuthGet(token, '/Catalog/Product/Products', dto);

export const postProductApi = (token, dto) =>
  BaseAuthPost(token, '/Catalog/Product/Products', dto);
export const putProductApi = (token, dto) =>
  BaseAuthPut(token, '/Catalog/Product/Products', dto);
export const deleteProductApi = (token, id) =>
  BaseAuthDelete(token, `/Catalog/Product/Products/${id}`);

export const postProductNamesApi = (token, dto) =>
  BaseAuthGet(token, '/Catalog/Product/ProductNameSearch', dto);

export const getProductTypesApi = (token, dto) =>
  BaseAuthGet(token, '/Catalog/ProductType/ProductTypes', dto);

export const getCategoryApi = (token, dto) =>
  BaseAuthGet(token, '/Catalog/Category/Categories', dto);

export const postCategoryApi = (token, dto) =>
  BaseAuthPost(token, '/Catalog/Category/Categories', dto);

export const putCategoryApi = (token, dto) =>
  BaseAuthPut(token, '/Catalog/Category/Categories', dto);

export const deleteCategoryApi = (token, id) =>
  BaseAuthDelete(token, `/Catalog/Category/Categories/${id}`);

export const getProductSpecificationAttributeApi = (token, dto) =>
  BaseAuthGet(
    token,
    '/Catalog/ProductSpecificationAttribute/ProductSpecificationAttributes',
    dto,
  );
export const postProductSpecificationAttributeApi = (token, dto) =>
  BaseAuthPost(
    token,
    '/Catalog/ProductSpecificationAttribute/ProductSpecificationAttribute',
    dto,
  );
export const putProductSpecificationAttributeApi = (token, dto) =>
  BaseAuthPut(
    token,
    '/Catalog/ProductSpecificationAttribute/ProductSpecificationAttribute',
    dto,
  );
export const deleteProductSpecificationAttributeApi = (token, id) =>
  BaseAuthDelete(
    token,
    `/Catalog/ProductSpecificationAttribute/ProductSpecificationAttribute/${id}`,
  );

export const putVendorBranchProductApi = (token, dto) =>
  BaseAuthPut(token, '/Catalog/VendorBranchProduct/VendorBranchProducts', dto);
// </editor-fold>

// <editor-fold dsc="color option Apis">
export const getColorOptionApi = (token, dto) =>
  BaseAuthGet(token, '/Catalog/ColorOption/ColorOptions', dto);

export const postColorOptionApi = (token, dto) =>
  BaseAuthPost(token, '/Catalog/ColorOption/ColorOptions', dto);

export const putColorOptionApi = (token, dto) =>
  BaseAuthPut(token, '/Catalog/ColorOption/ColorOptions', dto);

export const deleteColorOptionApi = (token, id) =>
  BaseAuthDelete(token, `/Catalog/ColorOption/ColorOptions/${id}`);
// </editor-fold>

// <editor-fold dsc="specification attribute">
export const getSpecificationAttributeApi = (token, dto) =>
  BaseAuthGet(
    token,
    '/Catalog/SpecificationAttribute/SpecificationAttributes',
    dto,
  );
export const postSpecificationAttributeApi = (token, dto) =>
  BaseAuthPost(
    token,
    '/Catalog/SpecificationAttribute/SpecificationAttributes',
    dto,
  );
export const putSpecificationAttributeApi = (token, dto) =>
  BaseAuthPut(
    token,
    '/Catalog/SpecificationAttribute/SpecificationAttributes',
    dto,
  );
export const deleteSpecificationAttributeApi = (token, id) =>
  BaseAuthDelete(
    token,
    `/Catalog/SpecificationAttribute/SpecificationAttributes/${id}`,
  );
// </editor-fold>

// <editor-fold dsc="product time">
export const getProductTimeApi = (token, dto) =>
  BaseAuthGet(
    token,
    '/Catalog/PreparingProductTime/PreparingProductTimes',
    dto,
  );
export const putProductTimeApi = (token, dto) =>
  BaseAuthPut(
    token,
    '/Catalog/PreparingProductTime/PreparingProductTimes',
    dto,
  );
// </editor-fold>

// <editor-fold dsc="tag Apis">
export const getTagApi = (token, dto) =>
  BaseAuthGet(token, '/Catalog/Tag/Tags', dto);

export const postTagApi = (token, dto) =>
  BaseAuthPost(token, '/Catalog/Tag/Tags', dto);
export const putTagApi = (token, dto) =>
  BaseAuthPut(token, '/Catalog/Tag/Tags', dto);
export const deleteTagApi = (token, id) =>
  BaseAuthDelete(token, `/Catalog/Tag/Tags/${id}`);

export const getRelatedTagApi = (token, dto) =>
  BaseAuthGet(token, '/Catalog/RelatedTag/RelatedTags', dto);

// </editor-fold>

// <editor-fold dsc="InventoryManagement Apis">
export const getInventoryManagementApi = (token, dto) =>
  BaseAuthGet(token, '/Catalog/InventoryHistory/InventoryHistories', dto);

export const postInventoryManagementApi = (token, dto) =>
  BaseAuthPost(token, '/Catalog/InventoryHistory/InventoryHistories', dto);

export const putInventoryManagementApi = (token, dto) =>
  BaseAuthPut(token, '/Catalog/InventoryHistory/InventoryHistories', dto);

export const postSyncPreparingProductTime = token =>
  BaseAuthPost(
    token,
    '/Catalog/PreparingProductTime/SyncPreparingProductTime',
    {},
  );

// </editor-fold>
