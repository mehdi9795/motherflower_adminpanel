import {
  BaseAuthGet,
  BaseAuthDelete,
  BaseAuthPut,
  BaseAuthPost, BaseGenericDto,
} from './baseApi';

// <editor-fold dsc="Agent Apis">
export const getAgentApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/Agent/Agents', dto);
export const getAgentUserApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranchUser/VendorBranchUserByVendorId', dto);
export const postAgentApi = (token, dto) =>
  BaseAuthPost(token, 'Vendor/Agent/Agents', dto);
export const putAgentApi = (token, dto) =>
  BaseAuthPut(token, 'Vendor/Agent/Agents', dto);
export const deleteAgentApi = (token, id) =>
  BaseAuthDelete(token, `Vendor/Agent/Agents/${id}`);
// </editor-fold>

// <editor-fold dsc="Vendor Branch Apis">
export const getVendorBranchApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranch/VendorBranches', dto);
export const postVendorBranchApi = (token, dto) =>
  BaseAuthPost(token, 'Vendor/VendorBranch/VendorBranches', dto);
export const putVendorBranchApi = (token, dto) =>
  BaseAuthPut(token, 'Vendor/VendorBranch/VendorBranches', dto);
export const deleteVendorBranchApi = (token, id) =>
  BaseAuthDelete(token, `Vendor/VendorBranch/VendorBranches/${id}`);

// </editor-fold>

// <editor-fold dsc="Vendor branch facility">
export const getFacilityApi = (token, dto) =>
  BaseAuthGet(token, '/Vendor/Facility/Facilities', dto);

export const postFacilityApi = (token, dto) =>
  BaseAuthPost(token, '/Vendor/Facility/Facilities', dto);

export const getVendorBranchFacilityApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranchFacility/VendorBranchFacilities', dto);

export const postVendorBranchFacilityApi = (token, dto) =>
  BaseAuthPost(
    token,
    'Vendor/VendorBranchFacility/VendorBranchFacilities',
    dto,
  );
// </editor-fold>

// <editor-fold dsc="Vendor Branch OffShifts">
export const getOffShiftApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranchOffShift/VendorBranchOffShifts', dto);

export const postOffShiftApi = (token, dto) =>
  BaseAuthPost(token, 'Vendor/VendorBranchOffShift/VendorBranchOffShifts', dto);

export const deleteOffShiftApi = (token, id) =>
  BaseAuthDelete(
    token,
    `Vendor/VendorBranchOffShift/VendorBranchOffShifts/${id}`,
  );

// </editor-fold>ies

// <editor-fold dsc="Vendor Branch user">
export const getVendorBranchUserApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranchUser/VendorBranchUsers', dto);

export const postVendorBranchUserApi = (token, dto) =>
  BaseAuthPost(token, 'Vendor/VendorBranchUser/VendorBranchUsers', dto);

export const putVendorBranchUserApi = (token, dto) =>
  BaseAuthPut(token, 'Vendor/VendorBranchUser/VendorBranchUsers', dto);

export const deleteVendorBranchUserApi = (token, id) =>
  BaseAuthDelete(token, `Vendor/VendorBranchUser/VendorBranchUsers/${id}`);
// </editor-fold>ies

// <editor-fold dsc="Vendor Branch zone">
export const getVendorBranchZoneApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranchZone/VendorBranchZones', dto);

export const postVendorBranchZoneApi = (token, dto) =>
  BaseAuthPost(token, 'Vendor/VendorBranchZone/VendorBranchZones', dto);

export const putVendorBranchZoneApi = (token, dto) =>
  BaseAuthPut(token, 'Vendor/VendorBranchZone/VendorBranchZones', dto);

export const deleteVendorBranchZoneApi = (token, id) =>
  BaseAuthDelete(token, `Vendor/VendorBranchZone/VendorBranchZones/${id}`);
// </editor-fold>ies

// <editor-fold dsc="Vendor Branch category">
export const getVendorBranchCategoryApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranchCategory/VendorBranchCategoreis', dto);

// </editor-fold>ies

// <editor-fold dsc="Agent Apis">
export const getVendorAffiliateApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorAffiliate/VendorAffiliates', dto);
export const putVendorAffiliateApi = (token, dto) =>
  BaseAuthPut(token, 'Vendor/VendorAffiliate/VendorAffiliates', dto);
export const deleteVendorAffiliateApi = (token, id) =>
  BaseAuthDelete(token, `Vendor/VendorAffiliate/VendorAffiliates/${id}`);
// </editor-fold>

// <editor-fold dsc="vendor branch cities Apis">
export const postVendorBranchCitiesApi = (token, dto) =>
  BaseAuthPost(token, 'Vendor/VendorBranchCity/VendorBranchCities', dto);
export const getVendorBranchCityApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranchCity/VendorBranchCities', dto);
// </editor-fold>


export const getCalendarExceptionApi = (dto) =>
  BaseGenericDto('/Vendor/CalendarException', dto);

export const getAvailableProductShiftApi = (dto) =>
  BaseGenericDto('/Vendor/AvailableProductShift', dto);
