import {
  BasePost,
  BaseAuthDelete,
  BaseAuthPut,
  BaseAuthPost,
  BaseAuthGet,
} from './baseApi';

export const getUserApi = (token, dto) =>
  BaseAuthGet(token, 'Identity/User/Users', dto);
export const postUserApi = (token, dto) =>
  BaseAuthPost(token, 'Identity/User/Users', dto);
export const putUserApi = (token, dto) =>
  BaseAuthPut(token, 'Identity/User/Users', dto);
export const deleteUserApi = (token, id) =>
  BaseAuthDelete(token, `Identity/User/Users/${id}`);

export const postAuthenticateApi = dto => BasePost('Panel/authenticate', dto);

export const getRoleApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/Role/Roles', dto);

export const getPermissionApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/Permission/Permissions', dto);

export const getUserRoleApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserRole/UserRoles', dto);

export const postUserRoleApi = (token, dto) =>
  BaseAuthPost(token, '/Identity/UserRole/UserRoles', dto);
export const deleteUserRoleApi = (token, dto) =>
  BaseAuthDelete(token, `/Identity/UserRole/UserRoles${dto}`);

export const getUserAddressApi = (token, dto) =>
  BaseAuthGet(token, 'Identity/Address/Addresses', dto);
export const postUserAddressApi = (token, dto) =>
  BaseAuthPost(token, 'Identity/Address/Addresses', dto);
export const putUserAddressApi = (token, dto) =>
  BaseAuthPut(token, 'Identity/Address/Addresses', dto);
export const deleteUserAddressApi = (token, id) =>
  BaseAuthDelete(token, `Identity/Address/Addresses/${id}`);

export const UserVerificationApi = dto =>
  BasePost('/Identity/Account/UserVerification', dto);

export const SendVerificationCodeApi = dto =>
  BasePost('/Identity/Account/SendVerificationCode', dto);

export const ChangePasswordApi = dto =>
  BasePost('/Identity/Account/ChangePassword', dto);

export const ChangePasswordAuthApi = (token, dto) =>
  BaseAuthPost(token, '/Identity/Account/ChangePassword', dto);

export const ChangePasswordPutAuthApi = (token, dto) =>
  BaseAuthPut(token, '/Identity/User/ChangePassword', dto);

export const getUserAppInfoApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserAppInfo/UserAppInfos', dto);

export const authPostAppUserInfo = (dto, token) =>
  BaseAuthPost(token, 'Identity/UserAppInfo/UserAppInfos', dto);

export const getUserReportSettingApi = (token, dto) =>
  BaseAuthGet(token, 'Identity/UserReportSetting/UserReportSettings', dto);

export const postUserReportSettingApi = (token, dto) =>
  BaseAuthPost(token, 'Identity/UserReportSetting/UserReportSettings', dto);

export const getUserAppInfoAppTypesApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserAppInfoAppType/UserAppInfoAppTypes', dto);

export const deleteUserEventApi = (token, id) =>
  BaseAuthDelete(token, `/Identity/UserEvent/UserEvent/${id}`);

export const getUserEventTypeApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserEventType/UserEventTypes', dto);

export const getUserRelationShipApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserRelationShip/UserRelationShips', dto);

export const getUserEventApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserEvent/UserEvents', dto);

export const postUserEventApi = (token, dto) =>
  BaseAuthPost(token, '/Identity/UserEvent/UserEvent', dto);

export const putUserEventApi = (token, dto) =>
  BaseAuthPut(token, '/Identity/UserEvent/UserEvent', dto);

export const GetAddressUser = (token, dto) =>
  BaseAuthGet(token, '/Identity/Address/Addresses', dto);
export const PostAddressUser = (token, dto) =>
  BaseAuthPost(token, '/Identity/Address/Addresses', dto);
export const PutAddressUser = (token, dto) =>
  BaseAuthPut(token, '/Identity/Address/Addresses', dto);
export const DeleteAddressUser = (token, id) =>
  BaseAuthDelete(token, `/Identity/Address/Addresses/${id}`);
