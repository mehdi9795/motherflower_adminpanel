import apisauce from 'apisauce';
import { message } from 'antd';

// const baseURL = 'http://192.168.110.12:4060/';
// const baseURL = 'https://192.168.120.12:4070/';
// const baseURL = 'http://104.227.248.180:4070/';
// const baseURL = 'http://104.227.248.180:4060/';
//            const baseURL = 'http://localhost:50454/';
const baseURL = 'https://api.motherflower.com:4070/';

// const baseURL = 'http://192.168.110.12:4070/';
export const baseApi = apisauce.create({
  baseURL,
  headers: { 'Content-Type': 'application/json' },
  timeout: 10000,
});

export const BaseAuthGet = async (token, url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `Bearer ${token}`,
      // 'ss-id': `${token}`,
      'Content-Type': 'application/json',
    });
    let response = baseApi.get(url, { container: dto });
    if (response.status !== 401) {
      if (response.status === 500) {
        message.error(response.data.errorMessage);
      }
    } else {
      window.location = '/login';

      response = undefined;
    }
    resolve(response);
  });
  return func;
};

export const BasePost = async (url, dto) => {
  const func = new Promise(resolve => {
    const response = baseApi.post(url, JSON.stringify(dto));
    if (response.status === 500) {
      message.error(response.data.errorMessage);
    }
    resolve(response);
  });
  return func;
};

export const BaseAuthPost = async (token, url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `Bearer ${token}`,
      // 'ss-id': `${token}`,
      'Content-Type': 'application/json',
    });
    let response = baseApi.post(url, JSON.stringify(dto));
    if (response.status === 401) {
      window.location = '/login';
      response = undefined;
    } else if (response.status === 500) {
      message.error(response.data.errorMessage);
    }

    resolve(response);
  });
  return func;
};

export const BaseAuthPut = async (token, url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `Bearer ${token}`,
      // 'ss-id': `${token}`,
      'Content-Type': 'application/json',
    });
    let response = baseApi.put(url, JSON.stringify(dto));

    if (response.status === 401) {
      window.location = '/login';
      response = undefined;
    } else if (response.status === 500) {
      message.error(response.data.errorMessage);
    }
    resolve(response);
  });
  return func;
};

export const BaseAuthDelete = async (token, url) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `Bearer ${token}`,
      // 'ss-id': `${token}`,
      'Content-Type': 'application/json',
    });
    let response = baseApi.delete(url);

    if (response.status === 401) {
      window.location = '/login';
      response = undefined;
    } else if (response.status === 500) {
      message.error(response.data.errorMessage);
    }
    resolve(response);
  });
  return func;
};
export const BaseGenericDto = async (url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `null`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.get(url, { dto });

    resolve(response);
  });
  return func;
};
