import {
  BaseAuthGet,
  BaseAuthDelete,
  BaseAuthPut,
  BaseAuthPost,
} from './baseApi';

// <editor-fold dsc="city Apis">
export const getCityApi = (token, dto) =>
  BaseAuthGet(token, 'Common/City/Cities', dto);
// </editor-fold>

// <editor-fold dsc="district Apis">
export const getDistrictApi = (token, dto) =>
  BaseAuthGet(token, 'Common/District/Districts', dto);
export const postDistrictApi = (token, dto) =>
  BaseAuthPost(token, 'Common/Districts', dto);
export const putDistrictApi = (token, dto) =>
  BaseAuthPut(token, 'Common/Districts', dto);
export const deleteDistrictApi = (token, id) =>
  BaseAuthDelete(token, `Common/Districts/${id}`);

// </editor-fold>

// <editor-fold dsc="district zone Apis">
export const getDistrictZoneApi = (token, dto) =>
  BaseAuthGet(token, 'Common/District/DistrictZones', dto);
export const postDistrictZoneApi = (token, dto) =>
  BaseAuthPost(token, 'Common/District/DistrictZones', dto);
export const deleteDistrictZoneApi = (token, ids) =>
  BaseAuthDelete(token, `Common/District/DistrictZones?ids=[${ids}]`);
// </editor-fold>

// <editor-fold dsc=" zone Apis">
export const getZoneApi = (token, dto) =>
  BaseAuthGet(token, 'Common/Zone/Zones', dto);
export const postZoneApi = (token, dto) =>
  BaseAuthPost(token, 'Common/Zone', dto);
export const putZoneApi = (token, dto) =>
  BaseAuthPut(token, 'Common/Zone', dto);
export const deleteZoneApi = (token, id) =>
  BaseAuthDelete(token, `Common/Zone/${id}`);
// </editor-fold>

// <editor-fold dsc=" public event Apis">
export const getPublicEventApi = (token, dto) =>
  BaseAuthGet(token, 'Common/PublicEvent/PublicEvents', dto);
export const postPublicEventApi = (token, dto) =>
  BaseAuthPost(token, 'Common/PublicEvents', dto);
export const putPublicEventApi = (token, dto) =>
  BaseAuthPut(token, 'Common/PublicEvents', dto);
export const deletePublicEventApi = (token, id) =>
  BaseAuthDelete(token, `Common/PublicEvents/${id}`);
// </editor-fold>

// <editor-fold dsc="report Apis">
export const getReportApi = (token, dto) =>
  BaseAuthGet(token, 'Common/Report/Reports', dto);
export const postReportApi = (token, dto) =>
  BaseAuthPost(token, 'Common/Reports', dto);
// </editor-fold>

export const getNearestLocationsApi = (token, dto) =>
  BaseAuthGet(token, '/Common/District/NearestLocations', dto);

export const getOccasionTypeApi = (token, dto) =>
  BaseAuthGet(token, '/Common/OccasionType/OccasionTypes', dto);
