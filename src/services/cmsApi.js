import {
  BaseAuthGet,
  BaseAuthDelete,
  BaseAuthPut,
  BaseAuthPost,
} from './baseApi';

export const getRateApi = (token, dto) =>
  BaseAuthGet(token, 'CMS/Rate/Rates', dto);
export const postRateApi = (token, dto) =>
  BaseAuthPost(token, '/CMS/Rate', dto);
export const getImageApi = (token, dto) =>
  BaseAuthGet(token, '/CMS/Image/Images', dto);
export const postImageApi = (token, dto) =>
  BaseAuthPost(token, '/CMS/Image', dto);
export const putImageApi = (token, dto) =>
  BaseAuthPut(token, '/CMS/Image', dto);
export const deleteImageApi = (token, id) =>
  BaseAuthDelete(token, `/CMS/Image/${id}`);

export const getBannerApi = (token, dto) =>
  BaseAuthGet(token, '/CMS/HomePageBanner/HomePageBanners', dto);
export const postBannerApi = (token, dto) =>
  BaseAuthPost(token, '/CMS/HomePageBanner', dto);
export const putBannerApi = (token, dto) =>
  BaseAuthPut(token, '/CMS/HomePageBanner', dto);
export const deleteBannerApi = (token, id) =>
  BaseAuthDelete(token, `/CMS/HomePageBanner/${id}`);

export const getCommentApi = (token, dto) =>
  BaseAuthGet(token, '/CMS/Comment/Comments', dto);
export const postCommentApi = (token, dto) =>
  BaseAuthPost(token, '/CMS/Comment', dto);
export const putCommentApi = (token, dto) =>
  BaseAuthPut(token, '/CMS/Comment', dto);
export const deleteCommentApi = (token, id) =>
  BaseAuthDelete(token, `/CMS/Comment/${id}`);
