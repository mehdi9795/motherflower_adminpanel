import {
  BaseAuthDelete,
  BaseAuthGet,
  BaseAuthPost,
  BaseAuthPut,
} from './baseApi';

// <editor-fold dsc="commission">
export const getCommissionApi = (token, dto) =>
  BaseAuthGet(
    token,
    '/SAM/VendorBranchCommission/VendorBranchCommissions',
    dto,
  );

export const putCommissionApi = (token, dto) =>
  BaseAuthPut(
    token,
    '/SAM/VendorBranchCommission/VendorBranchCommissions',
    dto,
  );

export const postCommissionApi = (token, dto) =>
  BaseAuthPost(
    token,
    '/SAM/VendorBranchCommission/VendorBranchCommissions',
    dto,
  );

export const deleteCommissionApi = (token, id) =>
  BaseAuthDelete(
    token,
    `/SAM/VendorBranchCommission/VendorBranchCommissions/${id}`,
  );

// </editor-fold>

// <editor-fold dsc="vendor branch product price">
export const getVendorBranchProductPriceApi = (token, dto) =>
  BaseAuthGet(
    token,
    '/SAM/VendorBranchProductPrice/VendorBranchProductPrices',
    dto,
  );

export const putVendorBranchProductPriceApi = (token, dto) =>
  BaseAuthPut(
    token,
    '/SAM/VendorBranchProductPrice/VendorBranchProductPrices',
    dto,
  );

// </editor-fold>

// <editor-fold dsc="Promotions">
export const getPromotionsApi = (token, dto) =>
  BaseAuthGet(token, 'SAM/Promotion/Promotions', dto);

export const postPromotionApi = (token, dto) =>
  BaseAuthPost(token, 'SAM/Promotion/Promotions', dto);

export const putPromotionApi = (token, dto) =>
  BaseAuthPut(token, 'SAM/Promotion/Promotions', dto);

export const deletePromotionApi = (token, id) =>
  BaseAuthDelete(token, `SAM/Promotion/Promotions/${id}`);

export const postPromotionGroupActionApi = (token, dto) =>
  BaseAuthPost(token, 'SAM/Promotion/PromotionGroupAction', dto);

export const getPromotionTypeApi = (token, dto) =>
  BaseAuthGet(token, 'SAM/Promotion/PromotionType/PromotionTypes', dto);

export const getVendorBranchPromotionsApi = (token, dto) =>
  BaseAuthGet(token, 'SAM/VendorBranchPromotion/VendorBranchPromotions', dto);

export const postVendorBranchPromotionApi = (token, dto) =>
  BaseAuthPost(token, 'SAM/VendorBranchPromotion/VendorBranchPromotions', dto);

export const putVendorBranchPromotionApi = (token, dto) =>
  BaseAuthPut(token, 'SAM/VendorBranchPromotion/VendorBranchPromotions', dto);

export const deleteVendorBranchPromotionApi = (token, id) =>
  BaseAuthDelete(
    token,
    `SAM/VendorBranchPromotion/VendorBranchPromotions/${id}`,
  );

export const getCategoryPromotionDetailsApi = (token, dto) =>
  BaseAuthGet(
    token,
    'SAM/VendorBranchPromotionDetail/VendorBranchPromotionDetails',
    dto,
  );

export const getProductPromotionDetailsApi = (token, dto) =>
  BaseAuthGet(
    token,
    'SAM/VendorBranchPromotionDetail/VendorBranchPromotionDetails',
    dto,
  );
export const postVendorBranchPromotionDetailsApi = (token, dto) =>
  BaseAuthPost(
    token,
    'SAM/VendorBranchPromotionDetail/VendorBranchPromotionDetails',
    dto,
  );

export const putVendorBranchPromotionDetailsApi = (token, dto) =>
  BaseAuthPut(
    token,
    'SAM/VendorBranchPromotionDetail/VendorBranchPromotionDetails',
    dto,
  );

export const deleteVendorBranchPromotionDetailsApi = (token, id) =>
  BaseAuthDelete(
    token,
    `SAM/VendorBranchPromotionDetail/VendorBranchPromotionDetails/${id}`,
  );

export const getPromotionsDiscountCodeApi = (token, dto) =>
  BaseAuthGet(token, 'SAM/DiscountCode/DiscountCodes', dto);

export const postPromotionDiscountCodeApi = (token, dto) =>
  BaseAuthPost(token, 'SAM/DiscountCode', dto);

export const putPromotionDiscountCodeApi = (token, dto) =>
  BaseAuthPut(token, 'SAM/DiscountCode', dto);

export const deletePromotionDiscountCodeApi = (token, id) =>
  BaseAuthDelete(token, `SAM/DiscountCode/${id}`);
// </editor-fold>

// <editor-fold dsc="vendor branch shipping tariff">
export const getVendorBranchShippingTariffApi = (token, dto) =>
  BaseAuthGet(
    token,
    '/SAM/VendorBranchShippingTariff/VendorBranchShippingTariffs',
    dto,
  );

export const postVendorBranchShippingTariffApi = (token, dto) =>
  BaseAuthPost(token, '/SAM/VendorBranchShippingTariffs', dto);

export const putVendorBranchShippingTariffApi = (token, dto) =>
  BaseAuthPut(token, '/SAM/VendorBranchShippingTariffs', dto);

export const deleteVendorBranchShippingTariffApi = (token, id) =>
  BaseAuthDelete(token, `/SAM/VendorBranchShippingTariffs/${id}`);

// </editor-fold>

// <editor-fold dsc="vendor branch shipping tariff promotion">
export const getVendorBranchShippingTariffPromotionApi = (token, dto) =>
  BaseAuthGet(
    token,
    '/SAM/VendorBranchShippingTariffPromotion/VendorBranchShippingTariffPromotions',
    dto,
  );

export const postVendorBranchShippingTariffPromotionApi = (token, dto) =>
  BaseAuthPost(token, '/SAM/VendorBranchShippingTariffPromotions', dto);

export const putVendorBranchShippingTariffPromotionApi = (token, dto) =>
  BaseAuthPut(token, '/SAM/VendorBranchShippingTariffPromotions', dto);

export const deleteVendorBranchShippingTariffPromotionApi = (token, id) =>
  BaseAuthDelete(token, `/SAM/VendorBranchShippingTariffPromotions/${id}`);

// </editor-fold>

// <editor-fold dsc="order">

export const getBillApi = (token, dto) =>
  BaseAuthGet(token, '/SAM/Bill/Bills', dto);

export const putBillApi = (token, dto) => BaseAuthPut(token, '/SAM/Bills', dto);

export const getBillStatusApi = (token, dto) =>
  BaseAuthGet(token, '/SAM/Bill/BillStatuses', dto);

export const getBillItemStatusApi = (token, dto) =>
  BaseAuthGet(token, '/SAM/Bill/BillItemStatuses', dto);

export const getBillItemApi = (token, dto) =>
  BaseAuthGet(token, '/SAM/BillItem/BillItems', dto);

export const putBillItemApi = (token, dto) =>
  BaseAuthPut(token, '/SAM/BillItems', dto);
// </editor-fold>

export const postSendToUserApi = (token, dto) =>
  BaseAuthPost(token, 'SAM/DiscountCode/SendToUser', dto);

export const getOccasionTextPdfApi = (token, dto, isColor) =>
  BaseAuthGet(
    token,
    `SAM/BillItem/OccasionTextPdf?isColor=${isColor}&container=${dto}`,
  );

export const UserBankHistoriesApi = (token, dto) =>
  BaseAuthGet(token, 'SAM/UserBankHistory/UserBankHistories', dto);

export const PostUserBankHistoriesApi = (token, dto) =>
  BaseAuthPost(token, '/SAM/UserBankHistory', dto, true);

export const PutUserBankHistoriesApi = (token, dto) =>
  BaseAuthPut(token, '/SAM/UserBankHistory', dto, true);

export const DeleteUserBankHistoriesApi = (token, id) =>
  BaseAuthDelete(token, `/SAM/UserBankHistory/${id}`, true);

export const BankApi = (token, dto) =>
  BaseAuthGet(token, 'SAM/Bank/Banks', dto);

// <editor-fold dsc="Wallet Promotions">
export const putWalletPromotionApi = (token, dto) =>
  BaseAuthPut(token, 'SAM/WalletPromotions', dto);
// </editor-fold>

export const GetWalletsApi = (token, dto) =>
  BaseAuthGet(token, '/SAM/Wallet/Wallets', dto);

export const PostWalletDepositHistoryApi = (token, dto) =>
  BaseAuthPost(token, '/SAM/WalletDepositHistories', dto);
