export class MessageDto {
  constructor({
    subject = undefined,
    body = undefined,
    parent = undefined,
    userDto = undefined,
    userMessageDtos = undefined,
    searchUserDtos = undefined,
    imageDto = undefined,
    messageSensitivity = undefined,
    requestType = undefined,
    stringCreatedOnUtc = undefined,
    messageSearchMode = undefined,
    messageDto = undefined,
    searchIsRead = undefined,
    id = undefined,
    requestTypeDto = undefined,
  } = {}) {
    this.subject = subject;
    this.body = body;
    this.parent = parent;
    this.userDto = userDto;
    this.userMessageDtos = userMessageDtos;
    this.searchUserDtos = searchUserDtos;
    this.imageDto = imageDto;
    this.messageSensitivity = messageSensitivity;
    this.requestType = requestType;
    this.stringCreatedOnUtc = stringCreatedOnUtc;
    this.messageSearchMode = messageSearchMode;
    this.messageDto = messageDto;
    this.searchIsRead = searchIsRead;
    this.id = id;
    this.requestTypeDto = requestTypeDto;
  }
}

export class RequestTypeDto {
  constructor({
    name = undefined,
    enumType = undefined,
    orderBy = undefined,
    published = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.enumType = enumType;
    this.orderBy = orderBy;
    this.published = published;
    this.id = id;
  }
}

export class PushDto {
  constructor({
    subject = undefined,
    body = undefined,
    icon = undefined,
    id = undefined,
    pushMetadataDtos = undefined,
    entityType = undefined,
    entityId = undefined,
    imageDto = undefined,
    status = undefined,
    persianDateUtc = undefined,
    hour = undefined,
    testStatus = undefined,
  } = {}) {
    this.subject = subject;
    this.body = body;
    this.icon = icon;
    this.pushMetadataDtos = pushMetadataDtos;
    this.id = id;
    this.entityType = entityType;
    this.entityId = entityId;
    this.imageDto = imageDto;
    this.status = status;
    this.persianDateUtc = persianDateUtc;
    this.hour = hour;
    this.testStatus = testStatus;
  }
}

export class PushMetadataDto {
  constructor({
    metaKey = undefined,
    metaValue = undefined,
    pushDto = undefined,
    id = undefined,
  } = {}) {
    this.metaKey = metaKey;
    this.metaValue = metaValue;
    this.pushDto = pushDto;
    this.id = id;
  }
}

export class PushTestGroupDto {
  constructor({
    userAppInfoAppTypeDto = undefined,
    published = undefined,
    id = undefined,
  } = {}) {
    this.userAppInfoAppTypeDto = userAppInfoAppTypeDto;
    this.published = published;
    this.id = id;
  }
}

export class UserAppInfoDto {
  constructor({
    userDto = undefined,
    searchUserDtos = undefined,
    deviceUUID = undefined,
    appVersion = undefined,
    token = undefined,
    os = undefined,
    osVersion = undefined,
    deviceModel = undefined,
    id = undefined,
  } = {}) {
    this.userDto = userDto;
    this.searchUserDtos = searchUserDtos;
    this.deviceUUID = deviceUUID;
    this.appVersion = appVersion;
    this.token = token;
    this.os = os;
    this.osVersion = osVersion;
    this.deviceModel = deviceModel;
    this.id = id;
  }
}

export class UserAppInfoAppTypeDto {
  constructor({
    userDto = undefined,
    userAppInfoDto = undefined,
    token = undefined,
    appType = undefined,
    appVersion = undefined,
    id = undefined,
  } = {}) {
    this.userDto = userDto;
    this.userAppInfoDto = userAppInfoDto;
    this.appType = appType;
    this.appVersion = appVersion;
    this.token = token;
    this.id = id;
  }
}

export class SmsDto {
  constructor({
    body = undefined,
    status = undefined,
    entityType = undefined,
    dateUtc = undefined,
    persianDateUtc = undefined,
    hour = undefined,
    testStatus = undefined,
    smsBulkRoleDtos = undefined,
    smsBulkUserDtos = undefined,
    editable = undefined,
    fromJob = undefined,
    id = undefined,
    smsBulkUserTypes = undefined,
  } = {}) {
    this.body = body;
    this.status = status;
    this.entityType = entityType;
    this.dateUtc = dateUtc;
    this.persianDateUtc = persianDateUtc;
    this.hour = hour;
    this.testStatus = testStatus;
    this.smsBulkRoleDtos = smsBulkRoleDtos;
    this.smsBulkUserDtos = smsBulkUserDtos;
    this.editable = editable;
    this.fromJob = fromJob;
    this.id = id;
    this.smsBulkUserTypes = smsBulkUserTypes;
  }
}

export class SmsBulkRoleDto {
  constructor({ smsBulkDto = undefined, roleDto = undefined } = {}) {
    this.smsBulkDto = smsBulkDto;
    this.roleDto = roleDto;
  }
}

export class SmsBulkUserDto {
  constructor({ smsBulkDto = undefined, userDto = undefined } = {}) {
    this.smsBulkDto = smsBulkDto;
    this.userDto = userDto;
  }
}
