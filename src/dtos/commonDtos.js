export class DistrictDto {
  constructor({
    name = undefined,
    lat = undefined,
    lng = undefined,
    displayOrder = undefined,
    cityId = undefined,
    cityDto = undefined, // instance of cityDto
    id = undefined,
    grade = undefined,
    expectedZoneIds = undefined,
    districtZoneDtos = undefined,
    districtZoneStatusType = undefined,
    published = undefined,
    offered = undefined,
    searchGrade = undefined,
    searchOffered = undefined,
  } = {}) {
    this.name = name;
    this.lat = lat;
    this.lng = lng;
    this.displayOrder = displayOrder;
    this.cityId = cityId;
    this.cityDto = cityDto; // instance of cityDto
    this.id = id;
    this.grade = grade;
    this.expectedZoneIds = expectedZoneIds;
    this.districtZoneDtos = districtZoneDtos;
    this.districtZoneStatusType = districtZoneStatusType;
    this.published = published;
    this.offered = offered;
    this.searchGrade = searchGrade;
    this.searchOffered = searchOffered;
  }
}
export class CityDto {
  constructor({
    name = undefined,
    displayOrder = undefined,
    active = undefined,
    lat = undefined,
    lng = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.displayOrder = displayOrder;
    this.active = active;
    this.lat = lat;
    this.lng = lng;
    this.id = id;
  }
}
export class PublicEventDto {
  constructor({
    name = undefined,
    publicEventType = undefined,
    eventDate = undefined,
    searchFromDate = undefined,
    searchToDate = undefined,
    searchFromPersianDate = undefined,
    searchToPersianDate = undefined,
    persianEventDate = undefined,
    imageUrl = undefined,
    published = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.publicEventType = publicEventType;
    this.eventDate = eventDate;
    this.searchFromDate = searchFromDate;
    this.searchToDate = searchToDate;
    this.searchFromPersianDate = searchFromPersianDate;
    this.searchToPersianDate = searchToPersianDate;
    this.persianEventDate = persianEventDate;
    this.imageUrl = imageUrl;
    this.published = published;
    this.id = id;
  }
}
export class ZoneDto {
  constructor({
    name = undefined,
    active = undefined,
    zoneGrade = undefined,
    description = undefined,
    districts = undefined,
    cityDto = undefined,
    items = undefined,
    count = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.active = active;
    this.zoneGrade = zoneGrade;
    this.description = description;
    this.districts = districts;
    this.cityDto = cityDto;
    this.items = items;
    this.count = count;
    this.id = id;
  }
}
export class DistrictZoneDto {
  constructor({ zone = undefined, district = undefined, id = undefined } = {}) {
    this.zone = zone;
    this.district = district;
    this.id = id;
  }
}
export class ReportDto {
  constructor({
    title = undefined,
    name = undefined,
    category = undefined,
    subSystem = undefined,
    displayOrder = undefined,
    published = undefined,
    id = undefined,
  } = {}) {
    this.title = title;
    this.name = name;
    this.category = category;
    this.subSystem = subSystem;
    this.displayOrder = displayOrder;
    this.id = id;
    this.published = published;
  }
}

export class OccasionTypeDto {
  constructor({
                id = undefined,
                name = undefined,
                occasionTypeDto = undefined,
                published = undefined,
              } = {}) {
    this.id = id;
    this.name = name;
    this.occasionTypeDto = occasionTypeDto;
    this.published = published;
  }
}
