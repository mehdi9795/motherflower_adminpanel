export class UserDto {
  constructor({
    firstName = undefined,
    lastName = undefined,
    email = undefined,
    password = undefined,
    phone = undefined,
    genderType = undefined,
    active = undefined,
    deleted = undefined,
    newsLetter = undefined,
    vendorId = undefined,
    persianDateOfBirth = undefined,
    userName = undefined,
    addressDto = undefined, // array of addressDto
    userRoleDto = undefined, // array of userRoleDto
    id = undefined,
    verify = undefined,
    countryCode = undefined,
    cityDto = undefined,
    imageDto = undefined,
    fromMobile = undefined,
    dayOfBirthDate = undefined,
    monthOfBirthDate = undefined,
    userEventStatus = undefined,
    searchReagentUser = undefined,
    reagentUserDto = undefined,
    offline = undefined,
    searchUserEventStatus = undefined,
    searchOffline = undefined,
  } = {}) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.phone = phone;
    this.genderType = genderType;
    this.active = active;
    this.deleted = deleted;
    this.newsLetter = newsLetter;
    this.vendorId = vendorId;
    this.persianDateOfBirth = persianDateOfBirth;
    this.userName = userName;
    this.addressDto = addressDto; // array of addressDto
    this.userRoleDto = userRoleDto; // array of userRoleDto
    this.id = id;
    this.verify = verify;
    this.countryCode = countryCode;
    this.cityDto = cityDto;
    this.imageDto = imageDto;
    this.fromMobile = fromMobile;
    this.dayOfBirthDate = dayOfBirthDate;
    this.monthOfBirthDate = monthOfBirthDate;
    this.userEventStatus = userEventStatus;
    this.searchReagentUser = searchReagentUser;
    this.reagentUserDto = reagentUserDto;
    this.offline = offline;
    this.searchUserEventStatus = searchUserEventStatus;
    this.searchOffline = searchOffline;
  }
}

export class RoleDto {
  constructor({
    name = undefined,
    isDefault = undefined,
    active = undefined,
    roleStatus = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.isDefault = isDefault;
    this.active = active;
    this.roleStatus = roleStatus;
    this.id = id;
  }
}

export class UserRoleDto {
  constructor({
    userDto = undefined, // instance userDto
    roleDto = undefined, // instance roleDto
    id = undefined,
  } = {}) {
    this.userDto = userDto; // instance userDto
    this.roleDto = roleDto; // instance roleDto
    this.id = id;
  }
}

export class AddressDto {
  constructor({
    fromPanel = undefined,
    userDto = undefined,
    receiverName = undefined,
    lat = undefined,
    lng = undefined,
    content = undefined,
    userId = undefined,
    phone = undefined,
    fax = undefined,
    cityId = undefined,
    districtId = undefined,
    id = undefined,
    active = undefined,
  } = {}) {
    this.fromPanel = fromPanel;
    this.userDto = userDto;
    this.receiverName = receiverName;
    this.lat = lat;
    this.lng = lng;
    this.content = content;
    this.userId = userId;
    this.phone = phone;
    this.fax = fax;
    this.cityId = cityId;
    this.districtId = districtId;
    this.id = id;
    this.active = active;
  }
}

export class AuthenticateDto {
  constructor({
    UserName = undefined,
    Password = undefined,
    nc = undefined,
    State = undefined,
  } = {}) {
    this.UserName = UserName;
    this.Password = Password;
    this.nc = nc;
    this.State = State;
  }
}

export class PermissionDto {
  constructor({
    parentId = undefined,
    resourceTitle = undefined,
    resourceKey = undefined,
    resourceValue = undefined,
    attribute = undefined,
    permissionStatus = undefined,
    resourceType = undefined,
    uiResourceStatus = undefined,
    children = undefined, // array of permissionDto
    id = undefined,
  } = {}) {
    this.parentId = parentId;
    this.resourceTitle = resourceTitle;
    this.resourceKey = resourceKey;
    this.resourceValue = resourceValue;
    this.attribute = attribute;
    this.permissionStatus = permissionStatus;
    this.resourceType = resourceType;
    this.uiResourceStatus = uiResourceStatus;
    this.children = children; // array of permissionDto
    this.id = id;
  }
}

export class UserVerificationDto {
  constructor({
    verificationCode = undefined,
    verificationType = undefined,
    userName = undefined,
    id = undefined,
  } = {}) {
    this.verificationCode = verificationCode;
    this.verificationType = verificationType;
    this.userName = userName;
    this.id = id;
  }
}

export class SendVerificationCodeDto {
  constructor({
    userDto = undefined, // instance userDto
    sendViaEmail = undefined, // instance roleDto
    verificationType = undefined,
    resend = undefined,
    id = undefined,
  } = {}) {
    this.userDto = userDto; // instance userDto
    this.sendViaEmail = sendViaEmail; // instance roleDto
    this.verificationType = verificationType;
    this.resend = resend;
    this.id = id;
  }
}

export class ChangePasswordDto {
  constructor({
    userName = undefined,
    currentPassword = undefined,
    newPassword = undefined,
    newConfirmPassword = undefined,
    id = undefined,
  } = {}) {
    this.userName = userName;
    this.currentPassword = currentPassword;
    this.newPassword = newPassword;
    this.newConfirmPassword = newConfirmPassword;
    this.id = id;
  }
}

export class UserAppInfo {
  constructor({ token = undefined, os = undefined, appType = undefined } = {}) {
    this.token = token;
    this.os = os;
    this.appType = appType;
  }
}

export class UserReportSetting {
  constructor({ reportDto = undefined, region = undefined } = {}) {
    this.reportDto = reportDto;
    this.region = region;
  }
}

export class UserEvent {
  constructor({
    userDto = undefined,
    userEventTypeDto = undefined,
    userRelationShipDto = undefined,
    eventMonth = undefined,
    eventDay = undefined,
    name = undefined,
    customRelationShip = undefined,
    fromPanel = undefined,
    id = undefined,
    published = undefined,
    phone = undefined,
    searchPublished = undefined,
  } = {}) {
    this.userDto = userDto;
    this.userEventTypeDto = userEventTypeDto;
    this.userRelationShipDto = userRelationShipDto;
    this.eventMonth = eventMonth;
    this.eventDay = eventDay;
    this.name = name;
    this.customRelationShip = customRelationShip;
    this.fromPanel = fromPanel;
    this.id = id;
    this.published = published;
    this.phone = phone;
    this.searchPublished = searchPublished;
  }
}

export class UserEventType {
  constructor({ id = undefined } = {}) {
    this.id = id;
  }
}

export class UserRelationShip {
  constructor({ id = undefined } = {}) {
    this.id = id;
  }
}
