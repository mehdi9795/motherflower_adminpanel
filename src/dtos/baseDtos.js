export class BaseGetDto {
  dto = undefined;
  pageIndex = undefined;
  pageSize = undefined;
  disabledCount = undefined;
  all = undefined;
  orderByFields = undefined;
  orderByDescending = undefined;
  ids = undefined;
  notExpectedIds = undefined;
  includes = undefined;
  searchMode = undefined;
  removeChildtoParentList = undefined;
  fromCache = undefined;
}
export class BaseListGetDto {
  dtos = undefined;
  pageIndex = undefined;
  pageSize = undefined;
  disabledCount = undefined;
  all = undefined;
  orderByFields = undefined;
  orderByDescending = undefined;
  ids = undefined;
  notExpectedIds = undefined;
  includes = undefined;
  removeChildtoParentList = undefined;
}
export class BaseCRUDDto {
  id = undefined;
  dto = undefined;
  fromIdentity = undefined;
}
export class BaseListCRUDDto {
  ids = undefined;
  dtos = [];
}
