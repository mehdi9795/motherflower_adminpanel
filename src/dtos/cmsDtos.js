export class ImageDto {
  constructor({
    imagetype = undefined,
    extention = undefined,
    url = undefined,
    binaries = undefined, // array of byte
    showOnHomePage = undefined,
    displayOrder = undefined,
    tooltip = undefined,
    entityId = undefined,
    isCover = undefined,
    entityIds = undefined, // array of number
    id = undefined,
  } = {}) {
    this.imagetype = imagetype;
    this.extention = extention;
    this.url = url;
    this.binaries = binaries; // array of byte
    this.showOnHomePage = showOnHomePage;
    this.displayOrder = displayOrder;
    this.tooltip = tooltip;
    this.entityId = entityId;
    this.isCover = isCover;
    this.entityIds = entityIds; // array of number
    this.id = id;
  }
}

export class RateDto {
  constructor({
    rateType = undefined,
    value = undefined,
    entityId = undefined,
    userId = undefined,
    userFullName = undefined,
    userEmail = undefined,
    persianCreatedOnUtc = undefined,
    persianUpdatedOnUtc = undefined,
    id = undefined,
  } = {}) {
    this.rateType = rateType;
    this.value = value;
    this.entityId = entityId;
    this.userId = userId;
    this.userFullName = userFullName;
    this.userEmail = userEmail;
    this.persianCreatedOnUtc = persianCreatedOnUtc;
    this.persianUpdatedOnUtc = persianUpdatedOnUtc;
    this.id = id;
  }
}

export class CommentDto {
  constructor({
    rateDto = undefined, // instance of rateDto
    commentType = undefined,
    body = undefined,
    entityId = undefined,
    userId = undefined,
    userFullName = undefined,
    userEmail = undefined,
    published = undefined,
    id = undefined,
  } = {}) {
    this.rateDto = rateDto; // instance of rateDto
    this.commentType = commentType;
    this.body = body;
    this.entityId = entityId;
    this.userId = userId;
    this.userFullName = userFullName;
    this.userEmail = userEmail;
    this.published = published;
    this.id = id;
  }
}

export class BannerDto {
  constructor({
    imageDto = undefined,
    link = undefined,
    orderBy = undefined,
    published = undefined,
    id = undefined,
  } = {}) {
    this.imageDto = imageDto;
    this.link = link;
    this.orderBy = orderBy;
    this.published = published;
    this.id = id;
  }
}
