import {
  BaseCRUDDto,
  BaseGetDto,
  BaseListCRUDDto,
  BaseListGetDto,
} from './baseDtos';

export class BaseGetDtoBuilder {
  constructor() {
    this.baseGetDto = new BaseGetDto();
  }
  dto(dto) {
    this.baseGetDto.dto = dto;
    return this;
  }

  pageIndex(pageIndex) {
    this.baseGetDto.pageIndex = pageIndex;
    return this;
  }
  pageSize(pageSize) {
    this.baseGetDto.pageSize = pageSize;
    return this;
  }
  disabledCount(disabledCount) {
    this.baseGetDto.disabledCount = disabledCount;
    return this;
  }
  all(all) {
    this.baseGetDto.all = all;
    return this;
  }
  orderByFields(orderByFields) {
    this.baseGetDto.orderByFields = orderByFields;
    return this;
  }
  orderByDescending(orderByDescending) {
    this.baseGetDto.orderByDescending = orderByDescending;
    return this;
  }
  ids(ids) {
    this.baseGetDto.ids = ids;
    return this;
  }
  notExpectedIds(notExpectedIds) {
    this.baseGetDto.notExpectedIds = notExpectedIds;
    return this;
  }
  includes(includes) {
    this.baseGetDto.includes = includes;
    return this;
  }
  searchMode(searchMode) {
    this.baseGetDto.searchMode = searchMode;
    return this;
  }
  removeChildtoParentList(removeChildtoParentList) {
    this.baseGetDto.removeChildtoParentList = removeChildtoParentList;
    return this;
  }
  fromCache(fromCache) {
    this.baseGetDto.fromCache = fromCache;
    return this;
  }

  build() {
    return this.baseGetDto;
  }

  buildJson() {
    return JSON.stringify(this.baseGetDto);
  }
}

export class BaseListGetDtoBuilder {
  constructor() {
    this.baseListGetDto = new BaseListGetDto();
  }
  dtos(dtos) {
    this.baseListGetDto.dtos = dtos;
    return this;
  }

  pageIndex(pageIndex) {
    this.baseListGetDto.pageIndex = pageIndex;
    return this;
  }
  pageSize(pageSize) {
    this.baseListGetDto.pageSize = pageSize;
    return this;
  }
  disabledCount(disabledCount) {
    this.baseListGetDto.disabledCount = disabledCount;
    return this;
  }
  all(all) {
    this.baseListGetDto.all = all;
    return this;
  }
  orderByFields(orderByFields) {
    this.baseListGetDto.orderByFields = orderByFields;
    return this;
  }
  orderByDescending(orderByDescending) {
    this.baseListGetDto.orderByDescending = orderByDescending;
    return this;
  }
  ids(ids) {
    this.baseListGetDto.ids = ids;
    return this;
  }
  notExpectedIds(notExpectedIds) {
    this.baseListGetDto.notExpectedIds = notExpectedIds;
    return this;
  }
  includes(includes) {
    this.baseListGetDto.includes = includes;
    return this;
  }
  searchMode(searchMode) {
    this.baseListGetDto.searchMode = searchMode;
    return this;
  }
  removeChildtoParentList(removeChildtoParentList) {
    this.baseListGetDto.removeChildtoParentList = removeChildtoParentList;
    return this;
  }

  build() {
    return this.baseListGetDto;
  }

  buildJson() {
    return JSON.stringify(this.baseListGetDto);
  }
}

export class BaseCRUDDtoBuilder {
  constructor() {
    this.baseCRUDDto = new BaseCRUDDto();
  }
  dto(dto) {
    this.baseCRUDDto.dto = dto;
    return this;
  }

  id(id) {
    this.baseCRUDDto.id = id;
    return this;
  }
  fromIdentity(fromIdentity) {
    this.baseCRUDDto.fromIdentity = fromIdentity;
    return this;
  }

  justPublished(justPublished) {
    this.baseCRUDDto.justPublished = justPublished;
    return this;
  }

  build() {
    return this.baseCRUDDto;
  }

  buildJson() {
    return JSON.stringify(this.baseCRUDDto);
  }
}

export class BaseListCRUDDtoBuilder {
  constructor() {
    this.baseListCRUDDto = new BaseListCRUDDto();
  }
  dtos(dtos) {
    this.baseListCRUDDto.dtos = dtos;
    return this;
  }

  ids(ids) {
    this.baseListCRUDDto.ids = ids;
    return this;
  }

  build() {
    return this.baseListCRUDDto;
  }

  buildJson() {
    return JSON.stringify(this.baseListCRUDDto);
  }
}
