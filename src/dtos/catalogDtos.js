export class CategoryDto {
  constructor({
    name = undefined,
    description = undefined,
    metaKeywords = undefined,
    metaDescription = undefined,
    metaTitle = undefined,
    parentCategoryId = undefined,
    pictureId = undefined,
    priceRanges = undefined,
    showOnHomePage = undefined,
    includeundefinedopMenu = undefined,
    published = undefined,
    justParent = undefined,
    justChild = undefined,
    secondaryLevel = undefined,
    makeTree = undefined,
    displayOrder = undefined,
    children = undefined, // array of categoryDto
    imageDtos = undefined, // instance of camDtos>imageDto
    code = undefined,
    id = undefined,
    removeChildtoParentList = undefined,
  } = {}) {
    this.name = name;
    this.description = description;
    this.metaKeywords = metaKeywords;
    this.metaDescription = metaDescription;
    this.metaTitle = metaTitle;
    this.parentCategoryId = parentCategoryId;
    this.pictureId = pictureId;
    this.priceRanges = priceRanges;
    this.showOnHomePage = showOnHomePage;
    this.includeundefinedopMenu = includeundefinedopMenu;
    this.published = published;
    this.justParent = justParent;
    this.justChild = justChild;
    this.secondaryLevel = secondaryLevel;
    this.makeTree = makeTree;
    this.displayOrder = displayOrder;
    this.children = children; // array of categoryDto
    this.imageDtos = imageDtos; // instance of camDtos>imageDto
    this.code = code;
    this.id = id;
    this.removeChildtoParentList = removeChildtoParentList;
  }
}

export class ProductTypeDto {
  constructor({
    name = undefined,
    id = undefined,
    published = undefined,
  } = {}) {
    this.name = name;
    this.id = id;
    this.published = published;
  }
}

export class ProductDto {
  constructor({
    name = undefined,
    code = undefined,
    marked = undefined,
    description = undefined,
    metaKeywords = undefined,
    metaDescription = undefined,
    metaTitle = undefined,
    adminComment = undefined,
    approvedRatingSum = undefined,
    notApprovedRatingSum = undefined,
    approvedTotalReviews = undefined,
    notApprovedTotalReviews = undefined,
    isGiftCard = undefined,
    showOnHomePage = undefined,
    markAsNew = undefined,
    takesTime = undefined,
    markAsNewStartPersianDateTimeUtc = undefined,
    markAsNewEndPersianDateTimeUtc = undefined,
    availableStartPersianDateTimeUtc = undefined,
    availableEndPersianDateTimeUtc = undefined,
    searchFromDateTimeUtc = undefined,
    searchToDateTimeUtc = undefined,
    inventoryStatus = undefined,
    inventoryStartPersianDateTimeUtc = undefined,
    inventoryEndPersianDateTimeUtc = undefined,
    published = undefined,
    displayOrder = undefined,
    persianCreatedOnUtc = undefined,
    persianUpdatedOnUtc = undefined,
    createdUerDto = undefined, // instance of identotyDtos>userDto
    updatedUserDto = undefined, // instance of identotyDtos>userDto
    confirmedUserDto = undefined, // instance of identotyDtos>userDto
    confirmedOnUtc = undefined,
    productTypeDto = undefined, // instance of productTypeDto,
    vendorDto = undefined, // instance of vendorDtos>agentDto
    productCategoryDtos = undefined, // instance of productCategoryDto array
    vendorBranchProductDtos = undefined, // instance from vendorDros>vendorBranchProductDto
    productRelatedCategoryDto = undefined, // instance from productRelatedCategoryDto array
    productTagDtos = undefined, // instance from productTagDto array
    vendorBranchProductPriceDtos = undefined, // instance from samDtos>vendorBranchProductPriceDto array
    productSpecificationAttributeDtos = undefined, // instance from productSpecificationAttributeDto array
    imageDtos = undefined, // instance from cmsDtos>imageDto array
    openCalendarContainerDto = undefined, // instance from vendorDtos> openCalendarContainerDto
    commentDtos = undefined, // instance from cmdDtos>commentDtos
    searchFromBasePrice = undefined,
    searchToBasePrice = undefined,
    searchIsPickupMode = undefined,
    siteCode = undefined,
    id = undefined,
    vendorBranchDto = undefined,
    disablePublishedFilter = undefined,
    vendorCode = undefined,
    b2bSupport = undefined,
    searchB2BSupport = undefined,
    searchFromBasePriceB2B = undefined,
    searchToBasePriceB2B = undefined,
  } = {}) {
    this.name = name;
    this.code = code;
    this.marked = marked;
    this.description = description;
    this.metaKeywords = metaKeywords;
    this.metaDescription = metaDescription;
    this.metaTitle = metaTitle;
    this.adminComment = adminComment;
    this.approvedRatingSum = approvedRatingSum;
    this.notApprovedRatingSum = notApprovedRatingSum;
    this.approvedTotalReviews = approvedTotalReviews;
    this.notApprovedTotalReviews = notApprovedTotalReviews;
    this.isGiftCard = isGiftCard;
    this.showOnHomePage = showOnHomePage;
    this.markAsNew = markAsNew;
    this.takesTime = takesTime;
    this.markAsNewStartPersianDateTimeUtc = markAsNewStartPersianDateTimeUtc;
    this.markAsNewEndPersianDateTimeUtc = markAsNewEndPersianDateTimeUtc;
    this.availableStartPersianDateTimeUtc = availableStartPersianDateTimeUtc;
    this.availableEndPersianDateTimeUtc = availableEndPersianDateTimeUtc;
    this.searchFromDateTimeUtc = searchFromDateTimeUtc;
    this.searchToDateTimeUtc = searchToDateTimeUtc;
    this.inventoryStatus = inventoryStatus;
    this.inventoryStartPersianDateTimeUtc = inventoryStartPersianDateTimeUtc;
    this.inventoryEndPersianDateTimeUtc = inventoryEndPersianDateTimeUtc;
    this.published = published;
    this.displayOrder = displayOrder;
    this.persianCreatedOnUtc = persianCreatedOnUtc;
    this.persianUpdatedOnUtc = persianUpdatedOnUtc;
    this.createdUerDto = createdUerDto; // instance of identotyDtos>userDto
    this.updatedUserDto = updatedUserDto; // instance of identotyDtos>userDto
    this.confirmedUserDto = confirmedUserDto; // instance of identotyDtos>userDto
    this.confirmedOnUtc = confirmedOnUtc;
    this.productTypeDto = productTypeDto; // instance of productTypeDto;
    this.vendorDto = vendorDto; // instance of vendorDtos>agentDto
    this.productCategoryDtos = productCategoryDtos; // instance of productCategoryDto array
    this.vendorBranchProductDtos = vendorBranchProductDtos; // instance from vendorDros>vendorBranchProductDto
    this.productRelatedCategoryDto = productRelatedCategoryDto; // instance from productRelatedCategoryDto array
    this.productTagDtos = productTagDtos; // instance from productTagDto array
    this.vendorBranchProductPriceDtos = vendorBranchProductPriceDtos; // instance from samDtos>vendorBranchProductPriceDto array
    this.productSpecificationAttributeDtos = productSpecificationAttributeDtos; // instance from productSpecificationAttributeDto array
    this.imageDtos = imageDtos; // instance from cmsDtos>imageDto array
    this.openCalendarContainerDto = openCalendarContainerDto; // instance from vendorDtos> openCalendarContainerDto
    this.commentDtos = commentDtos; // instance from cmdDtos>commentDtos
    this.searchFromBasePrice = searchFromBasePrice;
    this.searchToBasePrice = searchToBasePrice;
    this.searchIsPickupMode = searchIsPickupMode;
    this.siteCode = siteCode;
    this.id = id;
    this.vendorBranchDto = vendorBranchDto;
    this.disablePublishedFilter = disablePublishedFilter;
    this.vendorCode = vendorCode;
    this.b2bSupport = b2bSupport;
    this.searchB2BSupport = searchB2BSupport;
    this.searchFromBasePriceB2B = searchFromBasePriceB2B;
    this.searchToBasePriceB2B = searchToBasePriceB2B;
  }
}

export class ProductCategoryDto {
  constructor({
    DisplayOrder = undefined,
    categoryDto = undefined, // instance from categoryDto
    productDto = undefined, // instance from productDto
    id = undefined,
  } = {}) {
    this.DisplayOrder = DisplayOrder;
    this.categoryDto = categoryDto; // instance from categoryDto
    this.productDto = productDto; // instance from productDto
    this.id = id;
  }
}

export class VendorBranchProductDto {
  constructor({
    productDto = undefined,
    vendorBranchDto = undefined,
    id = undefined,
    viewedCount = undefined,
    soldCount = undefined,
    markAsNew = undefined,
    published = undefined,
    inventoryStatus = undefined,
  } = {}) {
    this.productDto = productDto; // instance from productDto
    this.vendorBranchDto = vendorBranchDto; // instance from vendorDtos>vendorBranchDto
    this.id = id;
    this.viewedCount = viewedCount;
    this.soldCount = soldCount;
    this.markAsNew = markAsNew;
    this.published = published;
    this.inventoryStatus = inventoryStatus;
  }
}

export class ProductRelatedCategoryDto {
  constructor({
    DisplayOrder = undefined,
    categoryDto = undefined, // instance from categoryDto
    productDto = undefined, // instance from productDto
    id = undefined,
  } = {}) {
    this.DisplayOrder = DisplayOrder;
    this.categoryDto = categoryDto; // instance from categoryDto
    this.productDto = productDto; // instance from productDto
    this.id = id;
  }
}

export class ProductTagDto {
  constructor({
    displayOrder = undefined,
    tagDto = undefined, // instance from tagDto
    productDto = undefined, // instance from productDto;
    id = undefined,
  } = {}) {
    this.DisplayOrder = displayOrder;
    this.tagDto = tagDto; // instance from tagDto
    this.productDto = productDto; // instance from productDto;
    this.id = id;
  }
}

export class TagDto {
  constructor({
    title = undefined,
    published = undefined,
    reservedVendorBranchDto = undefined,
    RelatedTagDtos = undefined,
    imageDtos = undefined,
    id = undefined,
    categoryTagDtos = undefined,
  } = {}) {
    this.title = title;
    this.published = published;
    this.reservedVendorBranchDto = reservedVendorBranchDto;
    this.RelatedTagDtos = RelatedTagDtos;
    this.imageDtos = imageDtos;
    this.id = id;
    this.categoryTagDtos = categoryTagDtos;
  }
}

export class ProductSpecificationAttributeDto {
  constructor({
    customValue = undefined,
    customValueDescription = undefined,
    displayOrder = undefined,
    productDto = undefined,
    specificationAttributeDto = undefined,
    colorOptionDto = undefined,
    id = undefined,
  } = {}) {
    this.customValue = customValue;
    this.customValueDescription = customValueDescription;
    this.displayOrder = displayOrder;
    this.productDto = productDto; // instance from productDto
    this.specificationAttributeDto = specificationAttributeDto; // instance from specificationAttributeDto
    this.colorOptionDto = colorOptionDto; // instance from colorOptionDto
    this.id = id;
  }
}

export class SpecificationAttributeDto {
  constructor({
    name = undefined,
    allowFiltering = undefined,
    searchAllowFiltering = undefined,
    showOnProductPage = undefined,
    searchShowOnProductPage = undefined,
    specificationAttributeType = undefined,
    displayOrder = undefined,
    id = undefined,
    imageDtos = undefined,
  } = {}) {
    this.name = name;
    this.allowFiltering = allowFiltering;
    this.searchAllowFiltering = searchAllowFiltering;
    this.showOnProductPage = showOnProductPage;
    this.searchShowOnProductPage = searchShowOnProductPage;
    this.specificationAttributeType = specificationAttributeType;
    this.displayOrder = displayOrder;
    this.id = id;
    this.imageDtos = imageDtos;
  }
}

export class ColorOptionDto {
  constructor({
    name = undefined,
    attribute = undefined,
    displayOrder = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.attribute = attribute;
    this.displayOrder = displayOrder;
    this.id = id;
  }
}

export class RelatedProductDto {
  constructor({
    name = undefined,
    code = undefined,
    description = undefined,
    metaKeywords = undefined,
    metaDescription = undefined,
    metaTitle = undefined,
    adminComment = undefined,
    approvedRatingSum = undefined,
    notApprovedRatingSum = undefined,
    approvedTotalReviews = undefined,
    notApprovedTotalReviews = undefined,
    isGiftCard = undefined,
    showOnHomePage = undefined,
    markAsNew = undefined,
    takesTime = undefined,
    markAsNewStartPersianDateTimeUtc = undefined,
    markAsNewEndPersianDateTimeUtc = undefined,
    availableStartPersianDateTimeUtc = undefined,
    availableEndPersianDateTimeUtc = undefined,
    searchFromDateTimeUtc = undefined,
    searchToDateTimeUtc = undefined,
    inventoryStatus = undefined,
    inventoryStartPersianDateTimeUtc = undefined,
    inventoryEndPersianDateTimeUtc = undefined,
    published = undefined,
    displayOrder = undefined,
    persianCreatedOnUtc = undefined,
    persianUpdatedOnUtc = undefined,
    createdUerDto = undefined, // instance of identotyDtos>userDto
    updatedUserDto = undefined, // instance of identotyDtos>userDto
    confirmedUserDto = undefined, // instance of identotyDtos>userDto
    confirmedOnUtc = undefined,
    productTypeDto = undefined, // instance of productTypeDto,
    vendorDto = undefined, // instance of vendorDtos>agentDto
    productCategoryDtos = undefined, // instance of productCategoryDto array
    vendorBranchProductDtos = undefined, // instance from vendorDros>vendorBranchProductDto
    productRelatedCategoryDto = undefined, // instance from productRelatedCategoryDto array
    productTagDtos = undefined, // instance from productTagDto array
    vendorBranchProductPriceDtos = undefined, // instance from samDtos>vendorBranchProductPriceDto array
    productSpecificationAttributeDtos = undefined, // instance from productSpecificationAttributeDto array
    imageDtos = undefined, // instance from cmsDtos>imageDto array
    openCalendarContainerDto = undefined, // instance from vendorDtos> openCalendarContainerDto
    commentDtos = undefined, // instance from cmdDtos>commentDtos
    id = undefined,
  } = {}) {
    this.name = name;
    this.code = code;
    this.description = description;
    this.metaKeywords = metaKeywords;
    this.metaDescription = metaDescription;
    this.metaTitle = metaTitle;
    this.adminComment = adminComment;
    this.approvedRatingSum = approvedRatingSum;
    this.notApprovedRatingSum = notApprovedRatingSum;
    this.approvedTotalReviews = approvedTotalReviews;
    this.notApprovedTotalReviews = notApprovedTotalReviews;
    this.isGiftCard = isGiftCard;
    this.showOnHomePage = showOnHomePage;
    this.markAsNew = markAsNew;
    this.takesTime = takesTime;
    this.markAsNewStartPersianDateTimeUtc = markAsNewStartPersianDateTimeUtc;
    this.markAsNewEndPersianDateTimeUtc = markAsNewEndPersianDateTimeUtc;
    this.availableStartPersianDateTimeUtc = availableStartPersianDateTimeUtc;
    this.availableEndPersianDateTimeUtc = availableEndPersianDateTimeUtc;
    this.searchFromDateTimeUtc = searchFromDateTimeUtc;
    this.searchToDateTimeUtc = searchToDateTimeUtc;
    this.inventoryStatus = inventoryStatus;
    this.inventoryStartPersianDateTimeUtc = inventoryStartPersianDateTimeUtc;
    this.inventoryEndPersianDateTimeUtc = inventoryEndPersianDateTimeUtc;
    this.published = published;
    this.displayOrder = displayOrder;
    this.persianCreatedOnUtc = persianCreatedOnUtc;
    this.persianUpdatedOnUtc = persianUpdatedOnUtc;
    this.createdUerDto = createdUerDto; // instance of identotyDtos>userDto
    this.updatedUserDto = updatedUserDto; // instance of identotyDtos>userDto
    this.confirmedUserDto = confirmedUserDto; // instance of identotyDtos>userDto
    this.confirmedOnUtc = confirmedOnUtc;
    this.productTypeDto = productTypeDto; // instance of productTypeDto;
    this.vendorDto = vendorDto; // instance of vendorDtos>agentDto
    this.productCategoryDtos = productCategoryDtos; // instance of productCategoryDto array
    this.vendorBranchProductDtos = vendorBranchProductDtos; // instance from vendorDros>vendorBranchProductDto
    this.productRelatedCategoryDto = productRelatedCategoryDto; // instance from productRelatedCategoryDto array
    this.productTagDtos = productTagDtos; // instance from productTagDto array
    this.vendorBranchProductPriceDtos = vendorBranchProductPriceDtos; // instance from samDtos>vendorBranchProductPriceDto array
    this.productSpecificationAttributeDtos = productSpecificationAttributeDtos; // instance from productSpecificationAttributeDto array
    this.imageDtos = imageDtos; // instance from cmsDtos>imageDto array
    this.openCalendarContainerDto = openCalendarContainerDto; // instance from vendorDtos> openCalendarContainerDto
    this.commentDtos = commentDtos; // instance from cmdDtos>commentDtos
    this.id = id;
  }
}

export class PreparingProductTimeDto {
  constructor({
    categoryDto = undefined,
    takesTime = undefined,
    vendorBranchDto = undefined,
    id = undefined,
  } = {}) {
    this.categoryDto = categoryDto;
    this.takesTime = takesTime;
    this.vendorBranchDto = vendorBranchDto;
    this.id = id;
  }
}

export class InventoryManagementDto {
  constructor({
    inventoryStatus = undefined,
    searchInventoryStatus = undefined,
    searchFromDateTimeUtc = undefined,
    searchFromPersianDateTimeUtc = undefined,
    searchToDateTimeUtc = undefined,
    searchToPersianDateTimeUtc = undefined,
    vendorBranchDtos = undefined,
    vendorBranchProductDtos = undefined,
    SpecificationAttributeDtos = undefined,
    id = undefined,
  } = {}) {
    this.inventoryStatus = inventoryStatus;
    this.searchInventoryStatus = searchInventoryStatus;
    this.searchFromDateTimeUtc = searchFromDateTimeUtc;
    this.searchFromPersianDateTimeUtc = searchFromPersianDateTimeUtc;
    this.searchToDateTimeUtc = searchToDateTimeUtc;
    this.searchToPersianDateTimeUtc = searchToPersianDateTimeUtc;
    this.vendorBranchDtos = vendorBranchDtos;
    this.vendorBranchProductDtos = vendorBranchProductDtos;
    this.SpecificationAttributeDtos = SpecificationAttributeDtos;
    this.id = id;
  }
}

export class ProductNameSearchDto {
  constructor({ name = undefined, vendorDto = undefined } = {}) {
    this.name = name;
    this.vendorDto = vendorDto;
  }
}

export class SpecificationAttributeDtos {
  constructor({ specificationAttributeDto = undefined } = {}) {
    this.specificationAttributeDto = specificationAttributeDto;
  }
}
