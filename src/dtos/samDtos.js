export class VendorBranchPromotionDto {
  constructor({
    vendorBranchDto = undefined, // instance from vendorDtos>vendorBranchDto
    promotionDto = undefined, // instance from promotionDto
    active = undefined,
    id = undefined,
    promotionType = undefined,
    searchPromotionTypes = undefined,
    name = undefined,
    applyJob = undefined,
  } = {}) {
    this.vendorBranchDto = vendorBranchDto; // instance from vendorDtos>vendorBranchDto
    this.promotionDto = promotionDto; // instance from promotionDto
    this.active = active;
    this.id = id;
    this.promotionType = promotionType;
    this.searchPromotionTypes = searchPromotionTypes;
    this.name = name;
    this.applyJob = applyJob;
  }
}

export class PromotionDto {
  constructor({
    name = undefined,
    persianFromDateUtc = undefined,
    persianToDateUtc = undefined,
    promotionType = undefined,
    promotionTypeName = undefined,
    active = undefined,
    discountCodeQty = undefined,
    promotionDiscountCodeDetailDto = undefined, // instance from promotionDiscountCodeDetailDto,
    id = undefined,
    fromHour = undefined,
    toHour = undefined,
    sendAfterSignUp = undefined,
    promotionPermissionDtos = undefined,
    usedForReagent = undefined,
  } = {}) {
    this.name = name;
    this.persianFromDateUtc = persianFromDateUtc;
    this.persianToDateUtc = persianToDateUtc;
    this.promotionType = promotionType;
    this.promotionTypeName = promotionTypeName;
    this.active = active;
    this.discountCodeQty = discountCodeQty;
    this.promotionDiscountCodeDetailDto = promotionDiscountCodeDetailDto; // instance from promotionDiscountCodeDetailDto;
    this.id = id;
    this.fromHour = fromHour;
    this.toHour = toHour;
    this.sendAfterSignUp = sendAfterSignUp;
    this.promotionPermissionDtos = promotionPermissionDtos;
    this.usedForReagent = usedForReagent;
  }
}

export class PromotionPermissionDto {
  constructor({ promotionDto = undefined, roleDto = undefined } = {}) {
    this.promotionDto = promotionDto;
    this.roleDto = roleDto;
  }
}

export class PromotionTypeDto {
  constructor({ name = undefined, id = undefined } = {}) {
    this.name = name;
    this.id = id;
  }
}

export class PromotionDiscountCodeDetailDto {
  constructor({
    vendorBranchAmountType = undefined,
    boAmountType = undefined,
    vendorBranchAmount = undefined,
    stringVendorBranchAmount = undefined,
    boAmount = undefined,
    stringBoAmount = undefined,
    active = undefined,
    promotionDto = undefined, // instance from promotionDto,
    id = undefined,
  } = {}) {
    this.vendorBranchAmountType = vendorBranchAmountType;
    this.boAmountType = boAmountType;
    this.vendorBranchAmount = vendorBranchAmount;
    this.stringVendorBranchAmount = stringVendorBranchAmount;
    this.boAmount = boAmount;
    this.stringBoAmount = stringBoAmount;
    this.active = active;
    this.promotionDto = promotionDto; // instance from promotionDto;
    this.id = id;
  }
}

export class VendorBranchProductPriceDto {
  constructor({
    vendorBranchDto = undefined, // instance from vendorDtos>vendorBranchDto
    productDto = undefined, // instance from productDto
    productQTYId = undefined,
    deliveryBasePrice = undefined,
    stringDeliveryBasePrice = undefined,
    pickupBasePrice = undefined,
    stringPickupBasePrice = undefined,
    deliveryPromotions = undefined, // instance from vdendorBranchPromotionDetailDto array
    pickupPromotions = undefined, // instance from vdendorBranchPromotionDetailDto array
    deliverySummeryPromotions = undefined, // instance from vdendorBranchPromotionDetailSummeryDto
    pickupSummeryPromotions = undefined, // instance from vdendorBranchPromotionDetailSummeryDto
    id = undefined,
    pickupPriceAfterDiscount = undefined,
    deliveryPriceAfterDiscount = undefined,
    deliveryBasePriceB2B = undefined,
    deliveryPriceDiscountB2B = undefined,
    deliveryPriceAfterDiscountB2B = undefined,
    vendorBranchPickupBasePrice = undefined,
    vendorBranchDeliveryBasePrice = undefined,
  } = {}) {
    this.vendorBranchDto = vendorBranchDto; // instance from vendorDtos>vendorBranchDto
    this.productDto = productDto; // instance from productDto
    this.productQTYId = productQTYId;
    this.deliveryBasePrice = deliveryBasePrice;
    this.stringDeliveryBasePrice = stringDeliveryBasePrice;
    this.pickupBasePrice = pickupBasePrice;
    this.stringPickupBasePrice = stringPickupBasePrice;
    this.deliveryPromotions = deliveryPromotions; // instance from vdendorBranchPromotionDetailDto array
    this.pickupPromotions = pickupPromotions; // instance from vdendorBranchPromotionDetailDto array
    this.deliverySummeryPromotions = deliverySummeryPromotions; // instance from vdendorBranchPromotionDetailSummeryDto
    this.pickupSummeryPromotions = pickupSummeryPromotions; // instance from vdendorBranchPromotionDetailSummeryDto
    this.id = id;
    this.pickupPriceAfterDiscount = pickupPriceAfterDiscount;
    this.deliveryPriceAfterDiscount = deliveryPriceAfterDiscount;
    this.deliveryBasePriceB2B = deliveryBasePriceB2B;
    this.deliveryPriceDiscountB2B = deliveryPriceDiscountB2B;
    this.deliveryPriceAfterDiscountB2B = deliveryPriceAfterDiscountB2B;
    this.vendorBranchPickupBasePrice = vendorBranchPickupBasePrice;
    this.vendorBranchDeliveryBasePrice = vendorBranchDeliveryBasePrice;
  }
}

export class VdendorBranchPromotionDetailDto {
  constructor({
    productDto = undefined, // insatnce from productDto,
    categoryDto = undefined, // insatnce from categoryDto
    vendorBranchAmountType = undefined,
    boAmountType = undefined,
    vendorBranchPromotionDetailType = undefined,
    vendorBranchAmount = undefined,
    vendorBranchMaxAmount = undefined,
    boAmount = undefined,
    boAMaxAmount = undefined,
    active = undefined,
    vendorBranchPromotionDto = undefined, // instance from samDtos>vendorBranchPromotionDto
    id = undefined,
  } = {}) {
    this.productDto = productDto; // insatnce from productDto;
    this.categoryDto = categoryDto; // insatnce from categoryDto
    this.vendorBranchAmountType = vendorBranchAmountType;
    this.boAmountType = boAmountType;
    this.vendorBranchPromotionDetailType = vendorBranchPromotionDetailType;
    this.vendorBranchAmount = vendorBranchAmount;
    this.vendorBranchMaxAmount = vendorBranchMaxAmount;
    this.boAmount = boAmount;
    this.boAMaxAmount = boAMaxAmount;
    this.active = active;
    this.vendorBranchPromotionDto = vendorBranchPromotionDto; // instance from samDtos>vendorBranchPromotionDto
    this.id = id;
  }
}

export class VdendorBranchPromotionDetailSummeryDto {
  constructor({
    amountType = undefined,
    amount = undefined,
    stringAmount = undefined,
    productPriceAfterDiscount = undefined,
    stringProductPriceAfterDiscount = undefined,
    maxAmount = undefined,
    stringMaxAmount = undefined,
    promotionName = undefined,
    id = undefined,
    key = undefined,
  } = {}) {
    this.amountType = amountType;
    this.amount = amount;
    this.stringAmount = stringAmount;
    this.productPriceAfterDiscount = productPriceAfterDiscount;
    this.stringProductPriceAfterDiscount = stringProductPriceAfterDiscount;
    this.maxAmount = maxAmount;
    this.stringMaxAmount = stringMaxAmount;
    this.promotionName = promotionName;
    this.id = id;
    this.key = key;
  }
}

export class VendorBranchCommissionDto {
  constructor({
    vendorBranchId = undefined,
    categoryId = undefined,
    categoryName = undefined,
    value = undefined,
    valueType = undefined,
    discount = undefined,
    discountType = undefined,
    fromPrice = undefined,
    toPrice = undefined,
    active = undefined,
    id = undefined,
  } = {}) {
    this.vendorBranchId = vendorBranchId;
    this.categoryId = categoryId;
    this.categoryName = categoryName;
    this.value = value;
    this.valueType = valueType;
    this.discount = discount;
    this.discountType = discountType;
    this.fromPrice = fromPrice;
    this.toPrice = toPrice;
    this.active = active;
    this.id = id;
  }
}

export class PromotionGroupActionDto {
  constructor({
    promotionId = undefined,
    actionOnCategories = undefined,
    vendorBranchAmountType = undefined,
    boAmountType = undefined,
    vendorBranchAmount = undefined,
    stringVendorBranchAmount = undefined,
    vendorBranchMaxAmount = undefined,
    stringVendorBranchMaxAmount = undefined,
    boAmount = undefined,
    stringBoAmount = undefined,
    boMaxAmount = undefined,
    stringBoMaxAmount = undefined,
    active = undefined,
    id = undefined,
  } = {}) {
    this.promotionId = promotionId;
    this.actionOnCategories = actionOnCategories;
    this.vendorBranchAmountType = vendorBranchAmountType;
    this.boAmountType = boAmountType;
    this.vendorBranchAmount = vendorBranchAmount;
    this.stringVendorBranchAmount = stringVendorBranchAmount;
    this.vendorBranchMaxAmount = vendorBranchMaxAmount;
    this.stringVendorBranchMaxAmount = stringVendorBranchMaxAmount;
    this.boAmount = boAmount;
    this.stringBoAmount = stringBoAmount;
    this.boMaxAmount = boMaxAmount;
    this.stringBoMaxAmount = stringBoMaxAmount;
    this.active = active;
    this.id = id;
  }
}

export class VendorBranchPromotionDetailDto {
  constructor({
    id = undefined,
    productDto = undefined, // instance of catalogDtos>productDtos,
    categoryDto = undefined, // instance of catalogDtos>categoryDtos,
    vendorBranchAmountType = undefined,
    boAmountType = undefined,
    vendorBranchPromotionDetailType = undefined,
    vendorBranchAmount = undefined,
    stringVendorBranchAmount = undefined,
    vendorBranchMaxAmount = undefined,
    stringVendorBranchMaxAmount = undefined,
    boAmount = undefined,
    stringBoAmount = undefined,
    boMaxAmount = undefined,
    stringBoMaxAmount = undefined,
    active = undefined,
    vendorBranchPromotionDto = undefined, // instance from samDtos>vendorBranchPromotionDto
    applyJob = undefined,
    stringPickupDeliveryDeadline = undefined,
  } = {}) {
    this.id = id;
    this.productDto = productDto; // instance of catalogDtos>productDtos;
    this.categoryDto = categoryDto; // instance of catalogDtos>categoryDtos;
    this.vendorBranchAmountType = vendorBranchAmountType;
    this.boAmountType = boAmountType;
    this.vendorBranchPromotionDetailType = vendorBranchPromotionDetailType;
    this.vendorBranchAmount = vendorBranchAmount;
    this.stringVendorBranchAmount = stringVendorBranchAmount;
    this.vendorBranchMaxAmount = vendorBranchMaxAmount;
    this.stringVendorBranchMaxAmount = stringVendorBranchMaxAmount;
    this.boAmount = boAmount;
    this.stringBoAmount = stringBoAmount;
    this.boMaxAmount = boMaxAmount;
    this.stringBoMaxAmount = stringBoMaxAmount;
    this.active = active;
    this.vendorBranchPromotionDto = vendorBranchPromotionDto; // instance from samDtos>vendorBranchPromotionDto
    this.applyJob = applyJob;
    this.stringPickupDeliveryDeadline = stringPickupDeliveryDeadline;
  }
}

export class DiscountCodeDto {
  constructor({
    code = undefined,
    active = undefined,
    searchFromDateTimeUtc = undefined,
    searchToDateTimeUtc = undefined,
    persianSearchFromDateTimeUtc = undefined,
    persianSearchToDateTimeUtc = undefined,
    promotionDto = undefined, // instance from samDtos>PromotionDto,
    freeQuota = undefined,
    usedQuota = undefined,
    id = undefined,
    searchReservedUser = undefined,
    searchUsedQuota = undefined,
  } = {}) {
    this.code = code;
    this.active = active;
    this.searchFromDateTimeUtc = searchFromDateTimeUtc;
    this.searchToDateTimeUtc = searchToDateTimeUtc;
    this.persianSearchFromDateTimeUtc = persianSearchFromDateTimeUtc;
    this.persianSearchToDateTimeUtc = persianSearchToDateTimeUtc;
    this.promotionDto = promotionDto; // instance from samDtos>PromotionDto;
    this.freeQuota = freeQuota;
    this.usedQuota = usedQuota;
    this.id = id;
    this.searchReservedUser = searchReservedUser;
    this.searchUsedQuota = searchUsedQuota;
  }
}

export class VendorBranchShippingTariffsDto {
  constructor({
    vendorBranchDto = undefined,
    categoryDto = undefined,
    zoneDto = undefined,
    qty = undefined,
    shippingAmount = undefined,
    vendorBranchShippingTariffPromotionDtos = undefined,
    id = undefined,
  } = {}) {
    this.vendorBranchDto = vendorBranchDto;
    this.categoryDto = categoryDto;
    this.zoneDto = zoneDto;
    this.qty = qty;
    this.shippingAmount = shippingAmount;
    this.vendorBranchShippingTariffPromotionDtos = vendorBranchShippingTariffPromotionDtos;
    this.id = id;
  }
}

export class VendorBranchShippingTariffPromotionDto {
  constructor({
    fromPrice = undefined,
    toPrice = undefined,
    amount = undefined,
    amountType = undefined,
    published = undefined,
    vendorBranchShippingTariffDto = undefined,
    id = undefined,
  } = {}) {
    this.fromPrice = fromPrice;
    this.toPrice = toPrice;
    this.amount = amount;
    this.amountType = amountType;
    this.published = published;
    this.vendorBranchShippingTariffDto = vendorBranchShippingTariffDto;
    this.id = id;
  }
}

export class BankDto {
  constructor({
    name = undefined,
    imageDto = undefined,
    published = undefined,
    usedForProfile = undefined,
    id = undefined,
    userDto = undefined,
  } = {}) {
    this.name = name;
    this.imageDto = imageDto;
    this.published = published;
    this.usedForProfile = usedForProfile;
    this.id = id;
    this.userDto = userDto;
  }
}

export class BillDto {
  constructor({
    userDto = undefined,
    code = undefined,
    addressDto = undefined,
    totalAmount = undefined,
    stringTotalAmount = undefined,
    totalDiscount = undefined,
    stringTotalDiscount = undefined,
    totalDiscountCodeAmount = undefined,
    stringTotalDiscountCodeAmount = undefined,
    totalShipmentAmount = undefined,
    stringTotalShipmentAmount = undefined,
    payableAmount = undefined,
    payableBillingAmount = undefined,
    stringPayableBillingAmount = undefined,
    stringPayableAmount = undefined,
    discountCode = undefined,
    billStatus = undefined,
    searchBillItemStatus = undefined, // array of string,
    searchBillStatus = undefined,
    billItemDtos = undefined, // array of ,
    searchFromDateTimeUtc = undefined,
    searchToDateTimeUtc = undefined,
    persianSearchFromDateTimeUtc = undefined,
    persianSearchToDateTimeUtc = undefined,
    fromPanel = undefined,
    id = undefined,
    searchHasDiscountCode = undefined,
    saerchHasPromotion = undefined,
    searchVendorBranchDtos = undefined,
    searchPriceOption = undefined,
    persianSearchDPFromDateTimeUtc = undefined,
    persianSearchDPToDateTimeUtc = undefined,
    published = undefined,
  } = {}) {
    this.userDto = userDto;
    this.code = code;
    this.addressDto = addressDto;
    this.totalAmount = totalAmount;
    this.totalDiscount = totalDiscount;
    this.stringTotalDiscount = stringTotalDiscount;
    this.totalDiscountCodeAmount = totalDiscountCodeAmount;
    this.stringTotalDiscountCodeAmount = stringTotalDiscountCodeAmount;
    this.totalShipmentAmount = totalShipmentAmount;
    this.stringTotalShipmentAmount = stringTotalShipmentAmount;
    this.payableAmount = payableAmount;
    this.payableBillingAmount = payableBillingAmount;
    this.stringPayableBillingAmount = stringPayableBillingAmount;
    this.stringPayableAmount = stringPayableAmount;
    this.discountCode = discountCode;
    this.billStatus = billStatus;
    this.searchBillItemStatus = searchBillItemStatus; // array of string;
    this.searchBillStatus = searchBillStatus;
    this.billItemDtos = billItemDtos; // array of ;
    this.searchFromDateTimeUtc = searchFromDateTimeUtc;
    this.searchToDateTimeUtc = searchToDateTimeUtc;
    this.persianSearchFromDateTimeUtc = persianSearchFromDateTimeUtc;
    this.persianSearchToDateTimeUtc = persianSearchToDateTimeUtc;
    this.fromPanel = fromPanel;
    this.id = id;
    this.searchHasDiscountCode = searchHasDiscountCode;
    this.saerchHasPromotion = saerchHasPromotion;
    this.searchVendorBranchDtos = searchVendorBranchDtos;
    this.stringTotalAmount = stringTotalAmount;
    this.searchPriceOption = searchPriceOption;
    this.persianSearchDPFromDateTimeUtc = persianSearchDPFromDateTimeUtc;
    this.persianSearchDPToDateTimeUtc = persianSearchDPToDateTimeUtc;
    this.published = published;
  }
}

export class BillItemDto {
  constructor({
    count = undefined,
    totalAmount = undefined,
    stringTotalAmount = undefined,
    totalDiscount = undefined,
    stringTotalDiscount = undefined,
    shipmentAmount = undefined,
    stringShipmentAmount = undefined,
    vendorBranchProductDto = undefined,
    vendorBranchDto = undefined,
    priceOption = undefined,
    requestedDate = undefined,
    requestedHour = undefined,
    rule = undefined,
    discountCode = undefined,
    vendorBranchPromotionDetailDto = undefined,
    billItemStatus = undefined,
    billItemStatusId = undefined,
    searchBillItemStatus = undefined,
    billDto = undefined,
    id = undefined,
    updateOccasionText = undefined,
    OccasionText = undefined,
    isReadOccasionText = undefined,
    billItemDriverDto = undefined,
    fromOccasionText = undefined,
    toOccasionText = undefined,
    occasionTypeId = undefined,
  } = {}) {
    this.count = count;
    this.totalAmount = totalAmount;
    this.stringTotalAmount = stringTotalAmount;
    this.totalDiscount = totalDiscount;
    this.stringTotalDiscount = stringTotalDiscount;
    this.shipmentAmount = shipmentAmount;
    this.stringShipmentAmount = stringShipmentAmount;
    this.vendorBranchProductDto = vendorBranchProductDto;
    this.vendorBranchDto = vendorBranchDto;
    this.priceOption = priceOption;
    this.requestedDate = requestedDate;
    this.requestedHour = requestedHour;
    this.rule = rule;
    this.discountCode = discountCode;
    this.vendorBranchPromotionDetailDto = vendorBranchPromotionDetailDto;
    this.billItemStatus = billItemStatus;
    this.billItemStatusId = billItemStatusId;
    this.searchBillItemStatus = searchBillItemStatus;
    this.billDto = billDto;
    this.id = id;
    this.updateOccasionText = updateOccasionText;
    this.OccasionText = OccasionText;
    this.isReadOccasionText = isReadOccasionText;
    this.billItemDriverDto = billItemDriverDto;
    this.fromOccasionText = fromOccasionText;
    this.toOccasionText = toOccasionText;
    this.occasionTypeId = occasionTypeId;
  }
}

export class UserBankHistoryDto {
  constructor({
    name = undefined,
    shabaNo = undefined,
    cardNo = undefined,
    userDto = undefined,
    bankDto = undefined,
    fromPanel = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.shabaNo = shabaNo;
    this.cardNo = cardNo;
    this.userDto = userDto;
    this.bankDto = bankDto;
    this.fromPanel = fromPanel;
    this.id = id;
  }
}

export class SendDiscountCodeGroupDto {
  constructor({ promotionDto = undefined, userId = undefined } = {}) {
    this.promotionDto = promotionDto;
    this.userId = userId;
  }
}

export class WalletPromotionDto {
  constructor({ promotionDto = undefined, amount = undefined } = {}) {
    this.promotionDto = promotionDto;
    this.amount = amount;
  }
}

export class WalletDto {
  constructor({
    userDto = undefined,
    fromPanel = undefined,
    discountCode = undefined,
    payableWalletAmount = undefined,
  } = {}) {
    this.userDto = userDto;
    this.fromPanel = fromPanel;
    this.discountCode = discountCode;
    this.payableWalletAmount = payableWalletAmount;
  }
}
