import { call, put, select } from 'redux-saga/effects';
import {
  COMMISSION_LIST_FAILURE,
  COMMISSION_LIST_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  COMMISSION_PUT_SUCCESS,
  COMMISSION_PUT_FAILURE,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  COMMISSION_LIST_REQUEST,
  COMMISSION_POST_SUCCESS,
  COMMISSION_POST_FAILURE,
  COMMISSION_DELETE_SUCCESS,
  COMMISSION_DELETE_FAILURE,
} from '../../../constants';
import {
  VENDOR_BRANCH_COMMISSION_CREATE_IS_SUCCESS,
  VENDOR_BRANCH_COMMISSION_DELETE_IS_SUCCESS,
  VENDOR_BRANCH_COMMISSION_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* commissionListRequest(
  getDtoQueryString,
  getCommissionApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getCommissionApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: COMMISSION_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: COMMISSION_LIST_FAILURE });
  }
}

export function* commissionPostRequest(postCommissionApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postCommissionApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: COMMISSION_POST_SUCCESS, data: action.dto.listDto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COMMISSION_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* commissionPostSuccess(message, action) {
  yield put({ type: COMMISSION_LIST_REQUEST, dto: action.data });

  message.success(VENDOR_BRANCH_COMMISSION_CREATE_IS_SUCCESS, 5);
}

export function* commissionPutRequest(putCommissionApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putCommissionApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: COMMISSION_PUT_SUCCESS, data: action.dto.listDto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COMMISSION_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* commissionPutSuccess(message, action) {
  yield put({ type: COMMISSION_LIST_REQUEST, dto: action.data });

  message.success(VENDOR_BRANCH_COMMISSION_UPDATE_IS_SUCCESS, 5);
}

export function* commissionDeleteRequest(deleteCommissionApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteCommissionApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: COMMISSION_DELETE_SUCCESS, data: action.dto.listDto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COMMISSION_DELETE_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* commissionDeleteSuccess(message, action) {
  yield put({ type: COMMISSION_LIST_REQUEST, dto: action.data });

  message.success(VENDOR_BRANCH_COMMISSION_DELETE_IS_SUCCESS, 5);
}
