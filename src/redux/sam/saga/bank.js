import { call, put, select } from 'redux-saga/effects';
import {
  BANK_LIST_SUCCESS,
  BANK_LIST_FAILURE,
  COMMISSION_POST_SUCCESS,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  COMMISSION_POST_FAILURE,
  COMMISSION_LIST_REQUEST,
  USER_CARD_NUMBER_POST_REQUEST,
  USER_CARD_NUMBER_POST_FAILURE,
  USER_CARD_NUMBER_LIST_REQUEST,
  USER_CARD_NUMBER_POST_SUCCESS,
  USER_CARD_NUMBER_PUT_SUCCESS,
  USER_CARD_NUMBER_PUT_FAILURE,
  COMMISSION_DELETE_SUCCESS,
  COMMISSION_DELETE_FAILURE, USER_CARD_NUMBER_DELETE_SUCCESS, USER_CARD_NUMBER_DELETE_FAILURE
} from '../../../constants';
import {
  VENDOR_BRANCH_COMMISSION_CREATE_IS_SUCCESS,
  VENDOR_BRANCH_COMMISSION_DELETE_IS_SUCCESS
} from "../../../Resources/Localization";

const getCurrentSession = state => state.login.data;

export function* bankListRequest(getDtoQueryString, getBankApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getBankApi, currentSession.token, container);

  if (response) {
    yield put({ type: BANK_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: BANK_LIST_FAILURE });
  }
}

export function* userCardNumberPostRequest(postUserCardNumberApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postUserCardNumberApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: USER_CARD_NUMBER_POST_SUCCESS, data: action.dto.listDto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_CARD_NUMBER_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* userCardNumberPostSuccess(message, action) {
  yield put({ type: USER_CARD_NUMBER_LIST_REQUEST, dto: action.data });

  message.success('شماره کارت بانکی با موفقیت اضافه شد.', 5);
}


export function* userCardNumberPutRequest(putUserCardNumberApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putUserCardNumberApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: USER_CARD_NUMBER_PUT_SUCCESS, data: action.dto.listDto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_CARD_NUMBER_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* userCardNumberPutSuccess(message, action) {
  yield put({ type: USER_CARD_NUMBER_LIST_REQUEST, dto: action.data });

  message.success('شماره کارت بانکی با موفقیت ویرایش شد.', 5);
}

export function* userCardNumberDeleteRequest(deleteUserCardNumberApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteUserCardNumberApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: USER_CARD_NUMBER_DELETE_SUCCESS, data: action.dto.listDto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_CARD_NUMBER_DELETE_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* userCardNumberDeleteSuccess(message, action) {
  yield put({ type: USER_CARD_NUMBER_LIST_REQUEST, dto: action.data });

  message.success('شماره کارت بانکی با موفقیت حذف شد.', 5);
}
