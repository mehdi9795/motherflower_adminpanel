import { call, put, select } from 'redux-saga/effects';
import {
  VB_SHIPPING_TARIFF_LIST_FAILURE,
  VB_SHIPPING_TARIFF_LIST_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  VB_SHIPPING_TARIFF_PUT_SUCCESS,
  VB_SHIPPING_TARIFF_PUT_FAILURE,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  VB_SHIPPING_TARIFF_LIST_REQUEST,
  VB_SHIPPING_TARIFF_POST_SUCCESS,
  VB_SHIPPING_TARIFF_POST_FAILURE,
  VB_SHIPPING_TARIFF_DELETE_SUCCESS,
  VB_SHIPPING_TARIFF_DELETE_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_POST_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_POST_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_FAILURE,
} from '../../../constants';
import {
  VENDOR_BRANCH_SHIPPING_TARIFF_CREATE_IS_SUCCESS,
  VENDOR_BRANCH_SHIPPING_TARIFF_DELETE_IS_SUCCESS,
  VENDOR_BRANCH_SHIPPING_TARIFF_PROMOTION_CREATE_IS_SUCCESS,
  VENDOR_BRANCH_SHIPPING_TARIFF_PROMOTION_DELETE_IS_SUCCESS,
  VENDOR_BRANCH_SHIPPING_TARIFF_PROMOTION_UPDATE_IS_SUCCESS,
  VENDOR_BRANCH_SHIPPING_TARIFF_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';
import history from '../../../history';

const getCurrentSession = state => state.login.data;

export function* vbShippingTariffListRequest(
  getDtoQueryString,
  getVbShippingTariffApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVbShippingTariffApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: VB_SHIPPING_TARIFF_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VB_SHIPPING_TARIFF_LIST_FAILURE });
  }
}

export function* vbShippingTariffPostRequest(postVbShippingTariffApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postVbShippingTariffApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto };
    }
    yield put({
      type: VB_SHIPPING_TARIFF_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function vbShippingTariffPostSuccess(message, action) {
  message.success(VENDOR_BRANCH_SHIPPING_TARIFF_CREATE_IS_SUCCESS, 5);
  if (action.data.data.status === 'saveAndContinue') {
    history.push(`/shippingTariff/edit/${action.data.id}`);
  } else {
    history.push('/shippingTariff/list');
  }
}

export function* vbShippingTariffPutRequest(putVbShippingTariffApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putVbShippingTariffApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function vbShippingTariffPutSuccess(message, action) {
  if (action.data.status === 'save') {
    history.push('/shippingTariff/list');
  }

  message.success(VENDOR_BRANCH_SHIPPING_TARIFF_UPDATE_IS_SUCCESS, 5);
}

export function* vbShippingTariffDeleteRequest(
  deleteVbShippingTariffApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteVbShippingTariffApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_DELETE_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_DELETE_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vbShippingTariffDeleteSuccess(message, action) {
  yield put({ type: VB_SHIPPING_TARIFF_LIST_REQUEST, dto: action.data });

  message.success(VENDOR_BRANCH_SHIPPING_TARIFF_DELETE_IS_SUCCESS, 5);
}

export function* vbShippingTariffPromotionListRequest(
  getDtoQueryString,
  getVbShippingTariffPromotionApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVbShippingTariffPromotionApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({
      type: VB_SHIPPING_TARIFF_PROMOTION_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: VB_SHIPPING_TARIFF_PROMOTION_LIST_FAILURE });
  }
}

export function* vbShippingTariffPromotionPostRequest(
  postVbShippingTariffPromotionApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postVbShippingTariffPromotionApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });

    yield put({
      type: VB_SHIPPING_TARIFF_PROMOTION_POST_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_PROMOTION_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vbShippingTariffPromotionPostSuccess(message, action) {
  yield put({
    type: VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
    dto: action.data,
  });
  message.success(VENDOR_BRANCH_SHIPPING_TARIFF_PROMOTION_CREATE_IS_SUCCESS, 5);
}

export function* vbShippingTariffPromotionPutRequest(
  putVbShippingTariffPromotionApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putVbShippingTariffPromotionApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_PROMOTION_PUT_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_PROMOTION_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vbShippingTariffPromotionPutSuccess(message, action) {
  yield put({
    type: VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
    dto: action.data,
  });

  message.success(VENDOR_BRANCH_SHIPPING_TARIFF_PROMOTION_UPDATE_IS_SUCCESS, 5);
}

export function* vbShippingTariffPromotionDeleteRequest(
  deleteVbShippingTariffPromotionApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteVbShippingTariffPromotionApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_PROMOTION_DELETE_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VB_SHIPPING_TARIFF_PROMOTION_DELETE_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vbShippingTariffPromotionDeleteSuccess(message, action) {
  yield put({
    type: VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
    dto: action.data,
  });

  message.success(VENDOR_BRANCH_SHIPPING_TARIFF_PROMOTION_DELETE_IS_SUCCESS, 5);
}
