import { call, put, select } from 'redux-saga/effects';
import {
  VENDOR_BRANCH__PRODUCT_PRICE_PUT_FAILURE,
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_FAILURE,
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_REQUEST,
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_SUCCESS,
  VENDOR_BRANCH_PRODUCT_PRICE_PUT_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
} from '../../../constants';
import { VENDOR_BRANCH_PRODUCT_PRICE_UPDATE_IS_SUCCESS } from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* vendorBranchProductPriceListRequest(
  getDtoQueryString,
  getVendorBranchProductPriceApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVendorBranchProductPriceApi,
    currentSession.token,
    container,
  );

  if (response.status === 200) {
    yield put({
      type: VENDOR_BRANCH_PRODUCT_PRICE_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: VENDOR_BRANCH_PRODUCT_PRICE_LIST_FAILURE });
  }
}

export function* vendorBranchProductPricePutRequest(
  putVendorBranchProductPriceApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putVendorBranchProductPriceApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PRODUCT_PRICE_PUT_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH__PRODUCT_PRICE_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vendorBranchProductPricePutSuccess(message, action) {
  yield put({
    type: VENDOR_BRANCH_PRODUCT_PRICE_LIST_REQUEST,
    dto: action.data,
  });

  message.success(VENDOR_BRANCH_PRODUCT_PRICE_UPDATE_IS_SUCCESS, 5);
}
