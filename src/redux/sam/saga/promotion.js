import { call, put, select } from 'redux-saga/effects';
import {
  CATEGORY_PROMOTION_DETAILS_LIST_FAILURE,
  CATEGORY_PROMOTION_DETAILS_LIST_SUCCESS,
  PRODUCT_PROMOTION_DETAILS_LIST_FAILURE,
  PRODUCT_PROMOTION_DETAILS_LIST_SUCCESS,
  PROMOTION_DISCOUNT_CODE_LIST_FAILURE,
  PROMOTION_DISCOUNT_CODE_LIST_SUCCESS,
  PROMOTION_LIST_FAILURE,
  PROMOTION_LIST_SUCCESS,
  PROMOTION_TYPE_LIST_FAILURE,
  PROMOTION_TYPE_LIST_SUCCESS,
  SINGLE_PROMOTION_INIT_FAILURE,
  SINGLE_PROMOTION_INIT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_LIST_FAILURE,
  VENDOR_BRANCH_PROMOTION_LIST_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  PROMOTION_POST_SUCCESS,
  PROMOTION_POST_FAILURE,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  PROMOTION_LIST_REQUEST,
  PROMOTION_PUT_SUCCESS,
  PROMOTION_PUT_FAILURE,
  PROMOTION_DELETE_FAILURE,
  PROMOTION_DELETE_SUCCESS,
  PROMOTION_GROUP_ACTION_POST_SUCCESS,
  PROMOTION_GROUP_ACTION_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
  VENDOR_BRANCH_PROMOTION_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_PUT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_PUT_FAILURE,
  VENDOR_BRANCH_PROMOTION_DELETE_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_FAILURE,
  PROMOTION_DISCOUNT_CODE_POST_SUCCESS,
  PROMOTION_DISCOUNT_CODE_POST_FAILURE,
  PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
  PROMOTION_DISCOUNT_CODE_PUT_SUCCESS,
  PROMOTION_DISCOUNT_CODE_PUT_FAILURE,
  PROMOTION_DISCOUNT_CODE_DELETE_SUCCESS,
  PROMOTION_DISCOUNT_CODE_DELETE_FAILURE,
  VENDOR_BRANCH_PROMOTION_DELETE_SUCCESS, WALLET_PROMOTION_PUT_SUCCESS, WALLET_PROMOTION_PUT_FAILURE,
} from '../../../constants';
import {
  PROMOTION_CREATE_IS_SUCCESS,
  PROMOTION_GROUP_ACTION_SUCCESS,
  PROMOTION_UPDATE_IS_SUCCESS,
  VENDOR_BRANCH_PROMOTION_CREATE_SUCCESS,
  VENDOR_BRANCH_PROMOTION_UPDATE_SUCCESS,
  DISCOUNT_CODE_CREATE_IS_SUCCESS,
  DISCOUNT_CODE_UPDATE_IS_SUCCESS,
  DISCOUNT_CODE_DELETE_IS_SUCCESS,
  PROMOTION_DELETE_IS_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DELETE_IS_SUCCESS, DISCOUNT_CODE_WALLET_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* promotionListRequest(
  getDtoQueryString,
  getPromotionApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getPromotionApi, currentSession.token, container);

  if (response) {
    yield put({ type: PROMOTION_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PROMOTION_LIST_FAILURE });
  }
}

export function* promotionTypeListRequest(
  getDtoQueryString,
  getPromotionTypeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getPromotionTypeApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: PROMOTION_TYPE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PROMOTION_TYPE_LIST_FAILURE });
  }
}

export function* singlePromotionInitRequest(
  getDtoQueryString,
  getSinglePromotionApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getSinglePromotionApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: SINGLE_PROMOTION_INIT_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_PROMOTION_INIT_FAILURE });
  }
}

export function* promotionPostRequest(postPromotionApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postPromotionApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto };
    }
    yield put({
      type: PROMOTION_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* promotionPostSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue') {
    yield put({
      type: SINGLE_PROMOTION_INIT_SUCCESS,
      data: action.data.data.singlePromotionDto,
    });
    message.success(PROMOTION_CREATE_IS_SUCCESS, 5);
    history.push(`/promotion/edit/${action.data.id}`);
  } else {
    message.success(PROMOTION_CREATE_IS_SUCCESS, 5);
    history.push('/promotion/list');
  }
}

export function* promotionPutRequest(putPromotionApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putPromotionApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function promotionPutSuccess(history, message, action) {
  if (action.data.status === 'saveAndContinue') {
    message.success(PROMOTION_UPDATE_IS_SUCCESS, 5);
    // history.push(`/promotion/edit/${action.data.data.dto.id}`);
  } else {
    message.success(PROMOTION_UPDATE_IS_SUCCESS, 5);
    history.push('/promotion/list');
  }
}

export function* promotionDeleteRequest(deletePromotionApi, action) {
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deletePromotionApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: PROMOTION_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: PROMOTION_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* promotionDeleteSuccess(history, message, action) {
  message.success(PROMOTION_DELETE_IS_SUCCESS, 5);
  if (action.data.status === 'edit') {
    history.push('/promotion/list');
  } else {
    yield put({
      type: PROMOTION_LIST_REQUEST,
      dto: action.data.listDto,
    });
  }
}

export function* promotionGroupActionPostRequest(
  postPromotionGroupActionApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postPromotionGroupActionApi,
    currentSession.token,
    action.data.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_GROUP_ACTION_POST_SUCCESS,
      data: action.data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_GROUP_ACTION_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* promotionGroupActionPostSuccess(message, action) {
  yield put({
    type: VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
    data: action.data.listDto,
  });
  message.success(PROMOTION_GROUP_ACTION_SUCCESS, 5);
}

export function* vendorBranchPromotionListRequest(
  getDtoQueryString,
  getPromotionApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getPromotionApi, currentSession.token, container);

  if (response) {
    yield put({
      type: VENDOR_BRANCH_PROMOTION_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: VENDOR_BRANCH_PROMOTION_LIST_FAILURE });
  }
}

export function* vendorBranchPromotionPostRequest(
  postPromotionVendorBranchApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postPromotionVendorBranchApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_POST_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vendorBranchPromotionPostSuccess(message, action) {
  yield put({
    type: VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
    dto: action.data,
  });
  message.success(VENDOR_BRANCH_PROMOTION_CREATE_SUCCESS, 5);
}

export function* vendorBranchPromotionPutRequest(
  putPromotionVendorBranchApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putPromotionVendorBranchApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vendorBranchPromotionPutSuccess(message, action) {
  if (action.data.listDto)
    yield put({
      type: VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
      dto: action.data.listDto,
    });
  message.success(VENDOR_BRANCH_PROMOTION_UPDATE_SUCCESS, 5);
}

export function* vendorBranchPromotionDeleteRequest(
  putPromotionVendorBranchApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putPromotionVendorBranchApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchPromotionDeleteSuccess(message, action) {
  yield put({
    type: VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
    dto: action.data.listDto,
  });

  message.success(VENDOR_BRANCH_PROMOTION_DELETE_IS_SUCCESS, 5);
}

export function* categoryPromotionDetailsListRequest(
  getDtoQueryString,
  getCategoryPromotionDetailsApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getCategoryPromotionDetailsApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({
      type: CATEGORY_PROMOTION_DETAILS_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: CATEGORY_PROMOTION_DETAILS_LIST_FAILURE });
  }
}

export function* productPromotionDetailsListRequest(
  getDtoQueryString,
  getProductPromotionDetailsApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getProductPromotionDetailsApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({
      type: PRODUCT_PROMOTION_DETAILS_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: PRODUCT_PROMOTION_DETAILS_LIST_FAILURE });
  }
}

export function* vendorBranchPromotionDetailsPostRequest(
  postVendorBranchPromotionDetailsApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postVendorBranchPromotionDetailsApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_DETAILS_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_DETAILS_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchPromotionDetailsPostSuccess(message, action) {
  yield put({
    type: action.data.type,
    dto: action.data.listDto,
  });
  message.success(VENDOR_BRANCH_PROMOTION_CREATE_SUCCESS, 5);
}

export function* vendorBranchPromotionDetailsPutRequest(
  putVendorBranchPromotionDetailsApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putVendorBranchPromotionDetailsApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_DETAILS_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_DETAILS_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchPromotionDetailsPutSuccess(message, action) {
  yield put({
    type: action.data.type,
    dto: action.data.listDto,
  });
  message.success(VENDOR_BRANCH_PROMOTION_UPDATE_SUCCESS, 5);
}

export function* vendorBranchPromotionDetailsDeleteRequest(
  deleteVendorBranchPromotionDetailsApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteVendorBranchPromotionDetailsApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchPromotionDetailsDeleteSuccess(message, action) {
  yield put({
    type: action.data.type,
    dto: action.data.listDto,
  });

  message.success(VENDOR_BRANCH_PROMOTION_DELETE_IS_SUCCESS, 5);
}

export function* promotionDiscountCodeListRequest(
  getDtoQueryString,
  getPromotionDiscountCodeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getPromotionDiscountCodeApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({
      type: PROMOTION_DISCOUNT_CODE_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: PROMOTION_DISCOUNT_CODE_LIST_FAILURE });
  }
}

export function* promotionDiscountCodePostRequest(
  postPromotionDiscountCodeApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postPromotionDiscountCodeApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_DISCOUNT_CODE_POST_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_DISCOUNT_CODE_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* promotionDiscountCodePostSuccess(history, message, action) {
  yield put({
    type: PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
    dto: action.data,
  });
  message.success(DISCOUNT_CODE_CREATE_IS_SUCCESS, 5);
}

export function* promotionDiscountCodePutRequest(
  putPromotionDiscountCodeApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putPromotionDiscountCodeApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_DISCOUNT_CODE_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_DISCOUNT_CODE_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* promotionDiscountCodePutSuccess(history, message, action) {
  yield put({
    type: PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
    dto: action.data.listDto,
  });
  message.success(DISCOUNT_CODE_UPDATE_IS_SUCCESS, 5);
}

export function* promotionDiscountCodeDeleteRequest(
  deletePromotionDiscountCodeApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deletePromotionDiscountCodeApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_DISCOUNT_CODE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PROMOTION_DISCOUNT_CODE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* promotionDiscountCodeDeleteSuccess(history, message, action) {
  message.success(DISCOUNT_CODE_DELETE_IS_SUCCESS, 5);
  yield put({
    type: PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* walletPromotionPutRequest(
  putWalletPromotionApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putWalletPromotionApi,
    currentSession.token,
    action.dto,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: WALLET_PROMOTION_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: WALLET_PROMOTION_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function walletPromotionPutSuccess(message) {
  message.success(DISCOUNT_CODE_WALLET_UPDATE_IS_SUCCESS, 5);
}
