import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import { getDtoQueryString } from '../../../utils/helper';
import {
  BankApi,
  deleteCommissionApi,
  deletePromotionApi,
  deletePromotionDiscountCodeApi,
  DeleteUserBankHistoriesApi,
  deleteVendorBranchPromotionApi,
  deleteVendorBranchPromotionDetailsApi,
  deleteVendorBranchShippingTariffApi,
  deleteVendorBranchShippingTariffPromotionApi,
  getBillApi,
  getBillItemApi,
  getCategoryPromotionDetailsApi,
  getCommissionApi,
  getProductPromotionDetailsApi,
  getPromotionsApi,
  getPromotionsDiscountCodeApi,
  getPromotionTypeApi,
  getVendorBranchProductPriceApi,
  getVendorBranchPromotionsApi,
  getVendorBranchShippingTariffApi,
  getVendorBranchShippingTariffPromotionApi,
  postCommissionApi,
  postPromotionApi,
  postPromotionDiscountCodeApi,
  postPromotionGroupActionApi,
  PostUserBankHistoriesApi,
  postVendorBranchPromotionApi,
  postVendorBranchPromotionDetailsApi,
  postVendorBranchShippingTariffApi,
  postVendorBranchShippingTariffPromotionApi,
  putBillApi,
  putBillItemApi,
  putCommissionApi,
  putPromotionApi,
  putPromotionDiscountCodeApi,
  PutUserBankHistoriesApi,
  putVendorBranchProductPriceApi,
  putVendorBranchPromotionApi,
  putVendorBranchPromotionDetailsApi,
  putVendorBranchShippingTariffApi,
  putVendorBranchShippingTariffPromotionApi,
  putWalletPromotionApi,
  PostWalletDepositHistoryApi,
  GetWalletsApi,
} from '../../../services/samApi';
import {
  commissionDeleteRequest,
  commissionDeleteSuccess,
  commissionListRequest,
  commissionPostRequest,
  commissionPostSuccess,
  commissionPutRequest,
  commissionPutSuccess,
} from './commission';
import {
  COMMISSION_LIST_REQUEST,
  COMMISSION_PUT_REQUEST,
  COMMISSION_PUT_SUCCESS,
  PROMOTION_LIST_REQUEST,
  PROMOTION_TYPE_LIST_REQUEST,
  // SINGLE_PROMOTION_INIT_REQUEST,
  PROMOTION_DELETE_REQUEST,
  PROMOTION_DELETE_SUCCESS,
  PROMOTION_POST_REQUEST,
  PROMOTION_POST_SUCCESS,
  PROMOTION_PUT_REQUEST,
  PROMOTION_PUT_SUCCESS,
  PROMOTION_GROUP_ACTION_POST_REQUEST,
  PROMOTION_GROUP_ACTION_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
  VENDOR_BRANCH_PROMOTION_PUT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_PUT_REQUEST,
  VENDOR_BRANCH_PROMOTION_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_POST_REQUEST,
  VENDOR_BRANCH_PROMOTION_DELETE_REQUEST,
  VENDOR_BRANCH_PROMOTION_DELETE_SUCCESS,
  CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
  PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_SUCCESS,
  PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
  PROMOTION_DISCOUNT_CODE_POST_REQUEST,
  PROMOTION_DISCOUNT_CODE_POST_SUCCESS,
  PROMOTION_DISCOUNT_CODE_DELETE_REQUEST,
  PROMOTION_DISCOUNT_CODE_DELETE_SUCCESS,
  PROMOTION_DISCOUNT_CODE_PUT_REQUEST,
  PROMOTION_DISCOUNT_CODE_PUT_SUCCESS,
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_REQUEST,
  VENDOR_BRANCH_PRODUCT_PRICE_PUT_SUCCESS,
  VENDOR_BRANCH_PRODUCT_PRICE_PUT_REQUEST,
  COMMISSION_POST_SUCCESS,
  COMMISSION_POST_REQUEST,
  COMMISSION_DELETE_SUCCESS,
  COMMISSION_DELETE_REQUEST,
  VB_SHIPPING_TARIFF_LIST_REQUEST,
  VB_SHIPPING_TARIFF_POST_REQUEST,
  VB_SHIPPING_TARIFF_PUT_REQUEST,
  VB_SHIPPING_TARIFF_DELETE_REQUEST,
  VB_SHIPPING_TARIFF_POST_SUCCESS,
  VB_SHIPPING_TARIFF_PUT_SUCCESS,
  VB_SHIPPING_TARIFF_DELETE_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_POST_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_POST_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_SUCCESS,
  ORDER_LIST_REQUEST,
  ORDER_PUT_REQUEST,
  ORDER_PUT_SUCCESS,
  SINGLE_ORDER_REQUEST,
  ORDER_ITEM_LIST_REQUEST,
  WALLET_PROMOTION_PUT_REQUEST,
  WALLET_PROMOTION_PUT_SUCCESS,
  ORDER_ITEM_PUT_SUCCESS,
  ORDER_ITEM_PUT_REQUEST,
  BANK_LIST_SUCCESS,
  BANK_LIST_REQUEST,
  USER_CARD_NUMBER_POST_REQUEST,
  USER_CARD_NUMBER_POST_SUCCESS,
  USER_CARD_NUMBER_PUT_REQUEST,
  USER_CARD_NUMBER_PUT_SUCCESS,
  USER_CARD_NUMBER_DELETE_REQUEST,
  USER_CARD_NUMBER_DELETE_SUCCESS,
} from '../../../constants';
import {
  categoryPromotionDetailsListRequest,
  productPromotionDetailsListRequest,
  promotionDeleteRequest,
  promotionDeleteSuccess,
  promotionDiscountCodeDeleteRequest,
  promotionDiscountCodeDeleteSuccess,
  promotionDiscountCodeListRequest,
  promotionDiscountCodePostRequest,
  promotionDiscountCodePostSuccess,
  promotionDiscountCodePutRequest,
  promotionDiscountCodePutSuccess,
  promotionGroupActionPostRequest,
  promotionGroupActionPostSuccess,
  promotionListRequest,
  promotionPostRequest,
  promotionPostSuccess,
  promotionPutRequest,
  promotionPutSuccess,
  promotionTypeListRequest,
  vendorBranchPromotionDeleteRequest,
  vendorBranchPromotionDeleteSuccess,
  vendorBranchPromotionDetailsDeleteRequest,
  vendorBranchPromotionDetailsDeleteSuccess,
  vendorBranchPromotionDetailsPostRequest,
  vendorBranchPromotionDetailsPostSuccess,
  vendorBranchPromotionDetailsPutRequest,
  vendorBranchPromotionDetailsPutSuccess,
  vendorBranchPromotionListRequest,
  vendorBranchPromotionPostRequest,
  vendorBranchPromotionPostSuccess,
  vendorBranchPromotionPutRequest,
  vendorBranchPromotionPutSuccess,
  walletPromotionPutRequest,
  walletPromotionPutSuccess,
} from './promotion';
import history from '../../../history';
import {
  vendorBranchProductPriceListRequest,
  vendorBranchProductPricePutRequest,
  vendorBranchProductPricePutSuccess,
} from './vendorBranchProductPrice';
import {
  vbShippingTariffDeleteRequest,
  vbShippingTariffDeleteSuccess,
  vbShippingTariffListRequest,
  vbShippingTariffPostRequest,
  vbShippingTariffPostSuccess,
  vbShippingTariffPromotionDeleteRequest,
  vbShippingTariffPromotionDeleteSuccess,
  vbShippingTariffPromotionListRequest,
  vbShippingTariffPromotionPostRequest,
  vbShippingTariffPromotionPostSuccess,
  vbShippingTariffPromotionPutRequest,
  vbShippingTariffPromotionPutSuccess,
  vbShippingTariffPutRequest,
  vbShippingTariffPutSuccess,
} from './VendorBranchShippingTariff';
import {
  orderItemListRequest,
  orderItemPutRequest,
  orderItemPutSuccess,
  orderListRequest,
  orderPutRequest,
  orderPutSuccess,
  singleOrderRequest,
} from './order';
import {
  bankListRequest,
  userCardNumberDeleteRequest,
  userCardNumberDeleteSuccess,
  userCardNumberPostRequest,
  userCardNumberPostSuccess,
  userCardNumberPutRequest,
  userCardNumberPutSuccess,
} from './bank';
import {
  walletPostSuccess,
  walletPostRequest,
  walletGetRequest,
} from './wallet';

// main saga generators
export default function* sagaSamIndex() {
  // yield takeLatest(testRequestFlow);
  // yield all(takeLatest(testRequestFlow));
  yield all([
    // <editor-fold dsc="commission">
    takeLatest(
      COMMISSION_LIST_REQUEST,
      commissionListRequest,
      getDtoQueryString,
      getCommissionApi,
    ),
    takeLatest(
      COMMISSION_POST_REQUEST,
      commissionPostRequest,
      postCommissionApi,
    ),
    takeLatest(COMMISSION_POST_SUCCESS, commissionPostSuccess, message),
    takeLatest(COMMISSION_PUT_REQUEST, commissionPutRequest, putCommissionApi),
    takeLatest(COMMISSION_PUT_SUCCESS, commissionPutSuccess, message),
    takeLatest(
      COMMISSION_DELETE_REQUEST,
      commissionDeleteRequest,
      deleteCommissionApi,
    ),
    takeLatest(COMMISSION_DELETE_SUCCESS, commissionDeleteSuccess, message),
    // </editor-fold>

    // <editor-fold dsc="promotion">
    takeLatest(
      PROMOTION_LIST_REQUEST,
      promotionListRequest,
      getDtoQueryString,
      getPromotionsApi,
    ),
    takeLatest(
      PROMOTION_TYPE_LIST_REQUEST,
      promotionTypeListRequest,
      getDtoQueryString,
      getPromotionTypeApi,
    ),
    // takeLatest(
    //   SINGLE_PROMOTION_INIT_REQUEST,
    //   singlePromotionInitRequest,
    //   getDtoQueryString,
    //   getPromotionsApi,
    // ),
    takeLatest(PROMOTION_POST_REQUEST, promotionPostRequest, postPromotionApi),
    takeLatest(PROMOTION_POST_SUCCESS, promotionPostSuccess, history, message),
    takeLatest(PROMOTION_PUT_REQUEST, promotionPutRequest, putPromotionApi),
    takeLatest(PROMOTION_PUT_SUCCESS, promotionPutSuccess, history, message),
    takeLatest(
      PROMOTION_DELETE_REQUEST,
      promotionDeleteRequest,
      deletePromotionApi,
    ),
    takeLatest(
      PROMOTION_DELETE_SUCCESS,
      promotionDeleteSuccess,
      history,
      message,
    ),

    takeLatest(
      PROMOTION_GROUP_ACTION_POST_REQUEST,
      promotionGroupActionPostRequest,
      postPromotionGroupActionApi,
    ),
    takeLatest(
      PROMOTION_GROUP_ACTION_POST_SUCCESS,
      promotionGroupActionPostSuccess,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
      vendorBranchPromotionListRequest,
      getDtoQueryString,
      getVendorBranchPromotionsApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_POST_REQUEST,
      vendorBranchPromotionPostRequest,
      postVendorBranchPromotionApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_POST_SUCCESS,
      vendorBranchPromotionPostSuccess,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_PUT_REQUEST,
      vendorBranchPromotionPutRequest,
      putVendorBranchPromotionApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_PUT_SUCCESS,
      vendorBranchPromotionPutSuccess,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_DELETE_REQUEST,
      vendorBranchPromotionDeleteRequest,
      deleteVendorBranchPromotionApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_DELETE_SUCCESS,
      vendorBranchPromotionDeleteSuccess,
      message,
    ),
    takeLatest(
      CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
      categoryPromotionDetailsListRequest,
      getDtoQueryString,
      getCategoryPromotionDetailsApi,
    ),
    takeLatest(
      PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
      productPromotionDetailsListRequest,
      getDtoQueryString,
      getProductPromotionDetailsApi,
    ),

    takeLatest(
      VENDOR_BRANCH_PROMOTION_DETAILS_POST_REQUEST,
      vendorBranchPromotionDetailsPostRequest,
      postVendorBranchPromotionDetailsApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_DETAILS_POST_SUCCESS,
      vendorBranchPromotionDetailsPostSuccess,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_DETAILS_PUT_REQUEST,
      vendorBranchPromotionDetailsPutRequest,
      putVendorBranchPromotionDetailsApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_DETAILS_PUT_SUCCESS,
      vendorBranchPromotionDetailsPutSuccess,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_REQUEST,
      vendorBranchPromotionDetailsDeleteRequest,
      deleteVendorBranchPromotionDetailsApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_SUCCESS,
      vendorBranchPromotionDetailsDeleteSuccess,
      message,
    ),

    takeLatest(
      PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
      promotionDiscountCodeListRequest,
      getDtoQueryString,
      getPromotionsDiscountCodeApi,
    ),
    takeLatest(
      PROMOTION_DISCOUNT_CODE_POST_REQUEST,
      promotionDiscountCodePostRequest,
      postPromotionDiscountCodeApi,
    ),
    takeLatest(
      PROMOTION_DISCOUNT_CODE_POST_SUCCESS,
      promotionDiscountCodePostSuccess,
      history,
      message,
    ),
    takeLatest(
      PROMOTION_DISCOUNT_CODE_PUT_REQUEST,
      promotionDiscountCodePutRequest,
      putPromotionDiscountCodeApi,
    ),
    takeLatest(
      PROMOTION_DISCOUNT_CODE_PUT_SUCCESS,
      promotionDiscountCodePutSuccess,
      history,
      message,
    ),
    takeLatest(
      PROMOTION_DISCOUNT_CODE_DELETE_REQUEST,
      promotionDiscountCodeDeleteRequest,
      deletePromotionDiscountCodeApi,
    ),
    takeLatest(
      PROMOTION_DISCOUNT_CODE_DELETE_SUCCESS,
      promotionDiscountCodeDeleteSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="vendor branch product price">
    takeLatest(
      VENDOR_BRANCH_PRODUCT_PRICE_LIST_REQUEST,
      vendorBranchProductPriceListRequest,
      getDtoQueryString,
      getVendorBranchProductPriceApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PRODUCT_PRICE_PUT_REQUEST,
      vendorBranchProductPricePutRequest,
      putVendorBranchProductPriceApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PRODUCT_PRICE_PUT_SUCCESS,
      vendorBranchProductPricePutSuccess,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="vendor branch shipping tariff">
    takeLatest(
      VB_SHIPPING_TARIFF_LIST_REQUEST,
      vbShippingTariffListRequest,
      getDtoQueryString,
      getVendorBranchShippingTariffApi,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_POST_REQUEST,
      vbShippingTariffPostRequest,
      postVendorBranchShippingTariffApi,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_POST_SUCCESS,
      vbShippingTariffPostSuccess,
      message,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_PUT_REQUEST,
      vbShippingTariffPutRequest,
      putVendorBranchShippingTariffApi,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_PUT_SUCCESS,
      vbShippingTariffPutSuccess,
      message,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_DELETE_REQUEST,
      vbShippingTariffDeleteRequest,
      deleteVendorBranchShippingTariffApi,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_DELETE_SUCCESS,
      vbShippingTariffDeleteSuccess,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="vendor branch shipping tariff Promotion">
    takeLatest(
      VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
      vbShippingTariffPromotionListRequest,
      getDtoQueryString,
      getVendorBranchShippingTariffPromotionApi,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_PROMOTION_POST_REQUEST,
      vbShippingTariffPromotionPostRequest,
      postVendorBranchShippingTariffPromotionApi,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_PROMOTION_POST_SUCCESS,
      vbShippingTariffPromotionPostSuccess,
      message,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_PROMOTION_PUT_REQUEST,
      vbShippingTariffPromotionPutRequest,
      putVendorBranchShippingTariffPromotionApi,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_PROMOTION_PUT_SUCCESS,
      vbShippingTariffPromotionPutSuccess,
      message,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_PROMOTION_DELETE_REQUEST,
      vbShippingTariffPromotionDeleteRequest,
      deleteVendorBranchShippingTariffPromotionApi,
    ),
    takeLatest(
      VB_SHIPPING_TARIFF_PROMOTION_DELETE_SUCCESS,
      vbShippingTariffPromotionDeleteSuccess,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="order">
    takeLatest(
      ORDER_LIST_REQUEST,
      orderListRequest,
      getDtoQueryString,
      getBillApi,
    ),
    takeLatest(
      SINGLE_ORDER_REQUEST,
      singleOrderRequest,
      getDtoQueryString,
      getBillApi,
    ),
    takeLatest(
      ORDER_ITEM_LIST_REQUEST,
      orderItemListRequest,
      getDtoQueryString,
      getBillItemApi,
    ),
    takeLatest(ORDER_PUT_REQUEST, orderPutRequest, putBillApi),
    takeLatest(ORDER_PUT_SUCCESS, orderPutSuccess, message),
    takeLatest(ORDER_ITEM_PUT_REQUEST, orderItemPutRequest, putBillItemApi),
    takeLatest(ORDER_ITEM_PUT_SUCCESS, orderItemPutSuccess, message),
    // </editor-fold>

    // <editor-fold dsc="wallet">
    takeLatest(
      WALLET_PROMOTION_PUT_REQUEST,
      walletPromotionPutRequest,
      putWalletPromotionApi,
    ),
    takeLatest(
      WALLET_PROMOTION_PUT_SUCCESS,
      walletPromotionPutSuccess,
      message,
    ),
    takeLatest(
      'WALLET_POST_REQUEST',
      walletPostRequest,
      PostWalletDepositHistoryApi,
    ),
    takeLatest('WALLET_POST_SUCCESS', walletPostSuccess, message),
    takeLatest('WALLET_LIST_REQUEST', walletGetRequest, GetWalletsApi),
    // </editor-fold>

    // <editor-fold dsc="bank">
    takeLatest(BANK_LIST_REQUEST, bankListRequest, getDtoQueryString, BankApi),
    takeLatest(
      USER_CARD_NUMBER_POST_REQUEST,
      userCardNumberPostRequest,
      PostUserBankHistoriesApi,
    ),
    takeLatest(
      USER_CARD_NUMBER_POST_SUCCESS,
      userCardNumberPostSuccess,
      message,
    ),
    takeLatest(
      USER_CARD_NUMBER_PUT_REQUEST,
      userCardNumberPutRequest,
      PutUserBankHistoriesApi,
    ),
    takeLatest(USER_CARD_NUMBER_PUT_SUCCESS, userCardNumberPutSuccess, message),
    takeLatest(
      USER_CARD_NUMBER_DELETE_REQUEST,
      userCardNumberDeleteRequest,
      DeleteUserBankHistoriesApi,
    ),
    takeLatest(
      USER_CARD_NUMBER_DELETE_SUCCESS,
      userCardNumberDeleteSuccess,
      message,
    ),
    // </editor-fold>
  ]);
}
