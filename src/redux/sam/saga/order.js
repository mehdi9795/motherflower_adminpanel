import { call, put, select } from 'redux-saga/effects';

import {
  ORDER_UPDATE_IS_SUCCESS,
  VENDOR_BRANCH_PRODUCT_PRICE_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';
import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  ORDER_ITEM_LIST_FAILURE,
  ORDER_ITEM_LIST_SUCCESS, ORDER_ITEM_PUT_FAILURE, ORDER_ITEM_PUT_SUCCESS,
  ORDER_LIST_FAILURE,
  ORDER_LIST_REQUEST,
  ORDER_LIST_SUCCESS,
  ORDER_PUT_FAILURE,
  ORDER_PUT_SUCCESS,
  SINGLE_ORDER_FAILURE,
  SINGLE_ORDER_SUCCESS,
} from '../../../constants';

const getCurrentSession = state => state.login.data;

export function* orderListRequest(getDtoQueryString, getOrderApi, action) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getOrderApi, currentSession.token, container);

  if (response.status === 200) {
    yield put({
      type: ORDER_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: ORDER_LIST_FAILURE });
  }
}

export function* singleOrderRequest(getDtoQueryString, getOrderApi, action) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getOrderApi, currentSession.token, container);

  if (response.status === 200) {
    yield put({
      type: SINGLE_ORDER_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: SINGLE_ORDER_FAILURE });
  }
}

export function* orderItemListRequest(
  getDtoQueryString,
  getOrderItemApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getOrderItemApi, currentSession.token, container);

  if (response.status === 200) {
    yield put({
      type: ORDER_ITEM_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: ORDER_ITEM_LIST_FAILURE });
  }
}

export function* orderPutRequest(putOrderApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putOrderApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ORDER_PUT_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ORDER_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* orderPutSuccess(message, action) {
  yield put({
    type: ORDER_LIST_REQUEST,
    dto: action.data,
  });

  message.success(ORDER_UPDATE_IS_SUCCESS, 5);
}


export function* orderItemPutRequest(putItemOrderApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putItemOrderApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ORDER_ITEM_PUT_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ORDER_ITEM_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* orderItemPutSuccess(message, action) {
  yield put({
    type: ORDER_LIST_REQUEST,
    dto: action.data,
  });

  message.success(ORDER_UPDATE_IS_SUCCESS, 5);
}
