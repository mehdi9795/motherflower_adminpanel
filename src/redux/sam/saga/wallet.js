import { call, put, select } from 'redux-saga/effects';
import {
  BANK_LIST_SUCCESS,
  BANK_LIST_FAILURE,
  COMMISSION_POST_SUCCESS,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  SINGLE_USER_REQUEST,
} from '../../../constants';
import {
  VENDOR_BRANCH_COMMISSION_CREATE_IS_SUCCESS,
  VENDOR_BRANCH_COMMISSION_DELETE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* walletGetRequest(getWalletDepositHistoryApi, action) {
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    getWalletDepositHistoryApi,
    currentSession.token,
    action.dto,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: 'WALLET_LIST_SUCCESS', data: response.data });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: 'WALLET_LIST_FAILURE',
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* walletPostRequest(postWalletDepositHistoryApi, action) {
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postWalletDepositHistoryApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: 'WALLET_POST_SUCCESS', data: action.dto.listDto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: 'WALLET_POST_FAILURE',
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* walletPostSuccess(message, action) {
  console.log('action', action);

  yield put({
    type: 'WALLET_LIST_REQUEST',
    dto: action.data,
  });
  message.success('افزایش اعتبار با موفقیت انجام شد.', 5);
}
