import {
  COMMISSION_LIST_REQUEST,
  COMMISSION_LIST_SUCCESS,
  COMMISSION_LIST_FAILURE,
  COMMISSION_PUT_REQUEST,
  COMMISSION_PUT_SUCCESS,
  COMMISSION_PUT_FAILURE,
  COMMISSION_POST_REQUEST,
  COMMISSION_POST_SUCCESS,
  COMMISSION_POST_FAILURE,
  COMMISSION_DELETE_REQUEST,
  COMMISSION_DELETE_SUCCESS,
  COMMISSION_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="Commission List">
export function commissionListRequest(data) {
  return {
    type: COMMISSION_LIST_REQUEST,
    data,
  };
}

export function commissionListSuccess(data) {
  return {
    type: COMMISSION_LIST_SUCCESS,
    data,
  };
}

export function commissionListFailure() {
  return {
    type: COMMISSION_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Commission post">
export function commissionPostRequest(data) {
  return {
    type: COMMISSION_POST_REQUEST,
    data,
  };
}

export function commissionPostSuccess(data) {
  return {
    type: COMMISSION_POST_SUCCESS,
    data,
  };
}

export function commissionPostFailure() {
  return {
    type: COMMISSION_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Commission Put">
export function commissionPutRequest(data) {
  return {
    type: COMMISSION_PUT_REQUEST,
    data,
  };
}

export function commissionPutSuccess(data) {
  return {
    type: COMMISSION_PUT_SUCCESS,
    data,
  };
}

export function commissionPutFailure() {
  return {
    type: COMMISSION_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Commission Delete">
export function commissionDeleteRequest(data) {
  return {
    type: COMMISSION_DELETE_REQUEST,
    data,
  };
}

export function commissionDeleteSuccess(data) {
  return {
    type: COMMISSION_DELETE_SUCCESS,
    data,
  };
}

export function commissionDeleteFailure() {
  return {
    type: COMMISSION_DELETE_FAILURE,
  };
}
// </editor-fold>
