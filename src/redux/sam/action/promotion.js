import {
  PROMOTION_TYPE_LIST_REQUEST,
  PROMOTION_TYPE_LIST_SUCCESS,
  PROMOTION_TYPE_LIST_FAILURE,
  SINGLE_PROMOTION_INIT_REQUEST,
  SINGLE_PROMOTION_INIT_SUCCESS,
  SINGLE_PROMOTION_INIT_FAILURE,
  PROMOTION_LIST_REQUEST,
  PROMOTION_LIST_SUCCESS,
  PROMOTION_LIST_FAILURE,
  PROMOTION_POST_REQUEST,
  PROMOTION_POST_SUCCESS,
  PROMOTION_POST_FAILURE,
  PROMOTION_PUT_REQUEST,
  PROMOTION_PUT_SUCCESS,
  PROMOTION_PUT_FAILURE,
  PROMOTION_DELETE_REQUEST,
  PROMOTION_DELETE_SUCCESS,
  PROMOTION_DELETE_FAILURE,
  PROMOTION_ALERT,
  PROMOTION_ALERT_RESET,
  PROMOTION_GROUP_ACTION_POST_REQUEST,
  PROMOTION_GROUP_ACTION_POST_SUCCESS,
  PROMOTION_GROUP_ACTION_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
  VENDOR_BRANCH_PROMOTION_LIST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_LIST_FAILURE,
  VENDOR_BRANCH_PROMOTION_PUT_REQUEST,
  VENDOR_BRANCH_PROMOTION_PUT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_PUT_FAILURE,
  VENDOR_BRANCH_PROMOTION_POST_REQUEST,
  VENDOR_BRANCH_PROMOTION_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DELETE_FAILURE,
  VENDOR_BRANCH_PROMOTION_DELETE_REQUEST,
  VENDOR_BRANCH_PROMOTION_DELETE_SUCCESS,
  CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
  CATEGORY_PROMOTION_DETAILS_LIST_FAILURE,
  CATEGORY_PROMOTION_DETAILS_LIST_SUCCESS,
  PRODUCT_PROMOTION_DETAILS_LIST_FAILURE,
  PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
  PRODUCT_PROMOTION_DETAILS_LIST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_SUCCESS,
  PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
  PROMOTION_DISCOUNT_CODE_LIST_SUCCESS,
  PROMOTION_DISCOUNT_CODE_LIST_FAILURE,
  PROMOTION_DISCOUNT_CODE_POST_REQUEST,
  PROMOTION_DISCOUNT_CODE_POST_SUCCESS,
  PROMOTION_DISCOUNT_CODE_POST_FAILURE,
  PROMOTION_DISCOUNT_CODE_PUT_REQUEST,
  PROMOTION_DISCOUNT_CODE_PUT_SUCCESS,
  PROMOTION_DISCOUNT_CODE_PUT_FAILURE,
  PROMOTION_DISCOUNT_CODE_DELETE_REQUEST,
  PROMOTION_DISCOUNT_CODE_DELETE_SUCCESS,
  PROMOTION_DISCOUNT_CODE_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="Promotion List">
export function promotionListRequest(data) {
  return {
    type: PROMOTION_LIST_REQUEST,
    data,
  };
}

export function promotionListSuccess(data) {
  return {
    type: PROMOTION_LIST_SUCCESS,
    data,
  };
}

export function promotionListFailure() {
  return {
    type: PROMOTION_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Single Promotion init">
export function singlePromotionInitRequest() {
  return {
    type: SINGLE_PROMOTION_INIT_REQUEST,
  };
}

export function singlePromotionInitSuccess(data) {
  return {
    type: SINGLE_PROMOTION_INIT_SUCCESS,
    data,
  };
}

export function singlePromotionInitFailure() {
  return {
    type: SINGLE_PROMOTION_INIT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Promotion Type">
export function promotionTypeListRequest(data) {
  return {
    type: PROMOTION_TYPE_LIST_REQUEST,
    data,
  };
}

export function promotionTypeListSuccess(data) {
  return {
    type: PROMOTION_TYPE_LIST_SUCCESS,
    data,
  };
}

export function promotionTypeListFailure(error) {
  return {
    type: PROMOTION_TYPE_LIST_FAILURE,
    payload: {
      error,
    },
  };
}

// </editor-fold>

// <editor-fold dsc="Promotion post">
export function promotionPostRequest(data) {
  return {
    type: PROMOTION_POST_REQUEST,
    data,
  };
}

export function promotionPostSuccess(data) {
  return {
    type: PROMOTION_POST_SUCCESS,
    data,
  };
}

export function promotionPostFailure() {
  return {
    type: PROMOTION_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Promotion put">
export function promotionPutRequest(data) {
  return {
    type: PROMOTION_PUT_REQUEST,
    data,
  };
}

export function promotionPutSuccess(data) {
  return {
    type: PROMOTION_PUT_SUCCESS,
    data,
  };
}

export function promotionPutFailure() {
  return {
    type: PROMOTION_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Promotion delete">
export function promotionDeleteRequest(data) {
  return {
    type: PROMOTION_DELETE_REQUEST,
    data,
  };
}

export function promotionDeleteSuccess(data) {
  return {
    type: PROMOTION_DELETE_SUCCESS,
    data,
  };
}

export function promotionDeleteFailure() {
  return {
    type: PROMOTION_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Promotion group action post">
export function promotionGroupActionPostRequest(data) {
  return {
    type: PROMOTION_GROUP_ACTION_POST_REQUEST,
    data,
  };
}

export function promotionGroupActionPostSuccess(data) {
  return {
    type: PROMOTION_GROUP_ACTION_POST_SUCCESS,
    data,
  };
}

export function promotionGroupActionPostFailure() {
  return {
    type: PROMOTION_GROUP_ACTION_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Promotion List">
export function vendorBranchPromotionListRequest(data) {
  return {
    type: VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
    data,
  };
}

export function vendorBranchPromotionListSuccess(data) {
  return {
    type: VENDOR_BRANCH_PROMOTION_LIST_SUCCESS,
    data,
  };
}

export function vendorBranchPromotionListFailure() {
  return {
    type: VENDOR_BRANCH_PROMOTION_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Promotion Put">
export function vendorBranchPromotionPutRequest(data) {
  return {
    type: VENDOR_BRANCH_PROMOTION_PUT_REQUEST,
    data,
  };
}

export function vendorBranchPromotionPutSuccess() {
  return {
    type: VENDOR_BRANCH_PROMOTION_PUT_SUCCESS,
  };
}

export function vendorBranchPromotionPutFailure() {
  return {
    type: VENDOR_BRANCH_PROMOTION_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Promotion Post">
export function vendorBranchPromotionPostRequest(data) {
  return {
    type: VENDOR_BRANCH_PROMOTION_POST_REQUEST,
    data,
  };
}

export function vendorBranchPromotionPostSuccess() {
  return {
    type: VENDOR_BRANCH_PROMOTION_POST_SUCCESS,
  };
}

export function vendorBranchPromotionPostFailure() {
  return {
    type: VENDOR_BRANCH_PROMOTION_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Promotion Delete">
export function vendorBranchPromotionDeleteRequest(data) {
  return {
    type: VENDOR_BRANCH_PROMOTION_DELETE_REQUEST,
    data,
  };
}

export function vendorBranchPromotionDeleteSuccess() {
  return {
    type: VENDOR_BRANCH_PROMOTION_DELETE_SUCCESS,
  };
}

export function vendorBranchPromotionDeleteFailure() {
  return {
    type: VENDOR_BRANCH_PROMOTION_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="category Promotion details List">
export function categoryPromotionDetailsListRequest(data) {
  return {
    type: CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
    data,
  };
}

export function categoryPromotionDetailsListSuccess(data) {
  return {
    type: CATEGORY_PROMOTION_DETAILS_LIST_SUCCESS,
    data,
  };
}

export function categoryPromotionDetailsListFailure() {
  return {
    type: CATEGORY_PROMOTION_DETAILS_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="product Promotion details List">
export function productPromotionDetailsListRequest(data) {
  return {
    type: PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
    data,
  };
}

export function productPromotionDetailsListSuccess(data) {
  return {
    type: PRODUCT_PROMOTION_DETAILS_LIST_SUCCESS,
    data,
  };
}

export function productPromotionDetailsListFailure() {
  return {
    type: PRODUCT_PROMOTION_DETAILS_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Promotion Details Post">
export function vendorBranchPromotionDetailsPostRequest(data) {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_POST_REQUEST,
    data,
  };
}

export function vendorBranchPromotionDetailsPostSuccess() {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_POST_SUCCESS,
  };
}

export function vendorBranchPromotionDetailsPostFailure() {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Promotion Details Put">
export function vendorBranchPromotionDetailsPutRequest(data) {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_PUT_REQUEST,
    data,
  };
}

export function vendorBranchPromotionDetailsPutSuccess() {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_PUT_SUCCESS,
  };
}

export function vendorBranchPromotionDetailsPutFailure() {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Promotion Details Delete">
export function vendorBranchPromotionDetailsDeleteRequest(data) {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_REQUEST,
    data,
  };
}

export function vendorBranchPromotionDetailsDeleteSuccess() {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_SUCCESS,
  };
}

export function vendorBranchPromotionDetailsDeleteFailure() {
  return {
    type: VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Promotion discount code List">
export function promotionDiscountCodeListRequest(data) {
  return {
    type: PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
    data,
  };
}

export function promotionDiscountCodeListSuccess(data) {
  return {
    type: PROMOTION_DISCOUNT_CODE_LIST_SUCCESS,
    data,
  };
}

export function promotionDiscountCodeListFailure() {
  return {
    type: PROMOTION_DISCOUNT_CODE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Promotion discount code post">
export function promotionDiscountCodePostRequest(data) {
  return {
    type: PROMOTION_DISCOUNT_CODE_POST_REQUEST,
    data,
  };
}

export function promotionDiscountCodePostSuccess(data) {
  return {
    type: PROMOTION_DISCOUNT_CODE_POST_SUCCESS,
    data,
  };
}

export function promotionDiscountCodePostFailure() {
  return {
    type: PROMOTION_DISCOUNT_CODE_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Promotion discount code put">
export function promotionDiscountCodePutRequest(data) {
  return {
    type: PROMOTION_DISCOUNT_CODE_PUT_REQUEST,
    data,
  };
}

export function promotionDiscountCodePutSuccess(data) {
  return {
    type: PROMOTION_DISCOUNT_CODE_PUT_SUCCESS,
    data,
  };
}

export function promotionDiscountCodePutFailure() {
  return {
    type: PROMOTION_DISCOUNT_CODE_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Promotion discount code delete">
export function promotionDiscountCodeDeleteRequest(data) {
  return {
    type: PROMOTION_DISCOUNT_CODE_DELETE_REQUEST,
    data,
  };
}

export function promotionDiscountCodeDeleteSuccess(data) {
  return {
    type: PROMOTION_DISCOUNT_CODE_DELETE_SUCCESS,
    data,
  };
}

export function promotionDiscountCodeDeleteFailure() {
  return {
    type: PROMOTION_DISCOUNT_CODE_DELETE_FAILURE,
  };
}
// </editor-fold>

export function promotionAlert(data) {
  return {
    type: PROMOTION_ALERT,
    data,
  };
}

export function promotionAlertReset() {
  return {
    type: PROMOTION_ALERT_RESET,
  };
}
