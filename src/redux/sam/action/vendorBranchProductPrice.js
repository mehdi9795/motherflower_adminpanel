import {
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_REQUEST,
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_SUCCESS,
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_FAILURE,
  VENDOR_BRANCH_PRODUCT_PRICE_PUT_REQUEST,
  VENDOR_BRANCH_PRODUCT_PRICE_PUT_SUCCESS,
  VENDOR_BRANCH__PRODUCT_PRICE_PUT_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="Commission List">
export function vendorBranchProductPriceListRequest(data) {
  return {
    type: VENDOR_BRANCH_PRODUCT_PRICE_LIST_REQUEST,
    data,
  };
}

export function vendorBranchProductPriceListSuccess(data) {
  return {
    type: VENDOR_BRANCH_PRODUCT_PRICE_LIST_SUCCESS,
    data,
  };
}

export function vendorBranchProductPriceListFailure() {
  return {
    type: VENDOR_BRANCH_PRODUCT_PRICE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Commission Put">
export function vendorBranchProductPricePutRequest(data) {
  return {
    type: VENDOR_BRANCH_PRODUCT_PRICE_PUT_REQUEST,
    data,
  };
}

export function vendorBranchProductPricePutSuccess(data) {
  return {
    type: VENDOR_BRANCH_PRODUCT_PRICE_PUT_SUCCESS,
    data,
  };
}

export function vendorBranchProductPricePutFailure() {
  return {
    type: VENDOR_BRANCH__PRODUCT_PRICE_PUT_FAILURE,
  };
}
// </editor-fold>
