import {
  ORDER_LIST_REQUEST,
  ORDER_LIST_SUCCESS,
  ORDER_LIST_FAILURE,
  SINGLE_ORDER_REQUEST,
  SINGLE_ORDER_SUCCESS,
  SINGLE_ORDER_FAILURE,
  ORDER_PUT_REQUEST,
  ORDER_PUT_SUCCESS,
  ORDER_PUT_FAILURE,
  ORDER_STATUS_LIST_REQUEST,
  ORDER_STATUS_LIST_SUCCESS,
  ORDER_STATUS_LIST_FAILURE,
  ORDER_ITEM_LIST_REQUEST,
  ORDER_ITEM_LIST_SUCCESS,
  ORDER_ITEM_LIST_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="order List">
export function orderListRequest(data) {
  return {
    type: ORDER_LIST_REQUEST,
    data,
  };
}

export function orderListSuccess(data) {
  return {
    type: ORDER_LIST_SUCCESS,
    data,
  };
}

export function orderListFailure() {
  return {
    type: ORDER_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single order">
export function singleOrderListRequest(data) {
  return {
    type: SINGLE_ORDER_REQUEST,
    data,
  };
}

export function singleOrderListSuccess(data) {
  return {
    type: SINGLE_ORDER_SUCCESS,
    data,
  };
}

export function singleOrderListFailure() {
  return {
    type: SINGLE_ORDER_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="order Put">
export function orderPutRequest(data) {
  return {
    type: ORDER_PUT_REQUEST,
    data,
  };
}

export function orderPutSuccess(data) {
  return {
    type: ORDER_PUT_SUCCESS,
    data,
  };
}

export function orderPutFailure() {
  return {
    type: ORDER_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="order status list">
export function orderStatusListRequest(data) {
  return {
    type: ORDER_STATUS_LIST_REQUEST,
    data,
  };
}

export function orderStatusListSuccess(data) {
  return {
    type: ORDER_STATUS_LIST_SUCCESS,
    data,
  };
}

export function orderStatusListFailure() {
  return {
    type: ORDER_STATUS_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="order item List">
export function orderItemListRequest(data) {
  return {
    type: ORDER_ITEM_LIST_REQUEST,
    data,
  };
}

export function orderItemListSuccess(data) {
  return {
    type: ORDER_ITEM_LIST_SUCCESS,
    data,
  };
}

export function orderItemListFailure() {
  return {
    type: ORDER_ITEM_LIST_FAILURE,
  };
}
// </editor-fold>
