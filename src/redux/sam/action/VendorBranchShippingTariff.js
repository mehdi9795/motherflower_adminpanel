import {
  VB_SHIPPING_TARIFF_LIST_REQUEST,
  VB_SHIPPING_TARIFF_LIST_SUCCESS,
  VB_SHIPPING_TARIFF_LIST_FAILURE,
  VB_SHIPPING_TARIFF_POST_REQUEST,
  VB_SHIPPING_TARIFF_POST_SUCCESS,
  VB_SHIPPING_TARIFF_POST_FAILURE,
  VB_SHIPPING_TARIFF_PUT_REQUEST,
  VB_SHIPPING_TARIFF_PUT_SUCCESS,
  VB_SHIPPING_TARIFF_PUT_FAILURE,
  VB_SHIPPING_TARIFF_DELETE_REQUEST,
  VB_SHIPPING_TARIFF_DELETE_SUCCESS,
  VB_SHIPPING_TARIFF_DELETE_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_POST_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_POST_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_POST_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="vb shipping tariff List">
export function vbShippingTariffListRequest(data) {
  return {
    type: VB_SHIPPING_TARIFF_LIST_REQUEST,
    data,
  };
}

export function vbShippingTariffListSuccess(data) {
  return {
    type: VB_SHIPPING_TARIFF_LIST_SUCCESS,
    data,
  };
}

export function vbShippingTariffListFailure() {
  return {
    type: VB_SHIPPING_TARIFF_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vb shift trafic post">
export function vbShippingTariffPostRequest(data) {
  return {
    type: VB_SHIPPING_TARIFF_POST_REQUEST,
    data,
  };
}

export function vbShippingTariffPostSuccess(data) {
  return {
    type: VB_SHIPPING_TARIFF_POST_SUCCESS,
    data,
  };
}

export function vbShippingTariffPostFailure() {
  return {
    type: VB_SHIPPING_TARIFF_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vb shift trafic Put">
export function vbShippingTariffPutRequest(data) {
  return {
    type: VB_SHIPPING_TARIFF_PUT_REQUEST,
    data,
  };
}

export function vbShippingTariffPutSuccess(data) {
  return {
    type: VB_SHIPPING_TARIFF_PUT_SUCCESS,
    data,
  };
}

export function vbShippingTariffPutFailure() {
  return {
    type: VB_SHIPPING_TARIFF_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vb shift tariff Delete">
export function vbShippingTariffDeleteRequest(data) {
  return {
    type: VB_SHIPPING_TARIFF_DELETE_REQUEST,
    data,
  };
}

export function vbShippingTariffDeleteSuccess(data) {
  return {
    type: VB_SHIPPING_TARIFF_DELETE_SUCCESS,
    data,
  };
}

export function vbShippingTariffDeleteFailure() {
  return {
    type: VB_SHIPPING_TARIFF_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vb shiping tariff promotion List">
export function vbShippingTariffPromotionListRequest(data) {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
    data,
  };
}

export function vbShippingTariffPromotionListSuccess(data) {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_LIST_SUCCESS,
    data,
  };
}

export function vbShippingTariffPromotionListFailure() {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vb shift tariff promotion post">
export function vbShippingTariffPromotionPostRequest(data) {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_POST_REQUEST,
    data,
  };
}

export function vbShippingTariffPromotionPostSuccess(data) {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_POST_SUCCESS,
    data,
  };
}

export function vbShippingTariffPromotionPostFailure() {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vb shift tariff promotion Put">
export function vbShippingTariffPromotionPutRequest(data) {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_PUT_REQUEST,
    data,
  };
}

export function vbShippingTariffPromotionPutSuccess(data) {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_PUT_SUCCESS,
    data,
  };
}

export function vbShippingTariffPromotionPutFailure() {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vb shift tariff promotion Delete">
export function vbShippingTariffPromotionDeleteRequest(data) {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_DELETE_REQUEST,
    data,
  };
}

export function vbShippingTariffPromotionDeleteSuccess(data) {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_DELETE_SUCCESS,
    data,
  };
}

export function vbShippingTariffPromotionDeleteFailure() {
  return {
    type: VB_SHIPPING_TARIFF_PROMOTION_DELETE_FAILURE,
  };
}
// </editor-fold>
