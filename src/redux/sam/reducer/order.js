import {
  ORDER_LIST_REQUEST,
  ORDER_LIST_SUCCESS,
  ORDER_LIST_FAILURE,
  SINGLE_ORDER_REQUEST,
  SINGLE_ORDER_SUCCESS,
  SINGLE_ORDER_FAILURE,
  ORDER_PUT_REQUEST,
  ORDER_PUT_SUCCESS,
  ORDER_PUT_FAILURE,
  ORDER_STATUS_LIST_REQUEST,
  ORDER_STATUS_LIST_SUCCESS,
  ORDER_STATUS_LIST_FAILURE,
  ORDER_ITEM_LIST_REQUEST,
  ORDER_ITEM_LIST_SUCCESS,
  ORDER_ITEM_LIST_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  orderListRequest: false,
  orderListData: getResponseModel,
  orderListError: false,

  orderItemListRequest: false,
  orderItemListData: getResponseModel,
  orderItemListError: false,

  singleOrderRequest: false,
  singleOrderData: getResponseModel,
  singleOrderError: false,

  orderStatusListRequest: false,
  orderStatusListData: getResponseModel,
  orderStatusListError: false,

  orderPutRequest: false,
  orderPutData: getResponseModel,

  orderCRUDError: false,
  orderErrorMessage: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="order List">
    case ORDER_LIST_REQUEST:
      return {
        ...state,
        orderListRequest: true,
        orderListError: false,
      };
    case ORDER_LIST_SUCCESS:
      return {
        ...state,
        orderListData: action.data,
        orderListRequest: false,
        orderListError: false,
      };
    case ORDER_LIST_FAILURE:
      return {
        ...state,
        orderListError: true,
        orderListRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="single order">
    case SINGLE_ORDER_REQUEST:
      return {
        ...state,
        singleOrderRequest: true,
        singleOrderError: false,
      };
    case SINGLE_ORDER_SUCCESS:
      return {
        ...state,
        singleOrderData: action.data,
        singleOrderRequest: false,
        singleOrderError: false,
      };
    case SINGLE_ORDER_FAILURE:
      return {
        ...state,
        singleOrderError: true,
        singleOrderRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="order put">
    case ORDER_PUT_REQUEST:
      return {
        ...state,
        orderPutRequest: true,
        orderCRUDError: false,
      };
    case ORDER_PUT_SUCCESS:
      return {
        ...state,
        orderPutData: action.data,
        orderPutRequest: false,
        orderCRUDError: false,
      };
    case ORDER_PUT_FAILURE:
      return {
        ...state,
        orderCRUDError: true,
        orderPutRequest: false,
        orderErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="order status List">
    case ORDER_STATUS_LIST_REQUEST:
      return {
        ...state,
        orderStatusListRequest: true,
        orderStatusListError: false,
      };
    case ORDER_STATUS_LIST_SUCCESS:
      return {
        ...state,
        orderStatusListData: action.data,
        orderStatusListRequest: false,
        orderStatusListError: false,
      };
    case ORDER_STATUS_LIST_FAILURE:
      return {
        ...state,
        orderStatusListError: true,
        orderStatusListRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="order item List">
    case ORDER_ITEM_LIST_REQUEST:
      return {
        ...state,
        orderItemListRequest: true,
        orderItemListError: false,
      };
    case ORDER_ITEM_LIST_SUCCESS:
      return {
        ...state,
        orderItemListData: action.data,
        orderItemListRequest: false,
        orderItemListError: false,
      };
    case ORDER_ITEM_LIST_FAILURE:
      return {
        ...state,
        orderItemListError: true,
        orderItemListRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
