import {
  VB_SHIPPING_TARIFF_LIST_REQUEST,
  VB_SHIPPING_TARIFF_LIST_SUCCESS,
  VB_SHIPPING_TARIFF_LIST_FAILURE,
  VB_SHIPPING_TARIFF_PUT_REQUEST,
  VB_SHIPPING_TARIFF_PUT_SUCCESS,
  VB_SHIPPING_TARIFF_PUT_FAILURE,
  VB_SHIPPING_TARIFF_POST_REQUEST,
  VB_SHIPPING_TARIFF_POST_SUCCESS,
  VB_SHIPPING_TARIFF_POST_FAILURE,
  VB_SHIPPING_TARIFF_DELETE_REQUEST,
  VB_SHIPPING_TARIFF_DELETE_SUCCESS,
  VB_SHIPPING_TARIFF_DELETE_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_LIST_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_POST_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_POST_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_POST_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_PUT_FAILURE,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_REQUEST,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_SUCCESS,
  VB_SHIPPING_TARIFF_PROMOTION_DELETE_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  vbShippingTariffRequest: false,
  vbShippingTariffData: getResponseModel,
  vbShippingTariffError: false,

  vbShippingTariffPostRequest: false,
  vbShippingTariffPostData: getResponseModel,

  vbShippingTariffPutRequest: false,
  vbShippingTariffPutData: getResponseModel,

  vbShippingTariffDeleteRequest: false,
  vbShippingTariffDeleteData: getResponseModel,

  vbShippingTariffPromotionRequest: false,
  vbShippingTariffPromotionData: getResponseModel,
  vbShippingTariffPromotionError: false,

  vbShippingTariffPromotionPostRequest: false,
  vbShippingTariffPromotionPostData: getResponseModel,

  vbShippingTariffPromotionPutRequest: false,
  vbShippingTariffPromotionPutData: getResponseModel,

  vbShippingTariffPromotionDeleteRequest: false,
  vbShippingTariffPromotionDeleteData: getResponseModel,

  vbShippingTariffCRUDError: false,
  vbShippingTariffErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="vb shipping tariff List">
    case VB_SHIPPING_TARIFF_LIST_REQUEST:
      return {
        ...state,
        vbShippingTariffRequest: true,
        vbShippingTariffError: false,
      };
    case VB_SHIPPING_TARIFF_LIST_SUCCESS:
      return {
        ...state,
        vbShippingTariffData: action.data,
        vbShippingTariffRequest: false,
        vbShippingTariffError: false,
      };
    case VB_SHIPPING_TARIFF_LIST_FAILURE:
      return {
        ...state,
        vbShippingTariffError: true,
        vbShippingTariffRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="vb shipping tariff post">
    case VB_SHIPPING_TARIFF_POST_REQUEST:
      return {
        ...state,
        vbShippingTariffPostRequest: true,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_POST_SUCCESS:
      return {
        ...state,
        vbShippingTariffPostData: action.data,
        vbShippingTariffPostRequest: false,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_POST_FAILURE:
      return {
        ...state,
        vbShippingTariffCRUDError: true,
        vbShippingTariffPostRequest: false,
        vbShippingTariffErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vb shipping tariff put">
    case VB_SHIPPING_TARIFF_PUT_REQUEST:
      return {
        ...state,
        vbShippingTariffPutRequest: true,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_PUT_SUCCESS:
      return {
        ...state,
        vbShippingTariffPutData: action.data,
        vbShippingTariffPutRequest: false,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_PUT_FAILURE:
      return {
        ...state,
        vbShippingTariffCRUDError: true,
        vbShippingTariffPutRequest: false,
        vbShippingTariffErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vb shipping tariff delete">
    case VB_SHIPPING_TARIFF_DELETE_REQUEST:
      return {
        ...state,
        vbShippingTariffDeleteRequest: true,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_DELETE_SUCCESS:
      return {
        ...state,
        vbShippingTariffDeleteData: action.data,
        vbShippingTariffDeleteRequest: false,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_DELETE_FAILURE:
      return {
        ...state,
        vbShippingTariffCRUDError: true,
        vbShippingTariffDeleteRequest: false,
        vbShippingTariffErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vb shipping tariff promotion List">
    case VB_SHIPPING_TARIFF_PROMOTION_LIST_REQUEST:
      return {
        ...state,
        vbShippingTariffPromotionRequest: true,
        vbShippingTariffPromotionError: false,
      };
    case VB_SHIPPING_TARIFF_PROMOTION_LIST_SUCCESS:
      return {
        ...state,
        vbShippingTariffPromotionData: action.data,
        vbShippingTariffPromotionRequest: false,
        vbShippingTariffPromotionError: false,
      };
    case VB_SHIPPING_TARIFF_PROMOTION_LIST_FAILURE:
      return {
        ...state,
        vbShippingTariffPromotionError: true,
        vbShippingTariffPromotionRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="vb shipping tariff promotion post">
    case VB_SHIPPING_TARIFF_PROMOTION_POST_REQUEST:
      return {
        ...state,
        vbShippingTariffPromotionPostRequest: true,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_PROMOTION_POST_SUCCESS:
      return {
        ...state,
        vbShippingTariffPromotionPostData: action.data,
        vbShippingTariffPromotionPostRequest: false,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_PROMOTION_POST_FAILURE:
      return {
        ...state,
        vbShippingTariffCRUDError: true,
        vbShippingTariffPromotionPostRequest: false,
        vbShippingTariffErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vb shipping tariff promotion put">
    case VB_SHIPPING_TARIFF_PROMOTION_PUT_REQUEST:
      return {
        ...state,
        vbShippingTariffPromotionPutRequest: true,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_PROMOTION_PUT_SUCCESS:
      return {
        ...state,
        vbShippingTariffPromotionPutData: action.data,
        vbShippingTariffPromotionPutRequest: false,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_PROMOTION_PUT_FAILURE:
      return {
        ...state,
        vbShippingTariffCRUDError: true,
        vbShippingTariffPromotionPutRequest: false,
        vbShippingTariffErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vb shipping tariff promotion delete">
    case VB_SHIPPING_TARIFF_PROMOTION_DELETE_REQUEST:
      return {
        ...state,
        vbShippingTariffPromotionDeleteRequest: true,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_PROMOTION_DELETE_SUCCESS:
      return {
        ...state,
        vbShippingTariffPromotionDeleteData: action.data,
        vbShippingTariffPromotionDeleteRequest: false,
        vbShippingTariffCRUDError: false,
      };
    case VB_SHIPPING_TARIFF_PROMOTION_DELETE_FAILURE:
      return {
        ...state,
        vbShippingTariffCRUDError: true,
        vbShippingTariffPromotionDeleteRequest: false,
        vbShippingTariffErrorMessage: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
