import {
  PROMOTION_TYPE_LIST_REQUEST,
  PROMOTION_TYPE_LIST_SUCCESS,
  PROMOTION_TYPE_LIST_FAILURE,
  SINGLE_PROMOTION_INIT_REQUEST,
  SINGLE_PROMOTION_INIT_SUCCESS,
  SINGLE_PROMOTION_INIT_FAILURE,
  PROMOTION_LIST_REQUEST,
  PROMOTION_LIST_SUCCESS,
  PROMOTION_LIST_FAILURE,
  PROMOTION_POST_REQUEST,
  PROMOTION_POST_SUCCESS,
  PROMOTION_POST_FAILURE,
  PROMOTION_PUT_REQUEST,
  PROMOTION_PUT_SUCCESS,
  PROMOTION_PUT_FAILURE,
  PROMOTION_DELETE_REQUEST,
  PROMOTION_DELETE_SUCCESS,
  PROMOTION_DELETE_FAILURE,
  PROMOTION_ALERT,
  PROMOTION_ALERT_RESET,
  PROMOTION_GROUP_ACTION_POST_REQUEST,
  PROMOTION_GROUP_ACTION_POST_SUCCESS,
  PROMOTION_GROUP_ACTION_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_LIST_REQUEST,
  VENDOR_BRANCH_PROMOTION_LIST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_LIST_FAILURE,
  VENDOR_BRANCH_PROMOTION_PUT_REQUEST,
  VENDOR_BRANCH_PROMOTION_PUT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_PUT_FAILURE,
  VENDOR_BRANCH_PROMOTION_POST_REQUEST,
  VENDOR_BRANCH_PROMOTION_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DELETE_FAILURE,
  VENDOR_BRANCH_PROMOTION_DELETE_REQUEST,
  VENDOR_BRANCH_PROMOTION_DELETE_SUCCESS,
  CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
  CATEGORY_PROMOTION_DETAILS_LIST_FAILURE,
  CATEGORY_PROMOTION_DETAILS_LIST_SUCCESS,
  PRODUCT_PROMOTION_DETAILS_LIST_FAILURE,
  PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
  PRODUCT_PROMOTION_DETAILS_LIST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_POST_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_PUT_SUCCESS,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_REQUEST,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_FAILURE,
  VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_SUCCESS,
  PROMOTION_DISCOUNT_CODE_POST_REQUEST,
  PROMOTION_DISCOUNT_CODE_POST_SUCCESS,
  PROMOTION_DISCOUNT_CODE_POST_FAILURE,
  PROMOTION_DISCOUNT_CODE_PUT_REQUEST,
  PROMOTION_DISCOUNT_CODE_PUT_SUCCESS,
  PROMOTION_DISCOUNT_CODE_PUT_FAILURE,
  PROMOTION_DISCOUNT_CODE_DELETE_REQUEST,
  PROMOTION_DISCOUNT_CODE_DELETE_SUCCESS,
  PROMOTION_DISCOUNT_CODE_DELETE_FAILURE,
  PROMOTION_DISCOUNT_CODE_LIST_REQUEST,
  PROMOTION_DISCOUNT_CODE_LIST_SUCCESS,
  PROMOTION_DISCOUNT_CODE_LIST_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  promotionRequest: false,
  promotionData: getResponseModel,
  promotionError: false,

  singlePromotionInitRequest: false,
  singlePromotionInitData: getResponseModel,
  singlePromotionInitError: false,

  promotionTypesRequest: false,
  promotionTypesData: getResponseModel,
  promotionTypesError: false,

  vendorBranchPromotionListRequest: false,
  vendorBranchPromotionListData: getResponseModel,
  vendorBranchPromotionListError: false,

  promotionPostRequest: false,
  promotionPostData: getResponseModel,

  promotionPutRequest: false,
  promotionPutData: getResponseModel,

  promotionDeleteRequest: false,
  promotionDeleteData: getResponseModel,

  promotionGroupActionPostRequest: false,
  promotionGroupActionPostData: getResponseModel,

  vendorBranchPromotionPutRequest: false,
  vendorBranchPromotionPostRequest: false,
  vendorBranchPromotionDeleteRequest: false,

  categoryPromotionDetailsListRequest: false,
  categoryPromotionDetailsListData: getResponseModel,
  categoryPromotionDetailsListError: false,

  productPromotionDetailsListRequest: false,
  productPromotionDetailsListData: getResponseModel,
  productPromotionDetailsListError: false,

  vendorBranchPromotionDetailsPostRequest: false,
  vendorBranchPromotionDetailsPostData: getResponseModel,

  vendorBranchPromotionDetailsPutRequest: false,
  vendorBranchPromotionDetailsPutData: getResponseModel,

  vendorBranchPromotionDetailsDeleteRequest: false,
  vendorBranchPromotionDetailsDeleteData: getResponseModel,

  PromotionDiscountCodeListRequest: false,
  PromotionDiscountCodeListData: getResponseModel,
  PromotionDiscountCodeListError: false,

  promotionDiscountCodePostRequest: false,
  promotionDiscountCodePostData: getResponseModel,

  promotionDiscountCodePutRequest: false,
  promotionDiscountCodePutData: getResponseModel,

  promotionDiscountCodeDeleteRequest: false,
  promotionDiscountCodeDeleteData: getResponseModel,

  PromotionCRUDError: false,
  PromotionErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="Promotion List">
    case PROMOTION_LIST_REQUEST:
      return {
        ...state,
        promotionRequest: true,
        promotionError: false,
      };
    case PROMOTION_LIST_SUCCESS:
      return {
        ...state,
        promotionData: action.data,
        promotionRequest: false,
        promotionError: false,
      };
    case PROMOTION_LIST_FAILURE:
      return {
        ...state,
        promotionError: true,
        promotionRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Single Promotion Init">
    case SINGLE_PROMOTION_INIT_REQUEST:
      return {
        ...state,
        singlePromotionLoading: true,
        singlePromotionError: false,
      };
    case SINGLE_PROMOTION_INIT_SUCCESS:
      return {
        ...state,
        singlePromotionLoading: false,
        singlePromotionInitData: action.data,
        singlePromotionError: false,
      };
    case SINGLE_PROMOTION_INIT_FAILURE:
      return {
        ...state,
        singlePromotionError: true,
        singlePromotionLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Promotion Type List">
    case PROMOTION_TYPE_LIST_REQUEST:
      return {
        ...state,
        promotionTypesRequest: true,
        promotionTypesError: false,
      };
    case PROMOTION_TYPE_LIST_SUCCESS:
      return {
        ...state,
        promotionTypesData: action.data,
        promotionTypesRequest: false,
        promotionTypesError: false,
      };
    case PROMOTION_TYPE_LIST_FAILURE:
      return {
        ...state,
        promotionTypesError: true,
        promotionTypesRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Promotion post">
    case PROMOTION_POST_REQUEST:
      return {
        ...state,
        promotionPostRequest: true,
        PromotionCRUDError: false,
      };
    case PROMOTION_POST_SUCCESS:
      return {
        ...state,
        promotionPostData: action.data,
        promotionPostRequest: false,
        PromotionCRUDError: false,
      };
    case PROMOTION_POST_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        promotionPostRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="Promotion put">
    case PROMOTION_PUT_REQUEST:
      return {
        ...state,
        promotionPutRequest: true,
        PromotionCRUDError: false,
      };
    case PROMOTION_PUT_SUCCESS:
      return {
        ...state,
        promotionPutData: action.data,
        promotionPutRequest: false,
        PromotionCRUDError: false,
      };
    case PROMOTION_PUT_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        promotionPutRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="Promotion delete">
    case PROMOTION_DELETE_REQUEST:
      return {
        ...state,
        promotionDeleteRequest: true,
        PromotionCRUDError: false,
      };
    case PROMOTION_DELETE_SUCCESS:
      return {
        ...state,
        promotionDeleteData: action.data,
        promotionDeleteRequest: false,
        PromotionCRUDError: false,
      };
    case PROMOTION_DELETE_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        promotionDeleteRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="Promotion group action post">
    case PROMOTION_GROUP_ACTION_POST_REQUEST:
      return {
        ...state,
        promotionGroupActionPostRequest: true,
        PromotionCRUDError: false,
      };
    case PROMOTION_GROUP_ACTION_POST_SUCCESS:
      return {
        ...state,
        promotionGroupActionPostData: action.data,
        promotionGroupActionPostRequest: false,
        PromotionCRUDError: false,
      };
    case PROMOTION_GROUP_ACTION_POST_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        promotionGroupActionPostRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch Promotion List">
    case VENDOR_BRANCH_PROMOTION_LIST_REQUEST:
      return {
        ...state,
        vendorBranchPromotionListRequest: true,
        vendorBranchPromotionListError: false,
      };
    case VENDOR_BRANCH_PROMOTION_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchPromotionListData: action.data,
        vendorBranchPromotionListRequest: false,
        vendorBranchPromotionListError: false,
      };
    case VENDOR_BRANCH_PROMOTION_LIST_FAILURE:
      return {
        ...state,
        vendorBranchPromotionListError: true,
        vendorBranchPromotionListRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch Promotion Put">
    case VENDOR_BRANCH_PROMOTION_PUT_REQUEST:
      return {
        ...state,
        vendorBranchPromotionPutRequest: true,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_PUT_SUCCESS:
      return {
        ...state,
        vendorBranchPromotionPutRequest: false,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_PUT_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        vendorBranchPromotionPutRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch Promotion Post">
    case VENDOR_BRANCH_PROMOTION_POST_REQUEST:
      return {
        ...state,
        vendorBranchPromotionPostRequest: true,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_POST_SUCCESS:
      return {
        ...state,
        vendorBranchPromotionPostRequest: false,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_POST_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        vendorBranchPromotionPostRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch Promotion Delete">
    case VENDOR_BRANCH_PROMOTION_DELETE_REQUEST:
      return {
        ...state,
        vendorBranchPromotionDeleteRequest: true,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_DELETE_SUCCESS:
      return {
        ...state,
        vendorBranchPromotionDeleteRequest: false,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_DELETE_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        vendorBranchPromotionDeleteRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="category Promotion Details List">
    case CATEGORY_PROMOTION_DETAILS_LIST_REQUEST:
      return {
        ...state,
        categoryPromotionDetailsListRequest: true,
        categoryPromotionDetailsListError: false,
      };
    case CATEGORY_PROMOTION_DETAILS_LIST_SUCCESS:
      return {
        ...state,
        categoryPromotionDetailsListData: action.data,
        categoryPromotionDetailsListRequest: false,
        categoryPromotionDetailsListError: false,
      };
    case CATEGORY_PROMOTION_DETAILS_LIST_FAILURE:
      return {
        ...state,
        categoryPromotionDetailsListError: true,
        categoryPromotionDetailsListRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="product Promotion Details List">
    case PRODUCT_PROMOTION_DETAILS_LIST_REQUEST:
      return {
        ...state,
        productPromotionDetailsListRequest: true,
        productPromotionDetailsListError: false,
      };
    case PRODUCT_PROMOTION_DETAILS_LIST_SUCCESS:
      return {
        ...state,
        productPromotionDetailsListData: action.data,
        productPromotionDetailsListRequest: false,
        productPromotionDetailsListError: false,
      };
    case PRODUCT_PROMOTION_DETAILS_LIST_FAILURE:
      return {
        ...state,
        productPromotionDetailsListError: true,
        productPromotionDetailsListRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch Promotion Details Post">
    case VENDOR_BRANCH_PROMOTION_DETAILS_POST_REQUEST:
      return {
        ...state,
        vendorBranchPromotionDetailsPostRequest: true,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_DETAILS_POST_SUCCESS:
      return {
        ...state,
        vendorBranchPromotionDetailsPostRequest: false,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_DETAILS_POST_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        vendorBranchPromotionDetailsPostRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch Promotion Details put">
    case VENDOR_BRANCH_PROMOTION_DETAILS_PUT_REQUEST:
      return {
        ...state,
        vendorBranchPromotionDetailsPutRequest: true,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_DETAILS_PUT_SUCCESS:
      return {
        ...state,
        vendorBranchPromotionDetailsPutRequest: false,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_DETAILS_PUT_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        vendorBranchPromotionDetailsPutRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch Promotion Details Delete">
    case VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_REQUEST:
      return {
        ...state,
        vendorBranchPromotionDetailsDeleteRequest: true,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_SUCCESS:
      return {
        ...state,
        vendorBranchPromotionDetailsDeleteRequest: false,
        PromotionCRUDError: false,
      };
    case VENDOR_BRANCH_PROMOTION_DETAILS_DELETE_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        vendorBranchPromotionDetailsDeleteRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="Promotion discount Code List">
    case PROMOTION_DISCOUNT_CODE_LIST_REQUEST:
      return {
        ...state,
        promotionDiscountCodeRequest: true,
        promotionDiscountCodeError: false,
      };
    case PROMOTION_DISCOUNT_CODE_LIST_SUCCESS:
      return {
        ...state,
        promotionDiscountCodeData: action.data,
        promotionDiscountCodeRequest: false,
        promotionDiscountCodeError: false,
      };
    case PROMOTION_DISCOUNT_CODE_LIST_FAILURE:
      return {
        ...state,
        promotionDiscountCodeError: true,
        promotionDiscountCodeRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Promotion Discount Code post">
    case PROMOTION_DISCOUNT_CODE_POST_REQUEST:
      return {
        ...state,
        promotionDiscountCodePostRequest: true,
        PromotionCRUDError: false,
      };
    case PROMOTION_DISCOUNT_CODE_POST_SUCCESS:
      return {
        ...state,
        promotionDiscountCodePostData: action.data,
        promotionDiscountCodePostRequest: false,
        PromotionCRUDError: false,
      };
    case PROMOTION_DISCOUNT_CODE_POST_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        promotionDiscountCodePostRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="Promotion Discount Code put">
    case PROMOTION_DISCOUNT_CODE_PUT_REQUEST:
      return {
        ...state,
        promotionDiscountCodePutRequest: true,
        PromotionCRUDError: false,
      };
    case PROMOTION_DISCOUNT_CODE_PUT_SUCCESS:
      return {
        ...state,
        promotionDiscountCodePutData: action.data,
        promotionDiscountCodePutRequest: false,
        PromotionCRUDError: false,
      };
    case PROMOTION_DISCOUNT_CODE_PUT_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        promotionDiscountCodePutRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    // <editor-fold desc="Promotion Discount Code delete">
    case PROMOTION_DISCOUNT_CODE_DELETE_REQUEST:
      return {
        ...state,
        promotionDiscountCodeDeleteRequest: true,
        PromotionCRUDError: false,
      };
    case PROMOTION_DISCOUNT_CODE_DELETE_SUCCESS:
      return {
        ...state,
        promotionDiscountCodeDeleteData: action.data,
        promotionDiscountCodeDeleteRequest: false,
        PromotionCRUDError: false,
      };
    case PROMOTION_DISCOUNT_CODE_DELETE_FAILURE:
      return {
        ...state,
        PromotionCRUDError: true,
        promotionDiscountCodeDeleteRequest: false,
        PromotionErrorMessage: '',
      };
    // </editor-fold>

    case PROMOTION_ALERT_RESET:
      return {
        ...state,
        promotionErrorMessage: '',
        promotionPostRequest: false,
        promotionCRUDError: false,
      };

    case PROMOTION_ALERT:
      return {
        ...state,
        promotionErrorMessage: action.data,
        promotionPostRequest: false,
        promotionCRUDError: true,
      };
    default:
      return state;
  }
}
