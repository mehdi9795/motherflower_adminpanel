import {
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_REQUEST,
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_SUCCESS,
  VENDOR_BRANCH_PRODUCT_PRICE_LIST_FAILURE,
  VENDOR_BRANCH_PRODUCT_PRICE_PUT_REQUEST,
  VENDOR_BRANCH_PRODUCT_PRICE_PUT_SUCCESS,
  VENDOR_BRANCH__PRODUCT_PRICE_PUT_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  vendorBranchProductPriceRequest: false,
  vendorBranchProductPriceData: getResponseModel,
  vendorBranchProductPriceError: false,

  vendorBranchProductPricePutRequest: false,
  vendorBranchProductPricePutData: getResponseModel,

  vendorBranchProductPriceCRUDError: false,
  vendorBranchProductPriceErrorMessage: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="vendor Branch Product Price List">
    case VENDOR_BRANCH_PRODUCT_PRICE_LIST_REQUEST:
      return {
        ...state,
        vendorBranchProductPriceRequest: true,
        vendorBranchProductPriceError: false,
      };
    case VENDOR_BRANCH_PRODUCT_PRICE_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchProductPriceData: action.data,
        vendorBranchProductPriceRequest: false,
        vendorBranchProductPriceError: false,
      };
    case VENDOR_BRANCH_PRODUCT_PRICE_LIST_FAILURE:
      return {
        ...state,
        vendorBranchProductPriceError: true,
        vendorBranchProductPriceRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="vendor Branch Product Price put">
    case VENDOR_BRANCH_PRODUCT_PRICE_PUT_REQUEST:
      return {
        ...state,
        vendorBranchProductPricePutRequest: true,
        vendorBranchProductPriceCRUDError: false,
      };
    case VENDOR_BRANCH_PRODUCT_PRICE_PUT_SUCCESS:
      return {
        ...state,
        vendorBranchProductPricePutData: action.data,
        vendorBranchProductPricePutRequest: false,
        vendorBranchProductPriceCRUDError: false,
      };
    case VENDOR_BRANCH__PRODUCT_PRICE_PUT_FAILURE:
      return {
        ...state,
        vendorBranchProductPriceCRUDError: true,
        vendorBranchProductPricePutRequest: false,
        vendorBranchProductPriceErrorMessage: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
