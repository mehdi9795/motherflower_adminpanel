import {
  COMMISSION_LIST_REQUEST,
  COMMISSION_LIST_SUCCESS,
  COMMISSION_LIST_FAILURE,
  COMMISSION_PUT_REQUEST,
  COMMISSION_PUT_SUCCESS,
  COMMISSION_PUT_FAILURE,
  COMMISSION_POST_REQUEST,
  COMMISSION_POST_SUCCESS,
  COMMISSION_POST_FAILURE,
  COMMISSION_DELETE_REQUEST,
  COMMISSION_DELETE_SUCCESS,
  COMMISSION_DELETE_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  commissionRequest: false,
  commissionData: getResponseModel,
  commissionError: false,

  commissionPostRequest: false,
  commissionPostData: getResponseModel,

  commissionPutRequest: false,
  commissionPutData: getResponseModel,

  commissionDeleteRequest: false,
  commissionDeleteData: getResponseModel,

  commissionCRUDError: false,
  commissionErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="Commission List">
    case COMMISSION_LIST_REQUEST:
      return {
        ...state,
        commissionRequest: true,
        commissionError: false,
      };
    case COMMISSION_LIST_SUCCESS:
      return {
        ...state,
        commissionData: action.data,
        commissionRequest: false,
        commissionError: false,
      };
    case COMMISSION_LIST_FAILURE:
      return {
        ...state,
        commissionError: true,
        commissionRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Commission post">
    case COMMISSION_POST_REQUEST:
      return {
        ...state,
        commissionPostRequest: true,
        commissionCRUDError: false,
      };
    case COMMISSION_POST_SUCCESS:
      return {
        ...state,
        commissionPostData: action.data,
        commissionPostRequest: false,
        commissionCRUDError: false,
      };
    case COMMISSION_POST_FAILURE:
      return {
        ...state,
        commissionCRUDError: true,
        commissionPostRequest: false,
        commissionErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="Commission put">
    case COMMISSION_PUT_REQUEST:
      return {
        ...state,
        commissionPutRequest: true,
        commissionCRUDError: false,
      };
    case COMMISSION_PUT_SUCCESS:
      return {
        ...state,
        commissionPutData: action.data,
        commissionPutRequest: false,
        commissionCRUDError: false,
      };
    case COMMISSION_PUT_FAILURE:
      return {
        ...state,
        commissionCRUDError: true,
        commissionPutRequest: false,
        commissionErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="Commission delete">
    case COMMISSION_DELETE_REQUEST:
      return {
        ...state,
        commissionDeleteRequest: true,
        commissionCRUDError: false,
      };
    case COMMISSION_DELETE_SUCCESS:
      return {
        ...state,
        commissionDeleteData: action.data,
        commissionDeleteRequest: false,
        commissionCRUDError: false,
      };
    case COMMISSION_DELETE_FAILURE:
      return {
        ...state,
        commissionCRUDError: true,
        commissionDeleteRequest: false,
        commissionErrorMessage: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
