import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import runtime from './runtime/reducer/runtime';

export default combineReducers({
  runtime,
  loadingBar: loadingBarReducer,
  setUserCreateBackUrl: require('./identity/actionReducer/backUrl/setUserCreateBackUrl')
    .reducer,
  userList: require('./identity/actionReducer/user/List').reducer,
  userRelationShipList: require('./identity/actionReducer/user/UserRelationShipList')
    .reducer,
  userEventList: require('./identity/actionReducer/user/UserEventList').reducer,
  userEventTypeList: require('./identity/actionReducer/user/UserEventTypeList')
    .reducer,
  userAppInfoList: require('./identity/actionReducer/user/UserAppInfoList')
    .reducer,
  userRoleList: require('./identity/actionReducer/user/UserRoleList').reducer,
  singleUser: require('./identity/actionReducer/user/SingleUser').reducer,
  singleUserInit: require('./identity/actionReducer/user/SingleUserInit')
    .reducer,
  userAddressList: require('./identity/actionReducer/user/UserAddressList')
    .reducer,
  login: require('./identity/actionReducer/account/Login').reducer,
  logout: require('./identity/actionReducer/account/Logout').reducer,
  signup: require('./identity/actionReducer/account/Signup').reducer,
  role: require('./identity/actionReducer/role/Role').reducer,
  commissionList: require('./sam/actionReducer/commission/List').reducer,
  orderList: require('./sam/actionReducer/order/OrderList').reducer,
  orderSingle: require('./sam/actionReducer/order/SingleOrder').reducer,
  orderStatusList: require('./sam/actionReducer/order/OrderStatusList').reducer,
  orderItemList: require('./sam/actionReducer/order/OrderItemList').reducer,
  vbProductPriceList: require('./sam/actionReducer/vendorBranchProductPrice/List')
    .reducer,
  vbProductPricePut: require('./sam/actionReducer/vendorBranchProductPrice/Put')
    .reducer,
  vbShippingTariffList: require('./sam/actionReducer/vendorBranchShippingTariff/List')
    .reducer,
  vbShippingTariffPromotionList: require('./sam/actionReducer/vendorBranchShippingTariff/VBShippingTariffPromotionList')
    .reducer,
  promotionTypeList: require('./sam/actionReducer/promotion/PromotionTypeList')
    .reducer,
  promotionList: require('./sam/actionReducer/promotion/PromotionList').reducer,
  singlePromotion: require('./sam/actionReducer/promotion/SinglePromotion')
    .reducer,
  vendorBranchPromotionList: require('./sam/actionReducer/promotion/VendorBranchPromotionList')
    .reducer,
  categoryPromotionDetailsList: require('./sam/actionReducer/promotion/CategoryPromotionDetailsList')
    .reducer,
  productPromotionDetailsList: require('./sam/actionReducer/promotion/ProductPromotionDetailsList')
    .reducer,
  promotionDiscountCodeList: require('./sam/actionReducer/promotion/PromotionDiscountCodeList')
    .reducer,
  categoryList: require('./catalog/actionReducer/category/CategoryList')
    .reducer,
  childCategoryList: require('./catalog/actionReducer/category/ChildCategoryList')
    .reducer,
  relatedCategoryList: require('./catalog/actionReducer/category/RelatedCategoryList')
    .reducer,
  singleCategory: require('./catalog/actionReducer/category/SingleCategory')
    .reducer,
  singleCategoryInit: require('./catalog/actionReducer/category/SingleCategoryInit')
    .reducer,
  secondaryLevelList: require('./catalog/actionReducer/category/SecondaryLevelList')
    .reducer,
  colorOptionList: require('./catalog/actionReducer/colorOption/List').reducer,
  inventoryManagementList: require('./catalog/actionReducer/inventoryManagement/List')
    .reducer,
  productTimeList: require('./catalog/actionReducer/productTime/List').reducer,
  productTypesList: require('./catalog/actionReducer/productType/List').reducer,
  productList: require('./catalog/actionReducer/product/ProductList').reducer,
  productNameList: require('./catalog/actionReducer/product/ProductNameList')
    .reducer,
  singleProduct: require('./catalog/actionReducer/product/SingleProduct')
    .reducer,
  singleProductInit: require('./catalog/actionReducer/product/SingleProductInit')
    .reducer,
  productTag: require('./catalog/actionReducer/product/ProductTag').reducer,
  productSpecificationAttributeList: require('./catalog/actionReducer/product/ProductSpecificationAttributeList')
    .reducer,
  specificationAttributeList: require('./catalog/actionReducer/specificationAttribute/List')
    .reducer,
  singleSpecificationAttribute: require('./catalog/actionReducer/specificationAttribute/Single')
    .reducer,
  tagList: require('./catalog/actionReducer/tag/List').reducer,
  relatedTagList: require('./catalog/actionReducer/tag/RelatedTagList').reducer,
  vendorList: require('./vendor/actionReducer/vendor/List').reducer,
  vendorUserList: require('./vendor/actionReducer/vendor/VendorUserList')
    .reducer,
  singleVendorAgent: require('./vendor/actionReducer/vendor/Single').reducer,
  singleVendorInit: require('./vendor/actionReducer/vendor/SingleVendorAgentInit')
    .reducer,
  facilityList: require('./vendor/actionReducer/facility/FacilityList').reducer,
  vendorFacilityList: require('./vendor/actionReducer/facility/VendorFacilityList')
    .reducer,
  vendorBranchList: require('./vendor/actionReducer/vendorBranch/List').reducer,
  singleVendorBranch: require('./vendor/actionReducer/vendorBranch/Single')
    .reducer,
  singleVendorBranchInit: require('./vendor/actionReducer/vendorBranch/SingleInit')
    .reducer,
  vendorBranchOffShiftList: require('./vendor/actionReducer/vendorBranchOffShift/List')
    .reducer,
  vendorBranchUserList: require('./vendor/actionReducer/vendorBranchUser/List')
    .reducer,
  vendorBranchZoneList: require('./vendor/actionReducer/vendorBranchZone/List')
    .reducer,
  bannerList: require('./cms/actionReducer/banner/List').reducer,
  imageList: require('./cms/actionReducer/image/List').reducer,
  ratePost: require('./cms/actionReducer/rate/Post').reducer,
  cityList: require('./common/actionReducer/city/List').reducer,
  singleCity: require('./common/actionReducer/city/Single').reducer,
  singleCityInit: require('./common/actionReducer/city/SingleInit').reducer,
  districtList: require('./common/actionReducer/district/List').reducer,
  districtZoneList: require('./common/actionReducer/district/DistrictZoneList')
    .reducer,
  districtZoneNotContainList: require('./common/actionReducer/district/DistrictZoneNotContainList')
    .reducer,
  publicEventList: require('./common/actionReducer/publicEvent/List').reducer,
  singlePublicEvent: require('./common/actionReducer/publicEvent/Single')
    .reducer,
  singlePublicEventInit: require('./common/actionReducer/publicEvent/SingleInit')
    .reducer,
  zoneList: require('./common/actionReducer/zone/List').reducer,
  singleZone: require('./common/actionReducer/zone/Single').reducer,
  singleZoneInit: require('./common/actionReducer/zone/SingleInit').reducer,
  occasionTypeList: require('./common/actionReducer/occasionType/List').reducer,
  messageList: require('./notification/actionReducer/message/List').reducer,
  messageDetails: require('./notification/actionReducer/message/Details')
    .reducer,
  notificationList: require('./notification/actionReducer/notification/GetNotification')
    .reducer,
  requestTypeList: require('./notification/actionReducer/requestType/List')
    .reducer,
  editFormAlert: require('./shared/actionReducer/alert/alert').reducer,
  editFormLoading: require('./shared/actionReducer/loading/loading').reducer,
  permission: require('./identity/actionReducer/permission/Permission').reducer,
  vendorAffiliateList: require('./vendor/actionReducer/vendorAffiliate/List')
    .reducer,
  singleVendorAffiliate: require('./vendor/actionReducer/vendorAffiliate/Single')
    .reducer,
  pushList: require('./notification/actionReducer/push/List').reducer,
  smsList: require('./notification/actionReducer/sms/List').reducer,
  singleSms: require('./notification/actionReducer/sms/SingleSms').reducer,
  smsStatusList: require('./notification/actionReducer/sms/StatusList').reducer,
  pushStatusList: require('./notification/actionReducer/push/PushStatusList')
    .reducer,
  singlePush: require('./notification/actionReducer/push/SinglePush').reducer,
  pushTestGroupList: require('./notification/actionReducer/push/PushTestGroupList')
    .reducer,
  commentList: require('./cms/actionReducer/comment/List').reducer,
  backUrl: require('./shared/actionReducer/url/backUrl').reducer,
  reportList: require('./common/actionReducer/report/List').reducer,
  reportSettingList: require('./identity/actionReducer/reportSetting/List')
    .reducer,
  invitedUserList: require('./identity/actionReducer/user/InvitedUserList')
    .reducer,
  calendarException: require('./vendor/actionReducer/vendorBranch/CalendarException')
    .reducer,
  availableProductShift: require('./vendor/actionReducer/vendorBranch/AvailableProductShift')
    .reducer,
  userCardNumberList: require('./identity/actionReducer/user/UserCardNumberList')
    .reducer,
  bankList: require('./sam/actionReducer/bank/List').reducer,
  walletList: require('./sam/actionReducer/wallet/List').reducer,
});
