import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  productTimePutRequest: ['dto'],
  productTimePutSuccess: ['data'],
  productTimePutFailure: [],
});

export default Creators;
/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: [],
  error: {},
});

/* ------------- Reducers ------------- */

// we're attempting to fetching
export const request = state => ({
  ...state,
});
// we've successfully fetch LIst items from server
export const success = (state, action) => {
  const { data } = action;
  return {
    ...state,
    data,
    error: {},
  };
};

// we've had a problem in fetching
export const failure = (state, { error }) => ({
  ...state,
  error,
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PRODUCT_TIME_PUT_REQUEST]: request,
  [Types.PRODUCT_TIME_PUT_SUCCESS]: success,
  [Types.PRODUCT_TIME_PUT_FAILURE]: failure,
});

/* ------------- Selectors ------------- */
export const getProductTimetPut = state => state.productTimePut;

// Is the current user logged in?
// export const isLastPage = (listFetchState) => listFetchState.count / pageSize >
