import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  productSpecificationAttributePostRequest: ['dto'],
  productSpecificationAttributePostSuccess: ['data'],
  productSpecificationAttributePostFailure: [],
});

export default Creators;
/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: [],
  error: {},
});

/* ------------- Reducers ------------- */

// we're attempting to fetching
export const request = state => ({
  ...state,
});
// we've successfully fetch LIst items from server
export const success = (state, action) => {
  const { data } = action;
  return {
    ...state,
    data,
    error: {},
  };
};

// we've had a problem in fetching
export const failure = (state, { error }) => ({
  ...state,
  error,
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PRODUCT_SPECIFICATION_ATTRIBUTE_POST_REQUEST]: request,
  [Types.PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS]: success,
  [Types.PRODUCT_SPECIFICATION_ATTRIBUTE_POST_FAILURE]: failure,
});

// Is the current user logged in?
// export const isLastPage = (listFetchState) => listFetchState.count / pageSize >
