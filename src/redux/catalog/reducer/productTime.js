import {
  PRODUCT_TIME_LIST_REQUEST,
  PRODUCT_TIME_LIST_SUCCESS,
  PRODUCT_TIME_LIST_FAILURE,
  PRODUCT_TIME_PUT_REQUEST,
  PRODUCT_TIME_PUT_SUCCESS,
  PRODUCT_TIME_PUT_FAILURE,
} from '../../../constants';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  productTimeListRequest: false,
  productTimeListData: getResponseModel,
  productTimeListFailure: false,

  productTimePutRequest: false,
  productTimePutData: getResponseModel,

  productCRUDError: false,
  productErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="product time list">
    case PRODUCT_TIME_LIST_REQUEST:
      return {
        ...state,
        productTimeListRequest: true,
        productTimeListFailure: false,
      };
    case PRODUCT_TIME_LIST_SUCCESS:
      return {
        ...state,
        productTimeListData: action.data,
        productTimeListRequest: false,
        productTimeListFailure: false,
      };
    case PRODUCT_TIME_LIST_FAILURE:
      return {
        ...state,
        productTimeListFailure: true,
        productTimeListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="product time put">
    case PRODUCT_TIME_PUT_REQUEST:
      return {
        ...state,
        productTimePutRequest: true,
        productCRUDError: false,
      };
    case PRODUCT_TIME_PUT_SUCCESS:
      return {
        ...state,
        productTimePutData: action.data,
        productTimePutRequest: false,
        productCRUDError: false,
      };
    case PRODUCT_TIME_PUT_FAILURE:
      return {
        ...state,
        productCRUDError: true,
        productTimePutRequest: false,
        productErrorMessage: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
