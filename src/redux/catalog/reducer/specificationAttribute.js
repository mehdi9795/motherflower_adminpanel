import {
  SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
  SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
  SPECIFICATION_ATTRIBUTE_LIST_FAILURE,
  SINGLE_SPECIFICATION_ATTRIBUTE_REQUEST,
  SINGLE_SPECIFICATION_ATTRIBUTE_SUCCESS,
  SINGLE_SPECIFICATION_ATTRIBUTE_FAILURE,
  SPECIFICATION_ATTRIBUTE_POST_REQUEST,
  SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  SPECIFICATION_ATTRIBUTE_POST_FAILURE,
  SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
  SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
  SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  specificationAttributeListRequest: false,
  specificationAttributeListData: getResponseModel,
  specificationAttributeListFailure: false,

  singleSpecificationAttributeRequest: false,
  singleSpecificationAttributeData: getResponseModel,
  singleSpecificationAttributeFailure: false,

  specificationAttributePostRequest: false,
  specificationAttributePostData: getResponseModel,

  specificationAttributePutRequest: false,
  specificationAttributePutData: getResponseModel,

  specificationAttributeDeleteRequest: false,
  specificationAttributeDeleteData: getResponseModel,

  imageSpecificationAttributeDeleteRequest: false,
  imageSpecificationAttributeDeleteData: getResponseModel,

  specificationAttributeCRUDError: false,
  specificationAttributeErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="specification attribute list">
    case SPECIFICATION_ATTRIBUTE_LIST_REQUEST:
      return {
        ...state,
        specificationAttributeListRequest: true,
        specificationAttributeListFailure: false,
      };
    case SPECIFICATION_ATTRIBUTE_LIST_SUCCESS:
      return {
        ...state,
        specificationAttributeListData: action.data,
        specificationAttributeListRequest: false,
        specificationAttributeListFailure: false,
      };
    case SPECIFICATION_ATTRIBUTE_LIST_FAILURE:
      return {
        ...state,
        specificationAttributeListFailure: true,
        specificationAttributeListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="specification attribute single">
    case SINGLE_SPECIFICATION_ATTRIBUTE_REQUEST:
      return {
        ...state,
        singleSpecificationAttributeRequest: true,
        singleSpecificationAttributeFailure: false,
      };
    case SINGLE_SPECIFICATION_ATTRIBUTE_SUCCESS:
      return {
        ...state,
        singleSpecificationAttributeData: action.data,
        singleSpecificationAttributeRequest: false,
        singleSpecificationAttributeFailure: false,
      };
    case SINGLE_SPECIFICATION_ATTRIBUTE_FAILURE:
      return {
        ...state,
        singleSpecificationAttributeFailure: true,
        singleSpecificationAttributeRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="specificationAttribute post">
    case SPECIFICATION_ATTRIBUTE_POST_REQUEST:
      return {
        ...state,
        specificationAttributePostRequest: true,
        specificationAttributeCRUDError: false,
      };
    case SPECIFICATION_ATTRIBUTE_POST_SUCCESS:
      return {
        ...state,
        specificationAttributePostData: action.data,
        specificationAttributePostRequest: false,
        specificationAttributeCRUDError: false,
      };
    case SPECIFICATION_ATTRIBUTE_POST_FAILURE:
      return {
        ...state,
        specificationAttributeCRUDError: true,
        specificationAttributePostRequest: false,
        specificationAttributeErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="specificationAttribute put">
    case SPECIFICATION_ATTRIBUTE_PUT_REQUEST:
      return {
        ...state,
        specificationAttributePutRequest: true,
        specificationAttributeCRUDError: false,
      };
    case SPECIFICATION_ATTRIBUTE_PUT_SUCCESS:
      return {
        ...state,
        specificationAttributePutData: action.data,
        specificationAttributePutRequest: false,
        specificationAttributeCRUDError: false,
      };
    case SPECIFICATION_ATTRIBUTE_PUT_FAILURE:
      return {
        ...state,
        specificationAttributeCRUDError: true,
        specificationAttributePutRequest: false,
        specificationAttributeErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="specificationAttribute delete">
    case SPECIFICATION_ATTRIBUTE_DELETE_REQUEST:
      return {
        ...state,
        specificationAttributeDeleteRequest: true,
        specificationAttributeCRUDError: false,
      };
    case SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS:
      return {
        ...state,
        specificationAttributeDeleteData: action.data,
        specificationAttributeDeleteRequest: false,
        specificationAttributeCRUDError: false,
      };
    case SPECIFICATION_ATTRIBUTE_DELETE_FAILURE:
      return {
        ...state,
        specificationAttributeErrorMessage: action.data,
        specificationAttributeCRUDError: true,
        specificationAttributeDeleteRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="specificationAttribute delete">
    case IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST:
      return {
        ...state,
        imageSpecificationAttributeDeleteRequest: true,
        imageSpecificationAttributeCRUDError: false,
      };
    case IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS:
      return {
        ...state,
        imageSDeleteData: action.data,
        imageSDeleteRequest: false,
        imageSCRUDError: false,
      };
    case IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE:
      return {
        ...state,
        imageSErrorMessage: action.data,
        imageSCRUDError: true,
        imageSDeleteRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
