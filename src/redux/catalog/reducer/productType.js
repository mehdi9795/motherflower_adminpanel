import {
  PRODUCT_TYPES_LIST_REQUEST,
  PRODUCT_TYPES_LIST_SUCCESS,
  PRODUCT_TYPES_LIST_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  productTypesListRequest: false,
  productTypesListData: getResponseModel,
  productTypesListFailure: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Product Type">
    case PRODUCT_TYPES_LIST_REQUEST:
      return {
        ...state,
        productTypesListRequest: true,
        productTypesListFailure: false,
      };
    case PRODUCT_TYPES_LIST_SUCCESS:
      return {
        ...state,
        productTypesListData: action.data,
        productTypesListRequest: false,
        productTypesListFailure: false,
      };
    case PRODUCT_TYPES_LIST_FAILURE:
      return {
        ...state,
        productTypesListFailure: true,
        productTypesListRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
