import {
  SECONDARY_LEVEL_LIST_REQUEST,
  SECONDARY_LEVEL_LIST_SUCCESS,
  SECONDARY_LEVEL_LIST_FAILURE,
  CHILD_CATEGORY_LIST_REQUEST,
  CHILD_CATEGORY_LIST_SUCCESS,
  CHILD_CATEGORY_LIST_FAILURE,
  CATEGORY_LIST_REQUEST,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAILURE,
  SINGLE_CATEGORY_REQUEST,
  SINGLE_CATEGORY_SUCCESS,
  SINGLE_CATEGORY_FAILURE,
  SINGLE_CATEGORY_INIT_REQUEST,
  SINGLE_CATEGORY_INIT_SUCCESS,
  SINGLE_CATEGORY_INIT_FAILURE,
  RELATED_CATEGORY_LIST_FAILURE,
  RELATED_CATEGORY_LIST_REQUEST,
  RELATED_CATEGORY_LIST_SUCCESS,
  CATEGORY_POST_REQUEST,
  CATEGORY_POST_SUCCESS,
  CATEGORY_POST_FAILURE,
  CATEGORY_PUT_REQUEST,
  CATEGORY_PUT_SUCCESS,
  CATEGORY_PUT_FAILURE,
  CATEGORY_DELETE_REQUEST,
  CATEGORY_DELETE_SUCCESS,
  CATEGORY_DELETE_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  categoryLoading: false,
  categoryData: getResponseModel,
  categoryError: false,

  categoryListRequest: false,
  categoryListData: getResponseModel,
  categoryListFailure: false,

  categoryChildLoading: false,
  categoryChildData: getResponseModel,
  categoryChildError: false,

  childCategoryListRequest: false,
  childCategoryListData: getResponseModel,
  childCategoryListFailure: false,

  categorySecondaryLevelListRequest: false,
  categorySecondaryLevelListData: getResponseModel,
  categorySecondaryLevelListError: false,

  singleCategoryRequest: false,
  singleCategoryData: getResponseModel,
  singleCategoryError: false,

  relatedCategoryListLoading: false,
  relatedCategoryListData: getResponseModel,
  relatedCategoryListError: false,

  singleCategoryInitRequest: false,
  singleCategoryInitError: false,

  categoryPostRequest: false,
  categoryPostData: getResponseModel,

  categoryPutRequest: false,
  categoryPutData: getResponseModel,

  categoryDeleteRequest: false,
  categoryDeleteData: getResponseModel,

  categoryCRUDError: false,
  categoryErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Category new">
    case CATEGORY_LIST_REQUEST:
      return {
        ...state,
        categoryListRequest: true,
        categoryListFailure: false,
      };
    case CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        categoryListData: action.data,
        categoryListRequest: false,
        categoryListFailure: false,
      };
    case CATEGORY_LIST_FAILURE:
      return {
        ...state,
        categoryListFailure: true,
        categoryListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Child Category new">
    case CHILD_CATEGORY_LIST_REQUEST:
      return {
        ...state,
        childCategoryListRequest: true,
        childCategoryListFailure: false,
      };
    case CHILD_CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        childCategoryListData: action.data,
        childCategoryListRequest: false,
        childCategoryListFailure: false,
      };
    case CHILD_CATEGORY_LIST_FAILURE:
      return {
        ...state,
        childCategoryListFailure: true,
        childCategoryListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="secondary level Category List">
    case SECONDARY_LEVEL_LIST_REQUEST:
      return {
        ...state,
        categorySecondaryLevelListRequest: true,
      };
    case SECONDARY_LEVEL_LIST_SUCCESS:
      return {
        ...state,
        categorySecondaryLevelListRequest: false,
        categorySecondaryLevelListData: action.data,
      };
    case SECONDARY_LEVEL_LIST_FAILURE:
      return {
        ...state,
        categorySecondaryLevelListRequest: false,
        categorySecondaryLevelListError: true,
      };
    // </editor-fold>

    // <editor-fold dsc="Single Category ">
    case SINGLE_CATEGORY_REQUEST:
      return {
        ...state,
        singleCategoryRequest: true,
      };
    case SINGLE_CATEGORY_SUCCESS:
      return {
        ...state,
        singleCategoryRequest: false,
        singleCategoryData: action.data,
      };
    case SINGLE_CATEGORY_FAILURE:
      return {
        ...state,
        singleCategoryRequest: false,
        singleCategoryError: true,
      };
    // </editor-fold>

    // <editor-fold dsc="Single Category init">
    case SINGLE_CATEGORY_INIT_REQUEST:
      return {
        ...state,
        singleCategoryInitRequest: true,
      };
    case SINGLE_CATEGORY_INIT_SUCCESS:
      return {
        ...state,
        singleCategoryInitRequest: false,
      };
    case SINGLE_CATEGORY_INIT_FAILURE:
      return {
        ...state,
        singleCategoryInitRequest: false,
        singleCategoryInitError: true,
      };
    // </editor-fold>

    // <editor-fold dsc="related category List">
    case RELATED_CATEGORY_LIST_REQUEST:
      return {
        ...state,
        relatedCategoryListRequest: true,
        relatedCategoryListFailure: false,
      };
    case RELATED_CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        relatedCategoryListData: action.data,
        relatedCategoryListRequest: false,
        relatedCategoryListFailure: false,
      };
    case RELATED_CATEGORY_LIST_FAILURE:
      return {
        ...state,
        relatedCategoryListFailure: true,
        relatedCategoryListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Category post">
    case CATEGORY_POST_REQUEST:
      return {
        ...state,
        categoryPostRequest: true,
        categoryCRUDError: false,
      };
    case CATEGORY_POST_SUCCESS:
      return {
        ...state,
        categoryPostData: action.data,
        categoryPostRequest: false,
        categoryCRUDError: false,
      };
    case CATEGORY_POST_FAILURE:
      return {
        ...state,
        categoryCRUDError: true,
        categoryPostRequest: false,
        categoryErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Category put">
    case CATEGORY_PUT_REQUEST:
      return {
        ...state,
        categoryPutRequest: true,
        categoryCRUDError: false,
      };
    case CATEGORY_PUT_SUCCESS:
      return {
        ...state,
        categoryPutData: action.data,
        categoryPutRequest: false,
        categoryCRUDError: false,
      };
    case CATEGORY_PUT_FAILURE:
      return {
        ...state,
        categoryCRUDError: true,
        categoryPutRequest: false,
        categoryErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Category delete">
    case CATEGORY_DELETE_REQUEST:
      return {
        ...state,
        categoryDeleteRequest: true,
        categoryCRUDError: false,
      };
    case CATEGORY_DELETE_SUCCESS:
      return {
        ...state,
        categoryDeleteData: action.data,
        categoryDeleteRequest: false,
        categoryCRUDError: false,
      };
    case CATEGORY_DELETE_FAILURE:
      return {
        ...state,
        categoryErrorMessage: action.data,
        categoryCRUDError: true,
        categoryDeleteRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
