import {
  TAG_LIST_REQUEST,
  TAG_LIST_SUCCESS,
  TAG_LIST_FAILURE,
  RELATED_TAG_LIST_FAILURE,
  RELATED_TAG_LIST_REQUEST,
  RELATED_TAG_LIST_SUCCESS,
  TAG_POST_REQUEST,
  TAG_POST_SUCCESS,
  TAG_POST_FAILURE,
  TAG_PUT_REQUEST,
  TAG_PUT_SUCCESS,
  TAG_PUT_FAILURE,
  TAG_DELETE_REQUEST,
  TAG_DELETE_SUCCESS,
  TAG_DELETE_FAILURE,
  IMAGE_TAG_DELETE_REQUEST,
  IMAGE_TAG_DELETE_SUCCESS,
  IMAGE_TAG_DELETE_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  tagListRequest: false,
  tagListData: getResponseModel,
  tagListFailure: false,

  relatedTagListLoading: false,
  relatedTagListData: getResponseModel,
  relatedTagListError: false,

  tagPostRequest: false,
  tagPostData: getResponseModel,

  tagPutRequest: false,
  tagPutData: getResponseModel,

  tagDeleteRequest: false,
  tagDeleteData: getResponseModel,

  imageTagDeleteRequest: false,
  imageTagDeleteData: getResponseModel,

  tagCRUDError: false,
  tagErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="tag list">
    case TAG_LIST_REQUEST:
      return {
        ...state,
        tagListRequest: true,
        tagListFailure: false,
      };
    case TAG_LIST_SUCCESS:
      return {
        ...state,
        tagListData: action.data,
        tagListRequest: false,
        tagListFailure: false,
      };
    case TAG_LIST_FAILURE:
      return {
        ...state,
        tagListFailure: true,
        tagListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="related tag List">
    case RELATED_TAG_LIST_REQUEST:
      return {
        ...state,
        relatedTagListRequest: true,
        relatedTagListFailure: false,
      };
    case RELATED_TAG_LIST_SUCCESS:
      return {
        ...state,
        relatedTagListData: action.data,
        relatedTagListRequest: false,
        relatedTagListFailure: false,
      };
    case RELATED_TAG_LIST_FAILURE:
      return {
        ...state,
        relatedTagListFailure: true,
        relatedTagListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="tag post">
    case TAG_POST_REQUEST:
      return {
        ...state,
        tagPostRequest: true,
        tagCRUDError: false,
      };
    case TAG_POST_SUCCESS:
      return {
        ...state,
        tagPostData: action.data,
        tagPostRequest: false,
        tagCRUDError: false,
      };
    case TAG_POST_FAILURE:
      return {
        ...state,
        tagCRUDError: true,
        tagPostRequest: false,
        tagErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="tag put">
    case TAG_PUT_REQUEST:
      return {
        ...state,
        tagPutRequest: true,
        tagCRUDError: false,
      };
    case TAG_PUT_SUCCESS:
      return {
        ...state,
        tagPutData: action.data,
        tagPutRequest: false,
        tagCRUDError: false,
      };
    case TAG_PUT_FAILURE:
      return {
        ...state,
        tagCRUDError: true,
        tagPutRequest: false,
        tagErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="tag delete">
    case TAG_DELETE_REQUEST:
      return {
        ...state,
        tagDeleteRequest: true,
        tagCRUDError: false,
      };
    case TAG_DELETE_SUCCESS:
      return {
        ...state,
        tagDeleteData: action.data,
        tagDeleteRequest: false,
        tagCRUDError: false,
      };
    case TAG_DELETE_FAILURE:
      return {
        ...state,
        tagErrorMessage: action.data,
        tagCRUDError: true,
        tagDeleteRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="image tag delete">
    case IMAGE_TAG_DELETE_REQUEST:
      return {
        ...state,
        imageTagDeleteRequest: true,
        tagCRUDError: false,
      };
    case IMAGE_TAG_DELETE_SUCCESS:
      return {
        ...state,
        imageTagDeleteData: action.data,
        imageTagDeleteRequest: false,
        tagCRUDError: false,
      };
    case IMAGE_TAG_DELETE_FAILURE:
      return {
        ...state,
        tagErrorMessage: action.data,
        tagCRUDError: true,
        imageTagDeleteRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
