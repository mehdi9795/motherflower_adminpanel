import {
  INVENTORY_MANAGEMENT_LIST_REQUEST,
  INVENTORY_MANAGEMENT_LIST_SUCCESS,
  INVENTORY_MANAGEMENT_LIST_FAILURE,
  INVENTORY_MANAGEMENT_POST_REQUEST,
  INVENTORY_MANAGEMENT_POST_SUCCESS,
  INVENTORY_MANAGEMENT_POST_FAILURE,
  INVENTORY_MANAGEMENT_PUT_REQUEST,
  INVENTORY_MANAGEMENT_PUT_SUCCESS,
  INVENTORY_MANAGEMENT_PUT_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  inventoryManagementListRequest: false,
  inventoryManagementListData: getResponseModel,
  inventoryManagementListFailure: false,

  inventoryManagementPostRequest: false,
  inventoryManagementPostData: getResponseModel,

  inventoryManagementPutRequest: false,
  inventoryManagementPutData: getResponseModel,

  inventoryManagementCRUDError: false,
  inventoryManagementErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="color Option list">
    case INVENTORY_MANAGEMENT_LIST_REQUEST:
      return {
        ...state,
        inventoryManagementListRequest: true,
        inventoryManagementListFailure: false,
      };
    case INVENTORY_MANAGEMENT_LIST_SUCCESS:
      return {
        ...state,
        inventoryManagementListData: action.data,
        inventoryManagementListRequest: false,
        inventoryManagementListFailure: false,
      };
    case INVENTORY_MANAGEMENT_LIST_FAILURE:
      return {
        ...state,
        inventoryManagementListFailure: true,
        inventoryManagementListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="inventoryManagement post">
    case INVENTORY_MANAGEMENT_POST_REQUEST:
      return {
        ...state,
        inventoryManagementPostRequest: true,
        inventoryManagementCRUDError: false,
      };
    case INVENTORY_MANAGEMENT_POST_SUCCESS:
      return {
        ...state,
        inventoryManagementPostData: action.data,
        inventoryManagementPostRequest: false,
        inventoryManagementCRUDError: false,
      };
    case INVENTORY_MANAGEMENT_POST_FAILURE:
      return {
        ...state,
        inventoryManagementCRUDError: true,
        inventoryManagementPostRequest: false,
        inventoryManagementErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="inventoryManagement put">
    case INVENTORY_MANAGEMENT_PUT_REQUEST:
      return {
        ...state,
        inventoryManagementPutRequest: true,
        inventoryManagementCRUDError: false,
      };
    case INVENTORY_MANAGEMENT_PUT_SUCCESS:
      return {
        ...state,
        inventoryManagementPutData: action.data,
        inventoryManagementPutRequest: false,
        inventoryManagementCRUDError: false,
      };
    case INVENTORY_MANAGEMENT_PUT_FAILURE:
      return {
        ...state,
        inventoryManagementCRUDError: true,
        inventoryManagementPutRequest: false,
        inventoryManagementErrorMessage: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
