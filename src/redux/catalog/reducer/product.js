import {
  PRODUCT_LIST_LOADING,
  PRODUCT_LIST_IS_LOADED,
  PRODUCT_LIST_IS_FAILURE,
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS,
  PRODUCT_LIST_FAILURE,
  PRODUCT_TYPES_LIST_REQUEST,
  PRODUCT_TYPES_LIST_SUCCESS,
  PRODUCT_TYPES_LIST_FAILURE,
  PRODUCT_TYPES_LIST_LOADING,
  PRODUCT_TYPES_LIST_IS_LOADED,
  PRODUCT_TYPES_LIST_IS_FAILURE,
  SINGLE_PRODUCT_REQUEST,
  SINGLE_PRODUCT_SUCCESS,
  SINGLE_PRODUCT_FAILURE,
  SINGLE_PRODUCT_INIT_REQUEST,
  SINGLE_PRODUCT_INIT_SUCCESS,
  SINGLE_PRODUCT_INIT_FAILURE,
  PRODUCT_POST_REQUEST,
  PRODUCT_POST_SUCCESS,
  PRODUCT_POST_FAILURE,
  PRODUCT_PUT_REQUEST,
  PRODUCT_PUT_SUCCESS,
  PRODUCT_PUT_FAILURE,
  PRODUCT_DELETE_REQUEST,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_DELETE_FAILURE,
  PRODUCT_ALERT,
  PRODUCT_ALERT_RESET,
  PRODUCT_TAG_REQUEST,
  PRODUCT_TAG_SUCCESS,
  PRODUCT_TAG_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  PRODUCT_NAME_LIST_REQUEST,
  PRODUCT_NAME_LIST_SUCCESS,
  PRODUCT_NAME_LIST_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  productLoading: false,
  productData: getResponseModel,
  productError: false,

  productTypeLoading: false,
  productTypeData: getResponseModel,
  productTypeError: false,

  productListRequest: false,
  productListData: getResponseModel,
  productListFailure: false,

  productNameLoading: false,
  productNameData: getResponseModel,
  productNameError: false,

  productTypesListRequest: false,
  productTypesListData: getResponseModel,
  productTypesListFailure: false,

  singleProductRequest: false,
  singleProductData: getResponseModel,
  singleProductError: false,

  singleProductInitRequest: false,
  singleProductInitData: getResponseModel,
  singleProductInitError: false,

  productPostRequest: false,
  productPostData: getResponseModel,

  productPutRequest: false,
  productPutData: getResponseModel,

  productDeleteRequest: false,
  productDeleteData: getResponseModel,

  productTagListRequest: false,
  productTagListData: getResponseModel,
  productTagListFailure: false,

  productSpecificationAttributeListRequest: false,
  productSpecificationAttributeListData: getResponseModel,
  productSpecificationAttributeListFailure: false,

  productSpecificationAttributePostRequest: false,
  productSpecificationAttributePostData: getResponseModel,

  productSpecificationAttributePutRequest: false,
  productSpecificationAttributePutData: getResponseModel,

  productSpecificationAttributeDeleteRequest: false,
  productSpecificationAttributeDeleteData: getResponseModel,

  productCRUDError: false,
  productErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Product List old">
    case PRODUCT_LIST_LOADING:
      return {
        ...state,
        productLoading: true,
        productError: false,
      };
    case PRODUCT_LIST_IS_LOADED:
      return {
        ...state,
        productData: action.payload.data,
        productLoading: false,
        productError: false,
      };
    case PRODUCT_LIST_IS_FAILURE:
      return {
        ...state,
        productError: true,
        productLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product Type List old">
    case PRODUCT_TYPES_LIST_LOADING:
      return {
        ...state,
        productTypeLoading: true,
        productTypeError: false,
      };
    case PRODUCT_TYPES_LIST_IS_LOADED:
      return {
        ...state,
        productTypeData: action.payload.data,
        productTypeLoading: false,
        productTypeError: false,
      };
    case PRODUCT_TYPES_LIST_IS_FAILURE:
      return {
        ...state,
        productTypeError: true,
        productTypeLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product List new">
    case PRODUCT_LIST_REQUEST:
      return {
        ...state,
        productListRequest: true,
        productListFailure: false,
      };
    case PRODUCT_LIST_SUCCESS:
      return {
        ...state,
        productListData: action.data,
        productListRequest: false,
        productListFailure: false,
      };
    case PRODUCT_LIST_FAILURE:
      return {
        ...state,
        productListFailure: true,
        productListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product name List">
    case PRODUCT_NAME_LIST_REQUEST:
      return {
        ...state,
        productNameListRequest: true,
        productNameListFailure: false,
      };
    case PRODUCT_NAME_LIST_SUCCESS:
      return {
        ...state,
        productNameListData: action.data,
        productNameListRequest: false,
        productNameListFailure: false,
      };
    case PRODUCT_NAME_LIST_FAILURE:
      return {
        ...state,
        productNameListFailure: true,
        productNameListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product Type new">
    case PRODUCT_TYPES_LIST_REQUEST:
      return {
        ...state,
        productTypesListRequest: true,
        productTypesListFailure: false,
      };
    case PRODUCT_TYPES_LIST_SUCCESS:
      return {
        ...state,
        productTypesListData: action.data,
        productTypesListRequest: false,
        productTypesListFailure: false,
      };
    case PRODUCT_TYPES_LIST_FAILURE:
      return {
        ...state,
        productTypesListFailure: true,
        productTypesListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Single Product">
    case SINGLE_PRODUCT_REQUEST:
      return {
        ...state,
        singleProductRequest: true,
        singleProductFailure: false,
      };
    case SINGLE_PRODUCT_SUCCESS:
      return {
        ...state,
        singleProductData: action.data,
        singleProductRequest: false,
        singleProductFailure: false,
      };
    case SINGLE_PRODUCT_FAILURE:
      return {
        ...state,
        singleProductFailure: true,
        singleProductRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Single Product Init">
    case SINGLE_PRODUCT_INIT_REQUEST:
      return {
        ...state,
        singleProductInitRequest: true,
        singleProductInitFailure: false,
      };
    case SINGLE_PRODUCT_INIT_SUCCESS:
      return {
        ...state,
        singleProductInitData: action.data,
        singleProductInitRequest: false,
        singleProductInitFailure: false,
      };
    case SINGLE_PRODUCT_INIT_FAILURE:
      return {
        ...state,
        singleProductInitFailure: true,
        singleProductInitRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product post">
    case PRODUCT_POST_REQUEST:
      return {
        ...state,
        productPostRequest: true,
        productCRUDError: false,
      };
    case PRODUCT_POST_SUCCESS:
      return {
        ...state,
        productPostData: action.data,
        productPostRequest: false,
        productCRUDError: false,
      };
    case PRODUCT_POST_FAILURE:
      return {
        ...state,
        productCRUDError: true,
        productPostRequest: false,
        productErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Product put">
    case PRODUCT_PUT_REQUEST:
      return {
        ...state,
        productPutRequest: true,
        productCRUDError: false,
      };
    case PRODUCT_PUT_SUCCESS:
      return {
        ...state,
        productPutData: action.data,
        productPutRequest: false,
        productCRUDError: false,
      };
    case PRODUCT_PUT_FAILURE:
      return {
        ...state,
        productCRUDError: true,
        productPutRequest: false,
        productErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Product delete">
    case PRODUCT_DELETE_REQUEST:
      return {
        ...state,
        productDeleteRequest: true,
        productCRUDError: false,
      };
    case PRODUCT_DELETE_SUCCESS:
      return {
        ...state,
        productDeleteData: action.data,
        productDeleteRequest: false,
        productCRUDError: false,
      };
    case PRODUCT_DELETE_FAILURE:
      return {
        ...state,
        productErrorMessage: action.data,
        productCRUDError: true,
        productDeleteRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product Tag">
    case PRODUCT_TAG_REQUEST:
      return {
        ...state,
        productTagListRequest: true,
        productTagListFailure: false,
      };
    case PRODUCT_TAG_SUCCESS:
      return {
        ...state,
        productTagListData: action.data,
        productTagListRequest: false,
        productTagListFailure: false,
      };
    case PRODUCT_TAG_FAILURE:
      return {
        ...state,
        productTagListFailure: true,
        productTagListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product Specification Attribute List">
    case PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST:
      return {
        ...state,
        productSpecificationAttributeListRequest: true,
        productSpecificationAttributeListFailure: false,
      };
    case PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_SUCCESS:
      return {
        ...state,
        productSpecificationAttributeListData: action.data,
        productSpecificationAttributeListRequest: false,
        productSpecificationAttributeListFailure: false,
      };
    case PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_FAILURE:
      return {
        ...state,
        productSpecificationAttributeListFailure: true,
        productSpecificationAttributeListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product Specification Attribute post">
    case PRODUCT_SPECIFICATION_ATTRIBUTE_POST_REQUEST:
      return {
        ...state,
        productSpecificationAttributePostRequest: true,
        productCRUDError: false,
      };
    case PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS:
      return {
        ...state,
        productSpecificationAttributePostData: action.data,
        productSpecificationAttributePostRequest: false,
        productCRUDError: false,
      };
    case PRODUCT_SPECIFICATION_ATTRIBUTE_POST_FAILURE:
      return {
        ...state,
        productCRUDError: true,
        productSpecificationAttributePostRequest: false,
        productErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Product Specification Attribute put">
    case PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_REQUEST:
      return {
        ...state,
        productSpecificationAttributePutRequest: true,
        productCRUDError: false,
      };
    case PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_SUCCESS:
      return {
        ...state,
        productSpecificationAttributePutData: action.data,
        productSpecificationAttributePutRequest: false,
        productCRUDError: false,
      };
    case PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_FAILURE:
      return {
        ...state,
        productCRUDError: true,
        productSpecificationAttributePutRequest: false,
        productErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Product Specification Attribute delete">
    case PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST:
      return {
        ...state,
        productSpecificationAttributeDeleteRequest: true,
        productCRUDError: false,
      };
    case PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS:
      return {
        ...state,
        productSpecificationAttributeDeleteData: action.data,
        productSpecificationAttributeDeleteRequest: false,
        productCRUDError: false,
      };
    case PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE:
      return {
        ...state,
        productCRUDError: true,
        productSpecificationAttributeDeleteRequest: false,
        productErrorMessage: action.data,
      };
    // </editor-fold>

    case PRODUCT_ALERT_RESET:
      return {
        ...state,
        productErrorMessage: '',
        productPostRequest: false,
        productCRUDError: false,
      };

    case PRODUCT_ALERT:
      return {
        ...state,
        productErrorMessage: action.data,
        productPostRequest: false,
        productCRUDError: true,
      };
    default:
      return state;
  }
}
