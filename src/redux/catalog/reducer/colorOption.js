import {
  COLOR_OPTION_LIST_REQUEST,
  COLOR_OPTION_LIST_SUCCESS,
  COLOR_OPTION_LIST_FAILURE,
  COLOR_OPTION_POST_REQUEST,
  COLOR_OPTION_POST_SUCCESS,
  COLOR_OPTION_POST_FAILURE,
  COLOR_OPTION_PUT_REQUEST,
  COLOR_OPTION_PUT_SUCCESS,
  COLOR_OPTION_PUT_FAILURE,
  COLOR_OPTION_DELETE_REQUEST,
  COLOR_OPTION_DELETE_SUCCESS,
  COLOR_OPTION_DELETE_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  colorOptionListRequest: false,
  colorOptionListData: getResponseModel,
  colorOptionListFailure: false,

  colorOptionPostRequest: false,
  colorOptionPostData: getResponseModel,

  colorOptionPutRequest: false,
  colorOptionPutData: getResponseModel,

  colorOptionDeleteRequest: false,
  colorOptionDeleteData: getResponseModel,

  colorOptionCRUDError: false,
  colorOptionErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="color Option list">
    case COLOR_OPTION_LIST_REQUEST:
      return {
        ...state,
        colorOptionListRequest: true,
        colorOptionListFailure: false,
      };
    case COLOR_OPTION_LIST_SUCCESS:
      return {
        ...state,
        colorOptionListData: action.data,
        colorOptionListRequest: false,
        colorOptionListFailure: false,
      };
    case COLOR_OPTION_LIST_FAILURE:
      return {
        ...state,
        colorOptionListFailure: true,
        colorOptionListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="colorOption post">
    case COLOR_OPTION_POST_REQUEST:
      return {
        ...state,
        colorOptionPostRequest: true,
        colorOptionCRUDError: false,
      };
    case COLOR_OPTION_POST_SUCCESS:
      return {
        ...state,
        colorOptionPostData: action.data,
        colorOptionPostRequest: false,
        colorOptionCRUDError: false,
      };
    case COLOR_OPTION_POST_FAILURE:
      return {
        ...state,
        colorOptionCRUDError: true,
        colorOptionPostRequest: false,
        colorOptionErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="colorOption put">
    case COLOR_OPTION_PUT_REQUEST:
      return {
        ...state,
        colorOptionPutRequest: true,
        colorOptionCRUDError: false,
      };
    case COLOR_OPTION_PUT_SUCCESS:
      return {
        ...state,
        colorOptionPutData: action.data,
        colorOptionPutRequest: false,
        colorOptionCRUDError: false,
      };
    case COLOR_OPTION_PUT_FAILURE:
      return {
        ...state,
        colorOptionCRUDError: true,
        colorOptionPutRequest: false,
        colorOptionErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="colorOption Delete">
    case COLOR_OPTION_DELETE_REQUEST:
      return {
        ...state,
        colorOptionDeleteRequest: true,
        colorOptionCRUDError: false,
      };
    case COLOR_OPTION_DELETE_SUCCESS:
      return {
        ...state,
        colorOptionDeleteData: action.data,
        colorOptionDeleteRequest: false,
        colorOptionCRUDError: false,
      };
    case COLOR_OPTION_DELETE_FAILURE:
      return {
        ...state,
        colorOptionCRUDError: true,
        colorOptionDeleteRequest: false,
        colorOptionErrorMessage: action.data,
      };
    // </editor-fold>
    default:
      return state;
  }
}
