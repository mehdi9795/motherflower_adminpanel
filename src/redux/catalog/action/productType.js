import {
  PRODUCT_TYPES_LIST_REQUEST,
  PRODUCT_TYPES_LIST_SUCCESS,
  PRODUCT_TYPES_LIST_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="Product Type">
export function productTypesListRequest(data) {
  return {
    type: PRODUCT_TYPES_LIST_REQUEST,
    data,
  };
}

export function productTypesLIstSuccess(data) {
  return {
    type: PRODUCT_TYPES_LIST_SUCCESS,
    data,
  };
}

export function productTypesListFailure() {
  return {
    type: PRODUCT_TYPES_LIST_FAILURE,
  };
}
// </editor-fold>
