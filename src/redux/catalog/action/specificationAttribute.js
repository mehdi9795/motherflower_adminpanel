import {
  SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
  SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
  SPECIFICATION_ATTRIBUTE_LIST_FAILURE,
  SINGLE_SPECIFICATION_ATTRIBUTE_REQUEST,
  SINGLE_SPECIFICATION_ATTRIBUTE_SUCCESS,
  SINGLE_SPECIFICATION_ATTRIBUTE_FAILURE,
  SPECIFICATION_ATTRIBUTE_POST_REQUEST,
  SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  SPECIFICATION_ATTRIBUTE_POST_FAILURE,
  SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
  SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
  SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="specification Attribute list">
export function specificationAttributeListRequest(data) {
  return {
    type: SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    data,
  };
}

export function specificationAttributeLIstSuccess(data) {
  return {
    type: SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
    data,
  };
}

export function specificationAttributeListFailure() {
  return {
    type: SPECIFICATION_ATTRIBUTE_LIST_FAILURE,
  };
}
// </editor-fold>
// <editor-fold dsc="specification Attribute single">
export function singleSpecificationAttributeRequest(data) {
  return {
    type: SINGLE_SPECIFICATION_ATTRIBUTE_REQUEST,
    data,
  };
}

export function singleSpecificationAttributeSuccess(data) {
  return {
    type: SINGLE_SPECIFICATION_ATTRIBUTE_SUCCESS,
    data,
  };
}

export function singleSpecificationAttributeFailure() {
  return {
    type: SINGLE_SPECIFICATION_ATTRIBUTE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="specification Attribute post">
export function specificationAttributePostRequest(data) {
  return {
    type: SPECIFICATION_ATTRIBUTE_POST_REQUEST,
    data,
  };
}

export function specificationAttributePostSuccess(data) {
  return {
    type: SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
    data,
  };
}

export function specificationAttributePostFailure() {
  return {
    type: SPECIFICATION_ATTRIBUTE_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="specification Attribute put">
export function specificationAttributePutRequest(data) {
  return {
    type: SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
    data,
  };
}

export function specificationAttributePutSuccess(data) {
  return {
    type: SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
    data,
  };
}

export function specificationAttributePutFailure() {
  return {
    type: SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="specification Attribute delete">
export function specificationAttributeDeleteRequest(data) {
  return {
    type: SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
    data,
  };
}

export function specificationAttributeDeleteSuccess(data) {
  return {
    type: SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
    data,
  };
}

export function specificationAttributeDeleteFailure() {
  return {
    type: SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="image specification Attribute delete">
export function imageSpecificationAttributeDeleteRequest(data) {
  return {
    type: IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
    data,
  };
}

export function imageSpecificationAttributeDeleteSuccess(data) {
  return {
    type: IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
    data,
  };
}

export function imageSpecificationAttributeDeleteFailure() {
  return {
    type: IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  };
}
// </editor-fold>
