import {
  SECONDARY_LEVEL_LIST_REQUEST,
  SECONDARY_LEVEL_LIST_SUCCESS,
  SECONDARY_LEVEL_LIST_FAILURE,
  CHILD_CATEGORY_LIST_REQUEST,
  CHILD_CATEGORY_LIST_SUCCESS,
  CHILD_CATEGORY_LIST_FAILURE,
  CATEGORY_LIST_REQUEST,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAILURE,
  SINGLE_CATEGORY_REQUEST,
  SINGLE_CATEGORY_SUCCESS,
  SINGLE_CATEGORY_FAILURE,
  SINGLE_CATEGORY_INIT_REQUEST,
  SINGLE_CATEGORY_INIT_SUCCESS,
  SINGLE_CATEGORY_INIT_FAILURE,
  RELATED_CATEGORY_LIST_FAILURE,
  RELATED_CATEGORY_LIST_REQUEST,
  RELATED_CATEGORY_LIST_SUCCESS,
  CATEGORY_POST_REQUEST,
  CATEGORY_POST_SUCCESS,
  CATEGORY_POST_FAILURE,
  CATEGORY_PUT_REQUEST,
  CATEGORY_PUT_SUCCESS,
  CATEGORY_PUT_FAILURE,
  CATEGORY_DELETE_REQUEST,
  CATEGORY_DELETE_SUCCESS,
  CATEGORY_DELETE_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Category new">
export function categoryListRequest(data) {
  return {
    type: CATEGORY_LIST_REQUEST,
    data,
  };
}

export function categoryListSuccess(data) {
  return {
    type: CATEGORY_LIST_SUCCESS,
    data,
  };
}

export function categoryListFailure() {
  return {
    type: CATEGORY_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Child category new">
export function childCategoryListRequest(data) {
  return {
    type: CHILD_CATEGORY_LIST_REQUEST,
    data,
  };
}

export function childCategoryListSuccess(data) {
  return {
    type: CHILD_CATEGORY_LIST_SUCCESS,
    data,
  };
}

export function childCategoryListFailure() {
  return {
    type: CHILD_CATEGORY_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="secondary level Category">
export function secondaryLevelListRequest(data) {
  return {
    type: SECONDARY_LEVEL_LIST_REQUEST,
    data,
  };
}

export function secondaryLevelListSuccess(data) {
  return {
    type: SECONDARY_LEVEL_LIST_SUCCESS,
    data,
  };
}

export function secondaryLevelListFailure(error) {
  return {
    type: SECONDARY_LEVEL_LIST_FAILURE,
    payload: {
      error,
    },
  };
}

// </editor-fold>

// <editor-fold dsc="single Category">
export function singleCategoryRequest(data) {
  return {
    type: SINGLE_CATEGORY_REQUEST,
    data,
  };
}

export function singleCategorySuccess(data) {
  return {
    type: SINGLE_CATEGORY_SUCCESS,
    data,
  };
}

export function singleCategoryFailure() {
  return {
    type: SINGLE_CATEGORY_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single Category init">
export function singleCategoryInitRequest(data) {
  return {
    type: SINGLE_CATEGORY_INIT_REQUEST,
    data,
  };
}

export function singleCategoryInitSuccess(data) {
  return {
    type: SINGLE_CATEGORY_INIT_SUCCESS,
    data,
  };
}

export function singleCategoryInitFailure() {
  return {
    type: SINGLE_CATEGORY_INIT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="related category list">
export function relatedCategoryListRequest(data) {
  return {
    type: RELATED_CATEGORY_LIST_REQUEST,
    data,
  };
}

export function relatedCategoryLIstSuccess(data) {
  return {
    type: RELATED_CATEGORY_LIST_SUCCESS,
    data,
  };
}

export function relatedCategoryListFailure() {
  return {
    type: RELATED_CATEGORY_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="category Post">
export function categoryPostRequest(data) {
  return {
    type: CATEGORY_POST_REQUEST,
    data,
  };
}

export function categoryPostSuccess() {
  return {
    type: CATEGORY_POST_SUCCESS,
  };
}

export function categoryPostFailure() {
  return {
    type: CATEGORY_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="category Put">
export function categoryPutRequest(data) {
  return {
    type: CATEGORY_PUT_REQUEST,
    data,
  };
}

export function categoryPutSuccess() {
  return {
    type: CATEGORY_PUT_SUCCESS,
  };
}

export function categoryPutFailure() {
  return {
    type: CATEGORY_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Product Delete">
export function categoryDeleteRequest(data) {
  return {
    type: CATEGORY_DELETE_REQUEST,
    data,
  };
}

export function categoryDeleteSuccess() {
  return {
    type: CATEGORY_DELETE_SUCCESS,
  };
}

export function categoryDeleteFailure() {
  return {
    type: CATEGORY_DELETE_FAILURE,
  };
}
// </editor-fold>
