import {
  PRODUCT_TIME_LIST_REQUEST,
  PRODUCT_TIME_LIST_SUCCESS,
  PRODUCT_TIME_LIST_FAILURE,
  PRODUCT_TIME_PUT_REQUEST,
  PRODUCT_TIME_PUT_SUCCESS,
  PRODUCT_TIME_PUT_FAILURE,
} from '../../../constants';

// <editor-fold dsc="product time list">
export function productTimeListRequest(data) {
  return {
    type: PRODUCT_TIME_LIST_REQUEST,
    data,
  };
}

export function productTimeListSuccess(data) {
  return {
    type: PRODUCT_TIME_LIST_SUCCESS,
    data,
  };
}

export function productTimeListFailure() {
  return {
    type: PRODUCT_TIME_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="product time Put">
export function productTimePutRequest(data) {
  return {
    type: PRODUCT_TIME_PUT_REQUEST,
    data,
  };
}

export function productTimePutSuccess() {
  return {
    type: PRODUCT_TIME_PUT_SUCCESS,
  };
}

export function productTimePutFailure() {
  return {
    type: PRODUCT_TIME_PUT_FAILURE,
  };
}
// </editor-fold>
