import {
  INVENTORY_MANAGEMENT_LIST_REQUEST,
  INVENTORY_MANAGEMENT_LIST_SUCCESS,
  INVENTORY_MANAGEMENT_LIST_FAILURE,
  INVENTORY_MANAGEMENT_POST_REQUEST,
  INVENTORY_MANAGEMENT_POST_SUCCESS,
  INVENTORY_MANAGEMENT_POST_FAILURE,
  INVENTORY_MANAGEMENT_PUT_REQUEST,
  INVENTORY_MANAGEMENT_PUT_SUCCESS,
  INVENTORY_MANAGEMENT_PUT_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="inventory Management list">
export function inventoryManagementListRequest(data) {
  return {
    type: INVENTORY_MANAGEMENT_LIST_REQUEST,
    data,
  };
}

export function inventoryManagementLIstSuccess(data) {
  return {
    type: INVENTORY_MANAGEMENT_LIST_SUCCESS,
    data,
  };
}

export function inventoryManagementListFailure() {
  return {
    type: INVENTORY_MANAGEMENT_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="inventory Management post">
export function inventoryManagementPostRequest(data) {
  return {
    type: INVENTORY_MANAGEMENT_POST_REQUEST,
    data,
  };
}

export function inventoryManagementPostSuccess(data) {
  return {
    type: INVENTORY_MANAGEMENT_POST_SUCCESS,
    data,
  };
}

export function inventoryManagementPostFailure() {
  return {
    type: INVENTORY_MANAGEMENT_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="inventory Management put">
export function inventoryManagementPutRequest(data) {
  return {
    type: INVENTORY_MANAGEMENT_PUT_REQUEST,
    data,
  };
}

export function inventoryManagementPutSuccess(data) {
  return {
    type: INVENTORY_MANAGEMENT_PUT_SUCCESS,
    data,
  };
}

export function inventoryManagementPutFailure() {
  return {
    type: INVENTORY_MANAGEMENT_PUT_FAILURE,
  };
}
// </editor-fold>
