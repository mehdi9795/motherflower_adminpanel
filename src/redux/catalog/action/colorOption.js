import {
  COLOR_OPTION_LIST_REQUEST,
  COLOR_OPTION_LIST_SUCCESS,
  COLOR_OPTION_LIST_FAILURE,
  COLOR_OPTION_POST_REQUEST,
  COLOR_OPTION_POST_SUCCESS,
  COLOR_OPTION_POST_FAILURE,
  COLOR_OPTION_PUT_REQUEST,
  COLOR_OPTION_PUT_SUCCESS,
  COLOR_OPTION_PUT_FAILURE,
  COLOR_OPTION_DELETE_REQUEST,
  COLOR_OPTION_DELETE_SUCCESS,
  COLOR_OPTION_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="color Option">
export function colorOptionListRequest(data) {
  return {
    type: COLOR_OPTION_LIST_REQUEST,
    data,
  };
}

export function colorOptionLIstSuccess(data) {
  return {
    type: COLOR_OPTION_LIST_SUCCESS,
    data,
  };
}

export function colorOptionListFailure() {
  return {
    type: COLOR_OPTION_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="color Option post">
export function colorOptionPostRequest(data) {
  return {
    type: COLOR_OPTION_POST_REQUEST,
    data,
  };
}

export function colorOptionPostSuccess(data) {
  return {
    type: COLOR_OPTION_POST_SUCCESS,
    data,
  };
}

export function colorOptionPostFailure() {
  return {
    type: COLOR_OPTION_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="color Option put">
export function colorOptionPutRequest(data) {
  return {
    type: COLOR_OPTION_PUT_REQUEST,
    data,
  };
}

export function colorOptionPutSuccess(data) {
  return {
    type: COLOR_OPTION_PUT_SUCCESS,
    data,
  };
}

export function colorOptionPutFailure() {
  return {
    type: COLOR_OPTION_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="color Option delete">
export function colorOptionDeleteRequest(data) {
  return {
    type: COLOR_OPTION_DELETE_REQUEST,
    data,
  };
}

export function colorOptionPutDeleteSuccess(data) {
  return {
    type: COLOR_OPTION_DELETE_SUCCESS,
    data,
  };
}

export function colorOptionDeleteFailure() {
  return {
    type: COLOR_OPTION_DELETE_FAILURE,
  };
}
// </editor-fold>
