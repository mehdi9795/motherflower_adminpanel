import {
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS,
  PRODUCT_LIST_FAILURE,
  PRODUCT_TYPES_LIST_LOADING,
  PRODUCT_TYPES_LIST_IS_LOADED,
  PRODUCT_TYPES_LIST_IS_FAILURE,
  PRODUCT_TYPES_LIST_FAILURE,
  PRODUCT_TYPES_LIST_REQUEST,
  PRODUCT_TYPES_LIST_SUCCESS,
  SINGLE_PRODUCT_REQUEST,
  SINGLE_PRODUCT_SUCCESS,
  SINGLE_PRODUCT_FAILURE,
  SINGLE_PRODUCT_INIT_REQUEST,
  SINGLE_PRODUCT_INIT_SUCCESS,
  SINGLE_PRODUCT_INIT_FAILURE,
  PRODUCT_POST_REQUEST,
  PRODUCT_POST_SUCCESS,
  PRODUCT_POST_FAILURE,
  PRODUCT_PUT_REQUEST,
  PRODUCT_PUT_SUCCESS,
  PRODUCT_PUT_FAILURE,
  PRODUCT_DELETE_REQUEST,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_DELETE_FAILURE,
  PRODUCT_ALERT,
  PRODUCT_ALERT_RESET,
  PRODUCT_TAG_REQUEST,
  PRODUCT_TAG_SUCCESS,
  PRODUCT_TAG_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  PRODUCT_NAME_LIST_REQUEST,
  PRODUCT_NAME_LIST_SUCCESS,
  PRODUCT_NAME_LIST_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="Product">
export function productListRequest(data) {
  return {
    type: PRODUCT_LIST_REQUEST,
    data,
  };
}

export function productListSuccess(data) {
  return {
    type: PRODUCT_LIST_SUCCESS,
    data,
  };
}

export function productListFailure() {
  return {
    type: PRODUCT_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Product name list">
export function productNameListRequest(data) {
  return {
    type: PRODUCT_NAME_LIST_REQUEST,
    data,
  };
}

export function productNameListSuccess(data) {
  return {
    type: PRODUCT_NAME_LIST_SUCCESS,
    data,
  };
}

export function productNameListFailure() {
  return {
    type: PRODUCT_NAME_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Single Product">
export function singleProductRequest(data) {
  return {
    type: SINGLE_PRODUCT_REQUEST,
    data,
  };
}

export function singleProductSuccess(data) {
  return {
    type: SINGLE_PRODUCT_SUCCESS,
    data,
  };
}

export function singleProductFailure() {
  return {
    type: SINGLE_PRODUCT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Single Product Init">
export function singleProductInitRequest() {
  return {
    type: SINGLE_PRODUCT_INIT_REQUEST,
  };
}

export function singleProductInitSuccess(data) {
  return {
    type: SINGLE_PRODUCT_INIT_SUCCESS,
    data,
  };
}

export function singleProductInitFailure() {
  return {
    type: SINGLE_PRODUCT_INIT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="product Post">
export function productPostRequest(data) {
  return {
    type: PRODUCT_POST_REQUEST,
    data,
  };
}

export function productPostSuccess() {
  return {
    type: PRODUCT_POST_SUCCESS,
  };
}

export function productPostFailure() {
  return {
    type: PRODUCT_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="product Put">
export function productPutRequest(data) {
  return {
    type: PRODUCT_PUT_REQUEST,
    data,
  };
}

export function productPutSuccess() {
  return {
    type: PRODUCT_PUT_SUCCESS,
  };
}

export function productPutFailure() {
  return {
    type: PRODUCT_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Product Delete">
export function productDeleteRequest(data) {
  return {
    type: PRODUCT_DELETE_REQUEST,
    data,
  };
}

export function productDeleteSuccess() {
  return {
    type: PRODUCT_DELETE_SUCCESS,
  };
}

export function productDeleteFailure() {
  return {
    type: PRODUCT_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Product Tag">
export function productTagRequest(data) {
  return {
    type: PRODUCT_TAG_REQUEST,
    data,
  };
}

export function productTagSuccess(data) {
  return {
    type: PRODUCT_TAG_SUCCESS,
    data,
  };
}

export function productTagFailure() {
  return {
    type: PRODUCT_TAG_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Product specification attribute list">
export function productSpecificationAttributeListRequest(data) {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    data,
  };
}

export function productSpecificationAttributeListSuccess(data) {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
    data,
  };
}

export function productSpecificationAttributeListFailure() {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="product specification attribute  Post">
export function productSpecificationAttributePostRequest(data) {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_POST_REQUEST,
    data,
  };
}

export function productSpecificationAttributePostSuccess() {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  };
}

export function productSpecificationAttributePostFailure() {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="product specification attribute  Put">
export function productSpecificationAttributePutRequest(data) {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
    data,
  };
}

export function productSpecificationAttributePutSuccess() {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  };
}

export function productSpecificationAttributePutFailure() {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Product specification attribute  Delete">
export function productSpecificationAttributeDeleteRequest(data) {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
    data,
  };
}

export function productSpecificationAttributeDeleteSuccess() {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  };
}

export function productSpecificationAttributeDeleteFailure() {
  return {
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  };
}
// </editor-fold>

export function productAlert(data) {
  return {
    type: PRODUCT_ALERT,
    data,
  };
}

export function productAlertReset() {
  return {
    type: PRODUCT_ALERT_RESET,
  };
}
