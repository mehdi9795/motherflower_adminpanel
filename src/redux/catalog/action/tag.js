import {
  TAG_LIST_REQUEST,
  TAG_LIST_SUCCESS,
  TAG_LIST_FAILURE,
  TAG_POST_REQUEST,
  TAG_POST_SUCCESS,
  TAG_POST_FAILURE,
  TAG_PUT_REQUEST,
  TAG_PUT_SUCCESS,
  TAG_PUT_FAILURE,
  TAG_DELETE_SUCCESS,
  TAG_DELETE_FAILURE,
  RELATED_TAG_LIST_REQUEST,
  RELATED_TAG_LIST_SUCCESS,
  RELATED_TAG_LIST_FAILURE,
  TAG_DELETE_REQUEST,
  IMAGE_TAG_DELETE_REQUEST,
  IMAGE_TAG_DELETE_SUCCESS,
  IMAGE_TAG_DELETE_FAILURE,
} from '../../../constants';

// <editor-fold dsc="tag list">
export function tagListRequest(data) {
  return {
    type: TAG_LIST_REQUEST,
    data,
  };
}

export function tagListSuccess(data) {
  return {
    type: TAG_LIST_SUCCESS,
    data,
  };
}

export function tagListFailure() {
  return {
    type: TAG_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="related tag list">
export function relatedTagListRequest(data) {
  return {
    type: RELATED_TAG_LIST_REQUEST,
    data,
  };
}

export function relatedTagListSuccess(data) {
  return {
    type: RELATED_TAG_LIST_SUCCESS,
    data,
  };
}

export function relatedTagListFailure() {
  return {
    type: RELATED_TAG_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="tag Post">
export function tagPostRequest(data) {
  return {
    type: TAG_POST_REQUEST,
    data,
  };
}

export function tagPostSuccess() {
  return {
    type: TAG_POST_SUCCESS,
  };
}

export function tagPostFailure() {
  return {
    type: TAG_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="tag Put">
export function tagPutRequest(data) {
  return {
    type: TAG_PUT_REQUEST,
    data,
  };
}

export function tagPutSuccess() {
  return {
    type: TAG_PUT_SUCCESS,
  };
}

export function tagPutFailure() {
  return {
    type: TAG_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="tag Delete">
export function tagDeleteRequest(data) {
  return {
    type: TAG_DELETE_REQUEST,
    data,
  };
}

export function tagDeleteSuccess() {
  return {
    type: TAG_DELETE_SUCCESS,
  };
}

export function tagDeleteFailure() {
  return {
    type: TAG_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="tag image Delete">
export function imageTagDeleteRequest(data) {
  return {
    type: IMAGE_TAG_DELETE_REQUEST,
    data,
  };
}

export function imageTagDeleteSuccess() {
  return {
    type: IMAGE_TAG_DELETE_SUCCESS,
  };
}

export function imageTagDeleteFailure() {
  return {
    type: IMAGE_TAG_DELETE_FAILURE,
  };
}
// </editor-fold>
