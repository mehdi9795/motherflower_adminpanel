import { call, put, select } from 'redux-saga/effects';
import {
  SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
  SPECIFICATION_ATTRIBUTE_LIST_FAILURE,
  SINGLE_SPECIFICATION_ATTRIBUTE_FAILURE,
  SINGLE_SPECIFICATION_ATTRIBUTE_SUCCESS,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  SPECIFICATION_ATTRIBUTE_POST_FAILURE,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
  SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
  SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS, IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
} from '../../../constants';
import {
  IMAGE_DELETE_IS_SUCCESS,
  SPECIFICATION_ATTRIBUTE_CREATE_IS_SUCCESS,
  SPECIFICATION_ATTRIBUTE_DELETE_IS_SUCCESS,
  SPECIFICATION_ATTRIBUTE_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* specificationAttributeListRequest(
  getDtoQueryString,
  getSpecificationAttributeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getSpecificationAttributeApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({
      type: SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: SPECIFICATION_ATTRIBUTE_LIST_FAILURE });
  }
}

export function* singleSpecificationAttributeRequest(
  getDtoQueryString,
  getSpecificationAttributeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getSpecificationAttributeApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({
      type: SINGLE_SPECIFICATION_ATTRIBUTE_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: SINGLE_SPECIFICATION_ATTRIBUTE_FAILURE });
  }
}

export function* specificationAttributePostRequest(
  postSpecificationAttributeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    postSpecificationAttributeApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SPECIFICATION_ATTRIBUTE_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* specificationAttributePostSuccess(history, message, action) {
  message.success(SPECIFICATION_ATTRIBUTE_CREATE_IS_SUCCESS, 5);
  yield put({
    type: SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
export function* specificationAttributePutRequest(
  putSpecificationAttributeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    putSpecificationAttributeApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* specificationAttributePutSuccess(history, message, action) {
  message.success(SPECIFICATION_ATTRIBUTE_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* specificationAttributeDeleteRequest(
  deleteSpecificationAttributeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    deleteSpecificationAttributeApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* specificationAttributeDeleteSuccess(history, message, action) {
  message.success(SPECIFICATION_ATTRIBUTE_DELETE_IS_SUCCESS, 5);
  yield put({
    type: SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* imageSpecificationAttributeDeleteRequest(
  deleteImageSpecificationAttributeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    deleteImageSpecificationAttributeApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* imageSpecificationAttributeDeleteSuccess(message, action) {
  message.success(IMAGE_DELETE_IS_SUCCESS, 5);
  yield put({
    type: SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
