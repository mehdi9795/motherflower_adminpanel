import { select, all, call, put } from 'redux-saga/effects';
import {
  CHILD_CATEGORY_LIST_SUCCESS,
  CHILD_CATEGORY_LIST_FAILURE,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAILURE,
  SECONDARY_LEVEL_LIST_FAILURE,
  SECONDARY_LEVEL_LIST_SUCCESS,
  // CATEGORY_TYPES_LIST_SUCCESS,
  RELATED_CATEGORY_LIST_FAILURE,
  RELATED_CATEGORY_LIST_SUCCESS,
  SINGLE_CATEGORY_INIT_FAILURE,
  SINGLE_CATEGORY_INIT_SUCCESS,
  SINGLE_CATEGORY_SUCCESS,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  CATEGORY_POST_SUCCESS,
  CATEGORY_POST_FAILURE,
  CATEGORY_PUT_SUCCESS,
  CATEGORY_PUT_FAILURE,
  CATEGORY_DELETE_SUCCESS,
  CATEGORY_DELETE_FAILURE,
  PRODUCT_TYPES_LIST_SUCCESS,
  SINGLE_CATEGORY_FAILURE,
} from '../../../constants';
import {
  CATEGORY_CREATE_IS_SUCCESS,
  CATEGORY_UPDATE_IS_SUCCESS,
  CATEGORY_DELETE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* categoryListRequest(
  getDtoQueryString,
  getCategoryApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getCategoryApi, currentSession.token, container);
  if (response) {
    yield put({ type: CATEGORY_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: CATEGORY_LIST_FAILURE });
  }
}

export function* childCategoryListRequest(
  getDtoQueryString,
  getCategoryApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getCategoryApi, currentSession.token, container);
  if (response) {
    yield put({ type: CHILD_CATEGORY_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: CHILD_CATEGORY_LIST_FAILURE });
  }
}

export function* secondaryLevelCategoryListRequest(
  getDtoQueryString,
  getChildCategoryApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);

  const response = yield call(
    getChildCategoryApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: SECONDARY_LEVEL_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: SECONDARY_LEVEL_LIST_FAILURE });
  }
}

export function* categorySingleInitRequest(
  getDtoQueryString,
  getCategoryApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const [singleCategory, categories] = yield all([
    call(
      getCategoryApi,
      currentSession.token,
      getDtoQueryString(action.dto.singleCategoryDto),
    ),
    call(
      getCategoryApi,
      currentSession.token,
      getDtoQueryString(action.data.categoriesDto),
    ),
  ]);

  if (singleCategory && categories) {
    yield put({ type: SINGLE_CATEGORY_SUCCESS, data: singleCategory.data });
    yield put({ type: PRODUCT_TYPES_LIST_SUCCESS, data: categories.data });

    yield put({ type: SINGLE_CATEGORY_INIT_SUCCESS });
  } else {
    yield put({ type: SINGLE_CATEGORY_INIT_FAILURE });
  }
}

export function* singleCategoryRequest(
  getDtoQueryString,
  getCategoryApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getCategoryApi, currentSession.token, container);
  if (response) {
    yield put({ type: SINGLE_CATEGORY_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_CATEGORY_FAILURE });
  }
}

export function* relatedCategoryListRequest(
  getDtoQueryString,
  getRelatedCategoryApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getRelatedCategoryApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: RELATED_CATEGORY_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: RELATED_CATEGORY_LIST_FAILURE });
  }
}

export function* categoryPostRequest(postCategoryApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    postCategoryApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    const data = {
      data: action.dto,
      id: response.data.id,
    };
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: CATEGORY_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: CATEGORY_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function categoryPostSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue') {
    message.success(CATEGORY_CREATE_IS_SUCCESS, 5);
    history.push(`/category/edit/${action.data.id}`);
  } else {
    message.success(CATEGORY_CREATE_IS_SUCCESS, 5);
    history.push('/category/list');
  }
}
export function* categoryPutRequest(putCategoryApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    putCategoryApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: CATEGORY_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: CATEGORY_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function categoryPutSuccess(history, message, action) {
  if (action.data.status === 'saveAndContinue') {
    message.success(CATEGORY_UPDATE_IS_SUCCESS, 5);
    // history.push(`/category/edit/${action.data.data.id}`);
  } else {
    message.success(CATEGORY_UPDATE_IS_SUCCESS, 5);
    history.push('/category/list');
  }
}

export function* categoryDeleteRequest(deleteCategoryApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    deleteCategoryApi,
    currentSession.token,
    action.dto,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: CATEGORY_DELETE_SUCCESS,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: CATEGORY_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function categoryDeleteSuccess(history, message) {
  message.success(CATEGORY_DELETE_IS_SUCCESS, 5);
  history.push('/category/list');
}
