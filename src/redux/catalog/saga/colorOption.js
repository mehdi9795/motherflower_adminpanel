import { call, put, select } from 'redux-saga/effects';
import {
  COLOR_OPTION_LIST_SUCCESS,
  COLOR_OPTION_LIST_FAILURE,
} from '../../../constants/index';
import {
  COLOR_OPTION_DELETE_FAILURE,
  COLOR_OPTION_DELETE_SUCCESS,
  COLOR_OPTION_LIST_REQUEST,
  COLOR_OPTION_POST_FAILURE,
  COLOR_OPTION_POST_SUCCESS,
  COLOR_OPTION_PUT_FAILURE,
  COLOR_OPTION_PUT_SUCCESS,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
} from '../../../constants';
import {
  COLOR_OPTION_CREATE_IS_SUCCESS,
  COLOR_OPTION_DELETE_IS_SUCCESS,
  COLOR_OPTION_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* colorOptionListRequest(
  getDtoQueryString,
  getColorOptionApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getColorOptionApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: COLOR_OPTION_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: COLOR_OPTION_LIST_FAILURE });
  }
}

export function* colorOptionPostRequest(postColorOptionApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    postColorOptionApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COLOR_OPTION_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COLOR_OPTION_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* colorOptionPostSuccess(history, message, action) {
  message.success(COLOR_OPTION_CREATE_IS_SUCCESS, 5);
  yield put({
    type: COLOR_OPTION_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
export function* colorOptionPutRequest(putColorOptionApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    putColorOptionApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COLOR_OPTION_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COLOR_OPTION_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* colorOptionPutSuccess(history, message, action) {
  message.success(COLOR_OPTION_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: COLOR_OPTION_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* colorOptionDeleteRequest(deleteColorOptionApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    deleteColorOptionApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COLOR_OPTION_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COLOR_OPTION_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* colorOptionDeleteSuccess(history, message, action) {
  message.success(COLOR_OPTION_DELETE_IS_SUCCESS, 5);
  yield put({
    type: COLOR_OPTION_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
