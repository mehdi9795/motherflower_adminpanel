import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import { getDtoQueryString } from '../../../utils/helper';
import {
  CATEGORY_LIST_REQUEST,
  CHILD_CATEGORY_LIST_REQUEST,
  PRODUCT_DELETE_REQUEST,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_POST_REQUEST,
  PRODUCT_POST_SUCCESS,
  PRODUCT_PUT_REQUEST,
  PRODUCT_PUT_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  PRODUCT_TAG_REQUEST,
  PRODUCT_TYPES_LIST_REQUEST,
  SINGLE_PRODUCT_REQUEST,
  PRODUCT_LIST_REQUEST,
  RELATED_CATEGORY_LIST_REQUEST,
  COLOR_OPTION_LIST_REQUEST,
  SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
  SINGLE_SPECIFICATION_ATTRIBUTE_REQUEST,
  SINGLE_CATEGORY_REQUEST,
  CATEGORY_POST_REQUEST,
  CATEGORY_POST_SUCCESS,
  CATEGORY_PUT_REQUEST,
  CATEGORY_PUT_SUCCESS,
  CATEGORY_DELETE_REQUEST,
  CATEGORY_DELETE_SUCCESS,
  PRODUCT_TIME_LIST_REQUEST,
  PRODUCT_TIME_PUT_REQUEST,
  PRODUCT_TIME_PUT_SUCCESS,
  TAG_LIST_REQUEST,
  RELATED_TAG_LIST_REQUEST,
  TAG_PUT_SUCCESS,
  TAG_POST_REQUEST,
  TAG_POST_SUCCESS,
  TAG_PUT_REQUEST,
  TAG_DELETE_REQUEST,
  TAG_DELETE_SUCCESS,
  SPECIFICATION_ATTRIBUTE_POST_REQUEST,
  SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
  SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
  IMAGE_TAG_DELETE_REQUEST,
  IMAGE_TAG_DELETE_SUCCESS,
  COLOR_OPTION_POST_REQUEST,
  COLOR_OPTION_POST_SUCCESS,
  COLOR_OPTION_PUT_REQUEST,
  COLOR_OPTION_PUT_SUCCESS,
  COLOR_OPTION_DELETE_REQUEST,
  COLOR_OPTION_DELETE_SUCCESS,
  INVENTORY_MANAGEMENT_LIST_REQUEST,
  INVENTORY_MANAGEMENT_POST_REQUEST,
  INVENTORY_MANAGEMENT_POST_SUCCESS,
  INVENTORY_MANAGEMENT_PUT_REQUEST,
  INVENTORY_MANAGEMENT_PUT_SUCCESS,
  PRODUCT_NAME_LIST_REQUEST,
} from '../../../constants';
import {
  productDeleteSuccess,
  productDeleteRequest,
  productListRequest,
  productPostRequest,
  productPostSuccess,
  productPutRequest,
  productPutSuccess,
  singleProductRequest,
  productTagListRequest,
  productSpecificationAttributeListRequest,
  productSpecificationAttributePostRequest,
  productSpecificationAttributePostSuccess,
  productSpecificationAttributePutRequest,
  productSpecificationAttributePutSuccess,
  productSpecificationAttributeDeleteRequest,
  productSpecificationAttributeDeleteSuccess,
  productNameListRequest,
} from './product';
import {
  categoryDeleteRequest,
  categoryDeleteSuccess,
  categoryListRequest,
  categoryPostRequest,
  categoryPostSuccess,
  categoryPutRequest,
  categoryPutSuccess,
  childCategoryListRequest,
  relatedCategoryListRequest,
} from './category';
import { productTypesListRequest } from './productType';
import {
  deleteCategoryApi,
  deleteColorOptionApi,
  putColorOptionApi,
  deleteProductApi,
  deleteProductSpecificationAttributeApi,
  deleteSpecificationAttributeApi,
  deleteTagApi,
  getCategoryApi,
  getColorOptionApi,
  getInventoryManagementApi,
  getProductApi,
  getProductSpecificationAttributeApi,
  getProductTimeApi,
  getProductTypesApi,
  getRelatedTagApi,
  getSpecificationAttributeApi,
  getTagApi,
  postCategoryApi,
  postColorOptionApi,
  postInventoryManagementApi,
  postProductApi,
  postProductNamesApi,
  postProductSpecificationAttributeApi,
  postSpecificationAttributeApi,
  postTagApi,
  putCategoryApi,
  putInventoryManagementApi,
  putProductApi,
  putProductSpecificationAttributeApi,
  putProductTimeApi,
  putSpecificationAttributeApi,
  putTagApi,
} from '../../../services/catalogApi';
import alert from '../../../components/CP/CPAlert';
import history from '../../../history';
import {
  colorOptionDeleteRequest,
  colorOptionDeleteSuccess,
  colorOptionListRequest,
  colorOptionPostRequest,
  colorOptionPostSuccess,
  colorOptionPutRequest,
  colorOptionPutSuccess,
} from './colorOption';
import {
  inventoryManagementListRequest,
  inventoryManagementPostRequest,
  inventoryManagementPostSuccess,
  inventoryManagementPutRequest,
  inventoryManagementPutSuccess,
} from './inventoryManagement';
import {
  imageSpecificationAttributeDeleteRequest,
  imageSpecificationAttributeDeleteSuccess,
  singleSpecificationAttributeRequest,
  specificationAttributeDeleteRequest,
  specificationAttributeDeleteSuccess,
  specificationAttributeListRequest,
  specificationAttributePostRequest,
  specificationAttributePostSuccess,
  specificationAttributePutRequest,
  specificationAttributePutSuccess,
} from './specificationAttribute';
import { singleCategoryRequest } from '../action/category';
import {
  productTimeListRequest,
  productTimePutRequest,
  productTimePutSuccess,
} from './productTime';
import {
  imageTagDeleteRequest,
  imageTagDeleteSuccess,
  relatedTagListRequest,
  tagDeleteRequest,
  tagDeleteSuccess,
  tagListRequest,
  tagPostRequest,
  tagPostSuccess,
  tagPutRequest,
  tagPutSuccess,
} from './tag';
import { deleteImageApi } from '../../../services/cmsApi';

export default function* sagaCatalogIndex() {
  yield all([
    // <editor-fold dsc="product">
    takeLatest(
      PRODUCT_LIST_REQUEST,
      productListRequest,
      getDtoQueryString,
      getProductApi,
    ),
    takeLatest(
      PRODUCT_NAME_LIST_REQUEST,
      productNameListRequest,
      getDtoQueryString,
      postProductNamesApi,
    ),
    takeLatest(
      SINGLE_PRODUCT_REQUEST,
      singleProductRequest,
      getDtoQueryString,
      getProductApi,
    ),
    takeLatest(
      PRODUCT_POST_REQUEST,
      productPostRequest,
      postProductApi,
      message,
      alert,
    ),
    takeLatest(PRODUCT_POST_SUCCESS, productPostSuccess, history, message),
    takeLatest(PRODUCT_PUT_REQUEST, productPutRequest, putProductApi),
    takeLatest(PRODUCT_PUT_SUCCESS, productPutSuccess, history, message),
    takeLatest(PRODUCT_DELETE_REQUEST, productDeleteRequest, deleteProductApi),
    takeLatest(PRODUCT_DELETE_SUCCESS, productDeleteSuccess, history, message),

    takeLatest(
      PRODUCT_TAG_REQUEST,
      productTagListRequest,
      getDtoQueryString,
      getTagApi,
    ),

    takeLatest(
      PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
      productSpecificationAttributeListRequest,
      getDtoQueryString,
      getProductSpecificationAttributeApi,
    ),

    takeLatest(
      PRODUCT_SPECIFICATION_ATTRIBUTE_POST_REQUEST,
      productSpecificationAttributePostRequest,
      postProductSpecificationAttributeApi,
      message,
      alert,
    ),
    takeLatest(
      PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
      productSpecificationAttributePostSuccess,
      history,
      message,
    ),
    takeLatest(
      PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
      productSpecificationAttributePutRequest,
      putProductSpecificationAttributeApi,
    ),
    takeLatest(
      PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
      productSpecificationAttributePutSuccess,
      history,
      message,
    ),
    takeLatest(
      PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
      productSpecificationAttributeDeleteRequest,
      deleteProductSpecificationAttributeApi,
    ),
    takeLatest(
      PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
      productSpecificationAttributeDeleteSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="catalog productType">
    takeLatest(
      PRODUCT_TYPES_LIST_REQUEST,
      productTypesListRequest,
      getDtoQueryString,
      getProductTypesApi,
    ),
    // </editor-fold>

    // <editor-fold dsc="Catalog Category">
    takeLatest(
      CATEGORY_LIST_REQUEST,
      categoryListRequest,
      getDtoQueryString,
      getCategoryApi,
    ),
    takeLatest(
      SINGLE_CATEGORY_REQUEST,
      singleCategoryRequest,
      getDtoQueryString,
      getCategoryApi,
    ),
    takeLatest(
      RELATED_CATEGORY_LIST_REQUEST,
      relatedCategoryListRequest,
      getDtoQueryString,
      getCategoryApi,
    ),
    takeLatest(CATEGORY_POST_REQUEST, categoryPostRequest, postCategoryApi),
    takeLatest(CATEGORY_POST_SUCCESS, categoryPostSuccess, history, message),
    takeLatest(CATEGORY_PUT_REQUEST, categoryPutRequest, putCategoryApi),
    takeLatest(CATEGORY_PUT_SUCCESS, categoryPutSuccess, history, message),
    takeLatest(
      CATEGORY_DELETE_REQUEST,
      categoryDeleteRequest,
      deleteCategoryApi,
    ),
    takeLatest(
      CATEGORY_DELETE_SUCCESS,
      categoryDeleteSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="Catalog Child Category">
    takeLatest(
      CHILD_CATEGORY_LIST_REQUEST,
      childCategoryListRequest,
      getDtoQueryString,
      getCategoryApi,
    ),
    // </editor-fold>

    // <editor-fold dsc="color option">
    takeLatest(
      COLOR_OPTION_LIST_REQUEST,
      colorOptionListRequest,
      getDtoQueryString,
      getColorOptionApi,
    ),
    takeLatest(
      COLOR_OPTION_POST_REQUEST,
      colorOptionPostRequest,
      postColorOptionApi,
    ),
    takeLatest(
      COLOR_OPTION_POST_SUCCESS,
      colorOptionPostSuccess,
      history,
      message,
    ),
    takeLatest(
      COLOR_OPTION_PUT_REQUEST,
      colorOptionPutRequest,
      putColorOptionApi,
    ),
    takeLatest(
      COLOR_OPTION_PUT_SUCCESS,
      colorOptionPutSuccess,
      history,
      message,
    ),
    takeLatest(
      COLOR_OPTION_DELETE_REQUEST,
      colorOptionDeleteRequest,
      deleteColorOptionApi,
    ),
    takeLatest(
      COLOR_OPTION_DELETE_SUCCESS,
      colorOptionDeleteSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="specification attribute">
    takeLatest(
      SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
      specificationAttributeListRequest,
      getDtoQueryString,
      getSpecificationAttributeApi,
    ),

    takeLatest(
      SINGLE_SPECIFICATION_ATTRIBUTE_REQUEST,
      singleSpecificationAttributeRequest,
      getDtoQueryString,
      getSpecificationAttributeApi,
    ),
    takeLatest(
      SPECIFICATION_ATTRIBUTE_POST_REQUEST,
      specificationAttributePostRequest,
      postSpecificationAttributeApi,
    ),
    takeLatest(
      SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
      specificationAttributePostSuccess,
      history,
      message,
    ),
    takeLatest(
      SPECIFICATION_ATTRIBUTE_PUT_REQUEST,
      specificationAttributePutRequest,
      putSpecificationAttributeApi,
    ),
    takeLatest(
      SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
      specificationAttributePutSuccess,
      history,
      message,
    ),
    takeLatest(
      SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
      specificationAttributeDeleteRequest,
      deleteSpecificationAttributeApi,
    ),
    takeLatest(
      SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
      specificationAttributeDeleteSuccess,
      history,
      message,
    ),
    takeLatest(
      IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_REQUEST,
      imageSpecificationAttributeDeleteRequest,
      deleteImageApi,
    ),
    takeLatest(
      IMAGE_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
      imageSpecificationAttributeDeleteSuccess,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="product time">
    takeLatest(
      PRODUCT_TIME_LIST_REQUEST,
      productTimeListRequest,
      getDtoQueryString,
      getProductTimeApi,
    ),
    takeLatest(
      PRODUCT_TIME_PUT_REQUEST,
      productTimePutRequest,
      putProductTimeApi,
    ),
    takeLatest(
      PRODUCT_TIME_PUT_SUCCESS,
      productTimePutSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="Catalog tag">
    takeLatest(TAG_LIST_REQUEST, tagListRequest, getDtoQueryString, getTagApi),
    takeLatest(
      RELATED_TAG_LIST_REQUEST,
      relatedTagListRequest,
      getDtoQueryString,
      getRelatedTagApi,
    ),
    takeLatest(TAG_POST_REQUEST, tagPostRequest, postTagApi),
    takeLatest(TAG_POST_SUCCESS, tagPostSuccess, history, message),
    takeLatest(TAG_PUT_REQUEST, tagPutRequest, putTagApi),
    takeLatest(TAG_PUT_SUCCESS, tagPutSuccess, history, message),
    takeLatest(TAG_DELETE_REQUEST, tagDeleteRequest, deleteTagApi),
    takeLatest(TAG_DELETE_SUCCESS, tagDeleteSuccess, history, message),
    takeLatest(IMAGE_TAG_DELETE_REQUEST, imageTagDeleteRequest, deleteImageApi),
    takeLatest(IMAGE_TAG_DELETE_SUCCESS, imageTagDeleteSuccess, message),
    // </editor-fold>

    // <editor-fold dsc="inventory management">
    takeLatest(
      INVENTORY_MANAGEMENT_LIST_REQUEST,
      inventoryManagementListRequest,
      getDtoQueryString,
      getInventoryManagementApi,
    ),
    takeLatest(
      INVENTORY_MANAGEMENT_POST_REQUEST,
      inventoryManagementPostRequest,
      postInventoryManagementApi,
    ),
    takeLatest(
      INVENTORY_MANAGEMENT_POST_SUCCESS,
      inventoryManagementPostSuccess,
      history,
      message,
    ),
    takeLatest(
      INVENTORY_MANAGEMENT_PUT_REQUEST,
      inventoryManagementPutRequest,
      putInventoryManagementApi,
    ),
    takeLatest(
      INVENTORY_MANAGEMENT_PUT_SUCCESS,
      inventoryManagementPutSuccess,
      history,
      message,
    ),
    // </editor-fold>
  ]);
}
