import { call, put, select } from 'redux-saga/effects';
import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  INVENTORY_MANAGEMENT_LIST_FAILURE,
  INVENTORY_MANAGEMENT_LIST_SUCCESS,
  INVENTORY_MANAGEMENT_POST_FAILURE,
  INVENTORY_MANAGEMENT_POST_SUCCESS,
  INVENTORY_MANAGEMENT_PUT_FAILURE,
  INVENTORY_MANAGEMENT_PUT_SUCCESS,
} from '../../../constants';
import {
  INVENTORY_MANAGEMENT_CREATE_IS_SUCCESS,
  INVENTORY_MANAGEMENT_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* inventoryManagementListRequest(
  getDtoQueryString,
  getInventoryManagementApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getInventoryManagementApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: INVENTORY_MANAGEMENT_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: INVENTORY_MANAGEMENT_LIST_FAILURE });
  }
}

export function* inventoryManagementPostRequest(
  postInventoryManagementApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    postInventoryManagementApi,
    currentSession.token,
    action.dto,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: INVENTORY_MANAGEMENT_POST_SUCCESS,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: INVENTORY_MANAGEMENT_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function inventoryManagementPostSuccess(history, message) {
  message.success(INVENTORY_MANAGEMENT_CREATE_IS_SUCCESS, 5);
  history.push('/inventory-management/list');
}
export function* inventoryManagementPutRequest(
  putInventoryManagementApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    putInventoryManagementApi,
    currentSession.token,
    action.dto,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: INVENTORY_MANAGEMENT_PUT_SUCCESS,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: INVENTORY_MANAGEMENT_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function inventoryManagementPutSuccess(history, message) {
  message.success(INVENTORY_MANAGEMENT_UPDATE_IS_SUCCESS, 5);
  history.push('/inventory-management/list');
}
