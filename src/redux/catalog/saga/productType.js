import { select, call, put } from 'redux-saga/effects';
import {
  PRODUCT_TYPES_LIST_SUCCESS,
  PRODUCT_TYPES_LIST_FAILURE,
} from '../../../constants/index';

const getCurrentSession = state => state.login.data;

export function* productTypesListRequest(
  getDtoQueryString,
  getProductTypesApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getProductTypesApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: PRODUCT_TYPES_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PRODUCT_TYPES_LIST_FAILURE });
  }
}
