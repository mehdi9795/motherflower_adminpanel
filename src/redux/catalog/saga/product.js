import { select, call, put } from 'redux-saga/effects';
import {
  PRODUCT_LIST_SUCCESS,
  PRODUCT_LIST_FAILURE,
  SINGLE_PRODUCT_FAILURE,
  SINGLE_PRODUCT_SUCCESS,
  PRODUCT_TAG_FAILURE,
  PRODUCT_TAG_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_FAILURE,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  PRODUCT_POST_SUCCESS,
  PRODUCT_POST_FAILURE,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  PRODUCT_LIST_REQUEST,
  PRODUCT_PUT_SUCCESS,
  PRODUCT_PUT_FAILURE,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_DELETE_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_POST_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
  PRODUCT_NAME_LIST_SUCCESS,
  PRODUCT_NAME_LIST_FAILURE,
} from '../../../constants';
import {
  PRODUCT_CREATE_IS_SUCCESS,
  PRODUCT_DELETE_IS_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_CREATE_IS_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_IS_SUCCESS,
  PRODUCT_SPECIFICATION_ATTRIBUTE_UPDATE_IS_SUCCESS,
  PRODUCT_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* productListRequest(getDtoQueryString, getProductApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getProductApi, currentSession.token, container);
  if (response) {
    yield put({ type: PRODUCT_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PRODUCT_LIST_FAILURE });
  }
}

export function* productNameListRequest(
  getDtoQueryString,
  getProductNameApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getProductNameApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: PRODUCT_NAME_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PRODUCT_NAME_LIST_FAILURE });
  }
}

export function* singleProductRequest(
  getDtoQueryString,
  getProductApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getProductApi, currentSession.token, container);
  if (response) {
    yield put({ type: SINGLE_PRODUCT_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_PRODUCT_FAILURE });
  }
}

export function* productPostRequest(postProductApi, message, alert, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    postProductApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    const data = {
      data: action.dto,
      id: response.data.id,
    };

    yield put({
      type: PRODUCT_POST_SUCCESS,
      data,
    });
    // yield put({
    //   type: EDIT_FORM_LOADING_HIDE_REQUEST,
    // });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function productPostSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue') {
    message.success(PRODUCT_CREATE_IS_SUCCESS, 5);
    if (action.data.data.backUrl === '/vendor/list')
      history.push(`/product/edit/${action.data.id}?back=vendor`);
    else history.push(`/product/edit/${action.data.id}`);
  } else {
    message.success(PRODUCT_CREATE_IS_SUCCESS, 5);
    history.push(action.data.data.backUrl);
  }
}
export function* productPutRequest(putProductApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    putProductApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* productPutSuccess(history, message, action) {
  if (action.data.status === 'saveAndContinue') {
    message.success(PRODUCT_UPDATE_IS_SUCCESS, 5);
    if (action.data.backUrl === '/vendor/list')
      history.push(`/product/edit/${action.data.data.dto.id}?back=vendor`);
    else history.push(`/product/edit/${action.data.data.dto.id}`);
  } else if (action.data.status === 'list') {
    message.success(PRODUCT_UPDATE_IS_SUCCESS, 5);
    yield put({
      type: PRODUCT_LIST_REQUEST,
      dto: action.data.listDto,
    });
  } else {
    message.success(PRODUCT_UPDATE_IS_SUCCESS, 5);
    history.push(action.data.backUrl);
  }
}

export function* productDeleteRequest(deleteProductApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteProductApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* productDeleteSuccess(history, message, action) {
  message.success(PRODUCT_DELETE_IS_SUCCESS, 5);
  if (action.data.status === 'edit') {
    history.push('/product/list');
  } else {
    yield put({
      type: PRODUCT_LIST_REQUEST,
      dto: action.data.listDto,
    });
  }
}

export function* productTagListRequest(getDtoQueryString, getTagApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getTagApi, currentSession.token, container);
  if (response) {
    yield put({ type: PRODUCT_TAG_SUCCESS, data: response.data });
  } else {
    yield put({ type: PRODUCT_TAG_FAILURE });
  }
}

export function* productSpecificationAttributeListRequest(
  getDtoQueryString,
  getProductSpecificationAttributeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getProductSpecificationAttributeApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({
      type: PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_FAILURE });
  }
}

export function* productSpecificationAttributePostRequest(
  postProductSpecificationAttributeApi,
  message,
  alert,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    postProductSpecificationAttributeApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_SPECIFICATION_ATTRIBUTE_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_SPECIFICATION_ATTRIBUTE_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* productSpecificationAttributePostSuccess(
  history,
  message,
  action,
) {
  message.success(PRODUCT_SPECIFICATION_ATTRIBUTE_CREATE_IS_SUCCESS, 5);
  yield put({
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
export function* productSpecificationAttributePutRequest(
  putProductSpecificationAttributeApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    putProductSpecificationAttributeApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_SPECIFICATION_ATTRIBUTE_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* productSpecificationAttributePutSuccess(
  history,
  message,
  action,
) {
  message.success(PRODUCT_SPECIFICATION_ATTRIBUTE_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* productSpecificationAttributeDeleteRequest(
  deleteProductSpecificationAttributeApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteProductSpecificationAttributeApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* productSpecificationAttributeDeleteSuccess(
  history,
  message,
  action,
) {
  message.success(PRODUCT_SPECIFICATION_ATTRIBUTE_DELETE_IS_SUCCESS, 5);
  yield put({
    type: PRODUCT_SPECIFICATION_ATTRIBUTE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
