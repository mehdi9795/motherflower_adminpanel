import { call, put, select } from 'redux-saga/effects';
import {
  PRODUCT_TIME_LIST_SUCCESS,
  PRODUCT_TIME_LIST_FAILURE,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  PRODUCT_TIME_PUT_SUCCESS,
  PRODUCT_TIME_PUT_FAILURE,
  PRODUCT_TIME_LIST_REQUEST,
} from '../../../constants';
import { PRODUCT_TIME_UPDATE_IS_SUCCESS } from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* productTimeListRequest(
  getDtoQueryString,
  getProductTimeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getProductTimeApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: PRODUCT_TIME_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PRODUCT_TIME_LIST_FAILURE });
  }
}

export function* productTimePutRequest(putProductTimeApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    putProductTimeApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_TIME_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PRODUCT_TIME_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* productTimePutSuccess(history, message, action) {
  message.success(PRODUCT_TIME_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: PRODUCT_TIME_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
