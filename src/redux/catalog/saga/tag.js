import { select, all, call, put } from 'redux-saga/effects';
import {
  CHILD_TAG_LIST_SUCCESS,
  CHILD_TAG_LIST_FAILURE,
  TAG_LIST_SUCCESS,
  TAG_LIST_FAILURE,
  RELATED_TAG_LIST_FAILURE,
  RELATED_TAG_LIST_SUCCESS,
  SINGLE_TAG_INIT_FAILURE,
  SINGLE_TAG_INIT_SUCCESS,
  SINGLE_TAG_SUCCESS,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  TAG_POST_SUCCESS,
  TAG_POST_FAILURE,
  TAG_PUT_SUCCESS,
  TAG_PUT_FAILURE,
  TAG_DELETE_SUCCESS,
  TAG_DELETE_FAILURE,
  SINGLE_TAG_FAILURE,
  TAG_LIST_REQUEST,
  IMAGE_TAG_DELETE_SUCCESS,
  IMAGE_TAG_DELETE_FAILURE,
} from '../../../constants';
import {
  TAG_CREATE_IS_SUCCESS,
  TAG_UPDATE_IS_SUCCESS,
  TAG_DELETE_IS_SUCCESS,
  IMAGE_DELETE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* tagListRequest(getDtoQueryString, getTagApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getTagApi, currentSession.token, container);

  if (response.status === 200) {
    yield put({ type: TAG_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: TAG_LIST_FAILURE });
  }
}

export function* relatedTagListRequest(
  getDtoQueryString,
  getRelatedTagApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getRelatedTagApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: RELATED_TAG_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: RELATED_TAG_LIST_FAILURE });
  }
}

export function* tagPostRequest(postTagApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    postTagApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: TAG_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: TAG_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* tagPostSuccess(history, message, action) {
  message.success(TAG_CREATE_IS_SUCCESS, 5);
  yield put({
    type: TAG_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
export function* tagPutRequest(putTagApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    putTagApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: TAG_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: TAG_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* tagPutSuccess(history, message, action) {
  message.success(TAG_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: TAG_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* tagDeleteRequest(deleteTagApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    deleteTagApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: TAG_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: TAG_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* tagDeleteSuccess(history, message, action) {
  message.success(TAG_DELETE_IS_SUCCESS, 5);
  yield put({
    type: TAG_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* imageTagDeleteRequest(deleteImageTagApi, action) {
  const currentSession = yield select(getCurrentSession);

  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(
    deleteImageTagApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_TAG_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_TAG_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* imageTagDeleteSuccess(message, action) {
  message.success(IMAGE_DELETE_IS_SUCCESS, 5);
  yield put({
    type: TAG_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
