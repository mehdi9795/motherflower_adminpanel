import {
  RATE_POST_REQUEST,
  RATE_POST_SUCCESS,
  RATE_POST_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  ratePostRequest: false,
  ratePostData: getResponseModel,
  ratePostFailure: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Rate Post">
    case RATE_POST_REQUEST:
      return {
        ...state,
        ratePostRequest: true,
        ratePostFailure: false,
      };
    case RATE_POST_SUCCESS:
      return {
        ...state,
        ratePostData: action.data,
        ratePostRequest: false,
        ratePostFailure: false,
      };
    case RATE_POST_FAILURE:
      return {
        ...state,
        ratePostFailure: true,
        ratePostRequest: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
