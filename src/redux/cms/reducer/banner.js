import {
  BANNER_LIST_REQUEST,
  BANNER_LIST_SUCCESS,
  BANNER_LIST_FAILURE,
  BANNER_POST_REQUEST,
  BANNER_POST_SUCCESS,
  BANNER_POST_FAILURE,
  BANNER_PUT_REQUEST,
  BANNER_PUT_SUCCESS,
  BANNER_PUT_FAILURE,
  BANNER_DELETE_REQUEST,
  BANNER_DELETE_SUCCESS,
  BANNER_DELETE_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  bannerListRequest: false,
  bannerListData: getResponseModel,
  bannerListFailure: false,

  bannerPostRequest: false,
  bannerPostData: getResponseModel,

  bannerPutRequest: false,
  bannerPutData: getResponseModel,

  bannerDeleteRequest: false,
  bannerDeleteData: getResponseModel,

  bannerCRUDError: false,
  bannerErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Image List new">
    case BANNER_LIST_REQUEST:
      return {
        ...state,
        bannerListRequest: true,
        bannerListFailure: false,
      };
    case BANNER_LIST_SUCCESS:
      return {
        ...state,
        bannerListData: action.data,
        bannerListRequest: false,
        bannerListFailure: false,
      };
    case BANNER_LIST_FAILURE:
      return {
        ...state,
        bannerListFailure: true,
        bannerListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Image post">
    case BANNER_POST_REQUEST:
      return {
        ...state,
        bannerPostRequest: true,
        bannerCRUDError: false,
      };
    case BANNER_POST_SUCCESS:
      return {
        ...state,
        bannerPostData: action.data,
        bannerPostRequest: false,
        bannerCRUDError: false,
      };
    case BANNER_POST_FAILURE:
      return {
        ...state,
        bannerCRUDError: true,
        bannerPostRequest: false,
        bannerErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Image put">
    case BANNER_PUT_REQUEST:
      return {
        ...state,
        bannerPutRequest: true,
        bannerCRUDError: false,
      };
    case BANNER_PUT_SUCCESS:
      return {
        ...state,
        bannerPutData: action.data,
        bannerPutRequest: false,
        bannerCRUDError: false,
      };
    case BANNER_PUT_FAILURE:
      return {
        ...state,
        bannerCRUDError: true,
        bannerPutRequest: false,
        bannerErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Image delete">
    case BANNER_DELETE_REQUEST:
      return {
        ...state,
        bannerDeleteRequest: true,
        bannerCRUDError: false,
      };
    case BANNER_DELETE_SUCCESS:
      return {
        ...state,
        bannerDeleteData: action.data,
        bannerDeleteRequest: false,
        bannerCRUDError: false,
      };
    case BANNER_DELETE_FAILURE:
      return {
        ...state,
        bannerErrorMessage: action.data,
        bannerCRUDError: true,
        bannerDeleteRequest: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
