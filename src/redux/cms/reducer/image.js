import {
  IMAGE_LIST_REQUEST,
  IMAGE_LIST_SUCCESS,
  IMAGE_LIST_FAILURE,
  IMAGE_POST_REQUEST,
  IMAGE_POST_SUCCESS,
  IMAGE_POST_FAILURE,
  IMAGE_PUT_REQUEST,
  IMAGE_PUT_SUCCESS,
  IMAGE_PUT_FAILURE,
  IMAGE_DELETE_REQUEST,
  IMAGE_DELETE_SUCCESS,
  IMAGE_DELETE_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  imageListRequest: false,
  imageListData: getResponseModel,
  imageListFailure: false,

  imagePostRequest: false,
  imagePostData: getResponseModel,

  imagePutRequest: false,
  imagePutData: getResponseModel,

  imageDeleteRequest: false,
  imageDeleteData: getResponseModel,

  imageCRUDError: false,
  imageErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Image List new">
    case IMAGE_LIST_REQUEST:
      return {
        ...state,
        imageListRequest: true,
        imageListFailure: false,
      };
    case IMAGE_LIST_SUCCESS:
      return {
        ...state,
        imageListData: action.data,
        imageListRequest: false,
        imageListFailure: false,
      };
    case IMAGE_LIST_FAILURE:
      return {
        ...state,
        imageListFailure: true,
        imageListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Image post">
    case IMAGE_POST_REQUEST:
      return {
        ...state,
        imagePostRequest: true,
        imageCRUDError: false,
      };
    case IMAGE_POST_SUCCESS:
      return {
        ...state,
        imagePostData: action.data,
        imagePostRequest: false,
        imageCRUDError: false,
      };
    case IMAGE_POST_FAILURE:
      return {
        ...state,
        imageCRUDError: true,
        imagePostRequest: false,
        imageErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Image put">
    case IMAGE_PUT_REQUEST:
      return {
        ...state,
        imagePutRequest: true,
        imageCRUDError: false,
      };
    case IMAGE_PUT_SUCCESS:
      return {
        ...state,
        imagePutData: action.data,
        imagePutRequest: false,
        imageCRUDError: false,
      };
    case IMAGE_PUT_FAILURE:
      return {
        ...state,
        imageCRUDError: true,
        imagePutRequest: false,
        imageErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="Image delete">
    case IMAGE_DELETE_REQUEST:
      return {
        ...state,
        imageDeleteRequest: true,
        imageCRUDError: false,
      };
    case IMAGE_DELETE_SUCCESS:
      return {
        ...state,
        imageDeleteData: action.data,
        imageDeleteRequest: false,
        imageCRUDError: false,
      };
    case IMAGE_DELETE_FAILURE:
      return {
        ...state,
        imageErrorMessage: action.data,
        imageCRUDError: true,
        imageDeleteRequest: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
