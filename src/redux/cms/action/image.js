import {
  IMAGE_LIST_REQUEST,
  IMAGE_LIST_SUCCESS,
  IMAGE_LIST_FAILURE,
  IMAGE_POST_REQUEST,
  IMAGE_POST_SUCCESS,
  IMAGE_POST_FAILURE,
  IMAGE_PUT_REQUEST,
  IMAGE_PUT_SUCCESS,
  IMAGE_PUT_FAILURE,
  IMAGE_DELETE_REQUEST,
  IMAGE_DELETE_SUCCESS,
  IMAGE_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="image list">
export function imageListRequest(data) {
  return {
    type: IMAGE_LIST_REQUEST,
    data,
  };
}

export function imageListSuccess(data) {
  return {
    type: IMAGE_LIST_SUCCESS,
    data,
  };
}

export function imageListFailure() {
  return {
    type: IMAGE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="image Post">
export function imagePostRequest(data) {
  return {
    type: IMAGE_POST_REQUEST,
    data,
  };
}

export function imagePostSuccess() {
  return {
    type: IMAGE_POST_SUCCESS,
  };
}

export function imagePostFailure() {
  return {
    type: IMAGE_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="image Put">
export function imagePutRequest(data) {
  return {
    type: IMAGE_PUT_REQUEST,
    data,
  };
}

export function imagePutSuccess() {
  return {
    type: IMAGE_PUT_SUCCESS,
  };
}

export function imagePutFailure() {
  return {
    type: IMAGE_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Image Delete">
export function imageDeleteRequest(data) {
  return {
    type: IMAGE_DELETE_REQUEST,
    data,
  };
}

export function imageDeleteSuccess() {
  return {
    type: IMAGE_DELETE_SUCCESS,
  };
}

export function imageDeleteFailure() {
  return {
    type: IMAGE_DELETE_FAILURE,
  };
}
// </editor-fold>
