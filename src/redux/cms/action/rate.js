import {
  RATE_POST_REQUEST,
  RATE_POST_SUCCESS,
  RATE_POST_FAILURE,
} from '../../../constants/index';

export function ratePostRequest(data) {
  return {
    type: RATE_POST_REQUEST,
    data,
  };
}

export function ratePostSuccess(data) {
  return {
    type: RATE_POST_SUCCESS,
    data,
  };
}

export function ratePostFailure() {
  return {
    type: RATE_POST_FAILURE,
  };
}
