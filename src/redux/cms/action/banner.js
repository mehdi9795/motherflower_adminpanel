import {
  BANNER_LIST_REQUEST,
  BANNER_LIST_SUCCESS,
  BANNER_LIST_FAILURE,
  BANNER_POST_REQUEST,
  BANNER_POST_SUCCESS,
  BANNER_POST_FAILURE,
  BANNER_PUT_REQUEST,
  BANNER_PUT_SUCCESS,
  BANNER_PUT_FAILURE,
  BANNER_DELETE_REQUEST,
  BANNER_DELETE_SUCCESS,
  BANNER_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="banner list">
export function bannerListRequest(data) {
  return {
    type: BANNER_LIST_REQUEST,
    data,
  };
}

export function bannerListSuccess(data) {
  return {
    type: BANNER_LIST_SUCCESS,
    data,
  };
}

export function bannerListFailure() {
  return {
    type: BANNER_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="banner Post">
export function bannerPostRequest(data) {
  return {
    type: BANNER_POST_REQUEST,
    data,
  };
}

export function bannerPostSuccess() {
  return {
    type: BANNER_POST_SUCCESS,
  };
}

export function bannerPostFailure() {
  return {
    type: BANNER_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="banner Put">
export function bannerPutRequest(data) {
  return {
    type: BANNER_PUT_REQUEST,
    data,
  };
}

export function bannerPutSuccess() {
  return {
    type: BANNER_PUT_SUCCESS,
  };
}

export function bannerPutFailure() {
  return {
    type: BANNER_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Image Delete">
export function bannerDeleteRequest(data) {
  return {
    type: BANNER_DELETE_REQUEST,
    data,
  };
}

export function bannerDeleteSuccess() {
  return {
    type: BANNER_DELETE_SUCCESS,
  };
}

export function bannerDeleteFailure() {
  return {
    type: BANNER_DELETE_FAILURE,
  };
}
// </editor-fold>
