import { call, put, select } from 'redux-saga/effects';
import { RATE_POST_SUCCESS, RATE_POST_FAILURE } from '../../../constants';

const getCurrentSession = state => state.login.data;

export function* ratePostRequest(getRateApi, action) {
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    getRateApi,
    currentSession.token,
    action.data.data,
  );
  if (response) {
    yield put({ type: RATE_POST_SUCCESS, data: response.data });
  } else {
    yield put({ type: RATE_POST_FAILURE });
  }
}
