import { select, call, put } from 'redux-saga/effects';
import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  BANNER_DELETE_FAILURE,
  BANNER_DELETE_SUCCESS,
  BANNER_LIST_FAILURE,
  BANNER_LIST_REQUEST,
  BANNER_LIST_SUCCESS,
  BANNER_POST_FAILURE,
  BANNER_POST_SUCCESS,
  BANNER_PUT_FAILURE,
  BANNER_PUT_SUCCESS,
} from '../../../constants';
import {
  BANNER_CREATE_IS_SUCCESS,
  BANNER_DELETE_IS_SUCCESS,
  BANNER_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* bannerListRequest(getDtoQueryString, getImageApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getImageApi, currentSession.token, container);
  if (response) {
    yield put({ type: BANNER_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: BANNER_LIST_FAILURE });
  }
}

export function* bannerPostRequest(postBannerApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    postBannerApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: BANNER_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: BANNER_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* bannerPostSuccess(message, action) {
  message.success(BANNER_CREATE_IS_SUCCESS, 5);
  yield put({
    type: BANNER_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* bannerPutRequest(putBannerApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    putBannerApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: BANNER_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: BANNER_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* bannerPutSuccess(message, action) {
  message.success(BANNER_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: BANNER_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* bannerDeleteRequest(deleteImageApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteImageApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: BANNER_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: BANNER_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* bannerDeleteSuccess(history, message, action) {
  message.success(BANNER_DELETE_IS_SUCCESS, 5);
  yield put({
    type: BANNER_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
