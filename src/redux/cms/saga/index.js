import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import { getDtoQueryString } from '../../../utils/helper';
import {
  IMAGE_LIST_REQUEST,
  IMAGE_DELETE_REQUEST,
  IMAGE_DELETE_SUCCESS,
  IMAGE_POST_REQUEST,
  IMAGE_POST_SUCCESS,
  IMAGE_PUT_REQUEST,
  IMAGE_PUT_SUCCESS,
  RATE_POST_REQUEST,
  BANNER_LIST_REQUEST,
  BANNER_POST_REQUEST,
  BANNER_POST_SUCCESS,
  BANNER_PUT_REQUEST,
  BANNER_PUT_SUCCESS,
  BANNER_DELETE_REQUEST,
  BANNER_DELETE_SUCCESS,
  COMMENT_LIST_REQUEST,
  COMMENT_PUT_REQUEST,
  COMMENT_PUT_SUCCESS,
  COMMENT_DELETE_REQUEST,
  COMMENT_DELETE_SUCCESS,
} from '../../../constants';
import {
  imageDeleteRequest,
  imageDeleteSuccess,
  imageListRequest,
  imagePostRequest,
  imagePostSuccess,
  imagePutRequest,
  imagePutSuccess,
} from './image';
import {
  deleteImageApi,
  getImageApi,
  postImageApi,
  putImageApi,
  getRateApi,
  postRateApi,
  postBannerApi,
  putBannerApi,
  deleteBannerApi,
  getBannerApi, getCommentApi, putCommentApi, deleteCommentApi,
} from '../../../services/cmsApi';
import alert from '../../../components/CP/CPAlert';
import history from '../../../history';
import { ratePostRequest } from './rate';
import {
  bannerDeleteRequest,
  bannerDeleteSuccess,
  bannerListRequest,
  bannerPostRequest,
  bannerPostSuccess,
  bannerPutRequest,
  bannerPutSuccess,
} from './banner';
import {
  commentDeleteRequest,
  commentDeleteSuccess,
  commentListRequest,
  commentPutRequest,
  commentPutSuccess
} from "./comment";

export default function* sagaCommonIndex() {
  yield all([
    takeLatest(
      IMAGE_LIST_REQUEST,
      imageListRequest,
      getDtoQueryString,
      getImageApi,
    ),
    takeLatest(IMAGE_POST_REQUEST, imagePostRequest, postImageApi),
    takeLatest(RATE_POST_REQUEST, ratePostRequest, postRateApi),
    takeLatest(IMAGE_POST_SUCCESS, imagePostSuccess, history, message),
    takeLatest(IMAGE_PUT_REQUEST, imagePutRequest, putImageApi),
    takeLatest(IMAGE_PUT_SUCCESS, imagePutSuccess, history, message),
    takeLatest(IMAGE_DELETE_REQUEST, imageDeleteRequest, deleteImageApi),
    takeLatest(IMAGE_DELETE_SUCCESS, imageDeleteSuccess, history, message),

    takeLatest(
      BANNER_LIST_REQUEST,
      bannerListRequest,
      getDtoQueryString,
      getBannerApi,
    ),
    takeLatest(BANNER_POST_REQUEST, bannerPostRequest, postBannerApi),
    takeLatest(BANNER_POST_SUCCESS, bannerPostSuccess, message),
    takeLatest(BANNER_PUT_REQUEST, bannerPutRequest, putBannerApi),
    takeLatest(BANNER_PUT_SUCCESS, bannerPutSuccess, message),
    takeLatest(BANNER_DELETE_REQUEST, bannerDeleteRequest, deleteBannerApi),
    takeLatest(BANNER_DELETE_SUCCESS, bannerDeleteSuccess, history, message),

    takeLatest(
      COMMENT_LIST_REQUEST,
      commentListRequest,
      getDtoQueryString,
      getCommentApi,
    ),
    takeLatest(COMMENT_PUT_REQUEST, commentPutRequest, putCommentApi),
    takeLatest(COMMENT_PUT_SUCCESS, commentPutSuccess, message),
    takeLatest(COMMENT_DELETE_REQUEST, commentDeleteRequest, deleteCommentApi),
    takeLatest(COMMENT_DELETE_SUCCESS, commentDeleteSuccess, history, message),
  ]);
}
