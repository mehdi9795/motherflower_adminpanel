import { select, call, put } from 'redux-saga/effects';
import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  COMMENT_DELETE_FAILURE,
  COMMENT_DELETE_SUCCESS,
  COMMENT_LIST_FAILURE,
  COMMENT_LIST_REQUEST,
  COMMENT_LIST_SUCCESS,
  COMMENT_PUT_FAILURE,
  COMMENT_PUT_SUCCESS,
} from '../../../constants';
import {
  COMMENT_DELETE_IS_SUCCESS,
  COMMENT_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* commentListRequest(getDtoQueryString, getCommentApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getCommentApi, currentSession.token, container);
  if (response) {
    yield put({ type: COMMENT_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: COMMENT_LIST_FAILURE });
  }
}

export function* commentPutRequest(putCommentApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    putCommentApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COMMENT_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COMMENT_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* commentPutSuccess(message, action) {
  message.success(COMMENT_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: COMMENT_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* commentDeleteRequest(deleteCommentApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteCommentApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COMMENT_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: COMMENT_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* commentDeleteSuccess(history, message, action) {
  message.success(COMMENT_DELETE_IS_SUCCESS, 5);
  yield put({
    type: COMMENT_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
