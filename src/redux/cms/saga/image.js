import { select, call, put } from 'redux-saga/effects';
import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  IMAGE_DELETE_FAILURE,
  IMAGE_DELETE_SUCCESS,
  IMAGE_LIST_FAILURE,
  IMAGE_LIST_REQUEST,
  IMAGE_LIST_SUCCESS,
  IMAGE_POST_FAILURE,
  IMAGE_POST_SUCCESS,
  IMAGE_PUT_FAILURE,
  IMAGE_PUT_SUCCESS,
} from '../../../constants';
import {
  IMAGE_CREATE_IS_SUCCESS,
  IMAGE_DELETE_IS_SUCCESS,
  IMAGE_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* imageListRequest(getDtoQueryString, getImageApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getImageApi, currentSession.token, container);
  if (response) {
    yield put({ type: IMAGE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: IMAGE_LIST_FAILURE });
  }
}

export function* imagePostRequest(postImageApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    postImageApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* imagePostSuccess(history, message, action) {
  message.success(IMAGE_CREATE_IS_SUCCESS, 5);
  yield put({
    type: IMAGE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* imagePutRequest(putImageApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    putImageApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* imagePutSuccess(history, message, action) {
  message.success(IMAGE_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: IMAGE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* imageDeleteRequest(deleteImageApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteImageApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: IMAGE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* imageDeleteSuccess(history, message, action) {
  message.success(IMAGE_DELETE_IS_SUCCESS, 5);
  yield put({
    type: IMAGE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
