import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import {
  vendorAgentDeleteRequest,
  vendorAgentPostRequest,
  vendorAgentDeleteSuccess,
  vendorAgentPostSuccess,
  vendorAgentPutRequest,
  vendorAgentPutSuccess,
  vendorListRequest,
  vendorUserListRequest,
} from './agent';
import {
  vendorBranchListRequest,
  vendorFacilityListRequest,
  vendorBranchPostRequest,
  vendorBranchPostSuccess,
  vendorBranchPutRequest,
  vendorBranchPutSuccess,
  vendorBranchDeleteRequest,
  vendorBranchDeleteSuccess,
  vendorBranchCategoryListRequest,
  calendarExceptionRequest, availableProductShiftRequest,
} from './vendorBranch';

import {
  vendorBranchOffShiftDeleteRequest,
  vendorBranchOffShiftDeleteSuccess,
  vendorBranchOffShiftListRequest,
  vendorBranchOffShiftPostRequest,
  vendorBranchOffShiftPostSuccess,
} from './vendorBranchOffShift';
import {
  vendorBranchUserDeleteRequest,
  vendorBranchUserDeleteSuccess,
  vendorBranchUserListRequest,
  vendorBranchUserPostRequest,
  vendorBranchUserPostSuccess,
  vendorBranchUserPutRequest,
  vendorBranchUserPutSuccess,
} from './vendorBranchUser';
import {
  facilityListRequest,
  vendorFacilityPostRequest,
  vendorFacilityPostSuccess,
} from './facility';

import {
  getVendorBranchApi,
  getVendorBranchFacilityApi,
  getAgentApi,
  getOffShiftApi,
  getVendorBranchUserApi,
  getFacilityApi,
  postVendorBranchApi,
  putVendorBranchApi,
  deleteVendorBranchApi,
  postOffShiftApi,
  deleteOffShiftApi,
  getAgentUserApi,
  postVendorBranchUserApi,
  putVendorBranchUserApi,
  deleteVendorBranchUserApi,
  postVendorBranchFacilityApi,
  getVendorBranchCategoryApi,
  putAgentApi,
  postAgentApi,
  deleteAgentApi,
  getVendorBranchZoneApi,
  postVendorBranchZoneApi,
  deleteVendorBranchZoneApi,
  getVendorAffiliateApi,
  putVendorAffiliateApi,
  deleteVendorAffiliateApi,
  postVendorBranchCitiesApi,
  getCalendarExceptionApi, getAvailableProductShiftApi,
} from '../../../services/vendorApi';
import history from '../../../history';
import {
  VENDOR_BRANCH_LIST_REQUEST,
  VENDOR_FACILITY_LIST_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST,
  VENDOR_BRANCH_USER_LIST_REQUEST,
  FACILITY_LIST_REQUEST,
  VENDOR_LIST_REQUEST,
  VENDOR_FACILITY_POST_REQUEST,
  VENDOR_FACILITY_POST_SUCCESS,
  VENDOR_BRANCH_DELETE_REQUEST,
  VENDOR_BRANCH_DELETE_SUCCESS,
  VENDOR_BRANCH_POST_REQUEST,
  VENDOR_BRANCH_POST_SUCCESS,
  VENDOR_BRANCH_PUT_REQUEST,
  VENDOR_BRANCH_PUT_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_POST_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_POST_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_SUCCESS,
  VENDOR_USER_LIST_REQUEST,
  VENDOR_BRANCH_USER_POST_REQUEST,
  VENDOR_BRANCH_USER_POST_SUCCESS,
  VENDOR_BRANCH_USER_DELETE_REQUEST,
  VENDOR_BRANCH_USER_DELETE_SUCCESS,
  VENDOR_BRANCH_USER_PUT_REQUEST,
  VENDOR_BRANCH_USER_PUT_SUCCESS,
  VENDOR_BRANCH_CATEGORY_LIST_REQUEST,
  VENDOR_AGENT_DELETE_REQUEST,
  VENDOR_AGENT_DELETE_SUCCESS,
  VENDOR_AGENT_POST_SUCCESS,
  VENDOR_AGENT_PUT_REQUEST,
  VENDOR_AGENT_PUT_SUCCESS,
  VENDOR_AGENT_POST_REQUEST,
  VENDOR_BRANCH_ZONE_DELETE_REQUEST,
  VENDOR_BRANCH_ZONE_DELETE_SUCCESS,
  VENDOR_BRANCH_ZONE_LIST_REQUEST,
  VENDOR_BRANCH_ZONE_POST_REQUEST,
  VENDOR_BRANCH_ZONE_POST_SUCCESS,
} from '../../../constants/';
import { getDtoQueryString } from '../../../utils/helper';
import alert from '../../../components/CP/CPAlert';
import {
  vendorBranchZoneDeleteRequest,
  vendorBranchZoneDeleteSuccess,
  vendorBranchZoneListRequest,
  vendorBranchZonePostRequest,
  vendorBranchZonePostSuccess,
} from './vendorBranchZone';
import {
  AVAILABLE_PRODUCT_SHIFT_REQUEST,
  CALENDAR_EXCEPTION_REQUEST,
  VENDOR_AFFILIATE_DELETE_REQUEST,
  VENDOR_AFFILIATE_DELETE_SUCCESS,
  VENDOR_AFFILIATE_LIST_REQUEST,
  VENDOR_AFFILIATE_PUT_REQUEST,
  VENDOR_AFFILIATE_PUT_SUCCESS,
} from '../../../constants';
import {
  vendorAffiliateDeleteRequest,
  vendorAffiliateDeleteSuccess,
  vendorAffiliateListRequest,
  vendorAffiliatePutRequest,
  vendorAffiliatePutSuccess,
} from './vendorAffiliate';

// main saga generators
export default function* sagaVendorIndex() {
  // yield takeLatest(testRequestFlow);
  // yield all(takeLatest(testRequestFlow));
  yield all([
    // <editor-fold dsc="vendorBranch">

    takeLatest(
      VENDOR_BRANCH_LIST_REQUEST,
      vendorBranchListRequest,
      getDtoQueryString,
      getVendorBranchApi,
    ),
    takeLatest(
      VENDOR_FACILITY_LIST_REQUEST,
      vendorFacilityListRequest,
      getDtoQueryString,
      getVendorBranchFacilityApi,
    ),
    takeLatest(
      VENDOR_BRANCH_POST_REQUEST,
      vendorBranchPostRequest,
      postVendorBranchApi,
      message,
      alert,
    ),
    takeLatest(
      VENDOR_BRANCH_POST_SUCCESS,
      vendorBranchPostSuccess,
      history,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_PUT_REQUEST,
      vendorBranchPutRequest,
      putVendorBranchApi,
      postVendorBranchCitiesApi,
    ),
    takeLatest(
      VENDOR_BRANCH_PUT_SUCCESS,
      vendorBranchPutSuccess,
      history,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_DELETE_REQUEST,
      vendorBranchDeleteRequest,
      deleteVendorBranchApi,
    ),
    takeLatest(
      VENDOR_BRANCH_DELETE_SUCCESS,
      vendorBranchDeleteSuccess,
      history,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_CATEGORY_LIST_REQUEST,
      vendorBranchCategoryListRequest,
      getDtoQueryString,
      getVendorBranchCategoryApi,
    ),
    // takeEvery(
    //   SINGLE_VENDOR_BRANCH_INIT_REQUEST,
    //   vendorBranchSingleInitRequest,
    //   getDtoQueryString,
    //   getVendorBranchApi,
    //   getAgentApi,
    //   getCityApi,
    //   getCategoryApi,
    // ),

    // </editor-fold>
    // <editor-fold dsc="off shift">

    takeLatest(
      VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST,
      vendorBranchOffShiftListRequest,
      getDtoQueryString,
      getOffShiftApi,
    ),
    takeLatest(
      VENDOR_BRANCH_OFF_SHIFT_POST_REQUEST,
      vendorBranchOffShiftPostRequest,
      postOffShiftApi,
    ),
    takeLatest(
      VENDOR_BRANCH_OFF_SHIFT_POST_SUCCESS,
      vendorBranchOffShiftPostSuccess,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_OFF_SHIFT_DELETE_REQUEST,
      vendorBranchOffShiftDeleteRequest,
      deleteOffShiftApi,
    ),
    takeLatest(
      VENDOR_BRANCH_OFF_SHIFT_DELETE_SUCCESS,
      vendorBranchOffShiftDeleteSuccess,
      message,
    ),

    // </editor-fold>
    // <editor-fold dsc="facility">

    takeLatest(
      FACILITY_LIST_REQUEST,
      facilityListRequest,
      getDtoQueryString,
      getFacilityApi,
    ),
    takeLatest(
      VENDOR_FACILITY_POST_REQUEST,
      vendorFacilityPostRequest,
      postVendorBranchFacilityApi,
    ),
    takeLatest(
      VENDOR_FACILITY_POST_SUCCESS,
      vendorFacilityPostSuccess,
      message,
    ),

    // </editor-fold>
    // <editor-fold dsc="agent">

    takeLatest(
      VENDOR_LIST_REQUEST,
      vendorListRequest,
      getDtoQueryString,
      getAgentApi,
    ),

    takeLatest(VENDOR_AGENT_POST_REQUEST, vendorAgentPostRequest, postAgentApi),
    takeLatest(
      VENDOR_AGENT_POST_SUCCESS,
      vendorAgentPostSuccess,
      history,
      message,
    ),
    takeLatest(VENDOR_AGENT_PUT_REQUEST, vendorAgentPutRequest, putAgentApi),
    takeLatest(
      VENDOR_AGENT_PUT_SUCCESS,
      vendorAgentPutSuccess,
      history,
      message,
    ),
    takeLatest(
      VENDOR_AGENT_DELETE_REQUEST,
      vendorAgentDeleteRequest,
      deleteAgentApi,
    ),
    takeLatest(
      VENDOR_AGENT_DELETE_SUCCESS,
      vendorAgentDeleteSuccess,
      history,
      message,
    ),

    // takeEvery(
    //   SINGLE_VENDOR_AGENT_INIT_REQUEST,
    //   agentSingleInitRequest,
    //   getDtoQueryString,
    //   getAgentApi,
    //   getProductTypesApi,
    // ),
    takeLatest(
      VENDOR_USER_LIST_REQUEST,
      vendorUserListRequest,
      getDtoQueryString,
      getAgentUserApi,
    ),

    // </editor-fold>
    // <editor-fold dsc="vendor branch user">
    takeLatest(
      VENDOR_BRANCH_USER_LIST_REQUEST,
      vendorBranchUserListRequest,
      getDtoQueryString,
      getVendorBranchUserApi,
    ),
    takeLatest(
      VENDOR_BRANCH_USER_POST_REQUEST,
      vendorBranchUserPostRequest,
      postVendorBranchUserApi,
    ),
    takeLatest(
      VENDOR_BRANCH_USER_POST_SUCCESS,
      vendorBranchUserPostSuccess,
      history,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_USER_PUT_REQUEST,
      vendorBranchUserPutRequest,
      putVendorBranchUserApi,
    ),
    takeLatest(
      VENDOR_BRANCH_USER_PUT_SUCCESS,
      vendorBranchUserPutSuccess,
      history,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_USER_DELETE_REQUEST,
      vendorBranchUserDeleteRequest,
      deleteVendorBranchUserApi,
    ),
    takeLatest(
      VENDOR_BRANCH_USER_DELETE_SUCCESS,
      vendorBranchUserDeleteSuccess,
      message,
    ),

    // </editor-fold>
    // <editor-fold dsc="vendor branch zone">
    takeLatest(
      VENDOR_BRANCH_ZONE_LIST_REQUEST,
      vendorBranchZoneListRequest,
      getDtoQueryString,
      getVendorBranchZoneApi,
    ),
    takeLatest(
      VENDOR_BRANCH_ZONE_POST_REQUEST,
      vendorBranchZonePostRequest,
      postVendorBranchZoneApi,
    ),
    takeLatest(
      VENDOR_BRANCH_ZONE_POST_SUCCESS,
      vendorBranchZonePostSuccess,
      history,
      message,
    ),
    takeLatest(
      VENDOR_BRANCH_ZONE_DELETE_REQUEST,
      vendorBranchZoneDeleteRequest,
      deleteVendorBranchZoneApi,
    ),
    takeLatest(
      VENDOR_BRANCH_ZONE_DELETE_SUCCESS,
      vendorBranchZoneDeleteSuccess,
      message,
    ),

    // </editor-fold>

    // <editor-fold dsc="vendor affiliate">

    takeLatest(
      VENDOR_AFFILIATE_LIST_REQUEST,
      vendorAffiliateListRequest,
      getDtoQueryString,
      getVendorAffiliateApi,
    ),

    takeLatest(
      VENDOR_AFFILIATE_PUT_REQUEST,
      vendorAffiliatePutRequest,
      putVendorAffiliateApi,
    ),
    takeLatest(
      VENDOR_AFFILIATE_PUT_SUCCESS,
      vendorAffiliatePutSuccess,
      history,
      message,
    ),
    takeLatest(
      VENDOR_AFFILIATE_DELETE_REQUEST,
      vendorAffiliateDeleteRequest,
      deleteVendorAffiliateApi,
    ),
    // </editor-fold>
    takeLatest(
      CALENDAR_EXCEPTION_REQUEST,
      calendarExceptionRequest,
      getDtoQueryString,
      getCalendarExceptionApi,
    ),
    takeLatest(
      AVAILABLE_PRODUCT_SHIFT_REQUEST,
      availableProductShiftRequest,
      getDtoQueryString,
      getAvailableProductShiftApi,
    ),
  ]);
}
