import { call, put, select } from 'redux-saga/effects';
import {
  FACILITY_LIST_SUCCESS,
  FACILITY_LIST_FAILURE,
  VENDOR_FACILITY_LIST_SUCCESS,
  VENDOR_FACILITY_LIST_FAILURE,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  VENDOR_FACILITY_POST_FAILURE,
  VENDOR_FACILITY_POST_SUCCESS,
} from '../../../constants';
import { VENDOR_BRANCH_FACILITY_UPDATE_IS_SUCCESS } from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* facilityListRequest(
  getDtoQueryString,
  getFacilityApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getFacilityApi, currentSession.token, container);

  if (response) {
    yield put({ type: FACILITY_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: FACILITY_LIST_FAILURE });
  }
}

export function* vendorFacilityListRequest(
  getDtoQueryString,
  getVendorFacilityApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVendorFacilityApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: VENDOR_FACILITY_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_FACILITY_LIST_FAILURE });
  }
}

export function* vendorFacilityPostRequest(postVendorFacilityApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postVendorFacilityApi,
    currentSession.token,
    action.dto,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_FACILITY_POST_SUCCESS,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_FACILITY_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function vendorFacilityPostSuccess(message) {
  message.success(VENDOR_BRANCH_FACILITY_UPDATE_IS_SUCCESS, 10);
}
