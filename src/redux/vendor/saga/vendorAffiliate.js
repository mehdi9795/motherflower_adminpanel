import { call, put, select } from 'redux-saga/effects';
import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  VENDOR_AFFILIATE_DELETE_FAILURE,
  VENDOR_AFFILIATE_DELETE_SUCCESS,
  VENDOR_AFFILIATE_LIST_FAILURE,
  VENDOR_AFFILIATE_LIST_REQUEST,
  VENDOR_AFFILIATE_LIST_SUCCESS,
  VENDOR_AFFILIATE_PUT_FAILURE,
  VENDOR_AFFILIATE_PUT_SUCCESS,
} from '../../../constants';
import {
  VENDOR_AFFILIATE_DELETE_IS_SUCCESS,
  VENDOR_AFFILIATE_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* vendorAffiliateListRequest(
  getDtoQueryString,
  getVendorAffiliateApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVendorAffiliateApi,
    currentSession.token,
    container,
  );

  if (response.status === 200) {
    yield put({ type: VENDOR_AFFILIATE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_AFFILIATE_LIST_FAILURE });
  }
}

export function* vendorAffiliatePutRequest(putVendorAffiliateApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putVendorAffiliateApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_AFFILIATE_PUT_SUCCESS,
      data: { status: action.dto.status },
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_AFFILIATE_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function vendorAffiliatePutSuccess(history, message, action) {
  if (action.data.status === 'saveAndContinue') {
    message.success(VENDOR_AFFILIATE_UPDATE_IS_SUCCESS, 10);
  } else {
    message.success(VENDOR_AFFILIATE_UPDATE_IS_SUCCESS, 10);
    history.push('/vendor-affiliate/list');
  }
}

export function* vendorAffiliateDeleteRequest(
  deleteVendorAffiliateApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteVendorAffiliateApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_AFFILIATE_DELETE_SUCCESS,
      data: { status: action.dto.status },
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_AFFILIATE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vendorAffiliateDeleteSuccess(history, message, action) {
  message.success(VENDOR_AFFILIATE_DELETE_IS_SUCCESS, 10);
  if (!action.data.status === 'edit') {
    // redirect to back url
    history.push('/vendor-affiliate/list');
  } else {
    yield put({
      type: VENDOR_AFFILIATE_LIST_REQUEST,
      dto: '{dto:{active:true}}',
    });
  }
}
