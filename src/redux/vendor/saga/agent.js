import { all, call, put, select } from 'redux-saga/effects';
import {
  VENDOR_LIST_SUCCESS,
  VENDOR_LIST_FAILURE,
  PRODUCT_TYPES_LIST_SUCCESS,
  SINGLE_USER_INIT_FAILURE,
  SINGLE_VENDOR_AGENT_INIT_SUCCESS,
  SINGLE_VENDOR_AGENT_SUCCESS,
  VENDOR_USER_LIST_FAILURE,
  VENDOR_USER_LIST_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  VENDOR_AGENT_DELETE_SUCCESS,
  VENDOR_AGENT_POST_FAILURE,
  VENDOR_AGENT_POST_SUCCESS,
  VENDOR_AGENT_PUT_FAILURE,
  VENDOR_AGENT_PUT_SUCCESS,
  VENDOR_BRANCH_DELETE_FAILURE,
  VENDOR_LIST_REQUEST,
} from '../../../constants/index';
import {
  VENDOR_CREATE_IS_SUCCESS,
  VENDOR_DELETE_SUCCESS,
  VENDOR_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* vendorListRequest(getDtoQueryString, getAgentApi, action) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getAgentApi, currentSession.token, container);
  if (response) {
    yield put({ type: VENDOR_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_LIST_FAILURE });
  }
}

export function* agentSingleInitRequest(
  getDtoQueryString,
  getAgentApi,
  getProductTypeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const [agent, productType] = yield all([
    call(
      getAgentApi,
      currentSession.token,
      getDtoQueryString(action.dto.agentDto),
    ),
    call(
      getProductTypeApi,
      currentSession.token,
      getDtoQueryString(action.dto.productTypeDto),
    ),
  ]);
  if (agent && productType) {
    yield put({ type: SINGLE_VENDOR_AGENT_SUCCESS, data: agent.data });
    yield put({ type: PRODUCT_TYPES_LIST_SUCCESS, data: productType.data });

    yield put({ type: SINGLE_VENDOR_AGENT_INIT_SUCCESS });
  } else {
    yield put({ type: SINGLE_USER_INIT_FAILURE });
  }
}

export function* vendorUserListRequest(
  getDtoQueryString,
  getAgentUserApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getAgentUserApi, currentSession.token, container);
  if (response) {
    yield put({ type: VENDOR_USER_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_USER_LIST_FAILURE });
  }
}

export function* vendorAgentPostRequest(postVendorAgentApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postVendorAgentApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto, id: response.data.id };
    }
    yield put({
      type: VENDOR_AGENT_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_AGENT_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });

    // message.error(response.data.errorMessage, 30);
  }
}
export function* vendorAgentPostSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue') {
    message.success(VENDOR_CREATE_IS_SUCCESS, 10);
    yield put({
      type: VENDOR_LIST_REQUEST,
      data: action.data.data.listDto,
    });
    history.push(`/vendor/edit/${action.data.id}`);
  } else {
    message.success(VENDOR_CREATE_IS_SUCCESS, 10);
    history.push('/vendor/list');
  }
}
export function* vendorAgentPutRequest(putVendorAgentApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putVendorAgentApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_AGENT_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_AGENT_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function vendorAgentPutSuccess(history, message, action) {
  if (action.data.status === 'saveAndContinue') {
    message.success(VENDOR_UPDATE_IS_SUCCESS, 10);
    // yield put({
    //   type: VENDOR_LIST_REQUEST,
    //   data: action.data.listDto,
    // });
  } else {
    message.success(VENDOR_UPDATE_IS_SUCCESS, 10);
    history.push('/vendor/list');
  }
}

export function* vendorAgentDeleteRequest(deleteVendorAgentApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteVendorAgentApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_AGENT_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorAgentDeleteSuccess(history, message, action) {
  message.success(VENDOR_DELETE_SUCCESS, 10);
  if (action.data.status === 'edit') {
    history.push('/vendor/list');
  } else {
    yield put({
      type: VENDOR_LIST_REQUEST,
      dto: action.data.listDto,
    });
  }
}
