import { call, put, select } from 'redux-saga/effects';
import {
  VENDOR_BRANCH_ZONE_LIST_FAILURE,
  VENDOR_BRANCH_ZONE_LIST_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  VENDOR_BRANCH_ZONE_POST_FAILURE,
  VENDOR_BRANCH_ZONE_POST_SUCCESS,
  VENDOR_BRANCH_ZONE_LIST_REQUEST,
  VENDOR_BRANCH_ZONE_DELETE_SUCCESS,
  VENDOR_BRANCH_ZONE_DELETE_FAILURE,
  VENDOR_BRANCH_LIST_REQUEST,
} from '../../../constants';
import {
  VENDOR_BRANCH_ZONE_CREATE_IS_SUCCESS,
  VENDOR_BRANCH_ZONE_DELETE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* vendorBranchZoneListRequest(
  getDtoQueryString,
  getVendorBranchZoneApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVendorBranchZoneApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: VENDOR_BRANCH_ZONE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_BRANCH_ZONE_LIST_FAILURE });
  }
}

export function* vendorBranchZonePostRequest(postVendorBranchZoneApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postVendorBranchZoneApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_ZONE_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_ZONE_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchZonePostSuccess(history, message, action) {
  yield put({
    type: VENDOR_BRANCH_ZONE_LIST_REQUEST,
    dto: action.data.vendorBranchZoneListDto,
  });
  yield put({
    type: VENDOR_BRANCH_LIST_REQUEST,
    dto: action.data.vendorBranchListDto,
  });
  message.success(VENDOR_BRANCH_ZONE_CREATE_IS_SUCCESS, 10);
}

export function* vendorBranchZoneDeleteRequest(
  deleteVendorBranchZoneApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteVendorBranchZoneApi,
    currentSession.token,
    action.dto.id,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_ZONE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_ZONE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchZoneDeleteSuccess(message, action) {
  yield put({
    type: VENDOR_BRANCH_ZONE_LIST_REQUEST,
    dto: action.data.vendorBranchZoneListDto,
  });
  yield put({
    type: VENDOR_BRANCH_LIST_REQUEST,
    dto: action.data.vendorBranchListDto,
  });
  message.success(VENDOR_BRANCH_ZONE_DELETE_IS_SUCCESS, 10);
}
