import { call, put, select } from 'redux-saga/effects';
import {
  VENDOR_BRANCH_OFF_SHIFT_LIST_FAILURE,
  VENDOR_BRANCH_OFF_SHIFT_LIST_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_POST_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_POST_FAILURE,
  VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_FAILURE,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_SUCCESS,
} from '../../../constants';
import {
  VENDOR_BRANCH_OFF_SHIFT_DELETE_IS_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* vendorBranchOffShiftListRequest(
  getDtoQueryString,
  getOffShiftApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getOffShiftApi, currentSession.token, container);

  if (response) {
    yield put({
      type: VENDOR_BRANCH_OFF_SHIFT_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: VENDOR_BRANCH_OFF_SHIFT_LIST_FAILURE });
  }
}

export function* vendorBranchOffShiftPostRequest(postOffShiftApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postOffShiftApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_OFF_SHIFT_POST_SUCCESS,
      data: action.dto.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_OFF_SHIFT_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vendorBranchOffShiftPostSuccess(message, action) {
  yield put({ type: VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST, dto: action.data });

  message.success(VENDOR_BRANCH_OFF_SHIFT_IS_SUCCESS, 10);
}

export function* vendorBranchOffShiftDeleteRequest(deleteOffShiftApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteOffShiftApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_OFF_SHIFT_DELETE_SUCCESS,
      data: action.dto.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_OFF_SHIFT_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vendorBranchOffShiftDeleteSuccess(message, action) {
  yield put({ type: VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST, dto: action.data });

  message.success(VENDOR_BRANCH_OFF_SHIFT_DELETE_IS_SUCCESS, 10);
}
