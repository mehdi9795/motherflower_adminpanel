import { all, call, put, select } from 'redux-saga/effects';
import {
  VENDOR_BRANCH_LIST_FAILURE,
  VENDOR_BRANCH_LIST_SUCCESS,
  SINGLE_VENDOR_BRANCH_SUCCESS,
  VENDOR_FACILITY_LIST_SUCCESS,
  VENDOR_FACILITY_LIST_FAILURE,
  VENDOR_LIST_SUCCESS,
  CITY_LIST_SUCCESS,
  SECONDARY_LEVEL_LIST_SUCCESS,
  SINGLE_VENDOR_BRANCH_INIT_SUCCESS,
  SINGLE_VENDOR_BRANCH_INIT_FAILURE,
  VENDOR_BRANCH_CATEGORY_LIST_SUCCESS,
  VENDOR_BRANCH_CATEGORY_LIST_FAILURE,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  VENDOR_BRANCH_POST_FAILURE,
  VENDOR_BRANCH_POST_SUCCESS,
  VENDOR_BRANCH_LIST_REQUEST,
  VENDOR_BRANCH_PUT_SUCCESS,
  VENDOR_BRANCH_PUT_FAILURE,
  VENDOR_BRANCH_DELETE_SUCCESS,
  VENDOR_BRANCH_DELETE_FAILURE,
  CALENDAR_EXCEPTION_SUCCESS,
  CALENDAR_EXCEPTION_REQUEST,
  CALENDAR_EXCEPTION_FAILURE,
  AVAILABLE_PRODUCT_SHIFT_SUCCESS,
  AVAILABLE_PRODUCT_SHIFT_FAILURE,
} from '../../../constants';
import {
  VENDOR_BRANCH_CREATE_IS_SUCCESS,
  VENDOR_BRANCH_DELETE_IS_SUCCESS,
  VENDOR_BRANCH_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* vendorBranchListRequest(
  getDtoQueryString,
  getVendorBranchApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVendorBranchApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: VENDOR_BRANCH_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_BRANCH_LIST_FAILURE });
  }
}

export function* vendorBranchSingleInitRequest(
  getDtoQueryString,
  getVendorBranchApi,
  getAgentApi,
  getCityApi,
  getCategoryApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const [vendorBranch, agents, cities, categories] = yield all([
    action.dto.vendorBranchDto
      ? call(
          getVendorBranchApi,
          currentSession.token,
          getDtoQueryString(action.dto.vendorBranchDto),
        )
      : undefined,
    call(
      getAgentApi,
      currentSession.token,
      getDtoQueryString(action.dto.vendorDto),
    ),
    call(
      getCityApi,
      currentSession.token,
      getDtoQueryString(action.dto.cityDto),
    ),
    call(
      getCategoryApi,
      currentSession.token,
      getDtoQueryString(action.dto.categoryDto),
    ),
  ]);

  if (
    vendorBranch === undefined &&
    action.dto.vendorBranchDto === undefined &&
    agents &&
    cities &&
    categories
  ) {
    if (vendorBranch) {
      yield put({
        type: SINGLE_VENDOR_BRANCH_SUCCESS,
        data: vendorBranch.data,
      });
    }
    yield put({ type: VENDOR_LIST_SUCCESS, data: agents.data });
    yield put({ type: CITY_LIST_SUCCESS, data: cities.data });
    yield put({ type: SECONDARY_LEVEL_LIST_SUCCESS, data: categories.data });

    yield put({ type: SINGLE_VENDOR_BRANCH_INIT_SUCCESS });
  } else {
    yield put({ type: SINGLE_VENDOR_BRANCH_INIT_FAILURE });
  }
}
export function* vendorFacilityListRequest(
  getDtoQueryString,
  getFacilityApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getFacilityApi, currentSession.token, container);
  if (response) {
    yield put({ type: VENDOR_FACILITY_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_FACILITY_LIST_FAILURE });
  }
}

export function* vendorBranchPostRequest(
  postVendorBranchApi,
  message,
  alert,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  // yield new Promise(resolve => setTimeout(resolve, 1000));
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postVendorBranchApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto };
    }
    yield put({
      type: VENDOR_BRANCH_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function vendorBranchPostSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue') {
    message.success(VENDOR_BRANCH_CREATE_IS_SUCCESS, 10);
    history.push(`/vendor-branch/edit/${action.data.id}`);
  } else {
    message.success(VENDOR_BRANCH_CREATE_IS_SUCCESS, 10);
    history.push('/vendor-branch/list');
  }
}

export function* vendorBranchPutRequest(
  putVendorBranchApi,
  postVendorBranchCitiesApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putVendorBranchApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield call(
      postVendorBranchCitiesApi,
      currentSession.token,
      action.dto.coveredCities,
    );
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PUT_SUCCESS,
      data: { status: action.dto.status },
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function vendorBranchPutSuccess(history, message, action) {
  if (action.data.status === 'saveAndContinue') {
    message.success(VENDOR_BRANCH_UPDATE_IS_SUCCESS, 10);
  } else {
    message.success(VENDOR_BRANCH_UPDATE_IS_SUCCESS, 10);
    history.push('/vendor-branch/list');
  }
}

export function* vendorBranchDeleteRequest(deleteVendorBranchApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteVendorBranchApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_DELETE_SUCCESS,
      data: { status: action.dto.status },
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* vendorBranchDeleteSuccess(history, message, action) {
  message.success(VENDOR_BRANCH_DELETE_IS_SUCCESS, 10);
  if (action.data.status === 'edit') {
    // redirect to back url
    history.push('/vendor-branch/list');
  } else {
    yield put({
      type: VENDOR_BRANCH_LIST_REQUEST,
      dto: '{dto:{active:true}}',
    });
  }
}

export function* vendorBranchCategoryListRequest(
  getDtoQueryString,
  getVendorBranchCategoryApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVendorBranchCategoryApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({
      type: VENDOR_BRANCH_CATEGORY_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: VENDOR_BRANCH_CATEGORY_LIST_FAILURE });
  }
}


export function* calendarExceptionRequest(
  getDtoQueryString,
  getCalendarExceptionApi,
  action,
) {
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getCalendarExceptionApi,
    container,
  );
  if (response) {
    yield put({
      type: CALENDAR_EXCEPTION_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: CALENDAR_EXCEPTION_FAILURE });
  }
}

export function* availableProductShiftRequest(
  getDtoQueryString,
  getAvailableProductShiftApi,
  action,
) {
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getAvailableProductShiftApi,
    container,
  );
  if (response) {
    yield put({
      type: AVAILABLE_PRODUCT_SHIFT_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: AVAILABLE_PRODUCT_SHIFT_FAILURE });
  }
}
