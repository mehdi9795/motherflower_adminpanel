import { call, put, select } from 'redux-saga/effects';
import {
  VENDOR_BRANCH_USER_LIST_FAILURE,
  VENDOR_BRANCH_USER_LIST_SUCCESS,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  VENDOR_BRANCH_USER_POST_FAILURE,
  VENDOR_BRANCH_USER_POST_SUCCESS,
  VENDOR_BRANCH_USER_LIST_REQUEST,
  VENDOR_USER_LIST_REQUEST,
  USER_ROLE_REQUEST,
  VENDOR_BRANCH_USER_PUT_SUCCESS,
  VENDOR_BRANCH_USER_PUT_FAILURE,
  VENDOR_BRANCH_USER_DELETE_SUCCESS,
  VENDOR_BRANCH_USER_DELETE_FAILURE,
} from '../../../constants';
import {
  VENDOR_BRANCH_USER_CREATE_IS_SUCCESS,
  VENDOR_BRANCH_USER_DELETE_IS_SUCCESS,
  VENDOR_BRANCH_USER_EDIT_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* vendorBranchUserListRequest(
  getDtoQueryString,
  getVendorBranchUserApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getVendorBranchUserApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: VENDOR_BRANCH_USER_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_BRANCH_USER_LIST_FAILURE });
  }
}

export function* vendorBranchUserPostRequest(postVendorBranchUserApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postVendorBranchUserApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_USER_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_USER_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchUserPostSuccess(history, message, action) {
  if (action.data.status === 'fromIdentity') {
    yield put({
      type: USER_ROLE_REQUEST,
      dto: action.data.listDto,
    });
  } else {
    yield put({
      type: VENDOR_BRANCH_USER_LIST_REQUEST,
      dto: action.data.vendorBranchUserDto,
    });
    yield put({
      type: VENDOR_USER_LIST_REQUEST,
      dto: action.data.vendorUserDto,
    });
    message.success(VENDOR_BRANCH_USER_CREATE_IS_SUCCESS, 10);
  }
}

export function* vendorBranchUserPutRequest(putVendorBranchUserApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putVendorBranchUserApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_USER_PUT_SUCCESS,
      data: action.dto.vendorBranchUserdto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_USER_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchUserPutSuccess(history, message, action) {
  yield put({
    type: VENDOR_BRANCH_USER_LIST_REQUEST,
    dto: action.data,
  });
  message.success(VENDOR_BRANCH_USER_EDIT_IS_SUCCESS, 10);
}

export function* vendorBranchUserDeleteRequest(
  deleteVendorBranchUserApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteVendorBranchUserApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_USER_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: VENDOR_BRANCH_USER_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* vendorBranchUserDeleteSuccess(message, action) {
  yield put({
    type: VENDOR_BRANCH_USER_LIST_REQUEST,
    dto: action.data.vendorBranchUserdto,
  });
  yield put({
    type: VENDOR_USER_LIST_REQUEST,
    dto: action.data.vendorUserDto,
  });
  message.success(VENDOR_BRANCH_USER_DELETE_IS_SUCCESS, 10);
}
