import {
  VENDORBRANCH_LIST_LOADING,
  VENDORBRANCH_LIST_IS_LOADED,
  VENDORBRANCH_LIST_IS_FAILURE,
  SINGLE_VENDOR_BRANCH_REQUEST,
  SINGLE_VENDOR_BRANCH_SUCCESS,
  SINGLE_VENDOR_BRANCH_FAILURE,
  SINGLE_VENDOR_BRANCH_INIT_REQUEST,
  SINGLE_VENDOR_BRANCH_INIT_SUCCESS,
  SINGLE_VENDOR_BRANCH_INIT_FAILURE,
  VENDOR_BRANCH_LIST_REQUEST,
  VENDOR_BRANCH_LIST_SUCCESS,
  VENDOR_BRANCH_LIST_FAILURE,
  VENDOR_BRANCH_POST_REQUEST,
  VENDOR_BRANCH_POST_SUCCESS,
  VENDOR_BRANCH_POST_FAILURE,
  VENDOR_BRANCH_PUT_FAILURE,
  VENDOR_BRANCH_PUT_REQUEST,
  VENDOR_BRANCH_PUT_SUCCESS,
  VENDOR_BRANCH_DELETE_FAILURE,
  VENDOR_BRANCH_DELETE_REQUEST,
  VENDOR_BRANCH_DELETE_SUCCESS,
  VENDOR_BRANCH_CATEGORY_LIST_REQUEST,
  VENDOR_BRANCH_CATEGORY_LIST_SUCCESS,
  VENDOR_BRANCH_CATEGORY_LIST_FAILURE,
  VENDOR_BRANCH_ALERT_RESET,
  VENDOR_BRANCH_ALERT,
} from '../../../constants/index';

// <editor-fold dsc="Vendor Branch List">
export function vendorBranchListLoading() {
  return {
    type: VENDORBRANCH_LIST_LOADING,
  };
}

export function vendorBranchListLoaded(data) {
  return {
    type: VENDORBRANCH_LIST_IS_LOADED,
      data,
  };
}
export function vendorBranchListFailure2(error) {
  return {
    type: VENDORBRANCH_LIST_IS_FAILURE,
      error,
  };
}
//  saga
export function vendorBranchListRequest(data) {
  return {
    type: VENDOR_BRANCH_LIST_REQUEST,
    data,
  };
}
export function vendorBranchListSuccess(data) {
  return {
    type: VENDOR_BRANCH_LIST_SUCCESS,
    data,
  };
}
export function vendorBranchListFailure() {
  return {
    type: VENDOR_BRANCH_LIST_FAILURE,
  };
}

// </editor-fold>
// <editor-fold dsc="single vendor branch">
export function singleVendorBranchRequest(data) {
  return {
    type: SINGLE_VENDOR_BRANCH_REQUEST,
    data,
  };
}

export function singleVendorBranchSuccess(data) {
  return {
    type: SINGLE_VENDOR_BRANCH_SUCCESS,
    data,
  };
}

export function singleVendorBranchFailure() {
  return {
    type: SINGLE_VENDOR_BRANCH_FAILURE,
  };
}
// </editor-fold>
// <editor-fold dsc="single vendor branch init">
export function singleVendorBranchInitRequest() {
  return {
    type: SINGLE_VENDOR_BRANCH_INIT_REQUEST,
  };
}

export function singleVendorBranchInitSuccess() {
  return {
    type: SINGLE_VENDOR_BRANCH_INIT_SUCCESS,
  };
}

export function singleVendorBranchInitFailure() {
  return {
    type: SINGLE_VENDOR_BRANCH_INIT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Post">
export function vendorBranchPostRequest(data) {
  return {
    type: VENDOR_BRANCH_POST_REQUEST,
    data,
  };
}

export function vendorBranchPostSuccess() {
  return {
    type: VENDOR_BRANCH_POST_SUCCESS,
  };
}
export function vendorBranchPostFailure() {
  return {
    type: VENDOR_BRANCH_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Put">
export function vendorBranchPutRequest(data) {
  return {
    type: VENDOR_BRANCH_PUT_REQUEST,
    data,
  };
}

export function vendorBranchPutSuccess() {
  return {
    type: VENDOR_BRANCH_PUT_SUCCESS,
  };
}
export function vendorBranchPutFailure() {
  return {
    type: VENDOR_BRANCH_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch Delete">
export function vendorBranchDeleteRequest(data) {
  return {
    type: VENDOR_BRANCH_DELETE_REQUEST,
    data,
  };
}

export function vendorBranchDeleteSuccess() {
  return {
    type: VENDOR_BRANCH_DELETE_SUCCESS,
  };
}
export function vendorBranchDeleteFailure() {
  return {
    type: VENDOR_BRANCH_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Branch category List">
export function vendorBranchCategoryListLoading() {
  return {
    type: VENDOR_BRANCH_CATEGORY_LIST_REQUEST,
  };
}

export function vendorBranchCategoryListLoaded(data) {
  return {
    type: VENDOR_BRANCH_CATEGORY_LIST_SUCCESS,
    payload: {
      data,
    },
  };
}
export function vendorBranchCategoryListFailure2(error) {
  return {
    type: VENDOR_BRANCH_CATEGORY_LIST_FAILURE,
    payload: {
      error,
    },
  };
}
// </editor-fold>

export function vendorBranchAlert(data) {
  return {
    type: VENDOR_BRANCH_ALERT,
    data,
  };
}

export function vendorBranchAlertReset() {
  return {
    type: VENDOR_BRANCH_ALERT_RESET,
  };
}
