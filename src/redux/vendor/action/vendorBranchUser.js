import {
  VENDOR_BRANCH_USER_LIST_REQUEST,
  VENDOR_BRANCH_USER_LIST_SUCCESS,
  VENDOR_BRANCH_USER_LIST_FAILURE,
  VENDOR_BRANCH_USER_POST_REQUEST,
  VENDOR_BRANCH_USER_POST_SUCCESS,
  VENDOR_BRANCH_USER_POST_FAILURE,
  VENDOR_BRANCH_USER_PUT_REQUEST,
  VENDOR_BRANCH_USER_PUT_SUCCESS,
  VENDOR_BRANCH_USER_PUT_FAILURE,
  VENDOR_BRANCH_USER_DELETE_REQUEST,
  VENDOR_BRANCH_USER_DELETE_SUCCESS,
  VENDOR_BRANCH_USER_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="vendor Branch User List">
export function vendorBranchUserListRequest(data) {
  return {
    type: VENDOR_BRANCH_USER_LIST_REQUEST,
    data,
  };
}

export function vendorBranchUserListSuccess(data) {
  return {
    type: VENDOR_BRANCH_USER_LIST_SUCCESS,
    data,
  };
}

export function vendorBranchUserListFailure() {
  return {
    type: VENDOR_BRANCH_USER_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vendor Branch User POST">
export function vendorBranchUserPostRequest(data) {
  return {
    type: VENDOR_BRANCH_USER_POST_REQUEST,
    data,
  };
}

export function vendorBranchUserPostSuccess() {
  return {
    type: VENDOR_BRANCH_USER_POST_SUCCESS,
  };
}

export function vendorBranchUserPostFailure(data) {
  return {
    type: VENDOR_BRANCH_USER_POST_FAILURE,
    data,
  };
}
// </editor-fold>

// <editor-fold dsc="vendor Branch User PUT">
export function vendorBranchUserPutRequest(data) {
  return {
    type: VENDOR_BRANCH_USER_PUT_REQUEST,
    data,
  };
}

export function vendorBranchUserPutSuccess() {
  return {
    type: VENDOR_BRANCH_USER_PUT_SUCCESS,
  };
}

export function vendorBranchUserPutFailure(data) {
  return {
    type: VENDOR_BRANCH_USER_PUT_FAILURE,
    data,
  };
}
// </editor-fold>

// <editor-fold dsc="vendor Branch User DELETE">
export function vendorBranchUserDeleteRequest(data) {
  return {
    type: VENDOR_BRANCH_USER_DELETE_REQUEST,
    data,
  };
}

export function vendorBranchUserDeleteSuccess() {
  return {
    type: VENDOR_BRANCH_USER_DELETE_SUCCESS,
  };
}

export function vendorBranchUserDeleteFailure(data) {
  return {
    type: VENDOR_BRANCH_USER_DELETE_FAILURE,
    data,
  };
}
// </editor-fold>
