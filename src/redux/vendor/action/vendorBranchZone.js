import {
  VENDOR_BRANCH_ZONE_LIST_REQUEST,
  VENDOR_BRANCH_ZONE_LIST_SUCCESS,
  VENDOR_BRANCH_ZONE_LIST_FAILURE,
  VENDOR_BRANCH_ZONE_POST_REQUEST,
  VENDOR_BRANCH_ZONE_POST_SUCCESS,
  VENDOR_BRANCH_ZONE_POST_FAILURE,
  VENDOR_BRANCH_ZONE_PUT_REQUEST,
  VENDOR_BRANCH_ZONE_PUT_SUCCESS,
  VENDOR_BRANCH_ZONE_PUT_FAILURE,
  VENDOR_BRANCH_ZONE_DELETE_REQUEST,
  VENDOR_BRANCH_ZONE_DELETE_SUCCESS,
  VENDOR_BRANCH_ZONE_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="vendor Branch Zone List">
export function vendorBranchZoneListRequest(data) {
  return {
    type: VENDOR_BRANCH_ZONE_LIST_REQUEST,
    data,
  };
}

export function vendorBranchZoneListSuccess(data) {
  return {
    type: VENDOR_BRANCH_ZONE_LIST_SUCCESS,
    data,
  };
}

export function vendorBranchZoneListFailure() {
  return {
    type: VENDOR_BRANCH_ZONE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="vendor Branch Zone POST">
export function vendorBranchZonePostRequest(data) {
  return {
    type: VENDOR_BRANCH_ZONE_POST_REQUEST,
    data,
  };
}

export function vendorBranchZonePostSuccess() {
  return {
    type: VENDOR_BRANCH_ZONE_POST_SUCCESS,
  };
}

export function vendorBranchZonePostFailure(data) {
  return {
    type: VENDOR_BRANCH_ZONE_POST_FAILURE,
    data,
  };
}
// </editor-fold>

// <editor-fold dsc="vendor Branch Zone PUT">
export function vendorBranchZonePutRequest(data) {
  return {
    type: VENDOR_BRANCH_ZONE_PUT_REQUEST,
    data,
  };
}

export function vendorBranchZonePutSuccess() {
  return {
    type: VENDOR_BRANCH_ZONE_PUT_SUCCESS,
  };
}

export function vendorBranchZonePutFailure(data) {
  return {
    type: VENDOR_BRANCH_ZONE_PUT_FAILURE,
    data,
  };
}
// </editor-fold>

// <editor-fold dsc="vendor Branch Zone DELETE">
export function vendorBranchZoneDeleteRequest(data) {
  return {
    type: VENDOR_BRANCH_ZONE_DELETE_REQUEST,
    data,
  };
}

export function vendorBranchZoneDeleteSuccess() {
  return {
    type: VENDOR_BRANCH_ZONE_DELETE_SUCCESS,
  };
}

export function vendorBranchZoneDeleteFailure(data) {
  return {
    type: VENDOR_BRANCH_ZONE_DELETE_FAILURE,
    data,
  };
}
// </editor-fold>
