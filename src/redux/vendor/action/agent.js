import {
  VENDOR_LIST_REQUEST,
  VENDOR_LIST_SUCCESS,
  VENDOR_LIST_FAILURE,
  SINGLE_VENDOR_AGENT_FAILURE,
  SINGLE_VENDOR_AGENT_REQUEST,
  SINGLE_VENDOR_AGENT_SUCCESS,
  SINGLE_VENDOR_AGENT_INIT_FAILURE,
  SINGLE_VENDOR_AGENT_INIT_REQUEST,
  SINGLE_VENDOR_AGENT_INIT_SUCCESS,
  VENDOR_USER_LIST_REQUEST,
  VENDOR_USER_LIST_SUCCESS,
  VENDOR_USER_LIST_FAILURE,
  VENDOR_AGENT_POST_REQUEST,
  VENDOR_AGENT_POST_SUCCESS,
  VENDOR_AGENT_POST_FAILURE,
  VENDOR_AGENT_PUT_REQUEST,
  VENDOR_AGENT_PUT_SUCCESS,
  VENDOR_AGENT_PUT_FAILURE,
  VENDOR_AGENT_DELETE_REQUEST,
  VENDOR_AGENT_DELETE_SUCCESS,
  VENDOR_AGENT_DELETE_FAILURE,
  VENDOR_AGENT_ALERT_RESET,
  VENDOR_AGENT_ALERT,
} from '../../../constants/index';

// <editor-fold dsc="Vendor List">
export function vendorListRequest(data) {
  return {
    type: VENDOR_LIST_REQUEST,
    data,
  };
}
export function vendorListSuccess(data) {
  return {
    type: VENDOR_LIST_SUCCESS,
    data,
  };
}
export function vendorListFailure() {
  return {
    type: VENDOR_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single Vendor ">
export function singleVendorAgentRequest(data) {
  return {
    type: SINGLE_VENDOR_AGENT_REQUEST,
    data,
  };
}
export function singleVendorAgentSuccess(data) {
  return {
    type: SINGLE_VENDOR_AGENT_SUCCESS,
    data,
  };
}
export function singleVendorAgentFailure() {
  return {
    type: SINGLE_VENDOR_AGENT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor User List">
export function vendorUserListRequest(data) {
  return {
    type: VENDOR_USER_LIST_REQUEST,
    data,
  };
}
export function vendorUserListSuccess(data) {
  return {
    type: VENDOR_USER_LIST_SUCCESS,
    data,
  };
}
export function vendorUserListFailure() {
  return {
    type: VENDOR_USER_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single Vendor init">
export function singleVendorAgentInitRequest(data) {
  return {
    type: SINGLE_VENDOR_AGENT_INIT_REQUEST,
    data,
  };
}
export function singleVendorAgentInitSuccess(data) {
  return {
    type: SINGLE_VENDOR_AGENT_INIT_SUCCESS,
    data,
  };
}
export function singleVendorAgentInitFailure() {
  return {
    type: SINGLE_VENDOR_AGENT_INIT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Agent Post">
export function vendorAgentPostRequest(data) {
  return {
    type: VENDOR_AGENT_POST_REQUEST,
    data,
  };
}

export function vendorAgentPostSuccess() {
  return {
    type: VENDOR_AGENT_POST_SUCCESS,
  };
}
export function vendorAgentPostFailure() {
  return {
    type: VENDOR_AGENT_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor aget Put">
export function vendorAgentPutRequest(data) {
  return {
    type: VENDOR_AGENT_PUT_REQUEST,
    data,
  };
}

export function vendorAgentPutSuccess() {
  return {
    type: VENDOR_AGENT_PUT_SUCCESS,
  };
}
export function vendorAgentPutFailure() {
  return {
    type: VENDOR_AGENT_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Agent Delete">
export function vendorAgentDeleteRequest(data) {
  return {
    type: VENDOR_AGENT_DELETE_REQUEST,
    data,
  };
}

export function vendorAgentDeleteSuccess() {
  return {
    type: VENDOR_AGENT_DELETE_SUCCESS,
  };
}
export function vendorAgentDeleteFailure() {
  return {
    type: VENDOR_AGENT_DELETE_FAILURE,
  };
}
// </editor-fold>

export function vendorAgentAlert(data) {
  return {
    type: VENDOR_AGENT_ALERT,
    data,
  };
}

export function vendorAgentAlertReset() {
  return {
    type: VENDOR_AGENT_ALERT_RESET,
  };
}
