import {
  VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_LIST_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_LIST_FAILURE,
  VENDOR_BRANCH_OFF_SHIFT_POST_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_POST_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_POST_FAILURE,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="offShift List">
export function vendorBranchOffShiftListRequest(data) {
  return {
    type: VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST,
    data,
  };
}

export function vendorBranchOffShiftListSuccess(data) {
  return {
    type: VENDOR_BRANCH_OFF_SHIFT_LIST_SUCCESS,
    data,
  };
}

export function vendorBranchOffShiftListFailure() {
  return {
    type: VENDOR_BRANCH_OFF_SHIFT_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="offShift Post">
export function vendorBranchOffShiftPostRequest(data) {
  return {
    type: VENDOR_BRANCH_OFF_SHIFT_POST_REQUEST,
    data,
  };
}

export function vendorBranchOffShiftPostSuccess() {
  return {
    type: VENDOR_BRANCH_OFF_SHIFT_POST_SUCCESS,
  };
}

export function vendorBranchOffShiftPostFailure(data) {
  return {
    type: VENDOR_BRANCH_OFF_SHIFT_POST_FAILURE,
    data,
  };
}
// </editor-fold>

// <editor-fold dsc="offShift Delete">
export function vendorBranchOffShiftDeleteRequest(data) {
  return {
    type: VENDOR_BRANCH_OFF_SHIFT_DELETE_REQUEST,
    data,
  };
}

export function vendorBranchOffShiftDeleteSuccess() {
  return {
    type: VENDORBRANCH_OFFSHIFT_DELETE_SUCCESS,
  };
}

export function vendorBranchOffShiftDeleteFailure(data) {
  return {
    type: VENDORBRANCH_OFFSHIFT_DELETE_FAILURE,
    data,
  };
}
// </editor-fold>
