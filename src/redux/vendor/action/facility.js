import {
  FACILITY_LIST_REQUEST,
  FACILITY_LIST_SUCCESS,
  FACILITY_LIST_FAILURE,
  VENDOR_FACILITY_LIST_REQUEST,
  VENDOR_FACILITY_LIST_SUCCESS,
  VENDOR_FACILITY_LIST_FAILURE,
  VENDOR_FACILITY_POST_FAILURE,
  VENDOR_FACILITY_POST_REQUEST,
  VENDOR_FACILITY_POST_SUCCESS,
} from '../../../constants/index';

// <editor-fold dsc="Facility List">
export function facilityListRequest(data) {
  return {
    type: FACILITY_LIST_REQUEST,
    data,
  };
}

export function facilityListSuccess(data) {
  return {
    type: FACILITY_LIST_SUCCESS,
    data,
  };
}

export function facilityListFailure() {
  return {
    type: FACILITY_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Facility List">
export function vendorFacilityListRequest(data) {
  return {
    type: VENDOR_FACILITY_LIST_REQUEST,
    data,
  };
}

export function vendorFacilityListSuccess(data) {
  return {
    type: VENDOR_FACILITY_LIST_SUCCESS,
    data,
  };
}

export function vendorFacilityListFailure() {
  return {
    type: VENDOR_FACILITY_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Vendor Facility Post">
export function vendorFacilityPostRequest(data) {
  return {
    type: VENDOR_FACILITY_POST_REQUEST,
    data,
  };
}

export function vendorFacilityPostSuccess() {
  return {
    type: VENDOR_FACILITY_POST_SUCCESS,
  };
}

export function vendorFacilityPostFailure(data) {
  return {
    type: VENDOR_FACILITY_POST_FAILURE,
    data,
  };
}
// </editor-fold>
