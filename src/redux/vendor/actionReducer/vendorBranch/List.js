import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  vendorBranchListRequest: ['dto'],
  vendorBranchListSuccess: ['data'],
  vendorBranchListFailure: [],
});

export default Creators;
/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: [],
  error: {},
});

/* ------------- Reducers ------------- */

// we're attempting to fetching
export const request = state => ({
  ...state,
  loading: true,
});
// we've successfully fetch LIst items from server
export const success = (state, action) => {
  const { data } = action;
  return {
    ...state,
    data,
    loading: false,
    error: {},
  };
};

// we've had a problem in fetching
export const failure = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.VENDOR_BRANCH_LIST_REQUEST]: request,
  [Types.VENDOR_BRANCH_LIST_SUCCESS]: success,
  [Types.VENDOR_BRANCH_LIST_FAILURE]: failure,
});

/* ------------- Selectors ------------- */
export const getVendorBranchList = state => state.vendorBranchList;

// Is the current user logged in?
// export const isLastPage = (listFetchState) => listFetchState.count / pageSize >
