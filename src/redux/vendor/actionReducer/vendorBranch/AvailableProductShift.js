import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  availableProductShiftRequest: ['dto'],
  availableProductShiftSuccess: ['data'],
  availableProductShiftFailure: [],
});

export default Creators;
/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: [],
  error: {},
});

/* ------------- Reducers ------------- */

// we're attempting to fetching
export const request = state => ({
  ...state,
  loading: true,
});
// we've successfully fetch LIst items from server
export const success = (state, action) => {
  const { data } = action;
  return {
    ...state,
    data,
    loading: false,
    error: {},
  };
};

// we've had a problem in fetching
export const failure = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.AVAILABLE_PRODUCT_SHIFT_REQUEST]: request,
  [Types.AVAILABLE_PRODUCT_SHIFT_SUCCESS]: success,
  [Types.AVAILABLE_PRODUCT_SHIFT_FAILURE]: failure,
});

/* ------------- Selectors ------------- */
export const getAvailableProductShift = state => state.availableProductShift;

// Is the current user logged in?
// export const isLastPage = (listFetchState) => listFetchState.count / pageSize >
