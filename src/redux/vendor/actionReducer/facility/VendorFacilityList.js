import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import {
  VENDOR_FACILITY_LIST_FAILURE, VENDOR_FACILITY_LIST_REQUEST,
  VENDOR_FACILITY_LIST_SUCCESS
} from "../../../../constants";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  vendorFacilityListRequest: ['dto'],
  vendorFacilityListSuccess: ['data'],
  vendorFacilityListFailure: [],
});

export default Creators;
/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: [],
  error: {},
});

/* ------------- Reducers ------------- */

// we're attempting to fetching
export const request = state => ({
  ...state,
  loading: true,
});
// we've successfully fetch LIst items from server
export const success = (state, action) => {
  const { data } = action;
  return {
    ...state,
    data,
    loading: false,
    error: {},
  };
};

// we've had a problem in fetching
export const failure = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.VENDOR_FACILITY_LIST_REQUEST]: request,
  [Types.VENDOR_FACILITY_LIST_SUCCESS]: success,
  [Types.VENDOR_FACILITY_LIST_FAILURE]: failure,
});

/* ------------- Selectors ------------- */
export const getVendorFacilityList = state => state.vendorFacilityList;

// Is the current user logged in?
// export const isLastPage = (listFetchState) => listFetchState.count / pageSize >
