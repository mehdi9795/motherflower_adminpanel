import {
  VENDORBRANCH_LIST_LOADING,
  VENDORBRANCH_LIST_IS_LOADED,
  VENDORBRANCH_LIST_IS_FAILURE,
  VENDOR_BRANCH_LIST_REQUEST,
  VENDOR_BRANCH_LIST_SUCCESS,
  VENDOR_BRANCH_LIST_FAILURE,
  SINGLE_VENDOR_BRANCH_REQUEST,
  SINGLE_VENDOR_BRANCH_SUCCESS,
  SINGLE_VENDOR_BRANCH_FAILURE,
  SINGLE_VENDOR_BRANCH_INIT_REQUEST,
  SINGLE_VENDOR_BRANCH_INIT_SUCCESS,
  SINGLE_VENDOR_BRANCH_INIT_FAILURE,
  VENDOR_BRANCH_POST_REQUEST,
  VENDOR_BRANCH_POST_SUCCESS,
  VENDOR_BRANCH_POST_FAILURE,
  VENDOR_BRANCH_PUT_FAILURE,
  VENDOR_BRANCH_PUT_REQUEST,
  VENDOR_BRANCH_PUT_SUCCESS,
  VENDOR_BRANCH_DELETE_FAILURE,
  VENDOR_BRANCH_DELETE_REQUEST,
  VENDOR_BRANCH_DELETE_SUCCESS,
  VENDOR_BRANCH_CATEGORY_LIST_REQUEST,
  VENDOR_BRANCH_CATEGORY_LIST_SUCCESS,
  VENDOR_BRANCH_CATEGORY_LIST_FAILURE,
  VENDOR_BRANCH_ALERT_RESET,
  VENDOR_BRANCH_ALERT,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  vendorBranchRequest: false,
  vendorBranchData: getResponseModel,
  vendorBranchError: false,

  singleVendorBranchRequest: false,
  singleVendorBranchData2: getResponseModel,
  singleVendorBranchError: false,

  singleVendorBranchInitRequest: false,
  singleVendorBranchInitError: true,

  vendorBranchPostRequest: false,
  vendorBranchPostData: getResponseModel,

  vendorBranchPutRequest: false,
  vendorBranchPutData: getResponseModel,

  vendorBranchDeleteRequest: false,
  vendorBranchDeleteData: getResponseModel,

  vendorBranchProductListRequest: false,
  vendorBranchProductListData: getResponseModel,
  vendorBranchProductListError: false,

  vendorBranchCRUDError: false,
  vendorBranchErrorMessage: '',
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="Vendor Branch List">
    case VENDORBRANCH_LIST_LOADING:
      return {
        ...state,
        vendorBranchLoading: true,
      };
    case VENDORBRANCH_LIST_IS_LOADED:
      return {
        ...state,
        vendorBranchLoading: false,
        vendorBranchData: action.data,
      };
    case VENDORBRANCH_LIST_IS_FAILURE:
      return {
        ...state,
        vendorBranchLoading: false,
        vendorBranchError: true,
      };

    case VENDOR_BRANCH_LIST_REQUEST:
      return {
        ...state,
        vendorBranchRequest: true,
      };
    case VENDOR_BRANCH_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchRequest: false,
        vendorBranchData: action.data,
      };
    case VENDOR_BRANCH_LIST_FAILURE:
      return {
        ...state,
        vendorBranchRequest: false,
        vendorBranchError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Single Vendor Branch">
    case SINGLE_VENDOR_BRANCH_REQUEST:
      return {
        ...state,
        singleVendorBranchRequest: true,
      };
    case SINGLE_VENDOR_BRANCH_SUCCESS:
      return {
        ...state,
        singleVendorBranchRequest: false,
        singleVendorBranchData2: action.data,
      };
    case SINGLE_VENDOR_BRANCH_FAILURE:
      return {
        ...state,
        singleVendorBranchRequest: false,
        singleVendorBranchError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Single Vendor Branch Init">
    case SINGLE_VENDOR_BRANCH_INIT_REQUEST:
      return {
        ...state,
        singleVendorBranchInitRequest: true,
        singleVendorBranchInitError: false,
      };
    case SINGLE_VENDOR_BRANCH_INIT_SUCCESS:
      return {
        ...state,
        singleVendorBranchInitRequest: false,
        singleVendorBranchInitError: false,
      };
    case SINGLE_VENDOR_BRANCH_INIT_FAILURE:
      return {
        ...state,
        singleVendorBranchInitRequest: false,
        singleVendorBranchInitError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Branch Post">
    case VENDOR_BRANCH_POST_REQUEST:
      return {
        ...state,
        vendorBranchPostRequest: true,
        vendorBranchCRUDError: false,
        vendorBranchPostData: action.data,
      };
    case VENDOR_BRANCH_POST_SUCCESS:
      return {
        ...state,
        vendorBranchPostRequest: false,
        vendorBranchCRUDError: false,
      };
    case VENDOR_BRANCH_POST_FAILURE:
      return {
        ...state,
        vendorBranchErrorMessage: action.data,
        vendorBranchPostRequest: false,
        vendorBranchCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Branch Put">
    case VENDOR_BRANCH_PUT_REQUEST:
      return {
        ...state,
        vendorBranchPutRequest: true,
        vendorBranchCRUDError: false,
        vendorBranchPutData: action.data,
      };
    case VENDOR_BRANCH_PUT_SUCCESS:
      return {
        ...state,
        vendorBranchPutRequest: false,
        vendorBranchCRUDError: false,
      };
    case VENDOR_BRANCH_PUT_FAILURE:
      return {
        ...state,
        vendorBranchErrorMessage: action.data,
        vendorBranchPutRequest: false,
        vendorBranchCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Branch Delete">
    case VENDOR_BRANCH_DELETE_REQUEST:
      return {
        ...state,
        vendorBranchDeleteRequest: true,
        vendorBranchCRUDError: false,
        vendorBranchDeleteData: action.data,
      };
    case VENDOR_BRANCH_DELETE_SUCCESS:
      return {
        ...state,
        vendorBranchDeleteRequest: false,
        vendorBranchCRUDError: false,
      };
    case VENDOR_BRANCH_DELETE_FAILURE:
      return {
        ...state,
        vendorBranchErrorMessage: action.data,
        vendorBranchDeleteRequest: false,
        vendorBranchCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Branch product List">
    case VENDOR_BRANCH_CATEGORY_LIST_REQUEST:
      return {
        ...state,
        vendorBranchProductListRequest: true,
      };
    case VENDOR_BRANCH_CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchProductListRequest: false,
        vendorBranchProductListData: action.data,
      };
    case VENDOR_BRANCH_CATEGORY_LIST_FAILURE:
      return {
        ...state,
        vendorBranchProductListRequest: false,
        vendorBranchProductListError: true,
      };
    // </editor-fold>

    case VENDOR_BRANCH_ALERT_RESET:
      return {
        ...state,
        vendorBranchErrorMessage: '',
        vendorBranchPostRequest: false,
        vendorBranchCRUDError: false,
      };

    case VENDOR_BRANCH_ALERT:
      return {
        ...state,
        vendorBranchErrorMessage: action.data,
        vendorBranchPostRequest: false,
        vendorBranchCRUDError: true,
      };
    default:
      return state;
  }
}
