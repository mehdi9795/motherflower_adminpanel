import {
  VENDOR_BRANCH_ZONE_LIST_REQUEST,
  VENDOR_BRANCH_ZONE_LIST_SUCCESS,
  VENDOR_BRANCH_ZONE_LIST_FAILURE,
  VENDOR_BRANCH_ZONE_POST_REQUEST,
  VENDOR_BRANCH_ZONE_POST_SUCCESS,
  VENDOR_BRANCH_ZONE_POST_FAILURE,
  VENDOR_BRANCH_ZONE_PUT_REQUEST,
  VENDOR_BRANCH_ZONE_PUT_SUCCESS,
  VENDOR_BRANCH_ZONE_PUT_FAILURE,
  VENDOR_BRANCH_ZONE_DELETE_REQUEST,
  VENDOR_BRANCH_ZONE_DELETE_SUCCESS,
  VENDOR_BRANCH_ZONE_DELETE_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  vendorBranchZoneRequest: false,
  vendorBranchZoneData: getResponseModel,
  vendorBranchZoneError: false,

  vendorBranchZonePostRequest: false,
  vendorBranchZonePostData: getResponseModel,

  vendorBranchZonePutRequest: false,
  vendorBranchZonePutData: getResponseModel,

  vendorBranchZoneDeleteRequest: false,
  vendorBranchZoneDeleteData: getResponseModel,

  vendorBranchZoneCRUDError: false,
  vendorBranchZoneErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="vendor branch zone list">
    case VENDOR_BRANCH_ZONE_LIST_REQUEST:
      return {
        ...state,
        vendorBranchZoneRequest: true,
        vendorBranchZoneError: false,
      };
    case VENDOR_BRANCH_ZONE_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchZoneData: action.data,
        vendorBranchZoneRequest: false,
        vendorBranchZoneError: false,
      };
    case VENDOR_BRANCH_ZONE_LIST_FAILURE:
      return {
        ...state,
        vendorBranchZoneError: true,
        vendorBranchZoneRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch user post">
    case VENDOR_BRANCH_ZONE_POST_REQUEST:
      return {
        ...state,
        vendorBranchZonePostRequest: true,
        vendorBranchZoneCRUDError: false,
      };
    case VENDOR_BRANCH_ZONE_POST_SUCCESS:
      return {
        ...state,
        vendorBranchZonePostRequest: false,
        vendorBranchZoneCRUDError: false,
      };
    case VENDOR_BRANCH_ZONE_POST_FAILURE:
      return {
        ...state,
        vendorBranchZoneCRUDError: true,
        vendorBranchZonePostRequest: false,
        vendorBranchZoneErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch user put">
    case VENDOR_BRANCH_ZONE_PUT_REQUEST:
      return {
        ...state,
        vendorBranchZonePutRequest: true,
        vendorBranchZoneCRUDError: false,
      };
    case VENDOR_BRANCH_ZONE_PUT_SUCCESS:
      return {
        ...state,
        vendorBranchZonePutData: action.data,
        vendorBranchZonePutRequest: false,
        vendorBranchZoneCRUDError: false,
      };
    case VENDOR_BRANCH_ZONE_PUT_FAILURE:
      return {
        ...state,
        vendorBranchZoneCRUDError: true,
        vendorBranchZonePutRequest: false,
        vendorBranchZoneErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch user delete">
    case VENDOR_BRANCH_ZONE_DELETE_REQUEST:
      return {
        ...state,
        vendorBranchZoneDeleteRequest: true,
        vendorBranchZoneCRUDError: false,
      };
    case VENDOR_BRANCH_ZONE_DELETE_SUCCESS:
      return {
        ...state,
        vendorBranchZoneDeleteData: action.data,
        vendorBranchZoneDeleteRequest: false,
        vendorBranchZoneCRUDError: false,
      };
    case VENDOR_BRANCH_ZONE_DELETE_FAILURE:
      return {
        ...state,
        vendorBranchZoneCRUDError: true,
        vendorBranchZoneDeleteRequest: false,
        vendorBranchZoneErrorMessage: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
