import {
  VENDOR_BRANCH_USER_LIST_REQUEST,
  VENDOR_BRANCH_USER_LIST_SUCCESS,
  VENDOR_BRANCH_USER_LIST_FAILURE,
  VENDOR_BRANCH_USER_POST_REQUEST,
  VENDOR_BRANCH_USER_POST_SUCCESS,
  VENDOR_BRANCH_USER_POST_FAILURE,
  VENDOR_BRANCH_USER_PUT_REQUEST,
  VENDOR_BRANCH_USER_PUT_SUCCESS,
  VENDOR_BRANCH_USER_PUT_FAILURE,
  VENDOR_BRANCH_USER_DELETE_REQUEST,
  VENDOR_BRANCH_USER_DELETE_SUCCESS,
  VENDOR_BRANCH_USER_DELETE_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  vendorBranchUserRequest: false,
  vendorBranchUserData: getResponseModel,
  vendorBranchUserError: false,

  vendorBranchUserPostRequest: false,
  vendorBranchUserPostData: getResponseModel,

  vendorBranchUserPutRequest: false,
  vendorBranchUserPutData: getResponseModel,

  vendorBranchUserDeleteRequest: false,
  vendorBranchUserDeleteData: getResponseModel,

  vendorBranchUserCRUDError: false,
  vendorBranchUserErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="vendor branch user list">
    case VENDOR_BRANCH_USER_LIST_REQUEST:
      return {
        ...state,
        vendorBranchUserRequest: true,
        vendorBranchUserError: false,
      };
    case VENDOR_BRANCH_USER_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchUserData: action.data,
        vendorBranchUserRequest: false,
        vendorBranchUserError: false,
      };
    case VENDOR_BRANCH_USER_LIST_FAILURE:
      return {
        ...state,
        vendorBranchUserError: true,
        vendorBranchUserRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch user post">
    case VENDOR_BRANCH_USER_POST_REQUEST:
      return {
        ...state,
        vendorBranchUserPostRequest: true,
        vendorBranchUserCRUDError: false,
      };
    case VENDOR_BRANCH_USER_POST_SUCCESS:
      return {
        ...state,
        vendorBranchUserPostRequest: false,
        vendorBranchUserCRUDError: false,
      };
    case VENDOR_BRANCH_USER_POST_FAILURE:
      return {
        ...state,
        vendorBranchUserCRUDError: true,
        vendorBranchUserPostRequest: false,
        vendorBranchUserErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch user put">
    case VENDOR_BRANCH_USER_PUT_REQUEST:
      return {
        ...state,
        vendorBranchUserPutRequest: true,
        vendorBranchUserCRUDError: false,
      };
    case VENDOR_BRANCH_USER_PUT_SUCCESS:
      return {
        ...state,
        vendorBranchUserPutData: action.data,
        vendorBranchUserPutRequest: false,
        vendorBranchUserCRUDError: false,
      };
    case VENDOR_BRANCH_USER_PUT_FAILURE:
      return {
        ...state,
        vendorBranchUserCRUDError: true,
        vendorBranchUserPutRequest: false,
        vendorBranchUserErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vendor branch user delete">
    case VENDOR_BRANCH_USER_DELETE_REQUEST:
      return {
        ...state,
        vendorBranchUserDeleteRequest: true,
        vendorBranchUserCRUDError: false,
      };
    case VENDOR_BRANCH_USER_DELETE_SUCCESS:
      return {
        ...state,
        vendorBranchUserDeleteData: action.data,
        vendorBranchUserDeleteRequest: false,
        vendorBranchUserCRUDError: false,
      };
    case VENDOR_BRANCH_USER_DELETE_FAILURE:
      return {
        ...state,
        vendorBranchUserCRUDError: true,
        vendorBranchUserDeleteRequest: false,
        vendorBranchUserErrorMessage: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
