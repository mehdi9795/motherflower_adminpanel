import {
  VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_LIST_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_LIST_FAILURE,
  VENDOR_BRANCH_OFF_SHIFT_POST_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_POST_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_POST_FAILURE,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_REQUEST,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_SUCCESS,
  VENDOR_BRANCH_OFF_SHIFT_DELETE_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  vendorBranchOffShiftRequest: false,
  vendorBranchOffShiftData: getResponseModel,
  vendorBranchOffShiftError: false,

  vendorBranchOffShiftPostRequest: false,
  vendorBranchOffShiftPostData: getResponseModel,

  vendorBranchOffShiftDeleteRequest: false,

  vendorBranchOffShiftCRUDError: false,
  vendorBranchOffShiftErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="vendor banch off shift List">
    case VENDOR_BRANCH_OFF_SHIFT_LIST_REQUEST:
      return {
        ...state,
        vendorBranchOffShiftRequest: true,
        vendorBranchOffShiftError: false,
      };
    case VENDOR_BRANCH_OFF_SHIFT_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchOffShiftData: action.data,
        vendorBranchOffShiftRequest: false,
        vendorBranchOffShiftError: false,
      };
    case VENDOR_BRANCH_OFF_SHIFT_LIST_FAILURE:
      return {
        ...state,
        vendorBranchOffShiftError: true,
        vendorBranchOffShiftRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="vendor banch off shift Post">
    case VENDOR_BRANCH_OFF_SHIFT_POST_REQUEST:
      return {
        ...state,
        vendorBranchOffShiftPostRequest: true,
        vendorBranchOffShiftPostData: action.data,
        vendorBranchOffShiftCRUDError: false,
      };
    case VENDOR_BRANCH_OFF_SHIFT_POST_SUCCESS:
      return {
        ...state,
        vendorBranchOffShiftPostRequest: false,
        vendorBranchOffShiftCRUDError: false,
      };
    case VENDOR_BRANCH_OFF_SHIFT_POST_FAILURE:
      return {
        ...state,
        vendorBranchOffShiftCRUDError: true,
        vendorBranchOffShiftPostRequest: false,
        vendorBranchOffShiftErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold desc="vendor banch off shift Delete">
    case VENDOR_BRANCH_OFF_SHIFT_DELETE_REQUEST:
      return {
        ...state,
        vendorBranchOffShiftDeleteRequest: true,
        vendorBranchOffShiftCRUDError: false,
      };
    case VENDOR_BRANCH_OFF_SHIFT_DELETE_SUCCESS:
      return {
        ...state,
        vendorBranchOffShiftDeleteRequest: false,
        vendorBranchOffShiftCRUDError: false,
      };
    case VENDOR_BRANCH_OFF_SHIFT_DELETE_FAILURE:
      return {
        ...state,
        vendorBranchOffShiftCRUDError: true,
        vendorBranchOffShiftDeleteRequest: false,
        vendorBranchOffShiftErrorMessage: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
