import {
  VENDOR_LIST_REQUEST,
  VENDOR_LIST_SUCCESS,
  VENDOR_LIST_FAILURE,
  SINGLE_VENDOR_AGENT_FAILURE,
  SINGLE_VENDOR_AGENT_REQUEST,
  SINGLE_VENDOR_AGENT_SUCCESS,
  SINGLE_VENDOR_AGENT_INIT_FAILURE,
  SINGLE_VENDOR_AGENT_INIT_REQUEST,
  SINGLE_VENDOR_AGENT_INIT_SUCCESS,
  VENDOR_USER_LIST_REQUEST,
  VENDOR_USER_LIST_SUCCESS,
  VENDOR_USER_LIST_FAILURE,
  VENDOR_AGENT_POST_REQUEST,
  VENDOR_AGENT_POST_SUCCESS,
  VENDOR_AGENT_POST_FAILURE,
  VENDOR_AGENT_PUT_REQUEST,
  VENDOR_AGENT_PUT_SUCCESS,
  VENDOR_AGENT_PUT_FAILURE,
  VENDOR_AGENT_DELETE_REQUEST,
  VENDOR_AGENT_DELETE_SUCCESS,
  VENDOR_AGENT_DELETE_FAILURE,
  VENDOR_AGENT_ALERT_RESET,
  VENDOR_AGENT_ALERT,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  vendorRequest: false,
  vendorData: getResponseModel,
  vendorError: false,
  vendorUserListRequest: false,
  vendorUserListData: getResponseModel,
  vendorUserListError: false,
  singleVendorAgentRequest: false,
  singleVendorAgentData: getResponseModel,
  singleVendorAgentError: false,
  singleVendorAgentInitRequest: false,
  singleVendorAgentInitData: getResponseModel,
  singleVendorAgentInitError: false,

  vendorAgentPostRequest: false,
  vendorAgentPostData: getResponseModel,

  vendorAgentPutRequest: false,
  vendorAgentPutData: getResponseModel,

  vendorAgentDeleteRequest: false,
  vendorAgentDeleteData: getResponseModel,

  vendorAgentCRUDError: false,
  vendorAgentErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="Vendor List">
    case VENDOR_LIST_REQUEST:
      return {
        ...state,
        vendorRequest: true,
        vendorError: false,
      };
    case VENDOR_LIST_SUCCESS:
      return {
        ...state,
        vendorData: action.data,
        vendorRequest: false,
        vendorError: false,
      };
    case VENDOR_LIST_FAILURE:
      return {
        ...state,
        vendorError: true,
        vendorRequest: false,
      };

    // </editor-fold>

    // <editor-fold desc="Single Vendor ">
    case SINGLE_VENDOR_AGENT_REQUEST:
      return {
        ...state,
        singleVendorAgentRequest: true,
        singleVendorAgentError: false,
      };
    case SINGLE_VENDOR_AGENT_SUCCESS:
      return {
        ...state,
        singleVendorAgentData: action.data,
        singleVendorAgentRequest: false,
        singleVendorAgentError: false,
      };
    case SINGLE_VENDOR_AGENT_FAILURE:
      return {
        ...state,
        singleVendorAgentError: true,
        singleVendorAgentRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor User List">
    case VENDOR_USER_LIST_REQUEST:
      return {
        ...state,
        vendorUserListRequest: true,
        vendorUserListError: false,
      };
    case VENDOR_USER_LIST_SUCCESS:
      return {
        ...state,
        vendorUserListData: action.data,
        vendorUserListRequest: false,
        vendorUserListError: false,
      };
    case VENDOR_USER_LIST_FAILURE:
      return {
        ...state,
        vendorUserListError: true,
        vendorUserListRequest: false,
      };

    // </editor-fold>

    // <editor-fold desc="Single Vendor init">
    case SINGLE_VENDOR_AGENT_INIT_REQUEST:
      return {
        ...state,
        singleVendorAgentInitRequest: true,
        singleVendorAgentError: false,
      };
    case SINGLE_VENDOR_AGENT_INIT_SUCCESS:
      return {
        ...state,
        singleVendorAgentInitRequest: false,
        singleVendorAgentInitError: false,
      };
    case SINGLE_VENDOR_AGENT_INIT_FAILURE:
      return {
        ...state,
        singleVendorAgentInitError: true,
        singleVendorAgentInitRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Agent Post">
    case VENDOR_AGENT_POST_REQUEST:
      return {
        ...state,
        vendorAgentPostRequest: true,
        vendorAgentCRUDError: false,
        vendorAgentPostData: action.data,
      };
    case VENDOR_AGENT_POST_SUCCESS:
      return {
        ...state,
        vendorAgentPostRequest: false,
        vendorAgentCRUDError: false,
      };
    case VENDOR_AGENT_POST_FAILURE:
      return {
        ...state,
        vendorAgentErrorMessage: action.data,
        vendorAgentPostRequest: false,
        vendorAgentCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Branch Put">
    case VENDOR_AGENT_PUT_REQUEST:
      return {
        ...state,
        vendorAgentPutRequest: true,
        vendorAgentCRUDError: false,
        vendorAgentPutData: action.data,
      };
    case VENDOR_AGENT_PUT_SUCCESS:
      return {
        ...state,
        vendorAgentPutRequest: false,
        vendorAgentCRUDError: false,
      };
    case VENDOR_AGENT_PUT_FAILURE:
      return {
        ...state,
        vendorAgentErrorMessage: action.data,
        vendorAgentPutRequest: false,
        vendorAgentCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Branch Delete">
    case VENDOR_AGENT_DELETE_REQUEST:
      return {
        ...state,
        vendorAgentDeleteRequest: true,
        vendorAgentCRUDError: false,
        vendorAgentDeleteData: action.data,
      };
    case VENDOR_AGENT_DELETE_SUCCESS:
      return {
        ...state,
        vendorAgentDeleteRequest: false,
        vendorAgentCRUDError: false,
      };
    case VENDOR_AGENT_DELETE_FAILURE:
      return {
        ...state,
        vendorAgentErrorMessage: action.data,
        vendorAgentDeleteRequest: false,
        vendorAgentCRUDError: true,
      };
    // </editor-fold>

    case VENDOR_AGENT_ALERT_RESET:
      return {
        ...state,
        vendorAgentErrorMessage: '',
        vendorAgentPostRequest: false,
        vendorAgentCRUDError: false,
      };

    case VENDOR_AGENT_ALERT:
      return {
        ...state,
        vendorAgentErrorMessage: action.data,
        vendorAgentPostRequest: false,
        vendorAgentAgentCRUDError: true,
      };

    default:
      return state;
  }
}
