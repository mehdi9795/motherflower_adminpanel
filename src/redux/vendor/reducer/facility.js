import {
  FACILITY_LIST_REQUEST,
  FACILITY_LIST_SUCCESS,
  FACILITY_LIST_FAILURE,
  VENDOR_FACILITY_LIST_REQUEST,
  VENDOR_FACILITY_LIST_SUCCESS,
  VENDOR_FACILITY_LIST_FAILURE,
  VENDOR_FACILITY_POST_FAILURE,
  VENDOR_FACILITY_POST_REQUEST,
  VENDOR_FACILITY_POST_SUCCESS,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  facilityRequest: false,
  facilityData: getResponseModel,
  facilityError: false,

  vendorFacilityRequest: false,
  vendorFacilityData: getResponseModel,
  vendorFacilityError: false,

  vendorFacilityPostRequest: false,
  vendorFacilityPostData: getResponseModel,

  vendorFacilityCRUDError: false,
  vendorFacilityErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="Facility List">
    case FACILITY_LIST_REQUEST:
      return {
        ...state,
        facilityRequest: true,
        facilityError: false,
      };
    case FACILITY_LIST_SUCCESS:
      return {
        ...state,
        facilityData: action.data,
        facilityRequest: false,
        facilityError: false,
      };
    case FACILITY_LIST_FAILURE:
      return {
        ...state,
        facilityError: true,
        facilityRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Facility List">
    case VENDOR_FACILITY_LIST_REQUEST:
      return {
        ...state,
        vendorFacilityRequest: true,
        vendorFacilityError: false,
      };
    case VENDOR_FACILITY_LIST_SUCCESS:
      return {
        ...state,
        vendorFacilityData: action.data,
        vendorFacilityRequest: false,
        vendorFacilityError: false,
      };
    case VENDOR_FACILITY_LIST_FAILURE:
      return {
        ...state,
        vendorFacilityError: true,
        vendorFacilityRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Vendor Facility Post">
    case VENDOR_FACILITY_POST_REQUEST:
      return {
        ...state,
        vendorFacilityPostRequest: true,
        vendorFacilityCRUDError: false,
      };
    case VENDOR_FACILITY_POST_SUCCESS:
      return {
        ...state,
        vendorFacilityPostData: action.data,
        vendorFacilityPostRequest: false,
        vendorFacilityCRUDError: false,
      };
    case VENDOR_FACILITY_POST_FAILURE:
      return {
        ...state,
        vendorFacilityErrorMessage: action.data,
        vendorFacilityCRUDError: true,
        vendorFacilityPostRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
