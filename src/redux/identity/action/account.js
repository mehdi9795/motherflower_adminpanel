import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAILURE,
  LOGOUT_REQUEST,
} from '../../../constants/index';
import { LOGOUT_SUCCESS } from '../../../constants';

export function loginRequest(data) {
  return {
    type: LOGIN_REQUEST,
    data,
  };
}

export function loginSuccess(data) {
  return {
    type: LOGIN_SUCCESS,
    data,
  };
}

export function loginFailure() {
  return {
    type: LOGIN_FAILURE,
  };
}

export function logoutRequest() {
  return {
    type: LOGOUT_REQUEST,
  };
}

export function logoutSuccess() {
  return {
    type: LOGOUT_SUCCESS,
  };
}

export function signUpRequest(data) {
  return {
    type: SIGNUP_REQUEST,
    data,
  };
}

export function signUpSuccess(data) {
  return {
    type: SIGNUP_SUCCESS,
    data,
  };
}

export function signUpFailure() {
  return {
    type: SIGNUP_FAILURE,
  };
}
