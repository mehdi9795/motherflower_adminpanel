import {
  CREATE_USER_BACK_URL_REQUEST,
  CREATE_USER_BACK_URL_SUCCESS,
  CREATE_USER_BACK_URL_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="create user back user">
export function createUserBackUrlRequest(data) {
  return {
    type: CREATE_USER_BACK_URL_REQUEST,
    data,
  };
}

export function createUserBackUrlSuccess(data) {
  return {
    type: CREATE_USER_BACK_URL_SUCCESS,
    data,
  };
}

export function createUserBackUrlFailure(error) {
  return {
    type: CREATE_USER_BACK_URL_FAILURE,
    error,
  };
}
// </editor-fold>
