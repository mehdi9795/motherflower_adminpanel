import {
  PERMISSION_LIST_REQUEST,
  PERMISSION_LIST_SUCCESS,
  PERMISSION_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Role new">

export function permissionListRequest() {
  return {
    type: PERMISSION_LIST_REQUEST,
  };
}

export function permissionListSuccess(data) {
  return {
    type: PERMISSION_LIST_SUCCESS,
    data,
  };
}

export function permissionListFailure() {
  return {
    type: PERMISSION_LIST_FAILURE,
  };
}
// </editor-fold>
