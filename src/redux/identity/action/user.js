import {
  USER_LIST_REQUEST,
  USER_LIST_SUCCESS,
  USER_LIST_FAILURE,
  USER_ROLE_REQUEST,
  USER_ROLE_SUCCESS,
  USER_ROLE_FAILURE,
  SINGLE_USER_INIT_SUCCESS,
  SINGLE_USER_INIT_REQUEST,
  SINGLE_USER_INIT_FAILURE,
  USER_POST_REQUEST,
  USER_POST_SUCCESS,
  USER_POST_FAILURE,
  USER_PUT_REQUEST,
  USER_PUT_SUCCESS,
  USER_PUT_FAILURE,
  USER_DELETE_REQUEST,
  USER_DELETE_SUCCESS,
  USER_DELETE_FAILURE,
  USER_ROLE_POST_REQUEST,
  USER_ROLE_POST_SUCCESS,
  USER_ROLE_POST_FAILURE,
  USER_ROLE_DELETE_REQUEST,
  USER_ROLE_DELETE_SUCCESS,
  USER_ROLE_DELETE_FAILURE,
  SINGLE_USER_FAILURE,
  SINGLE_USER_REQUEST,
  SINGLE_USER_SUCCESS,
  USER_ALERT,
  USER_ALERT_RESET,
  USER_ADDRESS_LIST_REQUEST,
  USER_ADDRESS_LIST_SUCCESS,
  USER_ADDRESS_LIST_FAILURE,
  USER_ADDRESS_POST_REQUEST,
  USER_ADDRESS_POST_SUCCESS,
  USER_ADDRESS_POST_FAILURE,
  USER_ADDRESS_PUT_REQUEST,
  USER_ADDRESS_PUT_SUCCESS,
  USER_ADDRESS_PUT_FAILURE,
  USER_ADDRESS_DELETE_REQUEST,
  USER_ADDRESS_DELETE_SUCCESS,
  USER_ADDRESS_DELETE_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="User new">
export function userListRequest(data) {
  return {
    type: USER_LIST_REQUEST,
    data,
  };
}

export function userListSuccess(data) {
  return {
    type: USER_LIST_SUCCESS,
    data,
  };
}

export function userListFailure() {
  return {
    type: USER_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single User">
export function singleUserRequest(data) {
  return {
    type: SINGLE_USER_REQUEST,
    data,
  };
}

export function singleUserSuccess(data) {
  return {
    type: SINGLE_USER_SUCCESS,
    data,
  };
}

export function singleUserFailure() {
  return {
    type: SINGLE_USER_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single User init">
export function singleUserInitRequest() {
  return {
    type: SINGLE_USER_INIT_REQUEST,
  };
}

export function singleUserInitSuccess(data) {
  return {
    type: SINGLE_USER_INIT_SUCCESS,
    data,
  };
}

export function singleUserInitFailure() {
  return {
    type: SINGLE_USER_INIT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="User Role">
export function userRoleRequest(data) {
  return {
    type: USER_ROLE_REQUEST,
    data,
  };
}

export function userRoleSuccess(data) {
  return {
    type: USER_ROLE_SUCCESS,
    data,
  };
}

export function userRoleFailure() {
  return {
    type: USER_ROLES_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="user Post">
export function userPostRequest(data) {
  return {
    type: USER_POST_REQUEST,
    data,
  };
}

export function userPostSuccess() {
  return {
    type: USER_POST_SUCCESS,
  };
}
export function userPostFailure() {
  return {
    type: USER_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="user Put">
export function userPutRequest(data) {
  return {
    type: USER_PUT_REQUEST,
    data,
  };
}

export function userPutSuccess() {
  return {
    type: USER_PUT_SUCCESS,
  };
}
export function userPutFailure() {
  return {
    type: USER_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="User Delete">
export function userDeleteRequest(data) {
  return {
    type: USER_DELETE_REQUEST,
    data,
  };
}

export function userDeleteSuccess() {
  return {
    type: USER_DELETE_SUCCESS,
  };
}
export function userDeleteFailure() {
  return {
    type: USER_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="user role Post">
export function userRolePostRequest(data) {
  return {
    type: USER_ROLE_POST_REQUEST,
    data,
  };
}

export function userRolePostSuccess() {
  return {
    type: USER_ROLE_POST_SUCCESS,
  };
}
export function userRolePostFailure() {
  return {
    type: USER_ROLE_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="User role Delete">
export function userRoleDeleteRequest(data) {
  return {
    type: USER_ROLE_DELETE_REQUEST,
    data,
  };
}

export function userRoleDeleteSuccess() {
  return {
    type: USER_ROLE_DELETE_SUCCESS,
  };
}
export function userRoleDeleteFailure() {
  return {
    type: USER_ROLE_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="User address list">
export function userAddressListRequest(data) {
  return {
    type: USER_ADDRESS_LIST_REQUEST,
    data,
  };
}

export function userAddressListSuccess(data) {
  return {
    type: USER_ADDRESS_LIST_SUCCESS,
    data,
  };
}

export function userAddressListFailure() {
  return {
    type: USER_ADDRESS_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="user address Post">
export function userAddressPostRequest(data) {
  return {
    type: USER_ADDRESS_POST_REQUEST,
    data,
  };
}

export function userAddressPostSuccess() {
  return {
    type: USER_ADDRESS_POST_SUCCESS,
  };
}
export function userAddressPostFailure() {
  return {
    type: USER_ADDRESS_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="user address Put">
export function userAddressPutRequest(data) {
  return {
    type: USER_ADDRESS_PUT_REQUEST,
    data,
  };
}

export function userAdressPutSuccess() {
  return {
    type: USER_ADDRESS_PUT_SUCCESS,
  };
}
export function userAddressPutFailure() {
  return {
    type: USER_ADDRESS_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="User address Delete">
export function userAddressDeleteRequest(data) {
  return {
    type: USER_ADDRESS_DELETE_REQUEST,
    data,
  };
}

export function userAddressDeleteSuccess() {
  return {
    type: USER_ADDRESS_DELETE_SUCCESS,
  };
}
export function userAddressDeleteFailure() {
  return {
    type: USER_ADDRESS_DELETE_FAILURE,
  };
}
// </editor-fold>

export function userAlert(data) {
  return {
    type: USER_ALERT,
    data,
  };
}

export function userAlertReset() {
  return {
    type: USER_ALERT_RESET,
  };
}
