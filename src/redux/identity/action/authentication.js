import {
  LOGIN_LOADING,
  LOGIN_IS_LOADED,
  LOGIN_IS_FAILURE,
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
} from '../../../constants/index';

import { deleteCookie, readCookieFrom } from '../../../utils/index';

export function loginLoading() {
  return {
    type: LOGIN_LOADING,
  };
}

export function login(data) {
  return {
    type: LOGIN_IS_LOADED,
    payload: data,
  };
}

export function loginLoaded(cookies) {
  const ssPId = readCookieFrom(cookies, 'ss-pid');
  const token = readCookieFrom(cookies, 'token');
  const userName = readCookieFrom(cookies, 'userName');
  const displayName = readCookieFrom(cookies, 'displayName');
  const phone = readCookieFrom(cookies, 'phone');
  const email = readCookieFrom(cookies, 'email');

  if (!token && !ssPId) {
    return {
      type: LOGIN_IS_FAILURE,
      payload: {
        status: 0,
        statusText: 'شما لاگین نکرده اید.',
      },
    };
  }
  return {
    type: LOGIN_IS_LOADED,
    payload: {
      token,
      userName,
      displayName,
      phone,
      email,
    },
  };
}

export function loginFailure(error) {
  return {
    type: LOGIN_IS_FAILURE,
    payload: {
      error,
    },
  };
}

export function logOut() {
  if (process.env.BROWSER) {
    deleteCookie('token');
    deleteCookie('userName');
    deleteCookie('displayName');
    deleteCookie('phone');
    deleteCookie('email');

    // hardcoded session attributes
    deleteCookie('ss-pid');
    deleteCookie('ss-opt');
  }
}

export function loginUserRequest() {
  return {
    type: LOGIN_REQUEST,
  };
}

export function loginUserSuccess(data) {
  return {
    type: LOGIN_SUCCESS,
    payload: data,
  };
}

export function loginUserFailure(error) {
  return {
    type: LOGIN_FAILURE,
    payload: {
      error,
    },
  };
}
