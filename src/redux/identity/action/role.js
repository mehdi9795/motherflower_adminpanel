import {
  ROLELIST_LOADING,
  ROLELIST_IS_LOADED,
  ROLELIST_IS_FAILURE,
  ROLE_LIST_REQUEST,
  ROLE_LIST_SUCCESS,
  ROLE_LIST_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="Role new">
export function roleListRequest(data) {
  return {
    type: ROLE_LIST_REQUEST,
    data,
  };
}

export function roleListSuccess(data) {
  return {
    type: ROLE_LIST_SUCCESS,
    data,
  };
}

export function roleListFailure() {
  return {
    type: ROLE_LIST_FAILURE,
  };
}
// </editor-fold>

export function roleListLoading() {
  return {
    type: ROLELIST_LOADING,
  };
}

export function roleListLoaded(data) {
  return {
    type: ROLELIST_IS_LOADED,
    payload: {
      data,
    },
  };
}

// export function roleListFailure(error) {
//   return {
//     type: ROLELIST_IS_FAILURE,
//     payload: {
//       error,
//     },
//   };
// }
