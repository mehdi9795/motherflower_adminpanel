import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
} from '../../../constants/index';

const initialState = {
  loginRequest: false,
  loginData: {},
  loginFailure: false,
  logoutRequest: false,
  logoutSuccess: false,
  signUpRequest: false,
  signUpData: {},
  signUpFailure: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Login">
    case LOGIN_REQUEST:
      return {
        ...state,
        loginRequest: true,
        loginFailure: false,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loginData: action.data,
        loginRequest: false,
        loginFailure: false,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        loginFailure: true,
        loginRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Sign Up">
    case SIGNUP_REQUEST:
      return {
        ...state,
        signUpRequest: true,
        signUpFailure: false,
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        signUpData: action.data,
        signUpRequest: false,
        signFailure: false,
      };
    case SIGNUP_FAILURE:
      return {
        ...state,
        signUpFailure: true,
        signUpRequest: false,
      };
    // </editor-fold>

    case LOGOUT_REQUEST:
      return {
        ...state,
        logoutRequest: true,
        logoutSuccess: false,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        logoutRequest: false,
        logoutSuccess: true,
      };
    default:
      return state;
  }
}
