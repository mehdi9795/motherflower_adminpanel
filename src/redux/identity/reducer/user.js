import {
  USER_LIST_REQUEST,
  USER_LIST_SUCCESS,
  USER_LIST_FAILURE,
  SINGLE_USER_INIT_REQUEST,
  SINGLE_USER_INIT_SUCCESS,
  SINGLE_USER_INIT_FAILURE,
  USER_ROLE_REQUEST,
  USER_ROLE_SUCCESS,
  USER_ROLE_FAILURE,
  SINGLE_USER_REQUEST,
  SINGLE_USER_SUCCESS,
  SINGLE_USER_FAILURE,
  USER_POST_REQUEST,
  USER_POST_SUCCESS,
  USER_POST_FAILURE,
  USER_PUT_REQUEST,
  USER_PUT_SUCCESS,
  USER_PUT_FAILURE,
  USER_DELETE_REQUEST,
  USER_DELETE_SUCCESS,
  USER_DELETE_FAILURE,
  USER_ALERT_RESET,
  USER_ALERT,
  USER_ROLE_POST_REQUEST,
  USER_ROLE_POST_SUCCESS,
  USER_ROLE_POST_FAILURE,
  USER_ROLE_DELETE_REQUEST,
  USER_ROLE_DELETE_SUCCESS,
  USER_ROLE_DELETE_FAILURE,
  USER_ADDRESS_LIST_REQUEST,
  USER_ADDRESS_LIST_SUCCESS,
  USER_ADDRESS_LIST_FAILURE,
  USER_ADDRESS_POST_REQUEST,
  USER_ADDRESS_POST_SUCCESS,
  USER_ADDRESS_POST_FAILURE,
  USER_ADDRESS_PUT_REQUEST,
  USER_ADDRESS_PUT_SUCCESS,
  USER_ADDRESS_PUT_FAILURE,
  USER_ADDRESS_DELETE_REQUEST,
  USER_ADDRESS_DELETE_SUCCESS,
  USER_ADDRESS_DELETE_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  userListRequest: false,
  userListData: getResponseModel,
  userListFailure: false,

  singleUserRequest: false,
  singleUserData: getResponseModel,
  singleUserError: false,

  singleUserInitRequest: false,
  singleUserInitData: getResponseModel,
  singleUserInitError: false,

  userRoleRequest: false,
  userRoleData: getResponseModel,
  userRoleError: false,

  userPostRequest: false,
  userPostData: getResponseModel,

  userPutRequest: false,
  userPutData: getResponseModel,

  userDeleteRequest: false,
  userDeleteData: getResponseModel,

  userRolePostRequest: false,
  userRolePostData: getResponseModel,

  userRoleDeleteRequest: false,
  userRoleDeleteData: getResponseModel,

  userAddressRequest: false,
  userAddressData: getResponseModel,
  userAddressError: false,

  userAddressPostRequest: false,
  userAddressPostData: getResponseModel,

  userAddressPutRequest: false,
  userAddressPutData: getResponseModel,

  userAddressDeleteRequest: false,
  userAddressDeleteData: getResponseModel,

  userCRUDError: false,
  userErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="User List new">
    case USER_LIST_REQUEST:
      return {
        ...state,
        userListRequest: true,
        userListFailure: false,
      };
    case USER_LIST_SUCCESS:
      return {
        ...state,
        userListData: action.data,
        userListRequest: false,
        userListFailure: false,
      };
    case USER_LIST_FAILURE:
      return {
        ...state,
        userListFailure: true,
        userListRequest: false,
      };
    // </editor-fold>

    // <editor-fold desc="Single User">
    case SINGLE_USER_REQUEST:
      return {
        ...state,
        singleUserRequest: true,
        singleUserError: false,
      };
    case SINGLE_USER_SUCCESS:
      return {
        ...state,
        singleUserRequest: false,
        singleUserData: action.data,
      };
    case SINGLE_USER_FAILURE:
      return {
        ...state,
        singleUserRequest: false,
        singleUserError: true,
      };
    // </editor-fold>

    // <editor-fold desc="Single User init">
    case SINGLE_USER_INIT_REQUEST:
      return {
        ...state,
        singleUserInitRequest: true,
        singleUserInitError: false,
      };
    case SINGLE_USER_INIT_SUCCESS:
      return {
        ...state,
        singleUserInitRequest: false,
      };
    case SINGLE_USER_INIT_FAILURE:
      return {
        ...state,
        singleUserInitRequest: false,
        singleUserInitError: true,
      };
    // </editor-fold>

    // <editor-fold desc="User Role new">
    case USER_ROLE_REQUEST:
      return {
        ...state,
        userRoleRequest: true,
        userRoleError: false,
      };
    case USER_ROLE_SUCCESS:
      return {
        ...state,
        userRoleRequest: false,
        userRoleData: action.data,
      };
    case USER_ROLE_FAILURE:
      return {
        ...state,
        userRoleRequest: false,
        userRoleError: true,
        userRoleData: getResponseModel,
      };
    // </editor-fold>

    // <editor-fold desc="User Post">
    case USER_POST_REQUEST:
      return {
        ...state,
        userPostRequest: true,
        userCRUDError: false,
        userPostData: action.data,
      };
    case USER_POST_SUCCESS:
      return {
        ...state,
        userPostRequest: false,
        userCRUDError: false,
      };
    case USER_POST_FAILURE:
      return {
        ...state,
        userErrorMessage: action.data,
        userPostRequest: false,
        userCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="user Put">
    case USER_PUT_REQUEST:
      return {
        ...state,
        userPutRequest: true,
        userCRUDError: false,
        userPutData: action.data,
      };
    case USER_PUT_SUCCESS:
      return {
        ...state,
        userPutRequest: false,
        userCRUDError: false,
      };
    case USER_PUT_FAILURE:
      return {
        ...state,
        userErrorMessage: action.data,
        userPutRequest: false,
        userCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="user Delete">
    case USER_DELETE_REQUEST:
      return {
        ...state,
        userDeleteRequest: true,
        userCRUDError: false,
        userDeleteData: action.data,
      };
    case USER_DELETE_SUCCESS:
      return {
        ...state,
        userDeleteRequest: false,
        userCRUDError: false,
      };
    case USER_DELETE_FAILURE:
      return {
        ...state,
        userErrorMessage: action.data,
        userDeleteRequest: false,
        userCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="User Role Post">
    case USER_ROLE_POST_REQUEST:
      return {
        ...state,
        userRolePostRequest: true,
        userRoleCRUDError: false,
        userRolePostData: action.data,
      };
    case USER_ROLE_POST_SUCCESS:
      return {
        ...state,
        userRolePostRequest: false,
        userRoleCRUDError: false,
      };
    case USER_ROLE_POST_FAILURE:
      return {
        ...state,
        userRoleErrorMessage: action.data,
        userRolePostRequest: false,
        userRoleCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="user Role Delete">
    case USER_ROLE_DELETE_REQUEST:
      return {
        ...state,
        userRoleDeleteRequest: true,
        userRoleCRUDError: false,
        userRoleDeleteData: action.data,
      };
    case USER_ROLE_DELETE_SUCCESS:
      return {
        ...state,
        userRoleDeleteRequest: false,
        userRoleCRUDError: false,
      };
    case USER_ROLE_DELETE_FAILURE:
      return {
        ...state,
        userRoleErrorMessage: action.data,
        userRoleDeleteRequest: false,
        userRoleCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="User address list">
    case USER_ADDRESS_LIST_REQUEST:
      return {
        ...state,
        UserListRequest: true,
        UserListError: false,
      };
    case USER_ADDRESS_LIST_SUCCESS:
      return {
        ...state,
        UserListRequest: false,
        UserListData: action.data,
      };
    case USER_ADDRESS_LIST_FAILURE:
      return {
        ...state,
        UserListRequest: false,
        UserListError: true,
      };
    // </editor-fold>

    // <editor-fold desc="User address Post">
    case USER_ADDRESS_POST_REQUEST:
      return {
        ...state,
        userAddressPostRequest: true,
        userCRUDError: false,
        userAddressPostData: action.data,
      };
    case USER_ADDRESS_POST_SUCCESS:
      return {
        ...state,
        userAddressPostRequest: false,
        userCRUDError: false,
      };
    case USER_ADDRESS_POST_FAILURE:
      return {
        ...state,
        userErrorMessage: action.data,
        userPostRequest: false,
        userAddressCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="user address Put">
    case USER_ADDRESS_PUT_REQUEST:
      return {
        ...state,
        userAddressPutRequest: true,
        userCRUDError: false,
        userAddressPutData: action.data,
      };
    case USER_ADDRESS_PUT_SUCCESS:
      return {
        ...state,
        userAddressPutRequest: false,
        userCRUDError: false,
      };
    case USER_ADDRESS_PUT_FAILURE:
      return {
        ...state,
        userErrorMessage: action.data,
        userAddressPutRequest: false,
        userCRUDError: true,
      };
    // </editor-fold>

    // <editor-fold desc="user address Delete">
    case USER_ADDRESS_DELETE_REQUEST:
      return {
        ...state,
        userAddressDeleteRequest: true,
        userCRUDError: false,
        userAddressDeleteData: action.data,
      };
    case USER_ADDRESS_DELETE_SUCCESS:
      return {
        ...state,
        userAddressDeleteRequest: false,
        userCRUDError: false,
      };
    case USER_ADDRESS_DELETE_FAILURE:
      return {
        ...state,
        userErrorMessage: action.data,
        userAddressDeleteRequest: false,
        userCRUDError: true,
      };
    // </editor-fold>

    case USER_ALERT_RESET:
      return {
        ...state,
        userErrorMessage: '',
        userPostRequest: false,
        userCRUDError: false,
      };

    case USER_ALERT:
      return {
        ...state,
        userErrorMessage: action.data,
        userPostRequest: false,
        userCRUDError: true,
      };

    default:
      return state;
  }
}
