import {
  LOGIN_LOADING,
  LOGIN_IS_LOADED,
  LOGIN_IS_FAILURE,
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
} from '../../../constants/index';

const initialState = {
  loading: false,
  data: [],
  error: false,
  loginRequest: false,
  loginSuccess: [],
  loginError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    case LOGIN_LOADING:
      return {
        ...initialState,
        loading: true,
      };
    case LOGIN_IS_LOADED:
      return {
        ...initialState,
        data: action.payload,
      };
    case LOGIN_IS_FAILURE:
      return {
        ...initialState,
        error: true,
      };

    case LOGIN_REQUEST:
      return {
        ...initialState,
        loginRequest: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...initialState,
        loginSuccess: action.payload,
      };
    case LOGIN_FAILURE:
      return {
        ...initialState,
        loginError: true,
      };
    default:
      return state;
  }
}
