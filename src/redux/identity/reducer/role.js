import {
  ROLELIST_LOADING,
  ROLELIST_IS_LOADED,
  ROLELIST_IS_FAILURE,
  ROLE_LIST_REQUEST,
  ROLE_LIST_SUCCESS,
  ROLE_LIST_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  loading: false,
  data: getResponseModel,
  error: false,
  roleListRequest: false,
  roleListData: getResponseModel,
  roleListFailure: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Role List new">
    case ROLE_LIST_REQUEST:
      return {
        ...state,
        roleListRequest: true,
        roleListFailure: false,
      };
    case ROLE_LIST_SUCCESS:
      return {
        ...state,
        roleListData: action.data,
        roleListRequest: false,
        roleListFailure: false,
      };
    case ROLE_LIST_FAILURE:
      return {
        ...state,
        roleListFailure: true,
        roleListRequest: false,
      };
    // </editor-fold>

    case ROLELIST_LOADING:
      return {
        ...initialState,
        loading: true,
      };
    case ROLELIST_IS_LOADED:
      return {
        ...initialState,
        data: action.payload.data,
      };
    case ROLELIST_IS_FAILURE:
      return {
        ...initialState,
        error: true,
      };
    default:
      return state;
  }
}
