import {
  CREATE_USER_BACK_URL_REQUEST,
  CREATE_USER_BACK_URL_SUCCESS,
  CREATE_USER_BACK_URL_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  backUrlRequest: false,
  backUrlData: getResponseModel,
  backUrlError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    case CREATE_USER_BACK_URL_REQUEST:
      return {
        ...initialState,
        backUrlRequest: true,
      };
    case CREATE_USER_BACK_URL_SUCCESS:
      return {
        ...initialState,
        backUrlData: action.data,
      };
    case CREATE_USER_BACK_URL_FAILURE:
      return {
        ...initialState,
        backUrlError: true,
      };
    default:
      return state;
  }
}
