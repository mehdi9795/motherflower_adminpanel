import { call, put, select } from 'redux-saga/effects';
import {
  REPORT_SETTING_LIST_REQUEST,
  REPORT_SETTING_LIST_SUCCESS,
  REPORT_SETTING_LIST_FAILURE,
  REPORT_SETTING_POST_REQUEST,
  REPORT_SETTING_POST_SUCCESS,
  REPORT_SETTING_POST_FAILURE,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
} from '../../../constants';
import { REPORT_CREATE_IS_SUCCESS } from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* reportSettingListRequest(
  getDtoQueryString,
  getReportSettingApi,
  action,
) {
  console.log('action.dto', action);
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getReportSettingApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: REPORT_SETTING_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: REPORT_SETTING_LIST_FAILURE });
  }
}

export function* reportSettingPostRequest(postReportSettingApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    postReportSettingApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: REPORT_SETTING_POST_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: REPORT_SETTING_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* reportSettingPostSuccess(message, action) {
  message.success(REPORT_CREATE_IS_SUCCESS, 10);
  yield put({ type: REPORT_SETTING_LIST_REQUEST, dto: action.data });
}
