import { call, put } from 'redux-saga/effects';
import { setCookie, deleteCookie } from '../../../utils/index';

import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  PERMISSION_LIST_REQUEST,
} from '../../../constants';
import {
  postAuthenticateApi,
  UserVerificationApi,
} from '../../../services/identityApi';

export function* loginRequest(history, message, UserVerificationApi, action) {
  let response;
  if (action.dto.fromLogin)
    response = yield call(postAuthenticateApi, action.dto.data);
  else response = yield call(UserVerificationApi, action.dto.data);

  if (response.status === 200) {
    if (response.data.bearerToken) {
      const authenticationCookieExpireHours = 8760;
      setCookie(
        'token',
        response.data.bearerToken,
        authenticationCookieExpireHours,
      );
      setCookie(
        'userName',
        response.data.userName,
        authenticationCookieExpireHours,
      );
      setCookie(
        'displayName',
        response.data.displayName,
        authenticationCookieExpireHours,
      );
      setCookie(
        'imageUser',
        response.data.meta ? response.data.meta.imageUrl : '',
        authenticationCookieExpireHours,
      );
      // setCookie('phone', response.meta.phone, authenticationCookieExpireHours);
      // setCookie('email', response.meta.email, authenticationCookieExpireHours);
    }

    if (response.data.meta.vendorBranchIds)
      setCookie('appType', 'Florist', 8760);
    else setCookie('appType', 'Admin', 8760);

    const data = {
      token: response.data.bearerToken,
      userName: response.data.userName,
      displayName: response.data.displayName,
      appType: response.data.meta.vendorBranchIds ? 'Florist' : 'Admin',
      imageUser: response.data.meta.imageUrl || '',
    };
    yield put({ type: LOGIN_SUCCESS, data });
    if (action.dto.backUrl !== '/') {
      const permissionData = {
        data: action.dto.permissionList,
        backUrl: action.dto.backUrl,
      };
      yield put({ type: PERMISSION_LIST_REQUEST, data: permissionData });
    }
    history.push('/');
  } else {
    yield put({ type: LOGIN_FAILURE });
    message.error(response.data.errorMessage, 10);
  }
}

export function logOut(history) {
  if (process.env.BROWSER) {
    deleteCookie('token');
    deleteCookie('userName');
    deleteCookie('displayName');
    deleteCookie('displayName');
    // deleteCookie('phone');
    // deleteCookie('email');

    // hardcoded session attributes
    deleteCookie('ss-pid');
    deleteCookie('ss-opt');
    history.push('/login');
    // yield put({ type: LOGOUT_SUCCESS });
  }
}
