import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import { getDtoQueryString } from '../../../utils/helper';
import {
  ROLE_LIST_REQUEST,
  SECONDARY_LEVEL_LIST_REQUEST,
  USER_LIST_REQUEST,
  USER_ROLE_REQUEST,
  LOGIN_REQUEST,
  LOGOUT_REQUEST,
  USER_POST_REQUEST,
  USER_POST_SUCCESS,
  USER_PUT_REQUEST,
  USER_PUT_SUCCESS,
  USER_DELETE_REQUEST,
  USER_DELETE_SUCCESS,
  USER_ROLE_POST_REQUEST,
  USER_ROLE_POST_SUCCESS,
  USER_ROLE_DELETE_REQUEST,
  USER_ROLE_DELETE_SUCCESS,
  SINGLE_USER_REQUEST,
  LOGIN_SUCCESS,
  PERMISSION_LIST_REQUEST,
  USER_ADDRESS_LIST_SUCCESS,
  USER_ADDRESS_LIST_REQUEST,
  USER_APP_INFO_LIST_REQUEST,
  REPORT_SETTING_LIST_REQUEST,
  REPORT_SETTING_POST_REQUEST,
  REPORT_SETTING_POST_SUCCESS,
  USER_EVENT_LIST_REQUEST,
  USER_EVENT_POST_REQUEST,
  USER_EVENT_POST_SUCCESS,
  USER_EVENT_PUT_REQUEST,
  USER_EVENT_PUT_SUCCESS,
  USER_EVENT_DELETE_REQUEST,
  USER_EVENT_DELETE_SUCCESS,
  USER_EVENT_TYPE_LIST_REQUEST,
  USER_RELATION_SHIP_LIST_REQUEST,
  INVITED_USER_LIST_REQUEST,
  USER_ADDRESS_POST_REQUEST,
  USER_ADDRESS_POST_SUCCESS,
  USER_ADDRESS_PUT_REQUEST,
  USER_ADDRESS_PUT_SUCCESS,
  USER_ADDRESS_DELETE_REQUEST,
  USER_ADDRESS_DELETE_SUCCESS,
  USER_CARD_NUMBER_LIST_SUCCESS, USER_CARD_NUMBER_LIST_REQUEST,
} from '../../../constants';
import {
  getUserApi,
  getRoleApi,
  getUserRoleApi,
  postAuthenticateApi,
  postUserApi,
  putUserApi,
  deleteUserApi,
  postUserRoleApi,
  deleteUserRoleApi,
  getPermissionApi,
  getUserReportSettingApi,
  postUserReportSettingApi,
  getUserAppInfoAppTypesApi,
  getUserEventApi,
  postUserEventApi,
  putUserEventApi,
  deleteUserEventApi,
  getUserEventTypeApi,
  getUserRelationShipApi,
  UserVerificationApi,
  getUserAddressApi,
  postUserAddressApi,
  putUserAddressApi,
  deleteUserAddressApi,
} from '../../../services/identityApi';
import {
  userListRequest,
  userRoleRequest,
  userDeleteRequest,
  userDeleteSuccess,
  userPostRequest,
  userPostSuccess,
  userPutRequest,
  userPutSuccess,
  userRolePostRequest,
  userRolePostSuccess,
  userRoleDeleteRequest,
  userRoleDeleteSuccess,
  singleUserRequest,
  userAddressListRequest,
  userAppInfoListRequest,
  userEventPostSuccess,
  userEventPutRequest,
  userEventPutSuccess,
  userEventDeleteRequest,
  userEventDeleteSuccess,
  userEventTypeListRequest,
  userRelationShipListRequest,
  userEventListRequest,
  userEventPostRequest,
  invitedUserListRequest,
  userAddressPostRequest,
  userAddressPostSuccess,
  userAddressPutRequest,
  userAddressPutSuccess, userAddressDeleteRequest, userAddressDeleteSuccess, userCardNumberListRequest,
} from './user';
import { loginRequest, logOut } from './account';
import { secondaryLevelCategoryListRequest } from '../../catalog/saga/category';
import { roleListRequest } from './role';
import { getCategoryApi } from '../../../services/catalogApi';
import history from '../../../history';
import alert from '../../../components/CP/CPAlert';
import { permissionListRequest } from './permission';
import {
  reportSettingListRequest,
  reportSettingPostRequest,
  reportSettingPostSuccess,
} from './reportSetting';
import {UserBankHistoriesApi} from "../../../services/samApi";

// main saga generators
export default function* sagaIdentityIndex() {
  // yield takeLatest(testRequestFlow);
  // yield all(takeLatest(testRequestFlow));
  yield all([
    // <editor-fold dsc="account">
    takeLatest(
      LOGIN_REQUEST,
      loginRequest,
      history,
      message,
      UserVerificationApi,
    ),
    takeLatest(LOGOUT_REQUEST, logOut, history),
    // takeLatest(LOGIN_SUCCESS, loginSuccess, history),

    // </editor-fold>

    // <editor-fold dsc="User">
    takeLatest(
      USER_APP_INFO_LIST_REQUEST,
      userAppInfoListRequest,
      getDtoQueryString,
      getUserAppInfoAppTypesApi,
    ),
    takeLatest(
      USER_LIST_REQUEST,
      userListRequest,
      getDtoQueryString,
      getUserApi,
    ),
    takeLatest(
      USER_CARD_NUMBER_LIST_REQUEST,
      userCardNumberListRequest,
      getDtoQueryString,
      UserBankHistoriesApi,
    ),
    takeLatest(
      SINGLE_USER_REQUEST,
      singleUserRequest,
      getDtoQueryString,
      getUserApi,
    ),
    // takeEvery(
    //   SINGLE_USER_INIT_REQUEST,
    //   userSingleInitRequest,
    //   getDtoQueryString,
    //   getUserApi,
    //   getAgentApi,
    // ),
    takeLatest(
      USER_ROLE_REQUEST,
      userRoleRequest,
      getDtoQueryString,
      getUserRoleApi,
    ),
    takeLatest(
      INVITED_USER_LIST_REQUEST,
      invitedUserListRequest,
      getDtoQueryString,
      getUserApi,
    ),

    takeLatest(USER_POST_REQUEST, userPostRequest, postUserApi, message, alert),
    takeLatest(USER_POST_SUCCESS, userPostSuccess, history, message),
    takeLatest(USER_PUT_REQUEST, userPutRequest, putUserApi),
    takeLatest(USER_PUT_SUCCESS, userPutSuccess, history, message),
    takeLatest(USER_DELETE_REQUEST, userDeleteRequest, deleteUserApi),
    takeLatest(USER_DELETE_SUCCESS, userDeleteSuccess, history, message),

    takeLatest(
      USER_ROLE_POST_REQUEST,
      userRolePostRequest,
      postUserRoleApi,
      message,
      alert,
    ),
    takeLatest(USER_ROLE_POST_SUCCESS, userRolePostSuccess, history, message),
    takeLatest(
      USER_ROLE_DELETE_REQUEST,
      userRoleDeleteRequest,
      getDtoQueryString,
      deleteUserRoleApi,
    ),
    takeLatest(
      USER_ROLE_DELETE_SUCCESS,
      userRoleDeleteSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="Role">
    takeLatest(
      ROLE_LIST_REQUEST,
      roleListRequest,
      getDtoQueryString,
      getRoleApi,
    ),
    takeLatest(
      SECONDARY_LEVEL_LIST_REQUEST,
      secondaryLevelCategoryListRequest,
      getDtoQueryString,
      getCategoryApi,
    ),
    // </editor-fold>

    // <editor-fold dsc="Permission">
    takeLatest(
      PERMISSION_LIST_REQUEST,
      permissionListRequest,
      getDtoQueryString,
      getPermissionApi,
      history,
    ),
    // </editor-fold>

    // <editor-fold dsc="User address">
    takeLatest(
      USER_ADDRESS_LIST_REQUEST,
      userAddressListRequest,
      getDtoQueryString,
      getUserAddressApi,
    ),
    takeLatest(
      USER_ADDRESS_POST_REQUEST,
      userAddressPostRequest,
      postUserAddressApi,
    ),
    takeLatest(
      USER_ADDRESS_POST_SUCCESS,
      userAddressPostSuccess,
      history,
      message,
    ),
    takeLatest(
      USER_ADDRESS_PUT_REQUEST,
      userAddressPutRequest,
      putUserAddressApi,
    ),
    takeLatest(
      USER_ADDRESS_PUT_SUCCESS,
      userAddressPutSuccess,
      history,
      message,
    ),
    takeLatest(
      USER_ADDRESS_DELETE_REQUEST,
      userAddressDeleteRequest,
      deleteUserAddressApi,
    ),
    takeLatest(
      USER_ADDRESS_DELETE_SUCCESS,
      userAddressDeleteSuccess,
      history,
      message,
    ),
    takeLatest(
      SINGLE_USER_REQUEST,
      singleUserRequest,
      getDtoQueryString,
      getUserApi,
    ),
    // takeEvery(
    //   SINGLE_USER_INIT_REQUEST,
    //   userSingleInitRequest,
    //   getDtoQueryString,
    //   getUserApi,
    //   getAgentApi,
    // ),
    // </editor-fold>

    // <editor-fold dsc="User report setting">
    takeLatest(
      REPORT_SETTING_LIST_REQUEST,
      reportSettingListRequest,
      getDtoQueryString,
      getUserReportSettingApi,
    ),
    takeLatest(
      REPORT_SETTING_POST_REQUEST,
      reportSettingPostRequest,
      postUserReportSettingApi,
    ),

    takeLatest(REPORT_SETTING_POST_SUCCESS, reportSettingPostSuccess, message),
    // </editor-fold>

    // <editor-fold dsc="User event">
    takeLatest(
      USER_EVENT_LIST_REQUEST,
      userEventListRequest,
      getDtoQueryString,
      getUserEventApi,
    ),
    takeLatest(USER_EVENT_POST_REQUEST, userEventPostRequest, postUserEventApi),

    takeLatest(USER_EVENT_POST_SUCCESS, userEventPostSuccess, message),

    takeLatest(USER_EVENT_PUT_REQUEST, userEventPutRequest, putUserEventApi),
    takeLatest(USER_EVENT_PUT_SUCCESS, userEventPutSuccess, message),
    takeLatest(
      USER_EVENT_DELETE_REQUEST,
      userEventDeleteRequest,
      deleteUserEventApi,
    ),
    takeLatest(USER_EVENT_DELETE_SUCCESS, userEventDeleteSuccess, message),
    // </editor-fold>

    // <editor-fold dsc="User event type">
    takeLatest(
      USER_EVENT_TYPE_LIST_REQUEST,
      userEventTypeListRequest,
      getDtoQueryString,
      getUserEventTypeApi,
    ),
    // </editor-fold>

    // <editor-fold dsc="User relation ship">
    takeLatest(
      USER_RELATION_SHIP_LIST_REQUEST,
      userRelationShipListRequest,
      getDtoQueryString,
      getUserRelationShipApi,
    ),
    // </editor-fold>
  ]);
}
