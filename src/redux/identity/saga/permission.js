import { call, put, select } from 'redux-saga/effects';
import {
  PERMISSION_LIST_SUCCESS,
  PERMISSION_LIST_FAILURE,
} from '../../../constants';

const getCurrentSession = state => state.login.data;

export function* permissionListRequest(
  getDtoQueryString,
  getPermissionApi,
  history,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.data.data);
  const response = yield call(
    getPermissionApi,
    currentSession.token,
    container,
  );

  if (response.status === 200) {
    yield put({ type: PERMISSION_LIST_SUCCESS, data: response.data.listDto });
    if (action.data.backUrl) history.push(action.data.backUrl);
  } else {
    yield put({ type: PERMISSION_LIST_FAILURE });
  }
}
