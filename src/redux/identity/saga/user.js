import { all, call, put, select } from 'redux-saga/effects';
import {
  VENDOR_LIST_SUCCESS,
  SINGLE_USER_INIT_SUCCESS,
  SINGLE_USER_INIT_FAILURE,
  USER_LIST_SUCCESS,
  USER_LIST_FAILURE,
  USER_ROLE_SUCCESS,
  USER_ROLE_FAILURE,
  SINGLE_USER_SUCCESS,
  SINGLE_USER_FAILURE,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  USER_POST_SUCCESS,
  USER_POST_FAILURE,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  USER_PUT_SUCCESS,
  USER_PUT_FAILURE,
  SINGLE_USER_REQUEST,
  USER_LIST_REQUEST,
  USER_DELETE_SUCCESS,
  USER_DELETE_FAILURE,
  USER_ADDRESS_LIST_SUCCESS,
  USER_ADDRESS_LIST_FAILURE,
  USER_ADDRESS_LIST_REQUEST,
  USER_ROLE_REQUEST,
  USER_ROLE_POST_SUCCESS,
  USER_ROLE_POST_FAILURE,
  USER_ROLE_DELETE_SUCCESS,
  USER_APP_INFO_LIST_SUCCESS,
  USER_APP_INFO_LIST_FAILURE,
  USER_EVENT_LIST_SUCCESS,
  USER_EVENT_LIST_FAILURE,
  USER_EVENT_POST_FAILURE,
  USER_EVENT_POST_SUCCESS,
  USER_EVENT_PUT_SUCCESS,
  USER_EVENT_PUT_FAILURE,
  USER_EVENT_DELETE_SUCCESS,
  USER_EVENT_DELETE_FAILURE,
  USER_EVENT_TYPE_LIST_SUCCESS,
  USER_EVENT_TYPE_LIST_FAILURE,
  USER_RELATION_SHIP_LIST_SUCCESS,
  USER_RELATION_SHIP_LIST_FAILURE,
  USER_EVENT_LIST_REQUEST,
  INVITED_USER_LIST_SUCCESS,
  INVITED_USER_LIST_FAILURE,
  USER_ADDRESS_POST_SUCCESS,
  USER_ADDRESS_POST_FAILURE,
  USER_ADDRESS_PUT_SUCCESS,
  USER_ADDRESS_PUT_FAILURE,
  USER_ADDRESS_DELETE_SUCCESS,
  USER_ADDRESS_DELETE_FAILURE,
  USER_CARD_NUMBER_LIST_SUCCESS,
  USER_CARD_NUMBER_LIST_FAILURE,
} from '../../../constants';
import {
  USER_ADDRESS_CREATE_IS_SUCCESS,
  USER_ADDRESS_DELETE_IS_SUCCESS,
  USER_ADDRESS_UPDATE_IS_SUCCESS,
  USER_CREATE_IS_SUCCESS,
  USER_DELETE_IS_SUCCESS,
  USER_ROLE_ADD_IS_SUCCESS,
  USER_UPDATE_IS_SUCCESS,
  USER_ROLE_DELETE_IS_SUCCESS,
  USER_EVENT_DELETE_IS_SUCCESS,
  USER_EVENT_PUT_IS_SUCCESS,
  USER_EVENT_POST_IS_SUCCESS,
} from '../../../Resources/Localization';
import { putBillApi } from '../../../services/samApi';

const getCurrentSession = state => state.login.data;

export function* userAppInfoListRequest(
  getDtoQueryString,
  getUserAppInfoAppTypesApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getUserAppInfoAppTypesApi,
    currentSession.token,
    container,
  );
  if (response.status === 200) {
    yield put({ type: USER_APP_INFO_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: USER_APP_INFO_LIST_FAILURE });
  }
}

export function* userListRequest(getDtoQueryString, getUserApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getUserApi, currentSession.token, container);
  if (response.status === 200) {
    yield put({ type: USER_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: USER_LIST_FAILURE });
  }
}

export function* userCardNumberListRequest(
  getDtoQueryString,
  getUserCardNumberApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getUserCardNumberApi,
    currentSession.token,
    container,
  );
  if (response.status === 200) {
    yield put({ type: USER_CARD_NUMBER_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: USER_CARD_NUMBER_LIST_FAILURE });
  }
}

export function* userSingleInitRequest(
  getDtoQueryString,
  getUserApi,
  getAgentApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const [user, agents] = yield all([
    call(
      getUserApi,
      currentSession.token,
      getDtoQueryString(action.data.userDto),
    ),
    call(
      getAgentApi,
      currentSession.token,
      getDtoQueryString(action.data.vendorDto),
    ),
  ]);
  if (user && agents) {
    yield put({ type: SINGLE_USER_SUCCESS, data: user.data });
    yield put({ type: VENDOR_LIST_SUCCESS, data: agents.data });

    yield put({ type: SINGLE_USER_INIT_SUCCESS });
  } else {
    yield put({ type: SINGLE_USER_INIT_FAILURE });
  }
}

export function* singleUserRequest(getDtoQueryString, getUserApi, action) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);
  const response = yield call(getUserApi, currentSession.token, container);
  if (response) {
    yield put({ type: SINGLE_USER_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_USER_FAILURE });
  }
}

export function* invitedUserListRequest(getDtoQueryString, getUserApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getUserApi, currentSession.token, container);
  if (response.status === 200) {
    yield put({ type: INVITED_USER_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: INVITED_USER_LIST_FAILURE });
  }
}

export function* createUserBackUrlRequest(getDtoQueryString) {
  yield put({ type: USER_LIST_SUCCESS, data: getDtoQueryString });
}

export function* userAddressListRequest(
  getDtoQueryString,
  getUserAddressApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getUserAddressApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: USER_ADDRESS_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: USER_ADDRESS_LIST_FAILURE });
  }
}

export function* userRoleRequest(getDtoQueryString, getUserRoleApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getUserRoleApi, currentSession.token, container);

  if (response.status === 200) {
    yield put({ type: USER_ROLE_SUCCESS, data: response.data });
  } else {
    yield put({ type: USER_ROLE_FAILURE });
  }
}

export function* userPostRequest(postUserApi, message, alert, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postUserApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.data.isSuccess) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (
      action.dto.status === 'saveAndContinue' ||
      action.dto.status === 'backToVendorBranch'
    ) {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto };
    }
    yield put({
      type: USER_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function userPostSuccess(history, message, action) {
  if (action.data.data.status === 'backToVendorBranch') {
    message.success(USER_CREATE_IS_SUCCESS, 10);
    history.push(
      `/user/edit/${action.data.id}?vendorBranchId=${
        action.data.data.vendorBranchId
      }&backTo=vendorBranch`,
    );
  } else if (action.data.data.status === 'saveAndContinue') {
    message.success(USER_CREATE_IS_SUCCESS, 10);
    history.push(`/user/edit/${action.data.id}?backTo=list}`);
  } else {
    message.success(USER_CREATE_IS_SUCCESS, 10);
    history.push('/user/list');
  }
}

export function* userPutRequest(putUserApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putUserApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto };
    }
    yield put({
      type: USER_PUT_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* userPutSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue') {
    // yield put({
    //   type: USER_LIST_REQUEST,
    //   data: action.data.data.listDto,
    // });
    yield put({
      type: SINGLE_USER_REQUEST,
      dto: `{dto:{id:${action.data.id}}`,
    });
    message.success(USER_UPDATE_IS_SUCCESS, 10);
  } else {
    message.success(USER_UPDATE_IS_SUCCESS, 10);
    history.push('/user/list');
  }
}

export function* userDeleteRequest(deleteUserApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteUserApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* userDeleteSuccess(history, message, action) {
  message.success(USER_DELETE_IS_SUCCESS, 10);

  if (action.data.status === 'edit') {
    history.push('/user/list');
  } else {
    yield put({
      type: USER_LIST_REQUEST,
      dto: action.data.listDto,
    });
  }
}

export function* userRolePostRequest(postUserRoleApi, message, alert, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postUserRoleApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_ROLE_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_ROLE_POST_FAILURE,
    });
    yield put({
      type: 'USER_ALERT',
      data: response.data.errorMessage,
    });

    // message.error(response.data.errorMessage, 30);
  }
}
export function* userRolePostSuccess(history, message, action) {
  message.success(USER_ROLE_ADD_IS_SUCCESS, 5);

  yield put({
    type: USER_ROLE_REQUEST,
    data: action.data.listDto,
  });
}

export function* userRoleDeleteRequest(
  getDtoQueryString,
  deleteUserRoleApi,
  action,
) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const deleteQueryString = `?dtos=${getDtoQueryString(action.dto.data)}`;
  const response = yield call(
    deleteUserRoleApi,
    currentSession.token,
    deleteQueryString,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_ROLE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: 'USER_ROLE_DELETE_FAILURE',
    });
    yield put({
      type: 'USER_ALERT',
      data: response.data.errorMessage,
    });
  }
}
export function* userRoleDeleteSuccess(history, message, action) {
  message.success(USER_ROLE_DELETE_IS_SUCCESS, 5);

  yield put({
    type: USER_ROLE_REQUEST,
    dto: action.data.listDto,
  });
}

export function* userAddressPostRequest(postUserAddressApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postUserAddressApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });

    yield put({
      type: USER_ADDRESS_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_ADDRESS_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* userAddressPostSuccess(history, message, action) {
  message.success(USER_ADDRESS_CREATE_IS_SUCCESS, 10);
  yield put({
    type: USER_ADDRESS_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* userAddressPutRequest(putUserAddressApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putUserAddressApi,
    currentSession.token,
    action.dto.data,
  );
  if (action.dto.putAddressBill)
    yield call(putBillApi, currentSession.token, action.dto.putAddressBill);

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_ADDRESS_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_ADDRESS_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* userAddressPutSuccess(history, message, action) {
  if (action.data.listDto)
    yield put({
      type: USER_ADDRESS_LIST_REQUEST,
      dto: action.data.listDto,
    });

  message.success(USER_ADDRESS_UPDATE_IS_SUCCESS, 10);
}

export function* userAddressDeleteRequest(deleteUserAddressApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteUserAddressApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_ADDRESS_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_ADDRESS_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* userAddressDeleteSuccess(history, message, action) {
  message.success(USER_ADDRESS_DELETE_IS_SUCCESS, 10);
  yield put({
    type: USER_ADDRESS_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* userEventListRequest(
  getDtoQueryString,
  getUserEventApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getUserEventApi, currentSession.token, container);
  if (response.status === 200) {
    yield put({ type: USER_EVENT_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: USER_EVENT_LIST_FAILURE });
  }
}

export function* userEventPostRequest(postUserEventApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postUserEventApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.data.isSuccess) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    const data = { data: action.dto };
    yield put({
      type: USER_EVENT_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_EVENT_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* userEventPostSuccess(message, action) {
  message.success(USER_EVENT_POST_IS_SUCCESS, 10);
  yield put({
    type: USER_EVENT_LIST_REQUEST,
    dto: action.data.data.dto,
  });
}

export function* userEventPutRequest(putUserEventApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putUserEventApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    const data = { data: action.dto };
    yield put({
      type: USER_EVENT_PUT_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_EVENT_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* userEventPutSuccess(message, action) {
  message.success(USER_EVENT_PUT_IS_SUCCESS, 10);
  yield put({
    type: USER_EVENT_LIST_REQUEST,
    dto: action.data.data.dto,
  });
}

export function* userEventDeleteRequest(deleteUserEventApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteUserEventApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_EVENT_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: USER_EVENT_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* userEventDeleteSuccess(message, action) {
  message.success(USER_EVENT_DELETE_IS_SUCCESS, 10);
  yield put({
    type: USER_EVENT_LIST_REQUEST,
    dto: action.data.dto,
  });
}

export function* userEventTypeListRequest(
  getDtoQueryString,
  getUserEventTypeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getUserEventTypeApi,
    currentSession.token,
    container,
  );
  if (response.status === 200) {
    yield put({ type: USER_EVENT_TYPE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: USER_EVENT_TYPE_LIST_FAILURE });
  }
}

export function* userRelationShipListRequest(
  getDtoQueryString,
  getUserRelationShipApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getUserRelationShipApi,
    currentSession.token,
    container,
  );
  if (response.status === 200) {
    yield put({ type: USER_RELATION_SHIP_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: USER_RELATION_SHIP_LIST_FAILURE });
  }
}
