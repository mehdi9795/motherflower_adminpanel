import { call, put, select } from 'redux-saga/effects';
import { ROLE_LIST_SUCCESS, ROLE_LIST_FAILURE } from '../../../constants/index';

const getCurrentSession = state => state.login.data;

export function* roleListRequest(getDtoQueryString, getRoleApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getRoleApi, currentSession.token, container);
  if (response) {
    yield put({ type: ROLE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: ROLE_LIST_FAILURE });
  }
}
