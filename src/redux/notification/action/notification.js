import {
  GET_NOTIFICATION_REQUEST,
  GET_NOTIFICATION_SUCCESS,
  GET_NOTIFICATION_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="notification list">
export function getNotificationRequest(data) {
  return {
    type: GET_NOTIFICATION_REQUEST,
    data,
  };
}

export function getNotificationSuccess(data) {
  return {
    type: GET_NOTIFICATION_SUCCESS,
    data,
  };
}

export function getNotificationFailure() {
  return {
    type: GET_NOTIFICATION_FAILURE,
  };
}
// </editor-fold>
