import {
  MESSAGE_LIST_REQUEST,
  MESSAGE_LIST_SUCCESS,
  MESSAGE_LIST_FAILURE,
  MESSAGE_POST_REQUEST,
  MESSAGE_POST_SUCCESS,
  MESSAGE_POST_FAILURE,
  MESSAGE_PUT_REQUEST,
  MESSAGE_PUT_SUCCESS,
  MESSAGE_PUT_FAILURE,
  MESSAGE_DELETE_REQUEST,
  MESSAGE_DELETE_SUCCESS,
  MESSAGE_DELETE_FAILURE,
  MESSAGE_DETAILS_SUCCESS,
} from '../../../constants';

// <editor-fold dsc="message list">
export function messageListRequest(data) {
  return {
    type: MESSAGE_LIST_REQUEST,
    data,
  };
}

export function messageListSuccess(data) {
  return {
    type: MESSAGE_LIST_SUCCESS,
    data,
  };
}

export function messageListFailure() {
  return {
    type: MESSAGE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="message post">
export function messagePostRequest(data) {
  return {
    type: MESSAGE_POST_REQUEST,
    data,
  };
}

export function messagePostSuccess(data) {
  return {
    type: MESSAGE_POST_SUCCESS,
    data,
  };
}

export function messagePostFailure() {
  return {
    type: MESSAGE_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="message put">
export function messagePutRequest(data) {
  return {
    type: MESSAGE_PUT_REQUEST,
    data,
  };
}

export function messagePutSuccess(data) {
  return {
    type: MESSAGE_PUT_SUCCESS,
    data,
  };
}

export function messagePutFailure() {
  return {
    type: MESSAGE_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="message delete">
export function messageDeleteRequest(data) {
  return {
    type: MESSAGE_DELETE_REQUEST,
    data,
  };
}

export function messageDeleteSuccess(data) {
  return {
    type: MESSAGE_DELETE_SUCCESS,
    data,
  };
}

export function messageDeleteFailure() {
  return {
    type: MESSAGE_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="message details">

export function messageDetailsSuccess(data) {
  return {
    type: MESSAGE_DETAILS_SUCCESS,
    data,
  };
}

// </editor-fold>
