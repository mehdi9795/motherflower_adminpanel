import {
  REQUEST_TYPE_LIST_REQUEST,
  REQUEST_TYPE_LIST_SUCCESS,
  REQUEST_TYPE_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="request type list">
export function requestTypeListRequest(data) {
  return {
    type: REQUEST_TYPE_LIST_REQUEST,
    data,
  };
}

export function requestTypeListSuccess(data) {
  return {
    type: REQUEST_TYPE_LIST_SUCCESS,
    data,
  };
}

export function requestTypeListFailure() {
  return {
    type: REQUEST_TYPE_LIST_FAILURE,
  };
}
// </editor-fold>
