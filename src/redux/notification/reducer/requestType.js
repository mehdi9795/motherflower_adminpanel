import { getResponseModel } from '../../../utils/model';
import {
  REQUEST_TYPE_LIST_REQUEST,
  REQUEST_TYPE_LIST_SUCCESS,
  REQUEST_TYPE_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  REQUEST_TYPE_ListRequest: false,
  REQUEST_TYPE_ListData: getResponseModel,
  REQUEST_TYPE_ListFailure: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="request Type list">
    case REQUEST_TYPE_LIST_REQUEST:
      return {
        ...state,
        requestTypeListLoading: true,
        requestTypeListFailure: false,
      };
    case REQUEST_TYPE_LIST_SUCCESS:
      return {
        ...state,
        requestTypeListData: action.data,
        requestTypeListLoading: false,
        requestTypeListFailure: false,
      };
    case REQUEST_TYPE_LIST_FAILURE:
      return {
        ...state,
        requestTypeListFailure: true,
        requestTypeListLoading: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
