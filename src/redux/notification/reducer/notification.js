import {
  GET_NOTIFICATION_REQUEST,
  GET_NOTIFICATION_SUCCESS,
  GET_NOTIFICATION_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  notificationListRequest: false,
  notificationListData: getResponseModel,
  notificationListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="notification List">
    case GET_NOTIFICATION_REQUEST:
      return {
        ...state,
        notificationListRequest: true,
        notificationListError: false,
      };
    case GET_NOTIFICATION_SUCCESS:
      return {
        ...state,
        notificationListData: action.data,
        notificationListRequest: false,
        notificationListError: false,
      };
    case GET_NOTIFICATION_FAILURE:
      return {
        ...state,
        notificationListError: true,
        notificationListRequest: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
