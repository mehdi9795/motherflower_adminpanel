import { getResponseModel } from '../../../utils/model';
import {
  MESSAGE_LIST_REQUEST,
  MESSAGE_LIST_SUCCESS,
  MESSAGE_LIST_FAILURE,
  MESSAGE_POST_REQUEST,
  MESSAGE_POST_SUCCESS,
  MESSAGE_POST_FAILURE,
  MESSAGE_PUT_REQUEST,
  MESSAGE_PUT_SUCCESS,
  MESSAGE_PUT_FAILURE,
  MESSAGE_DELETE_REQUEST,
  MESSAGE_DELETE_SUCCESS,
  MESSAGE_DELETE_FAILURE,
  MESSAGE_DETAILS_SUCCESS,
} from '../../../constants';

const initialState = {
  messageListRequest: false,
  messageListData: getResponseModel,
  messageListFailure: false,

  messagePostLoading: false,
  messagePostData: getResponseModel,

  messagePutLoading: false,
  messagePutData: getResponseModel,

  messageDeleteLoading: false,
  messageDeleteData: getResponseModel,

  messageDetails: {},

  messageCRUDError: false,
  messageErrorMessage: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="message list">
    case MESSAGE_LIST_REQUEST:
      return {
        ...state,
        messageListLoading: true,
        messageListFailure: false,
      };
    case MESSAGE_LIST_SUCCESS:
      return {
        ...state,
        messageListData: action.data,
        messageListLoading: false,
        messageListFailure: false,
      };
    case MESSAGE_LIST_FAILURE:
      return {
        ...state,
        messageListFailure: true,
        messageListLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="message put">
    case MESSAGE_PUT_REQUEST:
      return {
        ...state,
        messagePutLoading: true,
        messageCRUDError: false,
      };
    case MESSAGE_PUT_SUCCESS:
      return {
        ...state,
        messagePutData: action.data,
        messagePutLoading: false,
        messageCRUDError: false,
      };
    case MESSAGE_PUT_FAILURE:
      return {
        ...state,
        messageErrorMessage: action.data,
        messageCRUDError: true,
        messagePostLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="message post">
    case MESSAGE_POST_REQUEST:
      return {
        ...state,
        messagePostLoading: true,
        messageCRUDError: false,
      };
    case MESSAGE_POST_SUCCESS:
      return {
        ...state,
        messagePutData: action.data,
        messagePutLoading: false,
        messageCuDError: false,
      };
    case MESSAGE_POST_FAILURE:
      return {
        ...state,
        messageErrorMessage: action.data,
        messageCRUDError: true,
        messagePostLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="message delete">
    case MESSAGE_DELETE_REQUEST:
      return {
        ...state,
        messageDeleteLoading: true,
        messageCRUDError: false,
      };
    case MESSAGE_DELETE_SUCCESS:
      return {
        ...state,
        messageDeleteData: action.data,
        messageDeleteLoading: false,
        messageCuDError: false,
      };
    case MESSAGE_DELETE_FAILURE:
      return {
        ...state,
        messageErrorMessage: action.data,
        messageCRUDError: true,
        messageDeleteLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="message details">
    case MESSAGE_DETAILS_SUCCESS:
      return {
        ...state,
        messageDetails: action.data,
      };
    // </editor-fold>
    default:
      return state;
  }
}
