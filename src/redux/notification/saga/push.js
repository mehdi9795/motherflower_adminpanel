import { call, put, select } from 'redux-saga/effects';
import {
  NOTIFICATION_CREATE_IS_SUCCESS,
  NOTIFICATION_DELETE_IS_SUCCESS,
  NOTIFICATION_EDIT_IS_SUCCESS,
  NOTIFICATION_SEND_IS_SUCCESS,
  TEST_GROUP_CREATE_IS_SUCCESS,
  TEST_GROUP_DELETE_IS_SUCCESS,
  TEST_GROUP_EDIT_IS_SUCCESS,
} from '../../../Resources/Localization';
import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  PUSH_DELETE_FAILURE,
  PUSH_DELETE_SUCCESS,
  PUSH_LIST_FAILURE,
  PUSH_LIST_REQUEST,
  PUSH_LIST_SUCCESS,
  PUSH_POST_FAILURE,
  PUSH_POST_SUCCESS,
  PUSH_PUT_FAILURE,
  PUSH_PUT_SUCCESS,
  PUSH_STATUS_LIST_FAILURE,
  PUSH_STATUS_LIST_SUCCESS,
  PUSH_TEST_GROUP_DELETE_FAILURE,
  PUSH_TEST_GROUP_DELETE_SUCCESS,
  PUSH_TEST_GROUP_LIST_FAILURE,
  PUSH_TEST_GROUP_LIST_REQUEST,
  PUSH_TEST_GROUP_LIST_SUCCESS,
  PUSH_TEST_GROUP_POST_FAILURE,
  PUSH_TEST_GROUP_POST_SUCCESS,
  PUSH_TEST_GROUP_PUT_FAILURE,
  PUSH_TEST_GROUP_PUT_SUCCESS, SEND_PUSH_POST_FAILURE, SEND_PUSH_POST_SUCCESS,
} from '../../../constants';

const getCurrentSession = state => state.login.data;

export function* pushListRequest(getDtoQueryString, getPushApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getPushApi, currentSession.token, container);

  if (response) {
    yield put({ type: PUSH_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PUSH_LIST_FAILURE });
  }
}

export function* pushStatusListRequest(getDtoQueryString, getPushApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getPushApi, currentSession.token, container);

  if (response) {
    yield put({ type: PUSH_STATUS_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PUSH_STATUS_LIST_FAILURE });
  }
}

export function* pushPostRequest(postMessageApi, action) {
  const currentSession = yield select(getCurrentSession);
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const response = yield call(
    postMessageApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto, id: response.data.id };
    }
    yield put({
      type: PUSH_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function pushPostSuccess(history, message, action) {
  if (action.data.data.listDto !== null)
    if (action.data.data.status === 'saveAndContinue')
      history.push(`/push/edit/${action.data.id}`);
    else history.push(`/push/list`);
  message.success(NOTIFICATION_CREATE_IS_SUCCESS, 5);
}

export function* pushPutRequest(putPushApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putPushApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: action.dto.data.dto.id };
    } else {
      data = { data: action.dto, id: action.dto.data.dto.id };
    }
    yield put({
      type: PUSH_PUT_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function pushPutSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue')
    history.push(`/push/edit/${action.data.id}`);
  else history.push(`/push/list`);
  message.success(NOTIFICATION_EDIT_IS_SUCCESS, 5);
}

export function* pushDeleteRequest(deletePushApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deletePushApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: PUSH_DELETE_SUCCESS, data: action.dto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_DELETE_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* pushDeleteSuccess(history, message, action) {
  if (action.data.from === 'list')
    yield put({ type: PUSH_LIST_REQUEST, dto: action.data.listDto });
  else history.push(`/push/list`);
  message.success(NOTIFICATION_DELETE_IS_SUCCESS, 5);
}

export function* pushTestGroupListRequest(
  getDtoQueryString,
  getPushTestGroupApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getPushTestGroupApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: PUSH_TEST_GROUP_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PUSH_TEST_GROUP_LIST_FAILURE });
  }
}

export function* pushTestGroupPostRequest(postPushTestGroupApi, action) {
  const currentSession = yield select(getCurrentSession);
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const response = yield call(
    postPushTestGroupApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_TEST_GROUP_POST_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_TEST_GROUP_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* pushTestGroupPostSuccess(history, message, action) {
  yield put({ type: PUSH_TEST_GROUP_LIST_REQUEST, dto: action.data });
  message.success(TEST_GROUP_CREATE_IS_SUCCESS, 5);
}

export function* pushTestGroupPutRequest(putPushTestGroupApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    putPushTestGroupApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_TEST_GROUP_PUT_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_TEST_GROUP_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function pushTestGroupPutSuccess(history, message, action) {
  message.success(TEST_GROUP_EDIT_IS_SUCCESS, 5);
}

export function* pushTestGroupDeleteRequest(deletePushTestGroupApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deletePushTestGroupApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_TEST_GROUP_DELETE_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUSH_TEST_GROUP_DELETE_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* pushTestGroupDeleteSuccess(history, message, action) {
  yield put({ type: PUSH_TEST_GROUP_LIST_REQUEST, dto: action.data });
  message.success(TEST_GROUP_DELETE_IS_SUCCESS, 5);
}

export function* sendPushPostRequest(postSendPusApi, message, action) {
  const currentSession = yield select(getCurrentSession);
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const response = yield call(postSendPusApi, currentSession.token, action.dto);
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SEND_PUSH_POST_SUCCESS,
    });
    message.success(NOTIFICATION_SEND_IS_SUCCESS, 5);
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SEND_PUSH_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
