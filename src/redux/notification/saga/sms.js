import { call, put, select } from 'redux-saga/effects';
import {
  SMS_POST_SUCCESS,
  SMS_POST_FAILURE,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  SMS_DELETE_FAILURE,
  SMS_DELETE_SUCCESS,
  SMS_LIST_FAILURE,
  SMS_LIST_REQUEST,
  SMS_LIST_SUCCESS,
  SMS_PUT_FAILURE,
  SMS_PUT_SUCCESS,
  SINGLE_SMS_FAILURE,
  SINGLE_SMS_SUCCESS,
  SMS_STATUS_LIST_FAILURE,
  SMS_STATUS_LIST_SUCCESS,
} from '../../../constants/index';
import {
  SMS_UPDATE_IS_SUCCESS,
  SMS_CREATE_IS_SUCCESS,
  SMS_DELETE_IS_SUCCESS,
  SMS_SEND_IS_SUCCESS,
} from '../../../Resources/Localization';
import { SMS_SEND_FAILURE, SMS_SEND_SUCCESS } from '../../../constants';

const getCurrentSession = state => state.login.data;

export function* smsListRequest(getDtoQueryString, getSmsApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getSmsApi, currentSession.token, container);

  if (response) {
    yield put({ type: SMS_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: SMS_LIST_FAILURE });
  }
}

export function* singleSmsRequest(getDtoQueryString, getSmsApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getSmsApi, currentSession.token, container);

  if (response) {
    yield put({ type: SINGLE_SMS_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_SMS_FAILURE });
  }
}

export function* smsStatusListRequest(
  getDtoQueryString,
  getSmsStatusApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getSmsStatusApi, currentSession.token, container);

  if (response) {
    yield put({ type: SMS_STATUS_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: SMS_STATUS_LIST_FAILURE });
  }
}

export function* smsPostRequest(postSmsApi, action) {
  const currentSession = yield select(getCurrentSession);
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const response = yield call(
    postSmsApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto, id: response.data.id };
    }
    yield put({
      type: SMS_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SMS_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function smsPostSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue')
    history.push(`/sms/edit/${action.data.id}`);
  else history.push(`/sms/list`);
  message.success(SMS_CREATE_IS_SUCCESS, 5);
}

export function* smsPutRequest(putSmsApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(putSmsApi, currentSession.token, action.dto.data);
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: action.dto.data.dto.id };
    } else {
      data = { data: action.dto };
    }
    yield put({ type: SMS_PUT_SUCCESS, data });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SMS_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function smsPutSuccess(history, message, action) {
  if (action.data.data.status === 'saveAndContinue')
    history.push(`/sms/edit/${action.data.id}`);
  else history.push(`/sms/list`);

  message.success(SMS_UPDATE_IS_SUCCESS, 5);
}

export function* smsDeleteRequest(deleteSmsApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteSmsApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: SMS_DELETE_SUCCESS, data: action.dto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SMS_DELETE_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* smsDeleteSuccess(history, message, action) {
  if (action.data.from === 'edit') history.push('/sms/list');
  else yield put({ type: SMS_LIST_REQUEST, dto: action.data.listDto });

  message.success(SMS_DELETE_IS_SUCCESS, 5);
}

export function* smsSendRequest(SendSmsApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(SendSmsApi, currentSession.token, action.dto);
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: SMS_SEND_SUCCESS });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: SMS_SEND_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function smsSendSuccess(message) {
  message.success(SMS_SEND_IS_SUCCESS, 5);
}
