import { call, put, select } from 'redux-saga/effects';
import {
  MESSAGE_POST_SUCCESS,
  MESSAGE_POST_FAILURE,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  MESSAGE_DELETE_FAILURE,
  MESSAGE_DELETE_SUCCESS,
  MESSAGE_LIST_FAILURE,
  MESSAGE_LIST_REQUEST,
  MESSAGE_LIST_SUCCESS,
  MESSAGE_PUT_FAILURE,
  MESSAGE_PUT_SUCCESS,
} from '../../../constants/index';
import {
  MESSAGE_CREATE_IS_SUCCESS,
  MESSAGE_DELETE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* messageListRequest(getDtoQueryString, getMessageApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getMessageApi, currentSession.token, container);

  if (response) {
    yield put({ type: MESSAGE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: MESSAGE_LIST_FAILURE });
  }
}

export function* messagePostRequest(postMessageApi, action) {
  const currentSession = yield select(getCurrentSession);
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const response = yield call(
    postMessageApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: MESSAGE_POST_SUCCESS,
      data: action.dto.listDto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: MESSAGE_POST_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* messagePostSuccess(message, action) {
  yield put({
    type: MESSAGE_LIST_REQUEST,
    dto: action.data,
  });
  message.success(MESSAGE_CREATE_IS_SUCCESS, 5);
}

export function* messagePutRequest(putMessageApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(putMessageApi, currentSession.token, action.data);
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: MESSAGE_PUT_SUCCESS });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: MESSAGE_PUT_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}

export function* messageDeleteRequest(deleteMessageApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    deleteMessageApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({ type: MESSAGE_DELETE_SUCCESS, data: action.dto.listDto });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: MESSAGE_DELETE_FAILURE,
      data: response.data.errorMessage,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* messageDeleteSuccess(message, action) {
  yield put({ type: MESSAGE_LIST_REQUEST, dto: action.data });

  message.success(MESSAGE_DELETE_IS_SUCCESS, 5);
}
