import { call, put, select } from 'redux-saga/effects';
import {
  REQUEST_TYPE_LIST_FAILURE,
  REQUEST_TYPE_LIST_SUCCESS,
} from '../../../constants';

const getCurrentSession = state => state.login.data;

export function* requestTypeListRequest(
  getDtoQueryString,
  getRequestTypeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getRequestTypeApi,
    currentSession.token,
    container,
  );

  if (response) {
    yield put({ type: REQUEST_TYPE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: REQUEST_TYPE_LIST_FAILURE });
  }
}
