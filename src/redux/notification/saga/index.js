import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import {
  MESSAGE_LIST_REQUEST,
  MESSAGE_POST_REQUEST,
  MESSAGE_POST_SUCCESS,
  MESSAGE_PUT_REQUEST,
  MESSAGE_DELETE_REQUEST,
  MESSAGE_DELETE_SUCCESS,
  REQUEST_TYPE_LIST_REQUEST,
  PUSH_LIST_REQUEST,
  PUSH_POST_REQUEST,
  PUSH_POST_SUCCESS,
  PUSH_PUT_REQUEST,
  PUSH_DELETE_REQUEST,
  PUSH_DELETE_SUCCESS,
  PUSH_PUT_SUCCESS,
  PUSH_STATUS_LIST_REQUEST,
  PUSH_TEST_GROUP_POST_REQUEST,
  PUSH_TEST_GROUP_POST_SUCCESS,
  PUSH_TEST_GROUP_PUT_REQUEST,
  PUSH_TEST_GROUP_PUT_SUCCESS,
  PUSH_TEST_GROUP_DELETE_REQUEST,
  PUSH_TEST_GROUP_DELETE_SUCCESS,
  PUSH_TEST_GROUP_LIST_REQUEST,
  SEND_PUSH_POST_REQUEST,
  SMS_LIST_REQUEST,
  SMS_POST_REQUEST,
  SMS_POST_SUCCESS,
  SMS_PUT_REQUEST,
  SMS_DELETE_REQUEST,
  SMS_DELETE_SUCCESS, SINGLE_SMS_REQUEST, SMS_STATUS_LIST_REQUEST, SMS_PUT_SUCCESS, SMS_SEND_REQUEST, SMS_SEND_SUCCESS,
} from '../../../constants';
import { getDtoQueryString } from '../../../utils/helper';
import {
  messageDeleteRequest,
  messageDeleteSuccess,
  messageListRequest,
  messagePostRequest,
  messagePostSuccess,
  messagePutRequest,
} from './message';
import {
  DeleteNotificationApi,
  DeletePushApi,
  DeletePushTestGroupApi,
  DeleteSmsBulkApi,
  GetNotificationApi,
  GetPushApi,
  GetPushStatusApi,
  GetPushTestGroupApi,
  GetRequestTypeApi,
  GetSmsBulkApi, GetSmsStatusApi,
  PostNotificationApi,
  PostPushApi,
  PostPushTestGroupApi,
  PostSendPushApi,
  PostSmsBulkApi,
  PutNotificationApi,
  PutPushApi,
  PutPushTestGroupApi,
  PutSmsBulkApi, SendSmsBulkApi,
} from '../../../services/notificationApi';
import { requestTypeListRequest } from './requestType';
import {
  pushDeleteRequest,
  pushDeleteSuccess,
  pushListRequest,
  pushPostRequest,
  pushPostSuccess,
  pushPutRequest,
  pushPutSuccess,
  pushStatusListRequest,
  pushTestGroupDeleteRequest,
  pushTestGroupDeleteSuccess,
  pushTestGroupListRequest,
  pushTestGroupPostRequest,
  pushTestGroupPostSuccess,
  pushTestGroupPutRequest,
  pushTestGroupPutSuccess,
  sendPushPostRequest,
} from './push';
import history from '../../../history';
import {
  singleSmsRequest,
  smsDeleteRequest,
  smsDeleteSuccess,
  smsListRequest,
  smsPostRequest,
  smsPostSuccess,
  smsPutRequest, smsPutSuccess, smsSendRequest, smsSendSuccess, smsStatusListRequest,
} from './sms';

export default function* sagaCommonIndex() {
  yield all([
    // <editor-fold dsc="message">
    takeLatest(
      MESSAGE_LIST_REQUEST,
      messageListRequest,
      getDtoQueryString,
      GetNotificationApi,
    ),
    takeLatest(MESSAGE_POST_REQUEST, messagePostRequest, PostNotificationApi),
    takeLatest(MESSAGE_POST_SUCCESS, messagePostSuccess, message),
    takeLatest(MESSAGE_PUT_REQUEST, messagePutRequest, PutNotificationApi),
    takeLatest(
      MESSAGE_DELETE_REQUEST,
      messageDeleteRequest,
      DeleteNotificationApi,
    ),
    takeLatest(MESSAGE_DELETE_SUCCESS, messageDeleteSuccess, message),
    // </editor-fold>

    // <editor-fold dsc="push">
    takeLatest(
      PUSH_LIST_REQUEST,
      pushListRequest,
      getDtoQueryString,
      GetPushApi,
    ),
    takeLatest(
      SEND_PUSH_POST_REQUEST,
      sendPushPostRequest,
      PostSendPushApi,
      message,
    ),
    takeLatest(PUSH_POST_REQUEST, pushPostRequest, PostPushApi),
    takeLatest(PUSH_POST_SUCCESS, pushPostSuccess, history, message),
    takeLatest(PUSH_PUT_REQUEST, pushPutRequest, PutPushApi),
    takeLatest(PUSH_PUT_SUCCESS, pushPutSuccess, history, message),
    takeLatest(PUSH_DELETE_REQUEST, pushDeleteRequest, DeletePushApi),
    takeLatest(PUSH_DELETE_SUCCESS, pushDeleteSuccess, history, message),
    takeLatest(
      PUSH_STATUS_LIST_REQUEST,
      pushStatusListRequest,
      getDtoQueryString,
      GetPushStatusApi,
    ),

    takeLatest(
      PUSH_TEST_GROUP_LIST_REQUEST,
      pushTestGroupListRequest,
      getDtoQueryString,
      GetPushTestGroupApi,
    ),
    takeLatest(
      PUSH_TEST_GROUP_POST_REQUEST,
      pushTestGroupPostRequest,
      PostPushTestGroupApi,
    ),
    takeLatest(
      PUSH_TEST_GROUP_POST_SUCCESS,
      pushTestGroupPostSuccess,
      history,
      message,
    ),
    takeLatest(
      PUSH_TEST_GROUP_PUT_REQUEST,
      pushTestGroupPutRequest,
      PutPushTestGroupApi,
    ),
    takeLatest(
      PUSH_TEST_GROUP_PUT_SUCCESS,
      pushTestGroupPutSuccess,
      history,
      message,
    ),
    takeLatest(
      PUSH_TEST_GROUP_DELETE_REQUEST,
      pushTestGroupDeleteRequest,
      DeletePushTestGroupApi,
    ),
    takeLatest(
      PUSH_TEST_GROUP_DELETE_SUCCESS,
      pushTestGroupDeleteSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="request type">
    takeLatest(
      REQUEST_TYPE_LIST_REQUEST,
      requestTypeListRequest,
      getDtoQueryString,
      GetRequestTypeApi,
    ),
    // </editor-fold>

    // <editor-fold dsc="sms">
    takeLatest(
      SMS_LIST_REQUEST,
      smsListRequest,
      getDtoQueryString,
      GetSmsBulkApi,
    ),
    takeLatest(
      SINGLE_SMS_REQUEST,
      singleSmsRequest,
      getDtoQueryString,
      GetSmsBulkApi,
    ),
    takeLatest(
      SMS_STATUS_LIST_REQUEST,
      smsStatusListRequest,
      getDtoQueryString,
      GetSmsStatusApi,
    ),
    takeLatest(SMS_POST_REQUEST, smsPostRequest, PostSmsBulkApi),
    takeLatest(SMS_POST_SUCCESS, smsPostSuccess, history, message),
    takeLatest(SMS_PUT_SUCCESS, smsPutSuccess, history, message),
    takeLatest(SMS_PUT_REQUEST, smsPutRequest, PutSmsBulkApi),
    takeLatest(SMS_DELETE_REQUEST, smsDeleteRequest, DeleteSmsBulkApi),
    takeLatest(SMS_DELETE_SUCCESS, smsDeleteSuccess, history, message),
    takeLatest(SMS_SEND_REQUEST, smsSendRequest, SendSmsBulkApi),
    takeLatest(SMS_SEND_SUCCESS, smsSendSuccess, message),
    // </editor-fold>
  ]);
}
