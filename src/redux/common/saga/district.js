import { call, put, select } from 'redux-saga/effects';
import {
  DISTRICT_OPTION_LIST_SUCCESS,
  DISTRICT_OPTION_LIST_FAILURE,
  DISTRICT_ZONE_LIST_SUCCESS,
  DISTRICT_ZONE_LIST_FAILURE,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  DISTRICT_ZONE_DELETE_SUCCESS,
  DISTRICT_ZONE_DELETE_FAILURE,
  DISTRICT_ZONE_POST_SUCCESS,
  DISTRICT_ZONE_POST_FAILURE,
  DISTRICT_PUT_SUCCESS,
  DISTRICT_POST_SUCCESS,
  DISTRICT_POST_FAILURE,
  DISTRICT_PUT_FAILURE,
  DISTRICT_DELETE_SUCCESS,
  DISTRICT_DELETE_FAILURE,
  DISTRICT_OPTION_LIST_REQUEST,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_SUCCESS,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_FAILURE,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST,
  DISTRICT_ZONE_LIST_REQUEST,
} from '../../../constants';
import {
  DISTRICT_CREATE_IS_SUCCESS,
  DISTRICT_DELETE_IS_SUCCESS,
  DISTRICT_UPDATE_IS_SUCCESS,
  DISTRICT_ZONE_CREATE_IS_SUCCESS,
  DISTRICT_ZONE_DELETE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

// eslint-disable-next-line import/prefer-default-export
export function* districtOptionListRequest(
  getDtoQueryString,
  getDistrictApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getDistrictApi, currentSession.token, container);
  if (response) {
    yield put({ type: DISTRICT_OPTION_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: DISTRICT_OPTION_LIST_FAILURE });
  }
}

export function* districtZoneListRequest(
  getDtoQueryString,
  getDistrictZoneApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);

  const response = yield call(
    getDistrictZoneApi,
    currentSession.token,
    container,
  );
  if (response.status === 200) {
    yield put({ type: DISTRICT_ZONE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: DISTRICT_ZONE_LIST_FAILURE });
  }
}

export function* districtZoneNotContainListRequest(
  getDtoQueryString,
  getDistrictApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);

  const response = yield call(getDistrictApi, currentSession.token, container);
  if (response) {
    yield put({
      type: DISTRICT_ZONE_NOT_CONTAIN_LIST_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: DISTRICT_ZONE_NOT_CONTAIN_LIST_FAILURE });
  }
}

export function* districtPostRequest(postDistrictApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    postDistrictApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* districtPostSuccess(history, message, action) {
  message.success(DISTRICT_CREATE_IS_SUCCESS, 5);
  yield put({
    type: DISTRICT_OPTION_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* districtPutRequest(putDistrictApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    putDistrictApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* districtPutSuccess(history, message, action) {
  message.success(DISTRICT_UPDATE_IS_SUCCESS, 5);
  yield put({
    type: DISTRICT_OPTION_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* districtDeleteRequest(deleteDistrictApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteDistrictApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* districtDeleteSuccess(history, message, action) {
  message.success(DISTRICT_DELETE_IS_SUCCESS, 5);
  yield put({
    type: DISTRICT_OPTION_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* districtZonePostRequest(postDistrictZoneApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    postDistrictZoneApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_ZONE_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_ZONE_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* districtZonePostSuccess(history, message, action) {
  message.success(DISTRICT_ZONE_CREATE_IS_SUCCESS, 5);
  yield put({
    type: DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST,
    dto: action.data.districtListDto,
  });
  yield put({
    type: DISTRICT_ZONE_LIST_REQUEST,
    dto: action.data.districtZoneListDto,
  });
}

export function* districtZoneDeleteRequest(deleteDistrictZoneApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });

  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteDistrictZoneApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_ZONE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_ZONE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* districtZoneDeleteSuccess(history, message, action) {
  message.success(DISTRICT_ZONE_DELETE_IS_SUCCESS, 5);
  yield put({
    type: DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST,
    dto: action.data.districtListDto,
  });
  yield put({
    type: DISTRICT_ZONE_LIST_REQUEST,
    dto: action.data.districtZoneListDto,
  });
}
