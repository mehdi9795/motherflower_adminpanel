import { call, put, select } from 'redux-saga/effects';
import {
  OCCASION_TYPE_LIST_SUCCESS,
  OCCASION_TYPE_LIST_FAILURE,
} from '../../../constants';

const getCurrentSession = state => state.login.data;

export function* occasionTypeListRequest(
  getDtoQueryString,
  getOccasionTypeApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getOccasionTypeApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: OCCASION_TYPE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: OCCASION_TYPE_LIST_FAILURE });
  }
}
