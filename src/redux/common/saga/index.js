import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import { getDtoQueryString } from '../../../utils/helper';
import {
  CITY_LIST_REQUEST,
  DISTRICT_DELETE_REQUEST,
  DISTRICT_DELETE_SUCCESS,
  DISTRICT_OPTION_LIST_REQUEST,
  DISTRICT_POST_REQUEST,
  DISTRICT_POST_SUCCESS,
  DISTRICT_PUT_REQUEST,
  DISTRICT_PUT_SUCCESS,
  DISTRICT_ZONE_DELETE_REQUEST,
  DISTRICT_ZONE_DELETE_SUCCESS,
  DISTRICT_ZONE_LIST_REQUEST,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST,
  DISTRICT_ZONE_POST_REQUEST,
  DISTRICT_ZONE_POST_SUCCESS, OCCASION_TYPE_LIST_REQUEST,
  PUBLIC_EVENT_DELETE_REQUEST,
  PUBLIC_EVENT_DELETE_SUCCESS,
  PUBLIC_EVENT_LIST_REQUEST,
  PUBLIC_EVENT_POST_REQUEST,
  PUBLIC_EVENT_POST_SUCCESS,
  PUBLIC_EVENT_PUT_REQUEST,
  PUBLIC_EVENT_PUT_SUCCESS,
  REPORT_LIST_REQUEST,
  REPORT_POST_REQUEST,
  REPORT_POST_SUCCESS,
  SINGLE_CITY_REQUEST,
  SINGLE_PUBLIC_EVENT_REQUEST,
  SINGLE_ZONE_REQUEST,
  ZONE_DELETE_REQUEST,
  ZONE_DELETE_SUCCESS,
  ZONE_LIST_REQUEST,
  ZONE_POST_REQUEST,
  ZONE_POST_SUCCESS,
  ZONE_PUT_REQUEST,
  ZONE_PUT_SUCCESS,
} from '../../../constants';
import {
  districtDeleteRequest,
  districtDeleteSuccess,
  districtOptionListRequest,
  districtPostRequest,
  districtPostSuccess,
  districtPutRequest,
  districtPutSuccess,
  districtZoneDeleteRequest,
  districtZoneDeleteSuccess,
  districtZoneListRequest,
  districtZoneNotContainListRequest,
  districtZonePostRequest,
  districtZonePostSuccess,
} from './district';
import { cityListRequest, singleCityRequest } from './city';
import {
  deleteDistrictApi,
  deleteDistrictZoneApi,
  deletePublicEventApi,
  deleteZoneApi,
  getCityApi,
  getDistrictApi,
  getDistrictZoneApi, getOccasionTypeApi,
  getPublicEventApi,
  getReportApi,
  getZoneApi,
  postDistrictApi,
  postDistrictZoneApi,
  postPublicEventApi,
  postReportApi,
  postZoneApi,
  putDistrictApi,
  putPublicEventApi,
  putZoneApi,
} from '../../../services/commonApi';
import history from '../../../history';
import {
  singleZoneRequest,
  zoneDeleteRequest,
  zoneDeleteSuccess,
  zoneListRequest,
  zonePostRequest,
  zonePostSuccess,
  zonePutRequest,
  zonePutSuccess,
} from './zone';
import {
  publicEventDeleteRequest,
  publicEventDeleteSuccess,
  publicEventListRequest,
  publicEventPostRequest,
  publicEventPostSuccess,
  publicEventPutRequest,
  publicEventPutSuccess,
  singlePublicEventRequest,
} from './publicEvent';
import {
  reportListRequest,
  reportPostRequest,
  reportPostSuccess,
} from './report';
import {occasionTypeListRequest} from "./occasionType";

export default function* sagaCommonIndex() {
  // <editor-fold dsc="city">
  yield all([
    takeLatest(
      CITY_LIST_REQUEST,
      cityListRequest,
      getDtoQueryString,
      getCityApi,
    ),
    takeLatest(
      SINGLE_CITY_REQUEST,
      singleCityRequest,
      getDtoQueryString,
      getCityApi,
    ),
    // </editor-fold>

    // <editor-fold dsc="District">
    takeLatest(
      DISTRICT_OPTION_LIST_REQUEST,
      districtOptionListRequest,
      getDtoQueryString,
      getDistrictApi,
    ),
    takeLatest(
      DISTRICT_ZONE_LIST_REQUEST,
      districtZoneListRequest,
      getDtoQueryString,
      getDistrictZoneApi,
    ),

    takeLatest(
      DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST,
      districtZoneNotContainListRequest,
      getDtoQueryString,
      getDistrictApi,
    ),

    takeLatest(DISTRICT_POST_REQUEST, districtPostRequest, postDistrictApi),
    takeLatest(DISTRICT_POST_SUCCESS, districtPostSuccess, history, message),
    takeLatest(DISTRICT_PUT_REQUEST, districtPutRequest, putDistrictApi),
    takeLatest(DISTRICT_PUT_SUCCESS, districtPutSuccess, history, message),
    takeLatest(
      DISTRICT_DELETE_REQUEST,
      districtDeleteRequest,
      deleteDistrictApi,
    ),
    takeLatest(
      DISTRICT_DELETE_SUCCESS,
      districtDeleteSuccess,
      history,
      message,
    ),

    takeLatest(
      DISTRICT_ZONE_POST_REQUEST,
      districtZonePostRequest,
      postDistrictZoneApi,
    ),
    takeLatest(
      DISTRICT_ZONE_POST_SUCCESS,
      districtZonePostSuccess,
      history,
      message,
    ),
    takeLatest(
      DISTRICT_ZONE_DELETE_REQUEST,
      districtZoneDeleteRequest,
      deleteDistrictZoneApi,
    ),
    takeLatest(
      DISTRICT_ZONE_DELETE_SUCCESS,
      districtZoneDeleteSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="zone">
    takeLatest(
      ZONE_LIST_REQUEST,
      zoneListRequest,
      getDtoQueryString,
      getZoneApi,
    ),
    takeLatest(
      SINGLE_ZONE_REQUEST,
      singleZoneRequest,
      getDtoQueryString,
      getZoneApi,
    ),
    takeLatest(ZONE_POST_REQUEST, zonePostRequest, postZoneApi),
    takeLatest(ZONE_POST_SUCCESS, zonePostSuccess, history, message),
    takeLatest(ZONE_PUT_REQUEST, zonePutRequest, putZoneApi),
    takeLatest(ZONE_PUT_SUCCESS, zonePutSuccess, history, message),
    takeLatest(ZONE_DELETE_REQUEST, zoneDeleteRequest, deleteZoneApi),
    takeLatest(ZONE_DELETE_SUCCESS, zoneDeleteSuccess, history, message),
    // </editor-fold>

    // <editor-fold dsc="public event">
    takeLatest(
      PUBLIC_EVENT_LIST_REQUEST,
      publicEventListRequest,
      getDtoQueryString,
      getPublicEventApi,
    ),
    takeLatest(
      SINGLE_PUBLIC_EVENT_REQUEST,
      singlePublicEventRequest,
      getDtoQueryString,
      getPublicEventApi,
    ),
    takeLatest(
      PUBLIC_EVENT_POST_REQUEST,
      publicEventPostRequest,
      postPublicEventApi,
    ),
    takeLatest(
      PUBLIC_EVENT_POST_SUCCESS,
      publicEventPostSuccess,
      history,
      message,
    ),
    takeLatest(
      PUBLIC_EVENT_PUT_REQUEST,
      publicEventPutRequest,
      putPublicEventApi,
    ),
    takeLatest(
      PUBLIC_EVENT_PUT_SUCCESS,
      publicEventPutSuccess,
      history,
      message,
    ),
    takeLatest(
      PUBLIC_EVENT_DELETE_REQUEST,
      publicEventDeleteRequest,
      deletePublicEventApi,
    ),
    takeLatest(
      PUBLIC_EVENT_DELETE_SUCCESS,
      publicEventDeleteSuccess,
      history,
      message,
    ),
    // </editor-fold>

    // <editor-fold dsc="report">
    takeLatest(
      REPORT_LIST_REQUEST,
      reportListRequest,
      getDtoQueryString,
      getReportApi,
    ),
    takeLatest(REPORT_POST_REQUEST, reportPostRequest, postReportApi),

    takeLatest(REPORT_POST_SUCCESS, reportPostSuccess, history, message),
    // </editor-fold>

    // <editor-fold dsc="occasion type">
    takeLatest(
      OCCASION_TYPE_LIST_REQUEST,
      occasionTypeListRequest,
      getDtoQueryString,
      getOccasionTypeApi,
    ),
    // </editor-fold>
  ]);
}
