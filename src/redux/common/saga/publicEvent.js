import { call, put, select } from 'redux-saga/effects';
import {
  PUBLIC_EVENT_LIST_SUCCESS,
  PUBLIC_EVENT_LIST_FAILURE,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  PUBLIC_EVENT_LIST_REQUEST,
  PUBLIC_EVENT_DELETE_FAILURE,
  PUBLIC_EVENT_POST_FAILURE,
  PUBLIC_EVENT_POST_SUCCESS,
  PUBLIC_EVENT_PUT_FAILURE,
  SINGLE_PUBLIC_EVENT_FAILURE,
  SINGLE_PUBLIC_EVENT_SUCCESS,
  PUBLIC_EVENT_PUT_SUCCESS,
} from '../../../constants';
import {
  PUBLIC_EVENT_CREATE_IS_SUCCESS,
  PUBLIC_EVENT_DELETE_SUCCESS,
  PUBLIC_EVENT_UPDATE_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* publicEventListRequest(
  getDtoQueryString,
  getPublicEventApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getPublicEventApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: PUBLIC_EVENT_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: PUBLIC_EVENT_LIST_FAILURE });
  }
}

export function* singlePublicEventRequest(
  getDtoQueryString,
  getPublicEventApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(
    getPublicEventApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: SINGLE_PUBLIC_EVENT_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_PUBLIC_EVENT_FAILURE });
  }
}

export function* publicEventPostRequest(postPublicEventApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postPublicEventApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    let data = '';
    if (action.dto.status === 'saveAndContinue') {
      data = { data: action.dto, id: response.data.id };
    } else {
      data = { data: action.dto, id: response.data.id };
    }
    yield put({
      type: PUBLIC_EVENT_POST_SUCCESS,
      data,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUBLIC_EVENT_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });

    // message.error(response.data.errorMessage, 30);
  }
}
export function publicEventPostSuccess(history, message, action) {
  message.success(PUBLIC_EVENT_CREATE_IS_SUCCESS, 10);
  if (action.data.data.status === 'saveAndContinue') {
    history.push(`/public-event/edit/${action.data.id}`);
  } else {
    history.push('/public-event/list');
  }
}
export function* publicEventPutRequest(putPublicEventApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    putPublicEventApi,
    currentSession.token,
    action.dto.data,
  );

  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUBLIC_EVENT_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUBLIC_EVENT_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function publicEventPutSuccess(history, message, action) {
  if (action.data.status === 'saveAndContinue') {
    message.success(PUBLIC_EVENT_UPDATE_IS_SUCCESS, 10);
  } else {
    message.success(PUBLIC_EVENT_UPDATE_IS_SUCCESS, 10);
    history.push('/public-event/list');
  }
}

export function* publicEventDeleteRequest(deletePublicEventApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const response = yield call(deletePublicEventApi, action.dto.id);
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUBLIC_EVENT_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: PUBLIC_EVENT_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* publicEventDeleteSuccess(history, message, action) {
  message.success(PUBLIC_EVENT_DELETE_SUCCESS, 10);
  if (action.data.status === 'edit') {
    history.push('/public-event/list');
  } else {
    yield put({
      type: PUBLIC_EVENT_LIST_REQUEST,
      dto: action.data.listDto,
    });
  }
}
