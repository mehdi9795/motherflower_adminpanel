import { call, put, select } from 'redux-saga/effects';
import {
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  DISTRICT_ZONE_DELETE_FAILURE,
  ZONE_LIST_SUCCESS,
  ZONE_LIST_FAILURE,
  SINGLE_ZONE_SUCCESS,
  SINGLE_ZONE_FAILURE,
  ZONE_POST_SUCCESS,
  ZONE_POST_FAILURE,
  ZONE_PUT_SUCCESS,
  ZONE_PUT_FAILURE,
  ZONE_LIST_REQUEST,
  ZONE_DELETE_SUCCESS,
} from '../../../constants';
import {
  ZONE_CREATE_IS_SUCCESS,
  ZONE_DELETE_IS_SUCCESS,
  ZONE_EDIT_IS_SUCCESS,
} from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* zoneListRequest(getDtoQueryString, getZoneApi, action) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);

  const response = yield call(getZoneApi, currentSession.token, container);
  if (response) {
    yield put({ type: ZONE_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: ZONE_LIST_FAILURE });
  }
}

export function* singleZoneRequest(
  getDtoQueryString,
  getSingleZoneZoneApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  const container = getDtoQueryString(action.dto);

  const response = yield call(
    getSingleZoneZoneApi,
    currentSession.token,
    container,
  );
  if (response) {
    yield put({ type: SINGLE_ZONE_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_ZONE_FAILURE });
  }
}

export function* zonePostRequest(postZoneApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    postZoneApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ZONE_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ZONE_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* zonePostSuccess(history, message, action) {
  message.success(ZONE_CREATE_IS_SUCCESS, 5);
  yield put({
    type: ZONE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* zonePutRequest(putZoneApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    putZoneApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ZONE_PUT_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ZONE_PUT_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* zonePutSuccess(history, message, action) {
  message.success(ZONE_EDIT_IS_SUCCESS, 5);
  yield put({
    type: ZONE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}

export function* zoneDeleteRequest(deleteZoneApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);

  const response = yield call(
    deleteZoneApi,
    currentSession.token,
    action.dto.id,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: ZONE_DELETE_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: DISTRICT_ZONE_DELETE_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function* zoneDeleteSuccess(history, message, action) {
  message.success(ZONE_DELETE_IS_SUCCESS, 5);
  yield put({
    type: ZONE_LIST_REQUEST,
    dto: action.data.listDto,
  });
}
