import { call, put, select } from 'redux-saga/effects';
import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
  EDIT_FORM_LOADING_SHOW_REQUEST,
  REPORT_LIST_FAILURE,
  REPORT_LIST_SUCCESS,
  REPORT_POST_FAILURE,
  REPORT_POST_SUCCESS,
} from '../../../constants';
import { REPORT_CREATE_IS_SUCCESS } from '../../../Resources/Localization';

const getCurrentSession = state => state.login.data;

export function* reportListRequest(getDtoQueryString, getReportApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getReportApi, currentSession.token, container);
  if (response) {
    yield put({ type: REPORT_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: REPORT_LIST_FAILURE });
  }
}

export function* reportPostRequest(postReportApi, action) {
  yield put({
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  });
  const currentSession = yield select(getCurrentSession);
  const response = yield call(
    postReportApi,
    currentSession.token,
    action.dto.data,
  );
  if (response.status === 200) {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: REPORT_POST_SUCCESS,
      data: action.dto,
    });
  } else {
    yield put({
      type: EDIT_FORM_LOADING_HIDE_REQUEST,
    });
    yield put({
      type: REPORT_POST_FAILURE,
    });
    yield put({
      type: EDIT_FORM_ALERT_SHOW_REQUEST,
      data: response.data.errorMessage,
    });
  }
}
export function reportPostSuccess(history, message, action) {
  message.success(REPORT_CREATE_IS_SUCCESS, 10);
}
