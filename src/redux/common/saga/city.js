import { call, put, select } from 'redux-saga/effects';
import {
  CITY_LIST_SUCCESS,
  CITY_LIST_FAILURE,
  SINGLE_CITY_SUCCESS,
  SINGLE_CITY_FAILURE,
} from '../../../constants';

const getCurrentSession = state => state.login.data;

export function* cityListRequest(getDtoQueryString, getCityApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getCityApi, currentSession.token, container);
  if (response) {
    yield put({ type: CITY_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: CITY_LIST_FAILURE });
  }
}

export function* singleCityRequest(getDtoQueryString, getCityApi, action) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.dto);
  const response = yield call(getCityApi, currentSession.token, container);
  if (response) {
    yield put({ type: SINGLE_CITY_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_CITY_FAILURE });
  }
}
