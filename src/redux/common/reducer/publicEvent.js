import { getResponseModel } from '../../../utils/model';
import {
  SINGLE_PUBLIC_EVENT_FAILURE,
  SINGLE_PUBLIC_EVENT_INIT_FAILURE,
  SINGLE_PUBLIC_EVENT_INIT_REQUEST,
  SINGLE_PUBLIC_EVENT_INIT_SUCCESS,
  SINGLE_PUBLIC_EVENT_REQUEST,
  SINGLE_PUBLIC_EVENT_SUCCESS,
  PUBLIC_EVENT_POST_REQUEST,
  PUBLIC_EVENT_POST_SUCCESS,
  PUBLIC_EVENT_POST_FAILURE,
  PUBLIC_EVENT_PUT_REQUEST,
  PUBLIC_EVENT_PUT_SUCCESS,
  PUBLIC_EVENT_PUT_FAILURE,
  PUBLIC_EVENT_DELETE_REQUEST,
  PUBLIC_EVENT_DELETE_SUCCESS,
  PUBLIC_EVENT_DELETE_FAILURE,
  PUBLIC_EVENT_LIST_REQUEST,
  PUBLIC_EVENT_LIST_SUCCESS,
  PUBLIC_EVENT_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  publicEventListRequest: false,
  publicEventListData: getResponseModel,
  publicEventListError: false,

  singlePublicEventRequest: false,
  singlePublicEventData: getResponseModel,
  singlePublicEventError: false,

  singlePublicEventInitRequest: false,
  singlePublicEventInitError: false,

  publicEventPostRequest: false,
  publicEventPostData: getResponseModel,

  publicEventPutRequest: false,
  publicEventPutData: getResponseModel,

  publicEventDeleteRequest: false,
  publicEventDeleteData: getResponseModel,

  publicEventCRUDError: false,
  publicEventErrorMessage: '',
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="publicEvent List">
    case PUBLIC_EVENT_LIST_REQUEST:
      return {
        ...state,
        publicEventListRequest: true,
      };
    case PUBLIC_EVENT_LIST_SUCCESS:
      return {
        ...state,
        publicEventListRequest: false,
        publicEventListData: action.data,
      };
    case PUBLIC_EVENT_LIST_FAILURE:
      return {
        ...state,
        publicEventListRequest: false,
        publicEventListError: true,
      };
    // </editor-fold>

    // <editor-fold desc="single publicEvent List">
    case SINGLE_PUBLIC_EVENT_REQUEST:
      return {
        ...state,
        singlePublicEventRequest: true,
      };
    case SINGLE_PUBLIC_EVENT_SUCCESS:
      return {
        ...state,
        singlePublicEventRequest: false,
        singlePublicEventData: action.data,
      };
    case SINGLE_PUBLIC_EVENT_FAILURE:
      return {
        ...state,
        singlePublicEventRequest: false,
        singlePublicEventError: true,
      };
    // </editor-fold>

    // <editor-fold dsc="Single PublicEvent init">
    case SINGLE_PUBLIC_EVENT_INIT_REQUEST:
      return {
        ...state,
        singlePublicEventInitRequest: true,
      };
    case SINGLE_PUBLIC_EVENT_INIT_SUCCESS:
      return {
        ...state,
        singlePublicEventInitRequest: false,
      };
    case SINGLE_PUBLIC_EVENT_INIT_FAILURE:
      return {
        ...state,
        singlePublicEventInitRequest: false,
        singlePublicEventInitError: true,
      };
    // </editor-fold>

    // <editor-fold dsc=" PublicEvent post">
    case PUBLIC_EVENT_POST_REQUEST:
      return {
        ...state,
        publicEventPostRequest: true,
        publicEventCRUDError: false,
      };
    case PUBLIC_EVENT_POST_SUCCESS:
      return {
        ...state,
        publicEventPostData: action.data,
        publicEventPostRequest: false,
        publicEventCRUDError: false,
      };
    case PUBLIC_EVENT_POST_FAILURE:
      return {
        ...state,
        publicEventCRUDError: true,
        publicEventPostRequest: false,
        publicEventErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc=" PublicEvent put">
    case PUBLIC_EVENT_PUT_REQUEST:
      return {
        ...state,
        publicEventPutRequest: true,
        publicEventCRUDError: false,
      };
    case PUBLIC_EVENT_PUT_SUCCESS:
      return {
        ...state,
        publicEventPutData: action.data,
        publicEventPutRequest: false,
        publicEventCRUDError: false,
      };
    case PUBLIC_EVENT_PUT_FAILURE:
      return {
        ...state,
        publicEventCRUDError: true,
        publicEventPutRequest: false,
        publicEventErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc=" PublicEvent delete">
    case PUBLIC_EVENT_DELETE_REQUEST:
      return {
        ...state,
        districtPublicEventDeleteRequest: true,
        districtCRUDError: false,
      };
    case PUBLIC_EVENT_DELETE_SUCCESS:
      return {
        ...state,
        publicEventDeleteData: action.data,
        publicEventDeleteRequest: false,
        publicEventCRUDError: false,
      };
    case PUBLIC_EVENT_DELETE_FAILURE:
      return {
        ...state,
        publicEventErrorMessage: action.data,
        publicEventCRUDError: true,
        publicEventDeleteRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
