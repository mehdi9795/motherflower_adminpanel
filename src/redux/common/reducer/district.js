import {
  DISTRICT_OPTION_LIST_REQUEST,
  DISTRICT_OPTION_LIST_SUCCESS,
  DISTRICT_OPTION_LIST_FAILURE,
  DISTRICT_ZONE_LIST_FAILURE,
  DISTRICT_ZONE_LIST_REQUEST,
  DISTRICT_ZONE_LIST_SUCCESS,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_SUCCESS,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_FAILURE,
  DISTRICT_ZONE_DELETE_REQUEST,
  DISTRICT_ZONE_POST_SUCCESS,
  DISTRICT_ZONE_DELETE_SUCCESS,
  DISTRICT_ZONE_POST_FAILURE,
  DISTRICT_ZONE_POST_REQUEST,
  DISTRICT_ZONE_DELETE_FAILURE,
  DISTRICT_PUT_REQUEST,
  DISTRICT_PUT_SUCCESS,
  DISTRICT_PUT_FAILURE,
  DISTRICT_POST_REQUEST,
  DISTRICT_POST_SUCCESS,
  DISTRICT_POST_FAILURE,
  DISTRICT_DELETE_REQUEST,
  DISTRICT_DELETE_SUCCESS,
  DISTRICT_DELETE_FAILURE,
} from '../../../constants';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  districtOptionRequest: false,
  districtOptionData: getResponseModel,
  districtOptionError: false,

  districtZoneListRequest: false,
  districtZoneListData: getResponseModel,
  districtZoneListError: false,

  districtZoneNotContainListRequest: false,
  districtZoneNotContainListData: getResponseModel,
  districtZoneNotContainListError: false,

  districtPostRequest: false,
  districtPostData: getResponseModel,

  districtPutRequest: false,
  districtPutData: getResponseModel,

  districtDeleteRequest: false,
  districtDeleteData: getResponseModel,

  districtZonePostRequest: false,
  districtZonePostData: getResponseModel,

  districtZoneDeleteRequest: false,
  districtZoneDeleteData: getResponseModel,

  districtCRUDError: false,
  districtErrorMessage: '',
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="District List">
    case DISTRICT_OPTION_LIST_REQUEST:
      return {
        ...state,
        districtOptionRequest: true,
      };
    case DISTRICT_OPTION_LIST_SUCCESS:
      return {
        ...state,
        districtOptionRequest: false,
        districtOptionData: action.data,
      };
    case DISTRICT_OPTION_LIST_FAILURE:
      return {
        ...state,
        districtOptionRequest: false,
        districtOptionError: true,
      };
    // </editor-fold>

    // <editor-fold desc="District zone List">
    case DISTRICT_ZONE_LIST_REQUEST:
      return {
        ...state,
        districtZoneListRequest: true,
      };
    case DISTRICT_ZONE_LIST_SUCCESS:
      return {
        ...state,
        districtZoneListRequest: false,
        districtZoneListData: action.data,
      };
    case DISTRICT_ZONE_LIST_FAILURE:
      return {
        ...state,
        districtZoneListRequest: false,
        districtZoneListError: true,
      };
    // </editor-fold>


    // <editor-fold desc="District zone not contain List">
    case DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST:
      return {
        ...state,
        districtZoneNotContainListRequest: true,
      };
    case DISTRICT_ZONE_NOT_CONTAIN_LIST_SUCCESS:
      return {
        ...state,
        districtZoneNotContainListRequest: false,
        districtZoneNotContainListData: action.data,
      };
    case DISTRICT_ZONE_NOT_CONTAIN_LIST_FAILURE:
      return {
        ...state,
        districtZoneNotContainListRequest: false,
        districtZoneNotContainListError: true,
      };
    // </editor-fold>


    // <editor-fold dsc="district post">
    case DISTRICT_POST_REQUEST:
      return {
        ...state,
        districtPostRequest: true,
        districtCRUDError: false,
      };
    case DISTRICT_POST_SUCCESS:
      return {
        ...state,
        districtPostData: action.data,
        districtPostRequest: false,
        districtCRUDError: false,
      };
    case DISTRICT_POST_FAILURE:
      return {
        ...state,
        districtCRUDError: true,
        districtPostRequest: false,
        districtErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="district put">
    case DISTRICT_PUT_REQUEST:
      return {
        ...state,
        districtPutRequest: true,
        districtCRUDError: false,
      };
    case DISTRICT_PUT_SUCCESS:
      return {
        ...state,
        districtPutData: action.data,
        districtPutRequest: false,
        districtCRUDError: false,
      };
    case DISTRICT_PUT_FAILURE:
      return {
        ...state,
        districtCRUDError: true,
        districtPutRequest: false,
        districtErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="district delete">
    case DISTRICT_DELETE_REQUEST:
      return {
        ...state,
        districtDeleteRequest: true,
        districtCRUDError: false,
      };
    case DISTRICT_DELETE_SUCCESS:
      return {
        ...state,
        districtDeleteData: action.data,
        districtDeleteRequest: false,
        districtCRUDError: false,
      };
    case DISTRICT_DELETE_FAILURE:
      return {
        ...state,
        districtErrorMessage: action.data,
        districtCRUDError: true,
        districtDeleteRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="district Zone post">
    case DISTRICT_ZONE_POST_REQUEST:
      return {
        ...state,
        districtZonePostRequest: true,
        districtCRUDError: false,
      };
    case DISTRICT_ZONE_POST_SUCCESS:
      return {
        ...state,
        districtZonePostData: action.data,
        districtZonePostRequest: false,
        districtCRUDError: false,
      };
    case DISTRICT_ZONE_POST_FAILURE:
      return {
        ...state,
        districtCRUDError: true,
        districtZonePostRequest: false,
        districtErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="district Zone delete">
    case DISTRICT_ZONE_DELETE_REQUEST:
      return {
        ...state,
        districtZoneDeleteRequest: true,
        districtCRUDError: false,
      };
    case DISTRICT_ZONE_DELETE_SUCCESS:
      return {
        ...state,
        districtZoneDeleteData: action.data,
        districtZoneDeleteRequest: false,
        districtCRUDError: false,
      };
    case DISTRICT_ZONE_DELETE_FAILURE:
      return {
        ...state,
        districtErrorMessage: action.data,
        districtCRUDError: true,
        districtZoneDeleteRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
