import {
  CITY_LIST_FAILURE,
  CITY_LIST_SUCCESS,
  CITY_LIST_REQUEST,
  SINGLE_CITY_INIT_SUCCESS,
  SINGLE_CITY_INIT_FAILURE,
  SINGLE_CITY_INIT_REQUEST,
  SINGLE_CITY_FAILURE,
  SINGLE_CITY_SUCCESS,
  SINGLE_CITY_REQUEST,
} from '../../../constants';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  cityListRequest: false,
  cityListData: getResponseModel,
  cityListFailure: false,

  singleCityRequest: false,
  singleCityData: getResponseModel,
  singleCityFailure: false,

  singleCityInitRequest: false,
  singleCityInitData: getResponseModel,
  singleCityInitFailure: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="City List new">
    case CITY_LIST_REQUEST:
      return {
        ...state,
        cityListRequest: true,
        cityListFailure: false,
      };
    case CITY_LIST_SUCCESS:
      return {
        ...state,
        cityListData: action.data,
        cityListRequest: false,
        cityListFailure: false,
      };
    case CITY_LIST_FAILURE:
      return {
        ...state,
        cityListFailure: true,
        cityListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Single City ">
    case SINGLE_CITY_REQUEST:
      return {
        ...state,
        singleCityRequest: true,
      };
    case SINGLE_CITY_SUCCESS:
      return {
        ...state,
        singleCityRequest: false,
        singleCityData: action.data,
      };
    case SINGLE_CITY_FAILURE:
      return {
        ...state,
        singleCityRequest: false,
        singleCityError: true,
      };
    // </editor-fold>

    // <editor-fold dsc="Single City init">
    case SINGLE_CITY_INIT_REQUEST:
      return {
        ...state,
        singleCityInitRequest: true,
      };
    case SINGLE_CITY_INIT_SUCCESS:
      return {
        ...state,
        singleCityInitRequest: false,
      };
    case SINGLE_CITY_INIT_FAILURE:
      return {
        ...state,
        singleCityInitRequest: false,
        singleCityInitError: true,
      };
    // </editor-fold>

    default:
      return state;
  }
}
