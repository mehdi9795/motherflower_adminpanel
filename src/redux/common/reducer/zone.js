import { getResponseModel } from '../../../utils/model';
import {
  DISTRICT_ZONE_DELETE_FAILURE,
  DISTRICT_ZONE_DELETE_REQUEST,
  DISTRICT_ZONE_DELETE_SUCCESS,
  SINGLE_ZONE_INIT_FAILURE,
  SINGLE_ZONE_INIT_REQUEST,
  SINGLE_ZONE_INIT_SUCCESS,
  SINGLE_ZONE_FAILURE,
  SINGLE_ZONE_REQUEST,
  SINGLE_ZONE_SUCCESS,
  ZONE_LIST_FAILURE,
  ZONE_LIST_REQUEST,
  ZONE_LIST_SUCCESS,
  ZONE_POST_FAILURE,
  ZONE_POST_SUCCESS,
  ZONE_POST_REQUEST,
  ZONE_PUT_REQUEST,
  ZONE_PUT_SUCCESS,
  ZONE_PUT_FAILURE,
} from '../../../constants';

const initialState = {
  zoneListRequest: false,
  zoneListData: getResponseModel,
  zoneListError: false,

  singleZoneRequest: false,
  singleZoneData: getResponseModel,
  singleZoneError: false,

  singleZoneInitRequest: false,
  singleZoneInitError: false,

  zonePostRequest: false,
  zonePostData: getResponseModel,

  zonePutRequest: false,
  zonePutData: getResponseModel,

  zoneDeleteRequest: false,
  zoneDeleteData: getResponseModel,

  zoneCRUDError: false,
  zoneErrorMessage: '',
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="zone List">
    case ZONE_LIST_REQUEST:
      return {
        ...state,
        zoneListRequest: true,
      };
    case ZONE_LIST_SUCCESS:
      return {
        ...state,
        zoneListRequest: false,
        zoneListData: action.data,
      };
    case ZONE_LIST_FAILURE:
      return {
        ...state,
        zoneListRequest: false,
        zoneListError: true,
      };
    // </editor-fold>

    // <editor-fold desc="single zone List">
    case SINGLE_ZONE_REQUEST:
      return {
        ...state,
        singleZoneRequest: true,
      };
    case SINGLE_ZONE_SUCCESS:
      return {
        ...state,
        singleZoneRequest: false,
        singleZoneData: action.data,
      };
    case SINGLE_ZONE_FAILURE:
      return {
        ...state,
        singleZoneRequest: false,
        singleZoneError: true,
      };
    // </editor-fold>

    // <editor-fold dsc="Single Zone init">
    case SINGLE_ZONE_INIT_REQUEST:
      return {
        ...state,
        singleZoneInitRequest: true,
      };
    case SINGLE_ZONE_INIT_SUCCESS:
      return {
        ...state,
        singleZoneInitRequest: false,
      };
    case SINGLE_ZONE_INIT_FAILURE:
      return {
        ...state,
        singleZoneInitRequest: false,
        singleZoneInitError: true,
      };
    // </editor-fold>

    // <editor-fold dsc=" Zone post">
    case ZONE_POST_REQUEST:
      return {
        ...state,
        zonePostRequest: true,
        zoneCRUDError: false,
      };
    case ZONE_POST_SUCCESS:
      return {
        ...state,
        zonePostData: action.data,
        zonePostRequest: false,
        zoneCRUDError: false,
      };
    case ZONE_POST_FAILURE:
      return {
        ...state,
        zoneCRUDError: true,
        zonePostRequest: false,
        zoneErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc=" Zone put">
    case ZONE_PUT_REQUEST:
      return {
        ...state,
        zonePutRequest: true,
        zoneCRUDError: false,
      };
    case ZONE_PUT_SUCCESS:
      return {
        ...state,
        zonePutData: action.data,
        zonePutRequest: false,
        zoneCRUDError: false,
      };
    case ZONE_PUT_FAILURE:
      return {
        ...state,
        zoneCRUDError: true,
        zonePutRequest: false,
        zoneErrorMessage: action.data,
      };
    // </editor-fold>

    // <editor-fold dsc="district Zone delete">
    case DISTRICT_ZONE_DELETE_REQUEST:
      return {
        ...state,
        districtZoneDeleteRequest: true,
        districtCRUDError: false,
      };
    case DISTRICT_ZONE_DELETE_SUCCESS:
      return {
        ...state,
        zoneDeleteData: action.data,
        zoneDeleteRequest: false,
        zoneCRUDError: false,
      };
    case DISTRICT_ZONE_DELETE_FAILURE:
      return {
        ...state,
        zoneErrorMessage: action.data,
        zoneCRUDError: true,
        zoneDeleteRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
