import {
  DISTRICT_OPTION_LIST_REQUEST,
  DISTRICT_OPTION_LIST_SUCCESS,
  DISTRICT_OPTION_LIST_FAILURE,
  DISTRICT_ZONE_LIST_FAILURE,
  DISTRICT_ZONE_LIST_REQUEST,
  DISTRICT_ZONE_LIST_SUCCESS,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_SUCCESS,
  DISTRICT_ZONE_NOT_CONTAIN_LIST_FAILURE,
  DISTRICT_ZONE_DELETE_REQUEST,
  DISTRICT_ZONE_POST_SUCCESS,
  DISTRICT_ZONE_DELETE_SUCCESS,
  DISTRICT_ZONE_POST_FAILURE,
  DISTRICT_ZONE_POST_REQUEST,
  DISTRICT_ZONE_DELETE_FAILURE,
  DISTRICT_PUT_REQUEST,
  DISTRICT_PUT_SUCCESS,
  DISTRICT_PUT_FAILURE,
  DISTRICT_POST_REQUEST,
  DISTRICT_POST_SUCCESS,
  DISTRICT_POST_FAILURE,
  DISTRICT_DELETE_REQUEST,
  DISTRICT_DELETE_SUCCESS,
  DISTRICT_DELETE_FAILURE,
} from '../../../constants';

// <editor-fold desc="District option List">
export function districtOptionListRequest(data) {
  return {
    type: DISTRICT_OPTION_LIST_REQUEST,
    data,
  };
}

export function districtOptionListSuccess(data) {
  return {
    type: DISTRICT_OPTION_LIST_SUCCESS,
    data,
  };
}

export function districtOptionListFailure() {
  return {
    type: DISTRICT_OPTION_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="district Post">
export function districtPostRequest(data) {
  return {
    type: DISTRICT_POST_REQUEST,
    data,
  };
}

export function districtPostSuccess() {
  return {
    type: DISTRICT_POST_SUCCESS,
  };
}

export function districtPostFailure() {
  return {
    type: DISTRICT_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="district Put">
export function districtPutRequest(data) {
  return {
    type: DISTRICT_PUT_REQUEST,
    data,
  };
}

export function districtPutSuccess() {
  return {
    type: DISTRICT_PUT_SUCCESS,
  };
}

export function districtPutFailure() {
  return {
    type: DISTRICT_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="district Delete">
export function districtDeleteRequest(data) {
  return {
    type: DISTRICT_DELETE_REQUEST,
    data,
  };
}

export function districtDeleteSuccess() {
  return {
    type: DISTRICT_DELETE_SUCCESS,
  };
}

export function districtDeleteFailure() {
  return {
    type: DISTRICT_DELETE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="District zone list">
export function districtZoneListRequest(data) {
  return {
    type: DISTRICT_ZONE_LIST_REQUEST,
    data,
  };
}

export function districtZoneListSuccess(data) {
  return {
    type: DISTRICT_ZONE_LIST_SUCCESS,
    data,
  };
}

export function districtZoneListFailure() {
  return {
    type: DISTRICT_ZONE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="District zone not contain list">
export function districtZoneNotContainListRequest(data) {
  return {
    type: DISTRICT_ZONE_NOT_CONTAIN_LIST_REQUEST,
    data,
  };
}

export function districtZoneNotContainListSuccess(data) {
  return {
    type: DISTRICT_ZONE_NOT_CONTAIN_LIST_SUCCESS,
    data,
  };
}

export function districtZoneNotContainListFailure() {
  return {
    type: DISTRICT_ZONE_NOT_CONTAIN_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="district Zone Post">
export function districtZonePostRequest(data) {
  return {
    type: DISTRICT_ZONE_POST_REQUEST,
    data,
  };
}

export function districtZonePostSuccess() {
  return {
    type: DISTRICT_ZONE_POST_SUCCESS,
  };
}

export function districtZonePostFailure() {
  return {
    type: DISTRICT_ZONE_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="district zone Delete">
export function districtZoneDeleteRequest(data) {
  return {
    type: DISTRICT_ZONE_DELETE_REQUEST,
    data,
  };
}

export function districtZoneDeleteSuccess() {
  return {
    type: DISTRICT_ZONE_DELETE_SUCCESS,
  };
}

export function districtZoneDeleteFailure() {
  return {
    type: DISTRICT_ZONE_DELETE_FAILURE,
  };
}
// </editor-fold>
