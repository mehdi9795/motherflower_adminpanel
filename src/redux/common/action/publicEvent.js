import {
  SINGLE_PUBLIC_EVENT_FAILURE,
  SINGLE_PUBLIC_EVENT_INIT_FAILURE,
  SINGLE_PUBLIC_EVENT_INIT_REQUEST,
  SINGLE_PUBLIC_EVENT_INIT_SUCCESS,
  SINGLE_PUBLIC_EVENT_REQUEST,
  SINGLE_PUBLIC_EVENT_SUCCESS,
  PUBLIC_EVENT_POST_REQUEST,
  PUBLIC_EVENT_POST_SUCCESS,
  PUBLIC_EVENT_POST_FAILURE,
  PUBLIC_EVENT_PUT_REQUEST,
  PUBLIC_EVENT_PUT_SUCCESS,
  PUBLIC_EVENT_PUT_FAILURE,
  PUBLIC_EVENT_DELETE_REQUEST,
  PUBLIC_EVENT_DELETE_SUCCESS,
  PUBLIC_EVENT_DELETE_FAILURE,
  PUBLIC_EVENT_LIST_REQUEST,
  PUBLIC_EVENT_LIST_SUCCESS,
  PUBLIC_EVENT_LIST_FAILURE,
} from '../../../constants';

// <editor-fold desc="publicEvent List">
export function publicEventListRequest(data) {
  return {
    type: PUBLIC_EVENT_LIST_REQUEST,
    data,
  };
}
export function publicEventListSuccess(data) {
  return {
    type: PUBLIC_EVENT_LIST_SUCCESS,
    data,
  };
}
export function publicEventListFailure() {
  return {
    type: PUBLIC_EVENT_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single PublicEvent">

export function singlePublicEventRequest(data) {
  return {
    type: SINGLE_PUBLIC_EVENT_REQUEST,
    data,
  };
}

export function singlePublicEventSuccess(data) {
  return {
    type: SINGLE_PUBLIC_EVENT_SUCCESS,
    data,
  };
}

export function singlePublicEventFailure() {
  return {
    type: SINGLE_PUBLIC_EVENT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single PublicEvent init">
export function singlePublicEventInitRequest(data) {
  return {
    type: SINGLE_PUBLIC_EVENT_INIT_REQUEST,
    data,
  };
}

export function singlePublicEventInitSuccess(data) {
  return {
    type: SINGLE_PUBLIC_EVENT_INIT_SUCCESS,
    data,
  };
}

export function singlePublicEventInitFailure() {
  return {
    type: SINGLE_PUBLIC_EVENT_INIT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="publicEvent Post">
export function publicEventPostRequest(data) {
  return {
    type: PUBLIC_EVENT_POST_REQUEST,
    data,
  };
}

export function publicEventPostSuccess() {
  return {
    type: PUBLIC_EVENT_POST_SUCCESS,
  };
}

export function publicEventPostFailure() {
  return {
    type: PUBLIC_EVENT_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="publicEvent Put">
export function publicEventPutRequest(data) {
  return {
    type: PUBLIC_EVENT_PUT_REQUEST,
    data,
  };
}

export function publicEventPutSuccess() {
  return {
    type: PUBLIC_EVENT_PUT_SUCCESS,
  };
}

export function publicEventPutFailure() {
  return {
    type: PUBLIC_EVENT_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="PublicEvent Delete">
export function publicEventDeleteRequest(data) {
  return {
    type: PUBLIC_EVENT_DELETE_REQUEST,
    data,
  };
}

export function publicEventDeleteSuccess() {
  return {
    type: PUBLIC_EVENT_DELETE_SUCCESS,
  };
}

export function publicEventDeleteFailure() {
  return {
    type: PUBLIC_EVENT_DELETE_FAILURE,
  };
}
// </editor-fold>
