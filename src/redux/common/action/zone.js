import {
  SINGLE_ZONE_FAILURE,
  SINGLE_ZONE_INIT_FAILURE,
  SINGLE_ZONE_INIT_REQUEST,
  SINGLE_ZONE_INIT_SUCCESS,
  SINGLE_ZONE_REQUEST,
  SINGLE_ZONE_SUCCESS,
  ZONE_POST_REQUEST,
  ZONE_POST_SUCCESS,
  ZONE_POST_FAILURE,
  ZONE_PUT_REQUEST,
  ZONE_PUT_SUCCESS,
  ZONE_PUT_FAILURE,
  ZONE_DELETE_REQUEST,
  ZONE_DELETE_SUCCESS,
  ZONE_DELETE_FAILURE,
  ZONE_LIST_REQUEST,
  ZONE_LIST_SUCCESS,
  ZONE_LIST_FAILURE,
} from '../../../constants';

// <editor-fold desc="zone List">
export function zoneListRequest(data) {
  return {
    type: ZONE_LIST_REQUEST,
    data,
  };
}
export function zoneListSuccess(data) {
  return {
    type: ZONE_LIST_SUCCESS,
    data,
  };
}
export function zoneListFailure() {
  return {
    type: ZONE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single Zone">

export function singleZoneRequest(data) {
  return {
    type: SINGLE_ZONE_REQUEST,
    data,
  };
}

export function singleZoneSuccess(data) {
  return {
    type: SINGLE_ZONE_SUCCESS,
    data,
  };
}

export function singleZoneFailure() {
  return {
    type: SINGLE_ZONE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single Zone init">
export function singleZoneInitRequest(data) {
  return {
    type: SINGLE_ZONE_INIT_REQUEST,
    data,
  };
}

export function singleZoneInitSuccess(data) {
  return {
    type: SINGLE_ZONE_INIT_SUCCESS,
    data,
  };
}

export function singleZoneInitFailure() {
  return {
    type: SINGLE_ZONE_INIT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="zone Post">
export function zonePostRequest(data) {
  return {
    type: ZONE_POST_REQUEST,
    data,
  };
}

export function zonePostSuccess() {
  return {
    type: ZONE_POST_SUCCESS,
  };
}

export function zonePostFailure() {
  return {
    type: ZONE_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="zone Put">
export function zonePutRequest(data) {
  return {
    type: ZONE_PUT_REQUEST,
    data,
  };
}

export function zonePutSuccess() {
  return {
    type: ZONE_PUT_SUCCESS,
  };
}

export function zonePutFailure() {
  return {
    type: ZONE_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Zone Delete">
export function zoneDeleteRequest(data) {
  return {
    type: ZONE_DELETE_REQUEST,
    data,
  };
}

export function zoneDeleteSuccess() {
  return {
    type: ZONE_DELETE_SUCCESS,
  };
}

export function zoneDeleteFailure() {
  return {
    type: ZONE_DELETE_FAILURE,
  };
}
// </editor-fold>
