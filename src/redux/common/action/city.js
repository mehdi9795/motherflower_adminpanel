import {
  CITY_LIST_REQUEST,
  CITY_LIST_SUCCESS,
  CITY_LIST_FAILURE,
  SINGLE_CITY_INIT_SUCCESS,
  SINGLE_CITY_INIT_FAILURE,
  SINGLE_CITY_INIT_REQUEST,
  SINGLE_CITY_FAILURE,
  SINGLE_CITY_SUCCESS,
  SINGLE_CITY_REQUEST,
} from '../../../constants';

// <editor-fold desc="city List">
export function cityListRequest(data) {
  return {
    type: CITY_LIST_REQUEST,
    data,
  };
}
export function cityListSuccess(data) {
  return {
    type: CITY_LIST_SUCCESS,
    data,
  };
}
export function cityListFailure() {
  return {
    type: CITY_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single City">
export function singleCityRequest(data) {
  return {
    type: SINGLE_CITY_REQUEST,
    data,
  };
}

export function singleCitySuccess(data) {
  return {
    type: SINGLE_CITY_SUCCESS,
    data,
  };
}

export function singleCityFailure() {
  return {
    type: SINGLE_CITY_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="single City init">
export function singleCityInitRequest(data) {
  return {
    type: SINGLE_CITY_INIT_REQUEST,
    data,
  };
}

export function singleCityInitSuccess(data) {
  return {
    type: SINGLE_CITY_INIT_SUCCESS,
    data,
  };
}

export function singleCityInitFailure() {
  return {
    type: SINGLE_CITY_INIT_FAILURE,
  };
}
// </editor-fold>
