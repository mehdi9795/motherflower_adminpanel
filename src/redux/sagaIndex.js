import { fork } from 'redux-saga/effects';
import sagaCatalogIndex from './catalog/saga/index';
import sagaCommonIndex from './common/saga';
import sagaIdentityIndex from './identity/saga';
import sagaSamIndex from './sam/saga';
import sagaVendorIndex from './vendor/saga';
import sagaCmsIndex from './cms/saga';
import sagaNotificationIndex from './notification/saga';
import sagaSharedIndex from './shared/saga';

export default function* sagas() {
  yield [
    fork(sagaCatalogIndex),
    fork(sagaCommonIndex),
    fork(sagaIdentityIndex),
    fork(sagaSamIndex),
    fork(sagaVendorIndex),
    fork(sagaCmsIndex),
    fork(sagaNotificationIndex),
    fork(sagaSharedIndex),
  ];
}
