import {
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
} from '../../../constants/index';

const initialState = {
  editFormLoading: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Public Loading">
    case EDIT_FORM_LOADING_SHOW_REQUEST:
      return {
        ...state,
        editFormLoading: true,
      };
    case EDIT_FORM_LOADING_HIDE_REQUEST:
      return {
        ...state,
        editFormLoading: false,
      };

    // </editor-fold>

    default:
      return state;
  }
}
