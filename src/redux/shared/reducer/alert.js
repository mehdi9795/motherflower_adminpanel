import {
  EDIT_FORM_ALERT_HIDE_REQUEST,
  EDIT_FORM_ALERT_SHOW_REQUEST,
} from '../../../constants';

const initialState = {
  editFormAlert: false,
  editFormData: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Public Alert">
    case EDIT_FORM_ALERT_SHOW_REQUEST:
      return {
        ...state,
        editFormData: action.data,
        editFormAlert: true,
      };
    case EDIT_FORM_ALERT_HIDE_REQUEST:
      return {
        ...state,
        editFormAlert: false,
      };

    // </editor-fold>

    default:
      return state;
  }
}
