import {
  EDIT_FORM_LOADING_SHOW_REQUEST,
  EDIT_FORM_LOADING_HIDE_REQUEST,
} from '../../../constants/index';

// <editor-fold dsc="Public Loading">
export function loadingShowRequest() {
  return {
    type: EDIT_FORM_LOADING_SHOW_REQUEST,
  };
}

export function loadingHideRequest() {
  return {
    type: EDIT_FORM_LOADING_HIDE_REQUEST,
  };
}

// </editor-fold>
