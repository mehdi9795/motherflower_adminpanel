import {
  EDIT_FORM_ALERT_SHOW_REQUEST,
  EDIT_FORM_ALERT_HIDE_REQUEST,
} from '../../../constants/index';

// <editor-fold dsc="Public Alert">
export function alertShowRequest(data) {
  return {
    type: EDIT_FORM_ALERT_SHOW_REQUEST,
    data,
  };
}

export function AlertHideRequest() {
  return {
    type: EDIT_FORM_ALERT_HIDE_REQUEST,
  };
}

// </editor-fold>
