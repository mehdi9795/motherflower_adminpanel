import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  editFormAlertShowRequest: ['data'],
  editFormAlertHideRequest: null,
});

export default Creators;
/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: '',
  error: {},
});

/* ------------- Reducers ------------- */

// we're attempting to fetching
export const hide = state => ({
  ...state,
  loading: false,
});
// we've successfully fetch LIst items from server
export const show = (state, action) => {
  const { data } = action;
  return {
    ...state,
    data,
    loading: true,
    error: {},
  };
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EDIT_FORM_ALERT_SHOW_REQUEST]: show,
  [Types.EDIT_FORM_ALERT_HIDE_REQUEST]: hide,
});

/* ------------- Selectors ------------- */
export const getEditFormAlert = state => state.editFormAlert;

// Is the current user logged in?
// export const isLastPage = (listFetchState) => listFetchState.count / pageSize >
