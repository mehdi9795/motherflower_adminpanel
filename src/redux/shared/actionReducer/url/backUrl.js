import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  backUrlSuccess: ['data'],
});

export default Creators;
/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: '',
  error: {},
});

/* ------------- Reducers ------------- */

// we've successfully fetch LIst items from server
export const success = (state, action) => {
  const { data } = action;
  return {
    ...state,
    data,
    error: {},
  };
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.BACK_URL_SUCCESS]: success,
});

/* ------------- Selectors ------------- */
export const getBackUrl = state => state.backUrl;

// Is the current user logged in?
// export const isLastPage = (listFetchState) => listFetchState.count / pageSize >
