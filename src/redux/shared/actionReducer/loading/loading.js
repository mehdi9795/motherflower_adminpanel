import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  editFormLoadingShowRequest: null,
  editFormLoadingHideRequest: null,
});

export default Creators;
/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: [],
  error: {},
});

/* ------------- Reducers ------------- */

// we're attempting to fetching
export const hide = state => ({
  ...state,
  loading: false,
});
// we've successfully fetch LIst items from server
export const show = state => {
  return {
    ...state,
    loading: true,
  };
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EDIT_FORM_LOADING_SHOW_REQUEST]: show,
  [Types.EDIT_FORM_LOADING_HIDE_REQUEST]: hide,
});

/* ------------- Selectors ------------- */
export const getEditFormAlert = state => state.editFormLoading;

// Is the current user logged in?
// export const isLastPage = (listFetchState) => listFetchState.count / pageSize >
