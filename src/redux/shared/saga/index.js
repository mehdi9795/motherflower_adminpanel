import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import history from '../../../history';
import { BACK_URL_SUCCESS } from '../../../constants';
import { backUrlSuccess } from './url';

// main saga generators
export default function* sagaSharedIndex() {
  // yield takeLatest(testRequestFlow);
  // yield all(takeLatest(testRequestFlow));
  yield all([
    // <editor-fold dsc="back url">
    // takeLatest(BACK_URL_SUCCESS, backUrlSuccess, history),
    // </editor-fold>
  ]);
}
