import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import MessageList from './MessageList';
import { getDtoQueryString } from '../../../../utils/helper';
import { getVendorBranchApi } from '../../../../services/vendorApi';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import { MessageDto } from '../../../../dtos/notificationDtos';
import messageListActions from '../../../../redux/notification/actionReducer/message/List';
import messageDetailsActions from '../../../../redux/notification/actionReducer/message/Details';
import requestTypeListActions from '../../../../redux/notification/actionReducer/requestType/List';
import { PAGE_TITLE_MESSAGE_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import { getCookie } from '../../../../utils';
import {
  GetNotificationApi,
  PutNotificationApi,
} from '../../../../services/notificationApi';

async function action({ store, query }) {
  const token = getCookie('token');
  if (query.messageId) {
    await PutNotificationApi(token, { id: parseInt(query.messageId, 0) });
    const json = new BaseGetDtoBuilder()
      .dto(
        new MessageDto({
          messageDto: new MessageDto({ id: query.messageId }),
          messageSearchMode: query.mode,
        }),
      )
      .buildJson();

    const responseMessage = await GetNotificationApi(
      token,
      getDtoQueryString(json),
    );
    if (
      responseMessage.status === 200 &&
      responseMessage.data.items &&
      responseMessage.data.items.length > 0
    ) {
      store.dispatch(
        messageDetailsActions.messageDetailsSuccess(
          responseMessage.data.items[0],
        ),
      );
    }
  }

  /**
   * get all vendor branch for first time with default clause
   */

  const jsonList = new BaseGetDtoBuilder()
    .dto(
      new VendorBranchDto({
        active: true,
      }),
    )
    .all(true)
    .buildJson();
  const vendorBranchData = await getVendorBranchApi(
    store.getState().login.data.token,
    getDtoQueryString(jsonList),
  );
  store.dispatch(
    vendorBranchListActions.vendorBranchListSuccess(vendorBranchData.data),
  );

  /**
   * get all receive message with default clause
   */

  const jsonListMessage = new BaseGetDtoBuilder()
    .dto(
      new MessageDto({
        messageSearchMode: 'Inbox',
      }),
    )
    .buildJson();
  store.dispatch(messageListActions.messageListRequest(jsonListMessage));
  store.dispatch(
    requestTypeListActions.requestTypeListRequest(JSON.stringify({})),
  );

  return {
    chunks: ['messageList'],
    title: `${PAGE_TITLE_MESSAGE_LIST}`,
    component: (
      <Layout>
        <MessageList />
      </Layout>
    ),
  };
}

export default action;
