import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { hideLoading } from 'react-redux-loading-bar';
import cs from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import s from './MessageList.css';
import {
  YES,
  NO,
  PROMOTION_LIST,
  SUBJECT,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_MESSAGE,
  GENDER,
  VENDOR_BRANCH,
  ALL_VENDOR_BRANCH,
  USER,
  TYPE,
  STATUS,
  DATE,
  ALL_USERS,
  DETAILS,
  ANSWER,
  SMS,
  SEND,
  EMERGENCY,
  INSTANTANEOUS,
  NORMAL,
  INSERT,
  USERS,
  RECEIVER,
  USER_ROLE,
  REQUEST_LIST,
} from '../../../../Resources/Localization';
import CPButton from '../../../../components/CP/CPButton';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import CPRadio from '../../../../components/CP/CPRadio';
import CPMultiSelect from '../../../../components/CP/CPMultiSelect';
import {
  VendorBranchDto,
  VendorBranchUserDto,
} from '../../../../dtos/vendorDtos';
import vendorBranchUserListActions from '../../../../redux/vendor/actionReducer/vendorBranchUser/List';
import messageDetailsActions from '../../../../redux/notification/actionReducer/message/Details';
import messagePostActions from '../../../../redux/notification/actionReducer/message/Post';
import messageListActions from '../../../../redux/notification/actionReducer/message/List';
import messageDeleteActions from '../../../../redux/notification/actionReducer/message/Delete';
import { MessageDto, RequestTypeDto } from '../../../../dtos/notificationDtos';
import { UserDto } from '../../../../dtos/identityDtos';
import CPModal from '../../../../components/CP/CPModal';
import CPInput from '../../../../components/CP/CPInput';
import Link from '../../../../components/Link';
import CPTextArea from '../../../../components/CP/CPTextArea';
import { PutNotificationApi } from '../../../../services/notificationApi';
import { getCookie } from '../../../../utils';
import { getRoleApi, getUserApi } from '../../../../services/identityApi';
import { getDtoQueryString } from '../../../../utils/helper';
import {
  getVendorBranchApi,
  getVendorBranchUserApi,
} from '../../../../services/vendorApi';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class MessageList extends React.Component {
  static propTypes = {
    messageLoading: PropTypes.bool,
    messageCount: PropTypes.number,
    vendorBranchUserCount: PropTypes.number,
    messages: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    vendorBranchUser: PropTypes.arrayOf(PropTypes.object),
    requestTypes: PropTypes.arrayOf(PropTypes.object),
    messageDetails: PropTypes.objectOf(PropTypes.any),
    loginData: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    promotionTypes: [],
    messageLoading: false,
    messageCount: 0,
    vendorBranchUserCount: 0,
    messages: [],
    vendorBranches: [],
    vendorBranchUser: [],
    requestTypes: [],
    messageDetails: {},
    loginData: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      messageType: 'Inbox',
      vendorBranchId: '0',
      vendorBranchUserIds: [],
      visibleMessageDetailModal: !!props.messageDetails.title,
      senderName: '',
      subject: props.messageDetails.title ? props.messageDetails.subject : '',
      body: '',
      date: props.messageDetails.title ? props.messageDetails.subject : '',
      showReplayForm: false,
      replayBody: '',
      replaySubject: '',
      senderId: props.messageDetails.title
        ? props.messageDetails.userDto.id
        : 0,
      messageId: props.messageDetails.title ? props.messageDetails.id : 0,
      replayStatus: props.messageDetails.title
        ? props.messageDetails.messageSensitivity
        : 'Emergency',
      replayRequestType: props.messageDetails.title
        ? props.messageDetails.requestTypeDto.id
        : '',
      children: props.messageDetails.title ? props.messageDetails.children : [],
      visibleMessageInsertModal: false,
      visibleAddUserModal: false,
      sendStatus: '',
      sendRequestType:
        props.requestTypes.length > 0 ? props.requestTypes[0].id : '',
      sendSubject: '',
      sendToUsers: [],
      selectedUser: [],
      sendBody: '',
      userRoleId: '',
      vendorBranchIdForSend: '',
      vendorBranchesForSend: [],
      userIds: [],
      users: [],
      sendToCenter: false,
      roles: [],
      visibleVendorBranchInAddUser: true,
      visibleRoleInAddUser: true,
      userArray: [],
      isPending: false,
    };
    this.submitSearch = this.submitSearch.bind(this);
    this.showAddUserModal = this.showAddUserModal.bind(this);
    this.visibleDetailModal = this.visibleDetailModal.bind(this);
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.messageDetails.title) {
      this.setState({
        visibleMessageDetailModal: !!nextProps.messageDetails.title,
        subject: nextProps.messageDetails.title
          ? nextProps.messageDetails.subject
          : '',
        date: nextProps.messageDetails.title
          ? nextProps.messageDetails.stringCreatedOnUtc
          : '',
        children: nextProps.messageDetails.title
          ? nextProps.messageDetails.children
          : [],
        senderId: nextProps.messageDetails.title
          ? nextProps.messageDetails.userDto.id
          : 0,
        messageId: nextProps.messageDetails.title
          ? nextProps.messageDetails.id
          : 0,
        replayStatus: nextProps.messageDetails.title
          ? nextProps.messageDetails.messageSensitivity
          : 'Emergency',
        replayRequestType: nextProps.messageDetails.title
          ? nextProps.messageDetails.requestTypeDto.id
          : '',
      });
    }
  }

  onDelete = id => {
    const data = {
      id,
      listDto: this.messageContainer(),
    };
    this.props.actions.messageDeleteRequest(data);
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
    if (inputName === 'vendorBranchId' && value !== '0') {
      /**
       * get Vendor branch user for first time with vendorBranchId
       * @type {string}
       */
      this.props.actions.vendorBranchUserListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchUserDto({
              vendorBranchDto: new VendorBranchDto({ id: value }),
            }),
          )
          .includes(['Users'])
          .buildJson(),
      );
    } else if (inputName === 'vendorBranchId' && value === '0') {
      this.props.actions.vendorBranchUserListSuccess(null);
      this.setState({ vendorBranchUserIds: [] });
    }
  };

  messageContainer = () => {
    const { messageType, vendorBranchId, vendorBranchUserIds } = this.state;
    const { vendorBranchUserCount } = this.props;
    const { vendorBranchUser } = this.props;
    let usersSelected;
    let internalMessageDto;

    if (vendorBranchId !== '0') {
      usersSelected = [];
      if (vendorBranchUserCount > 0) {
        if (vendorBranchUserIds.length > 0) {
          vendorBranchUserIds.map(item => {
            // const userDtoTemp = new UserDto();
            // userDtoTemp.id = item;
            usersSelected.push(new UserDto({ id: item }));
            return null;
          });
        } else {
          vendorBranchUser.map(item => {
            // const userDtoTemp = new UserDto();
            // userDtoTemp.id = item.userDto.id;
            usersSelected.push(new UserDto({ id: item.userDto.id }));
            return null;
          });
        }

        // const messageDtoTemp = new MessageDto();
        // messageDtoTemp.searchUserDtos = usersSelected;
        internalMessageDto = new MessageDto({ searchUserDtos: usersSelected });
      } else {
        // const userDtoTemp = new UserDto();
        // userDtoTemp.id = 0;
        usersSelected.push(new UserDto({ id: 0 }));

        // const messageDtoTemp = new MessageDto();
        // messageDtoTemp.searchUserDtos = usersSelected;
        internalMessageDto = new MessageDto({ searchUserDtos: usersSelected });
      }
    }

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new MessageDto({
          messageSearchMode: messageType,
          messageDto: internalMessageDto,
          searchUserDtos: usersSelected,
        }),
      )
      .buildJson();

    return jsonList;
  };

  async submitSearch() {
    this.props.actions.messageListRequest(this.messageContainer());
  }

  rowClassName = record => {
    if (record.hasNew) return 'rowHasNew';
    return '';
  };

  async visibleDetailModal(e, record) {
    e.preventDefault();
    const token = getCookie('token');
    this.setState({
      visibleMessageDetailModal: true,
      senderName: record.fullName,
      date: record.date,
      subject: record.subject,
      body: record.body,
      senderId: record.senderId,
      messageId: record.key,
      replayRequestType: record.type,
      children: record.messages,
      isPending: record.isPending,
      showReplayForm: false,
      replayBody: '',
    });
    await PutNotificationApi(token, { id: record.key });
  }

  handleCancelMessageDetailModal = () => {
    this.setState({
      visibleMessageDetailModal: false,
      senderName: '',
      date: '',
      subject: '',
      body: '',
      senderId: '',
      messageId: 0,
      replayRequestType: '',
      children: [],
    });
    this.submitSearch();
    this.props.actions.messageDetailsSuccess({});
  };

  showReplayForm = () => {
    this.setState({ showReplayForm: true });
  };

  sendReplayForm = (newMessage = false) => {
    const {
      replayBody,
      replayRequestType,
      senderId,
      messageId,
      sendStatus,
      sendSubject,
      selectedUser,
      sendBody,
      sendRequestType,
    } = this.state;
    const userDtoArray = [];

    // let messageDtoTemp = new MessageDto();
    // messageDtoTemp.id = messageId;
    const parent = new MessageDto({ id: messageId });

    const userDtoTemp = new UserDto();
    if (newMessage === true) {
      selectedUser.map(item => {
        // userDtoTemp = new UserDto();
        // userDtoTemp.id = item;
        userDtoArray.push({ userDto: new UserDto({ id: item }) });
        return null;
      });
    } else {
      userDtoTemp.id = senderId;
    }

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new MessageDto({
          userMessageDtos:
            newMessage === true ? userDtoArray : [{ userDto: userDtoTemp }],
          parent: newMessage === true ? undefined : parent,
          body: newMessage === true ? sendBody : replayBody,
          subject: newMessage === true ? sendSubject : undefined,
          messageSensitivity: newMessage === true ? sendStatus : undefined,
          requestTypeDto: new RequestTypeDto({
            id: newMessage === true ? sendRequestType : replayRequestType,
          }),
        }),
      )
      .build();

    const data = {
      data: jsonCrud,
      listDto: new BaseGetDtoBuilder()
        .dto(
          new MessageDto({
            messageSearchMode: 'Send',
          }),
        )
        .buildJson(),
    };

    this.props.actions.messagePostRequest(data);
    this.props.actions.messageDetailsSuccess({});

    this.setState({
      visibleMessageDetailModal: false,
      senderName: '',
      date: '',
      subject: '',
      body: '',
      senderId: '',
      messageId: '',
      replayStatus: 'Emergency',
      messageType: 'Send',
    });
  };

  handleCancelMessageInsertModal = () => {
    this.setState({
      visibleMessageInsertModal: false,
    });
  };

  handleCancelAddUserModal = () => {
    this.setState({
      visibleAddUserModal: false,
    });
  };

  showInserModal = () => {
    const { requestTypes } = this.props;

    if (requestTypes.length > 0)
      this.setState({
        sendRequestType: requestTypes[0].id,
        visibleMessageInsertModal: true,
      });
    else
      this.setState({
        visibleMessageInsertModal: true,
      });
  };

  async showAddUserModal() {
    const token = getCookie('token');
    const { selectedUser } = this.state;
    const { loginData } = this.props;
    const roles = [];
    let userRoleId = '';
    const vendorBranches = [];
    let vendorBranchId = '';
    const users = [];
    const userIds = [];
    if (loginData.appType !== 'Florist') {
      const responseGetRole = await getRoleApi(
        token,
        getDtoQueryString(JSON.stringify({ all: true })),
      );
      if (responseGetRole.status === 200) {
        responseGetRole.data.items.map(item => {
          if (item.roleStatus !== 'Common') {
            roles.push(
              <Option key={item.id} value={`${item.id}*${item.roleStatus}`}>
                {item.name}
              </Option>,
            );
            if (userRoleId === '') userRoleId = `${item.id}*${item.roleStatus}`;
          }
          return null;
        });

        if (
          userRoleId.split('*')[1] === 'Driver' ||
          userRoleId.split('*')[1] === 'Seller' ||
          userRoleId.split('*')[1] === 'VendorAdmin'
        ) {
          const responseGetvendorBranch = await getVendorBranchApi(
            token,
            getDtoQueryString(JSON.stringify({ dto: { active: true } })),
          );

          if (responseGetvendorBranch.status === 200) {
            responseGetvendorBranch.data.items.map(item => {
              vendorBranches.push(
                <Option key={item.id} value={item.id}>
                  {item.name}
                </Option>,
              );
              if (vendorBranchId === '') vendorBranchId = item.id;
              return null;
            });

            if (vendorBranchId !== '') {
              const dto = {
                dto: {
                  vendorBranchDto: { id: vendorBranchId },
                  searchNotExpectedUserIds: selectedUser,
                  roleDtos: [{ id: userRoleId.split('*')[0] }],
                },
                includes: ['Users', 'VendorBranchUserRoles'],
              };
              const responseGetUser = await getVendorBranchUserApi(
                token,
                getDtoQueryString(JSON.stringify(dto)),
              );

              if (responseGetUser.status === 200) {
                responseGetUser.data.items.map(item => {
                  users.push(
                    <Option
                      key={item.userDto.id.toString()}
                      value={item.userDto.id.toString()}
                    >
                      {item.userDto.displayName}
                    </Option>,
                  );
                  userIds.push(item.userDto.id.toString());
                  return null;
                });
              }
            }
          }
        } else {
          const dto = {
            dto: {
              userRoleDto: [{ roleDto: { id: userRoleId.split('*')[0] } }],
            },
            notExpectedUserIds: selectedUser,
          };
          const responseGetUser = await getUserApi(
            token,
            getDtoQueryString(JSON.stringify(dto)),
          );
          if (responseGetUser.status === 200) {
            responseGetUser.data.items.map(item => {
              users.push(
                <Option key={item.id.toString()} value={item.id.toString()}>
                  {item.displayName}
                </Option>,
              );
              userIds.push(item.id.toString());
              return null;
            });
          }

          this.setState({ visibleVendorBranchInAddUser: false });
        }
      }
    } else {
      const responseGetUser = await getVendorBranchUserApi(
        loginData.token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new VendorBranchUserDto({
                fromCurrentVendorBranch: true,
              }),
            )
            .includes(['Users', 'Roles'])
            .buildJson(),
        ),
      );

      if (responseGetUser.status === 200) {
        responseGetUser.data.items.map(item => {
          users.push(
            <Option
              key={item.userDto.id.toString()}
              value={item.userDto.id.toString()}
            >
              {item.userDto.displayName}
            </Option>,
          );
          userIds.push(item.userDto.id.toString());
          return null;
        });
      }
      this.setState({ visibleVendorBranchInAddUser: false });
    }
    this.setState({
      visibleAddUserModal: true,
      roles,
      userRoleId,
      vendorBranchIdForSend: vendorBranchId,
      vendorBranchesForSend: vendorBranches,
      userIds,
      users,
      visibleRoleInAddUser: loginData.appType !== 'Florist',
    });
  }

  async handelChangeInputFromModal(inputName, value) {
    const token = getCookie('token');
    const vendorBranches = [];
    let vendorBranchId = '';
    const users = [];
    const userIds = [];
    const { selectedUser } = this.state;
    this.setState({ [inputName]: value });

    if (inputName === 'userRoleId') {
      if (
        value.split('*')[1] === 'Driver' ||
        value.split('*')[1] === 'Seller' ||
        value.split('*')[1] === 'VendorAdmin'
      ) {
        const responseGetvendorBranch = await getVendorBranchApi(
          token,
          getDtoQueryString(JSON.stringify({ dto: { active: true } })),
        );
        if (responseGetvendorBranch.status === 200) {
          responseGetvendorBranch.data.items.map(item => {
            vendorBranches.push(
              <Option key={item.id} value={item.id}>
                {item.name}
              </Option>,
            );
            if (vendorBranchId === '') vendorBranchId = item.id;
            return null;
          });

          if (vendorBranchId !== '') {
            const dto = {
              dto: {
                vendorBranchDto: { id: vendorBranchId },
                searchNotExpectedUserIds: selectedUser,
                roleDtos: [{ id: value.split('*')[0] }],
              },
              includes: ['Users', 'VendorBranchUserRoles'],
            };
            const responseGetUser = await getVendorBranchUserApi(
              token,
              getDtoQueryString(JSON.stringify(dto)),
            );

            if (responseGetUser.status === 200) {
              responseGetUser.data.items.map(item => {
                users.push(
                  <Option
                    key={item.userDto.id.toString()}
                    value={item.userDto.id.toString()}
                  >
                    {item.userDto.displayName}
                  </Option>,
                );
                userIds.push(item.userDto.id.toString());
                return null;
              });
            }
          }
          this.setState({ visibleVendorBranchInAddUser: true });
        }
      } else {
        const dto = {
          dto: { userRoleDto: [{ roleDto: { id: value.split('*')[0] } }] },
        };
        const responseGetUser = await getUserApi(
          token,
          getDtoQueryString(JSON.stringify(dto)),
        );

        if (responseGetUser.status === 200) {
          responseGetUser.data.items.map(item => {
            users.push(
              <Option key={item.id.toString()} value={item.id.toString()}>
                {item.displayName}
              </Option>,
            );
            userIds.push(item.id.toString());
            return null;
          });
        }

        this.setState({ visibleVendorBranchInAddUser: false });
      }
    }
    this.setState({
      vendorBranchIdForSend: vendorBranchId,
      vendorBranchesForSend: vendorBranches,
      userIds,
      users,
    });
  }

  insertToMultiSelect = () => {
    const userMultiSelect = [];
    this.state.users.map(item => {
      this.state.userIds.map(id => {
        if (item.props.value === id) {
          userMultiSelect.push(item);
          this.state.selectedUser.push(id);
          this.state.sendToUsers.push(item);
        }
        return null;
      });
      return null;
    });

    this.setState({ visibleAddUserModal: false });
  };

  renderInsertModal = () => {
    const {
      sendStatus,
      sendSubject,
      sendToUsers,
      selectedUser,
      sendBody,
      sendRequestType,
    } = this.state;
    const { requestTypes } = this.props;

    const requestTypeArray = [];

    requestTypes.map(item => {
      requestTypeArray.push(
        <Option key={item.id} value={item.id}>
          {item.name}
        </Option>,
      );
      return null;
    });

    return (
      <div>
        <CPModal
          visible={this.state.visibleMessageInsertModal}
          handleCancel={this.handleCancelMessageInsertModal}
          title={DETAILS}
          className="max_modal"
          showFooter={false}
        >
          <div className="row">
            <div className="col-lg-3 col-sm-10">
              <label>{SUBJECT}</label>
              <CPInput
                onChange={value =>
                  this.handelChangeInput('sendSubject', value.target.value)
                }
                value={sendSubject}
              />
            </div>
            <div className="col-lg-6 col-sm-12">
              <label>{RECEIVER}</label>
              <div className={s.receiver}>
                <CPMultiSelect
                  value={selectedUser}
                  onChange={value => {
                    this.handelChangeInput('selectedUser', value);
                  }}
                >
                  {sendToUsers}
                </CPMultiSelect>
                <CPButton onClick={this.showAddUserModal}>{INSERT}</CPButton>
              </div>
            </div>
            {/* <div className="col-lg-3 col-sm-12">
              <label>{TYPE}</label>
              <CPSelect
                value={sendRequestType}
                onChange={value =>
                  this.handelChangeInput('sendRequestType', value)
                }
              >
                {requestTypeArray}
              </CPSelect>
            </div> */}
            <div className="col-lg-3 col-sm-12">
              <label>{STATUS}</label>
              <CPSelect
                value={sendStatus}
                onChange={value => this.handelChangeInput('sendStatus', value)}
              >
                <Option key="Emergency" value="Emergency">
                  {EMERGENCY}
                </Option>
                <Option key="Instantaneous" value="Instantaneous">
                  {INSTANTANEOUS}
                </Option>
                <Option key="Normal" value="Normal">
                  {NORMAL}
                </Option>
              </CPSelect>
            </div>

            <div className="col-lg-9 col-sm-12">
              <label>{SMS}</label>
              <div className={s.receiver}>
                <CPTextArea
                  onChange={value =>
                    this.handelChangeInput('sendBody', value.target.value)
                  }
                  value={sendBody}
                />
                <CPButton
                  className={s.btnSend}
                  onClick={() => this.sendReplayForm(true)}
                >
                  {SEND}
                </CPButton>
              </div>
            </div>
          </div>
        </CPModal>
      </div>
    );
  };

  renderAddUserModal = () => {
    const {
      userRoleId,
      vendorBranchIdForSend,
      userIds,
      roles,
      vendorBranchesForSend,
      users,
      visibleVendorBranchInAddUser,
      visibleRoleInAddUser,
    } = this.state;
    return (
      <CPModal
        visible={this.state.visibleAddUserModal}
        handleCancel={this.handleCancelAddUserModal}
        title={DETAILS}
        // className="max_modal"
        showFooter={false}
      >
        <div className="row">
          {visibleRoleInAddUser && (
            <div className="col-sm-12">
              <label>{USER_ROLE}</label>
              <CPSelect
                value={userRoleId}
                onChange={value =>
                  this.handelChangeInputFromModal('userRoleId', value)
                }
              >
                {roles}
              </CPSelect>
            </div>
          )}
          {visibleVendorBranchInAddUser && (
            <div className="col-sm-12">
              <label>{VENDOR_BRANCH}</label>
              <CPSelect
                value={vendorBranchIdForSend}
                onChange={value =>
                  this.handelChangeInput('vendorBranchIdForSend', value)
                }
              >
                {vendorBranchesForSend}
              </CPSelect>
            </div>
          )}
          <div className="col-sm-12">
            <label>{USERS}</label>
            <CPMultiSelect
              value={userIds}
              onChange={value => {
                this.handelChangeInput('userIds', value);
              }}
            >
              {users}
            </CPMultiSelect>
          </div>

          <div className={cs('col-sm-12', s.btnAnswer)}>
            <CPButton onClick={this.insertToMultiSelect}>{INSERT}</CPButton>
          </div>
        </div>
      </CPModal>
    );
  };

  render() {
    const {
      messageLoading,
      messageCount,
      messages,
      vendorBranches,
      vendorBranchUser,
    } = this.props;
    const {
      messageType,
      vendorBranchId,
      vendorBranchUserIds,
      subject,
      date,
      showReplayForm,
      replayBody,
      children,
      isPending,
    } = this.state;

    const radioModel = [
      {
        value: 'Inbox',
        name: 'دریافتی',
      },
      {
        value: 'Send',
        name: 'ارسالی',
      },
    ];

    const columns = [
      { title: '', dataIndex: 'fullName', key: 'fullName' },
      {
        title: '',
        dataIndex: 'subject',
        key: 'subject',
        render: (text, record) => (
          <Link to="/" onClick={e => this.visibleDetailModal(e, record)}>
            {text}
          </Link>
        ),
      },
      {
        title: '',
        dataIndex: 'stringType',
        key: 'type',
      },
      {
        title: '',
        dataIndex: 'stringStatus',
        key: 'status',
        render: (text, record) =>
          record.status === 'Emergency' ? (
            <label className="errorOrder">{text}</label>
          ) : record.status === 'Instantaneous' ? (
            <label className="canceledOrder">{text}</label>
          ) : (
            record.status === 'Normal' && (
              <label className="pendingOrder">{text}</label>
            )
          ),
      },
      {
        title: '',
        dataIndex: 'date',
        key: 'date',
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 80,
        render: (text, record) => (
          <CPPopConfirm
            title={ARE_YOU_SURE_WANT_TO_DELETE_THE_MESSAGE}
            okText={YES}
            cancelText={NO}
            okType="primary"
            onConfirm={() => this.onDelete(record.key)}
          >
            <CPButton className="delete_action">
              <i className="cp-trash" />
            </CPButton>
          </CPPopConfirm>
        ),
      },
    ];
    const messageDataSource = [];
    const vendorBranchArray = [];
    const vendorBranchUserArray = [];

    messages.map(item => {
      const obj = {
        key: item.id,
        fullName: item.title,
        subject: item.subject,
        type: item.requestTypeDto.id,
        status: item.messageSensitivity,
        stringType: item.requestTypeDto.name,
        stringStatus: item.stringMessageSensitivity,
        date: item.stringCreatedOnUtc,
        body: item.body,
        senderId: item.userDto.id,
        messages: item.children,
        hasNew: item.hasNew,
        isPending: item.isPending,
      };
      messageDataSource.push(obj);
      return null;
    });

    vendorBranches.map(item =>
      vendorBranchArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    if (vendorBranchId !== '0') {
      vendorBranchUser.map(item =>
        vendorBranchUserArray.push(
          <Option key={item.userDto.id}>
            {item.userDto.firstName} {item.userDto.lastName}
          </Option>,
        ),
      );
    }

    return (
      <div className={s.mainContent}>
        <h3>{REQUEST_LIST}</h3>
        <CardTable
          data={messageDataSource}
          count={messageCount}
          columns={columns}
          loading={messageLoading}
          onAddClick={this.showInserModal}
          rowClassName={record => this.rowClassName(record)}
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{GENDER}</label>
                  <CPRadio
                    model={radioModel}
                    size="small"
                    onChange={value =>
                      this.handelChangeInput('messageType', value.target.value)
                    }
                    defaultValue={messageType}
                  />
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{VENDOR_BRANCH}</label>
                  <CPSelect
                    value={vendorBranchId}
                    onChange={value => {
                      this.handelChangeInput('vendorBranchId', value);
                    }}
                  >
                    <Option key={0}>{ALL_VENDOR_BRANCH}</Option>
                    {vendorBranchArray}
                  </CPSelect>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{USER}</label>
                  <CPMultiSelect
                    placeholder={ALL_USERS}
                    value={vendorBranchUserIds}
                    onChange={value => {
                      this.handelChangeInput('vendorBranchUserIds', value);
                    }}
                  >
                    {vendorBranchUserArray}
                  </CPMultiSelect>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CardTable>
        <CPModal
          visible={this.state.visibleMessageDetailModal}
          handleCancel={this.handleCancelMessageDetailModal}
          title={DETAILS}
          className="max_modal"
          showFooter={false}
        >
          <div className="row">
            {/* <div className="col-lg-4 col-sm-12">
              <label>{SENDER}</label>
              <CPInput value={senderName} />
            </div> */}
            <div className="col-lg-4 col-sm-12">
              <label>{DATE}</label>
              <CPInput value={date} />
            </div>
            <div className="col-lg-4 col-sm-12">
              <label>{SUBJECT}</label>
              <CPInput value={subject} />
            </div>
            {children.map(item => (
              <div className="col-sm-12">
                <label className={s.titleMessage}>
                  {item.userDto.displayName}
                </label>
                <div className={s.bodyMessage}>{item.body}</div>
              </div>
            ))}
            {!isPending &&
              !showReplayForm && (
                <div className={cs('col-sm-12', s.btnAnswer)}>
                  <CPButton onClick={this.showReplayForm}>{ANSWER}</CPButton>
                </div>
              )}
          </div>
          {showReplayForm && (
            <div className="row">
              <div className="col-sm-12">
                <label>{SMS}</label>
                <CPTextArea
                  onChange={value =>
                    this.handelChangeInput('replayBody', value.target.value)
                  }
                  value={replayBody}
                />
              </div>

              <div className={cs('col-sm-12', s.btnAnswer)}>
                <CPButton onClick={() => this.sendReplayForm()}>
                  {SEND}
                </CPButton>
              </div>
            </div>
          )}
        </CPModal>
        {this.renderInsertModal()}
        {this.renderAddUserModal()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messageList.data ? state.messageList.data.items : [],
  messageCount: state.messageList.data ? state.messageList.data.count : 0,
  messageLoading: state.messageList.loading,
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
  vendorBranchUser: state.vendorBranchUserList.data
    ? state.vendorBranchUserList.data.items
    : [],
  vendorBranchUserCount: state.vendorBranchUserList.data
    ? state.vendorBranchUserList.data.count
    : 0,
  requestTypes: state.requestTypeList.data
    ? state.requestTypeList.data.items
    : [],
  messageDetails: state.messageDetails.data,
  loginData: state.login.data ? state.login.data : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorBranchUserListRequest:
        vendorBranchUserListActions.vendorBranchUserListRequest,
      messageListRequest: messageListActions.messageListRequest,
      vendorBranchUserListSuccess:
        vendorBranchUserListActions.vendorBranchUserListSuccess,
      messagePostRequest: messagePostActions.messagePostRequest,
      messageDeleteRequest: messageDeleteActions.messageDeleteRequest,
      messageDetailsSuccess: messageDetailsActions.messageDetailsSuccess,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(MessageList));
