import React from 'react';
import SmsEdit from './SmsEdit';
import Layout from '../../../../components/Template/Layout/Layout';
import {
  NOTIFICATION_EDIT,
  SMS_EDIT,
} from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import smsStatusListActions from '../../../../redux/notification/actionReducer/sms/StatusList';
import singleSmsActions from '../../../../redux/notification/actionReducer/sms/SingleSms';
import { getDtoQueryString } from '../../../../utils/helper';
import { PushDto, SmsDto } from '../../../../dtos/notificationDtos';
import { GetSmsBulkApi } from '../../../../services/notificationApi';
import roleListActions from '../../../../redux/identity/actionReducer/role/Role';
import cityActions from "../../../../redux/common/actionReducer/city/List";

async function action({ store, query, params }) {
  const { backUrl = '/sms/list' } = query;

  const jsonSingleSms = new BaseGetDtoBuilder()
    .dto(
      new SmsDto({
        id: params.id,
      }),
    )
    .includes(['smsBulkRoleDtos', 'smsBulkUserDtos', 'roleDto'])
    .buildJson();
  const singleSms = await GetSmsBulkApi(
    store.getState().login.data.token,
    getDtoQueryString(jsonSingleSms),
  );

  store.dispatch(singleSmsActions.singleSmsSuccess(singleSms.data));

  if (singleSms.status === 200)
    store.dispatch(
      smsStatusListActions.smsStatusListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new PushDto({
              id:
                singleSms.data &&
                singleSms.data.items &&
                singleSms.data.items.length > 0
                  ? singleSms.data.items[0].statusId
                  : 0,
            }),
          )
          .buildJson(),
      ),
    );

  store.dispatch(
    roleListActions.roleListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  /**
   * get all city
   */
  store.dispatch(
    cityActions.cityListRequest(
      new BaseGetDtoBuilder()
        .all(true)
        .disabledCount(true)
        .buildJson(),
    ),
  );

  return {
    chunks: ['smsEdit'],
    title: `${SMS_EDIT}`,
    component: (
      <Layout>
        <SmsEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
