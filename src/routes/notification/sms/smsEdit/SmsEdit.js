import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './SmsEdit.css';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_SMS,
  NOTIFICATION,
} from '../../../../Resources/Localization';
import SmsForm from '../../../../components/Notification/Sms/SmsForm/SmsForm';
import smsDeleteActions from '../../../../redux/notification/actionReducer/sms/Delete';

class SmsEdit extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
    sms: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    backUrl: '/push/list',
    sms: null,
  };

  delete = () => {
    const { sms } = this.props;

    const deleteData = {
      data: sms.id,
      from: 'edit',
    };

    this.props.actions.smsDeleteRequest(deleteData);
  };

  render() {
    const { backUrl, sms } = this.props;

    return (
      <div className={s.mainContent}>
        <EditCard
          title={`${NOTIFICATION}`}
          backUrl={backUrl}
          isEdit={sms && sms.id > 0}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_SMS}
          onDelete={this.delete}
        >
          <SmsForm />
        </EditCard>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  sms:
    state.singleSms.data && state.singleSms.data.items[0]
      ? state.singleSms.data.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      smsDeleteRequest: smsDeleteActions.smsDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(SmsEdit));
