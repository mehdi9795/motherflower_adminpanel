import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import SmsList from './SmsList';
import smsListActions from '../../../../redux/notification/actionReducer/sms/List';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import smsStatusListActions from "../../../../redux/notification/actionReducer/sms/StatusList";

async function action({ store }) {
  store.dispatch(
    smsListActions.smsListRequest(
      new BaseGetDtoBuilder().all(true).includes(['smsBulkRoleDtos', 'smsBulkUserDtos']).buildJson(),
    ),
  );

  store.dispatch(
    smsStatusListActions.smsStatusListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  return {
    chunks: ['smsList'],
    title: `لیست `,
    component: (
      <Layout>
        <SmsList />
      </Layout>
    ),
  };
}

export default action;
