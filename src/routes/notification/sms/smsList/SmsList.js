import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import s from './SmsList.css';
import {
  YES,
  NO,
  SUBJECT,
  STATUS,
  DATE,
  SMS,
  REQUEST_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT,
  HOUR,
  ALL,
  TYPE,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_SMS, USER_ROLE,
} from '../../../../Resources/Localization';
import CPButton from '../../../../components/CP/CPButton';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import Link from '../../../../components/Link';
import CPAvatar from '../../../../components/CP/CPAvatar';
import smsListActions from '../../../../redux/notification/actionReducer/sms/List';
import smsDeleteActions from '../../../../redux/notification/actionReducer/sms/Delete';
import smsSendActions from '../../../../redux/notification/actionReducer/sms/Send';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import history from '../../../../history';
import {SmsBulkRoleDto, SmsBulkUserDto, SmsDto} from '../../../../dtos/notificationDtos';
import {RoleDto, UserDto} from "../../../../dtos/identityDtos";

const { Option } = Select;

class SmsList extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    smses: PropTypes.arrayOf(PropTypes.object),
    smsStatus: PropTypes.arrayOf(PropTypes.object),
    smsLoading: PropTypes.bool,
    smsCount: PropTypes.number,
  };

  static defaultProps = {
    smses: [],
    smsStatus: [],
    smsCount: 0,
    smsLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      searchStatus: '0',
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const { searchEntityType, searchStatus } = this.state;
    const listDto = new BaseGetDtoBuilder()
      .dto({
        entityType: searchEntityType,
        status: searchStatus,
      })
      .buildJson();

    const deleteData = {
      data: id,
      listDto,
    };

    this.props.actions.smsDeleteRequest(deleteData);
  };

  handleChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  async submitSearch() {
    const { searchStatus, searchEntityType } = this.state;

    this.props.actions.smsListRequest(
      new BaseGetDtoBuilder()
        .dto({
          entityType: searchEntityType,
          status: searchStatus,
        })
        .buildJson(),
    );
  }

  smsAddClick = () => {
    this.props.actions.showLoading();
    history.push('/sms/create');
  };

  sendPush = record => {
    const roleDtos = [];
    const userDtos = [];
    record.roleIds.map(item => {
      const dto = new SmsBulkRoleDto({
        roleDto: new RoleDto({
          id: item,
        }),
        smsBulkDto: new RoleDto({
          id: record.key,
        }),
      });
      roleDtos.push(dto);
      return null;
    });
    record.userIds.map(item => {
      const dto = new SmsBulkUserDto({
        userDto: new UserDto({
          id: item,
        }),
        smsBulkDto: new UserDto({
          id: record.key,
        }),
      });
      userDtos.push(dto);
      return null;
    });

    const jsonList = new BaseCRUDDtoBuilder()
      .dto(
        new SmsDto({
          id: record.key,
          body: record.body,
          status: record.statusId,
          persianDateUtc: record.date,
          hour: record.hour,
          smsBulkRoleDtos: roleDtos,
          testStatus: 'Force',
          smsBulkUserDtos: userDtos,
          smsBulkUserTypes: record.smsBulkUserTypes,
        }),
      )
      .build();
console.log('JSON',JSON.stringify(jsonList));
    this.props.actions.smsSendRequest(jsonList);
  };

  render() {
    const { smses, smsCount, smsLoading, smsStatus } = this.props;
    const { searchStatus } = this.state;

    const columns = [
      { title: SMS, dataIndex: 'body', key: 'body', width: 180 },
      { title: STATUS, dataIndex: 'status', key: 'status', width: 180 },
      { title: USER_ROLE, dataIndex: 'roles', key: 'roles', width: 180 },
      { title: USER_ROLE, dataIndex: 'users', key: 'users', width: 180 },
      {
        title: DATE,
        dataIndex: 'date',
        key: 'date',
        width: 100,
      },
      { title: HOUR, dataIndex: 'hour', key: 'hour', width: 10 },
      {
        title: '',
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPButton onClick={() => this.sendPush(record)}>
              ارسال تستی
            </CPButton>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Agent_Delete',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_SMS}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Agent_Edit',
        width: 50,
        render: (text, record) => (
          <div>
            {record.editable && (
              <Link
                to={`/sms/edit/${record.key}`}
                onClick={() => {
                  this.props.actions.showLoading();
                }}
                className="edit_action"
              >
                <i className="cp-edit" />
              </Link>
            )}
          </div>
        ),
      },
    ];
    const dataSource = [];
    const statusArray = [];

    smses.map(item => {
      const roleNames = [];
      const roleIds = [];
      const userNames = [];
      const userIds = [];
      if (item.smsBulkRoleDtos)
        item.smsBulkRoleDtos.map(item2 => {
          roleNames.push(item2.roleDto.name);
          roleIds.push(item2.roleDto.id);
          return null;
        });
      if (item.smsBulkUserDtos)
        item.smsBulkUserDtos.map(item2 => {
          userNames.push(item2.userDto.displayName);
          userIds.push(item2.userDto.id);
          return null;
        });
      const obj = {
        body: item.body.replace(/enter/g, '\n'),
        status: item.persianStatus,
        statusId: item.statusId,
        date: item.persianDateUtc,
        hour: item.hour,
        key: item.id,
        editable: item.editable,
        roles: roleNames,
        roleIds,
        users: userNames.toString(),
        userIds,
        smsBulkUserTypes: item.smsBulkUserTypes,
      };

      dataSource.push(obj);
      return null;
    });

    smsStatus.map(item =>
      statusArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <div className={s.mainContent}>
        <h3>{REQUEST_LIST}</h3>

        <CardTable
          data={dataSource}
          count={smsCount}
          columns={columns}
          loading={smsLoading}
          onAddClick={this.smsAddClick}
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{STATUS}</label>
                  <CPSelect
                    value={searchStatus}
                    onChange={value =>
                      this.handleChangeInput('searchStatus', value)
                    }
                  >
                    {statusArray}
                  </CPSelect>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CardTable>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  smses: state.smsList.data ? state.smsList.data.items : [],
  smsCount: state.smsList.data ? state.smsList.data.count : 0,
  smsLoading: state.smsList.loading,
  smsStatus: state.smsStatusList.data ? state.smsStatusList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      smsListRequest: smsListActions.smsListRequest,
      smsDeleteRequest: smsDeleteActions.smsDeleteRequest,
      smsSendRequest: smsSendActions.smsSendRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(SmsList));
