import React from 'react';
import SmsCreate from './SmsCreate';
import Layout from '../../../../components/Template/Layout/Layout';
import roleListActions from '../../../../redux/identity/actionReducer/role/Role';
import smsStatusListActions from '../../../../redux/notification/actionReducer/sms/StatusList';
import SingleSmsActions from '../../../../redux/notification/actionReducer/sms/SingleSms';
import { SMS_CREATE } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import cityActions from "../../../../redux/common/actionReducer/city/List";

async function action({ store, query }) {
  const { backUrl = '/sms/list' } = query;

  store.dispatch(
    roleListActions.roleListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  store.dispatch(SingleSmsActions.singleSmsSuccess([]));

  store.dispatch(
    smsStatusListActions.smsStatusListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  /**
   * get all city
   */
  store.dispatch(
    cityActions.cityListRequest(
      new BaseGetDtoBuilder()
        .all(true)
        .disabledCount(true)
        .buildJson(),
    ),
  );

  return {
    chunks: ['smsCreate'],
    title: `${SMS_CREATE}`,
    component: (
      <Layout>
        <SmsCreate backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
