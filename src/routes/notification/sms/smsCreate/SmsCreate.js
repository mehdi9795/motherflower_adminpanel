import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './SmsCreate.css';
import {NOTIFICATION_CREATE, SMS_CREATE} from '../../../../Resources/Localization';
import SmsForm from '../../../../components/Notification/Sms/SmsForm/SmsForm';

class VendorCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
  };

  render() {
    const { backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={SMS_CREATE} backUrl={backUrl}>
          <SmsForm />
        </EditCard>
      </div>
    );
  }
}

export default withStyles(s)(VendorCreate);
