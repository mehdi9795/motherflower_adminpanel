import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import s from './PushTestGroupList.css';
import {
  YES,
  NO,
  REQUEST_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT,
  NAME,
  LAST_NAME,
  PHONE,
  DETAILS,
  USER_USERNAME,
  USER_ROLE,
  USER_FULL_NAME,
  USER_USER_ROLE,
  STATUS,
  DEACTIVATE,
  ACTIVE,
} from '../../../../Resources/Localization';
import CPButton from '../../../../components/CP/CPButton';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import Link from '../../../../components/Link';
import pushListActions from '../../../../redux/notification/actionReducer/push/List';
import pushDeleteActions from '../../../../redux/notification/actionReducer/push/Delete';
import CPModal from '../../../../components/CP/CPModal';
import CPIntlPhoneInput from '../../../../components/CP/CPIntlPhoneInput';
import CPMultiSelect from '../../../../components/CP/CPMultiSelect';
import { RoleDto, UserDto, UserRoleDto } from '../../../../dtos/identityDtos';
import {
  BaseGetDtoBuilder,
  BaseListCRUDDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import userAppInfoListActions from '../../../../redux/identity/actionReducer/user/UserAppInfoList';
import {
  PushTestGroupDto,
  UserAppInfoAppTypeDto,
  UserAppInfoDto,
} from '../../../../dtos/notificationDtos';
import CPList from '../../../../components/CP/CPList';
import pushTestGroupPostActions from '../../../../redux/notification/actionReducer/push/PushTestGroupPost';
import pushTestGroupDeleteActions from '../../../../redux/notification/actionReducer/push/PushTestGroupDelete';
import pushTestGroupPutActions from '../../../../redux/notification/actionReducer/push/PushTestGroupPut';
import {
  getDtoQueryString,
  pressEnterAndCallApplySearch,
} from '../../../../utils/helper';
import CPSwitch from '../../../../components/CP/CPSwitch';
import history from '../../../../history';

const { Option } = Select;

class PushTestGroupList extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    pushTestGroups: PropTypes.arrayOf(PropTypes.object),
    users: PropTypes.arrayOf(PropTypes.object),
    roles: PropTypes.arrayOf(PropTypes.object),
    pushTestGroupCount: PropTypes.number,
    pushTestGroupLoading: PropTypes.bool,
    userLoading: PropTypes.bool,
  };

  static defaultProps = {
    pushTestGroups: [],
    users: [],
    roles: [],
    pushTestGroupCount: 0,
    pushTestGroupLoading: false,
    userLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      userNameFinal: '',
      userName: '',
      userRoleIds: [],
      visibleUserModal: false,
      selectedUserAppInfoIds: [],
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const jsonList = new BaseGetDtoBuilder()
      .includes(['userAppInfoDto'])
      .buildJson();

    const deleteData = {
      data: getDtoQueryString(JSON.stringify({ id })),
      listDto: jsonList,
    };

    this.props.actions.pushTestGroupDeleteRequest(deleteData);
  };

  handleChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  async submitSearch() {
    const { userNameFinal, userRoleIds } = this.state;
    const { pushTestGroups } = this.props;

    const userRoleDtos = [];
    const testGroupIds = [];

    if (pushTestGroups)
      pushTestGroups.map(item => {
        if (item && item.userAppInfoAppTypeDto)
          testGroupIds.push(item.userAppInfoAppTypeDto.id);
        return null;
      });

    userRoleIds.map(roleId => {
      userRoleDtos.push(
        new UserRoleDto({ roleDto: new RoleDto({ id: roleId }) }),
      );
      return null;
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserAppInfoDto({
          userDto: new UserDto({
            userName: userNameFinal,
            userRoleDto: userRoleDtos,
          }),
        }),
      )
      .includes(['userDto', 'userAppInfoDto'])
      .notExpectedIds(testGroupIds)
      .buildJson();

    this.props.actions.userAppInfoListRequest(jsonList);
  }

  pushTestGroupAddClick = () => {
    this.props.actions.userAppInfoListSuccess(null);
    this.setState({ visibleUserModal: true });
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        userNameFinal: number.replace(/ /g, ''),
        userName: value,
      });
    }
  };

  onSelectedUserAppInfo = selectedRowKeys => {
    this.setState({ selectedUserAppInfoIds: selectedRowKeys });
  };

  handleCancelUserModal = () => {
    this.setState({ visibleUserModal: false, selectedUserAppInfoIds: [] });
    window.location.replace('/pushTestGroup/list');
  };

  handleOkUserModal = () => {
    const { selectedUserAppInfoIds } = this.state;
    this.setState({ visibleUserModal: false, selectedUserAppInfoIds: [] });

    const appInfoArray = [];
    selectedUserAppInfoIds.map(item =>
      appInfoArray.push(
        new PushTestGroupDto({
          userAppInfoAppTypeDto: new UserAppInfoAppTypeDto({ id: item }),
          published: true,
        }),
      ),
    );
    const jsonCrud = new BaseListCRUDDtoBuilder().dtos(appInfoArray).build();

    const jsonList = new BaseGetDtoBuilder()
      .includes(['userAppInfoDto'])
      .buildJson();

    const postData = {
      data: jsonCrud,
      listDto: jsonList,
    };

    this.props.actions.pushTestGroupPostRequest(postData);
  };

  onChangeStatus = (value, id) => {
    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos([{ id, published: value }])
      .build();

    const postData = {
      data: jsonCrud,
    };

    this.props.actions.pushTestGroupPutRequest(postData);
  };

  render() {
    const {
      pushTestGroups,
      pushTestGroupCount,
      pushTestGroupLoading,
      userLoading,
      users,
      roles,
    } = this.props;
    const { userRoleIds, userName, selectedUserAppInfoIds } = this.state;

    const columns = [
      { title: NAME, dataIndex: 'firstName', key: 'firstName', width: 100 },
      { title: LAST_NAME, dataIndex: 'lastName', key: 'lastName', width: 100 },
      { title: PHONE, dataIndex: 'phone', key: 'phone', width: 100 },
      { title: 'OS', dataIndex: 'os', key: 'os', width: 100 },
      {
        title: 'OS Version',
        dataIndex: 'osVersion',
        key: 'osVersion',
        width: 100,
      },
      {
        title: 'App Version',
        dataIndex: 'appVersion',
        key: 'appVersion',
        width: 100,
      },
      {
        title: 'Device Model',
        dataIndex: 'deviceModel',
        key: 'deviceModel',
        width: 100,
      },
      {
        title: 'App Type',
        dataIndex: 'appType',
        key: 'appType',
        width: 100,
      },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 50,
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              checkedChildren={ACTIVE}
              unCheckedChildren={DEACTIVATE}
              defaultChecked={record.active}
              onChange={value => {
                this.onChangeStatus(value, record.key);
              }}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Agent_Delete',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
    ];
    const userColumns = [
      { title: USER_FULL_NAME, dataIndex: 'fullName', key: 'fullName' },
      { title: PHONE, dataIndex: 'phone', key: 'phone' },
      { title: 'OS', dataIndex: 'os', key: 'os' },
      { title: 'OS Version', dataIndex: 'osVersion', key: 'osVersion' },
      { title: 'App Version', dataIndex: 'appVersion', key: 'appVersion' },
      { title: 'Device Model', dataIndex: 'deviceModel', key: 'deviceModel' },
      { title: 'App Type', dataIndex: 'appType', key: 'appType' },
      // { title: USER_USER_ROLE, dataIndex: 'userRole', key: 'userRole' },
    ];
    const dataSource = [];
    const userDataSource = [];
    const userRoleArray = [];
    const rowSelection = {
      selectedUserAppInfoIds,
      onChange: this.onSelectedUserAppInfo,
    };

    pushTestGroups.map(item => {
      const obj = {
        firstName:
          item.userAppInfoAppTypeDto && item.userAppInfoAppTypeDto.userDto
            ? item.userAppInfoAppTypeDto.userDto.firstName
            : '',
        lastName:
          item.userAppInfoAppTypeDto && item.userAppInfoAppTypeDto.userDto
            ? item.userAppInfoAppTypeDto.userDto.lastName
            : '',
        phone:
          item.userAppInfoAppTypeDto && item.userAppInfoAppTypeDto.userDto
            ? item.userAppInfoAppTypeDto.userDto.phone.replace('+98', '0')
            : '',
        os: item.userAppInfoAppTypeDto
          ? item.userAppInfoAppTypeDto.userAppInfoDto.os
          : '',
        osVersion: item.userAppInfoAppTypeDto
          ? item.userAppInfoAppTypeDto.userAppInfoDto.osVersion
          : '',
        appVersion: item.userAppInfoAppTypeDto
          ? item.userAppInfoAppTypeDto.appVersion
          : '',
        deviceModel: item.userAppInfoAppTypeDto
          ? item.userAppInfoAppTypeDto.userAppInfoDto.deviceModel
          : '',
        appType: item.userAppInfoAppTypeDto
          ? item.userAppInfoAppTypeDto.appType
          : '',
        active: item.published,
        key: item.id,
      };

      dataSource.push(obj);
      return null;
    });
    users.map(item => {
      // const userRoles = [];
      // item.userRoleDto.map(userRole => userRoles.push(userRole.roleDto.name));

      const obj = {
        fullName: item.userDto.displayName,
        phone: item.userDto.phone.replace('+98', '0'),
        // userRole: userRoles.join(),
        key: item.id,
        os: item.userAppInfoDto.os,
        osVersion: item.userAppInfoDto.osVersion,
        appVersion: item.appVersion,
        deviceModel: item.userAppInfoDto.deviceModel,
        appType: item.appType,
      };
      return userDataSource.push(obj);
    });

    roles.map(item =>
      userRoleArray.push(<Option key={item.id}>{item.name}</Option>),
    );
    return (
      <div className={s.mainContent}>
        <h3>{REQUEST_LIST}</h3>

        <CPList
          data={dataSource}
          count={pushTestGroupCount}
          columns={columns}
          loading={pushTestGroupLoading}
          onAddClick={this.pushTestGroupAddClick}
          hideSearchBox
        />

        <CPModal
          visible={this.state.visibleUserModal}
          handleCancel={this.handleCancelUserModal}
          handleOk={this.handleOkUserModal}
          title={DETAILS}
          className="max_modal"
        >
          <CPList
            data={userDataSource}
            columns={userColumns}
            loading={userLoading}
            rowSelection={rowSelection}
            selectedIds={selectedUserAppInfoIds}
            onChange={this.onSelectedUserAppInfo}
            pagination={false}
            withCheckBox
            hideAdd
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{USER_USERNAME}</label>
                    <CPIntlPhoneInput
                      value={userName}
                      onChange={this.changeNumber}
                    />
                  </div>

                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{USER_ROLE}</label>
                    <CPMultiSelect
                      value={userRoleIds}
                      showSearch
                      onChange={value => {
                        this.handleChangeInput('userRoleIds', value);
                      }}
                    >
                      {userRoleArray}
                    </CPMultiSelect>
                  </div>
                </div>
              </div>
              <div className="field">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  pushTestGroups: state.pushTestGroupList.data
    ? state.pushTestGroupList.data.items
    : [],
  pushTestGroupCount: state.pushTestGroupList.data
    ? state.pushTestGroupList.data.count
    : 0,
  pushTestGroupLoading: state.pushTestGroupList.loading,
  users: state.userAppInfoList.data ? state.userAppInfoList.data.items : [],
  userLoading: state.userAppInfoList.loading,
  roles: state.role.data ? state.role.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      pushListRequest: pushListActions.pushListRequest,
      pushDeleteRequest: pushDeleteActions.pushDeleteRequest,
      userAppInfoListRequest: userAppInfoListActions.userAppInfoListRequest,
      userAppInfoListSuccess: userAppInfoListActions.userAppInfoListSuccess,
      pushTestGroupPostRequest:
        pushTestGroupPostActions.pushTestGroupPostRequest,
      pushTestGroupPutRequest: pushTestGroupPutActions.pushTestGroupPutRequest,
      pushTestGroupDeleteRequest:
        pushTestGroupDeleteActions.pushTestGroupDeleteRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PushTestGroupList));
