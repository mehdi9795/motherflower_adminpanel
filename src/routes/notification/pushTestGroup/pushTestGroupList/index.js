import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import PushTestGroupList from './PushTestGroupList';
import pushTestGroupListActions from '../../../../redux/notification/actionReducer/push/PushTestGroupList';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import roleActions from '../../../../redux/identity/actionReducer/role/Role';

async function action({ store }) {
  store.dispatch(
    pushTestGroupListActions.pushTestGroupListRequest(
      new BaseGetDtoBuilder().includes(['userAppInfoDto']).buildJson(),
    ),
  );

  /**
   * get all role
   */
  store.dispatch(
    roleActions.roleListRequest(
      new BaseGetDtoBuilder().dto({ roleStatus: 'System' }).buildJson(),
    ),
  );

  return {
    chunks: ['pushTestGroupList'],
    title: `لیست `,
    component: (
      <Layout>
        <PushTestGroupList />
      </Layout>
    ),
  };
}

export default action;
