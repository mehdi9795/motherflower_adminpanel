import React from 'react';
import PushEdit from './PushEdit';
import Layout from '../../../../components/Template/Layout/Layout';
import { NOTIFICATION_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import pushStatusListActions from '../../../../redux/notification/actionReducer/push/PushStatusList';
import singlePushActions from '../../../../redux/notification/actionReducer/push/SinglePush';
import { getDtoQueryString } from '../../../../utils/helper';
import { PushDto } from '../../../../dtos/notificationDtos';
import { GetPushApi } from '../../../../services/notificationApi';

async function action({ store, query, params }) {
  const { backUrl = '/push/list' } = query;

  /**
   * get single vendor agent by id for edit
   */
  const jsonSinglePush = new BaseGetDtoBuilder()
    .dto(
      new PushDto({
        id: params.id,
      }),
    )
    .includes(['imageDto'])
    .buildJson();
  const singlePush = await GetPushApi(
    store.getState().login.data.token,
    getDtoQueryString(jsonSinglePush),
  );

  store.dispatch(singlePushActions.singlePushSuccess(singlePush.data));

  if (singlePush.status === 200)
    store.dispatch(
      pushStatusListActions.pushStatusListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new PushDto({
              id:
                singlePush.data &&
                singlePush.data.items &&
                singlePush.data.items.length > 0
                  ? singlePush.data.items[0].statusId
                  : 0,
            }),
          )
          .buildJson(),
      ),
    );

  return {
    chunks: ['pushEdit'],
    title: `${NOTIFICATION_EDIT}`,
    component: (
      <Layout>
        <PushEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
