import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './PushEdit.css';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR,
  NOTIFICATION,
  VENDOR_EDIT,
} from '../../../../Resources/Localization';
import PushForm from '../../../../components/Notification/Push/PushForm/PushForm';
import history from '../../../../history';
import pushDeleteActions from '../../../../redux/notification/actionReducer/push/Delete';
import sendPushPostActions from '../../../../redux/notification/actionReducer/push/SendPushPost';
import { BaseCRUDDtoBuilder } from '../../../../dtos/dtoBuilder';
import { PushDto } from '../../../../dtos/notificationDtos';

class PushEdit extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
    push: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    backUrl: '/push/list',
    push: null,
  };

  delete = () => {
    const { push } = this.props;

    const deleteData = {
      data: push.id,
      from: 'edit',
    };

    this.props.actions.pushDeleteRequest(deleteData);
  };

  sendPush = e => {
    const { push } = this.props;
    e.preventDefault();
    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(new PushDto({ id: push.id, testStatus: 'force' }))
      .build();

    this.props.actions.sendPushPostRequest(jsonCrud);
  };

  render() {
    const { backUrl, push } = this.props;

    const renderActions = (
      <div>
        <div>
          <a onClick={e => this.sendPush(e)} href="#">
            ارسال تستی
          </a>
        </div>
      </div>
    );

    return (
      <div className={s.mainContent}>
        <EditCard
          title={`${NOTIFICATION}`}
          backUrl={backUrl}
          isEdit={push && push.id > 0}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR}
          onDelete={this.delete}
          renderActions={renderActions}
          showPopHover
        >
          <PushForm />
        </EditCard>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  push:
    state.singlePush.data && state.singlePush.data.items[0]
      ? state.singlePush.data.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      pushDeleteRequest: pushDeleteActions.pushDeleteRequest,
      sendPushPostRequest: sendPushPostActions.sendPushPostRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PushEdit));
