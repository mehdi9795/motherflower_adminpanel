import React from 'react';
import PushCreate from './PushCreate';
import Layout from '../../../../components/Template/Layout/Layout';
import { NOTIFICATION_CREATE } from '../../../../Resources/Localization';

async function action({ store, query }) {
  const { backUrl = '/push/list' } = query;

  return {
    chunks: ['pushCreate'],
    title: `${NOTIFICATION_CREATE}`,
    component: (
      <Layout>
        <PushCreate backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
