import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './PushCreate.css';
import { NOTIFICATION_CREATE } from '../../../../Resources/Localization';
import PushForm from '../../../../components/Notification/Push/PushForm/PushForm';

class VendorCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
  };

  render() {
    const { backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={NOTIFICATION_CREATE} backUrl={backUrl}>
          <PushForm />
        </EditCard>
      </div>
    );
  }
}

export default withStyles(s)(VendorCreate);
