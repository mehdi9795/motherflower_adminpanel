import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import s from './PushList.css';
import {
  YES,
  NO,
  SUBJECT,
  STATUS,
  DATE,
  SMS,
  REQUEST_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT,
  HOUR,
  ALL,
  TYPE,
} from '../../../../Resources/Localization';
import CPButton from '../../../../components/CP/CPButton';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import Link from '../../../../components/Link';
import CPAvatar from '../../../../components/CP/CPAvatar';
import pushListActions from '../../../../redux/notification/actionReducer/push/List';
import pushDeleteActions from '../../../../redux/notification/actionReducer/push/Delete';
import sendPushPostActions from '../../../../redux/notification/actionReducer/push/SendPushPost';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import history from '../../../../history';
import { PushDto } from '../../../../dtos/notificationDtos';

const { Option } = Select;

class PushList extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    pushes: PropTypes.arrayOf(PropTypes.object),
    pushStatus: PropTypes.arrayOf(PropTypes.object),
    pushCount: PropTypes.number,
    pushLoading: PropTypes.bool,
  };

  static defaultProps = {
    pushes: [],
    pushStatus: [],
    pushCount: 0,
    pushLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      visiblePushModal: false,
      searchStatus: '0',
      searchEntityType: 'None',
      currentPage: 1,
      pageSize: 20,
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const { searchStatus, searchEntityType } = this.state;
    const jsonList = new BaseGetDtoBuilder()
      .dto({
        entityType: searchEntityType,
        status: searchStatus,
      })
      .buildJson();

    const data = {
      data: id,
      listDto: jsonList,
      from: 'list',
    };

    this.props.actions.pushDeleteRequest(data);
  };

  handleChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  async submitSearch() {
    const { searchStatus, searchEntityType, pageSize } = this.state;

    this.props.actions.pushListRequest(
      new BaseGetDtoBuilder()
        .dto({
          entityType: searchEntityType,
          status: searchStatus,
        })
        .pageSize(pageSize)
        .pageIndex(0)
        .buildJson(),
    );
  }

  pushAddClick = () => {
    this.props.actions.showLoading();
    history.push('/push/create');
  };

  sendPush = id => {
    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(new PushDto({ id, testStatus: 'force' }))
      .build();

    this.props.actions.sendPushPostRequest(jsonCrud);
  };

  onChangePagination = page => {
    const {searchEntityType,searchStatus, pageSize, currentPage} = this.state;
    this.setState({ currentPage: page }, () =>
      this.props.actions.pushListRequest(
        new BaseGetDtoBuilder()
          .dto({
            entityType: searchEntityType,
            status: searchStatus,
          })
          .pageSize(pageSize)
          .pageIndex(currentPage)
          .buildJson(),
      ),
    );
  };

  onShowSizeChange = (current, newPageSize) => {
    const {searchEntityType,searchStatus, pageSize, currentPage} = this.state;
    this.setState({ currentPage: current, pageSize: newPageSize }, () =>
      this.props.actions.pushListRequest(
        new BaseGetDtoBuilder()
          .dto({
            entityType: searchEntityType,
            status: searchStatus,
          })
          .pageSize(pageSize)
          .pageIndex(currentPage)
          .buildJson(),
      ),
    );
  };

  render() {
    const { pushes, pushCount, pushLoading, pushStatus } = this.props;
    const { searchStatus, searchEntityType } = this.state;

    const columns = [
      {
        title: '',
        dataIndex: 'url',
        width: 50,
        render: (text, record) => (
          <div onClick={() => this.showImageModal(record.imgUrl, record.key)}>
            <CPAvatar src={record.imgUrl} />
          </div>
        ),
      },
      { title: SUBJECT, dataIndex: 'subject', key: 'subject', width: 180 },
      { title: SMS, dataIndex: 'body', key: 'body', width: 180 },
      { title: STATUS, dataIndex: 'status', key: 'status', width: 180 },
      { title: TYPE, dataIndex: 'entityType', key: 'entityType', width: 180 },
      {
        title: DATE,
        dataIndex: 'date',
        key: 'date',
        width: 250,
      },
      { title: HOUR, dataIndex: 'hour', key: 'hour', width: 100 },
      {
        title: '',
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            {record.editable && (
              <CPButton onClick={() => this.sendPush(record.key)}>
                ارسال تستی
              </CPButton>
            )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Agent_Delete',
        width: 50,
        render: (text, record) => (
          <div>
            {record.editable && (
              <CPPopConfirm
                title={ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT}
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() => this.onDelete(record.key)}
              >
                <CPButton className="delete_action">
                  <i className="cp-trash" />
                </CPButton>
              </CPPopConfirm>
            )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Agent_Edit',
        width: 50,
        render: (text, record) => (
          <div>
            {record.editable && (
              <Link
                to={`/push/edit/${record.key}`}
                onClick={() => {
                  this.props.actions.showLoading();
                }}
                className="edit_action"
              >
                <i className="cp-edit" />
              </Link>
            )}
          </div>
        ),
      },
    ];
    const dataSource = [];
    const statusArray = [];

    pushes.map(item => {
      const obj = {
        imgUrl: item.imageDto ? item.imageDto.url : '',
        subject: item.subject,
        body: item.body,
        status: item.persianStatus,
        date: item.persianDateUtc,
        hour: item.hour,
        key: item.id,
        editable: item.editable,
        entityType: item.persianEntityType,
      };

      dataSource.push(obj);
      return null;
    });

    pushStatus.map(item =>
      statusArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <div className={s.mainContent}>
        <h3>{REQUEST_LIST}</h3>

        <CardTable
          data={dataSource}
          count={pushCount}
          columns={columns}
          loading={pushLoading}
          onAddClick={this.pushAddClick}
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{STATUS}</label>
                  <CPSelect
                    value={searchStatus}
                    onChange={value =>
                      this.handleChangeInput('searchStatus', value)
                    }
                  >
                    {statusArray}
                  </CPSelect>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{TYPE}</label>
                  <CPSelect
                    value={searchEntityType}
                    onChange={value =>
                      this.handleChangeInput('searchEntityType', value)
                    }
                  >
                    <Option key="None">{ALL}</Option>
                    <Option key="Bill">سفارش</Option>
                    <Option key="Subscription">عادی</Option>
                    <Option key="Product">محصول</Option>
                  </CPSelect>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CardTable>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  pushes: state.pushList.data ? state.pushList.data.items : [],
  pushCount: state.pushList.data ? state.pushList.data.count : 0,
  pushLoading: state.pushList.loading,
  pushStatus: state.pushStatusList.data ? state.pushStatusList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      pushListRequest: pushListActions.pushListRequest,
      pushDeleteRequest: pushDeleteActions.pushDeleteRequest,
      sendPushPostRequest: sendPushPostActions.sendPushPostRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PushList));
