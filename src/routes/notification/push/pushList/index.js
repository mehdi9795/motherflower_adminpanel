import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import PushList from './PushList';
import pushListActions from '../../../../redux/notification/actionReducer/push/List';
import pushStatusListActions from '../../../../redux/notification/actionReducer/push/PushStatusList';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  store.dispatch(
    pushListActions.pushListRequest(
      new BaseGetDtoBuilder().includes(['imageDto']).pageSize(10).pageIndex(0).buildJson(),
    ),
  );
  store.dispatch(
    pushStatusListActions.pushStatusListRequest(
      new BaseGetDtoBuilder().dto({ id: 0 }).buildJson(),
    ),
  );

  return {
    chunks: ['pushList'],
    title: `لیست `,
    component: (
      <Layout>
        <PushList />
      </Layout>
    ),
  };
}

export default action;
