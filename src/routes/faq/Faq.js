/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import { Collapse } from 'antd';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Faq.css';
const Panel = Collapse.Panel;

const text = '  لورم ایپسوم یا طرح‌نما به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد. \n';

class Faq extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  };

  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <h1>{this.props.title}</h1>

          <Collapse accordion>
            <Panel header="سوال شماره 1" key="1">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 2" key="2">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 3" key="3">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 4" key="4">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 5" key="5">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 6" key="6">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 7" key="7">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 8" key="8">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 9" key="9">
              <p>{text}</p>
            </Panel>
            <Panel header="سوال شماره 10" key="10">
              <p>{text}</p>
            </Panel>
          </Collapse>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Faq);
