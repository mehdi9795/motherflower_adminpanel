import React from 'react';
import Profile from './Profile';
import { getDtoQueryString } from '../../utils/helper';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { getUserApi } from '../../services/identityApi';
import { UserDto } from '../../dtos/identityDtos';
import singleUserActions from '../../redux/identity/actionReducer/user/SingleUser';
import Layout from '../../components/Template/Layout';
import cityListActions from '../../redux/common/actionReducer/city/List';

async function action({ store }) {

  if (store.getState().login.data.token) {
    const responseUser = await getUserApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new UserDto({
              fromMobile: true,
            }),
          )
          .includes(['imageDto'])
          .buildJson(),
      ),
    );

    if (responseUser.status === 200)
      store.dispatch(singleUserActions.singleUserSuccess(responseUser.data));
    else store.dispatch(singleUserActions.singleUserFailure());
  }

  /**
   * get all city for first time with default clause
   */
  store.dispatch(
    cityListActions.cityListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  return {
    chunks: ['profile'],
    component: (
      <Layout>
        <Profile />
      </Layout>
    ),
  };
}

export default action;
