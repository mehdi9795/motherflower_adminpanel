import React from 'react';
import { Select } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Profile.css';
import {
  EMAIL,
  FAMILY_NAME,
  SEXUAL,
  NAME,
  BIRTHDAY_DATE,
  MOBILE,
  SAVE,
  EDIT_INFORMATION,
  CITY,
  USER_EDIT, FARVARDIN, ORDIBEHESHT, KHORDAD, TIR, ESFAND, BAHMAN, DEY, AZAR, ABAN, MEHR, SHAHRIVAR, MORDAD,
} from '../../Resources/Localization';
import CPInput from '../../components/CP/CPInput';
import CPSelect from '../../components/CP/CPSelect';
import CPRadio from '../../components/CP/CPRadio';
import CPButton from '../../components/CP/CPButton';
import { UserDto } from '../../dtos/identityDtos';
import { getDtoQueryString, showNotification } from '../../utils/helper';
import { getCookie, setCookie } from '../../utils';
import { getUserApi, putUserApi } from '../../services/identityApi';
import { BaseCRUDDtoBuilder, BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { CityDto } from '../../dtos/commonDtos';
import CPValidator from '../../components/CP/CPValidator';
import EditCard from '../../components/CP/EditCard';
import { message } from 'antd';
import { ImageDto } from '../../dtos/cmsDtos';
import {
  deleteImageApi,
  getImageApi,
  postImageApi,
  putImageApi,
} from '../../services/cmsApi';
import CPModal from '../../components/CP/CPModal';
import CPImageEditor from '../../components/CP/CPImageEditor';
import singleUserActions from '../../redux/identity/actionReducer/user/SingleUser';
import loginActions from '../../redux/identity/actionReducer/account/Login';

const { Option } = Select;

class Profile extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    cities: PropTypes.arrayOf(PropTypes.object),
    user: PropTypes.objectOf(PropTypes.any),
    loginData: PropTypes.objectOf(PropTypes.any),
    backUrl: PropTypes.string,
  };

  static defaultProps = {
    cities: [],
    backUrl: '/',
    user: {},
    loginData: {},
  };

  constructor(props) {
    super(props);
    let cityId = props.cities && props.cities[0] ? props.cities[0].id : '';
    if (props.user.cityDto) cityId = props.user.cityDto.id;
    this.state = {
      gender: props.user.id ? props.user.genderType : 'M',
      firstName: props.user.id ? props.user.firstName : '',
      lastName: props.user.id ? props.user.lastName : '',
      phone: props.user.id ? props.user.phone : '',
      dayOfBirthDate: props.user.id ? props.user.dayOfBirthDate.toString() : '1',
      monthOfBirthDate: props.user.id
        ? props.user.monthOfBirthDate.toString()
        : '1',
      email: props.user.id ? props.user.email : '',
      countryCode: props.user.id ? props.user.countryCode : '',
      errorValid: false,
      cityId,
      imageBase64:
        props.user.id && props.user.imageDto
          ? props.user.imageDto.url
          : '/images/avatar.jpg',
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.deleteProfileImage = this.deleteProfileImage.bind(this);
    this.showModalImage = this.showModalImage.bind(this);
    this.saveImage = this.saveImage.bind(this);
    this.onChangeImage = this.onChangeImage.bind(this);
    this.validation = [];
    this.imageEditorFunc = null;
  }

  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  async onSubmit() {
    const {
      gender,
      firstName,
      lastName,
      phone,
      dayOfBirthDate,
      monthOfBirthDate,
      email,
      countryCode,
      cityId,
    } = this.state;
    if (this.validation.length === 3) {
      // this.props.actions.loadingSearchShowRequest();
      const token = getCookie('token');

      const userCrud = new BaseCRUDDtoBuilder()
        .dto(
          new UserDto({
            firstName,
            lastName,
            phone,
            countryCode,
            dayOfBirthDate,
            monthOfBirthDate,
            email,
            genderType: gender,
            fromMobile: true,
            cityDto: new CityDto({ id: cityId }),
          }),
        )
        .build();

      const response = await putUserApi(token, userCrud);
      // this.props.actions.loadingSearchHideRequest();
      if (response.status === 200) {
        setCookie(
          'displayName',
          encodeURIComponent(`${firstName} ${lastName}`),
          8760,
        );
        message.success('ویرایش اطلاعات کاربری با موفقیت انجام شد', 5);
      } else {
        // this.props.actions.showAlertRequest(response.data.errorMessage);
      }
    } else {
      this.setState({ errorValid: true });
    }
  }

  handleInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
  };

  async showModalImage() {
    const { user } = this.props;
    const token = getCookie('siteToken');
    if (user.imageDto) {
      const json = new BaseGetDtoBuilder()
        .dto(
          new ImageDto({
            id: user.imageDto.id,
            imageSizeType: 'small',
          }),
        )
        .includes(['binaries'])
        .buildJson();
      const responseImage = await getImageApi(token, getDtoQueryString(json));
      if (responseImage.status === 200)
        this.setState({
          showModalImage: true,
          id: user.imageDto.id,
          imageBase64:
            responseImage.data.items.length > 0
              ? `data:image/jpeg;base64,${responseImage.data.items[0].binaries}`
              : '',
        });
    } else this.setState({ showModalImage: true, id: 0 });
  }

  closeModalImage = () => {
    this.setState({ showModalImage: false });
  };

  okModalImage = () => {
    this.imageEditorFunc.onClickSave();
  };

  async deleteProfileImage() {
    const { user } = this.props;

    const token = getCookie('token');
    const response = await deleteImageApi(token, user.imageDto.id);
    if (response.status === 200) {
      showNotification('success', '', 'تصویر با موفقیت حذف شد.', 10);
      const responseUser = await getUserApi(
        token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new UserDto({
                fromMobile: true,
              }),
            )
            .includes(['imageDto'])
            .buildJson(),
        ),
        true,
      );

      if (responseUser.status === 200)
        this.props.actions.singleUserSuccess(responseUser.data);
      else this.props.actions.singleUserFailure();
      this.setLoginData('');
      this.setState({ showModalImage: false });
    }
  }

  async onChangeImage(value) {
    this.setState(
      { binariesImage: value.split(',')[1], extention: 'png' },
      () => {
        this.saveImage();
      },
    );
  }

  async saveImage() {
    const { user } = this.props;
    const { binariesImage, extention, id } = this.state;
    const token = getCookie('token');
    this.setState({ showModalImage: false });
    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new ImageDto({
          entityId: user.id,
          binaries: binariesImage,
          extention,
          isCover: true,
          imagetype: 'User',
          id,
        }),
      )
      .build();

    let response = null;
    if (id === 0) response = await postImageApi(token, jsonCrud);
    else response = await putImageApi(token, jsonCrud);

    if (response.status === 200) {
      const responseUser = await getUserApi(
        token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new UserDto({
                fromMobile: true,
              }),
            )
            .includes(['imageDto'])
            .buildJson(),
        ),
        true,
      );

      if (responseUser.status === 200) {
        this.props.actions.singleUserSuccess(responseUser.data);
        this.setLoginData(
          responseUser.data.items.length > 0
            ? responseUser.data.items[0].imageDto.url
            : '',
        );
      } else this.props.actions.singleUserFailure();
    }
  }

  setLoginData = value => {
    const { loginData } = this.props;
    const obj = {
      token: loginData.token,
      userName: loginData.userName,
      displayName: loginData.displayName,
      imageUser: value,
    };

    this.props.actions.loginSuccess(obj);
    setCookie('imageUser', value, 8760);
  };

  render() {
    const { cities, backUrl, user } = this.props;
    const {
      gender,
      firstName,
      lastName,
      phone,
      dayOfBirthDate,
      monthOfBirthDate,
      email,
      cityId,
      errorValid,
      imageBase64,
    } = this.state;
    const sexual = [
      {
        value: 'M',
        name: 'مرد',
        defaultChecked: true,
      },
      {
        value: 'F',
        name: 'زن',
        defaultChecked: false,
      },
    ];
    const dayArray = [];
    for (let i = 1; i < 32; i += 1) {
      dayArray.push(
        <Option value={i} key={i}>
          {i}
        </Option>,
      );
    }
    const cityArray = [];
    cities.map(item =>
      cityArray.push(
        <Option key={item.id} value={item.id}>
          {item.name}
        </Option>,
      ),
    );

    return (
      <div className={s.mainContent}>
        <EditCard title={USER_EDIT} backUrl={backUrl}>
          <div className={s.userInfo}>
            <h3 className="mobileViewTitle">
              <i className="mf-edit" />
              {EDIT_INFORMATION}
            </h3>
            <div className="col-sm-12">
              {user.imageDto ? (
                <div>
                  <img
                    className={s.userImage}
                    alt="avatar"
                    width={64}
                    height={64}
                    src={`${user.imageDto.url}?dummy=${Math.random()}`}
                    onClick={this.showModalImage}
                  />
                </div>
              ) : (
                <img
                  className={s.userImage}
                  alt="avatar"
                  width={64}
                  height={64}
                  src={imageBase64}
                  onClick={this.showModalImage}
                />
                // <i className="cp-user" onClick={this.showModalImage} />
              )}
            </div>
            <div className={s.wrapper}>
              <div className="col-md-12">
                <b className={s.label}>{SEXUAL}</b>
                <CPRadio
                  model={sexual}
                  size="small"
                  onChange={value =>
                    this.onChange('gender', value.target.value)
                  }
                  defaultValue={gender}
                />
              </div>
              <div className="col-md-6">
                <CPValidator
                  value={firstName}
                  checkValidation={this.checkValidation}
                  minLength={2}
                  showMessage={errorValid}
                  name={NAME}
                >
                  <CPInput
                    onChange={value =>
                      this.handleInput('firstName', value.target.value)
                    }
                    label={NAME}
                    value={firstName}
                  />
                </CPValidator>
              </div>
              <div className="col-md-6">
                <CPValidator
                  value={lastName}
                  checkValidation={this.checkValidation}
                  minLength={2}
                  showMessage={errorValid}
                  name={FAMILY_NAME}
                >
                  <CPInput
                    onChange={value =>
                      this.handleInput('lastName', value.target.value)
                    }
                    label={FAMILY_NAME}
                    value={lastName}
                  />
                </CPValidator>
              </div>
              <div className="col-md-6">
                <CPValidator
                  validationEmail
                  value={email}
                  checkValidation={this.checkValidation}
                  showMessage={errorValid}
                  name={EMAIL}
                >
                  <CPInput
                    onChange={value =>
                      this.handleInput('email', value.target.value)
                    }
                    label={EMAIL}
                    value={email}
                  />
                </CPValidator>
              </div>

              <div className="col-md-6">
                <div className="row">
                  <div className="col-12">
                    <label className={s.dateOfBirth}>{BIRTHDAY_DATE}</label>
                  </div>
                  <div className="col-6">
                    <CPSelect
                      onChange={value =>
                        this.handleInput('monthOfBirthDate', value)
                      }
                      value={monthOfBirthDate}
                    >
                      <Option value="1" key="1">
                        {FARVARDIN}
                      </Option>
                      <Option value="2" key="2">
                        {ORDIBEHESHT}
                      </Option>
                      <Option value="3" key="3">
                        {KHORDAD}
                      </Option>
                      <Option value="4" key="4">
                        {TIR}
                      </Option>
                      <Option value="5" key="5">
                        {MORDAD}
                      </Option>
                      <Option value="6" key="6">
                        {SHAHRIVAR}
                      </Option>
                      <Option value="7" key="7">
                        {MEHR}
                      </Option>
                      <Option value="8" key="8">
                        {ABAN}
                      </Option>
                      <Option value="9" key="9">
                        {AZAR}
                      </Option>
                      <Option value="10" key="10">
                        {DEY}
                      </Option>
                      <Option value="11" key="11">
                        {BAHMAN}
                      </Option>
                      <Option value="12" key="12">
                        {ESFAND}
                      </Option>
                    </CPSelect>
                  </div>
                  <div className="col-6">
                    <CPSelect
                      onChange={value =>
                        this.handleInput('dayOfBirthDate', value)
                      }
                      value={dayOfBirthDate}
                    >
                      {dayArray}
                    </CPSelect>
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <CPInput label={MOBILE} value={phone.replace('+98', '0')} />
              </div>

              <div className="col-md-6">
                <label>{CITY}</label>
                <CPSelect
                  onChange={value => this.handleInput('cityId', value)}
                  value={cityId}
                  showSearch
                >
                  {cityArray}
                </CPSelect>
              </div>

              <div className="col-md-12 text-left">
                <CPButton className={s.saveBtn} onClick={this.onSubmit}>
                  {SAVE}
                </CPButton>
              </div>
            </div>
          </div>
        </EditCard>
        <CPModal
          // title="تصویر کاربر"
          handleCancel={this.closeModalImage}
          visible={this.state.showModalImage}
          showFooter={false}
        >
          <CPImageEditor
            image={user.imageDto ? imageBase64 : ''}
            onRef={ref => {
              this.imageEditorFunc = ref;
              return false;
            }}
            onChangeImage={this.onChangeImage}
            width={250}
            height={250}
            onSave={this.okModalImage}
            onDelete={this.deleteProfileImage}
          />
        </CPModal>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  loginData: state.login.data ? state.login.data : [],
  cities: state.cityList.data ? state.cityList.data.items : [],
  user:
    state.singleUser.data && state.singleUser.data.items
      ? state.singleUser.data.items[0]
      : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      singleUserSuccess: singleUserActions.singleUserSuccess,
      singleUserFailure: singleUserActions.singleUserFailure,
      loginSuccess: loginActions.loginSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Profile));
