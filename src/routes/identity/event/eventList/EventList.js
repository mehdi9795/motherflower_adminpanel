import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { message, Select } from 'antd';
import { connect } from 'react-redux';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import CPList from '../../../../components/CP/CPList/CPList';
import CPSwitch from '../../../../components/CP/CPSwitch';
import s from './EventList.css';
import CPButton from '../../../../components/CP/CPButton';
import {
  UserDto,
  UserEvent,
  UserEventType,
  UserRelationShip,
} from '../../../../dtos/identityDtos';
import {
  STATUS,
  ACTIVE,
  DEACTIVATE,
  NAME,
  EVENT_LIST,
  EVENT,
  DAYS_LEFT,
  DATE,
  RELATION,
  CREATE_USERNAME,
  PHONE,
  UPDATE_USERNAME,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT,
  YES,
  NO,
  MONTH,
  EVENT_TYPE,
  DAY,
  ALL,
  USER,
  CREATE_DATE,
  UPDATE_DATE, FARVARDIN, ORDIBEHESHT, ABAN, DEY, AZAR, MORDAD, SHAHRIVAR, TIR, KHORDAD, MEHR, BAHMAN, ESFAND,
} from '../../../../Resources/Localization';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPIntlPhoneInput from '../../../../components/CP/CPIntlPhoneInput';
import userEventPutActions from '../../../../redux/identity/actionReducer/user/UserEventPut';
import userEventDeleteActions from '../../../../redux/identity/actionReducer/user/UserEventDelete';
import eventUserListActions from '../../../../redux/identity/actionReducer/user/UserEventList';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import CPModal from '../../../../components/CP/CPModal';
import CPSelect from '../../../../components/CP/CPSelect';
import CPInput from '../../../../components/CP/CPInput';
import CPPagination from '../../../../components/CP/CPPagination';
import { pressEnterAndCallApplySearch } from '../../../../utils/helper';
import Link from "../../../../components/Link";

const { Option } = Select;

class EventList extends React.Component {
  static propTypes = {
    userEventLoading: PropTypes.bool,
    userEvents: PropTypes.arrayOf(PropTypes.object),
    userEventCount: PropTypes.number,
    userEventTypes: PropTypes.arrayOf(PropTypes.object),
    userRelationShips: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    userEventCount: 0,
    userEventLoading: false,
    userEvents: [],
    userEventTypes: [],
    userRelationShips: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      eventType: '',
      userRelation: '',
      name: '',
      day: 1,
      month: '1',
      id: 0,
      visibleModal: false,
      isValidPhoneNumber: false,
      phoneFinal: '',
      phone: '',
      userId: 0,
      status: false,
      currentPage: 1,
      pageSize: 20,
      searchEventType: '0',
      searchUserRelation: '0',
      searchDay: '0',
      searchMonth: '0',
      searchStatus: '0',
      searchPhoneFinal: '',
      searchIsValidPhoneNumber: false,
      searchPhone: '',
    };
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const data2 = {
      id,
      dto: this.prepareContainerList(),
    };

    this.props.actions.userEventDeleteRequest(data2);
  };

  onEdit = record => {
    this.setState({
      name: record.name,
      day: record.eventDay,
      month: record.eventMonth.toString(),
      eventType: record.eventTypeId.toString(),
      userRelation: record.relationShipId.toString(),
      id: record.key,
      visibleModal: true,
      isValidPhoneNumber: true,
      phone: record.phone.replace('+98', '0'),
      phoneFinal: record.phone,
      userId: record.userId,
      status: record.status,
    });
  };

  onCancel = () => {
    this.setState({ visibleModal: false });
  };

  onSubmit = () => {
    const {
      eventType,
      userRelation,
      name,
      day,
      userId,
      month,
      id,
      isValidPhoneNumber,
      phoneFinal,
      status,
    } = this.state;
    const { userEventTypes, userRelationShips } = this.props;

    if (!isValidPhoneNumber) {
      message.error('لطفا شماره همراه را  درست وارد کنید!', 7);
      return;
    }

    const eventCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserEvent({
          userEventTypeDto: new UserEventType({ id: eventType }),
          userRelationShipDto: new UserRelationShip({ id: userRelation }),
          name,
          eventDay: day,
          eventMonth: month,
          id,
          fromPanel: true,
          phone: phoneFinal,
          published: status,
          userDto: new UserDto({
            id: userId,
          }),
        }),
      )
      .build();

    const data2 = {
      data: eventCrud,
      dto: this.prepareContainerList(),
    };

    this.props.actions.userEventPutRequest(data2);

    this.setState({
      eventType: userEventTypes ? userEventTypes[0].id.toString() : '',
      userRelation: userRelationShips ? userRelationShips[0].id.toString() : '',
      id: 0,
      name: '',
      day: 1,
      month: '1',
      visibleModal: id === 0,
      isValidPhoneNumber: false,
      phone: '',
      phoneFinal: '',
    });
  };

  onChangePagination = page => {
    this.setState({ currentPage: page }, () =>
      this.props.actions.userEventListRequest(this.prepareContainerList()),
    );
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () =>
      this.props.actions.userEventListRequest(this.prepareContainerList()),
    );
  };

  submitSearch = () => {
    this.setState({ currentPage: 1 }, () =>
      this.props.actions.userEventListRequest(this.prepareContainerList()),
    );
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneFinal: number.replace(/ /g, ''),
        phone: value,
        isValidPhoneNumber: status,
      });
    }
  };

  changeNumberSearch = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        searchPhoneFinal: number.replace(/ /g, ''),
        searchPhone: value,
        searchIsValidPhoneNumber: status,
      });
    }
  };

  handleChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  changeStatus = (value, record) => {
    const eventCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserEvent({
          userEventTypeDto: new UserEventType({ id: record.eventTypeId }),
          userRelationShipDto: new UserRelationShip({
            id: record.relationShipId,
          }),
          name: record.name,
          eventDay: record.eventDay,
          eventMonth: record.eventMonth,
          id: record.key,
          fromPanel: true,
          phone: record.phone,
          userDto: new UserDto({ id: record.userId }),
          published: value,
        }),
      )
      .build();

    const eventList = new BaseGetDtoBuilder()
      .all(true)
      .orderByFields(['createdOnUtc'])
      .orderByDescending(true)
      .includes(['userEventTypeDto', 'userRelationShipDto'])
      .buildJson();

    const data2 = {
      data: eventCrud,
      dto: eventList,
    };
    this.props.actions.userEventPutRequest(data2);
  };

  prepareContainerList = () => {
    const {
      pageSize,
      currentPage,
      searchDay,
      searchEventType,
      searchIsValidPhoneNumber,
      searchMonth,
      searchPhoneFinal,
      searchStatus,
      searchUserRelation,
    } = this.state;

    if (!searchIsValidPhoneNumber && searchPhoneFinal.length > 0) {
      message.error('لطفا موبایل را صحیح وارد نمایید!', 7);
    }

    let searchPublished;

    if (searchStatus === '1') searchPublished = true;
    if (searchStatus === '2') searchPublished = false;

    const eventList = new BaseGetDtoBuilder()
      .dto(
        new UserEvent({
          eventDay: searchDay === '0' ? undefined : searchDay,
          eventMonth: searchMonth === '0' ? undefined : searchMonth,
          userEventTypeDto:
            searchEventType === '0'
              ? undefined
              : new UserEventType({ id: searchEventType }),
          userRelationShipDto:
            searchUserRelation === '0'
              ? undefined
              : new UserRelationShip({ id: searchUserRelation }),
          searchPublished,
          userDto:
            searchPhoneFinal.length === 0
              ? undefined
              : new UserDto({ phone: searchPhoneFinal }),
          fromPanel: true,
        }),
      )
      .pageIndex(currentPage - 1)
      .pageSize(pageSize)
      .orderByFields(['UpdatedOnUtc'])
      .orderByDescending(true)
      .includes(['userEventTypeDto', 'userRelationShipDto', 'userDto'])
      .buildJson();

    return eventList;
  };

  render() {
    const {
      userEventLoading,
      userEventCount,
      userEvents,
      userRelationShips,
      userEventTypes,
    } = this.props;
    const {
      eventType,
      userRelation,
      name,
      day,
      month,
      searchEventType,
      searchUserRelation,
      searchDay,
      searchMonth,
      searchPhone,
      visibleModal,
      phone,
      currentPage,
      searchStatus,
    } = this.state;

    const columns = [
      {
        title: USER,
        dataIndex: 'user',
        key: 'user',
        width: 150,
        fixed: 'right',
        render: (text, record) => <Link to={`/user/edit/${record.userId}`} target="_blank" >{record.user}</Link>
      },
      {
        title: EVENT,
        dataIndex: 'eventType',
        key: 'eventType',
        width: 80,
        fixed: 'right',
      },
      {
        title: RELATION,
        dataIndex: 'relationShip',
        key: 'relationShip',
        width: 100,
        fixed: 'right',
      },
      {
        title: DATE,
        dataIndex: 'date',
        key: 'date',
        width: 120,
        fixed: 'right',
      },
      {
        title: DAYS_LEFT,
        dataIndex: 'daysLeft',
        key: 'daysLeft',
        width: 150,
        fixed: 'right',
      },
      {
        title: NAME,
        dataIndex: 'name',
        key: 'name',
        width: 150,
      },
      {
        title: PHONE,
        dataIndex: 'phone',
        key: 'phone',
        width: 100,
        render: (text, record) => (
          <span>{record.phone ? record.phone.replace('+98', '0') : ''}</span>
        ),
      },
      {
        title: CREATE_DATE,
        dataIndex: 'persianCreatedOnUtc',
        key: 'persianCreatedOnUtc',
      },
      {
        title: UPDATE_DATE,
        dataIndex: 'persianUpdatedOnUtc',
        key: 'persianUpdatedOnUtc',
      },
      {
        title: CREATE_USERNAME,
        dataIndex: 'userCreated',
        key: 'userCreated',
      },
      {
        title: UPDATE_USERNAME,
        dataIndex: 'userUpdated',
        key: 'userUpdated',
      },
      {
        title: STATUS,
        dataIndex: 'status',
        key: 'status',
        width: 100,
        fixed: 'left',
        render: (text, record) => (
          <CPSwitch
            unCheckedChildren={DEACTIVATE}
            checkedChildren={ACTIVE}
            onChange={value => this.changeStatus(value, record)}
            defaultChecked={record.status}
          />
        ),
      },
      {
        title: '',
        dataIndex: 'Delete',
        width: 50,
        fixed: 'left',
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Edit',
        width: 50,
        fixed: 'left',
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.onEdit(record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </CPButton>
          </div>
        ),
      },
    ];

    const eventsDataSource = [];
    const userEventTypeDataSource = [];
    const userRelationShipDataSource = [];
    const dayDataSource = [];

    userEvents.map(item => {
      const obj = {
        key: item.id,
        eventType: item.userEventTypeDto.title,
        relationShip: item.userRelationShipDto.title,
        name: item.name,
        date: item.persinaDayMonth,
        daysLeft: item.leftOverDay,
        eventDay: item.eventDay,
        eventMonth: item.eventMonth,
        eventTypeId: item.userEventTypeDto.id,
        relationShipId: item.userRelationShipDto.id,
        phone: item.phone,
        userCreated: item.createdUerDto.displayName,
        userUpdated: item.updatedUerDto.displayName,
        status: item.published,
        userId: item.userDto.id,
        user: item.userDto ? item.userDto.displayName : '',
        persianUpdatedOnUtc: item.persianUpdatedOnUtc,
        persianCreatedOnUtc: item.persianCreatedOnUtc,
      };
      eventsDataSource.push(obj);
      return null;
    });

    /**
     * map userRelationShips for comboBox Datasource
     */
    if (userRelationShips)
      userRelationShips.map(item =>
        userRelationShipDataSource.push(
          <Option key={item.id.toString()}>{item.title}</Option>,
        ),
      );

    /**
     * map userEventTypes for comboBox Datasource
     */
    if (userEventTypes)
      userEventTypes.map(item =>
        userEventTypeDataSource.push(
          <Option key={item.id.toString()}>{item.title}</Option>,
        ),
      );

    for (let item = 1; item <= 31; item += 1) {
      dayDataSource.push(
        <Option key={item.toString()}>{item.toString()}</Option>,
      );
    }

    return (
      <div className={s.mainContent}>
        <h3>{EVENT_LIST}</h3>
        <CPList
          data={eventsDataSource}
          columns={columns}
          onAddClick={this.showForm}
          loading={userEventLoading}
          count={userEventCount}
          hideAdd
          pagination={false}
          scroll={{ x: 1900 }}
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-xs-12 col-sm-6 col-lg-2">
                  <label>{EVENT_TYPE}</label>
                  <CPSelect
                    value={searchEventType}
                    onChange={value =>
                      this.handleChangeInput('searchEventType', value)
                    }
                  >
                    <Option key="0">{ALL}</Option>
                    {userEventTypeDataSource}
                  </CPSelect>
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-2">
                  <label>{RELATION}</label>
                  <CPSelect
                    value={searchUserRelation}
                    onChange={value =>
                      this.handleChangeInput('searchUserRelation', value)
                    }
                  >
                    <Option key="0">{ALL}</Option>
                    {userRelationShipDataSource}
                  </CPSelect>
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-2">
                  <label>{PHONE}</label>
                  <CPIntlPhoneInput
                    value={searchPhone}
                    onChange={this.changeNumberSearch}
                  />
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-2">
                  <label>{DAY}</label>
                  <CPSelect
                    value={searchDay}
                    onChange={value =>
                      this.handleChangeInput('searchDay', value)
                    }
                    showSearch
                  >
                    <Option key="0">{ALL}</Option>
                    {dayDataSource}
                  </CPSelect>
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-2">
                  <label>{MONTH}</label>
                  <CPSelect
                    value={searchMonth}
                    onChange={value =>
                      this.handleChangeInput('searchMonth', value)
                    }
                    showSearch
                  >
                    <Option key="0">{ALL}</Option>
                    <Option value="1" key="1">
                      {FARVARDIN}
                    </Option>
                    <Option value="2" key="2">
                      {ORDIBEHESHT}
                    </Option>
                    <Option value="3" key="3">
                      {KHORDAD}
                    </Option>
                    <Option value="4" key="4">
                      {TIR}
                    </Option>
                    <Option value="5" key="5">
                      {MORDAD}
                    </Option>
                    <Option value="6" key="6">
                      {SHAHRIVAR}
                    </Option>
                    <Option value="7" key="7">
                      {MEHR}
                    </Option>
                    <Option value="8" key="8">
                      {ABAN}
                    </Option>
                    <Option value="9" key="9">
                      {AZAR}
                    </Option>
                    <Option value="10" key="10">
                      {DEY}
                    </Option>
                    <Option value="11" key="11">
                      {BAHMAN}
                    </Option>
                    <Option value="12" key="12">
                      {ESFAND}
                    </Option>
                  </CPSelect>
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-2">
                  <label>{STATUS}</label>
                  <CPSelect
                    value={searchStatus}
                    onChange={value =>
                      this.handleChangeInput('searchStatus', value)
                    }
                  >
                    <Option key="0">{ALL}</Option>
                    <Option key="1">{ACTIVE}</Option>
                    <Option key="2">{DEACTIVATE}</Option>
                  </CPSelect>
                </div>
              </div>
            </div>
            <div className={s.field}>
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CPList>
        <CPPagination
          total={userEventCount}
          current={currentPage}
          onChange={this.onChangePagination}
          onShowSizeChange={this.onShowSizeChange}
          defaultPageSize={20}
        />
        <CPModal
          visible={visibleModal}
          handleOk={this.onSubmit}
          handleCancel={this.onCancel}
          title={EVENT}
        >
          <div className="row eventForm">
            <div className="col-sm-12 col-lg-6">
              <label>{EVENT_TYPE}</label>
              <CPSelect
                value={eventType}
                onChange={value => this.handleChangeInput('eventType', value)}
                className={s.comboBox}
              >
                {userEventTypeDataSource}
              </CPSelect>
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{RELATION}</label>
              <CPSelect
                value={userRelation}
                onChange={value =>
                  this.handleChangeInput('userRelation', value)
                }
                className={s.comboBox}
              >
                {userRelationShipDataSource}
              </CPSelect>
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{NAME}</label>
              <CPInput
                value={name}
                onChange={value =>
                  this.handleChangeInput('name', value.target.value)
                }
                className={s.inputName}
              />
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{PHONE}</label>
              <CPIntlPhoneInput value={phone} onChange={this.changeNumber} />
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{DAY}</label>
              <CPSelect
                value={day}
                onChange={value => this.handleChangeInput('day', value)}
                className={s.comboBox}
                showSearch
              >
                {dayDataSource}
              </CPSelect>
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{MONTH}</label>
              <CPSelect
                value={month}
                onChange={value => this.handleChangeInput('month', value)}
                className={s.comboBox}
                showSearch
              >
                <Option value="1" key="1">
                  {FARVARDIN}
                </Option>
                <Option value="2" key="2">
                  {ORDIBEHESHT}
                </Option>
                <Option value="3" key="3">
                  {KHORDAD}
                </Option>
                <Option value="4" key="4">
                  {TIR}
                </Option>
                <Option value="5" key="5">
                  {MORDAD}
                </Option>
                <Option value="6" key="6">
                  {SHAHRIVAR}
                </Option>
                <Option value="7" key="7">
                  {MEHR}
                </Option>
                <Option value="8" key="8">
                  {ABAN}
                </Option>
                <Option value="9" key="9">
                  {AZAR}
                </Option>
                <Option value="10" key="10">
                  {DEY}
                </Option>
                <Option value="11" key="11">
                  {BAHMAN}
                </Option>
                <Option value="12" key="12">
                  {ESFAND}
                </Option>
              </CPSelect>
            </div>
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userEvents: state.userEventList.data ? state.userEventList.data.items : [],
  userEventCount: state.userEventList.data ? state.userEventList.data.count : 0,
  userEventLoading: state.userEventList.loading,
  userEventTypes: state.userEventTypeList.data
    ? state.userEventTypeList.data.items
    : [],
  userRelationShips: state.userRelationShipList.data
    ? state.userRelationShipList.data.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
      showLoading,
      userEventPutRequest: userEventPutActions.userEventPutRequest,
      userEventDeleteRequest: userEventDeleteActions.userEventDeleteRequest,
      userEventListRequest: eventUserListActions.userEventListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(EventList));
