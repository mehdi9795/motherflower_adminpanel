import React from 'react';
import EventList from './EventList';
import Layout from '../../../../components/Template/Layout/Layout';
import eventUserListActions from '../../../../redux/identity/actionReducer/user/UserEventList';
import userEventTypeListActions from '../../../../redux/identity/actionReducer/user/UserEventTypeList';
import userRelationShipListActions from '../../../../redux/identity/actionReducer/user/UserRelationShipList';
import { EVENT_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  // store.dispatch(showLoading());
  /**
   * get all event users
   */
  const jsonListEventUser = new BaseGetDtoBuilder()
    .all(true)
    .orderByFields(['UpdatedOnUtc'])
    .orderByDescending(true)
    .pageIndex(0)
    .pageSize(20)
    .includes(['userEventTypeDto', 'userRelationShipDto', 'userDto'])
    .buildJson();
  store.dispatch(eventUserListActions.userEventListRequest(jsonListEventUser));

  store.dispatch(
    userEventTypeListActions.userEventTypeListRequest(
      JSON.stringify({ dto: {} }),
    ),
  );
  store.dispatch(
    userRelationShipListActions.userRelationShipListRequest(
      new BaseGetDtoBuilder()
        .dto({ published: true })
        .orderByFields(['DisplayOrder'])
        .buildJson(),
    ),
  );

  return {
    chunks: ['userEventList'],
    title: `${EVENT_LIST}`,
    component: (
      <Layout>
        <EventList />
      </Layout>
    ),
  };
}

export default action;
