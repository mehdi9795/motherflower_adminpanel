import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { hideLoading } from 'react-redux-loading-bar';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ChangePassword.css';
import {
  SAVE,
  NEW_PASSWORD,
  LAST_PASSWORD,
  REENTER_NEW_PASSWORD,
  CHANGE_PASSWORD,
} from '../../../Resources/Localization';
import CPInput from '../../../components/CP/CPInput';
import CPButton from '../../../components/CP/CPButton';
import { ChangePasswordDto } from '../../../dtos/identityDtos';
import { getCookie, setCookie } from '../../../utils';
import { ChangePasswordAuthApi } from '../../../services/identityApi';
import loginActions from '../../../redux/identity/actionReducer/account/Login';
import { showNotification } from '../../../utils/helper';
import loadingActions from '../../../redux/shared/actionReducer/loading/loading';
import { BaseCRUDDtoBuilder } from '../../../dtos/dtoBuilder';

class ChangePassword extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      repeatPassword: '',
      message: '',
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  async onSubmit() {
    const { oldPassword, newPassword, repeatPassword } = this.state;
    const token = getCookie('token');

    if (
      oldPassword.length > 0 &&
      newPassword.length > 0 &&
      repeatPassword.length > 0
    ) {
      if (newPassword.length >= 5)
        if (newPassword === repeatPassword) {
          this.props.actions.loadingShowRequest();

          const jsonCrud = new BaseCRUDDtoBuilder()
            .dto(
              new ChangePasswordDto({
                currentPassword: oldPassword,
                newPassword,
                newConfirmPassword: repeatPassword,
              }),
            )
            .build();

          const response = await ChangePasswordAuthApi(token, jsonCrud);
          this.props.actions.loadingHideRequest();
          if (response.status === 200) {
            const authenticationCookieExpireHours = 8760;
            setCookie(
              'siteToken',
              response.data.sessionId,
              authenticationCookieExpireHours,
            );
            setCookie(
              'siteUserName',
              response.data.userName,
              authenticationCookieExpireHours,
            );
            
            setCookie(
              'siteDisplayName',
              response.data.displayName,
              authenticationCookieExpireHours,
            );

            const obj = {
              token: response.data.sessionId,
              userName: response.data.userName,
              displayName: response.data.displayName,
            };
            this.props.actions.loginSuccess(obj);
            showNotification(
              'success',
              '',
              'کلمه عبور با موفقیت ویرایش شد.',
              10,
            );
          } else {
            this.props.actions.loginFailure();
            showNotification('error', '', response.data.errorMessage, 10);
          }
        } else {
          this.setState({ message: 'رمز عبور با تکرار رمز عبور برابر نیست.' });
        }
      else this.setState({ message: 'رمز عبور حداقل باید 5 کاراکتر باشد.' });
    } else {
      this.setState({ message: 'لطفا تمام فیلد ها را پر کنید.' });
    }
  }

  handleInput = (inputName, value) => {
    this.setState({ [inputName]: value, message: '' });
  };

  render() {
    const { message, oldPassword, newPassword, repeatPassword } = this.state;
    return (
      <div className={s.changePass}>
        <h3 className="mobileViewTitle">
          <i className="mf-lock" />
          {CHANGE_PASSWORD}
        </h3>

        <div className={s.wrapper}>
          <div className={s.input}>
            {message && <p style={{ color: 'red' }}>{message}</p>}
            <CPInput
              onChange={value =>
                this.handleInput('oldPassword', value.target.value)
              }
              label={LAST_PASSWORD}
              value={oldPassword}
            />
          </div>
          <div className={s.input}>
            <CPInput
              onChange={value =>
                this.handleInput('newPassword', value.target.value)
              }
              label={NEW_PASSWORD}
              value={newPassword}
            />
          </div>
          <div className={s.input}>
            <CPInput
              onChange={value =>
                this.handleInput('repeatPassword', value.target.value)
              }
              label={REENTER_NEW_PASSWORD}
              value={repeatPassword}
            />
          </div>
          <div className={s.input}>
            <CPButton className={s.saveBtn} onClick={this.onSubmit}>
              {SAVE}
            </CPButton>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginSuccess: loginActions.loginSuccess,
      loginFailure: loginActions.loginFailure,
      loadingHideRequest: loadingActions.editFormLoadingHideRequest,
      loadingShowRequest: loadingActions.editFormLoadingShowRequest,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ChangePassword));
