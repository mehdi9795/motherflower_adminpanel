import React from 'react';
import Layout from '../../../components/Template/Layout/Layout';
import ChangePassword from './ChangePassword';
import { PAGE_TITLE_CHANGE_PASSWORD } from '../../../Resources/Localization';

async function action() {
  // store.dispatch(showLoading());

  return {
    chunks: ['changePassword'],
    title: `${PAGE_TITLE_CHANGE_PASSWORD}`,
    component: (
      <Layout>
        <ChangePassword />
      </Layout>
    ),
  };
}

export default action;
