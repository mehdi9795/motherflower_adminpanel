import React from 'react';
import UserEdit from './UserEdit';
import Layout from '../../../../components/Template/Layout/Layout';
import singleUserActions from '../../../../redux/identity/actionReducer/user/SingleUser';
import singleUserInitActions from '../../../../redux/identity/actionReducer/user/SingleUserInit';
import { UserDto } from '../../../../dtos/identityDtos';
import { AgentDto } from '../../../../dtos/vendorDtos';
import { getDtoQueryString } from '../../../../utils/helper';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import { getUserApi } from '../../../../services/identityApi';
import { getAgentApi } from '../../../../services/vendorApi';
import { GetWalletsApi } from '../../../../services/samApi';
import { PAGE_TITLE_USER_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import walletListActions from '../../../../redux/sam/actionReducer/wallet/List';
import { getCityApi } from '../../../../services/commonApi';
import { WalletDto } from '../../../../dtos/samDtos';

async function action({ store, params, query }) {
  try {
    store.dispatch(singleUserInitActions.singleUserInitRequest());

    const user = await getUserApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new UserDto({
              id: params.id,
              active: true,
            }),
          )
          .buildJson(),
      ),
    );
    /**
     * get all vendors with true status
     */
    const vendors = await getAgentApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .all(true)
          .includes(['VendorBranches'])
          .buildJson(),
      ),
    );

    /**
     * get all city with true status
     */
    const cities = await getCityApi(
      store.getState().login.data.token,
      getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
    );

    if (
      user.status === 200 &&
      vendors.status === 200 &&
      cities.status === 200
    ) {
      store.dispatch(singleUserActions.singleUserSuccess(user.data));
      store.dispatch(vendorListActions.vendorListSuccess(vendors.data));
      store.dispatch(cityListActions.cityListSuccess(cities.data));
    } else {
      store.dispatch(singleUserInitActions.singleUserInitFailure());
    }

    const responseWalletUser = await GetWalletsApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new WalletDto({
              userDto: new UserDto({ id: params.id }),
              fromPanel: true,
            }),
          )
          .buildJson(),
      ),
    );
    if (responseWalletUser.status === 200)
      store.dispatch(
        walletListActions.walletListSuccess(responseWalletUser.data),
      );
  } catch (error) {
    store.dispatch(singleUserInitActions.singleUserInitFailure());
  }

  const backUrl =
    query.backTo === 'vendorBranch'
      ? `/vendor-branch/edit/${query.vendorBranchId}?tab=4`
      : '/user/list';

  return {
    chunks: ['userEdit'],
    title: `${PAGE_TITLE_USER_EDIT}`,
    component: (
      <Layout>
        <UserEdit backUrl={backUrl} vendorBranchId={query.vendorBranchId} />
      </Layout>
    ),
  };
}

export default action;
