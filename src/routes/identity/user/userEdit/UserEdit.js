import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './UserEdit.css';
import UserForm from '../../../../components/Identity/User/UserForm/UserForm';
import history from '../../../../history';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_USER,
  USER_EDIT,
} from '../../../../Resources/Localization';
import userAddressListActions from '../../../../redux/identity/actionReducer/user/UserAddressList';
import userDeleteActions from '../../../../redux/identity/actionReducer/user/UserDelete';
import userRoleListActions from '../../../../redux/identity/actionReducer/user/UserRoleList';
import roleActions from '../../../../redux/identity/actionReducer/role/Role';
import { UserDto, UserRoleDto } from '../../../../dtos/identityDtos';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

class UserEdit extends React.Component {
  static propTypes = {
    user: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    backUrl: PropTypes.string,
    vendorBranchId: PropTypes.string,
  };

  static defaultProps = {
    backUrl: '/user/list',
    user: null,
    vendorBranchId: '',
  };

  constructor(props) {
    super(props);
    this.userRoleIds = [];
  }

  async componentWillMount() {
    const { user } = this.props;
    /**
     * get all user Role with user id
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserRoleDto({
          userDto: new UserDto({ id: user.id }),
        }),
      )
      .buildJson();
    this.props.actions.userRoleRequest(jsonList);

  }

  delete = () => {
    const { user } = this.props;

    const deleteData = {
      id: user.id,
      status: 'edit',
    };

    this.props.actions.userDeleteRequest(deleteData);
  };

  redirect = () => {
    history.push('/user/list');
  };

  render() {
    const { user, backUrl, vendorBranchId } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard
          title={` ${USER_EDIT}  ${user && user.firstName} ${user &&
            user.lastName}`}
          backUrl={backUrl}
          isEdit={!!(user && user.id)}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_USER}
          onDelete={this.delete}
        >
          <UserForm
            user={user}
            backUrlAndVendorId={{ backUrl }}
            vendorBranchId={vendorBranchId}
          />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.singleUser.data ? state.singleUser.data.items && state.singleUser.data.items[0] : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      userRoleRequest: userRoleListActions.userRoleRequest,
      roleListRequest: roleActions.roleListRequest,
      userDeleteRequest: userDeleteActions.userDeleteRequest,
      userAddressListRequest: userAddressListActions.userAddressListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(UserEdit));
