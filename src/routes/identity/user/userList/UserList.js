import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import HotKeys from 'react-hot-keys';
import { Select } from 'antd';
import CPList from '../../../../components/CP/CPList/CPList';
import CPSwitch from '../../../../components/CP/CPSwitch';
import s from './UserList.css';
import Link from '../../../../components/Link/Link';
import CPButton from '../../../../components/CP/CPButton';
import CPInput from '../../../../components/CP/CPInput';
import history from '../../../../history';
import CPMultiSelect from '../../../../components/CP/CPMultiSelect/CPMultiSelect';
import CPModal from '../../../../components/CP/CPModal';
import { UserDto, UserRoleDto, RoleDto } from '../../../../dtos/identityDtos';
import {
  USER_FULL_NAME,
  EMAIL,
  STATUS,
  USER_USERNAME,
  USER_FIRST_NAME,
  USER_LAST_NAME,
  USER_USER_ROLE,
  ACTIVE,
  DEACTIVATE,
  ROLE_SELECT_PLACEHOLDER,
  YES,
  NO,
  USER_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_USER,
  NAME,
  SENDING_CODE_DISCOUNT,
  FROM_DATE,
  TO_DATE,
  ARE_YOU_SURE_WANT_TO_SEND_DISCOUNT_CODE,
  PHONE,
  CREATE_DATE,
  PHONE_NUMBER,
  ENTER_EXPIRE_DATE,
  LOGIN_HOUR,
  REAGENT_USER,
  ALL,
  ONLINE,
  OFFLINE,
  BLOCK,
  SYSTEM,
  WAS_RESPONSE,
  SELECT_STATUS_EVENT,
  WAS_NOT_RESPONSE,
  UN_WILLINGNESS,
  CITY,
} from '../../../../Resources/Localization';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import userListActions from '../../../../redux/identity/actionReducer/user/List';
import userDeleteActions from '../../../../redux/identity/actionReducer/user/UserDelete';
import promotionListActions from '../../../../redux/sam/actionReducer/promotion/PromotionList';
import {
  PromotionDto,
  SendDiscountCodeGroupDto,
  VendorBranchPromotionDto,
} from '../../../../dtos/samDtos';
import CPConfirmation from '../../../../components/CP/CPConfirmation';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import { postSendToUserApi } from '../../../../services/samApi';
import { message } from 'antd';
import { getCookie } from '../../../../utils';
import CPIntlPhoneInput from '../../../../components/CP/CPIntlPhoneInput';
import CPPagination from '../../../../components/CP/CPPagination';
import CPAvatar from '../../../../components/CP/CPAvatar';
import CPPersianCalendar from '../../../../components/CP/CPPersianCalendar';
import {
  pressEnterAndCallApplySearch,
  showNotification,
} from '../../../../utils/helper';
import CPSelect from '../../../../components/CP/CPSelect';
import { CityDto } from '../../../../dtos/commonDtos';

const { Option } = Select;

class UserList extends React.Component {
  static propTypes = {
    userLoading: PropTypes.bool,
    users: PropTypes.arrayOf(PropTypes.object),
    roles: PropTypes.arrayOf(PropTypes.object),
    userCount: PropTypes.number,
    promotionLoading: PropTypes.bool,
    promotionCount: PropTypes.number,
    promotions: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    userCount: 0,
    userLoading: false,
    users: [],
    roles: [],
    promotions: [],
    cities: [],
    promotionLoading: false,
    promotionCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedRoles: [],
      firstName: '',
      lastName: '',
      email: '',
      userStatus: true,
      userName: '',
      visibleModal: false,
      visibleConfirm: false,
      userId: 0,
      promotionId: 0,
      phoneFinal: '',
      phoneNumber: '',
      currentPage: 1,
      pageSize: 30,
      persianToDateExpire: '',
      visibleDiscountCodeGroupModal: false,
      promotionTypeId: 0,
      searchReagentUser: false,
      searchUserEventStatus: 0,
      searchOffline: '0',
      searchCityId: 0,
    };
    this.submitSearch = this.submitSearch.bind(this);
    this.handleOkConfirm = this.handleOkConfirm.bind(this);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  /**
   * Change state when change input value
   * @param value
   */
  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  /**
   * Change selected roles.
   * @param value
   */
  roleChange = value => {
    this.setState({
      selectedRoles: value,
    });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  /**
   * Search users based on user selected values.
   * @returns {Promise<void>}
   */
  async submitSearch() {
    this.setState({ currentPage: 1 }, () => {
      this.props.actions.userListRequest(this.prepareContainer());
    });
  }

  prepareContainer = () => {
    const {
      userStatus,
      firstName,
      lastName,
      email,
      selectedRoles,
      phoneFinal,
      pageSize,
      currentPage,
      searchReagentUser,
      searchUserEventStatus,
      searchOffline,
      searchCityId,
    } = this.state;
    const userRoleDtos = [];

    let offline;
    if (searchOffline === '2') offline = true;
    else if (searchOffline === '1') offline = false;

    selectedRoles.map(roleId => {
      userRoleDtos.push(
        new UserRoleDto({ roleDto: new RoleDto({ id: roleId }) }),
      );
      return null;
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserDto({
          active: userStatus,
          firstName,
          lastName,
          userName: phoneFinal,
          email,
          userRoleDto: userRoleDtos,
          searchUserEventStatus,
          searchOffline: offline,
          searchReagentUser: searchReagentUser === false ? undefined : true,
          cityDto:
            searchCityId === 0 ? undefined : new CityDto({ id: searchCityId }),
        }),
      )
      .pageIndex(currentPage - 1)
      .pageSize(pageSize)
      .includes(['Roles', 'reagentUserDto'])
      .orderByFields(['createdOnUtc'])
      .orderByDescending(true)
      .buildJson();

    return jsonList;
  };

  /**
   * user list add button click handler
   */
  userAddClick = () => {
    this.props.actions.showLoading();
    history.push('/user/create');
  };

  delete = id => {
    /**
     * get all users
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserDto({
          userRoleDto: [new UserRoleDto({ roleDto: new RoleDto({ id: 1 }) })],
          active: true,
        }),
      )
      .orderByFields(['createdOnUtc'])
      .orderByDescending(true)
      .includes(['Roles'])
      .buildJson();

    const deleteData = {
      id,
      status: 'list',
      listDto: jsonList,
    };

    this.props.actions.userDeleteRequest(deleteData);
  };

  handleCancelModal = () => {
    this.setState({ visibleModal: false });
  };

  showModalDiscount = userId => {
    this.props.actions.promotionListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new VendorBranchPromotionDto({
            active: true,
            searchPromotionTypes: [3, 6, 7],
          }),
        )
        .buildJson(),
    );

    this.setState({ visibleModal: true, userId });
  };

  async handleOkConfirm() {
    const {
      promotionId,
      promotionTypeId,
      persianToDateExpire,
      userId,
    } = this.state;
    const token = getCookie('token');
    this.setState({
      visibleConfirm: false,
      visibleDiscountCodeGroupModal: false,
    });
    const response = await postSendToUserApi(
      token,
      new SendDiscountCodeGroupDto({
        userId,
        promotionDto: new PromotionDto({
          id: promotionId,
          promotionType: promotionTypeId,
          persianToDateUtc:
            persianToDateExpire.length > 0 ? persianToDateExpire : undefined,
        }),
      }),
    );
    if (response.status === 200)
      message.success('کد تخفیف با موفقیت ارسال شد', 5);
    else message.error(response.data.errorMessage, 5);
  }

  handleCancelConfirm = () => {
    this.setState({ visibleConfirm: false });
  };

  showModalConfirm = (promotionId, promotionTypeId) => {
    if (promotionTypeId === 6)
      this.setState({ visibleConfirm: true, promotionId, promotionTypeId });
    else
      this.setState({
        visibleDiscountCodeGroupModal: true,
        promotionId,
        promotionTypeId,
      });
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneFinal: number.replace(/ /g, ''),
        phoneNumber: value,
      });
    }
  };

  onChangePagination = page => {
    this.setState({ currentPage: page }, () =>
      this.props.actions.userListRequest(this.prepareContainer()),
    );
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () =>
      this.props.actions.userListRequest(this.prepareContainer()),
    );
  };

  handleCancelDiscountCodeGroupModal = () => {
    this.setState({ visibleDiscountCodeGroupModal: false });
  };

  handleOkDiscountCodeGroupModal = () => {
    const { persianToDateExpire } = this.state;

    if (persianToDateExpire.length > 0) {
      this.setState({ visibleConfirm: true });
    } else
      showNotification('error', '', 'لطفا تاریخ انقضاء را انتخاب کنید.', 7);
  };

  render() {
    const {
      userLoading,
      users,
      roles,
      userCount,
      promotions,
      promotionLoading,
      promotionCount,
      cities,
    } = this.props;
    const {
      userStatus,
      selectedRoles,
      lastName,
      firstName,
      userName,
      email,
      visibleModal,
      visibleConfirm,
      phoneNumber,
      currentPage,
      persianToDateExpire,
      visibleDiscountCodeGroupModal,
      searchReagentUser,
      searchUserEventStatus,
      searchOffline,
      searchCityId,
    } = this.state;
    const columns = [
      {
        title: USER_FULL_NAME,
        dataIndex: 'fullName',
        key: 'firstName',
        width: 150,
        render: (text, record) => (
          <Link
            to={`/user/edit/${record.key}`}
            onClick={() => {
              this.props.actions.showLoading();
            }}
            className="userName"
          >
            {record.fullName}
          </Link>
        ),
      },
      {
        title: '',
        dataIndex: 'isCity',
        key: 'isCity',
        width: 10,
        render: (text, record) => (
          <div>
            {record.isCity && (
              <img src="/images/city.png" alt="aa" width={15} />
            )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'alert',
        key: 'alert',
        width: 10,
        render: (text, record) => (
          <div>
            {record.userEventStatusId === 1 && (
              <img src="/images/WasResponsive.png" alt="aa" width={15} />
            )}
            {record.userEventStatusId === 2 && (
              <img src="/images/WasNotResponsive.png" alt="aa" width={15} />
            )}
            {record.userEventStatusId === 3 && (
              <img src="/images/Unwillingness.png" alt="aa" width={15} />
            )}
            {record.userEventStatusId === 4 && (
              <img src="/images/system.png" alt="aa" width={15} />
            )}
            {record.userEventStatusId === 5 && (
              <img src="/images/block.png" alt="aa" width={15} />
            )}

          </div>
        ),
      },
      { title: PHONE, dataIndex: 'phone', key: 'phone', width: 100 },
      {
        title: USER_USER_ROLE,
        dataIndex: 'userRole',
        key: 'userRole',
        width: 150,
      },
      {
        title: REAGENT_USER,
        dataIndex: 'reagentUser',
        key: 'reagentUser',
        width: 100,
      },
      {
        title: CREATE_DATE,
        dataIndex: 'createDate',
        key: 'createDate',
        width: 80,
      },
      {
        title: LOGIN_HOUR,
        dataIndex: 'signInFromWeb',
        key: 'signInFromWeb',
        width: 190,
        render: (text, record) => (
          <div>
            <img
              alt=""
              width={30}
              src={
                record.signInFromWeb ? '/images/pc.png' : '/images/mobile.png'
              }
            />
            <span>{record.loginHour}</span>
          </div>
        ),
      },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
        width: 50,
      },
      {
        title: '',
        dataIndex: 'offline',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.offline} checkedChildren={OFFLINE} unCheckedChildren={ONLINE} />
          </div>
        ),
        width: 80,
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 20,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_USER}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.delete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationThree',
        width: 50,
        render: (text, record) => (
          <CPButton
            className="btn-primary"
            onClick={() => this.showModalDiscount(record.key)}
          >
            {SENDING_CODE_DISCOUNT}
          </CPButton>
        ),
      },
    ];
    const discountModalColumns = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      {
        title: FROM_DATE,
        dataIndex: 'persianFromDateUtc',
        key: 'persianFromDateUtc',
      },
      {
        title: TO_DATE,
        dataIndex: 'persianToDateUtc',
        key: 'persianToDateUtc',
      },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operation',
        render: (text, record) => (
          <CPButton
            className="btn-primary"
            onClick={() =>
              this.showModalConfirm(record.id, record.promotionTypeId)
            }
          >
            {SENDING_CODE_DISCOUNT}
          </CPButton>
        ),
      },
    ];
    const roleNameArray = [];
    const cityArray = [];
    const userArray = [];

    /**
     * map users for user list
     */
    if (users)
      users.map(item => {
        const userRoleArray = [];
        item.userRoleDto.map(userRole =>
          userRoleArray.push(userRole.roleDto.name),
        );
        const obj = {
          key: item.id,
          fullName: `${item.firstName}  ${item.lastName}`,
          email: item.email,
          phone: item.phone.replace('+98', '0'),
          active: item.active,
          userRole: userRoleArray.join(),
          createDate: item.persianCreatedOnUtc,
          userEventStatusId: item.userEventStatusId,
          signInFromWeb: item.signInFromWeb,
          loginHour: item.lastLoginDateTime,
          reagentUser: item.reagentUserDto
            ? item.reagentUserDto.displayName
            : '',
          isCity: !!item.cityDto,
          offline: item.offline,
        };
        return userArray.push(obj);
      });
    /**
     * map roles for role drop down list
     */
    if (roles)
      roles.map(item =>
        roleNameArray.push(<Option key={item.id}>{item.name}</Option>),
      );

    /**
     * map cities for city drop down list
     */
    if (cities)
      cities.map(item =>
        cityArray.push(<Option key={item.id}>{item.name}</Option>),
      );

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.userAddClick}>
        <div className={s.mainContent}>
          <h3>{USER_LIST}</h3>
          <CPList
            data={userArray}
            count={userCount}
            columns={columns}
            loading={userLoading}
            onAddClick={this.userAddClick}
            pagination={false}
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="email"
                      label={EMAIL}
                      value={email}
                      onChange={value => {
                        this.handelChangeInput('email', value.target.value);
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{PHONE_NUMBER}</label>
                    <CPIntlPhoneInput
                      value={phoneNumber}
                      onChange={this.changeNumber}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="firstName"
                      label={USER_FIRST_NAME}
                      value={firstName}
                      onChange={value => {
                        this.handelChangeInput('firstName', value.target.value);
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="lastName"
                      label={USER_LAST_NAME}
                      value={lastName}
                      onChange={value => {
                        this.handelChangeInput('lastName', value.target.value);
                      }}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{USER_USER_ROLE}</label>
                    <CPMultiSelect
                      placeholder={ROLE_SELECT_PLACEHOLDER}
                      value={selectedRoles}
                      onChange={this.roleChange}
                    >
                      {roleNameArray}
                    </CPMultiSelect>
                  </div>
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <label>
                      {STATUS} {OFFLINE}
                    </label>
                    <CPSelect
                      value={searchOffline}
                      onChange={value =>
                        this.handelChangeInput('searchOffline', value)
                      }
                    >
                      <Option key="0">{ALL}</Option>
                      <Option key="1">{ONLINE}</Option>
                      <Option key="2">{OFFLINE}</Option>
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <label>{SELECT_STATUS_EVENT}</label>
                    <CPSelect
                      value={searchUserEventStatus}
                      onChange={value =>
                        this.handelChangeInput('searchUserEventStatus', value)
                      }
                    >
                      <Option value={0}>{ALL}</Option>
                      <Option value={1}>{WAS_RESPONSE}</Option>
                      <Option value={2}>{WAS_NOT_RESPONSE}</Option>
                      <Option value={3}>{UN_WILLINGNESS}</Option>
                      <Option value={4}>{SYSTEM}</Option>
                      <Option value={5}>{BLOCK}</Option>
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <label>{CITY}</label>
                    <CPSelect
                      value={searchCityId}
                      onChange={value =>
                        this.handelChangeInput('searchCityId', value)
                      }
                    >
                      <Option value={0}>{ALL}</Option>
                      {cityArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <div className="align-center">
                      <CPSwitch
                        defaultChecked={userStatus}
                        checkedChildren={ACTIVE}
                        unCheckedChildren={DEACTIVATE}
                        onChange={value => {
                          this.handelChangeInput('userStatus', value);
                        }}
                      />
                      <CPSwitch
                        defaultChecked={searchReagentUser}
                        checkedChildren={REAGENT_USER}
                        unCheckedChildren={REAGENT_USER}
                        onChange={value => {
                          this.handelChangeInput('searchReagentUser', value);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className={s.field}>
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
          <CPPagination
            total={userCount}
            current={currentPage}
            onChange={this.onChangePagination}
            onShowSizeChange={this.onShowSizeChange}
            defaultPageSize={30}
            pageSizeOptions={['10', '20', '30', '50', '80', '100']}
          />
          <CPModal
            className="max_modal"
            visible={visibleModal}
            handleCancel={this.handleCancelModal}
            showFooter={false}
            title={SENDING_CODE_DISCOUNT}
          >
            <CPList
              data={promotions}
              count={promotionCount}
              columns={discountModalColumns}
              loading={promotionLoading}
              hideAdd
              hideSearchBox
            />
          </CPModal>
          <CPModal
            visible={visibleDiscountCodeGroupModal}
            handleCancel={this.handleCancelDiscountCodeGroupModal}
            handleOk={this.handleOkDiscountCodeGroupModal}
            title={ENTER_EXPIRE_DATE}
          >
            <div className="col-xs-12 modalBody">
              <label>{ENTER_EXPIRE_DATE}</label>
              <CPPersianCalendar
                placeholder={ENTER_EXPIRE_DATE}
                // onChange={value => this.onChangeInpute('dateOfBirth', value)}
                onChange={(unix, formatted) =>
                  this.handelChangeInput('persianToDateExpire', formatted)
                }
                id="datePicker"
                preSelected={persianToDateExpire}
              />
            </div>
          </CPModal>
          <CPConfirmation
            visible={visibleConfirm}
            handleOk={this.handleOkConfirm}
            handleCancel={this.handleCancelConfirm}
            content={ARE_YOU_SURE_WANT_TO_SEND_DISCOUNT_CODE}
            zIndex={10000}
          />
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  users: state.userList.data ? state.userList.data.items : [],
  userCount: state.userList.data ? state.userList.data.count : 0,
  roles: state.role.data ? state.role.data.items : [],
  userLoading: state.userList.loading,
  promotions: state.promotionList.data.items,
  promotionCount:
    state.promotionList.data.items && state.promotionList.data.items.length > 0
      ? state.promotionList.data.count
      : 0,
  promotionLoading: state.promotionList.loading,
  cities: state.cityList.data ? state.cityList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
      showLoading,
      userListRequest: userListActions.userListRequest,
      userDeleteRequest: userDeleteActions.userDeleteRequest,
      promotionListRequest: promotionListActions.promotionListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(UserList));
