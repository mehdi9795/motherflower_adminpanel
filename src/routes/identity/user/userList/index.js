import React from 'react';
import UserList from './UserList';
import Layout from '../../../../components/Template/Layout/Layout';
import userListActions from '../../../../redux/identity/actionReducer/user/List';
import roleActions from '../../../../redux/identity/actionReducer/role/Role';
import cityActions from '../../../../redux/common/actionReducer/city/List';
import { UserDto, UserRoleDto, RoleDto } from '../../../../dtos/identityDtos';
import { PAGE_TITLE_USER_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  // store.dispatch(showLoading());
  /**
   * get all users
   */
  const jsonListUser = new BaseGetDtoBuilder()
    .dto(
      new UserDto({
        userRoleDto: [new UserRoleDto({ roleDto: new RoleDto({ id: 1 }) })],
        active: true,
      }),
    )
    .pageSize(30)
    .pageIndex(0)
    .includes(['Roles', 'reagentUserDto'])
    .orderByFields(['createdOnUtc'])
    .orderByDescending(true)
    .buildJson();
  store.dispatch(userListActions.userListRequest(jsonListUser));

  /**
   * get all role
   */
  store.dispatch(
    roleActions.roleListRequest(
      new BaseGetDtoBuilder()
        .all(true)
        .disabledCount(true)
        .buildJson(),
    ),
  );

  /**
   * get all city
   */
  store.dispatch(
    cityActions.cityListRequest(
      new BaseGetDtoBuilder()
        .all(true)
        .disabledCount(true)
        .buildJson(),
    ),
  );

  return {
    chunks: ['userList'],
    title: `${PAGE_TITLE_USER_LIST}`,
    component: (
      <Layout>
        <UserList />
      </Layout>
    ),
  };
}

export default action;
