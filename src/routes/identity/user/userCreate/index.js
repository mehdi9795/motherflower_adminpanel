import React from 'react';
import UserCreate from './UserCreate';
import Layout from '../../../../components/Template/Layout/Layout';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import { AgentDto } from '../../../../dtos/vendorDtos';
import singleUserActions from '../../../../redux/identity/actionReducer/user/SingleUser';
import { PAGE_TITLE_USER_CREATE } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import { getDtoQueryString } from '../../../../utils/helper';
import { getCityApi } from '../../../../services/commonApi';

async function action({ store, query }) {
  // const { backUrl = '/user/list' } = query;

  const backUrl = query.vendorId
    ? `/vendor-branch/edit/${query.vendorBranchId}?tab=4`
    : '/user/list';
  store.dispatch(singleUserActions.singleUserSuccess(null));

  /**
   * get all vendors with true status
   */
  store.dispatch(
    vendorListActions.vendorListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  /**
   * get all city with true status
   */
  const cities = await getCityApi(
    store.getState().login.data.token,
    getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
  );

  if (cities.status === 200) {
    store.dispatch(cityListActions.cityListSuccess(cities.data));
  } else {
    store.dispatch(cityListActions.cityListFailure());
  }

  return {
    chunks: ['userCreate'],
    title: `${PAGE_TITLE_USER_CREATE}`,
    component: (
      <Layout>
        <UserCreate
          backUrl={backUrl}
          vendorId={query.vendorId}
          vendorBranchId={query.vendorBranchId}
        />
      </Layout>
    ),
  };
}

export default action;
