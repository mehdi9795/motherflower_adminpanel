import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './UserCreate.css';
import * as actionCreators from '../../../../redux/identity/action/user';
import { USER_CREATE } from '../../../../Resources/Localization';
import UserForm from '../../../../components/Identity/User/UserForm/UserForm';

class UserCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
    vendorId: PropTypes.number,
    vendorBranchId: PropTypes.number,
  };

  static defaultProps = {
    vendorId: undefined,
    vendorBranchId: undefined,
  };

  render() {
    const { backUrl, vendorId, vendorBranchId } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={USER_CREATE} backUrl={backUrl}>
          <UserForm
            backUrlAndVendorId={backUrl}
            vendorId={vendorId}
            vendorBranchId={vendorBranchId}
          />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  backUrlFromVendorBranch:
    state.setUserCreateBackUrl.data && state.setUserCreateBackUrl.data[0],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actionCreators, dispatch),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(UserCreate));
