/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/* eslint-disable global-require */

// The top-level (parent) route
const routes = {
  path: '',

  // Keep in mind, routes are evaluated in order
  children: [
    {
      path: '',
      load: () => import(/* webpackChunkName: 'home' */ './home'),
    },
    {
      path: '/user/list',
      load: () =>
        import(/* webpackChunkName: 'userList' */ './identity/user/userList'),
    },
    {
      path: '/city/list',
      load: () =>
        import(/* webpackChunkName: 'cityList' */ './common/city/cityList'),
    },
    {
      path: '/city/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'cityEdit' */ './common/city/cityEdit'),
    },
    {
      path: '/user/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'userEdit' */ './identity/user/userEdit'),
    },
    {
      path: '/user/create',
      load: () =>
        import(/* webpackChunkName: 'userCreate' */ './identity/user/userCreate'),
    },
    {
      path: '/product/create',
      load: () =>
        import(/* webpackChunkName: 'productCreate' */ './catalog/product/productCreate'),
    },
    {
      path: '/product/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'productEdit' */ './catalog/product/productEdit'),
    },
    {
      path: '/product/list',
      load: () =>
        import(/* webpackChunkName: 'productList' */ './catalog/product/productList'),
    },
    {
      path: '/vendor/list',
      load: () =>
        import(/* webpackChunkName: 'vendorList' */ './vendor/agent/vendorList'),
    },
    {
      path: '/vendor/create',
      load: () =>
        import(/* webpackChunkName: 'vendorCreate' */ './vendor/agent/vendorCreate'),
    },
    {
      path: '/vendor/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'vendorEdit' */ './vendor/agent/vendorEdit'),
    },
    {
      path: '/vendor-affiliate/list',
      load: () =>
        import(/* webpackChunkName: 'vendorAffiliateList' */ './vendor/affiliate/vendorAffiliateList'),
    },
    {
      path: '/vendor-affiliate/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'vendorAffiliateEdit' */ './vendor/affiliate/vendorAffiliateEdit'),
    },
    {
      path: '/vendor-branch/list',
      load: () =>
        import(/* webpackChunkName: 'vendorBranchList' */ './vendor/vendorBranch/vendorBranchList'),
    },
    {
      path: '/vendor-branch/create',
      load: () =>
        import(/* webpackChunkName: 'vendorBranchCreate' */ './vendor/vendorBranch/vendorBranchCreate'),
    },
    {
      path: '/vendor-branch/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'vendorBranchEdit' */ './vendor/vendorBranch/vendorBranchEdit'),
    },
    {
      path: '/category/list',
      load: () =>
        import(/* webpackChunkName: 'categoryList' */ './catalog/category/categoryList'),
    },
    {
      path: '/category/create',
      load: () =>
        import(/* webpackChunkName: 'categoryCreate' */ './catalog/category/categoryCreate'),
    },
    {
      path: '/category/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'categoryEdit' */ './catalog/category/categoryEdit'),
    },
    {
      path: '/promotion/list',
      load: () =>
        import(/* webpackChunkName: 'promotionList' */ './sam/promotion/promotionList'),
    },
    {
      path: '/promotion/create',
      load: () =>
        import(/* webpackChunkName: 'promotionCreate' */ './sam/promotion/promotionCreate'),
    },
    {
      path: '/promotion/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'promotionEdit' */ './sam/promotion/promotionEdit'),
    },
    {
      path: '/promotion/vendorBranchPromotionDetails',
      load: () =>
        import(/* webpackChunkName: 'vendorBranchPromotionDetails' */ './sam/promotion/vendorBranchPromotionDetails'),
    },
    {
      path: '/shippingTariff/list',
      load: () =>
        import(/* webpackChunkName: 'shippingTariffList' */ './sam/shippingTariff/shippingTariffList'),
    },
    {
      path: '/shippingTariff/create',
      load: () =>
        import(/* webpackChunkName: 'shippingTariffCreate' */ './sam/shippingTariff/shippingTariffCreate'),
    },
    {
      path: '/shippingTariff/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'shippingTariffEdit' */ './sam/shippingTariff/shippingTariffEdit'),
    },
    {
      path: '/public-event/list',
      load: () =>
        import(/* webpackChunkName: 'publicEventList' */ './common/publicEvent/publicEventList'),
    },
    {
      path: '/order/list',
      load: () =>
        import(/* webpackChunkName: 'orderList' */ './sam/order/orderList'),
    },
    {
      path: '/order/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'orderEdit' */ './sam/order/orderEdit'),
    },
    {
      path: '/changePassword',
      load: () =>
        import(/* webpackChunkName: 'changePassword' */ './identity/changePassword'),
    },
    {
      path: '/message/list',
      load: () =>
        import(/* webpackChunkName: 'messageList' */ './notification/message/messageList'),
    },
    {
      path: '/banner/list',
      load: () =>
        import(/* webpackChunkName: 'messageList' */ './cms/banner/bannerList'),
    },
    {
      path: '/tag/list',
      load: () =>
        import(/* webpackChunkName: 'tagList' */ './catalog/tag/tagList'),
    },
    {
      path: '/specification-attribute/list',
      load: () =>
        import(/* webpackChunkName: 'specificationAttributeList' */ './catalog/specificationAttribute/specificationAttributeList'),
    },
    {
      path: '/color-option/list',
      load: () =>
        import(/* webpackChunkName: 'colorOptionList' */ './catalog/colorOption/colorOptionList'),
    },
    {
      path: '/inventory-management/list',
      load: () =>
        import(/* webpackChunkName: 'inventoryManagementList' */ './catalog/inventoryManagement/InventoryManagementList'),
    },
    {
      path: '/inventory-management/create',
      load: () =>
        import(/* webpackChunkName: 'inventoryManagementCreate' */ './catalog/inventoryManagement/inventoryManagementCreate'),
    },
    {
      path: '/inventory-management/edit/:id',
      load: () =>
        import(/* webpackChunkName: 'inventoryManagementEdit' */ './catalog/inventoryManagement/inventoryManagementEdit'),
    },
    {
      path: '/push/list',
      load: () =>
        import(/* webpackChunkName: 'pushList' */ './notification/push/pushList'),
    },
    {
      path: '/push/Create',
      load: () =>
        import(/* webpackChunkName: 'pushCreate' */ './notification/push/pushCreate'),
    },
    {
      path: '/push/Edit/:id',
      load: () =>
        import(/* webpackChunkName: 'pushEdit' */ './notification/push/pushEdit'),
    },
    {
      path: '/sms/list',
      load: () =>
        import(/* webpackChunkName: 'smsList' */ './notification/sms/smsList'),
    },
    {
      path: '/sms/create',
      load: () =>
        import(/* webpackChunkName: 'smsCreate' */ './notification/sms/smsCreate'),
    },
    {
      path: '/sms/Edit/:id',
      load: () =>
        import(/* webpackChunkName: 'smsEdit' */ './notification/sms/smsEdit'),
    },
    {
      path: '/pushTestGroup/list',
      load: () =>
        import(/* webpackChunkName: 'pushTestGroupList' */ './notification/pushTestGroup/pushTestGroupList'),
    },
    {
      path: '/comment/list',
      load: () =>
        import(/* webpackChunkName: 'commentList' */ './cms/comment/commentList'),
    },
    {
      path: '/userEvent/list',
      load: () =>
        import(/* webpackChunkName: 'userEventList' */ './identity/event/eventList'),
    },
    {
      path: '/profile',
      load: () => import(/* webpackChunkName: 'profile' */ './profile'),
    },
    {
      path: '/login',
      load: () => import(/* webpackChunkName: 'login' */ './login'),
    },
  ],

  async action({ next }) {
    // Execute each child route until one of them return the result
    const route = await next();

    // Provide default values for title, description etc.
    route.title = `${route.title || 'Untitled Page'}`;
    route.description = route.description || '';

    return route;
  },
};

// The error page is available by permanent url for development mode
if (__DEV__) {
  routes.children.unshift({
    path: '/error',
    action: require('./error').default,
  });
}

export default routes;
