import React from 'react';
import Home from './Home';
import Layout from '../../components/Template/Layout';
import { PermissionDto } from '../../dtos/identityDtos';
import { getDtoQueryString } from '../../utils/helper';
import {
  getPermissionApi,
  getUserReportSettingApi,
} from '../../services/identityApi';
import PermissionActions from '../../redux/identity/actionReducer/permission/Permission';
import ReportsActions from '../../redux/common/actionReducer/report/List';
import ReportSettingActions from '../../redux/identity/actionReducer/reportSetting/List';
import { PAGE_TITLE_HOME } from '../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { getReportApi } from '../../services/commonApi';
import { ReportDto } from '../../dtos/commonDtos';

async function action({ store }) {
  /**
   * get permissions
   * @type
   */
  const permissions = await getPermissionApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new PermissionDto({
            resourceType: 'UI',
            permissionStatus: 'Deny',
          }),
        )
        .buildJson(),
    ),
  );

  /**
   * get reports
   * @type
   */
  const reports = await getReportApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new ReportDto({
            published: true,
          }),
        )
        .buildJson(),
    ),
  );

  /**
   * get reportSetting
   * @type
   */
  const reportSetting = await getUserReportSettingApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(new ReportDto())
        .includes(['reportDto'])
        .buildJson(),
    ),
  );

  if (permissions.status === 200 && reports.status === 200) {
    store.dispatch(
      PermissionActions.permissionListSuccess(permissions.data.listDto),
    );
    store.dispatch(ReportsActions.reportListSuccess(reports.data));
    store.dispatch(
      ReportSettingActions.reportSettingListSuccess(reportSetting.data),
    );
  } else {
    store.dispatch(PermissionActions.permissionListFailure());
    store.dispatch(ReportsActions.reportListFailure());
  }

  return {
    chunks: ['home'],
    title: `${PAGE_TITLE_HOME}`,
    component: (
      <Layout>
        <Home />
      </Layout>
    ),
  };
}

export default action;
