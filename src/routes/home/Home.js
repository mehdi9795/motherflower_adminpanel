import React from 'react';
import { canvas, Chart } from 'chart.js';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import $ from 'jquery';
import PropTypes from 'prop-types';
import cs from 'classnames';
import { hideLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import s from './Home.css';
import { USERS, ORDERS } from '../../Resources/Localization';
import CPPanel from '../../components/CP/CPPanel';
import CPLeftDrawer from '../../components/CP/CPLeftDrawer';
import Link from '../../components/Link';
import CPWidget from '../../components/CP/CPWidget';
import { getPushToken } from '../../utils/push/getPushToken';

class Home extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    reportSetting: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    reportSetting: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      topDataSource: props.reportSetting.filter(item =>
        this.filterReportsWithRegion(item, 'Top'),
      ),
      centerDataSource: props.reportSetting.filter(item =>
        this.filterReportsWithRegion(item, 'Center'),
      ),
      bottomDataSource: props.reportSetting.filter(item =>
        this.filterReportsWithRegion(item, 'Bottom'),
      ),
    };
  }

  componentDidMount() {
    getPushToken();
    const ctx = document.getElementById('myChart');
    const ctx2 = document.getElementById('myChart2');
    const ctx3 = document.getElementById('myChart3');
    const myChart = new Chart(ctx, {
      type: 'horizontalBar',
      data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [
          {
            label: '# of Votes',
            data: [12, 19, 3, 5, 10, 3],
            backgroundColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)',
            ],
            borderColor: [
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)',
            ],
            borderWidth: 1,
          },
        ],
        type: 'line',
      },
      options: {
        scales: {
          xAxes: [
            {
              gridLines: {
                offsetGridLines: false,
              },
            },
          ],
        },
      },
    });
    setTimeout(() => {
      new Chart(ctx2, {
        type: 'pie',
        data: {
          labels: ['Red', 'Blue', 'Yellow', 'Green'],
          datasets: [
            {
              label: '# of Votes',
              data: [12, 19, 3, 5],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
              ],
              borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
              ],
              borderWidth: 1,
            },
          ],
        },
      });
    }, 500);
    setTimeout(() => {
      new Chart(ctx3, {
        type: 'line',
        data: {
          labels: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور'],
          datasets: [
            {
              label: 'web',
              data: [12, 30, 20, 12, 30, 20],
              borderWidth: 2,
              borderColor: 'red',
              backgroundColor: 'transparent',
              pointBackgroundColor: 'red',
            },
            {
              label: 'app',
              data: [8, 38, 18, 8, 38, 18],
              borderWidth: 1,
              borderColor: 'blue',
              backgroundColor: 'transparent',
              pointBackgroundColor: 'blue',
            },
            {
              label: 'tablet',
              data: [10, 35, 15, 10, 35, 15],
              borderWidth: 1,
              borderColor: 'yellow',
              backgroundColor: 'transparent',
              pointBackgroundColor: 'yellow',
            },
          ],
        },
      });
    }, 1000);
    //
    // $(document).mouseup(e => {
    //   const container = document.getElementById('left-drawer');
    //   const container2 = $('#left-drawer');
    //   if (!container2.is(e.target) && container2.has(e.target).length === 0)
    //     if (container && container.className.includes('open-drawer')) {
    //       container.classList.remove('open-drawer');
    //     }
    // });
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  filterReportsWithRegion = (item, region) => item.region === region;

  onOpenChange = e => {
    e.preventDefault();
    const element = document.getElementById('left-drawer');
    const elementSetting = $('.btnSetting');

    if (element.className.includes('open-drawer')) {
      element.classList.remove('open-drawer');
      element.classList.add('close-drawer');
      elementSetting.removeClass('open-btn-drawer');
      elementSetting.addClass('close-btn-drawer');
    } else {
      element.classList.remove('close-drawer');
      element.classList.add('open-drawer');
      elementSetting.removeClass('close-btn-drawer');
      elementSetting.addClass('open-btn-drawer');
    }
  };

  render() {
    const { topDataSource, centerDataSource, bottomDataSource } = this.state;

    return (
      <div>
        <CPLeftDrawer id="left-drawer" />
        <div className={s.container}>
          <h3 />
          <div className="row widgetCard">
            {topDataSource.map(item => (
              <CPWidget
                key={item.reportDto.id}
                value={item.reportDto.id}
                unit="نفر"
                icon="cp-users"
                label={item.reportDto.title}
              />
            ))}
            {/* <CPWidget
              value={105961}
              unit="نفر"
              icon="cp-users"
              label="تعداد کل کاربران"
            />
            <CPWidget
              content="گل لاله"
              icon="cp-bouquet"
              label="محصول پرفروش"
            />
            <CPWidget
              content="دسته گل"
              icon="cp-folder"
              label="گروه محصول پربازدید"
            />
            <CPWidget
              value={1297865000}
              unit="تومان"
              icon="cp-receipt"
              label="مبلغ کل سفارشات"
            /> */}
          </div>
          <div className="row">
            <div className="col-lg-4 col-sm-12">
              <CPPanel header={ORDERS} key="1">
                <canvas id="myChart" height="50vh" width="100" />
              </CPPanel>
            </div>
            <div className="col-lg-4 col-sm-12">
              <CPPanel header={USERS} key="2">
                <canvas id="myChart2" height="50vh" width="100" />
              </CPPanel>
            </div>
            <div className="col-lg-4 col-sm-12">
              <CPPanel header={USERS} key="3">
                <canvas id="myChart3" height="50vh" width="100" />
              </CPPanel>
            </div>
          </div>
          <div className="row widgetCard">
            <CPWidget
              value={105961}
              unit="نفر"
              icon="cp-users"
              label="تعداد کل کاربران"
            />
            <CPWidget
              content="گل لاله"
              icon="cp-bouquet"
              label="محصول پرفروش"
            />
            <CPWidget
              content="دسته گل"
              icon="cp-folder"
              label="گروه محصول پربازدید"
            />
            <CPWidget
              value={1297865000}
              unit="تومان"
              icon="cp-receipt"
              label="مبلغ کل سفارشات"
            />
          </div>
          <Link
            className={cs(s.btnSetting, 'btnSetting')}
            onClick={e => this.onOpenChange(e)}
            to="#"
          >
            <i className="cp-settings" />
          </Link>
          {/* <CPExcel /> */}
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  reportSetting: state.reportSettingList.data
    ? state.reportSettingList.data.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Home));
