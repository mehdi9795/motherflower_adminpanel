import React from 'react';
import VendorAffiliateList from './VendorAffiliateList';
import Layout from '../../../../components/Template/Layout/Layout';
import vendorAffiliateListActions from '../../../../redux/vendor/actionReducer/vendorAffiliate/List';
import { PAGE_TITLE_VENDOR_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all vendors with true status
   */
  store.dispatch(
    vendorAffiliateListActions.vendorAffiliateListRequest(
      new BaseGetDtoBuilder()
        .all(true)
        .includes(['cityDto'])
        .buildJson(),
    ),
  );

  return {
    chunks: ['vendorAffiliateList'],
    title: `${PAGE_TITLE_VENDOR_LIST}`,
    component: (
      <Layout>
        <VendorAffiliateList />
      </Layout>
    ),
  };
}

export default action;
