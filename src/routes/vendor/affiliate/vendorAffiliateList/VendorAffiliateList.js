import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import CPList from '../../../../components/CP/CPList/CPList';
import CPButton from '../../../../components/CP/CPButton';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import s from './VendorAffiliateList.css';
import Link from '../../../../components/Link/Link';
import {
  YES,
  NO,
  VENDOR_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR,
  USER_FULL_NAME,
  PHONE,
  VENDOR_NAME,
  CITY,
  DISTRICT,
} from '../../../../Resources/Localization';
import vendorAffiliateListActions from '../../../../redux/vendor/actionReducer/vendorAffiliate/List';
import vendorAffiliateDeleteActions from '../../../../redux/vendor/actionReducer/vendorAffiliate/Delete';
import {BaseGetDtoBuilder} from "../../../../dtos/dtoBuilder";

class VendorList extends React.Component {
  static propTypes = {
    vendorAffiliateLoading: PropTypes.bool,
    vendorAffiliateCount: PropTypes.number,
    vendorAffiliates: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    vendorAffiliateCount: 0,
    vendorAffiliateLoading: false,
    vendorAffiliates: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      vendorName: '',
      vendorStatus: true,
    };
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    /**
     * get all vendors with true status
     */

    const deleteData = {
      id,
      listDto: new BaseGetDtoBuilder().all(true).buildJson(),
      status: 'list',
    };

    this.props.actions.vendorAffiliateDeleteRequest(deleteData);
  };

  render() {
    const {
      vendorAffiliates,
      vendorAffiliateLoading,
      vendorAffiliateCount,
    } = this.props;

    const columns = [
      { title: USER_FULL_NAME, dataIndex: 'fullName', key: 'fullName' },
      { title: PHONE, dataIndex: 'phone', key: 'phone' },
      { title: VENDOR_NAME, dataIndex: 'vendorName', key: 'vendorName' },
      { title: CITY, dataIndex: 'city', key: 'city' },
      {
        title: DISTRICT,
        dataIndex: 'district',
        key: 'district',
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to={`/vendor-affiliate/edit/${record.key}`}
              onClick={() => {
                this.props.actions.showLoading();
              }}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];
    const vendorAffiliateListArray = [];

    /**
     * maping vendors list data source
     */
    if (vendorAffiliates)
      vendorAffiliates.map(item => {
        const obj = {
          key: item.id,
          fullName: item.fullName,
          phone: item.phone,
          vendorName: item.vendorName,
          city: item.cityDto ? item.cityDto.name : '',
          district: item.district,
        };
        return vendorAffiliateListArray.push(obj);
      });

    return (
      <div className={s.mainContent}>
        <h3>{VENDOR_LIST}</h3>
        <CPList
          data={vendorAffiliateListArray}
          count={vendorAffiliateCount}
          columns={columns}
          loading={vendorAffiliateLoading}
          hideAdd
          hideSearchBox
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  vendorAffiliates: state.vendorAffiliateList.data
    ? state.vendorAffiliateList.data.items
    : [],
  vendorAffiliateCount: state.vendorAffiliateList.data
    ? state.vendorAffiliateList.data.count
    : 0,
  vendorAffiliateLoading: state.vendorAffiliateList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
      showLoading,
      vendorAffiliateListRequest: vendorAffiliateListActions.vendorAffiliateListRequest,
      vendorAffiliateDeleteRequest:
      vendorAffiliateDeleteActions.vendorAffiliateDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorList));
