import React from 'react';
import VendorAffiliateEdit from './VendorAffiliateEdit';
import Layout from '../../../../components/Template/Layout/Layout';
import singleVendorAffiliateActions from '../../../../redux/vendor/actionReducer/vendorAffiliate/Single';
import { VendorAffiliateDto } from '../../../../dtos/vendorDtos';
import { getVendorAffiliateApi } from '../../../../services/vendorApi';
import { getDtoQueryString } from '../../../../utils/helper';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import { PAGE_TITLE_VENDOR_EDIT } from '../../../../Resources/Localization';
import { getCityApi } from '../../../../services/commonApi';
import {BaseGetDtoBuilder} from "../../../../dtos/dtoBuilder";

async function action({ store, query, params }) {
  const { backUrl = '/vendor-affiliate/list' } = query;
  try {
    /**
     * get single vendor affiliate by id for edit
     */

    const singleVendorAffiliate = await getVendorAffiliateApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new VendorAffiliateDto({
              id: params.id,
            }),
          )
          .includes(['cityDto'])
          .buildJson(),
      ),
    );

    /**
     * get all product types with true status in active field
     */

    const cities = await getCityApi(
      store.getState().login.data.token,
      getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
    );

    if (singleVendorAffiliate.status === 200 && cities.status === 200) {
      store.dispatch(
        singleVendorAffiliateActions.singleVendorAffiliateSuccess(
          singleVendorAffiliate.data,
        ),
      );
      store.dispatch(cityListActions.cityListSuccess(cities.data));
    } else {
      store.dispatch(
        singleVendorAffiliateActions.singleVendorAffiliateFailure(),
      );
    }
  } catch (error) {
    store.dispatch(singleVendorAffiliateActions.singleVendorAffiliateFailure());
  }

  return {
    chunks: ['vendorAffiliateEdit'],
    title: `${PAGE_TITLE_VENDOR_EDIT}`,
    component: (
      <Layout>
        <VendorAffiliateEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
