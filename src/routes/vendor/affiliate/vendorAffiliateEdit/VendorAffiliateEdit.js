import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './VendorAffiliateEdit.css';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR,
  VENDOR_EDIT,
} from '../../../../Resources/Localization';
import history from '../../../../history';
import vendorAgentDeleteActions from '../../../../redux/vendor/actionReducer/vendor/Delete';
import VendorAffiliateForm from '../../../../components/Vendor/VendorAffiliate/VendorAffiliateForm';

class VendorEdit extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
    vendorAffiliate: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    vendorAffiliate: null,
  };

  delete = () => {
    const { vendorAffiliate } = this.props;

    const deleteData = {
      id: vendorAffiliate.id,
      status: 'edit',
    };

    this.props.actions.vendorAgentDeleteRequest(deleteData);
  };

  redirect = id => {
    history.push(`/edit/list/${id}`);
  };

  render() {
    const { backUrl, vendorAffiliate } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard
          title={`${VENDOR_EDIT}`}
          backUrl={backUrl}
          isEdit={vendorAffiliate && vendorAffiliate.id > 0}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR}
          onDelete={this.delete}
        >
          <VendorAffiliateForm />
        </EditCard>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  vendorAffiliate:
    state.singleVendorAffiliate.data &&
    state.singleVendorAffiliate.data.items.length > 0
      ? state.singleVendorAffiliate.data.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorAgentDeleteRequest:
        vendorAgentDeleteActions.vendorAgentDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorEdit));
