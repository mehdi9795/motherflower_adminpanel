import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import HotKeys from 'react-hot-keys';
import { Select } from 'antd';
import PropTypes from 'prop-types';
import CPList from '../../../../components/CP/CPList/CPList';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import s from './VendorBranchList.css';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import vendorBranchDeleteActions from '../../../../redux/vendor/actionReducer/vendorBranch/Delete';
import districtOptionListActions from '../../../../redux/common/actionReducer/district/List';
import {
  NAME,
  CITY,
  DISTRICT,
  AGENT_NAME,
  STATUS,
  VENDOR_BRANCH_NAME,
  ALL_VENDORS,
  ALL_CITIES,
  ALL_DISTRICT,
  ACTIVE,
  DEACTIVATE,
  YES,
  NO,
  VENDOR_BRANCH_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR_BRANCH,
  GRADE,
  ALL,
} from '../../../../Resources/Localization';
import CPSwitch from '../../../../components/CP/CPSwitch';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import history from '../../../../history';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import { DistrictDto } from '../../../../dtos/commonDtos';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import {
  checkPermission,
  pressEnterAndCallApplySearch,
} from '../../../../utils/helper';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import CPAvatar from '../../../../components/CP/CPAvatar';

const { Option } = Select;

class VendorBranchList extends React.Component {
  static propTypes = {
    vendorBranchLoading: PropTypes.bool,
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    vendors: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    vendorBranchCount: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    districtOptions: PropTypes.arrayOf(PropTypes.object),
    flatPermission: PropTypes.objectOf(PropTypes.any),
    vendorId: PropTypes.string,
  };

  static defaultProps = {
    districtOptions: [],
    vendorId: '',
    vendorBranches: [],
    vendors: [],
    cities: [],
    vendorBranchLoading: false,
    vendorBranchCount: 0,
    flatPermission: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      vendorId: '0',
      cityId: '0',
      districtId: '0',
      vendorBranchStatus: true,
      districtValue: '0',
      vendorBranchGrade: 'all',
      orderByFields: undefined,
      orderByDescending: undefined,
    };

    this.districtDataSource = [];
    this.submitSearch = this.submitSearch.bind(this);
    this.cityChange = this.cityChange.bind(this);
  }

  componentWillMount() {
    const { vendorId } = this.props;
    if (vendorId) {
      this.setState({
        vendorId: vendorId.toString(),
      });
    }
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const deleteData = { id, status: 'list' };
    this.props.actions.vendorBranchDeleteRequest(deleteData);
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });

    if ([inputName][0] === 'districtId')
      this.setState({
        districtValue: value,
      });
  };

  /**
   * change city for automatic change district
   * @param inputName
   * @param value
   * @returns {Promise<void>}
   */
  async cityChange(inputName, value) {
    this.props.actions.districtOptionListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityId: value,
          }),
        )
        .buildJson(),
    );
    this.setState({
      [inputName]: value,
    });
  }

  /**
   * for search in vendor branch
   * @returns {Promise<void>}
   */
  async submitSearch() {
    const {
      vendorBranchStatus,
      name,
      cityId,
      vendorId,
      districtId,
      vendorBranchGrade,
      orderByDescending,
      orderByFields,
    } = this.state;
    /**
     * get vendor branch with where
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchDto({
          active: vendorBranchStatus,
          name,
          vendorBranchGrade:
            vendorBranchGrade === 'all' ? undefined : vendorBranchGrade,
          cityId,
          districtId,
          vendorId,
        }),
      )
      .includes(['districtDto', 'vendorDto'])
      .orderByDescending(orderByDescending)
      .orderByFields(orderByFields)
      .buildJson();

    this.props.actions.vendorBranchListRequest(jsonList);
  }
  /**
   * vendor branch list add button click handler
   */
  vendorBranchAddClick = () => {
    this.props.actions.showLoading();
    const { vendorId } = this.props;
    this.props.actions.showLoading();
    if (vendorId) history.push(`/vendor-branch/create?vendorId=${vendorId}`);
    else history.push('/vendor-branch/create');
  };

  handleTableChange = (pagination, filters, sorter) => {
    const {
      vendorBranchStatus,
      name,
      cityId,
      vendorId,
      districtId,
    } = this.state;

    let orderByDescending = false;

    if (Object.keys(sorter).length > 0) {
      if (sorter.order === 'ascend') {
        orderByDescending = false;
      } else {
        orderByDescending = true;
      }

      /**
       * get vendor branch with where
       */
      const jsonList = new BaseGetDtoBuilder()
        .dto(
          new VendorBranchDto({
            active: vendorBranchStatus,
            name,
            cityId,
            districtId,
            vendorId,
          }),
        )
        .includes(['districtDto', 'vendorDto'])
        .orderByDescending(orderByDescending)
        .orderByFields(sorter.field)
        .buildJson();

      this.props.actions.vendorBranchListRequest(jsonList);
    }
  };

  render() {
    const {
      vendorBranches,
      vendorBranchLoading,
      cities,
      vendors,
      vendorBranchCount,
      districtOptions,
      flatPermission,
    } = this.props;

    const {
      districtValue,
      name,
      vendorId,
      cityId,
      districtId,
      vendorBranchStatus,
      vendorBranchGrade,
    } = this.state;

    const columns = [
      {
        title: '',
        dataIndex: 'url',
        width: 50,
        render: (text, record) => <CPAvatar src={record.imgUrl} />,
      },
      {
        title: NAME,
        dataIndex: 'name',
        key: 'name',
        className: checkPermission('order-search-vendo', flatPermission),
        width: 250,
      },
      { title: CITY, dataIndex: 'city', key: 'city', width: 150 },
      { title: DISTRICT, dataIndex: 'district', key: 'district', width: 150 },
      { title: AGENT_NAME, dataIndex: 'vendor', key: 'vendor', width: 250 },
      {
        title: GRADE,
        dataIndex: 'vendorBranchGrade',
        key: 'vendorBranchGrade',
        sorter: true,
        width: 100,
      },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 80,
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR_BRANCH}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to={`/vendor-branch/edit/${record.key}`}
              onClick={() => {
                this.props.actions.showLoading();
              }}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];
    const vendorBranchDataSource = [];
    /**
     * maping vendor Branches for list data source
     */
    if (vendorBranches) {
      vendorBranches.map(item => {
        const obj = {
          key: item.id,
          name: item.name,
          vendor: item.vendorDto ? item.vendorDto.name : '',
          city: item.districtDto ? item.districtDto.cityDto.name : '',
          district: item.districtDto ? item.districtDto.name : '',
          active: item.active,
          vendorBranchGrade: item.vendorBranchGrade,
          imgUrl:
            item.imageDtos && item.imageDtos.length > 0
              ? `${item.imageDtos[0].url}`
              : ` `,
        };
        return vendorBranchDataSource.push(obj);
      });
    }
    const vendorDataSource = [];
    const citiesDataSource = [];
    const districtDataSource = [];

    /**
     * map vendors for comboBox Datasource
     */
    if (vendors)
      vendors.map(item =>
        vendorDataSource.push(<Option key={item.id}>{item.name}</Option>),
      );

    /**
     * map cities for comboBox Datasource
     */
    cities.map(item =>
      citiesDataSource.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map district for comboBox Datasource
     */
    if (districtOptions)
      districtOptions.map(item =>
        districtDataSource.push(<Option key={item.id}>{item.name}</Option>),
      );

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.vendorBranchAddClick}>
        <div className={s.mainContent}>
          <h3>{VENDOR_BRANCH_LIST}</h3>
          <CPList
            data={vendorBranchDataSource}
            columns={columns}
            count={vendorBranchCount}
            loading={vendorBranchLoading}
            onAddClick={this.vendorBranchAddClick}
            onChange={this.handleTableChange}
            searchBoxName="vendorBranch-search"
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      label={VENDOR_BRANCH_NAME}
                      value={name}
                      onChange={value => {
                        this.handelChangeInput('name', value.target.value);
                      }}
                    />
                  </div>

                  <div
                    className="col-lg-3 col-md-6 col-sm-12"
                    name="vendorBranch-search-vendorId"
                  >
                    <label>{AGENT_NAME}</label>
                    <CPSelect
                      defaultValue={vendorId}
                      showSearch
                      onChange={value => {
                        this.handelChangeInput('vendorId', value);
                      }}
                    >
                      <Option key={0}>{ALL_VENDORS}</Option>
                      {vendorDataSource}
                    </CPSelect>
                  </div>

                  <div
                    className="col-lg-3 col-md-6 col-sm-12"
                    name="vendorBranch-search-cityId"
                  >
                    <label>{CITY}</label>
                    <CPSelect
                      defaultValue={cityId}
                      showSearch
                      onChange={value => {
                        this.cityChange('cityId', value);
                      }}
                    >
                      <Option key={0}>{ALL_CITIES}</Option>
                      {citiesDataSource}
                    </CPSelect>
                  </div>

                  <div
                    className="col-lg-3 col-md-6 col-sm-12"
                    name="vendorBranch-search-districtId"
                  >
                    <label>{DISTRICT}</label>
                    <CPSelect
                      defaultValue={districtId}
                      value={districtValue}
                      showSearch
                      onChange={value => {
                        this.handelChangeInput('districtId', value);
                      }}
                    >
                      <Option key={0}>{ALL_DISTRICT}</Option>
                      {districtDataSource}
                    </CPSelect>
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col-lg-3 col-md-6 col-sm-12"
                    name="vendorBranch-search-grade"
                  >
                    <div className="align-margin">
                      <label>{GRADE}</label>
                      <CPSelect
                        value={vendorBranchGrade}
                        onChange={value => {
                          this.handelChangeInput('vendorBranchGrade', value);
                        }}
                      >
                        <Option value="all" key="all">
                          {ALL}
                        </Option>
                        <Option value="VIP" key="VIP">
                          VIP
                        </Option>
                        <Option value="Gold" key="Gold">
                          Gold
                        </Option>
                        <Option value="Silver" key="Silver">
                          Silver
                        </Option>
                        <Option value="Boronz" key="Boronz">
                          Boronz
                        </Option>
                      </CPSelect>
                    </div>
                  </div>

                  <div
                    className="col-lg-3 col-md-6 col-sm-12"
                    name="vendorBranch-search-active"
                  >
                    <div className="align-margin">
                      <label>{STATUS}</label>
                      <CPSwitch
                        defaultChecked={vendorBranchStatus}
                        checkedChildren={ACTIVE}
                        unCheckedChildren={DEACTIVATE}
                        onChange={value => {
                          this.handelChangeInput('vendorBranchStatus', value);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="field">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
  vendorBranchCount: state.vendorBranchList.data.count,
  vendorBranchLoading: state.vendorBranchList.loading,
  vendors: state.vendorList.data ? state.vendorList.data.items : [],
  cities: state.cityList.data ? state.cityList.data.items : [],
  districtOptions: state.districtList.data ? state.districtList.data.items : [],
  flatPermission:
    state.permission.data && state.permission.data.items.length > 0
      ? state.permission.data.items[0].flatermission
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorBranchListRequest: vendorBranchListActions.vendorBranchListRequest,
      districtOptionListRequest:
        districtOptionListActions.districtOptionListRequest,
      vendorBranchDeleteRequest:
        vendorBranchDeleteActions.vendorBranchDeleteRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchList));
