import React from 'react';
import VendorBranchList from './VendorBranchList';
import Layout from '../../../../components/Template/Layout/Layout';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import { AgentDto, VendorBranchDto } from '../../../../dtos/vendorDtos';
import { getDtoQueryString } from '../../../../utils/helper';
import { getVendorBranchApi } from '../../../../services/vendorApi';
import { PAGE_TITLE_VENDOR_BRANCH_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query }) {
  /**
   * get all vendor branch for first time with default clause
   */
  const jsonListVB = new BaseGetDtoBuilder()
    .dto(
      new VendorBranchDto({
        vendorId: query ? query.vendorId : undefined,
        active: true,
      }),
    )
    .all(Object.keys(query).length === 0)
    .includes(['districtDto', 'vendorDto', 'imageDtos'])
    .buildJson();
  const vendorBranchData = await getVendorBranchApi(
    store.getState().login.data.token,
    getDtoQueryString(jsonListVB),
  );
  store.dispatch(
    vendorBranchListActions.vendorBranchListSuccess(vendorBranchData.data),
  );

  /**
   * get all cities for first time with default clause
   */
  store.dispatch(
    cityListActions.cityListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  /**
   * get all vendors for first time with where clause
   */
  store.dispatch(
    vendorListActions.vendorListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new AgentDto({
            active: true,
          }),
        )
        .buildJson(),
    ),
  );

  return {
    chunks: ['vendorBranchList'],
    title: `${PAGE_TITLE_VENDOR_BRANCH_LIST}`,
    component: (
      <Layout>
        <VendorBranchList vendorId={query.vendorId} />
      </Layout>
    ),
  };
}

export default action;
