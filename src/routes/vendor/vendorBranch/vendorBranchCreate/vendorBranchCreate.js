import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './vendorBranchCreate.css';
import { VENDOR_BRANCH_CREATE } from '../../../../Resources/Localization';
import VendorBranchForm from '../../../../components/Vendor/VendorBranch/VendorBranchForm/VendorBranchForm';

class vendorBranchCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
    vendorId: PropTypes.string,
  };

  static defaultProps = {
    vendorId: '',
  };

  render() {
    const { backUrl, vendorId } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={VENDOR_BRANCH_CREATE} backUrl={backUrl}>
          <VendorBranchForm data={null} vendorId={vendorId} />
        </EditCard>
      </div>
    );
  }
}

export default withStyles(s)(vendorBranchCreate);
