import React from 'react';
import VendorBranchCreate from './vendorBranchCreate';
import Layout from '../../../../components/Template/Layout/Layout';
import { AgentDto } from '../../../../dtos/vendorDtos';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import secondaryLevelListActions from '../../../../redux/catalog/actionReducer/category/SecondaryLevelList';
import singleVendorBranchInitActions from '../../../../redux/vendor/actionReducer/vendorBranch/SingleInit';
import singleVendorBranchActions from '../../../../redux/vendor/actionReducer/vendorBranch/Single';
import { getDtoQueryString } from '../../../../utils/helper';
import { getCategoryApi } from '../../../../services/catalogApi';
import { getCityApi } from '../../../../services/commonApi';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import { getAgentApi } from '../../../../services/vendorApi';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import { PAGE_TITLE_VENDOR_BRANCH_CREATE } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query }) {
  const { backUrl = '/vendor-Branch/list' } = query;
  try {
    store.dispatch(
      singleVendorBranchInitActions.singleVendorBranchInitRequest(),
    );

    /**
     * get all vendors with true status
     */
    const vendorDtos = await getAgentApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new AgentDto({
              active: true,
            }),
          )
          .buildJson(),
      ),
    );

    /**
     * get all Secondary Level categories  with default clause secondaryLevel
     */
    const jsonListCategory = new BaseGetDtoBuilder()
      .dto(
        new CategoryDto({
          secondaryLevel: true,
          published: true,
        }),
      )
      .buildJson();

    const categoryDtos = await getCategoryApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonListCategory),
    );
    /**
     * get all cities for first time with default clause
     */
    const cityDtos = await getCityApi(
      store.getState().login.data.token,
      getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
    );

    if (
      vendorDtos.status === 200 &&
      categoryDtos.status === 200 &&
      cityDtos.status === 200
    ) {
      store.dispatch(singleVendorBranchActions.singleVendorBranchSuccess(null));
      store.dispatch(vendorListActions.vendorListSuccess(vendorDtos.data));
      store.dispatch(
        secondaryLevelListActions.secondaryLevelListSuccess(categoryDtos.data),
      );
      store.dispatch(cityListActions.cityListSuccess(cityDtos.data));
      store.dispatch(
        singleVendorBranchInitActions.singleVendorBranchInitSuccess(),
      );
    } else {
      store.dispatch(
        singleVendorBranchInitActions.singleVendorBranchInitFailure(),
      );
    }
  } catch (error) {
    store.dispatch(
      singleVendorBranchInitActions.singleVendorBranchInitFailure(),
    );
  }

  return {
    chunks: ['vendorBranchCreate'],
    title: `${PAGE_TITLE_VENDOR_BRANCH_CREATE}`,
    component: (
      <Layout>
        <VendorBranchCreate backUrl={backUrl} vendorId={query.vendorId} />
      </Layout>
    ),
  };
}

export default action;
