import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './vendorBranchEdit.css';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR_BRANCH,
  VENDOR_BRANCH_EDIT,
} from '../../../../Resources/Localization';
import VendorBranchForm from '../../../../components/Vendor/VendorBranch/VendorBranchForm/VendorBranchForm';
import history from '../../../../history';
import backUrlActions from '../../../../redux/identity/actionReducer/backUrl/setUserCreateBackUrl';
import vendorBranchDeleteActions from '../../../../redux/vendor/actionReducer/vendorBranch/Delete';

class VendorBranchEdit extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
    tabSelected: PropTypes.string,
    vendorBranch: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    backUrl: '/edit/list',
    vendorBranch: {},
    tabSelected: '',
  };

  constructor(props) {
    super(props);
    this.delete = this.delete.bind(this);
  }

  componentWillMount() {
    /**
     * empty create user back url
     */
    this.props.actions.createUserBackUrlSuccess(null);
  }

  async delete() {
    const { vendorBranch } = this.props;

    const deleteData = {
      id: vendorBranch.id,
      status: 'edit',
    };

    this.props.actions.vendorBranchDeleteRequest(deleteData);
  }

  redirect = () => {
    history.push('/vendor-branch/list');
  };

  render() {
    const { backUrl, vendorBranch, tabSelected } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard
          title={`${VENDOR_BRANCH_EDIT}  ${vendorBranch.name}`}
          backUrl={backUrl}
          isEdit={vendorBranch && vendorBranch.id > 0}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR_BRANCH}
          onDelete={this.delete}
        >
          <VendorBranchForm data={vendorBranch} tabSelected={tabSelected} />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  vendorBranch: state.singleVendorBranch.data && state.singleVendorBranch.data.items ? state.singleVendorBranch.data.items[0] : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      createUserBackUrlSuccess: backUrlActions.createUserBackUrlSuccess,
      vendorBranchDeleteRequest:
        vendorBranchDeleteActions.vendorBranchDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchEdit));
