import React from 'react';
import VendorBranchEdit from './vendorBranchEdit';
import Layout from '../../../../components/Template/Layout/Layout';
import { AgentDto, VendorBranchDto } from '../../../../dtos/vendorDtos';
import singleVendorBranchInitActions from '../../../../redux/vendor/actionReducer/vendorBranch/SingleInit';
import singleVendorBranchActions from '../../../../redux/vendor/actionReducer/vendorBranch/Single';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import secondaryLevelListActions from '../../../../redux/catalog/actionReducer/category/SecondaryLevelList';
import {
  CategoryDto,
  PreparingProductTimeDto,
} from '../../../../dtos/catalogDtos';

import {
  getCategoryApi,
  getProductTimeApi,
} from '../../../../services/catalogApi';
import {
  getVendorBranchApi,
  getAgentApi,
} from '../../../../services/vendorApi';
import { getCityApi } from '../../../../services/commonApi';
import { getDtoQueryString } from '../../../../utils/helper';
import productTimeListActions from '../../../../redux/catalog/actionReducer/productTime/List';
import { CityDto } from '../../../../dtos/commonDtos';
import { PAGE_TITLE_VENDOR_BRANCH_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, params, query }) {
  const { backUrl = '/vendor-Branch/list' } = query;
  const tabSelected = query.tab;
  try {
    store.dispatch(
      singleVendorBranchInitActions.singleVendorBranchInitRequest(),
    );
    /**
     * get single vendor bRanch  by id for edit
     */
    const vendorBranchDtos = await getVendorBranchApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchDto({
              id: params.id,
            }),
          )
          .includes(['districtDto', 'imageDtos'])
          .buildJson(),
      ),
    );

    /**
     * get all vendors with true status
     */
    const vendorDtos = await getAgentApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new AgentDto({
              active: true,
            }),
          )
          .buildJson(),
      ),
    );
    /**
     * get all Secondary Level categories  with default clause secondaryLevel
     */
    const categoryDtos = await getCategoryApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new CategoryDto({
              secondaryLevel: true,
              published: true,
            }),
          )
          .buildJson(),
      ),
    );

    /**
     * get all cities for first time with default clause
     */
    const cityDtos = await getCityApi(
      store.getState().login.data.token,
      getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
    );

    if (
      vendorDtos.status === 200 &&
      vendorBranchDtos.status === 200 &&
      categoryDtos.status === 200 &&
      cityDtos.status === 200
    ) {
      /**
       * get product time with vendorBranchId
       */
      const jsonListProductTime = new BaseGetDtoBuilder()
        .dto(
          new PreparingProductTimeDto({
            vendorBranchDto: new VendorBranchDto({
              id: vendorBranchDtos.data.items[0].id,
            }),
          }),
        )
        .buildJson();

      const productTimes = await getProductTimeApi(
        store.getState().login.data.token,
        getDtoQueryString(jsonListProductTime),
      );
      if (productTimes.status === 200) {
        store.dispatch(
          singleVendorBranchActions.singleVendorBranchSuccess(
            vendorBranchDtos.data,
          ),
        );
        store.dispatch(vendorListActions.vendorListSuccess(vendorDtos.data));
        store.dispatch(
          secondaryLevelListActions.secondaryLevelListSuccess(
            categoryDtos.data,
          ),
        );
        store.dispatch(cityListActions.cityListSuccess(cityDtos.data));
        store.dispatch(
          productTimeListActions.productTimeListSuccess(productTimes.data),
        );

        store.dispatch(
          singleVendorBranchInitActions.singleVendorBranchInitSuccess(),
        );
      } else {
        store.dispatch(
          singleVendorBranchInitActions.singleVendorBranchInitFailure(),
        );
      }
    } else {
      store.dispatch(
        singleVendorBranchInitActions.singleVendorBranchInitFailure(),
      );
    }
  } catch (error) {
    store.dispatch(
      singleVendorBranchInitActions.singleVendorBranchInitFailure(),
    );
  }

  return {
    chunks: ['vendorBranchCreate'],
    title: `${PAGE_TITLE_VENDOR_BRANCH_EDIT}`,
    component: (
      <Layout>
        <VendorBranchEdit backUrl={backUrl} tabSelected={tabSelected} />
      </Layout>
    ),
  };
}

export default action;
