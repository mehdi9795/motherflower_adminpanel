import React from 'react';
import VendorEdit from './VendorEdit';
import Layout from '../../../../components/Template/Layout/Layout';
import singleVendorAgentActions from '../../../../redux/vendor/actionReducer/vendor/Single';
import singleVendorAgentInitActions from '../../../../redux/vendor/actionReducer/vendor/SingleVendorAgentInit';
import { AgentDto } from '../../../../dtos/vendorDtos';
import { getAgentApi } from '../../../../services/vendorApi';
import { getDtoQueryString } from '../../../../utils/helper';
import productTypesLIstActions from '../../../../redux/catalog/actionReducer/productType/List';
import { getProductTypesApi } from '../../../../services/catalogApi';
import { ProductTypeDto } from '../../../../dtos/catalogDtos';
import { PAGE_TITLE_VENDOR_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query, params }) {
  const { backUrl = '/vendor/list' } = query;
  try {
    store.dispatch(singleVendorAgentInitActions.singleVendorAgentInitRequest());

    /**
     * get single vendor agent by id for edit
     */
    const jsonSingleAgent = new BaseGetDtoBuilder()
      .dto(
        new AgentDto({
          id: params.id,
          active: true,
        }),
      )
      .includes(['imageDtos'])
      .buildJson();
    const singleAgent = await getAgentApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonSingleAgent),
    );

    /**
     * get all product types with true status in active field
     */
    const productTypes = await getProductTypesApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new ProductTypeDto({ published: true }))
          .buildJson(),
      ),
    );

    if (singleAgent.status === 200 && productTypes.status === 200) {
      store.dispatch(
        singleVendorAgentActions.singleVendorAgentSuccess(singleAgent.data),
      );
      store.dispatch(
        productTypesLIstActions.productTypesListSuccess(productTypes.data),
      );
    } else {
      store.dispatch(
        singleVendorAgentInitActions.singleVendorAgentInitFailure(),
      );
    }
  } catch (error) {
    store.dispatch(singleVendorAgentInitActions.singleVendorAgentInitFailure());
  }

  return {
    chunks: ['vendorCreate'],
    title: `${PAGE_TITLE_VENDOR_EDIT}`,
    component: (
      <Layout>
        <VendorEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
