import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './VendorEdit.css';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR,
  VENDOR_EDIT,
} from '../../../../Resources/Localization';
import VendorForm from '../../../../components/Vendor/Agent/VendorForm/VendorForm';
import history from '../../../../history';
import vendorAgentDeleteActions from '../../../../redux/vendor/actionReducer/vendor/Delete';

class VendorEdit extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
    vendor: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    backUrl: '/edit/list',
    vendor: null,
  };

  delete = () => {
    const { vendor } = this.props;

    const deleteData = {
      id: vendor.id,
      status: 'edit',
    };

    this.props.actions.vendorAgentDeleteRequest(deleteData);
  };

  redirect = id => {
    history.push(`/edit/list/${id}`);
  };

  render() {
    const { backUrl, vendor } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard
          title={`${VENDOR_EDIT} ${vendor && vendor.name}`}
          backUrl={backUrl}
          isEdit={vendor && vendor.id > 0}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR}
          onDelete={this.delete}
        >
          <VendorForm />
        </EditCard>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  vendor:
    state.singleVendorAgent.data &&
    state.singleVendorAgent.data.items[0]
      ? state.singleVendorAgent.data.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorAgentDeleteRequest: vendorAgentDeleteActions.vendorAgentDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(VendorEdit),
);
