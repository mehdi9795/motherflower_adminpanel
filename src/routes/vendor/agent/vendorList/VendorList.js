import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import HotKeys from 'react-hot-keys';
import CPList from '../../../../components/CP/CPList/CPList';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import s from './VendorList.css';
import Link from '../../../../components/Link/Link';
import CPSwitch from '../../../../components/CP/CPSwitch';
import history from '../../../../history';
import CPAvatar from '../../../../components/CP/CPAvatar/CPAvatar';
import { AgentDto } from '../../../../dtos/vendorDtos';
import {
  NAME,
  EMAIL,
  STATUS,
  CREATION,
  AGENT_BRANCHES,
  ACTIVE,
  DEACTIVATE,
  YES,
  NO,
  VENDOR_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR,
  PRODUCT_CREATE,
  CODE,
} from '../../../../Resources/Localization';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import vendorAgentDeleteActions from '../../../../redux/vendor/actionReducer/vendor/Delete';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import {pressEnterAndCallApplySearch} from "../../../../utils/helper";

class VendorList extends React.Component {
  static propTypes = {
    vendorLoading: PropTypes.bool,
    vendorCount: PropTypes.number,
    vendors: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    vendorCount: 0,
    vendorLoading: false,
    vendors: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      vendorName: '',
      vendorStatus: true,
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    /**
     * get all vendors with true status
     */

    const deleteData = {
      id,
      listDto: new BaseGetDtoBuilder()
        .dto(new AgentDto({ active: true }))
        .includes(['imageDtos'])
        .buildJson(),
      status: 'list',
    };

    this.props.actions.vendorAgentDeleteRequest(deleteData);
  };

  /**
   * for submit search in vendor
   */
  prepareContainer = () => {
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new AgentDto({
          active: this.state.vendorStatus,
          name: this.state.vendorName,
        }),
      )
      .includes(['imageDtos'])
      .buildJson();
    return jsonList;
  };

  /**
   * set value inputs into state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  /**
   * submit search in vendor
   */
  async submitSearch() {
    this.props.actions.vendorListRequest(this.prepareContainer());
  }

  /**
   * vendor list add button click handler
   */
  vendorAddClick = () => {
    this.props.actions.showLoading();
    history.push('/vendor/create');
  };

  render() {
    const { vendors, vendorLoading, vendorCount } = this.props;
    const { vendorStatus, vendorName } = this.state;

    const columns = [
      {
        title: '',
        dataIndex: 'url',
        width: 50,
        render: (text, record) => <CPAvatar src={record.imgUrl} />,
      },
      { title: CODE, dataIndex: 'code', key: 'code', width: 50 },
      { title: NAME, dataIndex: 'name', key: 'name', width: 250 },
      { title: EMAIL, dataIndex: 'email', key: 'email', width: 250 },
      { title: CREATION, dataIndex: 'createdOnUtc', key: 'createdOnUtc', width: 150 },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 90,
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        width: 60,
        render: (text, record) => (
          <span>
            <Link
              to={`/vendor-branch/list?vendorId=${record.key}`}
              onClick={() => this.props.actions.showLoading()}
            >
              {AGENT_BRANCHES}
            </Link>
          </span>
        ),
      },
      {
        title: '',
        width: 100,
        render: (text, record) => (
          <span>
            <Link
              to={`/product/create?vendorId=${record.key}&back=vendor`}
              onClick={() => this.props.actions.showLoading()}
            >
              {PRODUCT_CREATE}
            </Link>
          </span>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to={`/vendor/edit/${record.key}`}
              onClick={() => {
                this.props.actions.showLoading();
              }}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];
    const vendorListArray = [];

    /**
     * maping vendors list data source
     */
    if (vendors)
      vendors.map(item => {
        const obj = {
          key: item.id,
          code: item.code,
          name: item.name,
          email: item.email,
          active: item.active,
          createdOnUtc: item.persianCreatedOnUtc,
          imgUrl:
            item.imageDtos && item.imageDtos.length > 0
              ? `${item.imageDtos[0].url}`
              : ` `,
        };
        return vendorListArray.push(obj);
      });

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.vendorAddClick}>
        <div className={s.mainContent}>
          <h3>{VENDOR_LIST}</h3>
          <CPList
            data={vendorListArray}
            count={vendorCount}
            columns={columns}
            loading={vendorLoading}
            onAddClick={this.vendorAddClick}
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="name"
                      value={vendorName}
                      label={NAME}
                      onChange={value => {
                        this.handelChangeInput(
                          'vendorName',
                          value.target.value,
                        );
                      }}
                    />
                  </div>

                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <div className="align-center">
                      <CPSwitch
                        defaultChecked={vendorStatus}
                        unCheckedChildren={DEACTIVATE}
                        checkedChildren={ACTIVE}
                        onChange={value => {
                          this.handelChangeInput('vendorStatus', value);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="field">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  vendors: state.vendorList.data.items,
  vendorCount: state.vendorList.data.count ? state.vendorList.data.count : 0,
  vendorLoading: state.vendorList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
      showLoading,
      vendorListRequest: vendorListActions.vendorListRequest,
      vendorAgentDeleteRequest:
        vendorAgentDeleteActions.vendorAgentDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorList));
