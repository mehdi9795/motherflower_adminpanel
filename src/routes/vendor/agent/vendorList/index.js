import React from 'react';
import VendorList from './VendorList';
import Layout from '../../../../components/Template/Layout/Layout';
import { AgentDto } from '../../../../dtos/vendorDtos';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import { PAGE_TITLE_VENDOR_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all vendors with true status
   */
  store.dispatch(
    vendorListActions.vendorListRequest(
      new BaseGetDtoBuilder()
        .dto(new AgentDto({ active: true }))
        .includes(['imageDtos'])
        .buildJson(),
    ),
  );

  return {
    chunks: ['vendorList'],
    title: `${PAGE_TITLE_VENDOR_LIST}`,
    component: (
      <Layout>
        <VendorList />
      </Layout>
    ),
  };
}

export default action;
