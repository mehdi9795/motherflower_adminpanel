import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './VendorCreate.css';
import { VENDOR_CREATE } from '../../../../Resources/Localization';
import VendorForm from '../../../../components/Vendor/Agent/VendorForm/VendorForm';

class VendorCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
  };

  render() {
    const { backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={VENDOR_CREATE} backUrl={backUrl}>
          <VendorForm />
        </EditCard>
      </div>
    );
  }
}

export default withStyles(s)(VendorCreate);
