import React from 'react';
import VendorCreate from './VendorCreate';
import Layout from '../../../../components/Template/Layout/Layout';
import productTypesListActions from '../../../../redux/catalog/actionReducer/productType/List';
import singleVendorAgentActions from '../../../../redux/vendor/actionReducer/vendor/Single';
import singleVendorAgentInitActions from '../../../../redux/vendor/actionReducer/vendor/SingleVendorAgentInit';
import { getProductTypesApi } from '../../../../services/catalogApi';
import { getDtoQueryString } from '../../../../utils/helper';
import { ProductTypeDto } from '../../../../dtos/catalogDtos';
import { PAGE_TITLE_VENDOR_CREATE } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query }) {
  const { backUrl = '/vendor/list' } = query;
  /**
   * get all product types with true status in active field
   */
  try {
    store.dispatch(singleVendorAgentInitActions.singleVendorAgentInitRequest());

    const productTypes = await getProductTypesApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new ProductTypeDto({ published: true }))
          .buildJson(),
      ),
    );

    if (productTypes.status === 200) {
      store.dispatch(
        productTypesListActions.productTypesListSuccess(productTypes.data),
      );
      store.dispatch(singleVendorAgentActions.singleVendorAgentSuccess(null));
    } else {
      store.dispatch(
        singleVendorAgentInitActions.singleVendorAgentInitFailure(),
      );
    }
  } catch (error) {
    store.dispatch(singleVendorAgentInitActions.singleVendorAgentInitFailure());
  }

  return {
    chunks: ['vendorCreate'],
    title: `${PAGE_TITLE_VENDOR_CREATE}`,
    component: (
      <Layout>
        <VendorCreate backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
