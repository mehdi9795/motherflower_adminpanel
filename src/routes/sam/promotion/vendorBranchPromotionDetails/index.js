import React from 'react';
import VendorBranchPromotionDetails from './VendorBranchPromotionDetails';
import Layout from '../../../../components/Template/Layout/Layout';
import {
  VendorBranchPromotionDetailDto,
  VendorBranchPromotionDto,
} from '../../../../dtos/samDtos';
import categoryPromotionDetailsListActions from '../../../../redux/sam/actionReducer/promotion/CategoryPromotionDetailsList';
import productPromotionDetailsListActions from '../../../../redux/sam/actionReducer/promotion/ProductPromotionDetailsList';
import { PAGE_TITLE_PROMOTION_DETAILS } from '../../../../Resources/Localization';
import { BaseListGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query }) {
  const jsonListVendorBranchPromotion = new BaseListGetDtoBuilder()
    .dtos(
      new VendorBranchPromotionDetailDto({
        vendorBranchPromotionDto: new VendorBranchPromotionDto({
          id: query.id,
        }),
        vendorBranchPromotionDetailType: 'Category',
      }),
    )
    .includes(['Category'])
    .buildJson();
  store.dispatch(
    categoryPromotionDetailsListActions.categoryPromotionDetailsListRequest(
      jsonListVendorBranchPromotion,
    ),
  );

  const jsonListVendorBranchPromotionProduct = new BaseListGetDtoBuilder()
    .dtos(
      new VendorBranchPromotionDetailDto({
        vendorBranchPromotionDto: new VendorBranchPromotionDto({
          id: query.id,
        }),
        vendorBranchPromotionDetailType: 'Product',
      }),
    )
    .includes(['Product', 'Category'])
    .buildJson();
  store.dispatch(
    productPromotionDetailsListActions.productPromotionDetailsListRequest(
      jsonListVendorBranchPromotionProduct,
    ),
  );

  const { backUrl = `/promotion/edit/${query.promotionId}` } = query;
  return {
    chunks: ['vendorBranchPromotionDetails'],
    title: `${PAGE_TITLE_PROMOTION_DETAILS}`,
    component: (
      <Layout>
        <VendorBranchPromotionDetails backUrl={backUrl} query={query} />
      </Layout>
    ),
  };
}

export default action;
