import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { hideLoading } from 'react-redux-loading-bar';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import EditCard from '../../../../components/CP/EditCard';
import s from './VendorBranchPromotionDetails.css';
import { PROMOTION_MANAGEMENT } from '../../../../Resources/Localization';
import VendorBranchPromotionDetailsForm from '../../../../components/Sam/Promotion/VendorBranchPromotionDetailsForm';

class VendorBranchPromotionDetails extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string,
    query: PropTypes.objectOf(PropTypes.object).isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    backUrl: '/promotion/list',
  };

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  render() {
    const { backUrl, query } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={` ${PROMOTION_MANAGEMENT}`} backUrl={backUrl}>
          <VendorBranchPromotionDetailsForm query={query} />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(VendorBranchPromotionDetails),
);
