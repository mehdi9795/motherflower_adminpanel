import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import s from './PromotionList.css';
import {
  FROM_DATE,
  TO_DATE,
  ALL_PROMOTIONS,
  PROMOTION_TYPE,
  NAME,
  STATUS,
  ACTIVE,
  DEACTIVATE,
  YES,
  NO,
  PROMOTION_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PROMOTION,
} from '../../../../Resources/Localization';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPSwitch from '../../../../components/CP/CPSwitch';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import { VendorBranchPromotionDto } from '../../../../dtos/samDtos';
import promotionListActions from '../../../../redux/sam/actionReducer/promotion/PromotionList';
import promotionDeleteActions from '../../../../redux/sam/actionReducer/promotion/PromotionDelete';
import history from '../../../../history';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import {pressEnterAndCallApplySearch} from "../../../../utils/helper";

const { Option } = Select;

class PromotionList extends React.Component {
  static propTypes = {
    promotionLoading: PropTypes.bool,
    promotionCount: PropTypes.number,
    promotions: PropTypes.arrayOf(PropTypes.object),
    promotionTypes: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    promotionTypes: [],
    promotions: [],
    promotionLoading: false,
    promotionCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      promotionType: '0',
      promotionName: '',
      promotionStatus: true,
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    /**
     * get all promotions for first time with default where clause
     */
    const deleteData = {
      id,
      status: 'list',
      listDto: new BaseGetDtoBuilder()
        .dto(
          new VendorBranchPromotionDto({
            active: this.state.promotionStatus,
          }),
        )
        .buildJson(),
    };
    this.props.actions.promotionDeleteRequest(deleteData);
  };

  promotionAddClick = () => {
    this.props.actions.showLoading();
    history.push('/promotion/create');
  };

  /**
   * set value inputs into state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  /**
   * Search promotions based on promotion selected values and names.
   * @returns {Promise<void>}
   */
  async submitSearch() {
    const { promotionStatus, promotionType, promotionName } = this.state;
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchPromotionDto({
          active: promotionStatus,
          promotionType,
          name: promotionName,
        }),
      )
      .buildJson();
    this.props.actions.promotionListRequest(jsonList);
  }

  render() {
    const {
      promotions,
      promotionLoading,
      promotionCount,
      promotionTypes,
    } = this.props;
    const { promotionName, promotionType, promotionStatus } = this.state;
    const columns = [
      { title: NAME, dataIndex: 'name', key: 'name', width: 250 },
      {
        title: FROM_DATE,
        dataIndex: 'persianFromDateUtc',
        key: 'persianFromDateUtc',
        width: 100,
      },
      {
        title: TO_DATE,
        dataIndex: 'persianToDateUtc',
        key: 'persianToDateUtc',
        width: 100,
      },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 50,
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 80,
        render: (text, record) => (
          <div>
            {!record.expired ? (
              <CPPopConfirm
                title={ARE_YOU_SURE_WANT_TO_DELETE_THE_PROMOTION}
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() => this.onDelete(record.key)}
              >
                <CPButton className="delete_action">
                  <i className="cp-trash" />
                </CPButton>
              </CPPopConfirm>
            ) : (
              <label>منقضی شد</label>
            )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to={`/promotion/edit/${record.key}`}
              onClick={() => {
                this.props.actions.showLoading();
              }}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];
    const promotionTypesArray = [];

    /**
     * map promotion Types for comboBox Datasource
     */
    promotionTypes.map(item =>
      promotionTypesArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <div className={s.mainContent}>
        <h3>{PROMOTION_LIST}</h3>
        <CardTable
          data={promotions}
          count={promotionCount}
          columns={columns}
          loading={promotionLoading}
          onAddClick={this.promotionAddClick}
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <CPInput
                    name="name"
                    label={NAME}
                    value={promotionName}
                    onChange={value => {
                      this.handelChangeInput(
                        'promotionName',
                        value.target.value,
                      );
                    }}
                  />
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{PROMOTION_TYPE}</label>
                  <CPSelect
                    value={promotionType}
                    onChange={value => {
                      this.handelChangeInput('promotionType', value);
                    }}
                  >
                    <Option key={0}>{ALL_PROMOTIONS}</Option>
                    {promotionTypesArray}
                  </CPSelect>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <div className="align-center">
                    <CPSwitch
                      defaultChecked={promotionStatus}
                      unCheckedChildren={DEACTIVATE}
                      checkedChildren={ACTIVE}
                      onChange={value => {
                        this.handelChangeInput('promotionStatus', value);
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CardTable>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  promotions: state.promotionList.data ? state.promotionList.data.items : [],
  promotionCount:
    state.promotionList.data && state.promotionList.data.items && state.promotionList.data.items.length > 0
      ? state.promotionList.data.count
      : 0,
  promotionLoading: state.promotionList.loading,
  promotionTypes: state.promotionTypeList.data.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      promotionDeleteRequest: promotionDeleteActions.promotionDeleteRequest,
      promotionListRequest: promotionListActions.promotionListRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PromotionList));
