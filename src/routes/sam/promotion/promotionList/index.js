import React from 'react';
import PromotionList from './PromotionList';
import Layout from '../../../../components/Template/Layout/Layout';
import promotionListActions from '../../../../redux/sam/actionReducer/promotion/PromotionList';
import promotionTypeListActions from '../../../../redux/sam/actionReducer/promotion/PromotionTypeList';
import { VendorBranchPromotionDto } from '../../../../dtos/samDtos';
import { PAGE_TITLE_PROMOTION_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all promotions for first time with default where clause
   */
  store.dispatch(
    promotionListActions.promotionListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new VendorBranchPromotionDto({
            active: true,
          }),
        )
        .buildJson(),
    ),
  );

  /**
   * get all promotion types for first time
   */
  store.dispatch(
    promotionTypeListActions.promotionTypeListRequest(
      new BaseGetDtoBuilder().buildJson(),
    ),
  );

  return {
    chunks: ['promotionList'],
    title: `${PAGE_TITLE_PROMOTION_LIST}`,
    component: (
      <Layout>
        <PromotionList />
      </Layout>
    ),
  };
}

export default action;
