import React from 'react';
import PromotionEdit from './PromotionEdit';
import Layout from '../../../../components/Template/Layout/Layout';
import vendorBranchPromotionListActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionList';
import singlePromotionInitActions from '../../../../redux/sam/actionReducer/promotion/SinglePromotion';
import promotionTypeListActions from '../../../../redux/sam/actionReducer/promotion/PromotionTypeList';
import {
  PromotionDto,
  VendorBranchPromotionDto,
} from '../../../../dtos/samDtos';
import { getDtoQueryString } from '../../../../utils/helper';
import {
  getPromotionsApi,
  getPromotionTypeApi,
  getVendorBranchPromotionsApi,
} from '../../../../services/samApi';
import { PAGE_TITLE_PROMOTION_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import { RoleDto } from '../../../../dtos/identityDtos';
import roleActions from '../../../../redux/identity/actionReducer/role/Role';

async function action({ store, params, query }) {
  try {
    store.dispatch(singlePromotionInitActions.singlePromotionInitRequest());

    /**
     * get single promotion
     */

    const promotion = await getPromotionsApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new PromotionDto({
              id: params.id,
            }),
          )
          .includes(['walletPromotionDtos'])
          .buildJson(),
      ),
    );

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchPromotionDto({
          promotionDto: new PromotionDto({ id: params.id }),
        }),
      )
      .includes(['VendorBranch', 'Promotion'])
      .buildJson();

    const vendorBranchPromotion = await getVendorBranchPromotionsApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonList),
    );

    /**
     * get all promotion types for first time
     */
    const promotionType = await getPromotionTypeApi(
      store.getState().login.data.token,
      getDtoQueryString(new BaseGetDtoBuilder().buildJson()),
    );

    /**
     * get all role
     */
    store.dispatch(
      roleActions.roleListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new RoleDto({
              roleStatus: 'System',
            }),
          )
          .disabledCount(true)
          .buildJson(),
      ),
    );

    if (
      promotion.status === 200 &&
      vendorBranchPromotion.status === 200 &&
      promotionType.status === 200
    ) {
      store.dispatch(
        singlePromotionInitActions.singlePromotionInitSuccess(promotion.data),
      );
      store.dispatch(
        vendorBranchPromotionListActions.vendorBranchPromotionListSuccess(
          vendorBranchPromotion.data,
        ),
      );
      store.dispatch(
        promotionTypeListActions.promotionTypeListSuccess(promotionType.data),
      );
    } else {
      store.dispatch(singlePromotionInitActions.singlePromotionInitFailure());
    }
  } catch (error) {
    store.dispatch(singlePromotionInitActions.singlePromotionInitFailure());
  }

  const { backUrl = '/promotion/list' } = query;
  return {
    chunks: ['promotionEdit'],
    title: `${PAGE_TITLE_PROMOTION_EDIT}`,
    component: (
      <Layout>
        <PromotionEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
