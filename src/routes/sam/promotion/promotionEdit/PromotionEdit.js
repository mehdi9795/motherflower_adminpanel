import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './PromotionEdit.css';
import history from '../../../../history';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PROMOTION,
  PROMOTION_EDIT,
} from '../../../../Resources/Localization';
import PromotionForm from '../../../../components/Sam/Promotion/PromotionForm/PromotionForm';
import { VendorBranchPromotionDto } from '../../../../dtos/samDtos';
import promotionDeleteActions from '../../../../redux/sam/actionReducer/promotion/PromotionDelete';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

class PromotionEdit extends React.Component {
  static propTypes = {
    promotion: PropTypes.objectOf(PropTypes.any),
    backUrl: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    backUrl: '/promotion/list',
    promotion: {},
  };

  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
  }

  onDelete = () => {
    const { promotion } = this.props;
    /**
     * get all promotions for first time with default where clause
     */
    const deleteData = {
      id: promotion.id,
      status: 'edit',
      listDto: new BaseGetDtoBuilder()
        .dto(
          new VendorBranchPromotionDto({
            active: true,
          }),
        )
        .buildJson(),
    };
    this.props.actions.promotionDeleteRequest(deleteData);
  };

  redirect = () => {
    history.push('/promotion/list');
  };

  render() {
    const { promotion, backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard
          title={` ${PROMOTION_EDIT} - ${promotion && promotion.name}`}
          backUrl={backUrl}
          isEdit={
            promotion && promotion.id && (promotion && !promotion.expired)
          }
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_PROMOTION}
          onDelete={this.onDelete}
        >
          <PromotionForm promotion={promotion} />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  promotion: state.singlePromotion.data && state.singlePromotion.data.items
    ? state.singlePromotion.data.items[0]
    : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      promotionDeleteRequest: promotionDeleteActions.promotionDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PromotionEdit));
