import React from 'react';
import PromotionCreate from './PromotionCreate';
import Layout from '../../../../components/Template/Layout/Layout';
import singlePromotionInitActions from '../../../../redux/sam/actionReducer/promotion/SinglePromotion';
import promotionTypeListActions from '../../../../redux/sam/actionReducer/promotion/PromotionTypeList';
import { getPromotionTypeApi } from '../../../../services/samApi';
import { getDtoQueryString } from '../../../../utils/helper';
import { PAGE_TITLE_PROMOTION_CREATE } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import roleActions from '../../../../redux/identity/actionReducer/role/Role';
import { RoleDto } from '../../../../dtos/identityDtos';

async function action({ store, query }) {
  const { backUrl = '/promotion/list' } = query;

  try {
    store.dispatch(singlePromotionInitActions.singlePromotionInitSuccess(null));

    /**
     * get all promotion types for first time
     */

    const promotionType = await getPromotionTypeApi(
      store.getState().login.data.token,
      getDtoQueryString(new BaseGetDtoBuilder().buildJson()),
    );

    /**
     * get all role
     */
    store.dispatch(
      roleActions.roleListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new RoleDto({
              roleStatus: 'System',
            }),
          )
          .disabledCount(true)
          .buildJson(),
      ),
    );

    if (promotionType.status === 200) {
      store.dispatch(
        promotionTypeListActions.promotionTypeListSuccess(promotionType.data),
      );
    } else {
      store.dispatch(singlePromotionInitActions.singlePromotionInitFailure());
    }
  } catch (error) {
    store.dispatch(singlePromotionInitActions.singlePromotionInitFailure());
  }

  return {
    chunks: ['promotionCreate'],
    title: `${PAGE_TITLE_PROMOTION_CREATE}`,
    component: (
      <Layout>
        <PromotionCreate backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
