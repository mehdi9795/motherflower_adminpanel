import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './PromotionCreate.css';
import { PROMOTION_CREATE } from '../../../../Resources/Localization';
import PromotionForm from '../../../../components/Sam/Promotion/PromotionForm/PromotionForm';

class promotionCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
  };

  render() {
    const { backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={PROMOTION_CREATE} backUrl={backUrl}>
          <PromotionForm />
        </EditCard>
      </div>
    );
  }
}

export default withStyles(s)(promotionCreate);
