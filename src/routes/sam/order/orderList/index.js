import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import OrderList from './OrderList';
import { BillDto } from '../../../../dtos/samDtos';
import { getBillStatusApi } from '../../../../services/samApi';
import { getDtoQueryString } from '../../../../utils/helper';
import {
  orderStatusListFailure,
  orderStatusListSuccess,
} from '../../../../redux/sam/action/order';
import orderListActions from '../../../../redux/sam/actionReducer/order/OrderList';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import { getVendorBranchApi } from '../../../../services/vendorApi';
import { PAGE_TITLE_ORDER_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query }) {
  const jsonListBill = new BaseGetDtoBuilder()
    .dto(
      new BillDto({
        fromPanel: true,
        code: query.code ? query.code : '',
        searchBillStatus: [],
        published: true,
      }),
    )
    .includes(['userDto', 'addressDto', 'billItemDtos', 'vendorBranchPromotionDetailDto'])
    .pageSize(20)
    .pageIndex(0)
    .buildJson();

  store.dispatch(orderListActions.orderListRequest(jsonListBill));

  /**
   * get all status bill
   */
  const response = await getBillStatusApi(
    store.getState().login.data.token,
    getDtoQueryString(new BaseGetDtoBuilder().dto(new BillDto()).buildJson()),
  );

  if (response.status === 200) {
    store.dispatch(orderStatusListSuccess(response.data));
  } else {
    store.dispatch(orderStatusListFailure());
  }

  /**
   * get all vendor branch for first time with default clause
   */
  const vendorBranchData = await getVendorBranchApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new VendorBranchDto({
            active: true,
          }),
        )
        .all(true)
        .buildJson(),
    ),
  );
  store.dispatch(
    vendorBranchListActions.vendorBranchListSuccess(vendorBranchData.data),
  );

  return {
    chunks: ['orderList'],
    title: `${PAGE_TITLE_ORDER_LIST}`,
    component: (
      <Layout>
        <OrderList orderCode={query.code} />
      </Layout>
    ),
  };
}

export default action;
