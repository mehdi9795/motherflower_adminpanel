/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import cs from 'classnames';
import { Select, message, Spin } from 'antd';
import s from './OrderList.css';
import {
  FROM_DATE,
  TO_DATE,
  STATUS,
  ID,
  ORDER_LIST,
  DISCOUNT_CODE,
  PROMOTION,
  USER_USERNAME,
  EMAIL,
  COUNT,
  TOTAL_PRICE,
  TOMAN,
  CANCEL_REQUEST,
  DETAILS,
  CHANGE_STATUS,
  ADDRESS,
  ORDER_DATE,
  PHONE,
  FACTOR_INFO,
  SENDER_NAME,
  PAYABLE_BILLING_AMOUNT,
  RECEIVER_NAME,
  YES,
  NO,
  ARE_YOU_SURE_WANT_TO_CANCEL_REQUEST,
  MESSAGE_CREATE_IS_SUCCESS,
  CATEGORY,
  TIME,
  ARE_YOU_SURE_WANT_TO_ORDER_STATUS_CHANGE,
  TEXT_COPY,
  UPDATE,
  VENDOR_BRANCH,
  DELIVERY_TYPE,
  EVENT,
  EVENT_UPDATE_IS_SUCCESS,
  EVENT_UPDATE_IS_FAILURE,
  DESCRIPTION,
  DRIVER,
  REQUESTED_DATE,
  REQUESTED_HOUR,
  INSERT_DATE,
  PENDING_ORDER,
  CANCEL_BY_SYSTEM,
  COMPLETE_ORDER,
  BANK_PENDING_ORDER,
  BANK_ERROR_ORDER,
  CLIENT_INFO,
  RECEIVER_INFO,
  MAP,
  CONFIRMED_ORDER,
  DELIVERY_DATE,
  DELIVERY_HOUR,
  DISTRICT,
  CREATE,
  CANCEL_FROM_PORT,
  AMOUNT_SHIPPING,
  TOTAL_DISCOUNT,
  CITY,
  SAVE,
  SELECT_PRODUCT_FOR_PROMOTION,
  SELECTED_ADDRESS,
  PAYMENT,
  PICKUP,
  DELIVERY,
  ALL,
  ARE_YOU_SURE_WANT_TO_DELETE_REQUEST,
  CHECKOUT,
} from '../../../../Resources/Localization';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import Link from '../../../../components/Link/Link';
import { BillDto, BillItemDto } from '../../../../dtos/samDtos';
import {
  DriverDto,
  VendorBranchDto,
  VendorBranchUserDto,
  VendorBranchCitiesDto,
  CalendarExceptionDto,
  AgentDto,
  AvailableProductShiftDto,
} from '../../../../dtos/vendorDtos';
import CPMultiSelect from '../../../../components/CP/CPMultiSelect';
import CPPersianCalendar from '../../../../components/CP/CPPersianCalendar';
import CPIntlPhoneInput from '../../../../components/CP/CPIntlPhoneInput';
import { AddressDto, UserDto } from '../../../../dtos/identityDtos';
import orderListActions from '../../../../redux/sam/actionReducer/order/OrderList';
import orderPutActions from '../../../../redux/sam/actionReducer/order/OrderPut';
import occasionTypeListActions from '../../../../redux/common/actionReducer/occasionType/List';
import districtListActions from '../../../../redux/common/actionReducer/district/List';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import CPNestedTable from '../../../../components/CP/CPNestedTable';
import CPAvatar from '../../../../components/CP/CPAvatar';
import CPModal from '../../../../components/CP/CPModal';
import CPPanel from '../../../../components/CP/CPPanel';
import {
  getBillItemStatusApi,
  getBillStatusApi,
  putBillApi,
  putBillItemApi,
} from '../../../../services/samApi';
import {
  getDtoQueryString,
  pressEnterAndCallApplySearch,
} from '../../../../utils/helper';
import CPPermission from '../../../../components/CP/CPPermission/CPPermission';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import { MessageDto, RequestTypeDto } from '../../../../dtos/notificationDtos';
import { PostNotificationApi } from '../../../../services/notificationApi';
import { getCookie } from '../../../../utils';
import CPPagination from '../../../../components/CP/CPPagination';
import CPTextArea from '../../../../components/CP/CPTextArea';
import CPCarousel from '../../../../components/CP/CPCarousel';
import CPConfirmation from '../../../../components/CP/CPConfirmation';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import {
  getVendorBranchApi,
  getVendorBranchCityApi,
  getVendorBranchUserApi,
} from '../../../../services/vendorApi';
import CPMap from '../../../../components/CP/CPMap';
import {
  CityDto,
  DistrictDto,
  OccasionTypeDto,
} from '../../../../dtos/commonDtos';
import {
  getCityApi,
  getDistrictApi,
  getNearestLocationsApi,
} from '../../../../services/commonApi';
import userAddressPutActions from '../../../../redux/identity/actionReducer/user/UserAddressPut';
import CPList from '../../../../components/CP/CPList';
import userAddressActions from '../../../../redux/identity/actionReducer/user/UserAddressList';
import calendarExceptionActions from '../../../../redux/vendor/actionReducer/vendorBranch/CalendarException';
import availableProductShiftActions from '../../../../redux/vendor/actionReducer/vendorBranch/AvailableProductShift';
import orderItemPutActions from '../../../../redux/sam/actionReducer/order/OrderItemPut';
import CPDatepicker from '../../../../components/CP/CPDatepicker';
import { ProductDto } from '../../../../dtos/catalogDtos';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';

const { Option } = Select;

class OrderList extends React.Component {
  static propTypes = {
    orderLoading: PropTypes.bool,
    orderCount: PropTypes.number,
    orders: PropTypes.arrayOf(PropTypes.object),
    orderStatuses: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    occasionTypes: PropTypes.arrayOf(PropTypes.object),
    districtOptions: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    loginData: PropTypes.objectOf(PropTypes.any),
    calendarException: PropTypes.objectOf(PropTypes.any),
    availableProductShift: PropTypes.objectOf(PropTypes.any),
    orderCode: PropTypes.string,
    userAddresses: PropTypes.arrayOf(PropTypes.object),
    userAddressCount: PropTypes.number,
    userAddressLoading: PropTypes.bool,
    calendarExceptionLoading: PropTypes.bool,
  };

  static defaultProps = {
    orderLoading: false,
    orderCount: 0,
    orders: [],
    orderStatuses: [],
    vendorBranches: [],
    occasionTypes: [],
    cities: [],
    districtOptions: [],
    loginData: {},
    calendarException: {},
    availableProductShift: {},
    orderCode: '',
    userAddresses: null,
    userAddressCount: 0,
    userAddressLoading: false,
    calendarExceptionLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      vendorBranchIds: [],
      statusId: [],
      fromDate: null,
      toDate: null,
      userName: '',
      code: props.orderCode ? props.orderCode : '',
      id: 0,
      userNameFinal: '',
      discountCode: 'all',
      promotion: 'all',
      visibleOrderDetailModal: false,
      fullName: '',
      phone: '',
      email: '',
      address: '',
      receiverName: '',
      receiverPhone: '',
      orderDate: '',
      totalAmount: '',
      totalDiscount: '',
      totalShipmentAmount: '',
      payableBillingAmount: '',
      orderStatus: '',
      orderStatusArray: [],
      orderItemStatusArray: [],
      visibleOrderStatusModal: false,
      visibleOrderItemStatusModal: false,
      idOrderItemStatus: 0,
      idOrderStatus: 0,
      idOrder: 0,
      idOrderItem: 0,
      pageSize: 20,
      currentPage: 1,
      visibleOccasionModal: false,
      occasionText: '',
      visibleImageModal: false,
      visibleConfirm: false,
      dataImage: '',
      changeStatusConfirm: false,
      changeStatusBill: false,
      idStatus: 0,
      description: '',
      driverArray: [],
      driverId: 0,
      showComboboxDriver: false,
      editable: false,
      latOrder: 0,
      lngOrder: 0,
      markerTitle: '',
      productDetailUrlSite: '',
      productDetailUrlAdmin: '',
      fromOccasionText: '',
      toOccasionText: '',
      isEditOccasionText: true,
      isValidAddress: true,
      visibleAddressListModal: false,
      deliveryFromDate: '',
      deliveryToDate: '',
      searchDeliveryType: '3',
      visibleCalendarModal: false,
      searchCityId: '',
    };
    this.submitSearch = this.submitSearch.bind(this);
    this.showImageModal = this.showImageModal.bind(this);
    this.showOrderStatusModal = this.showOrderStatusModal.bind(this);
    this.handleOkChangeStatusOrder = this.handleOkChangeStatusOrder.bind(this);
    this.updateEvent = this.updateEvent.bind(this);
    this.showModalOccasion = this.showModalOccasion.bind(this);
    this.showDetailOrderModal = this.showDetailOrderModal.bind(this);
    this.handleOkChangeStatusOrderItem = this.handleOkChangeStatusOrderItem.bind(
      this,
    );
    this.availableProductShiftArray;
    this.appType = getCookie('appType');
  }

  componentWillMount() {
    /**
     * get all city
     */
    this.props.actions.cityListRequest(
      new BaseGetDtoBuilder().dto(new CityDto({ active: true })).buildJson(),
    );
  }

  componentDidMount() {
    const occasionTypeList = new BaseGetDtoBuilder()
      .dto(
        new OccasionTypeDto({
          published: true,
        }),
      )
      .includes(['occasionTypeSampleDtos'])
      .buildJson();

    this.props.actions.occasionTypeListRequest(occasionTypeList);
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  async componentWillReceiveProps(nextProps) {
    this.props.actions.hideLoading();

    if (
      !nextProps.calendarExceptionLoading &&
      this.props.calendarExceptionLoading
    )
      this.setState({ visibleCalendarModal: true });

    if (nextProps.availableProductShift) {
      this.availableProductShiftArray = [];
      if (this.state.deliveryType === 'Pickup') {
        if (
          nextProps.availableProductShift &&
          nextProps.availableProductShift.pickupItems
        )
          Object.values(nextProps.availableProductShift.pickupItems).map(item =>
            this.availableProductShiftArray.push(
              <Option key={item} value={item}>
                {item}
              </Option>,
            ),
          );
      } else if (this.state.deliveryType === 'Delivery') {
        if (
          nextProps.availableProductShift &&
          nextProps.availableProductShift.deliveryItems
        )
          Object.values(nextProps.availableProductShift.deliveryItems).map(
            item =>
              this.availableProductShiftArray.push(
                <Option key={item} value={item}>
                  {item}
                </Option>,
              ),
          );
      }
    }
  }

  handelChangeInput(inputName, value) {
    if (inputName === 'district') {
      const position = value.split('*');
      this.setState({
        latOrder: parseFloat(position[1]),
        lngOrder: parseFloat(position[2]),
        [inputName]: value,
      });
    } else if (inputName === 'idOrderItemStatus' && value.toString() === '13') {
      this.setState({ [inputName]: value, showComboboxDriver: false });
    } else if (inputName === 'idOrderItemStatus' && value.toString() === '9') {
      this.setState({ [inputName]: value, showComboboxDriver: true });
    } else if (inputName === 'searchCityId') {
      this.setState({ [inputName]: value });
      const vendorBranchData = new BaseGetDtoBuilder()
        .dto(
          new VendorBranchDto({
            active: true,
            cityId: value === 0 ? undefined : value,
          }),
        )
        .buildJson();
      this.props.actions.vendorBranchListRequest(vendorBranchData);
    } else {
      this.setState({ [inputName]: value });
    }
  }

  async submitSearch() {
    this.setState({ currentPage: 1 }, () => {
      this.props.actions.orderListRequest(this.prepareContainer());
    });
  }

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        userNameFinal: number.replace(/ /g, ''),
        userName: value,
      });
    }
  };

  async showImageModal(
    imgUrl,
    productId,
    vendorBranchId,
    idStatus = 4,
    idOrderItem,
    editable = false,
    priceOption,
  ) {
    const { loginData } = this.props;
    const orderItemStatusArray = [];
    let responseDriver = null;
    const driverArray = [];

    if (!(idStatus === 4 || idStatus === 6)) {
      const response = await getBillItemStatusApi(
        loginData.token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new BillItemDto({
                priceOption,
                billItemStatus: idStatus,
              }),
            )
            .buildJson(),
        ),
      );

      if (priceOption === 'Delivery' && idStatus === 1) {
        responseDriver = await getVendorBranchUserApi(
          loginData.token,
          getDtoQueryString(
            new BaseGetDtoBuilder()
              .dto(
                new VendorBranchUserDto({
                  roleDtos: [{ id: 4 }],
                  fromCurrentVendorBranch: loginData.appType !== 'Admin',
                  vendorBranchDto:
                    loginData.appType !== 'Admin'
                      ? new VendorBranchDto({ id: vendorBranchId })
                      : undefined,
                }),
              )
              .includes(['Users', 'Roles'])
              .buildJson(),
          ),
        );
      }

      if (response.status === 200) {
        response.data.items.map(item =>
          orderItemStatusArray.push(
            <Option key={item.id} value={item.id}>
              {item.name}
            </Option>,
          ),
        );

        if (responseDriver)
          responseDriver.data.items.map(item =>
            driverArray.push(
              <Option key={item.userDto.id} value={item.userDto.id}>
                {item.userDto.displayName}
              </Option>,
            ),
          );

        this.setState({
          orderItemStatusArray,
          idOrderItemStatus:
            response.data.items && response.data.items[0]
              ? response.data.items[0].id
              : 0,
          idOrderItem,
          driverArray,
          driverId:
            responseDriver &&
            responseDriver.data.items &&
            responseDriver.data.items.length > 0 &&
            responseDriver.data.items[0].userDto
              ? responseDriver.data.items[0].userDto.id
              : '',
          showComboboxDriver: priceOption === 'Delivery' && idStatus === 1,
        });
      }
    }
    const dataImage = [];
    const obj = {
      small: `${imgUrl}`,
      large: `${imgUrl}`,
      key: imgUrl,
    };
    dataImage.push(obj);
    // dataImage.push(obj);
    this.setState({
      dataImage,
      visibleImageModal: true,
      editable,
      changeStatusConfirm: false,
      idStatus,
      productDetailUrlSite: `https://motherflower.com/product-details/${productId}/${vendorBranchId}`,
      productDetailUrlAdmin: `https://centerlink.motherflower.com/product/edit/${productId}`,
    });
  }

  async downloadPdf(key, isColor = false) {
    const token = getCookie('token');
    const crudDto = new BaseCRUDDtoBuilder()
      .dto(
        new BillItemDto({
          id: key,
          isReadOccasionText: true,
        }),
      )
      .build();
    await putBillItemApi(token, crudDto);

    window.open(
      `https://api.motherflower.com/SAM/BillItem/OccasionTextPdf?isColor=${isColor}&container=${getDtoQueryString(
        JSON.stringify({ dto: { id: key } }),
      )}`,
    );
  }

  showAndHideCalendarModal = (
    value,
    vendorBranchId = undefined,
    productId = undefined,
    requestedDate = undefined,
    requestedHour = undefined,
    deliveryType = undefined,
    id = undefined,
    statusId = 0,
  ) => {
    if (statusId !== 0 && statusId !== 1 && statusId !== 9) return;

    if (value) {
      const calendarException = JSON.stringify(
        new CalendarExceptionDto({
          productDto: new ProductDto({
            id: productId,
            vendorDto: new AgentDto({
              vendorBranchDtos: [new VendorBranchDto({ id: vendorBranchId })],
            }),
          }),
          fromMobile: true,
        }),
      );

      this.availableProductShiftArray.push(
        <Option key={requestedHour} value={requestedHour}>
          {requestedHour}
        </Option>,
      );

      this.setState({
        selectedDayDefault: requestedDate.split('/')[2],
        requestedDateCalendar: requestedDate,
        requestedHourCalendar: requestedHour,
        productId,
        vendorBranchId,
        deliveryType,
        billItemId: id,
      });

      this.props.actions.calendarExceptionRequest(calendarException);

      const availableProductShift = new AvailableProductShiftDto({
        persianSelectedDate: requestedDate,
        productDto: new ProductDto({
          id: productId,
          vendorDto: new AgentDto({
            vendorBranchDtos: [new VendorBranchDto({ id: vendorBranchId })],
          }),
        }),
      });

      this.props.actions.availableProductShiftRequest(
        JSON.stringify(availableProductShift),
      );
    } else {
      this.setState({
        visibleCalendarModal: value,
      });
    }
  };

  rowRender = record => {
    const { orders, calendarExceptionLoading } = this.props;
    const bill = orders.find(item => item.id === record.key);
    const billItemArray = bill.billItemDtos;

    const dataSource = [];
    const columns = [
      {
        width: 85,
        title: '',
        dataIndex: 'url',
        render: (text, record2) => (
          <div
            onClick={() =>
              this.showImageModal(
                record2.imgUrl,
                record2.productId,
                record2.vendorBranchId,
                record2.statusId,
                record2.key,
                record2.editable,
                record2.deliveryType,
              )
            }
          >
            <CPAvatar src={record2.imgUrl} />{' '}
            <div>کد فروشگاه: {record2.vendorCode}</div>{' '}
          </div>
        ),
      },
      { title: COUNT, dataIndex: 'count', key: 'count', width: 10 },
      // { title: UNIT_PRICE, dataIndex: 'unitPrice', key: 'unitPrice' },
      // {
      //   title: DISCOUNT,
      //   dataIndex: 'discountPrice',
      //   key: 'discountPrice',
      // },
      {
        title: VENDOR_BRANCH,
        dataIndex: 'vendorBranchName',
        key: 'vendorBranchName',
        width: 110,
      },
      {
        title: TOTAL_PRICE,
        dataIndex: 'totalPrice',
        key: 'totalPrice',
        width: 110,
      },
      {
        title: DELIVERY_TYPE,
        dataIndex: 'deliveryType',
        key: 'deliveryType',
        width: 10,
      },
      {
        title: STATUS,
        dataIndex: 'statusTitle',
        key: 'statusTitle',
        render: (text, record2) => (
          <label className={s.cellTable}>{record2.statusTitle}</label>
        ),
        width: 110,
      },
      {
        title: REQUESTED_DATE,
        dataIndex: 'requestedDatePersian',
        key: 'requestedDatePersian',
        width: 10,
        render: (text, record2) =>
          !calendarExceptionLoading ? (
            <CPPermission>
              <div name="order-list-full-access">
                <label
                  className={
                    (record2.statusId === 0 ||
                      record2.statusId === 1 ||
                      record2.statusId === 9) &&
                    s.cursor
                  }
                  onClick={() =>
                    this.showAndHideCalendarModal(
                      true,
                      record2.vendorBranchId,
                      record2.productId,
                      record2.requestedDate,
                      record2.requestedHour,
                      record2.deliveryType,
                      record2.key,
                      record2.statusId,
                    )
                  }
                >
                  {record2.requestedDatePersian}
                </label>
              </div>
            </CPPermission>
          ) : (
            <Spin spinning={calendarExceptionLoading} size="small" />
          ),
      },
      {
        title: REQUESTED_HOUR,
        dataIndex: 'requestedHour',
        key: 'requestedHour',
        width: 10,
        render: (text, record2) =>
          !calendarExceptionLoading ? (
            <CPPermission>
              <div name="order-list-full-access">
                <label
                  className={
                    (record2.statusId === 0 ||
                      record2.statusId === 1 ||
                      record2.statusId === 9) &&
                    s.cursor
                  }
                  onClick={() =>
                    this.showAndHideCalendarModal(
                      true,
                      record2.vendorBranchId,
                      record2.productId,
                      record2.requestedDate,
                      record2.requestedHour,
                      record2.deliveryType,
                      record2.key,
                      record2.statusId,
                    )
                  }
                >
                  {record2.requestedHour}
                </label>
              </div>
            </CPPermission>
          ) : (
            <Spin spinning={calendarExceptionLoading} size="small" />
          ),
      },
      {
        width: 10,
        title: '',
        dataIndex: 'operationTwo1',
        render: (text, record2) => (
          <div>
            {(record2.statusId === 4 || record2.editable) && (
              <div>
                <CPPermission>
                  <div name="order-list-delete">
                    <CPButton
                      onClick={() =>
                        this.showModalOccasion(
                          record2.occasionText,
                          record2.fromOccasionText,
                          record2.toOccasionText,
                          record2.key,
                          record2.occasionTypeId,
                        )
                      }
                    >
                      {record2.occasionTypeId ? (
                        <span>نمایش متن</span>
                      ) : (
                        <span>ایجاد متن</span>
                      )}
                    </CPButton>
                  </div>
                </CPPermission>

                {record2.occasionText && (
                  <CPButton
                    className={s.printButton}
                    onClick={() => this.downloadPdf(record2.key, true)}
                  >
                    پرینت رنگی
                  </CPButton>
                )}
                {record2.occasionText && (
                  <CPButton onClick={() => this.downloadPdf(record2.key)}>
                    پرینت سیاه و سفید
                  </CPButton>
                )}
              </div>
            )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationFour',
        width: 1,
        render: (text, record2) => (
          <div>
            {record2.editable && (
              <CPButton
                className={s.changeStatus}
                onClick={e =>
                  this.showOrderItemStatusModal(
                    e,
                    record2.statusId,
                    record2.key,
                    record2.deliveryType,
                    record2.vendorBranchId,
                  )
                }
              >
                {CHANGE_STATUS}
              </CPButton>
            )}
          </div>
        ),
      },
      {
        width: 1,
        title: '',
        dataIndex: 'operationTwo',
        render: (text, record2) => (
          <div>
            {record2.showCancelRequest && (
              <CPPopConfirm
                title={ARE_YOU_SURE_WANT_TO_CANCEL_REQUEST}
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() =>
                  this.onCancelRequest(2, bill.code, record2.code)
                }
              >
                <CPButton>{CANCEL_REQUEST}</CPButton>
              </CPPopConfirm>
            )}
          </div>
        ),
      },
    ];

    billItemArray.map(item => {
      const obj = {
        vendorCode:
          item.vendorBranchProductDto && item.vendorBranchProductDto.productDto
            ? item.vendorBranchProductDto.productDto.vendorCode
            : '',
        imgUrl:
          item.vendorBranchProductDto &&
          item.vendorBranchProductDto.productDto &&
          item.vendorBranchProductDto.productDto.imageDtos
            ? item.vendorBranchProductDto.productDto.imageDtos[0].url
            : '',
        count: item.count,
        unitPrice: item.stringUnitPrice,
        shippingTariff: `${item.stringShipmentAmount} ${
          item.shipmentAmount ? TOMAN : ''
        }`,
        totalPrice: `${
          this.appType === 'Admin'
            ? item.stringPayableAmount
            : item.stringVendorBranchPayableAmount
        } ${TOMAN}`,
        discountPrice: `${item.stringTotalDiscount} ${TOMAN}`,
        key: item.id,
        status: item.billItemStatus,
        statusTitle: item.billItemStatusTitle,
        statusId: item.billItemStatusId,
        showCancelRequest: item.showCancelRequest,
        editable: item.editable,
        occasionTypeId: item.occasionTypeId,
        occasionText: item.occasionText,
        deliveryType: item.priceOption,
        fromOccasionText: item.fromOccasionText,
        toOccasionText: item.toOccasionText,
        vendorBranchId: item.vendorBranchDto.id,
        requestedDatePersian: item.persianRequestedDate,
        requestedDate: item.requestedDate,
        requestedHour: item.requestedHour,
        productId:
        item.vendorBranchProductDto &&
        item.vendorBranchProductDto.productDto &&
        item.vendorBranchProductDto.productDto.id,
        vendorBranchName: item.vendorBranchProductDto.vendorBranchDto.name,
      };

      dataSource.push(obj);
      return null;
    });
    return (
      <CPList
        columns={columns}
        hideSearchBox
        hideAdd
        data={dataSource}
        pagination={false}
        className="orderItemTable"
        rowClassName={s.backgroundRow}
      />
    );
  };

  async showDetailOrderModal(e, record, vendorBranchId, vendorBranchName) {
    e.preventDefault();
    const token = getCookie('token');
    if (record.addressDto && record.addressDto.districtDto) {
      const response = await getDistrictApi(
        token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new DistrictDto({
                cityDto: new CityDto({
                  id: record.addressDto.districtDto.cityDto.id,
                }),
              }),
            )
            .buildJson(),
        ),
      );

      if (response.status === 200)
        this.props.actions.districtOptionListSuccess(response.data);
    }
    this.setState({
      visibleOrderDetailModal: true,
      fullName: `${record.userDto &&
      record.userDto.firstName} ${record.userDto && record.userDto.lastName}`,
      phone: record.userDto.phone.replace('+98', '0'),
      email: record.userDto.email,
      description: record.description,
      address: record.addressDto && record.addressDto.content,
      receiverName: record.addressDto && record.addressDto.receiverName,
      receiverPhone:
        record.addressDto && record.addressDto.phone
          ? record.addressDto.phone.replace('+98', '0')
          : '',
      id: record.code,
      billId: record.key,
      orderDate: `${record.persianCreatedDateTimeUtc} - ${record.createdHour}`,
      totalAmount:
        this.appType === 'Admin'
          ? record.stringTotalAmount
          : record.stringVendorBranchTotalAmount,
      totalVendorBranchAmount: record.stringVendorBranchTotalAmount,
      totalDiscount: record.stringTotalDiscount,
      totalShipmentAmount:
        this.appType === 'Admin'
          ? record.stringTotalShipmentAmount
          : record.stringVendorBranchTotalShipmentAmount,
      totalVendorBranchShipmentAmount:
      record.stringVendorBranchTotalShipmentAmount,
      stringTotalDiscountCodeAmount: record.stringTotalDiscountCodeAmount,
      discountCodeAmount: record.discountCode,
      payableBillingAmount:
        this.appType === 'Admin'
          ? record.stringPayableBillingAmount
          : record.stringVendorBranchPayableBillingAmount,
      vendorBranchPayableBillingAmount:
        record.stringVendorBranchPayableBillingAmount,
      orderStatus: record.billStatus,
      idOrderStatus: record.billStatusId,
      latOrder: record.addressDto ? record.addressDto.lat : 0,
      lngOrder: record.addressDto ? record.addressDto.lng : 0,
      markerTitle: record.addressDto ? record.addressDto.content : 0,
      deliveryType:
        record.billItemDtos && record.billItemDtos.length > 0
          ? record.billItemDtos[0].priceOption
          : '',
      deliveryDate:
        record.billItemDtos && record.billItemDtos.length > 0
          ? record.billItemDtos[0].persianRequestedDate
          : '',
      deliveryHour:
        record.billItemDtos && record.billItemDtos.length > 0
          ? record.billItemDtos[0].requestedHour
          : '',
      district:
        record.addressDto && record.addressDto.districtDto
          ? record.addressDto.districtDto.name
          : '',
      cityId:
        record.addressDto && record.addressDto.districtDto
          ? record.addressDto.districtDto.cityDto.id.toString()
          : '',
      userId: record.userDto.id,
      vendorBranchId,
      vendorBranchName,
      addressId: record.addressDto ? record.addressDto.id : 0,
    });
  }

  handleCancelOrderDetailModal = () => {
    this.setState({ visibleOrderDetailModal: false });
  };

  async showOrderStatusModal(e, idStatus, idOrder) {
    const { loginData } = this.props;
    const orderStatusArray = [];

    e.preventDefault();

    const response = await getBillStatusApi(
      loginData.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new BillDto({
              id: idStatus,
            }),
          )
          .buildJson(),
      ),
    );

    if (response.status === 200) {
      response.data.items.map(item =>
        orderStatusArray.push(
          <Option key={item.id} value={item.id}>
            {item.id === 6 ? (
              <span style={{ color: 'red' }}>{item.name}</span>
            ) : (
              item.name
            )}
          </Option>,
        ),
      );
      this.setState({
        orderStatusArray,
        visibleOrderStatusModal: true,
        idOrderStatus: response.data.items[0].id,
        idOrder,
        changeStatusConfirm: false,
      });
    }
  }

  handleCancelOrderStatusModal = () => {
    this.setState({ visibleOrderStatusModal: false });
  };

  async showOrderItemStatusModal(
    e,
    idStatus,
    idOrderItem,
    priceOption,
    vendorBranchId,
  ) {
    const { loginData } = this.props;
    const orderItemStatusArray = [];
    const driverArray = [];
    let responseDriver = null;

    e.preventDefault();

    const response = await getBillItemStatusApi(
      loginData.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new BillItemDto({
              priceOption,
              billItemStatus: idStatus,
            }),
          )
          .buildJson(),
      ),
    );

    if (priceOption === 'Delivery' && idStatus === 1) {
      responseDriver = await getVendorBranchUserApi(
        loginData.token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new VendorBranchUserDto({
                roleDtos: [{ id: 4 }],
                fromCurrentVendorBranch: loginData.appType !== 'Admin',
                vendorBranchDto: new VendorBranchDto({ id: vendorBranchId }),
              }),
            )
            .includes(['Users', 'Roles'])
            .buildJson(),
        ),
      );
    }

    if (response.status === 200) {
      if (response.data.items.length === 0)
        message.error('شما نمیتوانید وضعیت را تغییر دهید.', 5);
      else {
        response.data.items.map(item =>
          orderItemStatusArray.push(
            <Option key={item.id} value={item.id}>
              {item.id === 5 ? (
                <span style={{ color: 'red' }}>{item.name}</span>
              ) : (
                item.name
              )}
            </Option>,
          ),
        );
        if (responseDriver)
          responseDriver.data.items.map(item =>
            driverArray.push(
              <Option key={item.userDto.id} value={item.userDto.id}>
                {item.userDto.displayName}
              </Option>,
            ),
          );
        this.setState({
          orderItemStatusArray,
          driverArray,
          visibleOrderItemStatusModal: true,
          idOrderItemStatus: response.data.items[0].id,
          driverId:
            responseDriver &&
            responseDriver.data.items &&
            responseDriver.data.items.length > 0 &&
            responseDriver.data.items[0].userDto
              ? responseDriver.data.items[0].userDto.id
              : '',
          idOrderItem,
          changeStatusConfirm: false,
          showComboboxDriver: priceOption === 'Delivery' && idStatus === 1,
        });
      }
    }
  }

  handleCancelOrderItemStatusModal = () => {
    this.setState({ visibleOrderItemStatusModal: false });
  };

  async handleOkChangeStatusOrder() {
    const { loginData } = this.props;
    const { idOrderStatus, idOrder, changeStatusConfirm } = this.state;

    if (changeStatusConfirm) {
      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new BillDto({
            id: idOrder,
            billStatus: idOrderStatus,
          }),
        )
        .build();

      const response = await putBillApi(loginData.token, jsonCrud);

      if (response.status === 200) {
        this.submitSearch();
        this.setState({ visibleOrderStatusModal: false });
      }
    } else {
      this.setState({ visibleConfirm: true, changeStatusBill: true });
    }
  }

  async handleOkChangeStatusOrderItem() {
    const { loginData } = this.props;
    const {
      idOrderItemStatus,
      idOrderItem,
      changeStatusConfirm,
      driverId,
    } = this.state;

    if (!changeStatusConfirm) {
      this.setState({ visibleConfirm: true, changeStatusBill: false });
    } else {
      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new BillItemDto({
            id: idOrderItem,
            billItemStatus: idOrderItemStatus,
            billItemDriverDto:
              driverId === 0 || idOrderItemStatus.toString() === '13'
                ? undefined
                : new DriverDto({
                  userDto: new UserDto({ id: driverId }),
                  billItemDto: new BillItemDto({ id: idOrderItem }),
                }),
          }),
        )
        .build();

      const response = await putBillItemApi(loginData.token, jsonCrud);

      if (response.status === 200) {
        this.submitSearch();
        this.setState({
          visibleOrderItemStatusModal: false,
          visibleImageModal: false,
        });
        /* if (driverId) {
          const jsonCrud2 = new BaseGetDtoBuilder()
            .dto(
              new MessageDto({
                body:
                  'فلانی عزیز سفارش تاج گل برای تحویل به مشتری در تاریخ 123123 برای شما ثبت گردید.',
                subject: `سفارش جدید`,
                messageSensitivity: 'None',
                userMessageDtos: [
                  {
                    userDto:
                      driverId === 0
                        ? undefined
                        : new UserDto({ id: driverId }),
                    published: true,
                  },
                ],
              }),
            )
            .build();

          await PostNotificationApi(loginData.token, jsonCrud2);
        } */
      } else {
        message.error(response.data.errorMessage, 5);
      }
    }
  }

  handleOkConfirmModal = () => {
    const { changeStatusBill } = this.state;
    this.setState({ visibleConfirm: false, changeStatusConfirm: true }, () => {
      if (changeStatusBill) this.handleOkChangeStatusOrder();
      else this.handleOkChangeStatusOrderItem();
    });
  };

  handleCancelConfirmModal = () => {
    this.setState({ visibleConfirm: false });
  };

  rowClassName = record => {
    if (record.status === 'SystemPending') return 'pendingOrder';
    if (record.status === 'CancelBySystem') return 'errorOrder';
    if (record.status === 'Finished') return 'completeOrder';
    if (record.status === 'BankPending') return 'bankPendingOrder';
    if (record.status === 'BankError') return 'bankErrorOrder';
    if (record.status === 'Confirmed') return 'confirmedOrder';
    return 'red';
  };

  async onCancelRequest(typeId, orderId, code = '') {
    const token = getCookie('token');
    let body = '';

    if (code !== '') {
      body = `در خواست لغو سفارش با شماره فاکتور ${orderId} و کد محصول ${code} را دارم.`;
    } else {
      body = ` درخواست لغو سفارش با شماره فاکتور ${orderId} را دارم. `;
    }

    const jsonCrud = new BaseGetDtoBuilder()
      .dto(
        new MessageDto({
          body,
          subject: ` درخواست لغو سفارش`,
          messageSensitivity: 'Instantaneous',
          requestTypeDto: new RequestTypeDto({ id: typeId }),
        }),
      )
      .build();

    const response = await PostNotificationApi(token, jsonCrud);

    if (response.status === 200) message.success(MESSAGE_CREATE_IS_SUCCESS, 5);
    else message.error(response.data.errorMessage, 5);
  }

  onChangePagination = page => {
    this.setState({ currentPage: page }, () =>
      this.props.actions.orderListRequest(this.prepareContainer()),
    );
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () =>
      this.props.actions.orderListRequest(this.prepareContainer()),
    );
  };

  prepareContainer = () => {
    const {
      vendorBranchIds,
      statusId,
      fromDate,
      toDate,
      userNameFinal,
      code,
      discountCode,
      promotion,
      currentPage,
      pageSize,
      deliveryFromDate,
      deliveryToDate,
      searchDeliveryType,
    } = this.state;
    const vendorBranchDtos = [];

    vendorBranchIds.map(item => {
      vendorBranchDtos.push(new VendorBranchDto({ id: item }));
      return null;
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new BillDto({
          code: code || undefined,
          searchHasDiscountCode:
            discountCode === 'all' ? undefined : discountCode,
          saerchHasPromotion: promotion === 'all' ? undefined : promotion,
          searchBillStatus: statusId || undefined,
          persianSearchFromDateTimeUtc: fromDate || '',
          searchVendorBranchDtos: vendorBranchDtos,
          persianSearchToDateTimeUtc: toDate || '',
          fromPanel: true,
          userDto: new UserDto({
            userName: userNameFinal,
          }),
          searchPriceOption: searchDeliveryType,
          persianSearchDPToDateTimeUtc: deliveryToDate,
          persianSearchDPFromDateTimeUtc: deliveryFromDate,
          published: true,
        }),
      )
      .includes([
        'userDto',
        'addressDto',
        'billItemDtos',
        'vendorBranchPromotionDetailDto',
      ])
      .pageSize(pageSize)
      .pageIndex(currentPage - 1)
      .buildJson();

    return jsonList;
  };

  async showModalOccasion(
    occasionText,
    fromOccasionText,
    toOccasionText,
    key,
    occasionTypeId,
  ) {
    const token = getCookie('token');
    const crudDto = new BaseCRUDDtoBuilder()
      .dto(
        new BillItemDto({
          id: key,
          isReadOccasionText: true,
        }),
      )
      .build();
    await putBillItemApi(token, crudDto);
    this.setState({
      visibleOccasionModal: true,
      occasionText,
      fromOccasionText,
      toOccasionText,
      idOrderItem: key,
      occasionTypeId: occasionTypeId || this.props.occasionTypes[0].id,
      isEditOccasionText: occasionTypeId,
    });
  }

  handleOkOccasionModal = () => {
    const copyText = document.getElementById('occasionText');
    copyText.select();
    document.execCommand('copy');
    message.success('متن مورد نظر با موفقیت کپی شد.', 5);

    this.setState({ visibleOccasionModal: false });
  };

  handleCancelOccasionModal = () => {
    this.setState({ visibleOccasionModal: false });
  };

  handleCancelImageModal = () => {
    this.setState({ visibleImageModal: false });
  };

  async updateEvent() {
    const { loginData } = this.props;
    const {
      idOrderItem,
      occasionText,
      fromOccasionText,
      toOccasionText,
      occasionTypeId,
      isEditOccasionText,
    } = this.state;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new BillItemDto({
          id: idOrderItem,
          updateOccasionText: true,
          OccasionText: occasionText,
          fromOccasionText,
          toOccasionText,
          occasionTypeId,
        }),
      )
      .build();

    const response = await putBillItemApi(loginData.token, jsonCrud);

    if (response.status === 200 && response.data.isSuccess) {
      message.success(EVENT_UPDATE_IS_SUCCESS, 5);
      this.setState({
        // visibleOrderItemStatusModal: false,
        visibleImageModal: false,
        visibleOccasionModal: isEditOccasionText,
      });
      this.submitSearch();
    } else message.success(EVENT_UPDATE_IS_FAILURE, 5);
  }

  goToUrl = url => {
    window.open(url, '_blank');
  };

  async cityChange(
    inputName,
    value,
    districtId = undefined,
    lat = undefined,
    lng = undefined,
    fromSelectedUserAddress = false,
  ) {
    const token = getCookie('token');
    const { vendorBranchId, vendorBranchName } = this.state;
    const responseVBCities = await getVendorBranchCityApi(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchCitiesDto({
              vendorBranchDto: new VendorBranchDto({ id: vendorBranchId }),
              // cityDto: new CityDto({ id: value }),
            }),
          )
          .includes('cityDto')
          .buildJson(),
      ),
    );

    if (
      responseVBCities.status === 200 &&
      responseVBCities.data.items.length > 0
    ) {
      const currentCity = responseVBCities.data.items.find(
        item => item.cityDto.id.toString() === value,
      );
      const citiesName = [];
      responseVBCities.data.items.map(item =>
        citiesName.push(item.cityDto.name),
      );
      if (currentCity || fromSelectedUserAddress) {
        const response = await getDistrictApi(
          token,
          getDtoQueryString(
            new BaseGetDtoBuilder()
              .dto(
                new DistrictDto({
                  cityDto: new CityDto({ id: value }),
                }),
              )
              .buildJson(),
          ),
        );

        if (response.status === 200) {
          this.props.actions.districtOptionListSuccess(response.data);

          this.setState({
            [inputName]: value,
            district:
              districtId !== undefined
                ? districtId
                : `${response.data.items[0].id.toString()}*${
                  response.data.items[0].lat
                  }*${response.data.items[0].lng}`,
            latOrder: lat || response.data.items[0].lat,
            lngOrder: lng || response.data.items[0].lng,
            isValidAddress: !!currentCity,
            errorMessage: `${vendorBranchName} در حال حاضر در شهر ${citiesName.toString()} سرویس دهی دارد `,
          });
        }
      } else {
        this.setState({
          isValidAddress: false,
          errorMessage: `${vendorBranchName} در حال حاضر در شهر ${citiesName.toString()} سرویس دهی دارد `,
        });
        message.error(
          `${vendorBranchName} در حال حاضر در شهر ${citiesName.toString()} سرویس دهی دارد `,
        );
      }
    }
  }

  async setLatAndLng(value) {
    const token = getCookie('token');
    const response = await getNearestLocationsApi(
      token,
      getDtoQueryString(
        JSON.stringify({
          dto: { lat: value.lat, lng: value.lng },
          fromCache: true,
        }),
      ),
    );

    if (
      response.status === 200 &&
      response.data.items &&
      response.data.items.length > 0
    ) {
      this.setState({ latOrder: value.lat, lngOrder: value.lng });

      this.cityChange(
        'cityId',
        response.data.items[0].cityDto.id.toString(),
        `${response.data.items[0].id.toString()}*${
          response.data.items[0].lat
          }*${response.data.items[0].lng}`,
        value.lat,
        value.lng,
      );
    } else {
      message.error('محله مورد نظر تحت پوشش نیست', 5);
    }
  }

  saveAddress = () => {
    const {
      addressId,
      address,
      receiverName,
      receiverPhone,
      cityId,
      district,
      latOrder,
      lngOrder,
      userId,
      isValidAddress,
      errorMessage,
      billId,
    } = this.state;

    if (isValidAddress) {
      const addressCrud = new BaseCRUDDtoBuilder()
        .dto(
          new AddressDto({
            id: addressId,
            content: address,
            receiverName,
            phone: receiverPhone,
            cityId,
            districtId: district.split('*')[0],
            lat: latOrder,
            lng: lngOrder,
            userDto: new UserDto({ id: userId }),
            fromPanel: true,
          }),
        )
        .build();

      const putOrderCrud = new BaseCRUDDtoBuilder()
        .dto(
          new BillDto({
            addressDto: new AddressDto({ id: addressId }),
            id: billId,
          }),
        )
        .build();

      const data = {
        data: addressCrud,
        listDto: undefined,
        putAddressBill: putOrderCrud,
      };
      this.props.actions.userAddressPutRequest(data);
      setTimeout(() => this.submitSearch(), 3000);
    } else message.error(errorMessage, 5);
  };

  showAndHideModalAddressList = value => {
    if (value)
      this.props.actions.userAddressListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new AddressDto({
              fromPanel: true,
              userDto: new UserDto({ id: this.state.userId }),
              active: true,
            }),
          )
          .includes(['districtDto'])
          .buildJson(),
      );
    this.setState({ visibleAddressListModal: value });
  };

  selectedUserAddress = record => {
    this.setState(
      {
        addressId: record.key,
        receiverName: record.receiverName,
        receiverPhone: record.phone,
        address: record.address,
        visibleAddressListModal: false,
      },
      () => {
        this.cityChange(
          'cityId',
          record.cityId.toString(),
          record.districtId,
          record.lat,
          record.lng,
          true,
        );
      },
    );
  };

  onDeleteOrder = id => {
    const baseCrud = new BaseCRUDDtoBuilder()
      .dto(
        new BillDto({
          id,
          published: false,
        }),
      )
      .build();

    const data = {
      data: baseCrud,
      listDto: this.prepareContainer(),
    };

    this.props.actions.orderPutRequest(data);
  };

  changeCalenderDate = value => {
    const { productId, vendorBranchId } = this.state;
    this.setState({
      requestedDateCalendar: value,
      selectedDayDefault: value.split('/')[2],
    });
    const availableProductShift = new AvailableProductShiftDto({
      persianSelectedDate: value,
      productDto: new ProductDto({
        id: productId,
        vendorDto: new AgentDto({
          vendorBranchDtos: [new VendorBranchDto({ id: vendorBranchId })],
        }),
      }),
    });

    this.props.actions.availableProductShiftRequest(
      JSON.stringify(availableProductShift),
    );
  };

  handleOkCalendarModal = () => {
    const {
      requestedDateCalendar,
      requestedHourCalendar,
      billItemId,
    } = this.state;
    const baseCrud = new BaseCRUDDtoBuilder()
      .dto(
        new BillItemDto({
          id: billItemId,
          requestedDate: requestedDateCalendar,
          requestedHour: requestedHourCalendar,
        }),
      )
      .build();

    const data = {
      data: baseCrud,
      listDto: this.prepareContainer(),
    };

    this.props.actions.orderItemPutRequest(data);

    this.setState({ visibleCalendarModal: false });
  };

  render() {
    const {
      orders,
      orderLoading,
      orderStatuses,
      vendorBranches,
      orderCount,
      occasionTypes,
      districtOptions,
      cities,
      userAddresses,
      userAddressCount,
      userAddressLoading,
      calendarException,
    } = this.props;
    const {
      vendorBranchIds,
      statusId,
      fromDate,
      toDate,
      userName,
      code,
      discountCode,
      promotion,
      fullName,
      phone,
      email,
      address,
      receiverName,
      receiverPhone,
      orderDate,
      totalAmount,
      totalVendorBranchAmount,
      totalDiscount,
      totalShipmentAmount,
      totalVendorBranchShipmentAmount,
      stringTotalDiscountCodeAmount,
      discountCodeAmount,
      payableBillingAmount,
      vendorBranchPayableBillingAmount,
      orderStatus,
      id,
      orderStatusArray,
      orderItemStatusArray,
      idOrderItemStatus,
      idOrderStatus,
      currentPage,
      occasionText,
      visibleImageModal,
      dataImage,
      visibleConfirm,
      idStatus,
      description,
      driverArray,
      driverId,
      showComboboxDriver,
      editable,
      latOrder,
      lngOrder,
      markerTitle,
      productDetailUrlSite,
      productDetailUrlAdmin,
      fromOccasionText,
      toOccasionText,
      deliveryType,
      deliveryDate,
      deliveryHour,
      district,
      occasionTypeId,
      isEditOccasionText,
      userId,
      cityId,
      visibleAddressListModal,
      deliveryFromDate,
      deliveryToDate,
      searchDeliveryType,
      selectedDayDefault,
      requestedHourCalendar,
      searchCityId,
    } = this.state;
    const columns = [
      {
        title: '',
        dataIndex: 'img',
        width: 50,
        render: (text, record) => (
          <div className={s.avatar}>
            {record.item.billItemDtos.map(item2 => (
              <div
                onClick={() =>
                  this.showImageModal(
                    item2.vendorBranchProductDto.productDto.imageDtos[0].url,
                    item2.vendorBranchProductDto.productDto.id,
                    item2.vendorBranchProductDto.vendorBranchDto.id,
                  )
                }
              >
                <CPAvatar
                  src={item2.vendorBranchProductDto.productDto.imageDtos[0].url}
                />
              </div>
            ))}
          </div>
        ),
      },
      {
        title: ID,
        dataIndex: 'code',
        key: 'code',
        className: 'column',
        width: 100,
        render: (text, record) => (
          <Link
            to="/"
            onClick={e =>
              this.showDetailOrderModal(
                e,
                record.item,
                record.vendorBranchId,
                record.vendorBranch,
              )
            }
          >
            {`${record.code}  ${record.b2b ? ' __ B2B' : ''}`}
          </Link>
        ),
      },
      {
        title: VENDOR_BRANCH,
        dataIndex: 'vendorBranch',
        key: 'vendorBranch',
        width: 180,
        render: (text, record) => (
          <Link to={`/vendor-branch/edit/${record.vendorBranchId}`}>
            <label className={cs(s.cellTable, s.cursor)}>
              {record.vendorBranch}
            </label>
          </Link>
        ),
      },
      {
        title: CATEGORY,
        dataIndex: 'category',
        key: 'category',
        className: 'column',
        width: 80,
      },
      {
        title: COUNT,
        dataIndex: 'count',
        key: 'count',
        width: 50,
        render: (text, record) => (
          <div className={record.count > 1 ? 'overCountOrder' : ''}>
            {record.count}
          </div>
        ),
      },
      {
        title: INSERT_DATE,
        dataIndex: 'date',
        key: 'date',
        width: 130,
        render: (text, record) => (
          <div>{`${record.date} - ${record.hour}`}</div>
        ),
      },
      { title: TIME, dataIndex: 'time', key: 'time', width: 120 },
      {
        title: TOTAL_PRICE,
        dataIndex: 'price',
        key: 'price',
        width: 110,
      },
      {
        title: STATUS,
        dataIndex: 'statusTitle',
        key: 'statusTitle',
        width: 110,
        render: (text, record) => (
          <label className={s.cellTable}>{record.statusTitle}</label>
        ),
      },
      {
        title: '',
        dataIndex: 'fromApp',
        key: 'fromApp',
        render: (text, record) => (
          <CPPermission>
            <div className={s.fromApp} name="login-icon-order-list">
              <img
                alt=""
                width={30}
                src={record.fromApp ? '/images/mobile.png' : '/images/pc.png'}
              />
              {record.fromWallet ? (
                <img src="/images/wallet.png" width={20} height={20} />
              ) : (
                <img src="/images/creditCard.png" width={20} height={20} />
              )}
            </div>
          </CPPermission>
        ),
      },
      /* {
        title: '',
        dataIndex: 'operationTwo3',
        render: (text, record2) => (
          <div className={!record2.showCancelRequest ? 'hidden' : ''}>
            {record2.showCancelRequest && (
              <CPPopConfirm
                title={ARE_YOU_SURE_WANT_TO_CANCEL_REQUEST}
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() => this.onCancelRequest('1', record2.code)}
              >
                <CPButton>{CANCEL_REQUEST}</CPButton>
              </CPPopConfirm>
              /!* <Link to={`/order/edit/${record2.key}`}>{CANCEL_REQUEST}</Link> *!/
            )}
          </div>
        ),
      }, */
      {
        title: '',
        dataIndex: 'operationThree',
        render: (text, record2) => (
          <div>
            <Link
              to="/"
              onClick={e =>
                this.showDetailOrderModal(
                  e,
                  record2.item,
                  record2.vendorBranchId,
                  record2.vendorBranch,
                )
              }
            >
              {DETAILS}
            </Link>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo4',
        render: (text, record2) => (
          <CPPermission>
            <div name="order-list-delete">
              <CPPopConfirm
                title={ARE_YOU_SURE_WANT_TO_DELETE_REQUEST}
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() => this.onDeleteOrder(record2.key)}
              >
                <i className="cp-trash" />
              </CPPopConfirm>
            </div>
          </CPPermission>
        ),
      },
      /* {
        title: '',
        dataIndex: 'operationFour',
        render: (text, record2) => (
          <div>
            {record2.editable && (
              <Link
                to="/"
                onClick={e =>
                  this.showOrderStatusModal(e, record2.statusId, record2.key)
                }
              >
                {CHANGE_STATUS}
              </Link>
            )}
          </div>
        ),
      }, */
    ];
    const columnsUserAddress = [
      {
        title: ADDRESS,
        dataIndex: 'address',
        key: 'address',
      },
      {
        title: RECEIVER_NAME,
        dataIndex: 'receiverName',
        key: 'receiverName',
      },
      {
        title: PHONE,
        dataIndex: 'phone',
        key: 'phone',
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton onClick={() => this.selectedUserAddress(record)}>
              انتخاب آدرس
            </CPButton>
          </div>
        ),
      },
    ];
    const vendorBranchArray = [];
    const orderDataSource = [];
    const statusArray = [];
    const occasionTypeArray = [];
    const citiesDataSource = [];
    const activeCitiesDataSource = [];
    const districtDataSource = [];
    const userAddressDataSource = [];

    orders.map(item => {
      const obj = {
        code: item.code,
        statusId: item.billStatusId,
        status: item.billStatus,
        statusTitle:
          item.billStatus === 'Finished' || item.billStatus === 'Confirmed'
            ? `${item.billStatusTitle} (${
              item.billItemDtos[0].billItemStatusTitle
              })`
            : item.billStatusTitle,
        date: item.persianCreatedDateTimeUtc,
        user: item.userDto
          ? `${item.userDto.firstName} ${item.userDto.lastName}`
          : 'ناشناس',
        price: `${
          this.appType === 'Admin'
            ? item.stringPayableBillingAmount
            : item.stringVendorBranchPayableBillingAmount
        } ${TOMAN}`,
        key: item.id,
        item,
        description: item.description,
        showCancelRequest: item.showCancelRequest,
        editable: item.editable,
        category: item.categoryDescription,
        time: item.remainingPersianDate,
        vendorBranch:
          item.billItemDtos && item.billItemDtos.length > 0
            ? item.billItemDtos[0].vendorBranchProductDto.vendorBranchDto.name
            : '',
        vendorBranchId:
          item.billItemDtos && item.billItemDtos.length > 0
            ? item.billItemDtos[0].vendorBranchProductDto.vendorBranchDto.id
            : '',
        count: item.billItemDtos ? item.billItemDtos.length : 0,
        hour: item.createdHour,
        fromApp: item.fromApp,
        fromWallet: item.fromWallet,
        b2b: item.b2b,
      };
      orderDataSource.push(obj);
      return null;
    });

    orderStatuses.map(item =>
      statusArray.push(
        <Option key={item.id.toString()} value={item.id.toString()}>
          {item.name}
        </Option>,
      ),
    );

    vendorBranches.map(item =>
      vendorBranchArray.push(
        <Option key={item.id.toString()} value={item.id.toString()}>
          {item.name}
        </Option>,
      ),
    );

    occasionTypes.map(item =>
      occasionTypeArray.push(
        <Option key={item.id} value={item.id}>
          {item.name}
        </Option>,
      ),
    );

    /**
     * map cities for comboBox Datasource
     */
    cities.map(item =>
      citiesDataSource.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map active cities for comboBox Datasource
     */
    cities.map(item => {
      if (item.active)
        activeCitiesDataSource.push(<Option key={item.id}>{item.name}</Option>);
    });

    /**
     * map district for comboBox Datasource
     */
    districtOptions.map(item =>
      districtDataSource.push(
        <Option key={`${item.id}*${item.lat}*${item.lng}`}>{item.name}</Option>,
      ),
    );

    if (userAddresses)
      userAddresses.map(item => {
        const obj = {
          key: item.id,
          address: item.content,
          phone: item.phone,
          receiverName: item.receiverName,
          lat: item.lat,
          lng: item.lng,
          districtId: `${item.districtDto.id}*${item.districtDto.lat}*${
            item.districtDto.lng
            }`,
          cityId: item.districtDto.cityDto.id,
        };
        userAddressDataSource.push(obj);

        return userAddressDataSource;
      });

    return (
      <div className={s.mainContent}>
        <h3>{ORDER_LIST}</h3>
        <CPNestedTable
          bordered
          dataSource={orderDataSource}
          columns={columns}
          loading={orderLoading}
          expandedRowRender={value => this.rowRender(value)}
          rowClassName={(record, index) => this.rowClassName(record, index)}
          pagination={false}
        >
          <div className={s.parentSearchBox}>
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12 searchDate">
                    <div className="col-lg-6 col-sm-12 ">
                      <label>تاریخ ثبت سفارش</label>
                      <CPPersianCalendar
                        placeholder={FROM_DATE}
                        onChange={(unix, formatted) =>
                          this.handelChangeInput('fromDate', formatted)
                        }
                        preSelected={fromDate}
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <CPPersianCalendar
                        placeholder={TO_DATE}
                        onChange={(unix, formatted) =>
                          this.handelChangeInput('toDate', formatted)
                        }
                        id="datePicker"
                        preSelected={toDate}
                      />
                    </div>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12 searchDate">
                    <div className="col-lg-6 col-sm-12 ">
                      <label>تاریخ تحویل سفارش</label>
                      <CPPersianCalendar
                        placeholder={FROM_DATE}
                        onChange={(unix, formatted) =>
                          this.handelChangeInput('deliveryFromDate', formatted)
                        }
                        preSelected={deliveryFromDate}
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <CPPersianCalendar
                        placeholder={TO_DATE}
                        onChange={(unix, formatted) =>
                          this.handelChangeInput('deliveryToDate', formatted)
                        }
                        id="datePicker"
                        preSelected={deliveryToDate}
                      />
                    </div>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{STATUS}</label>
                    <CPMultiSelect
                      value={statusId}
                      onChange={value => {
                        this.handelChangeInput('statusId', value);
                      }}
                    >
                      {statusArray}
                    </CPMultiSelect>
                  </div>
                  <CPPermission>
                    <div
                      className="col-lg-3 col-md-6 col-sm-12"
                      name="bill-list-user-name"
                    >
                      <label>{USER_USERNAME}</label>
                      <CPIntlPhoneInput
                        value={userName}
                        onChange={this.changeNumber}
                      />
                    </div>
                  </CPPermission>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{ID}</label>
                    <CPInput
                      value={code}
                      onChange={value =>
                        this.handelChangeInput('code', value.target.value)
                      }
                    />
                  </div>

                  <CPPermission>
                    <div
                      name="order-search-vendorBranch"
                      className="col-lg-3 col-md-6 col-sm-12"
                    >
                      <label>{CITY}</label>
                      <CPSelect
                        value={searchCityId}
                        showSearch
                        onChange={value => {
                          this.handelChangeInput('searchCityId', value);
                        }}
                      >
                        {activeCitiesDataSource}
                      </CPSelect>
                    </div>
                  </CPPermission>
                  <CPPermission>
                    <div
                      name="order-search-vendorBranch"
                      className="col-lg-3 col-md-6 col-sm-12"
                    >
                      <label>{VENDOR_BRANCH}</label>
                      <CPMultiSelect
                        value={vendorBranchIds}
                        showSearch
                        onChange={value => {
                          this.handelChangeInput('vendorBranchIds', value);
                        }}
                      >
                        {vendorBranchArray}
                      </CPMultiSelect>
                      {/* <CPSelect
                    value={vendorBranchId}
                    showSearch
                    onChange={value => {
                      this.handelChangeInput('vendorBranchId', value);
                    }}
                  >
                    {vendorBranchArray}
                  </CPSelect> */}
                    </div>
                  </CPPermission>
                  <CPPermission>
                    <div
                      className="col-lg-3 col-md-6 col-sm-12"
                      name="bill-list-discount-code"
                    >
                      <label>{DISCOUNT_CODE}</label>
                      <CPSelect
                        value={discountCode}
                        onChange={value => {
                          this.handelChangeInput('discountCode', value);
                        }}
                      >
                        <Option key="all">همه</Option>
                        <Option key="true">با کد تخفیف</Option>
                        <Option key="false">بدون کد تخفیف</Option>
                      </CPSelect>
                    </div>
                  </CPPermission>
                  <CPPermission>
                    <div
                      className="col-lg-3 col-md-6 col-sm-12"
                      name="bill-list-promotion"
                    >
                      <label>{PROMOTION}</label>
                      <CPSelect
                        value={promotion}
                        onChange={value => {
                          this.handelChangeInput('promotion', value);
                        }}
                      >
                        <Option key="all">همه</Option>
                        <Option key="true">با پروموشن</Option>
                        <Option key="false">بدون پروموشن</Option>
                      </CPSelect>
                    </div>
                  </CPPermission>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{DELIVERY_TYPE}</label>
                    <CPSelect
                      value={searchDeliveryType}
                      onChange={value => {
                        this.handelChangeInput('searchDeliveryType', value);
                      }}
                    >
                      <Option key="3">{ALL}</Option>
                      <Option key="2">{DELIVERY}</Option>
                      <Option key="1">{PICKUP}</Option>
                    </CPSelect>
                  </div>
                </div>
              </div>
              <div className="field">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
            <div className={s.helpingStatus}>
              <div>
                <label className="pendingOrder" />
                <label>{PENDING_ORDER}</label>
              </div>
              <div>
                <label className="errorOrder" />
                <label>{CANCEL_BY_SYSTEM}</label>
              </div>
              <div>
                <label className="completeOrder" />
                <label>{COMPLETE_ORDER}</label>
              </div>
              <div>
                <label className="bankPendingOrder" />
                <label>{BANK_PENDING_ORDER}</label>
              </div>
              <div>
                <label className="bankErrorOrder" />
                <label>{BANK_ERROR_ORDER}</label>
              </div>
              <div>
                <label className="confirmedOrder" />
                <label>{CONFIRMED_ORDER}</label>
              </div>
              <div>
                <label className="cancelByPay" />
                <label>{CANCEL_FROM_PORT}</label>
              </div>
            </div>
          </div>
        </CPNestedTable>
        <CPPagination
          total={orderCount}
          current={currentPage}
          onChange={this.onChangePagination}
          onShowSizeChange={this.onShowSizeChange}
          defaultPageSize={20}
        />
        <CPModal
          visible={this.state.visibleOrderDetailModal}
          handleCancel={this.handleCancelOrderDetailModal}
          title={DETAILS}
          className="max_modal"
          showFooter={false}
        >
          <div className="row orderDetails">
            <div className="col-lg-6 col-sm-12">
              <CPPanel header={CLIENT_INFO}>
                <div className="row">
                  <div className="col-lg-6 col-sm-12">
                    <label>{SENDER_NAME}</label>
                    {getCookie('appType') === 'Admin' ? (
                      <div className={s.userName}>
                        <Link to={`/user/edit/${userId}`}>{fullName}</Link>
                      </div>
                    ) : (
                      <CPInput disabled value={fullName} />
                    )}
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{PHONE}</label>
                    <CPInput disabled value={phone} />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{EMAIL}</label>
                    <CPInput disabled value={email} />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{DESCRIPTION}</label>
                    <CPTextArea disabled value={description} />
                  </div>
                </div>
                {deliveryType === 'Pickup' && (
                  <div className="row">
                    <div className="col-lg-6 col-sm-12">
                      <label>{DELIVERY_DATE}</label>
                      <CPInput disabled value={deliveryDate} />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{DELIVERY_HOUR}</label>
                      <CPInput disabled value={deliveryHour} />
                    </div>
                  </div>
                )}
              </CPPanel>
            </div>
            <div className="col-lg-6 col-sm-12">
              <CPPanel header={FACTOR_INFO}>
                <div className="row">
                  <div className="col-lg-6 col-sm-12">
                    <div className="col-sm-12">
                      <label>{TOTAL_PRICE}</label>
                      <CPInput disabled value={`${totalAmount} ${TOMAN}`} />
                    </div>
                    <div className="col-sm-12">
                      <label>{AMOUNT_SHIPPING}</label>
                      <CPInput
                        disabled
                        value={`${totalShipmentAmount} ${TOMAN}`}
                      />
                    </div>
                    {this.appType === 'Admin' && (
                      <div className="col-sm-12">
                        <label>{DISCOUNT_CODE}</label>
                        <CPInput
                          disabled
                          value={`${stringTotalDiscountCodeAmount} ${TOMAN} ${
                            discountCodeAmount ? `(${discountCodeAmount})` : ''
                          }`}
                        />
                      </div>
                    )}
                    {this.appType === 'Admin' && (
                      <div className="col-sm-12">
                        <label>{TOTAL_DISCOUNT}</label>
                        <CPInput disabled value={`${totalDiscount} ${TOMAN}`} />
                      </div>
                    )}
                    <div className="col-sm-12">
                      <label>{PAYABLE_BILLING_AMOUNT}</label>
                      <CPInput
                        disabled
                        value={`${payableBillingAmount} ${TOMAN}`}
                      />
                    </div>
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <div className="col-sm-12">
                      <label>{DELIVERY_TYPE}</label>
                      <CPInput
                        disabled
                        value={deliveryType}
                        className={
                          deliveryType === 'Delivery' ? s.green : s.red
                        }
                      />
                    </div>
                    <div className="col-sm-12">
                      <label>{ID}</label>
                      <CPInput disabled value={id} />
                    </div>
                    <div className="col-sm-12">
                      <label>{ORDER_DATE}</label>
                      <CPInput disabled value={orderDate} />
                    </div>
                    <div className="col-sm-12">
                      <label>{STATUS}</label>
                      <CPInput disabled value={orderStatus} />
                    </div>
                    {this.appType === 'Admin' && (
                      <div className="col-sm-12 checkoutBox">
                        <label>{CHECKOUT}</label>
                        <div>
                          <div>
                            <label>{TOTAL_PRICE}</label>
                            <label
                            >{`${totalVendorBranchAmount} ${TOMAN}`}</label>
                          </div>
                          <hr />
                          <div>
                            <label>{AMOUNT_SHIPPING}</label>
                            <label
                            >{`${totalVendorBranchShipmentAmount} ${TOMAN}`}</label>
                          </div>
                          <hr />

                          <div>
                            <label>{PAYABLE_BILLING_AMOUNT}</label>
                            <label
                            >{`${vendorBranchPayableBillingAmount} ${TOMAN}`}</label>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </CPPanel>
            </div>
            {deliveryType === 'Delivery' && (
              <div className="col-lg-6 col-sm-12">
                <CPPanel header={RECEIVER_INFO}>
                  <div className="row">
                    <div className="col-lg-6 col-sm-12">
                      <label>{RECEIVER_NAME}</label>
                      <CPInput
                        onChange={value =>
                          this.handelChangeInput(
                            'receiverName',
                            value.target.value,
                          )
                        }
                        value={receiverName}
                        disabled={!(idOrderStatus === 4 || idOrderStatus === 3)}
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{PHONE}</label>
                      <CPInput
                        onChange={value =>
                          this.handelChangeInput(
                            'receiverPhone',
                            value.target.value,
                          )
                        }
                        value={receiverPhone}
                        disabled={!(idOrderStatus === 4 || idOrderStatus === 3)}
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{DELIVERY_DATE}</label>
                      <CPInput disabled value={deliveryDate} />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{DELIVERY_HOUR}</label>
                      <CPInput disabled value={deliveryHour} />
                    </div>
                    {/* <div className="col-lg-6 col-sm-12"> */}
                    {/* <label>{DISTRICT}</label> */}
                    {/* <CPInput disabled value={district} /> */}
                    {/* </div> */}
                    <div className="col-lg-6 col-sm-12">
                      <label>{CITY}</label>
                      <CPSelect
                        onChange={value => this.cityChange('cityId', value)}
                        value={cityId}
                        showSearch
                        disabled={!(idOrderStatus === 4 || idOrderStatus === 3)}
                      >
                        {citiesDataSource}
                      </CPSelect>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{DISTRICT}</label>
                      <CPSelect
                        onChange={value =>
                          this.handelChangeInput('district', value)
                        }
                        value={district}
                        showSearch
                        disabled={!(idOrderStatus === 4 || idOrderStatus === 3)}
                      >
                        {districtDataSource}
                      </CPSelect>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{ADDRESS}</label>
                      <CPTextArea
                        onChange={value =>
                          this.handelChangeInput('address', value.target.value)
                        }
                        value={address}
                        disabled={!(idOrderStatus === 4 || idOrderStatus === 3)}
                      />
                    </div>
                  </div>
                  {(idOrderStatus === 4 || idOrderStatus === 3) && (
                    <div>
                      <CPButton
                        className={s.btnSaveAddress}
                        onClick={() => this.showAndHideModalAddressList(true)}
                      >
                        {SELECTED_ADDRESS}
                      </CPButton>
                      <CPButton onClick={this.saveAddress}>{SAVE}</CPButton>
                    </div>
                  )}
                </CPPanel>
              </div>
            )}
            {deliveryType === 'Delivery' && (
              <div className="col-lg-6 col-sm-12">
                <CPPanel header={MAP}>
                  <CPMap
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
                    containerElement="222px"
                    loadingElement="100%"
                    mapElement="100%"
                    defaultZoom={15}
                    lat={latOrder}
                    lng={lngOrder}
                    draggable
                    onDragEnd={value => this.setLatAndLng(value)}
                    markerTitle={markerTitle}
                  />
                </CPPanel>
              </div>
            )}
          </div>
        </CPModal>
        <CPModal
          visible={this.state.visibleOrderStatusModal}
          handleCancel={this.handleCancelOrderStatusModal}
          handleOk={this.handleOkChangeStatusOrder}
          title={CHANGE_STATUS}
        >
          <div className="row">
            <div className="col-lg-6 col-sm-12">
              <label>{STATUS}</label>
              <CPSelect
                value={idOrderStatus}
                onChange={value =>
                  this.handelChangeInput('idOrderStatus', value)
                }
              >
                {orderStatusArray}
              </CPSelect>
            </div>
          </div>
        </CPModal>
        <CPModal
          visible={this.state.visibleOrderItemStatusModal}
          handleCancel={this.handleCancelOrderItemStatusModal}
          handleOk={this.handleOkChangeStatusOrderItem}
          title={CHANGE_STATUS}
          className={s.paddingModal}
        >
          <div className="row">
            <div className="col-lg-6 col-sm-12">
              <label>{STATUS}</label>
              <CPSelect
                value={idOrderItemStatus}
                onChange={value =>
                  this.handelChangeInput('idOrderItemStatus', value)
                }
              >
                {orderItemStatusArray}
              </CPSelect>
            </div>
            {showComboboxDriver && (
              <div className="col-lg-6 col-sm-12">
                <label>{DRIVER}</label>
                <CPSelect
                  value={driverId}
                  onChange={value => this.handelChangeInput('driverId', value)}
                >
                  {driverArray}
                </CPSelect>
              </div>
            )}
          </div>
        </CPModal>
        <CPModal
          visible={this.state.visibleOccasionModal}
          handleCancel={this.handleCancelOccasionModal}
          handleOk={this.handleOkOccasionModal}
          textSave={TEXT_COPY}
          title={EVENT}
          hideCancelButton
          hideOkButton={!isEditOccasionText}
          className={s.occasionModal}
          customButton={
            <CPPermission tag="span">
              <span>
                <CPButton
                  key="submit4"
                  type="primary"
                  name="bill-list-occasion-update"
                  onClick={this.updateEvent}
                >
                  {isEditOccasionText ? UPDATE : CREATE}
                </CPButton>
              </span>
            </CPPermission>
          }
        >
          <div className="row">
            <div className="col-sm-12">
              <label>انتخاب مناسبت</label>
              <CPSelect
                onChange={value =>
                  this.handelChangeInput('occasionTypeId', value)
                }
                value={occasionTypeId}
              >
                {occasionTypeArray}
              </CPSelect>
            </div>
            <div className="col-sm-12">
              <label>از طرف</label>
              <CPInput
                value={fromOccasionText}
                onChange={value =>
                  this.handelChangeInput('fromOccasionText', value.target.value)
                }
              />
            </div>
            <div className="col-sm-12">
              <label>به</label>
              <CPInput
                value={toOccasionText}
                onChange={value =>
                  this.handelChangeInput('toOccasionText', value.target.value)
                }
              />
            </div>
            <div className="col-sm-12">
              <label>متن ارسالی</label>
              <CPTextArea
                value={occasionText}
                onChange={value =>
                  this.handelChangeInput('occasionText', value.target.value)
                }
                id="occasionText"
              />
            </div>
          </div>
        </CPModal>
        <CPModal
          visible={visibleImageModal}
          handleCancel={this.handleCancelImageModal}
          handleOk={this.handleOkChangeStatusOrderItem}
          hideOkButton={(idStatus === 4 || idStatus === 6) && !editable}
        >
          <CPCarousel data={dataImage} width={500} height={500} />
          {!(idStatus === 4 || idStatus === 6) &&
          editable && (
            <div className={cs('row', s.btnImageView)}>
              <div className="col-lg-6 col-sm-12">
                <label>{STATUS}</label>
                <CPSelect
                  value={idOrderItemStatus}
                  onChange={value =>
                    this.handelChangeInput('idOrderItemStatus', value)
                  }
                >
                  {orderItemStatusArray}
                </CPSelect>
              </div>
              {showComboboxDriver && (
                <div className="col-lg-6 col-sm-12">
                  <label>{DRIVER}</label>
                  <CPSelect
                    value={driverId}
                    onChange={value =>
                      this.handelChangeInput('driverId', value)
                    }
                  >
                    {driverArray}
                  </CPSelect>
                </div>
              )}
            </div>
          )}

          <div className={cs('row', s.btnImageView)}>
            <div className="col-lg-6 col-sm-12">
              <CPButton onClick={() => this.goToUrl(productDetailUrlSite)}>
                دیدن محصول در سایت
              </CPButton>
            </div>
            <div className="col-lg-6 col-sm-12">
              <CPButton onClick={() => this.goToUrl(productDetailUrlAdmin)}>
                دیدن محصول در پنل ادمین
              </CPButton>
            </div>
          </div>
        </CPModal>
        <CPConfirmation
          visible={visibleConfirm}
          handleOk={this.handleOkConfirmModal}
          handleCancel={this.handleCancelConfirmModal}
          content={ARE_YOU_SURE_WANT_TO_ORDER_STATUS_CHANGE}
          zIndex={99999}
        />

        <CPModal
          visible={visibleAddressListModal}
          handleCancel={() => this.showAndHideModalAddressList(false)}
          showFooter={false}
          title={SELECT_PRODUCT_FOR_PROMOTION}
          className="max_modal"
        >
          <div className="row">
            <div className="col-sm-12">
              <CPList
                data={userAddressDataSource}
                columns={columnsUserAddress}
                count={userAddressCount}
                loading={userAddressLoading}
                hideAdd
                hideSearchBox
                pagination={false}
              />
            </div>
          </div>
        </CPModal>
        <CPModal
          className={s.calendar}
          width={650}
          visible={this.state.visibleCalendarModal}
          handleOk={this.handleOkCalendarModal}
          handleCancel={() => this.showAndHideCalendarModal(false)}
          customButton={
            <CPSelect
              value={requestedHourCalendar}
              placeholder="انتخاب ساعت تحویل"
              onChange={value =>
                this.handelChangeInput('requestedHourCalendar', value)
              }
              className={s.timePicker}
            >
              {this.availableProductShiftArray}
            </CPSelect>
          }
        >
          <CPDatepicker
            selectedDayDefault={selectedDayDefault}
            calendarData={calendarException}
            onChange={this.changeCalenderDate}
          />
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  orders: state.orderList.data ? state.orderList.data.items : [],
  orderLoading: state.orderList.loading,
  orderCount: state.orderList.data ? state.orderList.data.count : 0,
  orderStatuses: state.orderStatusList.data
    ? state.orderStatusList.data.items
    : [],
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
  loginData: state.login.data,
  occasionTypes: state.occasionTypeList.data
    ? state.occasionTypeList.data.items
    : [],
  cities: state.cityList.data ? state.cityList.data.items : [],
  calendarException: state.calendarException.data,
  calendarExceptionLoading: state.calendarException.loading,
  districtOptions: state.districtList.data ? state.districtList.data.items : [],
  userAddresses: state.userAddressList.data.items,
  userAddressCount: state.userAddressList.data.count,
  userAddressLoading: state.userAddressList.loading,
  availableProductShift: state.availableProductShift.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      orderListRequest: orderListActions.orderListRequest,
      orderPutRequest: orderPutActions.orderPutRequest,
      occasionTypeListRequest: occasionTypeListActions.occasionTypeListRequest,
      districtOptionListSuccess: districtListActions.districtOptionListSuccess,
      userAddressPutRequest: userAddressPutActions.userAddressPutRequest,
      cityListRequest: cityListActions.cityListRequest,
      userAddressListRequest: userAddressActions.userAddressListRequest,
      calendarExceptionRequest:
      calendarExceptionActions.calendarExceptionRequest,
      availableProductShiftRequest:
      availableProductShiftActions.availableProductShiftRequest,
      orderItemPutRequest: orderItemPutActions.orderItemPutRequest,
      vendorBranchListRequest: vendorBranchListActions.vendorBranchListRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(OrderList));
