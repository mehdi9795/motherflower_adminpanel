import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './OrderEdit.css';
import { ORDER_EDIT } from '../../../../Resources/Localization';
import OrderForm from '../../../../components/Sam/Order/OrderForm';

class OrderEdit extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string,
  };

  static defaultProps = {
    backUrl: '/order/list',
  };

  render() {
    const { backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={` ${ORDER_EDIT}`} backUrl={backUrl} isEdit={false}>
          <OrderForm />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(OrderEdit));
