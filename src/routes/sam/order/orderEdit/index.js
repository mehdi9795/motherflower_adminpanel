import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import { BillDto } from '../../../../dtos/samDtos';
import OrderEdit from './OrderEdit';
import { getBillApi, getBillItemApi } from '../../../../services/samApi';
import { getDtoQueryString } from '../../../../utils/helper';
import {
  orderItemListFailure,
  orderItemListSuccess,
  singleOrderListFailure,
} from '../../../../redux/sam/action/order';
import singleOrderActions from '../../../../redux/sam/actionReducer/order/SingleOrder';
import { PAGE_TITLE_ORDER_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, params, query }) {
  let baseGetDtoTemp = new BaseGetDtoBuilder()
    .dto(
      new BillDto({
        id: params.id,
        fromPanel: true,
      }),
    )
    .includes(['userDto', 'addressDto'])
    .buildJson();

  const response = await getBillApi(
    store.getState().login.data.token,
    getDtoQueryString(JSON.stringify(baseGetDtoTemp)),
  );

  if (response.status === 200) {
    store.dispatch(singleOrderActions.singleOrderSuccess(response.data));
  } else {
    store.dispatch(singleOrderListFailure());
  }

  baseGetDtoTemp = new BaseGetDtoBuilder()
    .dto(
      new BillDto({
        id: params.id,
      }),
    )
    .includes(['vendorBranchPromotionDetailDto'])
    .buildJson();

  const responseBillItem = await getBillItemApi(
    store.getState().login.data.token,
    getDtoQueryString(JSON.stringify(baseGetDtoTemp)),
  );

  if (responseBillItem.status === 200) {
    store.dispatch(orderItemListSuccess(responseBillItem.data));
  } else {
    store.dispatch(orderItemListFailure());
  }

  const { backUrl = '/order/list' } = query;
  return {
    chunks: ['orderEdit'],
    title: `${PAGE_TITLE_ORDER_EDIT}`,
    component: (
      <Layout>
        <OrderEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
