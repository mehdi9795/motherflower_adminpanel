import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './shippingTariffEdit.css';
import history from '../../../../history';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VB_SHIPPING_TARIFF,
  SHIPPING_TARIFF_EDIT,
} from '../../../../Resources/Localization';

import { deleteVendorBranchShippingTariffApi } from '../../../../services/samApi';
import ShippingTariffForm from '../../../../components/Sam/ShippingTariff/ShippingTariffForm';

class shippingTariffEdit extends React.Component {
  static propTypes = {
    shippingTariff: PropTypes.objectOf(PropTypes.any),
    backUrl: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    loginData: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    backUrl: '/shippingTariff/list',
    shippingTariff: {},
    loginData: {},
  };

  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
  }

  async onDelete() {
    const { shippingTariff, loginData } = this.props;
    const response = await deleteVendorBranchShippingTariffApi(
      loginData.token,
      shippingTariff.id,
    );

    if (response.status === 200) {
      history.push('/shippingTariff/list');
    }
  }

  render() {
    const { shippingTariff, backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard
          title={` ${SHIPPING_TARIFF_EDIT}`}
          backUrl={backUrl}
          isEdit={!!(shippingTariff && shippingTariff.id)}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_VB_SHIPPING_TARIFF}
          onDelete={this.onDelete}
        >
          <ShippingTariffForm />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.vbShippingTariffList.loading,
  shippingTariff: state.vbShippingTariffList.data
    ? state.vbShippingTariffList.data.items[0]
    : null,
  loginData: state.login.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(shippingTariffEdit));
