import React from 'react';
import ShippingTariffList from './ShippingTariffList';
import Layout from '../../../../components/Template/Layout/Layout';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import { CityDto, ZoneDto } from '../../../../dtos/commonDtos';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import { getDtoQueryString } from '../../../../utils/helper';
import secondaryLevelListActions from '../../../../redux/catalog/actionReducer/category/SecondaryLevelList';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import {
  getAgentApi,
  getVendorBranchApi,
} from '../../../../services/vendorApi';
import { getCategoryApi } from '../../../../services/catalogApi';
import { getCityApi, getZoneApi } from '../../../../services/commonApi';
import { VendorBranchShippingTariffsDto } from '../../../../dtos/samDtos';
import zoneListActions from '../../../../redux/common/actionReducer/zone/List';
import vbShippingTariffListListActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/List';
import { PAGE_TITLE_SHIPPING_TARIFF_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  this.zoneId = 0;
  /**
   * get all Secondary Level categories  with default clause secondaryLevel
   */

  const responseCategory = await getCategoryApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new CategoryDto({
            secondaryLevel: true,
            published: true,
          }),
        )
        .buildJson(),
    ),
  );
  if (responseCategory.status === 200)
    store.dispatch(
      secondaryLevelListActions.secondaryLevelListSuccess(
        responseCategory.data,
      ),
    );
  else store.dispatch(secondaryLevelListActions.secondaryLevelListFailure());

  /**
   * get all city for first time with default clause
   */
  const responseCity = await getCityApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder().dto(new CityDto({ active: true })).buildJson(),
    ),
  );
  if (responseCity.status === 200) {
    store.dispatch(cityListActions.cityListSuccess(responseCity.data));

    const listzone = new BaseGetDtoBuilder()
      .dto(
        new ZoneDto({
          cityDto: new CityDto({
            id:
              responseCity.data.items.length > 0
                ? responseCity.data.items[0].id
                : 0,
          }),
          active: true,
        }),
      )
      .buildJson();

    const response = await getZoneApi(
      store.getState().login.data.token,
      getDtoQueryString(listzone),
    );

    if (response.status === 200) {
      store.dispatch(zoneListActions.zoneListSuccess(response.data));
      this.zoneId =
        response.data.items.length > 0 ? response.data.items[0].id : 0;
    } else store.dispatch(zoneListActions.zoneListFailure());
  } else store.dispatch(cityListActions.cityListFailure());

  /**
   * get single vendor agent by id for edit
   */
  const jsonVendors = new BaseGetDtoBuilder().all(true).buildJson();
  const vendors = await getAgentApi(
    store.getState().login.data.token,
    getDtoQueryString(jsonVendors),
  );

  if (vendors.status === 200 && vendors.data.items.length > 0) {
    /**
     * get all vendor branch for first time with default clause
     */
    store.dispatch(vendorListActions.vendorListSuccess(vendors.data));
    const responseVendorBranch = await getVendorBranchApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchDto({
              vendorId: vendors.data.items[0].id,
              active: true,
            }),
          )
          .buildJson(),
      ),
    );

    if (responseVendorBranch.status === 200)
      store.dispatch(
        vendorBranchListActions.vendorBranchListSuccess(
          responseVendorBranch.data,
        ),
      );
    else store.dispatch(vendorBranchListActions.vendorBranchListFailure());

    if (
      responseVendorBranch.status === 200 &&
      responseCategory.status === 200 &&
      this.zoneId
    ) {
      const listJson = new BaseGetDtoBuilder()
        .dto(
          new VendorBranchShippingTariffsDto({
            vendorBranchDto: new VendorBranchDto({
              id: responseVendorBranch.data.items[0].id,
            }),
          }),
        )
        .includes(['zoneDto', 'vendorBranchDto', 'categoryDto'])
        .buildJson();

      store.dispatch(
        vbShippingTariffListListActions.vbShippingTariffListRequest(listJson),
      );
    }
  } else store.dispatch(vendorListActions.vendorListFailure());

  return {
    chunks: ['shippingTariffList'],
    title: `${PAGE_TITLE_SHIPPING_TARIFF_LIST}`,
    component: (
      <Layout>
        <ShippingTariffList />
      </Layout>
    ),
  };
}

export default action;
