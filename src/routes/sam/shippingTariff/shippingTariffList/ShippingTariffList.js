import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select } from 'antd';
import HotKeys from 'react-hot-keys';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import s from './ShippingTariffList.css';
import {
  YES,
  NO,
  PROMOTION_LIST,
  CATEGORY,
  AGENT_NAME,
  COUNT,
  VALUE,
  CITY,
  ZONE,
  TOMAN,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VB_SHIPPING_TARIFF,
  ALL,
  VENDOR_BRANCH,
  TO_PRICE,
  FROM_PRICE,
} from '../../../../Resources/Localization';
import CPButton from '../../../../components/CP/CPButton';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import { CityDto, ZoneDto } from '../../../../dtos/commonDtos';
import zoneListActions from '../../../../redux/common/actionReducer/zone/List';
import { VendorBranchShippingTariffsDto } from '../../../../dtos/samDtos';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import {
  getDtoQueryString,
  pressEnterAndCallApplySearch,
} from '../../../../utils/helper';
import vbShippingTariffListActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/List';
import vbShippingTariffPutActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/Put';
import vbShippingTariffDeleteActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/Delete';
import { getZoneApi } from '../../../../services/commonApi';
import history from '../../../../history';
import vbShippingTariffPostActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/Post';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import CPInputNumber from '../../../../components/CP/CPInputNumber';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';

const { Option } = Select;

class shippingTariffList extends React.Component {
  static propTypes = {
    shippingTariffLoading: PropTypes.bool,
    shippingTariffCount: PropTypes.number,
    cities: PropTypes.arrayOf(PropTypes.object),
    categories: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    vendors: PropTypes.arrayOf(PropTypes.object),
    zones: PropTypes.arrayOf(PropTypes.object),
    shippingTariffs: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    shippingTariffs: [],
    vendorBranches: [],
    vendors: [],
    categories: [],
    cities: [],
    zones: [],
    shippingTariffLoading: false,
    shippingTariffCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      categoryId: '0',
      cityId:
        props.cities && props.cities[0] ? props.cities[0].id.toString() : '0',
      vendorBranchId:
        props.vendorBranches && props.vendorBranches[0]
          ? props.vendorBranches[0].id.toString()
          : '0',
      vendorId:
        props.vendors && props.vendors[0]
          ? props.vendors[0].id.toString()
          : '0',
      zoneId: '0',
      fromPrice: null,
      toPrice: null,
      count: 0,
      amount: 0,
      id: 0,
    };
    this.handelChangeInput = this.handelChangeInput.bind(this);
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps(nextProps) {
    this.props.actions.hideLoading();
    if (nextProps.vendors && nextProps.vendorBranches.length > 0)
      this.setState({
        vendorBranchId: nextProps.vendorBranches[0].id.toString(),
      });
  }

  onDelete = id => {
    const { vendorBranchId, categoryId, zoneId } = this.state;

    /**
     * create dto for get vendor branch shipping tariff
     * @type {{id, dto, fromIdentity}}
     */
    const listJson = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchShippingTariffsDto({
          zoneDto: zoneId === '0' ? undefined : new ZoneDto({ id: zoneId }),
          vendorBranchDto: new VendorBranchDto({ id: vendorBranchId }),
          categoryDto:
            categoryId === '0'
              ? undefined
              : new CategoryDto({ id: categoryId }),
        }),
      )
      .includes(['zoneDto', 'vendorBranchDto', 'categoryDto'])
      .buildJson();

    const data = {
      id,
      listDto: listJson,
    };
    this.props.actions.vbShippingTariffDeleteRequest(data);
  };

  async submitSearch() {
    const { zoneId, categoryId, vendorBranchId } = this.state;

    const listJson = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchShippingTariffsDto({
          zoneDto: zoneId === '0' ? undefined : new ZoneDto({ id: zoneId }),
          vendorBranchDto: new VendorBranchDto({ id: vendorBranchId }),
          categoryDto:
            categoryId === '0'
              ? undefined
              : new CategoryDto({ id: categoryId }),
        }),
      )
      .includes(['zoneDto', 'vendorBranchDto', 'categoryDto'])
      .buildJson();

    this.props.actions.vbShippingTariffListRequest(listJson);
  }

  redirectToCreatePage = () => {
    this.props.actions.showLoading();
    history.push('/shippingTariff/create');
  };

  /**
   * set value inputs into state
   * @param inputName
   * @param value
   */
  async handelChangeInput(inputName, value) {
    const { loginData } = this.props;

    this.setState({ [inputName]: value });
    if (inputName === 'cityId') {
      const baseGetDtoTemp = new BaseGetDtoBuilder()
        .dto(
          new ZoneDto({
            active: true,
            cityDto: new CityDto({ id: value }),
          }),
        )
        .buildJson();

      const response = await getZoneApi(
        loginData.token,
        getDtoQueryString(baseGetDtoTemp),
      );

      if (response.status === 200) {
        this.props.actions.zoneListSuccess(response.data);
        this.setState({
          zoneId: '0',
        });
      }
    }
    if (inputName === 'vendorId') {
      this.setState({ vendorBranchId: '' });
      const jsonVendorBranch = new BaseGetDtoBuilder()
        .dto(new VendorBranchDto({ vendorId: value, active: true }))
        .buildJson();
      this.props.actions.vendorBranchListRequest(jsonVendorBranch);
    }
  }

  render() {
    const {
      categories,
      vendorBranches,
      cities,
      zones,
      shippingTariffLoading,
      shippingTariffCount,
      shippingTariffs,
      vendors,
    } = this.props;
    const {
      vendorBranchId,
      categoryId,
      zoneId,
      cityId,
      fromPrice,
      toPrice,
      vendorId,
    } = this.state;
    const columns = [
      {
        title: AGENT_NAME,
        dataIndex: 'vendorBranch',
        key: 'vendorBranch',
        width: 250,
      },
      { title: CATEGORY, dataIndex: 'category', key: 'category', width: 250 },
      { title: ZONE, dataIndex: 'zone', key: 'zone', width: 150 },
      {
        title: COUNT,
        dataIndex: 'count',
        key: 'count',
        width: 70,
      },
      {
        title: VALUE,
        dataIndex: 'stringAmount',
        key: 'stringAmount',
        width: 200,
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_VB_SHIPPING_TARIFF}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to={`/shippingTariff/edit/${record.key}`}
              onClick={() => {
                this.props.actions.showLoading();
              }}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];
    const categoryArray = [];
    const cityArray = [];
    const zoneArray = [];
    const vendorBranchArray = [];
    const vendorArray = [];
    const shippingTariffDataSource = [];

    if (shippingTariffs)
      shippingTariffs.map(item => {
        const obj = {
          category: item.categoryDto.name,
          vendorBranch: item.vendorBranchDto.name,
          zone: item.zoneDto.name,
          count: item.qty,
          amount: item.shippingAmount,
          stringAmount: `${item.stringShippingAmount} ${TOMAN}`,
          key: item.id,
          categoryId: item.categoryDto.id.toString(),
          vendorBranchId: item.vendorBranchDto.id.toString(),
          zoneId: item.zoneDto.id.toString(),
          cityId: item.zoneDto.cityDto.id.toString(),
        };
        shippingTariffDataSource.push(obj);
        return null;
      });

    /**
     * map category for comboBox Datasource
     */
    categories.map(item =>
      categoryArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map vendorBranch for comboBox Datasource
     */
    vendors.map(item =>
      vendorArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map vendorBranch for comboBox Datasource
     */
    vendorBranches.map(item =>
      vendorBranchArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map city for comboBox Datasource
     */
    cities.map(item =>
      cityArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map zone for comboBox Datasource
     */
    zones.map(item =>
      zoneArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.redirectToCreatePage}>
        <div className={s.mainContent}>
          <h3>{PROMOTION_LIST}</h3>
          <CardTable
            data={shippingTariffDataSource}
            count={shippingTariffCount}
            columns={columns}
            loading={shippingTariffLoading}
            onAddClick={this.redirectToCreatePage}
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{CITY}</label>
                    <CPSelect
                      value={cityId}
                      onChange={value => {
                        this.handelChangeInput('cityId', value);
                      }}
                    >
                      {cityArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{ZONE}</label>
                    <CPSelect
                      value={zoneId}
                      onChange={value => {
                        this.handelChangeInput('zoneId', value);
                      }}
                    >
                      <Option key={0}>{ALL}</Option>
                      {zoneArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{AGENT_NAME}</label>
                    <CPSelect
                      value={vendorId}
                      onChange={value => {
                        this.handelChangeInput('vendorId', value);
                      }}
                    >
                      {vendorArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{VENDOR_BRANCH}</label>
                    <CPSelect
                      value={vendorBranchId}
                      onChange={value => {
                        this.handelChangeInput('vendorBranchId', value);
                      }}
                    >
                      {vendorBranchArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{FROM_PRICE}</label>
                    <CPInputNumber
                      name="fromPrice"
                      label={FROM_PRICE}
                      value={fromPrice}
                      onChange={value => {
                        this.handelChangeInput('fromPrice', value);
                      }}
                      format="Currency"
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{TO_PRICE}</label>
                    <CPInputNumber
                      name="toPrice"
                      label={TO_PRICE}
                      value={toPrice}
                      onChange={value => {
                        this.handelChangeInput('toPrice', value);
                      }}
                      format="Currency"
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{CATEGORY}</label>
                    <CPSelect
                      value={categoryId}
                      onChange={value => {
                        this.handelChangeInput('categoryId', value);
                      }}
                    >
                      <Option key={0}>{ALL}</Option>
                      {categoryArray}
                    </CPSelect>
                  </div>
                </div>
              </div>
              <div className="field">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CardTable>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.secondaryLevelList.data.items,
  vendorBranches: state.vendorBranchList.data.items,
  cities: state.cityList.data.items,
  zones: state.zoneList.data.items,
  shippingTariffs: state.vbShippingTariffList.data.items,
  shippingTariffCount: state.vbShippingTariffList.data.count,
  shippingTariffLoading: state.vbShippingTariffList.loading,
  vendors: state.vendorList.data ? state.vendorList.data.items : [],
  loginData: state.login.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vbShippingTariffListRequest:
        vbShippingTariffListActions.vbShippingTariffListRequest,
      zoneListSuccess: zoneListActions.zoneListSuccess,
      zoneListFailure: zoneListActions.zoneListFailure,
      vbShippingTariffPostRequest:
        vbShippingTariffPostActions.vbShippingTariffPostRequest,
      vbShippingTariffPutRequest:
        vbShippingTariffPutActions.vbShippingTariffPutRequest,
      vbShippingTariffDeleteRequest:
        vbShippingTariffDeleteActions.vbShippingTariffDeleteRequest,
      vendorBranchListRequest: vendorBranchListActions.vendorBranchListRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(shippingTariffList));
