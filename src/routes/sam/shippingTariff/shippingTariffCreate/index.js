import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import ShippingTariffCreate from './ShippingTariffCreate';
import { getDtoQueryString } from '../../../../utils/helper';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import { CityDto, ZoneDto } from '../../../../dtos/commonDtos';
import { getVendorBranchApi } from '../../../../services/vendorApi';
import { getCityApi, getZoneApi } from '../../../../services/commonApi';
import secondaryLevelListActions from '../../../../redux/catalog/actionReducer/category/SecondaryLevelList';
import zoneListActions from '../../../../redux/common/actionReducer/zone/List';
import { getCategoryApi } from '../../../../services/catalogApi';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import vbShippingTariffListActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/List';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import { PAGE_TITLE_SHIPPING_TARIFF_CREATE } from '../../../../Resources/Localization';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

async function action({ store, query }) {
  const { backUrl = '/shippingTariff/list' } = query;
  store.dispatch(
    vbShippingTariffListActions.vbShippingTariffListSuccess({
      items: [],
      count: 0,
    }),
  );
  this.zoneId = 0;
  /**
   * get all Secondary Level categories  with default clause secondaryLevel
   */

  const responseCategory = await getCategoryApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new CategoryDto({
            secondaryLevel: true,
            published: true,
          }),
        )
        .buildJson(),
    ),
  );
  if (responseCategory.status === 200)
    store.dispatch(
      secondaryLevelListActions.secondaryLevelListSuccess(
        responseCategory.data,
      ),
    );
  else store.dispatch(secondaryLevelListActions.secondaryLevelListFailure());

  /**
   * get all city for first time with default clause
   */

  const responseCity = await getCityApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new CityDto({
            active: true,
          }),
        )
        .buildJson(),
    ),
  );
  if (responseCity.status === 200) {
    store.dispatch(cityListActions.cityListSuccess(responseCity.data));

    const jsonListZone = new BaseGetDtoBuilder()
      .dto(
        new ZoneDto({
          active: true,
          cityDto: new CityDto({
            id:
              responseCity.data.items.length > 0
                ? responseCity.data.items[0].id
                : 0,
          }),
        }),
      )
      .buildJson();

    const response = await getZoneApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonListZone),
    );

    if (response.status === 200) {
      store.dispatch(zoneListActions.zoneListSuccess(response.data));
      this.zoneId =
        response.data.items.length > 0 ? response.data.items[0].id : 0;
    } else store.dispatch(zoneListActions.zoneListFailure());
  } else store.dispatch(cityListActions.cityListFailure());

  /**
   * get all vendor branch for first time with default clause
   */
  const responseVendorBranch = await getVendorBranchApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new VendorBranchDto({
            active: true,
          }),
        )
        .all(true)
        .buildJson(),
    ),
  );

  if (responseVendorBranch.status === 200)
    store.dispatch(
      vendorBranchListActions.vendorBranchListSuccess(
        responseVendorBranch.data,
      ),
    );
  else store.dispatch(vendorBranchListActions.vendorBranchListFailure());

  return {
    chunks: ['shippingTariffCreate'],
    title: `${PAGE_TITLE_SHIPPING_TARIFF_CREATE}`,
    component: (
      <Layout>
        <ShippingTariffCreate backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
