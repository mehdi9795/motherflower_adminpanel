import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './ShippingTariffCreate.css';
import { PROMOTION_CREATE } from '../../../../Resources/Localization';
import ShippingTariffForm from '../../../../components/Sam/ShippingTariff/ShippingTariffForm';

class shippingTariffCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
  };

  render() {
    const { backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={PROMOTION_CREATE} backUrl={backUrl}>
          <ShippingTariffForm />
        </EditCard>
      </div>
    );
  }
}

export default withStyles(s)(shippingTariffCreate);
