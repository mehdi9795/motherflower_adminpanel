import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { hideLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HotKeys from 'react-hot-keys';
import s from './BannerList.css';
import {
  STATUS,
  PRODUCT_PRICE_EDIT,
  DISPLAY_ORDER,
  IMAGE_UPLOAD,
  LINK,
  BANNER_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_BANNER,
} from '../../../../Resources/Localization';
import CPModal from '../../../../components/CP/CPModal';
import CPInput from '../../../../components/CP/CPInput';
import CPImageGallery from '../../../../components/CP/CPImageGallery';
import { BannerDto, ImageDto } from '../../../../dtos/cmsDtos';
import CPUpload from '../../../../components/CP/CPUpload';
import CPCheckbox from '../../../../components/CP/CPCheckbox';
import bannerDeleteActions from '../../../../redux/cms/actionReducer/banner/Delete';
import bannerPostActions from '../../../../redux/cms/actionReducer/banner/Post';
import bannerPutActions from '../../../../redux/cms/actionReducer/banner/Put';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

class MessageList extends React.Component {
  static propTypes = {
    banners: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    banners: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      displayOrder: 0,
      isShowOnHomePage: false,
      url: '',
      visibleBannerModal: false,
      binariesImage: '',
      extention: '',
      id: 0,
      link: '',
    };
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = value => {
    /**
     * reload  Product images with productId
     * @type
     */

    const baseGetDtoTemp = new BaseGetDtoBuilder().all(true).buildJson();

    const putData = {
      id: value,
      listDto: JSON.stringify(baseGetDtoTemp),
    };

    this.props.actions.bannerDeleteRequest(putData);
  };

  onEdit = value => {
    this.setState({
      displayOrder: value ? value.displayOrder.toString() : '0',
      isShowOnHomePage: value ? value.isShowOnHomePage : false,
      visibleBannerModal: true,
      url: value ? value.imgUrl : '',
      id: value ? value.id : 0,
      link: value ? value.link : '',
      status: value ? value.status : false,
      imageId: value ? value.imageId : 0,
      binariesImage: '',
    });
  };

  handleImageChange = value => {
    this.setState({
      binariesImage: value.base64,
      extention: value.type.split('/')[1],
    });
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  handleCancelModal = () => {
    this.setState({
      visibleBannerModal: false,
    });
  };

  handleOkModal = () => {
    const {
      displayOrder,
      imageId,
      status,
      binariesImage,
      extention,
      link,
      id,
    } = this.state;

    this.setState({
      visibleBannerModal: false,
    });

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new BannerDto({
          imageDto: binariesImage.length > 0 ? new ImageDto({
            binaries: binariesImage.split(',')[1],
            extention,
            id: imageId,
            isCover: false,
            imagetype: 'HomePageBanner',
          }) : undefined,
          link,
          orderBy: displayOrder,
          published: status,
          id,
        }),
      )
      .build();

    const data = {
      data: jsonCrud,
      listDto: new BaseGetDtoBuilder().all(true).buildJson(),
    };
    if (id === 0) {
      this.props.actions.bannerPostRequest(data);
    } else {
      this.props.actions.bannerPutRequest(data);
    }
  };

  render() {
    const { banners } = this.props;
    const { displayOrder, url, isShowOnHomePage, status, link } = this.state;

    const bannerDataSource = [];

    banners.map(item => {
      const obj = {
        id: item.id,
        url: item.imageDto ? item.imageDto.url : '',
        status: item.published,
        displayOrder: item.orderBy,
        link: item.link,
        showOnHomePage: false,
        tooltip: '',
        entityId: 0,
        isCover: false,
        imageId: item.imageDto ? item.imageDto.id : 0,
        deleteTitle: ARE_YOU_SURE_WANT_TO_DELETE_THE_BANNER,
      };
      bannerDataSource.push(obj);
      return null;
    });

    return (
      <HotKeys keyName="shift+n" onKeyDown={() => this.onEdit(null)}>
        <div className={s.mainContent}>
          <h3>{BANNER_LIST}</h3>
          <CPImageGallery
            data={bannerDataSource}
            onEdit={this.onEdit}
            onDelete={this.onDelete}
            onCreate={() => this.onEdit(null)}
          />
          <CPModal
            visible={this.state.visibleBannerModal}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
            title={PRODUCT_PRICE_EDIT}
          >
            <div className="modalContent">
              <div className="row">
                <div className="col-sm-12">
                  <label>{IMAGE_UPLOAD}</label>
                  {this.state.visibleBannerModal && (
                    <CPUpload
                      onChange={this.handleImageChange}
                      image={`${url}`}
                    />
                  )}
                </div>
                <div className=" col-sm-12">
                  <label>{STATUS}</label>
                  <CPCheckbox
                    checked={status}
                    onChange={value =>
                      this.handelChangeInput('status', value.target.checked)
                    }
                  />
                </div>
                <div className="col-lg-6 col-sm-12">
                  <label>{DISPLAY_ORDER}</label>
                  <CPInput
                    name="displayOrder"
                    // label={PICKUP_PRICE}
                    value={displayOrder}
                    onChange={value => {
                      this.handelChangeInput(
                        'displayOrder',
                        value.target.value,
                      );
                    }}
                  />
                </div>
                <div className="col-lg-6 col-sm-12">
                  <label>{LINK}</label>
                  <CPInput
                    name="link"
                    // label={DELIVERY_PRICE}
                    value={link}
                    onChange={value => {
                      this.handelChangeInput('link', value.target.value);
                    }}
                  />
                </div>
              </div>
            </div>
          </CPModal>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  banners: state.bannerList.data ? state.bannerList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      bannerPostRequest: bannerPostActions.bannerPostRequest,
      bannerPutRequest: bannerPutActions.bannerPutRequest,
      bannerDeleteRequest: bannerDeleteActions.bannerDeleteRequest,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(MessageList));
