import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import BannerList from './BannerList';
import bannerListActions from '../../../../redux/cms/actionReducer/banner/List';
import { PAGE_TITLE_BANNER_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  store.dispatch(
    bannerListActions.bannerListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  return {
    chunks: ['messageList'],
    title: `${PAGE_TITLE_BANNER_LIST}`,
    component: (
      <Layout>
        <BannerList />
      </Layout>
    ),
  };
}

export default action;
