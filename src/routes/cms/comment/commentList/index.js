import React from 'react';
import CommentList from './CommentList';
import Layout from '../../../../components/Template/Layout/Layout';
import commentListActions from '../../../../redux/cms/actionReducer/comment/List';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import {COMMENT_LIST} from "../../../../Resources/Localization";

async function action({ store }) {
  /**
   * get all promotions for first time with default where clause
   */
  store.dispatch(
    commentListActions.commentListRequest(
      new BaseGetDtoBuilder().all(true).includes(['rateDto','userDto']).buildJson(),
    ),
  );

  return {
    chunks: ['commentList'],
    title: `${COMMENT_LIST}`,
    component: (
      <Layout>
        <CommentList />
      </Layout>
    ),
  };
}

export default action;
