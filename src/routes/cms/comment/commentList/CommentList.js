import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import s from './CommentList.css';
import {
  NAME,
  STATUS,
  ACTIVE,
  DEACTIVATE,
  YES,
  NO,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PROMOTION,
  SMS,
  RATE,
  COMMENT_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_COMMENT, TOMAN, VALUE, TO_PRICE, FROM_PRICE, COMMISSION_TYPE, PERCENT, COMMENT_EDIT,
} from '../../../../Resources/Localization';
import CPButton from '../../../../components/CP/CPButton';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPSwitch from '../../../../components/CP/CPSwitch';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import commentListActions from '../../../../redux/cms/actionReducer/comment/List';
import commentDeleteActions from '../../../../redux/cms/actionReducer/comment/Delete';
import commentPutActions from '../../../../redux/cms/actionReducer/comment/Put';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import { CommentDto } from '../../../../dtos/cmsDtos';
import CPInput from "../../../../components/CP/CPInput";
import CPModal from "../../../../components/CP/CPModal";

class CommentList extends React.Component {
  static propTypes = {
    commentLoading: PropTypes.bool,
    commentCount: PropTypes.number,
    comments: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    comments: [],
    commentLoading: false,
    commentCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      status: false,
      id: 0,
      body: '',
    };
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    /**
     * get all promotions for first time with default where clause
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(new CommentDto({
        published: this.state.status
      }))
      .all(true)
      .includes(['rateDto', 'userDto'])
      .buildJson();

    const deleteData = {
      id,
      listDto: jsonList,
    };
    this.props.actions.commentDeleteRequest(deleteData);
  };

  /**
   * set value inputs into state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  changeStatus = (value, record) => {
    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new CommentDto({
          id: record.key,
          published: value,
          body: record.body,
        }),
      )
      .build();

    const jsonList = new BaseGetDtoBuilder()
      .dto(new CommentDto({
        published: this.state.status
      }))
      .all(true)
      .includes(['rateDto', 'userDto'])
      .buildJson();

    const data = {
      data: jsonCrud,
      listDto: jsonList,
    };
    this.props.actions.commentPutRequest(data);
  };

  submitSearch = () => {
    const { status } = this.state;
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new CommentDto({
          published: status,
        }),
      )
      .includes(['rateDto', 'userDto'])
      .all(true)
      .buildJson();
    this.props.actions.commentListRequest(jsonList);
  };

  handleOkModal = () => {
    const { id, body, status } = this.state;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new CommentDto({
          id,
          body,
          published: status,
        }),
      )
      .build();

    const jsonList = new BaseGetDtoBuilder()
      .dto(new CommentDto({
        published: this.state.status
      }))
      .all(true)
      .includes(['rateDto', 'userDto'])
      .buildJson();

    const data = {
      data: jsonCrud,
      listDto: jsonList,
    };
    this.props.actions.commentPutRequest(data);
    this.setState({ visibleEditModal: false, id: 0, body: '' });
  }

  handleCancelModal = () => {
    this.setState({ visibleEditModal: false, id: 0, body: '' });
  }

  render() {
    const { comments, commentLoading, commentCount } = this.props;
    const { status, body, visibleEditModal } = this.state;
    const columns = [
      { title: NAME, dataIndex: 'userFullName', key: 'userFullName' },
      {
        title: SMS,
        dataIndex: 'body',
        key: 'body',
      },
      {
        title: RATE,
        dataIndex: 'rate',
        key: 'rate',
      },
      {
        title: STATUS,
        dataIndex: 'status',
        render: (text, record) => (
          <div>
            <CPSwitch
              onChange={value => this.changeStatus(value, record)}
              size="small"
              defaultChecked={record.status}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 80,
        render: (text, record) => (
          <CPPopConfirm
            title={ARE_YOU_SURE_WANT_TO_DELETE_THE_COMMENT}
            okText={YES}
            cancelText={NO}
            okType="primary"
            onConfirm={() => this.onDelete(record.key)}
          >
            <CPButton className="delete_action">
              <i className="cp-trash" />
            </CPButton>
          </CPPopConfirm>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => {
                this.setState({
                  visibleEditModal: true,
                  id: record.key,
                  body: record.body,
                  status: record.status,
                });
              }}
              className="edit_action"
            >
              <i className="cp-edit" />
            </CPButton>
          </div>
        ),
      },
    ];
    const dataSource = [];

    comments.map(item => {
      const obj = {
        userFullName: item.userDto
          ? `${item.userDto.firstName} ${item.userDto.lastName}`
          : '',
        body: item.body,
        rate: item.rateDto ? item.rateDto.value : 0,
        status: item.published,
        key: item.id,
      };

      dataSource.push(obj);
    });

    return (
      <div className={s.mainContent}>
        <h3>{COMMENT_LIST}</h3>
        <CardTable
          data={dataSource}
          count={commentCount}
          columns={columns}
          loading={commentLoading}
          hideAdd
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <div className="align-center">
                    <label>{STATUS}</label>
                    <CPSwitch
                      defaultChecked={status}
                      unCheckedChildren={DEACTIVATE}
                      checkedChildren={ACTIVE}
                      onChange={value => {
                        this.handelChangeInput('status', value);
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CardTable>
        <CPModal
          visible={visibleEditModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
          title={COMMENT_EDIT}
        >
          <div className="modalContent">
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{SMS}</label>
                <CPInput
                  value={body}
                  onChange={value => {
                    this.handelChangeInput('body', value.target.value);
                  }}
                />
              </div>
            </div>
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  comments: state.commentList.data ? state.commentList.data.items : [],
  commentCount:
    state.commentList.data.items && state.commentList.data.items.length > 0
      ? state.commentList.data.count
      : 0,
  commentLoading: state.commentList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      commentDeleteRequest: commentDeleteActions.commentDeleteRequest,
      commentListRequest: commentListActions.commentListRequest,
      commentPutRequest: commentPutActions.commentPutRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CommentList));
