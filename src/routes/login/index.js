import React from 'react';
import LoginLayout from '../../components/LoginLayout';
import Login from './Login';
import { PAGE_TITLE_LOGIN } from '../../Resources/Localization';
import BackUrlActions from '../../redux/shared/actionReducer/url/backUrl';

const title = `${PAGE_TITLE_LOGIN}`;

async function action({ fetch, query, store }) {

  const backUrl = query.returnUrl ? [query.returnUrl.replace('^', '=')] : '';
  store.dispatch(BackUrlActions.backUrlSuccess(backUrl));
  return {
    chunks: ['login'],
    title,
    component: (
      <LoginLayout>
        <Login title={title} fetch={fetch} />
      </LoginLayout>
    ),
  };
}

export default action;
