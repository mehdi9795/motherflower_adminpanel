import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'antd';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import s from './Login.css';
import RoundLogo from '../../components/Template/RoundLogo';
import AccountWizard from '../../components/LoginLayout/AccountWizard';

class Login extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    const classes = [
      '/images/login/back1.jpg',
      '/images/login/back2.jpg',
      '/images/login/back3.jpg',
      '/images/login/back4.jpg',
      '/images/login/back5.jpg',
      '/images/login/back6.jpg',
      '/images/login/back7.jpg',
      '/images/login/back8.jpg',
    ];

    const Background = classes[Math.floor(Math.random() * classes.length)];

    return (
      <div className={s.root} style={{ backgroundImage: `url(${Background})` }}>
        <RoundLogo />
        <AccountWizard selectedStep="forgetPassword" />
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

const WrappedLoginForm = Form.create()(Login);
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(WrappedLoginForm));
