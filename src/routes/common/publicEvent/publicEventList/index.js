import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import { PublicEventDto } from '../../../../dtos/commonDtos';
import publicEventListActions from '../../../../redux/common/actionReducer/publicEvent/List';
import PublicEventList from './PublicEventList';
import { PAGE_TITLE_PUBLIC_EVENT_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all public event for first time with default clause
   */
  store.dispatch(
    publicEventListActions.publicEventListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new PublicEventDto({
            published: true,
          }),
        )
        .buildJson(),
    ),
  );

  return {
    chunks: ['publicEventList'],
    title: `${PAGE_TITLE_PUBLIC_EVENT_EDIT}`,
    component: (
      <Layout>
        <PublicEventList />
      </Layout>
    ),
  };
}

export default action;
