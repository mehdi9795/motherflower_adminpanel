import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPButton from '../../../../components/CP/CPButton';
import s from './PublicEventList.css';
import CPSwitch from '../../../../components/CP/CPSwitch';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import { PublicEventDto } from '../../../../dtos/commonDtos';
import {
  ACTIVE,
  DEACTIVATE,
  NAME,
  STATUS,
  DELETE_CONFIRM,
  YES,
  NO,
  CITY_LIST,
  TYPE,
  EVENT_DATE,
  FROM_DATE,
  TO_DATE,
} from '../../../../Resources/Localization';
import CPPersianCalendar from '../../../../components/CP/CPPersianCalendar';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

class PublicEventList extends React.Component {
  static propTypes = {
    publicEventLoading: PropTypes.bool,
    publicEvents: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    publicEvents: [],
    publicEventLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      fromDate: '',
      toDate: '',
      status: true,
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const data = {
      listDto: this.prepareContainer(),
      status: 'list',
      id,
    };

    this.props.actions.publicEventDeleteRequest(data);
  };

  /**
   * set value inputs into state for search
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  /**
   * for submit search in city

   */
  prepareContainer = () => {
    const json = new BaseGetDtoBuilder()
      .dto(
        new PublicEventDto({
          published: this.state.status,
          searchFromPersianDate: this.state.fromDate,
          searchToPersianDate: this.state.toDate,
        }),
      )
      .buildJson();
    return json;
  };

  /**
   * for search in city
   * @returns {Promise<void>}
   */
  async submitSearch() {
    this.props.actions.publicEventListRequest(this.prepareContainer());
  }

  // <editor-fold dsc="CPPersianCalendar">
  changeFromDate = (unix, formatted) => {
    this.setState({
      fromDate: formatted,
    });
  };

  changeToDate = (unix, formatted) => {
    this.setState({
      toDate: formatted,
    });
  };
  // </editor-fold>

  render() {
    const { publicEvents, publicEventLoading } = this.props;
    const { status, fromDate, toDate } = this.state;
    const columns = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      { title: TYPE, dataIndex: 'publicEventType', key: 'publicEventType' },
      {
        title: EVENT_DATE,
        dataIndex: 'persianEventDate',
        key: 'persianEventDate',
      },
      {
        title: STATUS,
        dataIndex: 'published',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.published} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={DELETE_CONFIRM}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link to={`/city/edit/${record.key}`} className="edit_action">
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    return (
      <div className={s.mainContent}>
        <h3>{CITY_LIST}</h3>
        <CardTable
          data={publicEvents}
          columns={columns}
          loading={publicEventLoading}
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{FROM_DATE}</label>
                  <CPPersianCalendar
                    placeholder={FROM_DATE}
                    format="jYYYY/jMM/jDD"
                    onChange={this.changeFromDate}
                    id="datePicker"
                    preSelected={fromDate}
                  />
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{TO_DATE}</label>
                  <CPPersianCalendar
                    placeholder={FROM_DATE}
                    format="jYYYY/jMM/jDD"
                    onChange={this.changeToDate}
                    id="datePicker"
                    preSelected={toDate}
                  />
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <div className="align-center">
                    <CPSwitch
                      defaultChecked={status}
                      checkedChildren={ACTIVE}
                      unCheckedChildren={DEACTIVATE}
                      onChange={value => {
                        this.handelChangeInput('status', value);
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CardTable>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  publicEvents: state.publicEventList.data.items,
  publicEventLoading: state.publicEventList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PublicEventList));
