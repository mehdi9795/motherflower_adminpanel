import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import singleCityActions from '../../../../redux/common/actionReducer/city/Single';
import singleCityInitActions from '../../../../redux/common/actionReducer/city/SingleInit';

import { getDtoQueryString } from '../../../../utils/helper';
import { CityDto, DistrictDto, ZoneDto } from '../../../../dtos/commonDtos';
import {
  getCityApi,
  getDistrictApi,
  getZoneApi,
} from '../../../../services/commonApi';
import districtOptionListActions from '../../../../redux/common/actionReducer/district/List';
import CityEdit from './CityEdit';
import zoneListActions from '../../../../redux/common/actionReducer/zone/List';
import { PAGE_TITLE_CITY_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query, params }) {
  const { backUrl = '/city/list' } = query;

  try {
    store.dispatch(singleCityInitActions.singleCityInitRequest());

    const city = await getCityApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder().dto(new CityDto({ id: params.id })).buildJson(),
      ),
    );

    /**
     * get all distrcit with city id
     */
    const districts = await getDistrictApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new DistrictDto({
              cityDto: new CityDto({ id: params.id }),
            }),
          )
          .pageSize(10)
          .pageIndex(0)
          .buildJson(),
      ),
    );

    /**
     * get all zone with city id
     */
    const zones = await getZoneApi(
      store.getState().login.data.token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new ZoneDto({
              cityDto: new CityDto({ id: params.id }),
              active: true,
            }),
          )
          .buildJson(),
      ),
    );

    if (
      city.status === 200 &&
      districts.status === 200 &&
      zones.status === 200
    ) {
      store.dispatch(singleCityInitActions.singleCityInitSuccess(null));
      store.dispatch(singleCityActions.singleCitySuccess(city.data));
      store.dispatch(
        districtOptionListActions.districtOptionListSuccess(districts.data),
      );
      store.dispatch(zoneListActions.zoneListSuccess(zones.data));
    } else {
      store.dispatch(singleCityInitActions.singleCityInitFailure());
    }
  } catch (error) {
    store.dispatch(singleCityInitActions.singleCityInitFailure());
  }
  return {
    chunks: ['cityEdit'],
    title: `${PAGE_TITLE_CITY_EDIT}`,
    component: (
      <Layout>
        <CityEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
