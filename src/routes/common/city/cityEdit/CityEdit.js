import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './CityEdit.css';
import {CITY_EDIT, PRODUCT_EDIT} from '../../../../Resources/Localization';
import history from '../../../../history';
import CityForm from '../../../../components/Common/City/CityForm';

class CityEdit extends React.Component {
  static propTypes = {
    city: PropTypes.objectOf(PropTypes.any).isRequired,
    backUrl: PropTypes.string.isRequired,
  };

  static defaultProps = {
    backUrl: '/city/list',
  };

  redirect = () => {
    history.push(`/city/list`);
  };

  render() {
    const { backUrl, city } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard
          title={`${CITY_EDIT} - ${city && city.name}`}
          isEdit={!!(city && city.id)}
          backUrl={backUrl}
        >
          <CityForm city={city} />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  city: state.singleCity.data ? state.singleCity.data.items[0] : null,
});

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CityEdit));
