import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import s from './CityList.css';
import CPSwitch from '../../../../components/CP/CPSwitch';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import { CityDto } from '../../../../dtos/commonDtos';
import {
  ACTIVE,
  DEACTIVATE,
  NAME,
  STATUS,
  DELETE_CONFIRM,
  YES,
  NO,
  CITY_LIST,
} from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import { pressEnterAndCallApplySearch } from '../../../../utils/helper';
import cityListActions from '../../../../redux/common/actionReducer/city/List';

class CityList extends React.Component {
  static propTypes = {
    cityLoading: PropTypes.bool.isRequired,
    cities: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    cities: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      cityName: '',
      cityStatus: true,
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  /**
   * set value inputs into state for search
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  /**
   * for submit search in city

   */
  prepareContainer = () =>
    new BaseGetDtoBuilder()
      .dto(
        new CityDto({
          active: this.state.cityStatus,
          name: this.state.cityName,
        }),
      )
      .buildJson();

  /**
   * for search in city
   * @returns {Promise<void>}
   */
  async submitSearch() {
    this.props.actions.cityListRequest(this.prepareContainer());
  }

  render() {
    const { cities, cityLoading } = this.props;
    const { cityName, cityStatus } = this.state;
    const columns = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={DELETE_CONFIRM}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to={`/city/edit/${record.key}`}
              onClick={() => {
                this.props.actions.showLoading();
              }}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    return (
      <div className={s.mainContent}>
        <h3>{CITY_LIST}</h3>
        <CardTable data={cities} columns={columns} loading={cityLoading}>
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <CPInput
                    name="cityName"
                    label={NAME}
                    value={cityName}
                    onChange={value => {
                      this.handelChangeInput('cityName', value.target.value);
                    }}
                  />
                </div>

                <div className="col-lg-3 col-md-6 col-sm-12">
                  <div className="align-center">
                    <CPSwitch
                      defaultChecked={cityStatus}
                      checkedChildren={ACTIVE}
                      unCheckedChildren={DEACTIVATE}
                      onChange={value => {
                        this.handelChangeInput('cityStatus', value);
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CardTable>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cities: state.cityList.data.items,
  cityLoading: state.cityList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
      showLoading,
      cityListRequest: cityListActions.cityListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CityList));
