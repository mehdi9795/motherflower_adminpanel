import React from 'react';
import CityList from './CityList';
import Layout from '../../../../components/Template/Layout/Layout';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import { CityDto } from '../../../../dtos/commonDtos';
import { PAGE_TITLE_CITY_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all city for first time with default clause
   */
  store.dispatch(
    cityListActions.cityListRequest(
      new BaseGetDtoBuilder().dto(new CityDto({ active: true })).buildJson(),
    ),
  );

  return {
    chunks: ['cityList'],
    title: `${PAGE_TITLE_CITY_LIST}`,
    component: (
      <Layout>
        <CityList />
      </Layout>
    ),
  };
}

export default action;
