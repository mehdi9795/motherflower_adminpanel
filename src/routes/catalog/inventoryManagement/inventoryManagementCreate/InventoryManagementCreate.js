import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import EditCard from '../../../../components/CP/EditCard';
import s from './InventoryManagementCreate.css';
import { INVENTORY_MANAGEMENT } from '../../../../Resources/Localization';
import InventoryManagementForm from '../../../../components/Catalog/InventoryManagement/InventoryManagementForm';

class CategoryCreate extends React.Component {

  render() {
    return (
      <div className={s.mainContent}>
        <EditCard title={INVENTORY_MANAGEMENT} backUrl="/inventory-management/list">
          <InventoryManagementForm />
        </EditCard>
      </div>
    );
  }
}

export default withStyles(s)(CategoryCreate);
