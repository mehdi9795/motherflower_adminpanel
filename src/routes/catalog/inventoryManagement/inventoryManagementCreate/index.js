import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import InventoryManagementCreate from './InventoryManagementCreate';
import { getDtoQueryString } from '../../../../utils/helper';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import { getVendorBranchApi } from '../../../../services/vendorApi';
import { inventoryManagementLIstSuccess } from '../../../../redux/catalog/action/inventoryManagement';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import childCategoryListActions from '../../../../redux/catalog/actionReducer/category/ChildCategoryList';
import specificationAttributeListActions from '../../../../redux/catalog/actionReducer/specificationAttribute/List';
import { PAGE_TITLE_INVENTORY_CREATE } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all vendor branch for first time with default clause
   */
  const vendorBranchData = await getVendorBranchApi(
    store.getState().login.data.token,
    getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
  );
  store.dispatch(
    vendorBranchListActions.vendorBranchListSuccess(vendorBranchData.data),
  );

  store.dispatch(inventoryManagementLIstSuccess(null));

  /**
   * get all child categories for first time
   */
  store.dispatch(
    childCategoryListActions.childCategoryListRequest(
      new BaseGetDtoBuilder()
        .dto(new CategoryDto({ justChild: true, published: true }))
        .buildJson(),
    ),
  );

  /**
   * get all specification attribute for first time with default clause
   */
  store.dispatch(
    specificationAttributeListActions.specificationAttributeListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  return {
    chunks: ['inventoryManagementCreate'],
    title: `${PAGE_TITLE_INVENTORY_CREATE}`,
    component: (
      <Layout>
        <InventoryManagementCreate />
      </Layout>
    ),
  };
}

export default action;
