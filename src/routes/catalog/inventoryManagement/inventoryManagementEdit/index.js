import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import InventoryManagementEdit from './InventoryManagementEdit';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import { getDtoQueryString } from '../../../../utils/helper';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import { inventoryManagementLIstSuccess } from '../../../../redux/catalog/action/inventoryManagement';
import { getVendorBranchApi } from '../../../../services/vendorApi';
import { getInventoryManagementApi } from '../../../../services/catalogApi';
import { PAGE_TITLE_INVENTORY_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, params }) {
  const inventoryData = await getInventoryManagementApi(
    store.getState().login.data.token,
    getDtoQueryString(JSON.stringify({ dto: { id: params.id } })),
  );
  store.dispatch(inventoryManagementLIstSuccess(inventoryData.data));

  /**
   * get all vendor branch for first time with default clause
   */
  const vendorBranchData = await getVendorBranchApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(new VendorBranchDto({ active: true }))
        .all(true)
        .buildJson(),
    ),
  );
  store.dispatch(
    vendorBranchListActions.vendorBranchListSuccess(vendorBranchData.data),
  );

  return {
    chunks: ['inventoryManagementEdit'],
    title: `${PAGE_TITLE_INVENTORY_EDIT}`,
    component: (
      <Layout>
        <InventoryManagementEdit />
      </Layout>
    ),
  };
}

export default action;
