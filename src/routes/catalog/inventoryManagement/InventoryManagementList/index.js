import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import InventoryManagementList from './inventoryManagementList';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import { getVendorBranchApi } from '../../../../services/vendorApi';
import { getDtoQueryString } from '../../../../utils/helper';
import { inventoryManagementLIstSuccess } from '../../../../redux/catalog/action/inventoryManagement';
import { getInventoryManagementApi } from '../../../../services/catalogApi';
import { PAGE_TITLE_INVENTORY_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  const inventoryData = await getInventoryManagementApi(
    store.getState().login.data.token,
    getDtoQueryString(JSON.stringify({ dto: {} })),
  );
  store.dispatch(inventoryManagementLIstSuccess(inventoryData.data));

  /**
   * get all vendor branch for first time with default clause
   */
  const vendorBranchData = await getVendorBranchApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(new VendorBranchDto({ active: true }))
        .all(true)
        .buildJson(),
    ),
  );
  store.dispatch(
    vendorBranchListActions.vendorBranchListSuccess(vendorBranchData.data),
  );

  return {
    chunks: ['inventoryManagementList'],
    title: `${PAGE_TITLE_INVENTORY_LIST}`,
    component: (
      <Layout>
        <InventoryManagementList />
      </Layout>
    ),
  };
}

export default action;
