import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Select } from 'antd';
import HotKeys from 'react-hot-keys';
import { hideLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import CPList from '../../../../components/CP/CPList/CPList';
import s from './InventoryManagementList.css';
import {
  AGENT_BRANCHES,
  PRODUCT_SPECIFICATION_ATTRIBUTE,
  STATUS,
  NAME,
  SELECT_PRODUCT_FOR_PROMOTION,
  TYPE,
  SHOW_ON_PRODUCT_PAGE,
  ALLOW_FILTERING,
  SELECT,
  NOT_EXIST,
  EXIST,
  ALL,
  CREATE_USERNAME,
  UPDATE_USERNAME,
  CREATE_DATE,
  UPDATE_DATE,
  FROM_DATE,
  TO_DATE,
  VENDOR_BRANCH,
  PRODUCTS,
} from '../../../../Resources/Localization';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import Link from '../../../../components/Link/Link';
import AlertHideActions from '../../../../redux/shared/actionReducer/alert/alert';
import CPAlert from '../../../../components/CP/CPAlert';
import CPSwitch from '../../../../components/CP/CPSwitch';
import CPPersianCalendar from '../../../../components/CP/CPPersianCalendar';
import CPModal from '../../../../components/CP/CPModal';
import CPMultiSelect from '../../../../components/CP/CPMultiSelect';
import CPAvatar from '../../../../components/CP/CPAvatar';
import specificationAttributeListActions from '../../../../redux/catalog/actionReducer/specificationAttribute/List';
import {
  InventoryManagementDto,
  SpecificationAttributeDto,
} from '../../../../dtos/catalogDtos';
import history from '../../../../history';
import CPSelect from '../../../../components/CP/CPSelect';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import inventoryManagementListActions from '../../../../redux/catalog/actionReducer/inventoryManagement/List';
import CPPagination from '../../../../components/CP/CPPagination';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class inventoryManagementList extends React.Component {
  static propTypes = {
    inventories: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    inventoryCount: PropTypes.number,
    inventoryLoading: PropTypes.bool,
    errorMessage: PropTypes.string,
    errorAlert: PropTypes.bool,
    specificationAttributes: PropTypes.arrayOf(PropTypes.object),
    specificationAttributeCount: PropTypes.number,
    specificationAttributeLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    errorMessage: '',
    errorAlert: false,
    inventories: [],
    vendorBranches: [],
    inventoryCount: 0,
    inventoryLoading: false,
    specificationAttributes: [],
    specificationAttributeCount: 0,
    specificationAttributeLoading: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      visibleModal: false,
      selectedAttributeIds: [],
      searchFromDate: '',
      searchToDate: '',
      searchAttributeIds: [],
      searchVendorBranches: [],
      searchStatus: '2',
      selectedRows: [],
      currentPage: 1,
      pageSize: 10,
    };
    this.attributeArray = [];
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  // componentWillReceiveProps() {
  //   this.props.actions.hideLoading();
  // }

  onCloseErrorMessage = () => {
    this.props.actions.AlertHideRequest();
  };

  onSelectedAttributes = (selectedRowKeys, selectedRows) => {
    this.setState({ searchAttributeIds: selectedRowKeys, selectedRows });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  handleCancelModal = () => {
    this.setState({ visibleModal: false });
  };

  handleOkModal = () => {
    const { selectedRows } = this.state;
    // this.attributeArray = [];
    selectedRows.map(item =>
      this.attributeArray.push(<Option key={item.key}>{item.name}</Option>),
    );
    this.setState({ visibleModal: false });
  };

  showModal = () => {
    const selectedIds = [];
    this.attributeArray.map(item => selectedIds.push(item.key));

    this.props.actions.specificationAttributeListRequest(
      new BaseGetDtoBuilder()
        .notExpectedIds(selectedIds)
        .pageIndex(0)
        .pageSize(10)
        .includes(['imageDtos'])
        .buildJson(),
    );

    this.setState({ visibleModal: true, currentPage: 1 });
  };

  submitSearchAttribute = () => {
    const { searchName, searchAttributeIds } = this.state;

    this.props.actions.specificationAttributeListRequest(
      new BaseGetDtoBuilder()
        .dto(new SpecificationAttributeDto({ name: searchName }))
        .pageIndex(0)
        .pageSize(10)
        .notExpectedIds(searchAttributeIds)
        .includes(['imageDtos'])
        .buildJson(),
    );
  };

  submitSearch = () => {
    const {
      searchFromDate,
      searchToDate,
      searchAttributeIds,
      searchVendorBranches,
      searchStatus,
    } = this.state;

    const vendorBranchArray = [];
    const attributeArray = [];

    searchAttributeIds.map(item => {
      attributeArray.push(new SpecificationAttributeDto({ id: item }));
      return null;
    });

    searchVendorBranches.map(item => {
      vendorBranchArray.push(new VendorBranchDto({ id: item }));
      return null;
    });

    this.props.actions.inventoryManagementListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new InventoryManagementDto({
            searchInventoryStatus:
              searchStatus === '2' ? undefined : searchStatus,
            searchFromPersianDateTimeUtc: searchFromDate,
            searchToPersianDateTimeUtc: searchToDate,
            vendorBranchDtos: vendorBranchArray,
            SpecificationAttributeDtos: attributeArray,
          }),
        )
        .buildJson(),
    );
  };

  gotToCreate = () => {
    history.push('/inventory-management/create');
  };

  onChangePagination = page => {
    const { searchAttributeIds, pageSize } = this.state;

    this.setState({ currentPage: page }, () =>
      this.props.actions.specificationAttributeListRequest(
        new BaseGetDtoBuilder()
          .notExpectedIds(searchAttributeIds)
          .pageIndex(page - 1)
          .pageSize(pageSize)
          .includes(['imageDtos'])
          .buildJson(),
      ),
    );
  };

  onChangePagination = page => {
    const { pageSize, searchAttributeIds } = this.state;
    const selectedIds = [];
    this.attributeArray.map(item => selectedIds.push(item.key));

    this.setState({ currentPage: page }, () =>
      this.props.actions.specificationAttributeListRequest(
        new BaseGetDtoBuilder()
          .notExpectedIds(searchAttributeIds)
          .pageIndex(page - 1)
          .pageSize(pageSize)
          .includes(['imageDtos'])
          .buildJson(),
      ),
    );
  };

  renderAttributeModal = () => {
    const {
      visibleModal,
      selectedAttributeIds,
      searchName,
      currentPage,
    } = this.state;
    const {
      specificationAttributeLoading,
      specificationAttributes,
      specificationAttributeCount,
    } = this.props;

    const specificationAttributeArray = [];
    const columns = [
      {
        title: '',
        dataIndex: 'imageUrl',
        render: (text, record) => <CPAvatar src={record.imageUrl} />,
      },
      {
        title: NAME,
        dataIndex: 'name',
      },
      {
        title: TYPE,
        dataIndex: 'typeId',
      },
      {
        title: ALLOW_FILTERING,
        dataIndex: 'allowFiltering',
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              defaultChecked={record.allowFiltering}
            />
          </div>
        ),
      },
      {
        title: SHOW_ON_PRODUCT_PAGE,
        dataIndex: 'showOnProductPage',
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              defaultChecked={record.showOnProductPage}
            />
          </div>
        ),
      },
    ];

    specificationAttributes.map(item => {
      const obj = {
        name: item.name,
        allowFiltering: item.allowFiltering,
        key: item.id.toString(),
        showOnProductPage: item.showOnProductPage,
        displayOrder: item.displayOrder,
        imageUrl:
          item.imageDtos && item.imageDtos.length > 0
            ? item.imageDtos[0].url
            : '',
        imageId:
          item.imageDtos && item.imageDtos.length > 0
            ? item.imageDtos[0].id
            : '',
        typeId: item.specificationAttributeType,
      };
      specificationAttributeArray.push(obj);
      return null;
    });

    const rowSelection = {
      selectedAttributeIds,
      onChange: this.onSelectedAttributes,
    };

    return (
      <CPModal
        visible={visibleModal}
        handleCancel={this.handleCancelModal}
        handleOk={this.handleOkModal}
        title={SELECT_PRODUCT_FOR_PROMOTION}
        className="max_modal"
      >
        <div className="row">
          <div className="col-sm-12">
            <CPList
              data={specificationAttributeArray}
              columns={columns}
              count={specificationAttributeCount}
              loading={specificationAttributeLoading}
              rowSelection={rowSelection}
              withCheckBox
              hideAdd
              pagination={false}
            >
              <div className="searchBox">
                <div className={s.searchFildes}>
                  <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <CPInput
                        label={NAME}
                        value={searchName}
                        onChange={value => {
                          this.handelChangeInput(
                            'searchName',
                            value.target.value,
                          );
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className={s.field}>
                  <CPButton
                    size="large"
                    shape="circle"
                    type="primary"
                    icon="search"
                    onClick={this.submitSearchAttribute}
                  />
                </div>
              </div>
            </CPList>
            <CPPagination
              total={specificationAttributeCount}
              current={currentPage}
              onChange={this.onChangePagination}
              onShowSizeChange={this.onShowSizeChange}
            />
          </div>
        </div>
      </CPModal>
    );
  };

  render() {
    const {
      errorAlert,
      errorMessage,
      inventories,
      inventoryCount,
      inventoryLoading,
      vendorBranches,
    } = this.props;

    const {
      searchFromDate,
      searchToDate,
      searchAttributeIds,
      searchVendorBranches,
      searchStatus,
    } = this.state;

    const inventoryArray = [];
    const vendorBranchArray = [];
    const columns = [
      {
        title: CREATE_USERNAME,
        dataIndex: 'createUser',
        key: '1',
      },
      {
        title: UPDATE_USERNAME,
        dataIndex: 'updateUser',
        key: '2',
      },
      {
        title: CREATE_DATE,
        dataIndex: 'insertDate',
        key: '3',
      },
      {
        title: UPDATE_DATE,
        dataIndex: 'updateDate',
        key: '4',
      },
      {
        title: VENDOR_BRANCH,
        dataIndex: 'vendorBranches',
        fixed: 'left',
        key: '5',
        width: 250,
      },
      {
        title: PRODUCTS,
        dataIndex: 'products',
        fixed: 'left',
        key: 'products',
        width: 170,
      },
      {
        title: PRODUCT_SPECIFICATION_ATTRIBUTE,
        dataIndex: 'attributes',
        fixed: 'left',
        key: 'attributes',
        width: 220,
      },
      {
        title: STATUS,
        dataIndex: 'status',
        fixed: 'left',
        width: 70,
        key: 'status',
        render: (text, record) => (
          <div>
            <CPSwitch size="small" disabled defaultChecked={record.status} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Edit',
        width: 50,
        fixed: 'left',
        key: 'Edit',
        render: (text, record) => (
          <div>
            <Link
              to={`/inventory-management/edit/${record.key}`}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    vendorBranches.map(item =>
      vendorBranchArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    inventories.map(item => {
      let vendorBranchNames = '';
      let attributeNames = '';
      let productCodes = '';
      item.vendorBranchDtos.map(item2 => {
        vendorBranchNames = `${vendorBranchNames} ${item2.name},`;
        return null;
      });

      item.specificationAttributeDtos.map(item2 => {
        attributeNames = `${attributeNames} ${item2.name},`;
        return null;
      });

      item.vendorBranchProductDtos.map(item2 => {
        productCodes = `${productCodes} ${item2.productDto.code},`;
        return null;
      });

      const obj = {
        key: item.id,
        createUser: item.createdUerDto ? item.createdUerDto.displayName : '',
        updateUser: item.updatedUserDto ? item.updatedUserDto.displayName : '',
        insertDate: item.persianCreatedOnUtc,
        updateDate: item.persianUpdatedOnUtc,
        vendorBranches: vendorBranchNames.substring(
          0,
          vendorBranchNames.length - 1,
        ),
        attributes: attributeNames.substring(0, attributeNames.length - 1),
        products: productCodes.substring(0, productCodes.length - 1),
        status: item.inventoryStatus,
      };
      inventoryArray.push(obj);
      return null;
    });

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.gotToCreate}>
        <div className={s.mainContent}>
          {/* <h3>{LIST}</h3> */}
          {errorAlert && (
            <CPAlert
              showIcon
              closable
              onClose={this.onCloseErrorMessage}
              type="error"
              message="خطا"
              description={errorMessage}
            />
          )}
          <CPList
            data={inventoryArray}
            count={inventoryCount}
            columns={columns}
            loading={inventoryLoading}
            onAddClick={this.gotToCreate}
            scroll={{ x: 1300 }}
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{FROM_DATE}</label>
                    <CPPersianCalendar
                      placeholder={FROM_DATE}
                      // format="jYYYY/jMM/jDD"
                      onChange={(unix, formatted) =>
                        this.handelChangeInput('searchFromDate', formatted)
                      }
                      preSelected={searchFromDate}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{TO_DATE}</label>
                    <CPPersianCalendar
                      placeholder={TO_DATE}
                      // format="jYYYY/jMM/jDD"
                      onChange={(unix, formatted) =>
                        this.handelChangeInput('searchToDate', formatted)
                      }
                      preSelected={searchToDate}
                    />
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-12">
                    <label>{PRODUCT_SPECIFICATION_ATTRIBUTE}</label>
                    <div className="col-sm-12 attributeSearch">
                      <CPMultiSelect
                        placeholder={PRODUCT_SPECIFICATION_ATTRIBUTE}
                        value={searchAttributeIds}
                        onChange={value =>
                          this.handelChangeInput('searchAttributeIds', value)
                        }
                      >
                        {this.attributeArray}
                      </CPMultiSelect>
                      <CPButton onClick={this.showModal}>{SELECT}</CPButton>
                    </div>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{AGENT_BRANCHES}</label>
                    <CPMultiSelect
                      placeholder={AGENT_BRANCHES}
                      value={searchVendorBranches}
                      onChange={value =>
                        this.handelChangeInput('searchVendorBranches', value)
                      }
                    >
                      {vendorBranchArray}
                    </CPMultiSelect>
                  </div>
                  <div className="col-lg-2 col-md-6 col-sm-12">
                    <label>{STATUS}</label>
                    <CPSelect
                      value={searchStatus}
                      onChange={value =>
                        this.handelChangeInput('searchStatus', value)
                      }
                    >
                      <Option key={2}>{ALL}</Option>
                      <Option key={1}>{EXIST}</Option>
                      <Option key={0}>{NOT_EXIST}</Option>
                    </CPSelect>
                  </div>
                </div>
              </div>
              <div className={s.field}>
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
          {this.renderAttributeModal()}
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  inventories: state.inventoryManagementList.data
    ? state.inventoryManagementList.data.items
    : [],
  inventoryCount: state.inventoryManagementList.data
    ? state.inventoryManagementList.data.count
    : 0,
  inventoryLoading: state.inventoryManagementList.loading,
  specificationAttributes: state.specificationAttributeList.data
    ? state.specificationAttributeList.data.items
    : [],
  specificationAttributeCount: state.specificationAttributeList.data
    ? state.specificationAttributeList.data.count
    : 0,
  specificationAttributeLoading: state.specificationAttributeList.loading,
  errorAlert: state.editFormAlert.loading,
  errorMessage: state.editFormAlert.data,
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      AlertHideRequest: AlertHideActions.editFormAlertHideRequest,
      hideLoading,
      specificationAttributeListRequest:
        specificationAttributeListActions.specificationAttributeListRequest,
      inventoryManagementListRequest:
        inventoryManagementListActions.inventoryManagementListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(inventoryManagementList));
