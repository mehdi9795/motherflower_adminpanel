import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import s from './ProductCreate.css';
import { PRODUCT_CREATE } from '../../../../Resources/Localization';
import ProductForm from '../../../../components/Catalog/Product/ProductForm';

class ProductCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
    vendorId: PropTypes.string.isRequired,
    vendors: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    vendors: [],
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { backUrl, vendorId, vendors } = this.props;
    console.log('vendors',vendors)
    return (
      <div className={s.mainContent}>
        <EditCard
          // title={`${PRODUCT_CREATE}    ( ${vendors &&
          // vendors.find(item => item.id.toString() === vendorId).name} )`}
          backUrl={backUrl}
        >
          <ProductForm vendorId={vendorId} backUrl={backUrl} />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  vendors: state.vendorList.data
    ? state.vendorList.data.items
    : [],
});

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductCreate));
