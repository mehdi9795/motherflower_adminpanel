import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import ProductCreate from './ProductCreate';
import { AgentDto, VendorBranchDto } from '../../../../dtos/vendorDtos';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import categoryListActions from '../../../../redux/catalog/actionReducer/category/CategoryList';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import {
  getCategoryApi,
  getProductTypesApi,
  getTagApi,
} from '../../../../services/catalogApi';
import singleVendorAgentActions from '../../../../redux/vendor/actionReducer/vendor/Single';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import {
  getAgentApi,
  getVendorBranchApi,
} from '../../../../services/vendorApi';
import { getDtoQueryString } from '../../../../utils/helper';

import productTagActions from '../../../../redux/catalog/actionReducer/product/ProductTag';
import productTypesListActions from '../../../../redux/catalog/actionReducer/productType/List';
import singleProductInitActions from '../../../../redux/catalog/actionReducer/product/SingleProductInit';
import {PAGE_TITLE_CITY_LIST, PAGE_TITLE_PRODUCT_CREATE} from "../../../../Resources/Localization";
import {BaseGetDtoBuilder} from "../../../../dtos/dtoBuilder";

async function action({ store, query }) {
  const backUrl = query.back === 'vendor' ? '/vendor/list' : '/product/list';
  const productSingleData = [];
  /**
   * get all tags with true status
   */
  productSingleData.tag = new BaseGetDtoBuilder().all(true).buildJson();

  /**
   * get all vendors with true status
   */

  productSingleData.vendor = new BaseGetDtoBuilder().dto(new AgentDto({ active: true })).buildJson();

  /**
   * get all category for first time
   */
  productSingleData.category = new BaseGetDtoBuilder().dto(new CategoryDto({
    makeTree: true,
    published: true
  })).buildJson();


  /**
   * get all vendor branch of vendor by vendorId
   */

  productSingleData.vendorBranch = new BaseGetDtoBuilder().dto(new VendorBranchDto({
    vendorId: query.vendorId,
    active: true
  })).buildJson();

  /**
   * get single vendor by vendorId
   */
  productSingleData.singleVendor = new BaseGetDtoBuilder().dto(new AgentDto({
    id: query.vendorId,
  })).includes(['vendorBranches', 'VendorProductTypes']).buildJson();

  try {
    store.dispatch(singleProductInitActions.singleProductInitRequest());

    const tagDtos = await getTagApi(
      store.getState().login.data.token,
      getDtoQueryString(productSingleData.tag),
    );

    const vendorDtos = await getAgentApi(
      store.getState().login.data.token,
      getDtoQueryString(productSingleData.vendor),
    );

    const categoryDtos = await getCategoryApi(
      store.getState().login.data.token,
      getDtoQueryString(productSingleData.category),
    );

    const vendorBranchDtos = await getVendorBranchApi(
      store.getState().login.data.token,
      getDtoQueryString(productSingleData.vendorBranch),
    );

    const singleVendorDto = await getAgentApi(
      store.getState().login.data.token,
      getDtoQueryString(productSingleData.singleVendor),
    );

    if (
      tagDtos.status === 200 &&
      vendorDtos.status === 200 &&
      categoryDtos.status === 200 &&
      vendorBranchDtos.status === 200 &&
      singleVendorDto.status === 200
    ) {
      /**
       * get product type of vendor with product type ids
       * @type {Array}
       */
      const productTypeIdArray = [];
      if (
        singleVendorDto &&
        singleVendorDto.data &&
        singleVendorDto.data.items &&
        singleVendorDto.data.items.length > 0
      ) {
        if (
          singleVendorDto.data.items[0] &&
          singleVendorDto.data.items[0].vendorProductTypeDtos
        )
          singleVendorDto.data.items[0].vendorProductTypeDtos.map(item =>
            productTypeIdArray.push(item.productTypeDto.id),
          );
      }
      const productTypeDto = await getProductTypesApi(
        store.getState().login.data.token,
        getDtoQueryString(JSON.stringify(`{ids:[${productTypeIdArray}]}`)),
      );

      if (productTypeDto) {
        store.dispatch(singleProductInitActions.singleProductInitSuccess(null));
        store.dispatch(vendorListActions.vendorListSuccess(vendorDtos.data));
        store.dispatch(
          categoryListActions.categoryListSuccess(categoryDtos.data),
        );
        store.dispatch(productTagActions.productTagSuccess(tagDtos.data));
        store.dispatch(vendorBranchListActions.vendorBranchListSuccess(vendorBranchDtos.data));
        store.dispatch(
          singleVendorAgentActions.singleVendorAgentSuccess(
            singleVendorDto.data,
          ),
        );
        store.dispatch(
          productTypesListActions.productTypesListSuccess(productTypeDto.data),
        );
      } else {
        store.dispatch(singleProductInitActions.singleProductInitFailure());
      }
    } else {
      store.dispatch(singleProductInitActions.singleProductInitFailure());
    }
  } catch (error) {
    store.dispatch(singleProductInitActions.singleProductInitFailure());
  }

  return {
    chunks: ['productCreate'],
    title: `${PAGE_TITLE_PRODUCT_CREATE}`,
    component: (
      <Layout>
        <ProductCreate backUrl={backUrl} vendorId={query.vendorId} />
      </Layout>
    ),
  };
}

export default action;
