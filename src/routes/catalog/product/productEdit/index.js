import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import categoryListActions from '../../../../redux/catalog/actionReducer/category/CategoryList';
import {
  CategoryDto,
  ProductDto,
  ProductSpecificationAttributeDto,
  TagDto,
} from '../../../../dtos/catalogDtos';
import { AgentDto, VendorBranchDto } from '../../../../dtos/vendorDtos';
import {
  getCategoryApi,
  getProductApi,
  getProductSpecificationAttributeApi,
  getProductTypesApi,
  getTagApi,
} from '../../../../services/catalogApi';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import productSpecificationAttributeListActions from '../../../../redux/catalog/actionReducer/product/ProductSpecificationAttributeList';
import productTagActions from '../../../../redux/catalog/actionReducer/product/ProductTag';
import singleProductInitActions from '../../../../redux/catalog/actionReducer/product/SingleProductInit';
import singleProductActions from '../../../../redux/catalog/actionReducer/product/SingleProduct';
import productTypesListActions from '../../../../redux/catalog/actionReducer/productType/List';
import singleVendorAgentActions from '../../../../redux/vendor/actionReducer/vendor/Single';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import {
  getAgentApi,
  getVendorBranchApi,
} from '../../../../services/vendorApi';
import { getDtoQueryString } from '../../../../utils/helper';
import ProductEdit from './ProductEdit';
import { VendorBranchProductPriceDto } from '../../../../dtos/samDtos';
import { getVendorBranchProductPriceApi } from '../../../../services/samApi';
import vbProductPriceListActions from '../../../../redux/sam/actionReducer/vendorBranchProductPrice/List';
import { ImageDto } from '../../../../dtos/cmsDtos';
import { getImageApi } from '../../../../services/cmsApi';
import imageListActions from '../../../../redux/cms/actionReducer/image/List';
import { PAGE_TITLE_PRODUCT_EDIT } from '../../../../Resources/Localization';
import {
  BaseGetDtoBuilder,
  BaseListGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

async function action({ store, query, params }) {
  const backUrl = query.back === 'vendor' ? '/vendor/list' : '/product/list';

  try {
    store.dispatch(singleProductInitActions.singleProductInitRequest());

    /**
     * get single product by id
     */
    const jsonSingleProduct = new BaseGetDtoBuilder()
      .dto(new ProductDto({ id: params.id }))
      .includes([
        'vendorDto',
        'vendorBranches',
        'productCategoryDtos',
        'User',
        'productTagDtos',
        'productRelatedCategoryDto',
      ])
      .buildJson();

    const product = await getProductApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonSingleProduct),
    );

    /**
     * get all vendors with true status
     */
    const jsonVendorDto = new BaseGetDtoBuilder().all(true).buildJson();

    const vendorDtos = await getAgentApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonVendorDto),
    );

    /**
     * get all category for first time
     */
    const jsonCategorDto = new BaseGetDtoBuilder()
      .dto(new CategoryDto({ makeTree: true, published: true }))
      .buildJson();
    const categoryDtos = await getCategoryApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonCategorDto),
    );

    if (
      product.status === 200 &&
      vendorDtos.status === 200 &&
      categoryDtos.status === 200
    ) {
      /**
       * get all vendor branch of vendor by vendorId
       */
      const jsonVendorBranch = new BaseGetDtoBuilder()
        .dto(
          new VendorBranchDto({
            vendorId: product.data.items[0].vendorDto.id,
            active: true,
          }),
        )
        .buildJson();

      const vendorBranchDtos = await getVendorBranchApi(
        store.getState().login.data.token,
        getDtoQueryString(jsonVendorBranch),
      );

      /**
       * get all tags with true status
       */
      const jsonTagDto = new BaseGetDtoBuilder()
        .dto(
          new TagDto({
            published: true,
            categoryTagDtos: [
              {
                categoryDto: {
                  id:
                    product.data.items[0].productCategoryDtos[0].categoryDto.id,
                },
              },
            ],
          }),
        )
        .buildJson();
      const tagDtos = await getTagApi(
        store.getState().login.data.token,
        getDtoQueryString(jsonTagDto),
      );

      /**
       * get single vendor by vendorId
       */
      const jsonSingleVendorDto = new BaseGetDtoBuilder()
        .dto(new AgentDto({ id: product.data.items[0].vendorDto.id }))
        .includes(['vendorBranches', 'VendorProductTypes'])
        .buildJson();
      const singleVendorDto = await getAgentApi(
        store.getState().login.data.token,
        getDtoQueryString(jsonSingleVendorDto),
      );

      /**
       * get vedor Branch Product Price with productId
       * @type {{dtos, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, removeChildtoParentList}}
       */
      const jsonProductPriceDto = new BaseListGetDtoBuilder()
        .dtos([
          new VendorBranchProductPriceDto({
            productDto: new ProductDto({ id: product.data.items[0].id }),
          }),
        ])
        .includes(['VendorBranch'])
        .buildJson();

      const productPriceDto = await getVendorBranchProductPriceApi(
        store.getState().login.data.token,
        getDtoQueryString(jsonProductPriceDto),
      );

      /**
       * get product specification attribute with productId
       * @type
       */
      const jsonSpecificationAttributeDto = new BaseGetDtoBuilder()
        .dto(
          new ProductSpecificationAttributeDto({
            productDto: new ProductDto({ id: product.data.items[0].id }),
          }),
        )
        .disabledCount(false)
        .buildJson();
      const productSpecificationDto = await getProductSpecificationAttributeApi(
        store.getState().login.data.token,
        getDtoQueryString(jsonSpecificationAttributeDto),
      );

      /**
       * get product images with productId
       * @type
       */

      const jsonImageDto = new BaseGetDtoBuilder()
        .dto(
          new ImageDto({
            entityId: product.data.items[0].id,
            imagetype: 'Product',
          }),
        )
        .disabledCount(false)
        .buildJson();

      const imageDtos = await getImageApi(
        store.getState().login.data.token,
        getDtoQueryString(jsonImageDto),
      );

      if (
        singleVendorDto.status === 200 &&
        productPriceDto.status === 200 &&
        productSpecificationDto.status === 200 &&
        imageDtos.status === 200 &&
        vendorBranchDtos.status === 200
      ) {
        /**
         * get product type of vendor with product type ids
         * @type {Array}
         */
        const productTypeIdArray = [];
        singleVendorDto.data.items[0].vendorProductTypeDtos.map(item =>
          productTypeIdArray.push(item.productTypeDto.id),
        );
        const productTypeDto = await getProductTypesApi(
          store.getState().login.data.token,
          getDtoQueryString(JSON.stringify(`{ids:[${productTypeIdArray}]}`)),
        );

        if (productTypeDto) {
          store.dispatch(
            singleProductInitActions.singleProductInitSuccess(null),
          );
          store.dispatch(
            singleProductActions.singleProductSuccess(product.data),
          );
          store.dispatch(vendorListActions.vendorListSuccess(vendorDtos.data));
          store.dispatch(
            categoryListActions.categoryListSuccess(categoryDtos.data),
          );
          store.dispatch(productTagActions.productTagSuccess(tagDtos.data));
          store.dispatch(
            vendorBranchListActions.vendorBranchListSuccess(
              vendorBranchDtos.data,
            ),
          );
          store.dispatch(
            singleVendorAgentActions.singleVendorAgentSuccess(
              singleVendorDto.data,
            ),
          );
          store.dispatch(imageListActions.imageListSuccess(imageDtos.data));
          store.dispatch(
            productTypesListActions.productTypesListSuccess(
              productTypeDto.data,
            ),
          );
          store.dispatch(
            vbProductPriceListActions.vendorBranchProductPriceListSuccess(
              productPriceDto.data,
            ),
          );
          store.dispatch(
            productSpecificationAttributeListActions.productSpecificationAttributeListSuccess(
              productSpecificationDto.data,
            ),
          );
        }
      }
    } else {
      store.dispatch(singleProductInitActions.singleProductInitFailure());
    }
  } catch (error) {
    store.dispatch(singleProductInitActions.singleProductInitFailure());
  }
  return {
    chunks: ['productEdit'],
    title: `${PAGE_TITLE_PRODUCT_EDIT}`,
    component: (
      <Layout>
        <ProductEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
