import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './ProductEdit.css';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT,
  PRODUCT_EDIT,
} from '../../../../Resources/Localization';
import history from '../../../../history';
import ProductForm from '../../../../components/Catalog/Product/ProductForm';
import productDeleteActions from '../../../../redux/catalog/actionReducer/product/ProductDelete';

class ProductEdit extends React.Component {
  static propTypes = {
    product: PropTypes.objectOf(PropTypes.any),
    backUrl: PropTypes.string.isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    flatermission: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    backUrl: '/product/list',
    product: {},
    flatermission: {},
  };

  componentWillMount() {
    this.showPopHover = true;
    Object.keys(this.props.flatermission).map(item => {
      if (
        this.props.flatermission[item].resourceValue ===
        'product-management-info-create'
      )
        this.showPopHover = false;
      return null;
    });
  }

  onDelete = () => {
    const { product } = this.props;

    const data = {
      id: product.id,
      status: 'edit',
    };

    this.props.actions.productDeleteRequest(data);
  };

  redirect = () => {
    history.push(`/product/list`);
  };

  render() {
    const { backUrl, product } = this.props;

    const href = `/product/create?vendorId=${
      product && product.vendorDto ? product.vendorDto.id : 0
    }`;
    const href2 = `https://motherflower.com/product-details/${product &&
      product.id}/${
      product && product.vendorBranchProductDtos
        ? product.vendorBranchProductDtos[0].vendorBranchDto.id
        : 0
    }`;

    const renderActions = (
      <div>
        <div>
          <a href={href}>ایجاد محصول</a>
        </div>
        <div>
          <a href={href2} target="_blank">
            محصول در سایت
          </a>
        </div>
      </div>
    );

    return (
      <div className={s.mainContent}>
        <EditCard
          title={`${PRODUCT_EDIT} - ${product && product.name}    ( ${product &&
            product.vendorDto.name} )`}
          isEdit={!!(product && product.id)}
          backUrl={backUrl}
          onDelete={this.onDelete}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT}
          showPopHover={this.showPopHover}
          renderActions={renderActions}
        >
          <ProductForm product={product} backUrl={backUrl} />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product:
    state.singleProduct.data &&
    state.singleProduct.data.items &&
    state.singleProduct.data.items.length > 0
      ? state.singleProduct.data.items[0]
      : null,
  flatermission:
    state.permission.data && state.permission.data.items.length > 0
      ? state.permission.data.items[0].flatermission
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      productDeleteRequest: productDeleteActions.productDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductEdit));
