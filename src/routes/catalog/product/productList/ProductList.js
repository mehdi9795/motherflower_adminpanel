/* eslint-disable react/sort-comp */
import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import HotKeys from 'react-hot-keys';
import { Select } from 'antd';
import { connect } from 'react-redux';
import cs from 'classnames';
import { bindActionCreators } from 'redux';
import CPList from '../../../../components/CP/CPList/CPList';
import s from './ProductList.css';
import {
  PRODUCT_LIST_CODE,
  PRODUCT_LIST_NAME,
  PRODUCT_LIST_CATEGORY,
  PRODUCT_PRODUCT_TYPE,
  AGENT_NAME,
  STATUS,
  ACTIVE,
  YES,
  NO,
  PRODUCT_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT,
  SELECT_VENDOR,
  CONTINUE,
  DEACTIVATE,
  SEARCH_PRICE,
  SEARCH_FROM_PRICE,
  SEARCH_TO_PRICE,
  VENDOR_BRANCH_NAME,
  ALL_VENDORS,
  ALL_VENDOR_BRANCH,
  SELECT,
  CONFIRMATION,
  CODE,
  FILE_PATH,
  ALL_TYPES,
  ONLY_MARKED,
  ALL,
  ID,
  PRODUCT_LIST_CODE_VENDOR,
  SITE_PRODUCT_ID,
} from '../../../../Resources/Localization';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import CPMultiSelect from '../../../../components/CP/CPMultiSelect/CPMultiSelect';
import CPSwitch from '../../../../components/CP/CPSwitch';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import {
  ProductDto,
  ProductCategoryDto,
  CategoryDto,
  ProductTypeDto,
  ProductSpecificationAttributeDto,
} from '../../../../dtos/catalogDtos';
import { RateDto } from '../../../../dtos/cmsDtos';
import history from '../../../../history';
import { AgentDto, VendorBranchDto } from '../../../../dtos/vendorDtos';
import CPModal from '../../../../components/CP/CPModal';
import ratePostActions from '../../../../redux/cms/actionReducer/rate/Post';
import productDeleteActions from '../../../../redux/catalog/actionReducer/product/ProductDelete';
import singleProductActions from '../../../../redux/catalog/actionReducer/product/SingleProduct';
import productListActions from '../../../../redux/catalog/actionReducer/product/ProductList';
import productPutActions from '../../../../redux/catalog/actionReducer/product/ProductPut';
import CPAvatar from '../../../../components/CP/CPAvatar';
import CPPermission from '../../../../components/CP/CPPermission/CPPermission';
import CPPagination from '../../../../components/CP/CPPagination';
import CPRadio from '../../../../components/CP/CPRadio';
import CPCarousel from '../../../../components/CP/CPCarousel';
import CPInputNumber from '../../../../components/CP/CPInputNumber';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import productSpecificationAttributeListActions from '../../../../redux/catalog/actionReducer/product/ProductSpecificationAttributeList';
import CPTree from '../../../../components/CP/CPTree';
import { treeModel } from '../../../../utils/model';
import { getProductSpecificationAttributeApi } from '../../../../services/catalogApi';
import {
  getDtoQueryString,
  pressEnterAndCallApplySearch,
} from '../../../../utils/helper';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import { OptionBuilder } from '../../../../components/componentHelper';
import { VendorBranchPromotionDto } from '../../../../dtos/samDtos';

const { Option } = Select;

class productList extends React.Component {
  static propTypes = {
    products: PropTypes.arrayOf(PropTypes.object),
    productCount: PropTypes.number,
    categoryChildren: PropTypes.arrayOf(PropTypes.object),
    productTypes: PropTypes.arrayOf(PropTypes.object),
    vendors: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    productSpecificationAttributes: PropTypes.arrayOf(PropTypes.object),
    categories: PropTypes.arrayOf(PropTypes.object),
    productLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    filterPermission: PropTypes.objectOf(PropTypes.any),
    loginData: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    products: [],
    categories: [],
    productCount: 0,
    categoryChildren: [],
    productTypes: [],
    vendors: [],
    vendorBranches: [],
    productSpecificationAttributes: [],
    productLoading: false,
    filterPermission: {},
    loginData: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      vendorId: '0',
      productTypeId: '0',
      childCategorySelected: [],
      name: '',
      code: '',
      siteCode: '',
      statusProduct: 'disablePublishedFilter',
      visibleModal: false,
      selectedvendorId: '',
      rateType: '',
      value: 0.0,
      entityId: '',
      currentPage: 1,
      pageSize: 20,
      searchPriceType: 'Pickup',
      searchFromPrice: null,
      searchToPrice: null,
      dataImage: [],
      vendorBranchIds: [],
      visibleCategoryModal: false,
      categoryArray: [],
      categorySelectedArray: [],
      categorySelectedIds: [],
      marked: false,
      specificationAttributeNameArray: [],
      searchB2BSupport: false,
    };
    this.submitSearch = this.submitSearch.bind(this);
    this.showModalSelectVendor = this.showModalSelectVendor.bind(this);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    const { vendors } = this.props;
    this.props.actions.hideLoading();

    if (vendors.length > 0) {
      this.setState({ selectedvendorId: vendors[0].id.toString() });
    }
  }

  onDelete = id => {
    const data = {
      id,
      listDto: this.prepareContainer(),
      status: 'list',
    };

    this.props.actions.productDeleteRequest(data);
  };

  onChangePagination = page => {
    this.setState({ currentPage: page }, () =>
      this.props.actions.productListRequest(this.prepareContainer()),
    );
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () =>
      this.props.actions.productListRequest(this.prepareContainer()),
    );
  };

  /**
   * set value inputs into state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
    if (inputName === 'vendorId') {
      this.setState({ vendorBranchIds: [] });
      const jsonVendorBranch = new BaseGetDtoBuilder()
        .dto(new VendorBranchDto({ vendorId: value, active: true }))
        .buildJson();
      this.props.actions.vendorBranchListRequest(jsonVendorBranch);
    } else if (inputName === 'searchPriceType')
      this.setState({ searchB2BSupport: value === 'B2B' });
    else if (inputName === 'searchB2BSupport')
      this.setState({ searchPriceType: value ? 'B2B' : 'Pickup' });
  };

  /**
   * chnage child category value when select Option of comboBox category
   * @param value
   */
  categoryChange = value => {
    this.setState({
      categorySelectedIds: value,
    });
  };

  /**
   * for submit search in product
   */
  prepareContainer = () => {
    const {
      currentPage,
      pageSize,
      searchPriceType,
      searchFromPrice,
      searchToPrice,
      vendorBranchIds,
      vendorId,
      siteCode,
      code,
      name,
      statusProduct,
      productTypeId,
      marked,
      categorySelectedIds,
      searchB2BSupport,
    } = this.state;
    const productCategoryDtos = [];

    categorySelectedIds.map(categoryId => {
      productCategoryDtos.push(
        new ProductCategoryDto({
          categoryDto: new CategoryDto({ id: categoryId }),
        }),
      );
      return null;
    });

    const vendorBranchDtoArray = [];
    vendorBranchIds.map(item => {
      vendorBranchDtoArray.push(new VendorBranchDto({ id: item }));
      return null;
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new ProductDto({
          productTypeDto: new ProductTypeDto({ id: productTypeId }),
          productCategoryDtos,
          searchFromBasePrice: searchB2BSupport ? undefined : searchFromPrice,
          searchToBasePrice: searchB2BSupport ? undefined : searchToPrice,
          searchToBasePriceB2B: searchB2BSupport ? searchToPrice : undefined,
          searchFromBasePriceB2B: searchB2BSupport
            ? searchFromPrice
            : undefined,
          searchIsPickupMode: searchPriceType === 'Pickup',
          vendorDto:
            vendorId !== '0'
              ? new AgentDto({
                id: vendorId,
                vendorBranchDtos:
                  vendorBranchDtoArray.length > 0
                    ? vendorBranchDtoArray
                    : undefined,
              })
              : undefined,
          published:
            statusProduct !== 'disablePublishedFilter'
              ? statusProduct
              : undefined,
          disablePublishedFilter: statusProduct === 'disablePublishedFilter',
          name,
          vendorCode: code,
          marked,
          siteCode,
          searchB2BSupport: searchB2BSupport || undefined,
        }),
      )
      .pageIndex(currentPage - 1)
      .pageSize(pageSize)
      .includes([
        'vendorDto',
        'vendorBranches',
        'productCategoryDtos',
        'imageDtos',
        'rateAverageDto',
        'vendorBranchProductPriceDtos',
      ])
      .buildJson();

    return jsonList;
  };

  /**
   * for search product
   * @param value
   */
  async submitSearch() {
    this.setState({ currentPage: 1 }, () => {
      this.props.actions.productListRequest(this.prepareContainer());
    });
  }

  handleOkModal = () => {
    this.setState({
      visibleModal: false,
    });
    this.props.actions.showLoading();
    history.push(`/product/create?vendorId=${this.state.selectedvendorId}`);
  };

  handleCancelModal = () => {
    this.setState({
      visibleModal: false,
    });
  };

  showModalSelectVendor() {
    this.setState({
      visibleModal: true,
    });
    this.props.actions.singleProductSuccess(null);
  }

  handleChange = (value, id) => {
    const json = BaseCRUDDtoBuilder()
      .dto(new RateDto({ value, rateType: 'Product', entityId: id }))
      .build();
    const postData = {
      data: json,
      listDto: this.prepareContainer(),
    };

    this.props.actions.ratePostRequest(postData);
  };

  handleCancelImageModal = () => {
    this.setState({ visibleImageModal: false });
  };

  async showImageModal(record) {
    const { loginData } = this.props;
    const dataImage = [];
    const obj = {
      small: `${record.imgUrl}`,
      large: `${record.imgUrl}`,
      key: record.imgUrl,
      // srcSet: srcSetArray, TODO: It was commented on the error
    };
    dataImage.push(obj);
    // dataImage.push(obj);
    this.setState({ visibleImageModal: true, dataImage });

    /**
     * get product specification attribute with productId
     * @type
     */
    const jsonSpecification = new BaseGetDtoBuilder()
      .dto(
        new ProductSpecificationAttributeDto({
          productDto: new ProductDto({ id: record.key }),
        }),
      )
      .disabledCount(false)
      .buildJson();
    const response = await getProductSpecificationAttributeApi(
      loginData.token,
      getDtoQueryString(jsonSpecification),
    );
    const array = [];
    if (response.status === 200)
      response.data.items.map(item => {
        let specName = '';
        if (
          item.specificationAttributeDto.specificationAttributeType === 'Color'
        ) {
          specName = `${item.specificationAttributeDto.name} ${
            item.colorOptionDto.name
            } ${item.customValueDescription} ${item.customValue ? ' عدد' : ''}`;
        } else if (
          item.specificationAttributeDto.specificationAttributeType ===
          'CheckBox'
        ) {
          specName = `${item.specificationAttributeDto.name} ${
            item.customValue === 'true' ? ' دارد' : 'ندارد'
            }`;
        }

        array.push(<div className="col-sm-6">{specName}</div>);
        return null;
      });
    const productPrice = [];

    record.priceDtos.map((item, index) => {
      let deliveryPriceAfterDiscount = 0;
      let pickupPriceAfterDiscount = 0;

      const deliveryPrice = item.stringDeliveryBasePrice;
      const pickupPrice = item.stringPickupBasePrice;
      if (item.deliveryPriceAfterDiscount < item.deliveryBasePrice) {
        deliveryPriceAfterDiscount = item.stringDeliveryPriceAfterDiscount;
      }
      if (item.pickupPriceAfterDiscount < item.pickupBasePrice) {
        pickupPriceAfterDiscount = item.stringPickupPriceAfterDiscount;
      }

      productPrice.push(
        <div className="row">
          <div className={cs('col-sm-6', s.productPrice)}>
            <label>قیمت Delivery:</label>
            {deliveryPriceAfterDiscount !== 0 ? (
              <label>
                <del>{deliveryPrice}</del>
                {deliveryPriceAfterDiscount}
              </label>
            ) : (
              <label>{deliveryPrice}</label>
            )}
          </div>
          <div className={cs('col-sm-6', s.productPrice)}>
            <label>قیمت pickup:</label>
            {pickupPriceAfterDiscount !== 0 ? (
              <label>
                {pickupPriceAfterDiscount}
                <del>{pickupPrice}</del>
              </label>
            ) : (
              <label>{pickupPrice}</label>
            )}
          </div>
        </div>,
      );

      return null;
    });

    this.setState({
      specificationAttributeNameArray: array,
      priceProductArray: productPrice,
    });
  }

  showCategoryModal = () => {
    this.setState({
      visibleCategoryModal: true,
      categoryIds: this.state.categorySelectedIds,
    });
  };

  handleCancelCategoryModal = () => {
    this.setState({ visibleCategoryModal: false });
  };

  handleOkCategoryModal = () => {
    const { categoryIds, categoryArray } = this.state;
    const ids = [];
    categoryIds.map(item => ids.push(item.split('*')[0]));
    this.setState({
      visibleCategoryModal: false,
      categorySelectedArray: categoryArray,
      categorySelectedIds: ids,
    });
  };

  traverseTree(node) {
    if (node.children && node.children.length > 0) {
      let tempItem = {};
      node.children.map(item => {
        tempItem = item;
        tempItem.link = `/category/edit/${item.id}`;
        return this.traverseTree(tempItem);
      });
    }
  }

  onCheckCategory = (checkedKeys, info) => {
    const categories = [];
    const categoryIds = [];
    info.checkedNodes.map(item => {
      if (item.key.includes('*'))
        categories.push(
          <Option key={item.key.split('*')[0]}>
            {item.key.split('*')[1]}
          </Option>,
        );
    });

    checkedKeys.map(item => {
      if (item.includes('*')) categoryIds.push(item);
    });

    this.setState({ categoryIds, categoryArray: categories });
  };

  changeStatus = (value, record) => {
    const crud = new BaseCRUDDtoBuilder()
      .dto(
        new ProductDto({
          id: record.key,
          published: value,
        }),
      )
      .justPublished(true)
      .build();

    const data = {
      data: crud,
      listDto: this.prepareContainer(),
      status: 'list',
    };

    this.props.actions.productPutRequest(data);
  };

  render() {
    const {
      productLoading,
      products,
      categoryChildren,
      productTypes,
      productCount,
      vendors,
      filterPermission,
      vendorBranches,
      categories,
    } = this.props;

    const {
      vendorId,
      productTypeId,
      specificationAttributeNameArray,
      name,
      code,
      statusProduct,
      selectedvendorId,
      currentPage,
      searchPriceType,
      searchFromPrice,
      searchToPrice,
      visibleImageModal,
      dataImage,
      vendorBranchIds,
      // value,
      visibleCategoryModal,
      categorySelectedArray,
      categorySelectedIds,
      categoryIds,
      siteCode,
      marked,
      priceProductArray,
      searchB2BSupport,
    } = this.state;

    const priceModel = [
      {
        value: 'Pickup',
        name: 'PickUp',
      },
      {
        value: 'Delivery',
        name: 'Delivery',
      },
      {
        value: 'B2B',
        name: 'B2B',
      },
    ];

    const columns = [
      {
        title: (
          <CPPermission>
            <div name="product-list-code">{SITE_PRODUCT_ID}</div>
          </CPPermission>
        ),
        dataIndex: 'key',
        key: 'key',
        width: 50,
        render: (text, record) => (
          <CPPermission>
            <div name="product-list-code">{record.key}</div>
          </CPPermission>
        ),
      },
      {
        title: '',
        dataIndex: 'url',
        width: 50,
        render: (text, record) => (
          <div onClick={() => this.showImageModal(record)}>
            <CPAvatar src={record.imgUrl} />
          </div>
        ),
      },
      { title: PRODUCT_LIST_NAME, dataIndex: 'name', key: 'name', width: 200 },
      {
        title: PRODUCT_LIST_CODE_VENDOR,
        dataIndex: 'vendorCode',
        key: 'vendorCode',
        width: 50,
      },
      {
        title: (
          <CPPermission>
            <div name="product-list-deepCode">{FILE_PATH}</div>
          </CPPermission>
        ),
        dataIndex: 'deepCode',
        key: 'deepCode',
        width: 70,
        render: (text, record) => (
          <CPPermission>
            <div name="product-list-deepCode">{record.deepCode}</div>
          </CPPermission>
        ),
      },
      {
        title: PRODUCT_LIST_CATEGORY,
        dataIndex: 'category',
        key: 'category',
        width: 250,
      },
      { title: AGENT_NAME, dataIndex: 'vendor', key: 'vendor', width: 150 },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 50,
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              defaultChecked={record.active}
              onChange={value => this.changeStatus(value, record)}
            />
          </div>
        ),
      },
      // {
      //   title: RATE,
      //   dataIndex: 'rateAverage',
      //   render: (text, record) => <CPRateValue value={record.rateAverage} />,
      // },
      // {
      //   title: RATING,
      //   dataIndex: 'value',
      //   render: (text, record) => (
      //     <div>
      //       <CPRate
      //         allowHalf
      //         value={record.rateAverage}
      //         // onChange={value => this.handleChange(value, record.key)}
      //         disabled
      //       />
      //     </div>
      //   ),
      // },
      {
        title: '',
        dataIndex: 'Agent_Delete',
        width: 50,
        render: (text, record) => (
          <CPPermission>
            <div name="product-list-delete">
              <CPPopConfirm
                title={ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT}
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() => this.onDelete(record.key)}
              >
                <CPButton className="delete_action">
                  <i className="cp-trash" />
                </CPButton>
              </CPPopConfirm>
            </div>
          </CPPermission>
        ),
      },
      {
        title: '',
        dataIndex: 'Agent_Edit',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to={`/product/edit/${record.key}`}
              onClick={() => {
                this.props.actions.showLoading();
              }}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];
    const arrayIndexColumn = [];
    Object.keys(columns).map(column => {
      Object.keys(filterPermission).map(filter => {
        if (
          filterPermission[filter].resourceValue === columns[column].dataIndex
        ) {
          const index = columns.indexOf(columns[column]);
          arrayIndexColumn.push(index);
        }
      });
    });
    arrayIndexColumn.map(item => delete columns[item]);

    const productArray = [];
    const vendorArray = [];
    const vendorBranchArray = [];
    const productTypeArray = [];
    const CategoryChildrenArray = [];
    treeModel.items = [];
    categories.map(item => {
      const treeNodeModel = {
        id: item.id,
        name: `${item.code}-${item.name}`,
        children: item.children,
      };

      return treeModel.items.push(treeNodeModel);
    });
    treeModel.allowLastLeafSelect = true;

    categories.map(item => this.traverseTree(item));

    /**
     * map products for product list
     */
    products.map(item => {
      const vendorBranchList = [];
      let priceDtos = [];
      item.vendorBranchProductDtos.map(vendorBranch =>
        vendorBranchList.push(vendorBranch.vendorBranchDto.name),
      );

      if (
        item.vendorBranchProductPriceDtos &&
        item.vendorBranchProductPriceDtos.length > 0
      ) {
        priceDtos = item.vendorBranchProductPriceDtos;
      }

      const product = {
        key: item.id,
        vendorCode: item.vendorCode,
        name: item.name,
        vendor: item.vendorDto ? item.vendorDto.name : '',
        vendorBranches: vendorBranchList.join(),
        createdOn: item.persianCreatedOnUtc,
        updatedOn: item.persianUpdatedOnUtc,
        active: item.published,
        category: item.productCategoryDtos
          ? item.productCategoryDtos[0].categoryDto.description
          : '',
        deepCode: `${item.vendorBranchProductDtos &&
        item.vendorBranchProductDtos[0] &&
        item.vendorBranchProductDtos[0].vendorBranchDto &&
        item.vendorBranchProductDtos[0].vendorBranchDto.vendorDto &&
        item.vendorBranchProductDtos[0].vendorBranchDto.vendorDto.code}-${
          item.productCategoryDtos
            ? item.productCategoryDtos[0].categoryDto.deepCode
            : ''
          }`,
        rateAverage: item.rateAverage,
        imgUrl:
          item.imageDtos && item.imageDtos.length > 0
            ? `${item.imageDtos[0].url}`
            : ` `,
        priceDtos,
      };
      return productArray.push(product);
    });

    /**
     * map vendors for comboBox Datasource
     */
    vendors.map(item =>
      new OptionBuilder(item.id, item.name, vendorArray).buildAndPush(),
    );

    /**
     * map vendorBranches for comboBox Datasource
     */
    if (vendorId !== '0')
      vendorBranches.map(item =>
        vendorBranchArray.push(<Option key={item.id}>{item.name}</Option>),
      );

    /**
     * map child Category for comboBox Datasource
     */
    categoryChildren.map(item =>
      CategoryChildrenArray.push(
        <Option key={item.id}>{item.description}</Option>,
      ),
    );
    /**
     * map product Types for comboBox Datasource
     */
    productTypes.map(item =>
      productTypeArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.showModalSelectVendor}>
        <div className={s.mainContent}>
          <h3>{PRODUCT_LIST}</h3>
          <CPList
            data={productArray}
            count={productCount}
            columns={columns}
            loading={productLoading}
            onAddClick={this.showModalSelectVendor}
            pagination={false}
            showTopAdd={productCount > 5}
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="product-search-name"
                      label={PRODUCT_LIST_NAME}
                      value={name}
                      onChange={value => {
                        this.handelChangeInput('name', value.target.value);
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      label={PRODUCT_LIST_CODE_VENDOR}
                      value={code}
                      onChange={value => {
                        this.handelChangeInput('code', value.target.value);
                      }}
                    />
                  </div>
                  <CPPermission>
                    <div
                      className="col-lg-3 col-md-6 col-sm-12"
                      name="product-search-vendor"
                    >
                      <label name="label">{AGENT_NAME}</label>
                      <CPSelect
                        showSearch
                        value={vendorId}
                        onChange={value => {
                          this.handelChangeInput('vendorId', value);
                        }}
                      >
                        <Option key={0}>{ALL_VENDORS}</Option>
                        {vendorArray}
                      </CPSelect>
                    </div>
                  </CPPermission>
                  <CPPermission>
                    <div
                      className="col-lg-3 col-md-6 col-sm-12"
                      name="product-search-vendor"
                    >
                      <label name="label">{VENDOR_BRANCH_NAME}</label>
                      {/* <CPSelect
                        showSearch
                        value={vendorBranchIds}
                        onChange={value => {
                          this.handelChangeInput('vendorBranchIds', value);
                        }}
                      >
                        <Option key={0}>{ALL_VENDOR_BRANCH}</Option>
                        {vendorBranchArray}
                      </CPSelect> */}
                      <CPMultiSelect
                        value={vendorBranchIds}
                        onChange={value => {
                          this.handelChangeInput('vendorBranchIds', value);
                        }}
                      >
                        {vendorBranchArray}
                      </CPMultiSelect>
                    </div>
                  </CPPermission>

                  <CPPermission>
                    <div
                      className="col-lg-3 col-md-6 col-sm-12"
                      name="product-search-productType"
                    >
                      <label name="label">{PRODUCT_PRODUCT_TYPE}</label>
                      <CPSelect
                        value={productTypeId}
                        onChange={value => {
                          this.handelChangeInput('productTypeId', value);
                        }}
                      >
                        <Option key={0}>{ALL_TYPES}</Option>
                        {productTypeArray}
                      </CPSelect>
                    </div>
                  </CPPermission>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{SEARCH_PRICE}</label>
                    <div className={s.searchPrice}>
                      <CPRadio
                        model={priceModel}
                        size="small"
                        onChange={value =>
                          this.handelChangeInput(
                            'searchPriceType',
                            value.target.value,
                          )
                        }
                        defaultValue={searchPriceType}
                      />
                    </div>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{SEARCH_FROM_PRICE}</label>
                    <CPInputNumber
                      value={searchFromPrice}
                      // defaultValue={deliveryPrice}
                      onChange={value =>
                        this.handelChangeInput('searchFromPrice', value)
                      }
                      format="Currency"
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{SEARCH_TO_PRICE}</label>
                    <CPInputNumber
                      value={searchToPrice}
                      // defaultValue={deliveryPrice}
                      onChange={value =>
                        this.handelChangeInput('searchToPrice', value)
                      }
                      format="Currency"
                    />
                  </div>
                  <CPPermission>
                    <div
                      className="col-lg-3 col-md-6 col-sm-12"
                      name="product-search-code"
                    >
                      <CPInput
                        label={SITE_PRODUCT_ID}
                        value={siteCode}
                        onChange={value => {
                          this.handelChangeInput(
                            'siteCode',
                            value.target.value,
                          );
                        }}
                      />
                    </div>
                  </CPPermission>
                  <CPPermission>
                    <div
                      className="col-lg-9 col-md-6 col-sm-12"
                      name="product-search-category"
                    >
                      <label name="label">{PRODUCT_LIST_CATEGORY}</label>
                      <div className="col-sm-12 selectCategory">
                        <CPMultiSelect
                          placeholder="انتخاب گروه محصول"
                          onChange={this.categoryChange}
                          value={categorySelectedIds}
                        >
                          {categorySelectedArray}
                        </CPMultiSelect>
                        <CPButton onClick={this.showCategoryModal}>
                          {SELECT}
                        </CPButton>
                      </div>
                    </div>
                  </CPPermission>
                  <CPPermission>
                    <div
                      className="col-lg-3 col-md-6 col-sm-12"
                      name="product-search-active"
                    >
                      <div className="align-margin">
                        <label>{STATUS}</label>
                        <CPSelect
                          value={statusProduct}
                          onChange={value => {
                            this.handelChangeInput('statusProduct', value);
                          }}
                        >
                          <Option
                            value="disablePublishedFilter"
                            key="disablePublishedFilter"
                          >
                            {ALL}
                          </Option>
                          <Option value="true" key="true">
                            {ACTIVE}
                          </Option>
                          <Option value="false" key="false">
                            {DEACTIVATE}
                          </Option>
                        </CPSelect>
                      </div>
                    </div>
                  </CPPermission>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <div className="align-center">
                      <CPSwitch
                        defaultChecked={marked}
                        checkedChildren={ONLY_MARKED}
                        unCheckedChildren={ONLY_MARKED}
                        onChange={value => {
                          this.handelChangeInput('marked', value);
                        }}
                      />
                      <CPSwitch
                        defaultChecked={searchB2BSupport}
                        checkedChildren="B2BSupport"
                        unCheckedChildren="B2BSupport"
                        onChange={value => {
                          this.handelChangeInput('searchB2BSupport', value);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className={s.field}>
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
          <CPPagination
            total={productCount}
            current={currentPage}
            onChange={this.onChangePagination}
            onShowSizeChange={this.onShowSizeChange}
          />
          <CPModal
            visible={this.state.visibleModal}
            handleOk={this.handleOkModal}
            textSave={CONTINUE}
            handleCancel={this.handleCancelModal}
            title={SELECT_VENDOR}
          >
            <div className="row">
              <div className="col-sm-12">
                <label>{AGENT_NAME}</label>
                <CPSelect
                  onChange={value => {
                    this.handelChangeInput('selectedvendorId', value);
                  }}
                  value={selectedvendorId}
                  autoFocus
                >
                  {/* <Option key={0}>{VENDORSELECTED}</Option> */}
                  {vendorArray}
                </CPSelect>
              </div>
            </div>
          </CPModal>
          <CPModal
            visible={visibleImageModal}
            handleCancel={this.handleCancelImageModal}
            showFooter={false}
          >
            <CPCarousel data={dataImage} width={700} height={700} />
            <div className="row">{specificationAttributeNameArray}</div>
            {priceProductArray}
          </CPModal>
          <CPModal
            visible={visibleCategoryModal}
            handleCancel={this.handleCancelCategoryModal}
            handleOk={this.handleOkCategoryModal}
            className="max_modal"
            title="انتخاب دسته بندی"
            textSave={CONFIRMATION}
          >
            <CPTree
              model={treeModel}
              onCheck={this.onCheckCategory}
              checkable
              checkedKeys={categoryIds}
              defaultExpandAll
            />
          </CPModal>
        </div>
      </HotKeys>
    );
  }
}
const mapStateToProps = state => ({
  products: state.productList.data ? state.productList.data.items : [],
  productCount: state.productList.data ? state.productList.data.count : 0,
  categories: state.categoryList.data ? state.categoryList.data.items : [],
  categoryChildren: state.childCategoryList.data
    ? state.childCategoryList.data.items
    : [],
  productTypes: state.productTypesList.data
    ? state.productTypesList.data.items
    : [],
  vendors: state.vendorList.data ? state.vendorList.data.items : [],
  productLoading: state.productList.loading,
  rateValue: state.ratePost.data ? state.ratePost.data.items : [],
  filterPermission: state.permission.data
    ? state.permission.data.items[0].flatermission
    : [],
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
  loginData: state.login.data,
  flatermission:
    state.permission.data && state.permission.data.items.length > 0
      ? state.permission.data.items[0].flatermission
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      ratePostRequest: ratePostActions.ratePostRequest,
      productListRequest: productListActions.productListRequest,
      singleProductSuccess: singleProductActions.singleProductSuccess,
      productDeleteRequest: productDeleteActions.productDeleteRequest,
      productPutRequest: productPutActions.productPutRequest,
      hideLoading,
      showLoading,
      vendorBranchListRequest: vendorBranchListActions.vendorBranchListRequest,
      productSpecificationAttributeListRequest:
      productSpecificationAttributeListActions.productSpecificationAttributeListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(productList));
