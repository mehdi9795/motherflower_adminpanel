import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import ProductList from './ProductList';
import productListActions from '../../../../redux/catalog/actionReducer/product/ProductList';
import productTypesListActions from '../../../../redux/catalog/actionReducer/productType/List';
import childCategoryListActions from '../../../../redux/catalog/actionReducer/category/ChildCategoryList';
import categoryListActions from '../../../../redux/catalog/actionReducer/category/CategoryList';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import {
  ProductDto,
  CategoryDto,
  ProductTagDto,
  TagDto,
} from '../../../../dtos/catalogDtos';
import { PAGE_TITLE_PRODUCT_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all products for first time with default where clause
   */

  const json = new BaseGetDtoBuilder()
    .dto(
      new ProductDto({
        disablePublishedFilter: true,
        productTagDtos: [
          new ProductTagDto({ tagDto: new TagDto({ published: true }) }),
        ],
      }),
    )
    .pageSize(20)
    .pageIndex(0)
    .includes([
      'vendorDto',
      'vendorBranches',
      'productCategoryDtos',
      'imageDtos',
      'rateAverageDto',
      'vendorBranchProductPriceDtos',
    ])
    .buildJson();

  store.dispatch(productListActions.productListRequest(json));

  /**
   * get all product types for first time
   */
  store.dispatch(
    productTypesListActions.productTypesListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  /**
   * get all category for first time
   */
  const jsonCategoryDto = new BaseGetDtoBuilder()
    .dto(new CategoryDto({ makeTree: true, published: true }))
    .buildJson();
  store.dispatch(categoryListActions.categoryListRequest(jsonCategoryDto));

  /**
   * get all child categories for first time
   */
  const jsonChildCategoryDto = new BaseGetDtoBuilder()
    .dto(new CategoryDto({ justChild: true, published: true }))
    .buildJson();
  store.dispatch(
    childCategoryListActions.childCategoryListRequest(jsonChildCategoryDto),
  );

  /**
   * get all vendor for first time
   */
  store.dispatch(
    vendorListActions.vendorListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  return {
    chunks: ['productList'],
    title: `${PAGE_TITLE_PRODUCT_LIST}`,
    component: (
      <Layout>
        <ProductList />
      </Layout>
    ),
  };
}

export default action;
