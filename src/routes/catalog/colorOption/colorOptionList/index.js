import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import ColorOptionList from './ColorOptionList';
import colorOptionListActions from '../../../../redux/catalog/actionReducer/colorOption/List';
import { PAGE_TITLE_COLOR_OPTION_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  store.dispatch(
    colorOptionListActions.colorOptionListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    ),
  );

  return {
    chunks: ['colorOptionList'],
    title: `${PAGE_TITLE_COLOR_OPTION_LIST}`,
    component: (
      <Layout>
        <ColorOptionList />
      </Layout>
    ),
  };
}

export default action;
