import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { hideLoading } from 'react-redux-loading-bar';
import HotKeys from 'react-hot-keys';
import { bindActionCreators } from 'redux';
import CPList from '../../../../components/CP/CPList/CPList';
import s from './ColorOptionList.css';
import {
  YES,
  NO,
  NAME,
  TAG_LIST,
  FEATURE,
  ARE_YOU_SURE_WANT_TO_DELETE_COLOR,
  COLOR_CODE,
} from '../../../../Resources/Localization';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import CPModal from '../../../../components/CP/CPModal';
import { ColorOptionDto } from '../../../../dtos/catalogDtos';
import AlertHideActions from '../../../../redux/shared/actionReducer/alert/alert';
import CPAlert from '../../../../components/CP/CPAlert';
import colorOptionDeleteActions from '../../../../redux/catalog/actionReducer/colorOption/Delete';
import colorOptionPostActions from '../../../../redux/catalog/actionReducer/colorOption/Post';
import colorOptionPutActions from '../../../../redux/catalog/actionReducer/colorOption/Put';
import colorOptionListActions from '../../../../redux/catalog/actionReducer/colorOption/List';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import { pressEnterAndCallApplySearch } from '../../../../utils/helper';

class specificationAttributeList extends React.Component {
  static propTypes = {
    colorOptions: PropTypes.arrayOf(PropTypes.object),
    colorOptionCount: PropTypes.number,
    colorOptionLoading: PropTypes.bool,
    errorMessage: PropTypes.string,
    errorAlert: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    colorOptions: [],
    colorOptionCount: 0,
    errorMessage: '',
    errorAlert: false,
    colorOptionLoading: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      searchName: '',
      searchAttribute: '',
      name: ``,
      attribute: '000000',
      visibleModal: false,
      id: 0,
    };
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const data = {
      id,
      listDto: new BaseGetDtoBuilder().all(true).buildJson(),
    };

    this.props.actions.colorOptionDeleteRequest(data);
  };

  onCloseErrorMessage = () => {
    this.props.actions.AlertHideRequest();
  };

  handleOkModal = () => {
    const { name, attribute, id } = this.state;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(new ColorOptionDto({ name, attribute, id }))
      .build();

    const data = {
      data: jsonCrud,
      listDto: new BaseGetDtoBuilder().all(true).buildJson(),
    };
    if (id === 0) this.props.actions.colorOptionPostRequest(data);
    else this.props.actions.colorOptionPutRequest(data);

    this.setState({ visibleModal: false });
  };

  handleCancelModal = () => {
    this.setState({ visibleModal: false });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  submitSearch = () => {
    const { searchName } = this.state;

    this.props.actions.colorOptionListRequest(
      new BaseGetDtoBuilder()
        .dto(new ColorOptionDto({ name: searchName }))
        .buildJson(),
    );
  };

  showEditModal = (e, record) => {
    e.preventDefault();

    this.setState({
      visibleModal: true,
      name: record ? record.name : '',
      attribute: record.attribute,
      id: record.key,
    });
  };

  showModal = () => {
    this.setState({
      visibleModal: true,
      name: '',
      attribute: '',
      id: 0,
    });
  };

  render() {
    const {
      colorOptionLoading,
      colorOptions,
      colorOptionCount,
      errorAlert,
      errorMessage,
    } = this.props;

    const { searchName, name, attribute, visibleModal } = this.state;

    const colorOptionArray = [];
    const columns = [
      {
        title: NAME,
        dataIndex: 'name',
      },
      {
        title: COLOR_CODE,
        dataIndex: 'attribute',
        render: (text, record) => (
          <div className="colorCode">
            <label
              className={s.colorSwitch}
              style={{ background: record.attribute }}
            />
            <label>{record.attribute}</label>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Delete',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_COLOR}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Edit',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to="/"
              onClick={e => this.showEditModal(e, record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    colorOptions.map(item => {
      const obj = {
        name: item.name,
        attribute: item.attribute,
        key: item.id,
      };
      colorOptionArray.push(obj);
      return null;
    });

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.showModal}>
        <div className={s.mainContent}>
          <h3>{TAG_LIST}</h3>
          {errorAlert && (
            <CPAlert
              showIcon
              closable
              onClose={this.onCloseErrorMessage}
              type="error"
              message="خطا"
              description={errorMessage}
            />
          )}
          <CPList
            data={colorOptionArray}
            count={colorOptionCount}
            columns={columns}
            loading={colorOptionLoading}
            onAddClick={this.showModal}
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      label={NAME}
                      value={searchName}
                      onChange={value => {
                        this.handelChangeInput(
                          'searchName',
                          value.target.value,
                        );
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12" />
                </div>
              </div>
              <div className={s.field}>
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>

          <CPModal
            visible={visibleModal}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
            title={FEATURE}
          >
            <div className="row">
              <div className="col-sm-12">
                <CPInput
                  label={NAME}
                  value={name}
                  onChange={value => {
                    this.handelChangeInput('name', value.target.value);
                  }}
                />
              </div>
              <div className="col-sm-12">
                <CPInput
                  label={COLOR_CODE}
                  value={attribute}
                  onChange={value => {
                    this.handelChangeInput('attribute', value.target.value);
                  }}
                />
              </div>
            </div>
          </CPModal>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  colorOptions: state.colorOptionList.data.items,
  colorOptionCount: state.colorOptionList.data.count,
  colorOptionLoading: state.colorOptionList.loading,
  errorAlert: state.editFormAlert.loading,
  errorMessage: state.editFormAlert.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      colorOptionDeleteRequest:
        colorOptionDeleteActions.colorOptionDeleteRequest,
      colorOptionPostRequest: colorOptionPostActions.colorOptionPostRequest,
      colorOptionPutRequest: colorOptionPutActions.colorOptionPutRequest,
      colorOptionListRequest: colorOptionListActions.colorOptionListRequest,
      AlertHideRequest: AlertHideActions.editFormAlertHideRequest,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(specificationAttributeList));
