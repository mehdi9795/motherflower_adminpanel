import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import EditCard from '../../../../components/CP/EditCard';
import CategoryForm from '../../../../components/Catalog/Category/CategoryForm/CategoryForm';
import s from './CategoryCreate.css';
import { CATEGORY_CREATE } from '../../../../Resources/Localization';

class CategoryCreate extends React.Component {
  static propTypes = {
    backUrl: PropTypes.string.isRequired,
  };

  render() {
    const { backUrl } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard title={CATEGORY_CREATE} backUrl={backUrl}>
          <CategoryForm />
        </EditCard>
      </div>
    );
  }
}

export default withStyles(s)(CategoryCreate);
