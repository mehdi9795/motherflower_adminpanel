import React from 'react';
import CategoryCreate from './CategoryCreate';
import Layout from '../../../../components/Template/Layout/Layout';
import singleCategoryInitActions from '../../../../redux/catalog/actionReducer/category/SingleCategoryInit';
import categoryListActions from '../../../../redux/catalog/actionReducer/category/CategoryList';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import { getDtoQueryString } from '../../../../utils/helper';
import { getCategoryApi } from '../../../../services/catalogApi';
import { PAGE_TITLE_CATEGORY_CREATE } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query }) {
  const { backUrl = '/category/list' } = query;

  try {
    store.dispatch(singleCategoryInitActions.singleCategoryInitRequest());

    /**
     * get all category for first time
     */
    const json = new BaseGetDtoBuilder()
      .dto(new CategoryDto({ makeTree: true, published: true }))
      .buildJson();

    const categories = await getCategoryApi(
      store.getState().login.data.token,
      getDtoQueryString(json),
    );

    if (categories.status === 200) {
      store.dispatch(singleCategoryInitActions.singleCategoryInitSuccess(null));
      store.dispatch(categoryListActions.categoryListSuccess(categories.data));
    }
  } catch (error) {
    store.dispatch(singleCategoryInitActions.singleCategoryInitFailure());
    store.dispatch(categoryListActions.categoryListFailure());
  }

  return {
    chunks: ['categoryCreate'],
    title: `${PAGE_TITLE_CATEGORY_CREATE}`,
    component: (
      <Layout>
        <CategoryCreate backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
