import React from 'react';
import CategoryList from './CategoryList';
import Layout from '../../../../components/Template/Layout/Layout';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import categoryListAction from '../../../../redux/catalog/actionReducer/category/CategoryList';
import { PAGE_TITLE_CATEGORY_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all category for select parent
   */
  const jsonCategoriesDto = new BaseGetDtoBuilder()
    .dto(new CategoryDto({ makeTree: true, published: true }))
    .buildJson();
  store.dispatch(categoryListAction.categoryListRequest(jsonCategoriesDto));

  return {
    chunks: ['categoryList'],
    title: `${PAGE_TITLE_CATEGORY_LIST}`,
    component: (
      <Layout>
        <CategoryList />
      </Layout>
    ),
  };
}

export default action;
