import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import HotKeys from 'react-hot-keys';
import PropTypes from 'prop-types';
import s from './CategoryList.css';
import { treeModel } from '../../../../utils/model';
import CPTree from '../../../../components/CP/CPTree';
import CPCard from '../../../../components/CP/CPCard/CPCard';
import CPButton from '../../../../components/CP/CPButton';
import { CATEGORY_LIST } from '../../../../Resources/Localization';
import history from '../../../../history';
import singleCategoryActions from '../../../../redux/catalog/actionReducer/category/SingleCategory';
import { getCookie } from '../../../../utils';
import { postSyncPreparingProductTime } from '../../../../services/catalogApi';
import { message } from 'antd';

class CategoryList extends React.Component {
  static propTypes = {
    categories: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    categories: [],
  };

  constructor(props) {
    super(props);
    this.UpdateTimeProduct = this.UpdateTimeProduct.bind(this);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onSelectChild = (selectedKeys, info) => {
    if (info.node.props.isLeaf) {
      this.setState({
        selectedChild: selectedKeys[0],
      });
    }
  };

  // <editor-fold dsc="CPTree">
  traverseTree(node) {
    if (node.children && node.children.length > 0) {
      let tempItem = {};
      node.children.map(item => {
        tempItem = item;
        tempItem.link = `/category/edit/${item.id}`;
        return this.traverseTree(tempItem);
      });
    }
  }
  // </editor-fold>

  /**
   * category tree add button click handler
   */
  categoryAddClick = () => {
    this.props.actions.showLoading();
    history.push('/category/create');
    this.props.actions.singleCategorySuccess(null);
  };

  async UpdateTimeProduct() {
    const token = getCookie('token');
    const response = await postSyncPreparingProductTime(token);
    if (response.status === 200 && response.data.isSuccess)
      message.success('با موفقیت بروزرسانی شد.', 5);
  }

  render() {
    const { categories } = this.props;
    treeModel.items = [];
    if (categories)
      categories.map(item => {
        const treeNodeModel = {
          id: item.id,
          name: `${item.code}-${item.name}`,
          link: `/category/edit/${item.id}`,
          children: item.children,
        };

        return treeModel.items.push(treeNodeModel);
      });
    treeModel.allowLink = true;
    if (categories) categories.map(item => this.traverseTree(item));

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.categoryAddClick}>
        <div className={s.mainContent}>
          <h3>{CATEGORY_LIST}</h3>
          <CPCard>
            <CPTree
              model={treeModel}
              onSelect={this.onSelectChild}
              onCheck={null}
              defaultExpandAll
            />
            <CPButton
              onClick={this.categoryAddClick}
              type="primary"
              className={s.createRecord}
              shape="circle"
            >
              <i className="cp-add" />
            </CPButton>
            <CPButton
              onClick={this.UpdateTimeProduct}
              type="primary"
              className={s.updateTime}
            >
              بروزرسانی مدت زمان محصول
            </CPButton>
          </CPCard>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.categoryList.data.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      singleCategorySuccess: singleCategoryActions.singleCategorySuccess,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CategoryList));
