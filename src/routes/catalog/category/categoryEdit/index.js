import React from 'react';
import CategoryEdit from './CategoryEdit';
import Layout from '../../../../components/Template/Layout/Layout';
import categoryListActions from '../../../../redux/catalog/actionReducer/category/CategoryList';
import singleCategoryInitActions from '../../../../redux/catalog/actionReducer/category/SingleCategoryInit';
import singleCategoryActions from '../../../../redux/catalog/actionReducer/category/SingleCategory';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import { getCategoryApi } from '../../../../services/catalogApi';
import { getDtoQueryString } from '../../../../utils/helper';
import { PAGE_TITLE_CATEGORY_EDIT } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store, query, params }) {
  const { backUrl = '/category/list' } = query;

  try {
    store.dispatch(singleCategoryInitActions.singleCategoryInitRequest());

    /**
     * get single category
     */
    const jsonSingleCategoryDto = new BaseGetDtoBuilder()
      .dto(new CategoryDto({ published: true, id: params.id }))
      .includes(['imageDtos'])
      .buildJson();

    const singleCategory = await getCategoryApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonSingleCategoryDto),
    );

    /**
     * get all category for select parent
     */
    const jsonCategoriesDto = new BaseGetDtoBuilder()
      .dto(new CategoryDto({ makeTree: true, published: true }))
      .notExpectedIds([params.id])
      .buildJson();

    const categories = await getCategoryApi(
      store.getState().login.data.token,
      getDtoQueryString(jsonCategoriesDto),
    );

    if (singleCategory && categories) {
      store.dispatch(singleCategoryInitActions.singleCategoryInitSuccess(null));
      store.dispatch(
        singleCategoryActions.singleCategorySuccess(singleCategory.data),
      );
      store.dispatch(categoryListActions.categoryListSuccess(categories.data));
    }
  } catch (error) {
    store.dispatch(singleCategoryInitActions.singleCategoryInitFailure());
  }

  return {
    chunks: ['categoryEdit'],
    title: `${PAGE_TITLE_CATEGORY_EDIT}`,
    component: (
      <Layout>
        <CategoryEdit backUrl={backUrl} />
      </Layout>
    ),
  };
}

export default action;
