import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditCard from '../../../../components/CP/EditCard';
import s from './CategoryEdit.css';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_CATEGORY,
  CATEGORY_EDIT,
} from '../../../../Resources/Localization';
import CategoryForm from '../../../../components/Catalog/Category/CategoryForm/CategoryForm';
import history from '../../../../history';
import categoryDeleteActions from '../../../../redux/catalog/actionReducer/category/CategoryDelete';

class CategoryEdit extends React.Component {
  static propTypes = {
    category: PropTypes.objectOf(PropTypes.any).isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    backUrl: PropTypes.string.isRequired,
  };

  static defaultProps = {
    backUrl: '/category/list',
  };

  delete = () => {
    const { category } = this.props;

    this.props.actions.categoryDeleteRequest(category.id);
  };

  redirect = () => {
    history.push(`/category/list`);
  };

  render() {
    const { backUrl, category } = this.props;
    return (
      <div className={s.mainContent}>
        <EditCard
          title={`${CATEGORY_EDIT} - ${category && category.name}`}
          isEdit={!!(category && category.id)}
          backUrl={backUrl}
          onDelete={this.delete}
          deleteTitle={ARE_YOU_SURE_WANT_TO_DELETE_THE_CATEGORY}
        >
          <CategoryForm category={category} />
        </EditCard>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  category:
    state.singleCategory.data && state.singleCategory.data.items
      ? state.singleCategory.data.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      categoryDeleteRequest: categoryDeleteActions.categoryDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CategoryEdit));
