import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import tagListActions from '../../../../redux/catalog/actionReducer/tag/List';
import TagList from './TagList';
import { getVendorBranchApi } from '../../../../services/vendorApi';
import { getDtoQueryString } from '../../../../utils/helper';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import { getCategoryApi, getTagApi } from '../../../../services/catalogApi';
import { CategoryDto, TagDto } from '../../../../dtos/catalogDtos';
import { PAGE_TITLE_TAG_LIST } from '../../../../Resources/Localization';
import secondaryLevelListActions from '../../../../redux/catalog/actionReducer/category/SecondaryLevelList';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  const tagData = await getTagApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(new TagDto({ published: true }))
        .includes(['imageDtos', 'categoryTagDtos'])
        .buildJson(),
    ),
  );
  if (tagData.status === 200)
    store.dispatch(tagListActions.tagListSuccess(tagData.data));

  const vendorBranchData = await getVendorBranchApi(
    store.getState().login.data.token,
    getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
  );

  if (vendorBranchData.status === 200)
    store.dispatch(
      vendorBranchListActions.vendorBranchListSuccess(vendorBranchData.data),
    );

  /**
   * get all Secondary Level categories  with default clause secondaryLevel
   */
  const categoryData = await getCategoryApi(
    store.getState().login.data.token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(new CategoryDto({ secondaryLevel: true, published: true }))
        .buildJson(),
    ),
  );

  if (categoryData.status === 200)
    store.dispatch(
      secondaryLevelListActions.secondaryLevelListSuccess(categoryData.data),
    );

  return {
    chunks: ['tagList'],
    title: `${PAGE_TITLE_TAG_LIST}`,
    component: (
      <Layout>
        <TagList />
      </Layout>
    ),
  };
}

export default action;
