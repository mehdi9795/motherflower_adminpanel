import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { hideLoading } from 'react-redux-loading-bar';
import { Select } from 'antd';
import HotKeys from 'react-hot-keys';
import { bindActionCreators } from 'redux';
import CPList from '../../../../components/CP/CPList/CPList';
import s from './TagList.css';
import {
  STATUS,
  ACTIVE,
  YES,
  NO,
  DEACTIVATE,
  NAME,
  TAG_LIST,
  TAG,
  RESERVATION_VENDOR_BRANCH,
  RELATED_TAGS,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_TAG,
  IMAGE_UPLOAD,
  CATEGORY,
} from '../../../../Resources/Localization';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import CPSwitch from '../../../../components/CP/CPSwitch';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import CPModal from '../../../../components/CP/CPModal';
import CPSelect from '../../../../components/CP/CPSelect';
import CPMultiSelect from '../../../../components/CP/CPMultiSelect';
import tagListActions from '../../../../redux/catalog/actionReducer/tag/List';
import tagPostActions from '../../../../redux/catalog/actionReducer/tag/Post';
import tagPutActions from '../../../../redux/catalog/actionReducer/tag/Put';
import tagDeleteActions from '../../../../redux/catalog/actionReducer/tag/Delete';
import imageTagDeleteActions from '../../../../redux/catalog/actionReducer/tag/ImageDelete';
import { getRelatedTagApi } from '../../../../services/catalogApi';
import { getCookie } from '../../../../utils';
import {
  getDtoQueryString,
  pressEnterAndCallApplySearch,
} from '../../../../utils/helper';
import { CategoryDto, TagDto } from '../../../../dtos/catalogDtos';
import CPUpload from '../../../../components/CP/CPUpload';
import { ImageDto } from '../../../../dtos/cmsDtos';
import CPCheckbox from '../../../../components/CP/CPCheckbox';
import CPAvatar from '../../../../components/CP/CPAvatar';
import CPAlert from '../../../../components/CP/CPAlert';
import AlertHideActions from '../../../../redux/shared/actionReducer/alert/alert';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPCarousel from '../../../../components/CP/CPCarousel';

const { Option } = Select;

class tagList extends React.Component {
  static propTypes = {
    tags: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    categories: PropTypes.arrayOf(PropTypes.object),
    tagCount: PropTypes.number,
    tagLoading: PropTypes.bool,
    errorMessage: PropTypes.string,
    errorAlert: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    tags: [],
    vendorBranches: [],
    tagCount: 0,
    errorMessage: '',
    errorAlert: false,
    tagLoading: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      searchName: '',
      searchStatus: true,
      name: ``,
      vendorBranchId:
        props.vendorBranches.length > 0 ? props.vendorBranches[0].id : '',
      status: false,
      visibleModal: false,
      relatedTags: [],
      id: 0,
      relatedTagIds: [],
      binariesImage: '',
      extention: '',
      imageId: 0,
      firstLoadImage: true,
      selectedCategories: [],
      visibleImageModal: false,
      dataImage: [],
    };
    this.showEditModal = this.showEditModal.bind(this);
    this.props.actions.hideLoading();
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const jsonList = new BaseGetDtoBuilder()
      .dto({ published: this.state.searchStatus })
      .includes(['imageDtos'])
      .buildJson();
    const data = {
      id,
      listDto: jsonList,
    };

    this.props.actions.tagDeleteRequest(data);
  };

  onCloseErrorMessage = () => {
    this.props.actions.AlertHideRequest();
  };

  onDeleteImage = () => {
    const { imageId } = this.state;

    const jsonList = new BaseGetDtoBuilder()
      .dto({ published: this.state.searchStatus })
      .includes(['imageDtos'])
      .buildJson();

    const deleteData = {
      id: imageId,
      listDto: jsonList,
    };

    this.props.actions.imageTagDeleteRequest(deleteData);
    this.setState({ url: '' });
  };

  showModal = () => {
    const relatedTags = [];
    this.props.tags.map(item =>
      relatedTags.push(
        <Option value={item.id.toString()} key={item.id.toString()}>
          {item.title}
        </Option>,
      ),
    );

    this.setState({
      visibleModal: true,
      relatedTagIds: [],
      name: '',
      vendorBranchId: this.state.vendorBranchId,
      status: false,
      relatedTags,
      id: 0,
      url: '',
      firstLoadImage: true,
      binariesImage: '',
      selectedCategories: [],
    });
  };

  async showEditModal(e, record) {
    e.preventDefault();
    const relatedTagSelected = [];
    const relatedTags = [];

    const response = await getRelatedTagApi(
      getCookie('token'),
      getDtoQueryString(
        JSON.stringify({ dto: { tagSource: { id: record ? record.key : 0 } } }),
      ),
    );

    if (response.status === 200) {
      response.data.items.map(item =>
        relatedTagSelected.push(item.tagRelated.id.toString()),
      );
    }

    this.props.tags.map(item =>
      relatedTags.push(
        <Option value={item.id.toString()} key={item.id.toString()}>
          {item.title}
        </Option>,
      ),
    );

    this.setState({
      visibleModal: true,
      relatedTagIds: relatedTagSelected,
      name: record ? record.title : '',
      vendorBranchId: record
        ? record.vendorBranchId
        : this.state.vendorBranchId,
      status: record ? record.active : false,
      relatedTags,
      id: record.key,
      url: record.imageUrl,
      imageId: record.imageId,
      firstLoadImage: true,
      binariesImage: '',
      selectedCategories: record.categoryIds,
    });
  }

  handleOkModal = () => {
    const {
      name,
      vendorBranchId,
      status,
      relatedTagIds,
      id,
      extention,
      binariesImage,
      selectedCategories,
    } = this.state;
    const RelatedTagDtos = [];
    // const imageDtoArray = [];
    const categoryDtoArray = [];

    selectedCategories.map(item => {
      categoryDtoArray.push({ categoryDto: new CategoryDto({ id: item }) });
      return null;
    });

    relatedTagIds.map(item => {
      RelatedTagDtos.push(new TagDto({ id: item }));
      return null;
    });

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new TagDto({
          title: name,
          published: status,
          reservedVendorBranchDto: { id: vendorBranchId },
          RelatedTagDtos,
          id,
          categoryTagDtos: categoryDtoArray,
          imageDtos: binariesImage
            ? [
                new ImageDto({
                  extention,
                  binaries: binariesImage.split(',')[1],
                }),
              ]
            : null,
        }),
      )
      .build();

    const jsonList = new BaseGetDtoBuilder()
      .dto({ published: this.state.searchStatus })
      .includes(['imageDtos', 'categoryTagDtos'])
      .buildJson();
    const data = {
      data: jsonCrud,
      listDto: jsonList,
    };

    if (id === 0) this.props.actions.tagPostRequest(data);
    else this.props.actions.tagPutRequest(data);

    this.setState({
      visibleModal: false,
      relatedTagIds: [],
      name: '',
      vendorBranchId:
        this.props.vendorBranches.length > 0
          ? this.props.vendorBranches[0].id
          : '',
      status: false,
      relatedTags: [],
      id: 0,
      url: '',
      imageId: 0,
      selectedCategories: [],
    });
  };

  handleCancelModal = () => {
    this.setState({ visibleModal: false, selectedCategories: [] });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  handleImageChange = value => {
    this.setState({
      binariesImage: value.base64,
      extention: value.type.split('/')[1],
      firstLoadImage: false,
    });
  };

  submitSearch = () => {
    const { searchName, searchStatus } = this.state;

    const jsonList = new BaseGetDtoBuilder()
      .dto(new TagDto({ title: searchName, published: searchStatus }))
      .includes(['imageDtos'])
      .buildJson();

    this.props.actions.tagListRequest(jsonList);
  };

  showImageModal = image => {
    const dataImage = [];
    const obj = {
      small: `${image}`,
      large: `${image}`,
      key: image,
      // srcSet: srcSetArray, TODO: It was commented on the error
    };
    dataImage.push(obj);
    this.setState({ visibleImageModal: true, dataImage });
  };

  handleCancelImageModal = () => {
    this.setState({ visibleImageModal: false });
  };

  render() {
    const {
      tagLoading,
      tags,
      tagCount,
      vendorBranches,
      errorAlert,
      errorMessage,
      categories,
    } = this.props;

    const {
      searchStatus,
      searchName,
      name,
      vendorBranchId,
      status,
      visibleModal,
      relatedTags,
      relatedTagIds,
      url,
      firstLoadImage,
      selectedCategories,
      visibleImageModal,
      dataImage,
    } = this.state;

    const tagArray = [];
    const vendorBranchArray = [];
    const categoryArray = [];
    const columns = [
      {
        title: '',
        dataIndex: 'imageUrl',
        render: (text, record) => (
          <div onClick={() => this.showImageModal(record.imageUrl, record.key)}>
            <CPAvatar src={record.imageUrl} />
          </div>
        ),
      },
      {
        title: NAME,
        dataIndex: 'title',
      },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPSwitch size="small" disabled defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'tag_Delete',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_TAG}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Tag_Edit',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to="/"
              onClick={e => this.showEditModal(e, record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    tags.map(item => {
      const categoryIds = [];
      if (item.categoryTagDtos)
        item.categoryTagDtos.map(item2 => {
          categoryIds.push(item2.categoryDto.id.toString());
          return null;
        });

      const obj = {
        title: item.title,
        active: item.published,
        key: item.id,
        vendorBranchId: item.reservedVendorBranchDto
          ? item.reservedVendorBranchDto.id
          : '',
        imageUrl:
          item.imageDtos && item.imageDtos.length > 0
            ? item.imageDtos[0].url
            : '',
        imageId:
          item.imageDtos && item.imageDtos.length > 0
            ? item.imageDtos[0].id
            : 0,
        categoryIds,
      };
      tagArray.push(obj);
      return null;
    });

    vendorBranches.map(item => {
      vendorBranchArray.push(
        <Option value={item.id} key={item.id}>
          {item.name}
        </Option>,
      );
      return null;
    });

    /**
     * map secondary Category for comboBox Datasource
     */
    categories.map(item => {
      categoryArray.push(
        <Option value={item.id.toString()} key={item.id.toString()}>
          {item.name}
        </Option>,
      );
      return null;
    });

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.showModal}>
        <div className={s.mainContent}>
          <h3>{TAG_LIST}</h3>
          {errorAlert && (
            <CPAlert
              showIcon
              closable
              onClose={this.onCloseErrorMessage}
              type="error"
              message="خطا"
              description={errorMessage}
            />
          )}
          <CPList
            data={tagArray}
            count={tagCount}
            columns={columns}
            loading={tagLoading}
            onAddClick={this.showModal}
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      label={NAME}
                      value={searchName}
                      onChange={value => {
                        this.handelChangeInput(
                          'searchName',
                          value.target.value,
                        );
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <div className="align-center">
                      <CPSwitch
                        defaultChecked={searchStatus}
                        checkedChildren={ACTIVE}
                        unCheckedChildren={DEACTIVATE}
                        onChange={value => {
                          this.handelChangeInput('searchStatus', value);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className={s.field}>
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>

          <CPModal
            visible={visibleModal}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
            title={TAG}
          >
            <div className="row">
              <div className="col-sm-12">
                <CPInput
                  label={NAME}
                  value={name}
                  onChange={value => {
                    this.handelChangeInput('name', value.target.value);
                  }}
                  autoFocus
                />
              </div>
              <div className="col-sm-12">
                <label>{RESERVATION_VENDOR_BRANCH}</label>
                <CPSelect
                  onChange={value => {
                    this.handelChangeInput('vendorBranchId', value);
                  }}
                  placeholder=""
                  value={vendorBranchId}
                >
                  {vendorBranchArray}
                </CPSelect>
              </div>
              <div className="col-sm-12">
                <label>{CATEGORY}</label>
                <CPMultiSelect
                  onChange={value => {
                    this.handelChangeInput('selectedCategories', value);
                  }}
                  value={selectedCategories}
                >
                  {categoryArray}
                </CPMultiSelect>
              </div>
              <div className="col-sm-12">
                <label>{RELATED_TAGS}</label>
                <CPMultiSelect
                  onChange={value => {
                    this.handelChangeInput('relatedTagIds', value);
                  }}
                  placeholder=""
                  value={relatedTagIds}
                >
                  {relatedTags}
                </CPMultiSelect>
              </div>
              <div className="col-sm-12">
                <label>{IMAGE_UPLOAD}</label>
                <CPUpload
                  onChange={this.handleImageChange}
                  image={`${url}`}
                  deleted
                  onDelete={this.onDeleteImage}
                  firstLoad={firstLoadImage}
                />
              </div>
              <div className="col-sm-12">
                <label>{STATUS}</label>
                <CPCheckbox
                  checked={status}
                  onChange={value => {
                    this.handelChangeInput('status', value.target.checked);
                  }}
                />
              </div>
            </div>
          </CPModal>
          <CPModal
            visible={visibleImageModal}
            handleCancel={this.handleCancelImageModal}
            showFooter={false}
          >
            <CPCarousel data={dataImage} width={700} height={700} />
          </CPModal>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  tags: state.tagList.data.items,
  tagCount: state.tagList.data.count,
  tagLoading: state.tagList.loading,
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
  errorAlert: state.editFormAlert.loading,
  errorMessage: state.editFormAlert.data,
  categories: state.secondaryLevelList.data
    ? state.secondaryLevelList.data.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      tagPostRequest: tagPostActions.tagPostRequest,
      tagPutRequest: tagPutActions.tagPutRequest,
      tagDeleteRequest: tagDeleteActions.tagDeleteRequest,
      tagListRequest: tagListActions.tagListRequest,
      imageTagDeleteRequest: imageTagDeleteActions.imageTagDeleteRequest,
      AlertHideRequest: AlertHideActions.editFormAlertHideRequest,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(tagList));
