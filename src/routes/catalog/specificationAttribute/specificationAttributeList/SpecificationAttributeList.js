import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import HotKeys from 'react-hot-keys';
import { connect } from 'react-redux';
import { Select } from 'antd';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import CPList from '../../../../components/CP/CPList/CPList';
import s from './SpecificationAttributeList.css';
import {
  YES,
  NO,
  NAME,
  TAG_LIST,
  IMAGE_UPLOAD,
  FEATURE,
  TYPE,
  DISPLAY_ORDER,
  SHOW_ON_PRODUCT_PAGE,
  ALLOW_FILTERING,
  ARE_YOU_SURE_WANT_TO_DELETE_ATTRIBUTE,
  SHOW_ON_MAIN_PAGE,
  AGENT_NAME,
  ALL_VENDORS,
  ALL,
  ACTIVE,
  DEACTIVATE,
} from '../../../../Resources/Localization';
import CPInput from '../../../../components/CP/CPInput';
import CPButton from '../../../../components/CP/CPButton';
import CPSwitch from '../../../../components/CP/CPSwitch';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import CPModal from '../../../../components/CP/CPModal';
import CPSelect from '../../../../components/CP/CPSelect';
import { SpecificationAttributeDto } from '../../../../dtos/catalogDtos';
import CPUpload from '../../../../components/CP/CPUpload';
import { ImageDto } from '../../../../dtos/cmsDtos';
import specificationAttributeDeleteActions from '../../../../redux/catalog/actionReducer/specificationAttribute/Delete';
import specificationAttributePostActions from '../../../../redux/catalog/actionReducer/specificationAttribute/Post';
import specificationAttributePutActions from '../../../../redux/catalog/actionReducer/specificationAttribute/Put';
import specificationAttributeListActions from '../../../../redux/catalog/actionReducer/specificationAttribute/List';
import imageSpecificationAttributeDeleteActions from '../../../../redux/catalog/actionReducer/specificationAttribute/ImageDelete';
import CPCheckbox from '../../../../components/CP/CPCheckbox';
import CPAvatar from '../../../../components/CP/CPAvatar';
import AlertHideActions from '../../../../redux/shared/actionReducer/alert/alert';
import CPAlert from '../../../../components/CP/CPAlert';
import CPPagination from '../../../../components/CP/CPPagination';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import {pressEnterAndCallApplySearch} from "../../../../utils/helper";

const { Option } = Select;

class specificationAttributeList extends React.Component {
  static propTypes = {
    specificationAttributes: PropTypes.arrayOf(PropTypes.object),
    specificationAttributeCount: PropTypes.number,
    specificationAttributeLoading: PropTypes.bool,
    errorMessage: PropTypes.string,
    errorAlert: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    specificationAttributes: [],
    specificationAttributeCount: 0,
    errorMessage: '',
    errorAlert: false,
    specificationAttributeLoading: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      searchName: '',
      name: ``,
      typeId: '1',
      allowFiltering: false,
      showOnProductPage: false,
      displayOrder: 0,
      visibleModal: false,
      id: 0,
      binariesImage: '',
      extention: '',
      url: '',
      imageId: 0,
      firstLoadImage: true,
      currentPage: 1,
      pageSize: 20,
      searchFilter: 2,
      searchShowProductPage: 2,
    };
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  componentWillReceiveProps() {
    this.props.actions.hideLoading();
  }

  onDelete = id => {
    const { searchName } = this.state;
    const jsonList = new BaseGetDtoBuilder()
      .dto(new SpecificationAttributeDto({ name: searchName }))
      .includes(['imageDtos'])
      .buildJson();

    const data = {
      id,
      listDto: jsonList,
    };

    this.props.actions.specificationAttributeDeleteRequest(data);
  };

  onDeleteImage = () => {
    const { imageId } = this.state;

    const deleteData = {
      id: imageId,
      listDto: new BaseGetDtoBuilder()
        .all(true)
        .includes(['imageDtos'])
        .buildJson(),
    };

    this.props.actions.imageSpecificationAttributeDeleteRequest(deleteData);
    this.setState({ url: '' });
  };

  showModal = () => {
    this.setState({
      visibleModal: true,
      name: '',
      typeId: '1',
      allowFiltering: false,
      showOnProductPage: false,
      displayOrder: 0,
      url: '',
      id: 0,
      firstLoadImage: true,
      binariesImage: '',
    });
  };

  async showEditModal(e, record) {
    e.preventDefault();

    this.setState({
      visibleModal: true,
      name: record ? record.name : '',
      typeId: record.typeId,
      allowFiltering: record.allowFiltering,
      showOnProductPage: record.showOnProductPage,
      displayOrder: record.displayOrder,
      id: record.key,
      url: record.imageUrl,
      imageId: record.imageId,
      firstLoadImage: true,
      binariesImage: '',
    });
  }

  handleOkModal = () => {
    const {
      name,
      allowFiltering,
      showOnProductPage,
      displayOrder,
      id,
      extention,
      binariesImage,
      typeId,
    } = this.state;
    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new SpecificationAttributeDto({
          name,
          allowFiltering,
          showOnProductPage,
          displayOrder,
          specificationAttributeType: typeId,
          id,
          imageDtos: binariesImage
            ? [
                new ImageDto({
                  extention,
                  binaries: binariesImage.split(',')[1],
                }),
              ]
            : undefined,
        }),
      )
      .build();
    const jsonList = new BaseGetDtoBuilder()
      .dto(new SpecificationAttributeDto({ name: this.state.searchName }))
      .includes(['imageDtos'])
      .buildJson();

    const data = {
      data: jsonCrud,
      listDto: jsonList,
    };
    if (id === 0) this.props.actions.specificationAttributePostRequest(data);
    else this.props.actions.specificationAttributePutRequest(data);

    this.setState({ visibleModal: false });
  };

  handleCancelModal = () => {
    this.setState({ visibleModal: false });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  handleImageChange = value => {
    this.setState({
      binariesImage: value.base64,
      extention: value.type.split('/')[1],
      firstLoadImage: false,
    });
  };

  submitSearch = () => {
    const { searchName, searchShowProductPage, searchFilter } = this.state;
    console.log('searchShowProductPage',searchShowProductPage);
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new SpecificationAttributeDto({
          name: searchName,
          searchAllowFiltering: searchFilter === 2 ? undefined : searchFilter,
          searchShowOnProductPage:
            searchShowProductPage === 2 ? undefined : searchShowProductPage,
        }),
      )
      .includes(['imageDtos'])
      .buildJson();

    this.props.actions.specificationAttributeListRequest(jsonList);
  };

  onCloseErrorMessage = () => {
    this.props.actions.AlertHideRequest();
  };

  onChangePagination = page => {
    this.setState({ currentPage: page }, () => {
      const { searchName, pageSize, currentPage } = this.state;
      const jsonList = new BaseGetDtoBuilder()
        .dto(new SpecificationAttributeDto({ name: searchName }))
        .includes(['imageDtos'])
        .pageSize(pageSize)
        .pageIndex(currentPage - 1)
        .buildJson();
      this.props.actions.specificationAttributeListRequest(jsonList);
    });
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () => {
      const { searchName, currentPage } = this.state;

      const jsonList = new BaseGetDtoBuilder()
        .dto(new SpecificationAttributeDto({ name: searchName }))
        .includes(['imageDtos'])
        .pageSize(pageSize)
        .pageIndex(currentPage - 1)
        .buildJson();

      this.props.actions.specificationAttributeListRequest(jsonList);
    });
  };

  render() {
    const {
      specificationAttributeLoading,
      specificationAttributes,
      specificationAttributeCount,
      errorAlert,
      errorMessage,
    } = this.props;

    const {
      searchName,
      name,
      typeId,
      allowFiltering,
      showOnProductPage,
      displayOrder,
      visibleModal,
      url,
      firstLoadImage,
      currentPage,
      searchFilter,
      searchShowProductPage,
    } = this.state;

    const specificationAttributeArray = [];
    const columns = [
      {
        title: '',
        dataIndex: 'imageUrl',
        width: 50,
        render: (text, record) => <CPAvatar src={record.imageUrl} />,
      },
      {
        title: NAME,
        dataIndex: 'name',
        width: 120,
      },
      {
        title: TYPE,
        dataIndex: 'typeId',
        width: 80,
      },
      {
        title: DISPLAY_ORDER,
        dataIndex: 'displayOrder',
        width: 90,
      },
      {
        title: ALLOW_FILTERING,
        dataIndex: 'allowFiltering',
        width: 50,
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              defaultChecked={record.allowFiltering}
            />
          </div>
        ),
      },
      {
        title: SHOW_ON_PRODUCT_PAGE,
        dataIndex: 'showOnProductPage',
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              defaultChecked={record.showOnProductPage}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'tag_Delete',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_ATTRIBUTE}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Tag_Edit',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to="/"
              onClick={e => this.showEditModal(e, record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    specificationAttributes.map(item => {
      const obj = {
        name: item.name,
        allowFiltering: item.allowFiltering,
        key: item.id,
        showOnProductPage: item.showOnProductPage,
        displayOrder: item.displayOrder,
        imageUrl:
          item.imageDtos && item.imageDtos.length > 0
            ? item.imageDtos[0].url
            : '',
        imageId:
          item.imageDtos && item.imageDtos.length > 0
            ? item.imageDtos[0].id
            : '',
        typeId: item.specificationAttributeType,
      };
      specificationAttributeArray.push(obj);
      return null;
    });

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.showModal}>
        <div className={s.mainContent}>
          <h3>{TAG_LIST}</h3>
          {errorAlert && (
            <CPAlert
              showIcon
              closable
              onClose={this.onCloseErrorMessage}
              type="error"
              message="خطا"
              description={errorMessage}
            />
          )}
          <CPList
            data={specificationAttributeArray}
            count={specificationAttributeCount}
            columns={columns}
            loading={specificationAttributeLoading}
            onAddClick={this.showModal}
            pagination={false}
            showTopAdd
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      label={NAME}
                      value={searchName}
                      onChange={value => {
                        this.handelChangeInput(
                          'searchName',
                          value.target.value,
                        );
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label name="label">{ALLOW_FILTERING}</label>
                    <CPSelect
                      showSearch
                      value={searchFilter}
                      onChange={value => {
                        this.handelChangeInput('searchFilter', value);
                      }}
                    >
                      <Option key="2" value={2}>{ALL}</Option>
                      <Option key="1" value={1}>{ACTIVE}</Option>
                      <Option key="0" value={0}>{DEACTIVATE}</Option>
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label name="label">{SHOW_ON_PRODUCT_PAGE}</label>
                    <CPSelect
                      showSearch
                      value={searchShowProductPage}
                      onChange={value => {
                        this.handelChangeInput('searchShowProductPage', value);
                      }}
                    >
                      <Option key="2" value={2}>{ALL}</Option>
                      <Option key="1" value={1}>{ACTIVE}</Option>
                      <Option key="0" value={0}>{DEACTIVATE}</Option>
                    </CPSelect>
                  </div>
                </div>
              </div>
              <div className={s.field}>
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
          <CPPagination
            total={specificationAttributeCount}
            current={currentPage}
            onChange={this.onChangePagination}
            onShowSizeChange={this.onShowSizeChange}
          />
          <CPModal
            visible={visibleModal}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
            title={FEATURE}
          >
            <div className="row">
              <div className="col-sm-12">
                <CPInput
                  label={NAME}
                  value={name}
                  onChange={value => {
                    this.handelChangeInput('name', value.target.value);
                  }}
                  autoFocus
                />
              </div>
              <div className="col-sm-12">
                <label>{TYPE}</label>
                <CPSelect
                  onChange={value => {
                    this.handelChangeInput('typeId', value);
                  }}
                  placeholder=""
                  value={typeId}
                >
                  <Option value="1" key={1}>
                    Color
                  </Option>
                  <Option value="2" key={2}>
                    CheckBox
                  </Option>
                  <Option value="3" key={3}>
                    TextInt
                  </Option>
                </CPSelect>
              </div>
              <div className="col-sm-12">
                <label>{IMAGE_UPLOAD}</label>
                <CPUpload
                  onChange={this.handleImageChange}
                  image={`${url}`}
                  deleted
                  onDelete={this.onDeleteImage}
                  firstLoad={firstLoadImage}
                />
              </div>
              <div className="col-sm-12">
                <CPInput
                  label={DISPLAY_ORDER}
                  value={displayOrder.toString()}
                  onChange={value => {
                    this.handelChangeInput('displayOrder', value.target.value);
                  }}
                  type="number"
                />
              </div>
              <div className="col-sm-12 switches">
                <label>{ALLOW_FILTERING}</label>
                <CPCheckbox
                  checked={allowFiltering}
                  onChange={value => {
                    this.handelChangeInput(
                      'allowFiltering',
                      value.target.checked,
                    );
                  }}
                />
                <label>{SHOW_ON_PRODUCT_PAGE}</label>
                <CPCheckbox
                  checked={showOnProductPage}
                  onChange={value => {
                    this.handelChangeInput(
                      'showOnProductPage',
                      value.target.checked,
                    );
                  }}
                />
              </div>
            </div>
          </CPModal>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  specificationAttributes: state.specificationAttributeList.data.items,
  specificationAttributeCount: state.specificationAttributeList.data.count,
  specificationAttributeLoading: state.specificationAttributeList.loading,
  errorAlert: state.editFormAlert.loading,
  errorMessage: state.editFormAlert.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      specificationAttributePostRequest:
        specificationAttributePostActions.specificationAttributePostRequest,
      specificationAttributePutRequest:
        specificationAttributePutActions.specificationAttributePutRequest,
      specificationAttributeDeleteRequest:
        specificationAttributeDeleteActions.specificationAttributeDeleteRequest,
      specificationAttributeListRequest:
        specificationAttributeListActions.specificationAttributeListRequest,
      imageSpecificationAttributeDeleteRequest:
        imageSpecificationAttributeDeleteActions.imageSpecificationAttributeDeleteRequest,
      AlertHideRequest: AlertHideActions.editFormAlertHideRequest,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(specificationAttributeList));
