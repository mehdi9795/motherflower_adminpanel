import React from 'react';
import Layout from '../../../../components/Template/Layout/Layout';
import SpecificationAttributeList from './SpecificationAttributeList';
import specificationAttributeListActions from '../../../../redux/catalog/actionReducer/specificationAttribute/List';
import { PAGE_TITLE_SPECIFICATION_ATTRIBUTE_LIST } from '../../../../Resources/Localization';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

async function action({ store }) {
  const json = new BaseGetDtoBuilder()
    .all(true)
    .includes(['imageDtos'])
    .pageIndex(0)
    .pageSize(20)
    .buildJson();
  store.dispatch(
    specificationAttributeListActions.specificationAttributeListRequest(json),
  );

  return {
    chunks: ['specificationAttributeList'],
    title: `${PAGE_TITLE_SPECIFICATION_ATTRIBUTE_LIST}`,
    component: (
      <Layout>
        <SpecificationAttributeList />
      </Layout>
    ),
  };
}

export default action;
