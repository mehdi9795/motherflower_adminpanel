import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import CardTable from '../../../../components/CP/CPList/CPList';
import s from './OrderItem.css';
import {
  COUNT,
  SHIPPING_TARIFF,
  TOMAN,
  DISCOUNT,
  TOTAL_PRICE,
  UNIT_PRICE,
} from '../../../../Resources/Localization';
import CPAvatar from '../../../CP/CPAvatar';
import Link from '../../../Link';

class OrderInfo extends React.Component {
  static propTypes = {
    // actions: PropTypes.objectOf(PropTypes.func).isRequired,
    orderItems: PropTypes.arrayOf(PropTypes.object),
    orderItemCount: PropTypes.number,
    orderItemLoading: PropTypes.bool,
  };
  static defaultProps = {
    orderItems: null,
    orderItemCount: 0,
    orderItemLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { orderItems, orderItemCount, orderItemLoading } = this.props;
    const orderItemDAtaSource = [];

    const columns = [
      {
        width: '85',
        title: '',
        dataIndex: 'url',
        render: (text, record) => (
          <div>
            <CPAvatar src={record.imgUrl} /> <div>کد {record.code}</div>{' '}
          </div>
        ),
      },
      { title: COUNT, dataIndex: 'count', key: 'count' },
      { title: UNIT_PRICE, dataIndex: 'unitPrice', key: 'unitPrice' },
      {
        title: SHIPPING_TARIFF,
        dataIndex: 'shippingTariff',
        key: 'shippingTariff',
      },
      {
        title: TOTAL_PRICE,
        dataIndex: 'totalPrice',
        key: 'totalPrice',
      },
      {
        title: DISCOUNT,
        dataIndex: 'discountPrice',
        key: 'discountPrice',
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 90,
        render: (text, record) => (
          <div>
            <Link to={`/order/edit/${record.key}`}>درخواست</Link>
          </div>
        ),
      },
    ];

    orderItems.map(item => {
      const obj = {
        code: item.vendorBranchProductDto.productDto.code,
        imgUrl: item.vendorBranchProductDto.productDto.imageDtos
          ? item.vendorBranchProductDto.productDto.imageDtos[0].url
          : '',
        count: item.count,
        // unitPrice: item.
        shippingTariff: `${item.stringShipmentAmount} ${
          item.shipmentAmount ? TOMAN : ''
        }`,
        totalPrice: `${item.stringTotalAmount} ${TOMAN}`,
        discountPrice: `${item.stringTotalDiscount} ${TOMAN}`,
        key: item.id,
      };
      orderItemDAtaSource.push(obj);
      return null;
    });

    return (
      <div>
        <CardTable
          data={orderItemDAtaSource}
          count={orderItemCount}
          columns={columns}
          loading={orderItemLoading}
          hideAdd
          hideSearchBox
        />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  orderItems:
    state.orderItemList.data && state.orderItemList.data
      ? state.orderItemList.data.items
      : null,
  orderItemCount:
    state.orderItemList.data && state.orderItemList.data
      ? state.orderItemList.data.count
      : null,
  orderItemLoading: state.orderItemList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch),
  dispatch,
});
export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(OrderInfo),
);
