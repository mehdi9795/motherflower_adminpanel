import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './OrderForm.css';
import {
  PROMOTION_DETAILS,
  PROMOTION_INFO,
} from '../../../../Resources/Localization';
import OrderInfo from '../OrderInfo';
import OrderItem from '../OrderItem';

class OrderForm extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    return (
      <div className={s.root}>
        <Tabs>
          <TabList>
            <Tab>{PROMOTION_INFO}</Tab>
            <Tab>{PROMOTION_DETAILS}</Tab>
          </TabList>
          <TabPanel>
            <OrderInfo />
          </TabPanel>
          <TabPanel>
            <OrderItem />
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  promotion: state.singlePromotion.data &&
    state.singlePromotion.data.items
    ? state.singlePromotion.data.items[0]
    : null,
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(OrderForm),
);
