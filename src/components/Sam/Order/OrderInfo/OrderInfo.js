import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './OrderInfo.css';
import {
  STATUS,
  ID,
  USER_INFO,
  ADDRESS,
  RECEIVER_NAME,
  SENDER_NAME,
  PHONE,
  EMAIL,
  FACTOR_INFO,
  ORDER_DATE,
  TOTAL_PRICE,
  PAYABLE_BILLING_AMOUNT,
  DISCOUNT,
  DISCOUNT_SHIPPING,
} from '../../../../Resources/Localization';
import CPPanel from '../../../CP/CPPanel/CPPanel';
import CPInput from '../../../CP/CPInput/CPInput';

class OrderInfo extends React.Component {
  static propTypes = {
    orderDetail: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    orderDetail: {},
  };

  render() {
    const { orderDetail } = this.props;

    return (
      <div className="disabled">
        <div className="tabContent">
          <div className="row">
            <div className="col-lg-6 col-sm-12">
              <CPPanel header={USER_INFO}>
                <div className="row">
                  <div className="col-lg-6 col-sm-12">
                    <label>{SENDER_NAME}</label>
                    <CPInput
                      value={`${orderDetail &&
                        orderDetail.userDto &&
                        orderDetail.userDto.firstName} ${orderDetail &&
                        orderDetail.userDto &&
                        orderDetail.userDto.lastName}`}
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{PHONE}</label>
                    <CPInput
                      value={
                        orderDetail &&
                        orderDetail.userDto &&
                        orderDetail.userDto.phone.replace('+98', '0')
                      }
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{EMAIL}</label>
                    <CPInput value={orderDetail && orderDetail.userDto && orderDetail.userDto.email} />
                  </div>
                </div>
                {orderDetail && orderDetail.addressDto && (
                <div className="row">
                  <div className="col-lg-6 col-sm-12">
                    <label>{RECEIVER_NAME}</label>
                    <CPInput
                      value={orderDetail && orderDetail.addressDto && orderDetail.addressDto.receiverName}
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{PHONE}</label>
                    <CPInput
                      value={orderDetail && orderDetail.addressDto && orderDetail.addressDto.phone}
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{ADDRESS}</label>
                    <CPInput
                      value={orderDetail && orderDetail.addressDto && orderDetail.addressDto.content}
                    />
                  </div>
                </div>
                )}
              </CPPanel>
            </div>
            <div className="col-lg-6 col-sm-12">
              <CPPanel header={FACTOR_INFO}>
                <div className="row">
                  <div className="col-lg-6 col-sm-12">
                    <label>{ID}</label>
                    <CPInput value={orderDetail && orderDetail.code} />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{ORDER_DATE}</label>
                    <CPInput
                      value={
                        orderDetail && orderDetail.persianCreatedDateTimeUtc
                      }
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{TOTAL_PRICE}</label>
                    <CPInput
                      value={orderDetail && orderDetail.stringTotalAmount}
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{DISCOUNT}</label>
                    <CPInput
                      value={orderDetail && orderDetail.stringTotalDiscount}
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{DISCOUNT_SHIPPING}</label>
                    <CPInput
                      value={
                        orderDetail && orderDetail.stringTotalShipmentAmount
                      }
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{PAYABLE_BILLING_AMOUNT}</label>
                    <CPInput
                      value={
                        orderDetail && orderDetail.stringPayableBillingAmount
                      }
                    />
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{STATUS}</label>
                    <CPInput value={orderDetail && orderDetail.billStatus} />
                  </div>
                </div>
              </CPPanel>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  orderDetail:
    state.orderSingle.data && state.orderSingle.data.items
      ? state.orderSingle.data.items[0]
      : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
    },
    dispatch,
  ),
  dispatch,
});
export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(OrderInfo),
);
