import React from 'react';
import PropTypes from 'prop-types';
import { hideLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ShippingTariffForm.css';
import {
  PROMOTION_DETAILS,
  TARIFF_INFO,
} from '../../../../Resources/Localization';
import ShippingTariffInfo from '../ShippingTariffInfo';
import ShippingTariffPromotion from '../ShippingTariffPromotion';

class ShippingTariffForm extends React.Component {
  static propTypes = {
    shippingTariff: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    shippingTariff: {},
    loginData: {},
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    const { shippingTariff } = this.props;

    const isEdit = shippingTariff && shippingTariff.id;

    return (
      <div className={s.root}>
        <Tabs>
          <TabList>
            <Tab>{PROMOTION_DETAILS}</Tab>
            {isEdit && <Tab>{TARIFF_INFO}</Tab>}
          </TabList>
          <TabPanel>
            <ShippingTariffInfo />
          </TabPanel>
          {isEdit && (
            <TabPanel>
              <ShippingTariffPromotion />
            </TabPanel>
          )}
        </Tabs>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  shippingTariff: state.vbShippingTariffList.data
    ? state.vbShippingTariffList.data.items[0]
    : null,
  loginData: state.login.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(ShippingTariffForm),
);
