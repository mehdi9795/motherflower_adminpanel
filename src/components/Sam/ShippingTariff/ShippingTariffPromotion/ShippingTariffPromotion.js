import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ShippingTariffPromotion.css';
import {
  VALUE,
  YES,
  NO,
  PROMOTION_LIST,
  PRICE,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VB_SHIPPING_TARIFF_PROMOTION,
  FROM_PRICE,
  TO_PRICE,
  PERCENT,
  TOMAN,
  STATUS,
  DEACTIVATE,
  ACTIVE,
  COMMISSION_TYPE,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton/CPButton';
import CardTable from '../../../../components/CP/CPList/CPList';
import vbShippingTariffPromotionPutActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/VBShippingTariffPromotionPut';
import vbShippingTariffPromotionDeleteActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/VBShippingTariffPromotionDelete';
import vbShippingTariffPromotionPostActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/VBShippingTariffPromotionPost';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import Link from '../../../Link';
import CPSwitch from '../../../CP/CPSwitch';
import CPInputNumber from '../../../CP/CPInputNumber';
import CPModal from '../../../CP/CPModal';
import CPSelect from '../../../CP/CPSelect';
import {
  VendorBranchShippingTariffPromotionDto,
  VendorBranchShippingTariffsDto,
} from '../../../../dtos/samDtos';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class ShippingTariffInfo extends React.Component {
  static propTypes = {
    shippingTariffPromotionLoading: PropTypes.bool,
    shippingTariffPromotionCount: PropTypes.number,
    shippingTariffPromotions: PropTypes.arrayOf(PropTypes.object).isRequired,
    shippingTariffs: PropTypes.arrayOf(PropTypes.object).isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    shippingTariffPromotionLoading: false,
    shippingTariffPromotionCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      type: 'Percent',
      amount: 1,
      fromPrice: 0,
      toPrice: 0,
      published: true,
      visibleModal: false,
      id: 0,
    };
    this.handelChangeInput = this.handelChangeInput.bind(this);
  }

  onEdit = (e, record) => {
    e.preventDefault();
    this.setState({
      type: record.amountType,
      amount: record.amount,
      fromPrice: record.fromPrice,
      toPrice: record.toPrice,
      published: record.published,
      visibleModal: true,
      id: record.key,
    });
  };

  getList = () => {
    const { shippingTariffs } = this.props;

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchShippingTariffPromotionDto({
          vendorBranchShippingTariffDto: new VendorBranchShippingTariffsDto({
            id: shippingTariffs[0].id,
          }),
        }),
      )
      .buildJson();

    return jsonList;
  };

  onDelete = id => {
    /**
     * create dto for reload list after post data
     */
    const data = {
      id,
      listDto: this.getList(),
    };
    this.props.actions.vbShippingTariffPromotionDeleteRequest(data);
  };

  showModal = () => {
    this.setState({
      visibleModal: true,
      type: 'Percent',
      amount: 1,
      fromPrice: 0,
      toPrice: 0,
      published: true,
      id: 0,
    });
  };

  handleCancelModal = () => {
    this.setState({ visibleModal: false });
  };

  handleOkModal = () => {
    const { shippingTariffs } = this.props;
    const { type, amount, fromPrice, toPrice, published, id } = this.state;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new VendorBranchShippingTariffPromotionDto({
          vendorBranchShippingTariffDto: new VendorBranchShippingTariffsDto({
            id: shippingTariffs[0].id,
          }),
          fromPrice,
          toPrice,
          amountType: type,
          amount,
          published,
          id,
        }),
      )
      .build();

    /**
     * create dto for reload list after post data
     */
    const data = {
      data: jsonCrud,
      listDto: this.getList(),
    };
    if (id === 0) this.props.actions.vbShippingTariffPromotionPostRequest(data);
    else this.props.actions.vbShippingTariffPromotionPutRequest(data);
    this.handleCancelModal();
  };

  handelChangeInput(inputName, value) {
    this.setState({ [inputName]: value });
  }

  changeStatus = (value, record) => {
    this.setState(
      {
        published: value,
        type: record.amountType,
        amount: record.amount,
        fromPrice: record.fromPrice,
        toPrice: record.toPrice,
        id: record.key,
      },
      () => this.handleOkModal(),
    );
  };

  render() {
    const {
      shippingTariffPromotions,
      shippingTariffPromotionCount,
      shippingTariffPromotionLoading,
    } = this.props;
    const dataSource = [];

    shippingTariffPromotions.map(item => {
      const obj = {
        fromPrice: item.fromPrice,
        toPrice: item.toPrice,
        amount: item.amount,
        stringFromPrice: item.stringFromPrice,
        stringToPrice: item.stringToPrice,
        stringAmount: `${item.stringAmount} ${
          item.amountType === 'Amount' ? TOMAN : PERCENT
        }`,
        amountType: item.amountType,
        published: item.published,
        key: item.id,
      };
      dataSource.push(obj);
      return null;
    });

    const { type, amount, fromPrice, toPrice } = this.state;

    const columns = [
      {
        title: FROM_PRICE,
        dataIndex: 'stringFromPrice',
        key: 'stringFromPrice',
      },
      { title: TO_PRICE, dataIndex: 'stringToPrice', key: 'stringToPrice' },
      {
        title: PRICE,
        dataIndex: 'stringAmount',
        key: 'stringAmount',
      },
      // {
      //   title: TYPE,
      //   dataIndex: 'amountType',
      //   key: 'amountType',
      //   render: (text, record) => (
      //     <div>
      //       {record.amountType === 'Amount' ? (
      //         <label>{TOMAN}</label>
      //       ) : (
      //         <label>{PERCENT}</label>
      //       )}
      //     </div>
      //   ),
      // },
      {
        title: STATUS,
        dataIndex: 'published',
        key: 'published',
        render: (text, record) => (
          <div>
            <CPSwitch
              onChange={value => this.changeStatus(value, record)}
              defaultChecked={record.published}
              unCheckedChildren={DEACTIVATE}
              checkedChildren={ACTIVE}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={
                ARE_YOU_SURE_WANT_TO_DELETE_THE_VB_SHIPPING_TARIFF_PROMOTION
              }
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to="/"
              className="edit_action"
              onClick={e => this.onEdit(e, record)}
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    return (
      <div className={s.mainContent}>
        <h3>{PROMOTION_LIST}</h3>
        <CardTable
          data={dataSource}
          count={shippingTariffPromotionCount}
          columns={columns}
          loading={shippingTariffPromotionLoading}
          onAddClick={this.showModal}
          hideSearchBox
        />

        <CPModal
          visible={this.state.visibleModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
        >
          <div className="modalContent">
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{FROM_PRICE}</label>
                <CPInputNumber
                  name="fromPrice"
                  label={FROM_PRICE}
                  value={fromPrice}
                  onChange={value => {
                    this.handelChangeInput('fromPrice', value);
                  }}
                  format="Currency"
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{TO_PRICE}</label>
                <CPInputNumber
                  name="toPrice"
                  label={TO_PRICE}
                  value={toPrice}
                  onChange={value => {
                    this.handelChangeInput('toPrice', value);
                  }}
                  format="Currency"
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{COMMISSION_TYPE}</label>
                <CPSelect
                  name="type"
                  value={type}
                  onChange={value => {
                    this.handelChangeInput('type', value);
                  }}
                >
                  <Option key="Percent" value="Percent">
                    {PERCENT}
                  </Option>
                  <Option key="Amount" value="Amount">
                    {TOMAN}
                  </Option>
                </CPSelect>
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{VALUE}</label>
                <CPInputNumber
                  name="amount"
                  label={VALUE}
                  value={amount}
                  onChange={value => {
                    this.handelChangeInput('amount', value);
                  }}
                  format={type === 'Percent' ? 'Percent' : 'Currency'}
                />
              </div>
            </div>
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  shippingTariffPromotions: state.vbShippingTariffPromotionList.data.items,
  shippingTariffPromotionCount: state.vbShippingTariffPromotionList.data.count,
  shippingTariffPromotionLoading: state.vbShippingTariffPromotionList.loading,
  shippingTariffs: state.vbShippingTariffList.data.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vbShippingTariffPromotionPostRequest:
        vbShippingTariffPromotionPostActions.vbShippingTariffPromotionPostRequest,
      vbShippingTariffPromotionPutRequest:
        vbShippingTariffPromotionPutActions.vbShippingTariffPromotionPutRequest,
      vbShippingTariffPromotionDeleteRequest:
        vbShippingTariffPromotionDeleteActions.vbShippingTariffPromotionDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ShippingTariffInfo));
