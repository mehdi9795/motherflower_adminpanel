import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import HotKeys from 'react-hot-keys';
import { Select, message } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ShippingTariffInfo.css';
import {
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  AGENT_NAME,
  ZONE,
  CATEGORY,
  COUNT,
  CITY,
  VALUE,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton/CPButton';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import {
  VendorBranchShippingTariffPromotionDto,
  VendorBranchShippingTariffsDto,
} from '../../../../dtos/samDtos';
import { getZoneApi } from '../../../../services/commonApi';
import { getDtoQueryString } from '../../../../utils/helper';
import { CityDto, ZoneDto } from '../../../../dtos/commonDtos';
import vbShippingTariffPostActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/Post';
import vbShippingTariffPutActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/Put';
import vbShippingTariffListActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/VBShippingTariffPromotionList';
import zoneListActions from '../../../../redux/common/actionReducer/zone/List';
import CPInputNumber from '../../../CP/CPInputNumber';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
  BaseListCRUDDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPMultiSelect from '../../../CP/CPMultiSelect';

const { Option } = Select;

class ShippingTariffInfo extends React.Component {
  static propTypes = {
    cities: PropTypes.arrayOf(PropTypes.object).isRequired,
    categories: PropTypes.arrayOf(PropTypes.object).isRequired,
    vendorBranches: PropTypes.arrayOf(PropTypes.object).isRequired,
    zones: PropTypes.arrayOf(PropTypes.object).isRequired,
    shippingTariffs: PropTypes.arrayOf(PropTypes.object).isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      categoryId:
        props.shippingTariffs && props.shippingTariffs[0]
          ? props.shippingTariffs[0].categoryDto.id.toString()
          : '',
      categoryIds: [],
      cityId:
        props.shippingTariffs && props.shippingTariffs[0]
          ? props.shippingTariffs[0].zoneDto.cityDto.id.toString()
          : props.cities && props.cities[0] && props.cities[0].id.toString(),
      vendorBranchId:
        props.shippingTariffs && props.shippingTariffs[0]
          ? props.shippingTariffs[0].vendorBranchDto.id.toString()
          : props.vendorBranches &&
            props.vendorBranches[0] &&
            props.vendorBranches[0].id.toString(),
      zoneId:
        props.shippingTariffs && props.shippingTariffs[0]
          ? props.shippingTariffs[0].zoneDto.id.toString()
          : '',
      zoneIds: [],
      count:
        props.shippingTariffs && props.shippingTariffs[0]
          ? props.shippingTariffs[0].qty
          : 0,
      amount:
        props.shippingTariffs && props.shippingTariffs[0]
          ? props.shippingTariffs[0].shippingAmount
          : 0,
      id:
        props.shippingTariffs && props.shippingTariffs[0]
          ? props.shippingTariffs[0].id.toString()
          : 0,
      isEdit:
        props.shippingTariffs &&
        props.shippingTariffs[0] &&
        props.shippingTariffs[0].id,
    };
    this.handelChangeInput = this.handelChangeInput.bind(this);
  }

  componentWillMount() {
    const { shippingTariffs } = this.props;
    if (shippingTariffs && shippingTariffs[0]) {
      const jsonList = new BaseGetDtoBuilder()
        .dto(
          new VendorBranchShippingTariffPromotionDto({
            vendorBranchShippingTariffDto: new VendorBranchShippingTariffsDto({
              id: shippingTariffs[0].id,
            }),
          }),
        )
        .buildJson();

      this.props.actions.vbShippingTariffPromotionListRequest(jsonList);
    }
  }

  saveForm = () => {
    const {
      vendorBranchId,
      categoryIds,
      zoneIds,
      categoryId,
      zoneId,
      count,
      amount,
      id,
    } = this.state;

    if (categoryIds.length === 0 && id === 0) {
      message.error('حداقل یک دسته بندی باید انتخاب شود.', 5);
      return;
    }
    if (zoneIds.length === 0 && id === 0) {
      message.error('حداقل یک ناحیه باید انتخاب شود.', 5);
      return;
    }

    /**
     * create dto for post vendor branch shipping tariff
     * @type {{id, dto, fromIdentity}}
     */

    if (id === 0) {
      const listCrud = [];
      categoryIds.map(categoryId => {
        zoneIds.map(zoneId => {
          const jsonCrud = new VendorBranchShippingTariffsDto({
            zoneDto: new ZoneDto({ id: zoneId }),
            vendorBranchDto: new VendorBranchDto({ id: vendorBranchId }),
            categoryDto: new CategoryDto({ id: categoryId }),
            qty: count,
            shippingAmount: amount,
            id,
          });
          listCrud.push(jsonCrud);
          return null;
        });
        return null;
      });

      const data = {
        data: new BaseListCRUDDtoBuilder().dtos(listCrud).build(),
        status: 'save',
      };
      this.props.actions.vbShippingTariffPostRequest(data);
    } else {
      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new VendorBranchShippingTariffsDto({
            zoneDto: new ZoneDto({ id: zoneId }),
            vendorBranchDto: new VendorBranchDto({ id: vendorBranchId }),
            categoryDto: new CategoryDto({ id: categoryId }),
            qty: count,
            shippingAmount: amount,
            id,
          }),
        )
        .build();

      const data = {
        data: jsonCrud,
        status: 'save',
      };
      this.props.actions.vbShippingTariffPutRequest(data);
    }
  };

  /**
   * save data with post and put - redirect to edit page
   */

  async handelChangeInput(inputName, value) {
    const { loginData } = this.props;

    this.setState({ [inputName]: value });
    if (inputName === 'cityId') {
      const jsonlist = new BaseGetDtoBuilder()
        .dto(
          new ZoneDto({
            cityDto: new CityDto({ id: value }),
            active: true,
          }),
        )
        .buildJson();

      const response = await getZoneApi(
        loginData.token,
        getDtoQueryString(jsonlist),
      );

      if (response.status === 200) {
        this.props.actions.zoneListSuccess(response.data);
        this.setState({
          zoneIds:
            response.data.items.length > 0
              ? response.data.items[0].id.toString()
              : 0,
        });
      }
    }
  }

  render() {
    const {
      categories,
      vendorBranches,
      cities,
      zones,
      shippingTariffs,
    } = this.props;
    const {
      vendorBranchId,
      categoryIds,
      zoneIds,
      cityId,
      count,
      amount,
      isEdit,
      categoryId,
      zoneId,
    } = this.state;

    const categoryArray = [];
    const cityArray = [];
    const zoneArray = [];
    const vendorBranchArray = [];
    const shippingTariffDataSource = [];

    shippingTariffs.map(item => {
      const obj = {
        category: item.categoryDto.name,
        vendorBranch: item.vendorBranchDto.name,
        zone: item.zoneDto.name,
        count: item.qty,
        amount: item.shippingAmount,
        stringAmount: item.stringShippingAmount,
        key: item.id,
        categoryIds: item.categoryDto.id.toString(),
        vendorBranchId: item.vendorBranchDto.id.toString(),
        zoneIds: item.zoneDto.id.toString(),
        cityId: item.zoneDto.cityDto.id.toString(),
      };
      shippingTariffDataSource.push(obj);
      return null;
    });

    /**
     * map category for comboBox Datasource
     */
    categories.map(item =>
      categoryArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map vendorBranch for comboBox Datasource
     */
    vendorBranches.map(item =>
      vendorBranchArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map city for comboBox Datasource
     */
    cities.map(item =>
      cityArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map zone for comboBox Datasource
     */
    zones.map(item =>
      zoneArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <div className="tabContent">
          <div className="row">
            <div className="col-lg-6 col-sm-12">
              <label>{CATEGORY}</label>
              {!isEdit ? (
                <CPMultiSelect
                  placeholder="انتخاب گروه محصول"
                  onChange={value =>
                    this.handelChangeInput('categoryIds', value)
                  }
                  value={categoryIds}
                >
                  {categoryArray}
                </CPMultiSelect>
              ) : (
                <CPSelect
                  value={categoryId}
                  onChange={value => {
                    this.handelChangeInput('categoryId', value);
                  }}
                >
                  {categoryArray}
                </CPSelect>
              )}
            </div>

            <div className="col-lg-6 col-sm-12">
              <label>{AGENT_NAME}</label>
              <CPSelect
                value={vendorBranchId}
                onChange={value => {
                  this.handelChangeInput('vendorBranchId', value);
                }}
              >
                {vendorBranchArray}
              </CPSelect>
            </div>

            <div className="col-lg-6 col-sm-12">
              <label>{CITY}</label>
              <CPSelect
                value={cityId}
                onChange={value => {
                  this.handelChangeInput('cityId', value);
                }}
              >
                {cityArray}
              </CPSelect>
            </div>

            <div className="col-lg-6 col-sm-12">
              <label>{ZONE}</label>
              {!isEdit ? (
              <CPMultiSelect
                placeholder="انتخاب ناحیه"
                onChange={value => this.handelChangeInput('zoneIds', value)}
                value={zoneIds}
              >
                {zoneArray}
              </CPMultiSelect>
              ):(
                <CPSelect
                  value={zoneId}
                  onChange={value => {
                    this.handelChangeInput('zoneId', value);
                  }}
                >
                  {zoneArray}
                </CPSelect>
              )}
            </div>

            <div className="col-lg-6 col-sm-12">
              <label>{VALUE}</label>
              <CPInputNumber
                name="amount"
                label={VALUE}
                value={amount}
                onChange={value => {
                  this.handelChangeInput('amount', value);
                }}
                format="Currency"
              />
            </div>
            <div className="col-lg-6 col-sm-12">
              <label>{COUNT}</label>
              <CPInputNumber
                name="count"
                label={COUNT}
                value={count}
                onChange={value => {
                  this.handelChangeInput('count', value);
                }}
              />
            </div>
          </div>
          <div className="text-left">
            <CPButton icon="save" type="primary" onClick={this.saveForm}>
              {FORM_SAVE}
            </CPButton>
          </div>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.secondaryLevelList.data.items,
  vendorBranches: state.vendorBranchList.data.items,
  cities: state.cityList.data.items,
  zones: state.zoneList.data.items,
  shippingTariffs: state.vbShippingTariffList.data.items,
  shippingTariffCount: state.vbShippingTariffList.data.count,
  shippingTariffLoading: state.vbShippingTariffList.loading,
  loginData: state.login.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      zoneListSuccess: zoneListActions.zoneListSuccess,
      vbShippingTariffPostRequest:
        vbShippingTariffPostActions.vbShippingTariffPostRequest,
      vbShippingTariffPutRequest:
        vbShippingTariffPutActions.vbShippingTariffPutRequest,
      vbShippingTariffPromotionListRequest:
        vbShippingTariffListActions.vbShippingTariffPromotionListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ShippingTariffInfo));
