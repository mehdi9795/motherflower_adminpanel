import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import HotKeys from 'react-hot-keys';
import { connect } from 'react-redux';
import { Select, message } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PromotionInfo.css';
import {
  ACTION_ON_CATEGORY,
  ACTION_ON_VENDOR_BRANCH,
  ACTIVE,
  DEACTIVATE,
  DISCOUNT_CENTER,
  DISCOUNT_CENTER_TYPE,
  DISCOUNT_SELLER,
  DISCOUNT_SELLER_TYPE,
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  FROM_DATE,
  INFO_BASIC,
  MAX_DISCOUNT_CENTER,
  MAX_DISCOUNT_SELLER,
  PERCENT,
  PROMOTION_NAME,
  PROMOTION_TYPE,
  STATUS,
  TO_DATE,
  TOMAN,
  PUBLIC_APPLY,
  FROM_HOUR,
  TO_HOUR,
  SEND_AFTER_SIGN_UP,
  PRODUCT_LIST_CATEGORY,
  ACCESS, INVITED_FRIEND,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton/CPButton';
import CPPanel from '../../../CP/CPPanel/CPPanel';
import CPInput from '../../../CP/CPInput/CPInput';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPSwitch from '../../../CP/CPSwitch/CPSwitch';
import CPPersianCalendar from '../../../CP/CPPersianCalendar';
import {
  DiscountCodeDto,
  PromotionDto,
  PromotionGroupActionDto, PromotionPermissionDto,
  VendorBranchPromotionDto,
} from '../../../../dtos/samDtos';
import { promotionDiscountCodeListRequest } from '../../../../redux/sam/action/promotion';
import promotionGroupActionPostActions from '../../../../redux/sam/actionReducer/promotion/PromotionGroupActionPost';
import promotionPostActions from '../../../../redux/sam/actionReducer/promotion/PromotionPost';
import promotionPutActions from '../../../../redux/sam/actionReducer/promotion/PromotionPut';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPMultiSelect from '../../../CP/CPMultiSelect';
import {RoleDto} from "../../../../dtos/identityDtos";

const { Option } = Select;

class PromotionInfo extends React.Component {
  static propTypes = {
    promotion: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    promotionTypes: PropTypes.arrayOf(PropTypes.object),
    roles: PropTypes.arrayOf(PropTypes.object),
  };
  static defaultProps = {
    promotion: null,
    promotionTypes: [],
    roles: [],
  };

  constructor(props) {
    super(props);
    const roleIds = [];
    if (props.promotion && props.promotion.promotionPermissionDtos && props.promotion.promotionPermissionDtos.length > 0)
      props.promotion.promotionPermissionDtos.map(item => roleIds.push(item.roleDto.id.toString()));
    this.state = {
      name: props.promotion ? props.promotion.name : '',
      promotionType: props.promotion
        ? props.promotion.promotionTypeId
        : props.promotionTypes[0].id,
      fromDate: props.promotion ? props.promotion.persianFromDateUtc : null, // moment(props.promotion.persianFromDate) : null,
      toDate: props.promotion ? props.promotion.persianToDateUtc : null, // moment(props.promotion.persianToDate) : null,
      active: props.promotion ? props.promotion.active : false,
      id: props.promotion ? props.promotion.id : '0',
      actionOnCategory: false,
      actionOnVendorBranch: false,
      vendorBranchAmountType: 'Percent',
      boAmountType: 'Percent',
      vendorBranchAmount: '',
      vendorBranchMaxAmount: '',
      boAmount: '',
      boMaxAmount: '',
      groupActionActive: false,
      fromHour: props.promotion ? props.promotion.fromHour.toString() : '7',
      toHour: props.promotion ? props.promotion.toHour.toString() : '8',
      sendAfterSignUp: props.promotion
        ? props.promotion.sendAfterSignUp
        : false,
      roleIds,
      usedForReagent: props.promotion ? props.promotion.usedForReagent : false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.promotion !== null) {
      this.setState({
        name: nextProps.promotion.name,
        promotionType: nextProps.promotion
          ? nextProps.promotion.promotionTypeId
          : 0,
        fromDate: nextProps.promotion
          ? nextProps.promotion.persianFromDateUtc
          : null,
        toDate: nextProps.promotion ? nextProps.promotion.persianToDateUtc : null,
        active: nextProps.promotion.active,
        id: nextProps.promotion.id,
      });
    }
  }

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  prepareContainer = () => {
    const {
      name,
      promotionType,
      fromDate,
      toDate,
      active,
      id,
      fromHour,
      toHour,
      sendAfterSignUp,
      roleIds,
      usedForReagent,
    } = this.state;
    const promotionPermissionDtoArray = [];
    roleIds.map(item => {
      promotionPermissionDtoArray.push(
        new PromotionPermissionDto({
          promotionDto: id !== 0 ? new PromotionDto({ id }) : undefined,
          roleDto: new RoleDto({ id: item }),
        }),
      );
      return null;
    })

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new PromotionDto({
          id,
          name,
          persianFromDateUtc: fromDate,
          persianToDateUtc: toDate,
          active,
          fromHour,
          toHour,
          promotionType,
          sendAfterSignUp: promotionType === 3 ? sendAfterSignUp : 0,
          promotionPermissionDtos: promotionPermissionDtoArray,
          usedForReagent,
        }),
      )
      .build();

    return jsonCrud;
  };

  /**
   * save data with post and put - redirect to list page
   */
  saveForm = () => {
    const { promotion } = this.props;
    const { fromDate, toDate } = this.state;

    if (fromDate && toDate)
      if (promotion && promotion.id) {
        const putData = {
          data: this.prepareContainer(),
          status: 'save',
          listDto: new BaseGetDtoBuilder()
            .dto(
              new VendorBranchPromotionDto({
                active: true,
              }),
            )
            .buildJson(),
        };
        this.props.actions.promotionPutRequest(putData);
      } else {
        const postData = {
          data: this.prepareContainer(),
          status: 'save',
          listDto: new BaseGetDtoBuilder()
            .dto(
              new VendorBranchPromotionDto({
                active: true,
              }),
            )
            .buildJson(),
        };
        this.props.actions.promotionPostRequest(postData);
      }
    else message.error('لطفا تاریخ ها را انتخاب کنید', 5);
  };

  /**
   * save data with post and put - redirect to edit page
   */
  saveFormAndContinue = () => {
    const { promotion } = this.props;
    const { fromDate, toDate } = this.state;

    if (fromDate && toDate)
      if (promotion && promotion.id) {
        const putData = {
          data: this.prepareContainer(),
          status: 'saveAndContinue',
        };
        this.props.actions.promotionPutRequest(putData);
      } else {
        const postData = {
          data: this.prepareContainer(),
          status: 'saveAndContinue',
        };
        this.props.actions.promotionPostRequest(postData);
      }
    else message.error('لطفا تاریخ ها را انتخاب کنید', 5);
  };

  saveGroupAction = () => {
    const {
      actionOnCategory,
      vendorBranchAmountType,
      boAmountType,
      vendorBranchAmount,
      vendorBranchMaxAmount,
      boAmount,
      boMaxAmount,
      groupActionActive,
      id,
    } = this.state;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new PromotionGroupActionDto({
          promotionId: id,
          actionOnCategories: actionOnCategory,
          vendorBranchAmountType,
          boAmountType,
          vendorBranchAmount,
          vendorBranchMaxAmount,
          boAmount,
          boMaxAmount,
          active: groupActionActive,
        }),
      )
      .build();

    /**
     * reload vendor branch promotion list after insert group action
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes}}
     */

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchPromotionDto({
          promotionDto: new PromotionDto({ id }),
        }),
      )
      .includes(['VendorBranch', 'Promotion'])
      .buildJson();

    const postData = {
      data: jsonCrud,
      listDto: jsonList,
    };

    this.props.actions.promotionGroupActionPostRequest(postData);
  };

  render() {
    const { promotion, promotionTypes, roles } = this.props;
    const isEdit = !!(promotion && promotion.id);

    const {
      name,
      promotionType,
      fromDate,
      toDate,
      active,
      actionOnVendorBranch,
      actionOnCategory,
      vendorBranchAmountType,
      boAmountType,
      vendorBranchAmount,
      vendorBranchMaxAmount,
      boAmount,
      boMaxAmount,
      groupActionActive,
      fromHour,
      toHour,
      sendAfterSignUp,
      roleIds,
      usedForReagent,
    } = this.state;

    const fromHourArray = [];
    const promotionTypesArray = [];
    const rolesArray = [];

    for (let i = 7; i < 23; i += 1) {
      fromHourArray.push(<Option key={`${i}`}>{i}</Option>);
    }

    promotionTypes.map(item =>
      promotionTypesArray.push(
        <Option key={item.id} value={item.id}>
          {item.name}
        </Option>,
      ),
    );

    if (roles && roles.length > 0)
      roles.map(item =>
        rolesArray.push(<Option key={item.id}>{item.name}</Option>),
      );

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <div className={promotion && promotion.expired ? 'disabled' : ''}>
          <div className="tabContent">
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <CPPanel header={INFO_BASIC}>
                  <div className="row">
                    <div className="col-lg-6 col-sm-12">
                      <label>{PROMOTION_NAME}</label>
                      <CPInput
                        name="name"
                        value={name}
                        placeholder={PROMOTION_NAME}
                        onChange={value =>
                          this.handelChangeInput('name', value.target.value)
                        }
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{PROMOTION_TYPE}</label>
                      <CPSelect
                        name="promotionType"
                        disabled={isEdit}
                        value={promotionType}
                        onChange={value =>
                          this.handelChangeInput('promotionType', value)
                        }
                      >
                        {promotionTypesArray}
                      </CPSelect>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-sm-12">
                      <label>{FROM_DATE}</label>
                      <CPPersianCalendar
                        className={
                          promotion && !promotion.editable ? 'disabled' : ''
                        }
                        placeholder={FROM_DATE}
                        // format="jYYYY/jMM/jDD"
                        onChange={(unix, formatted) =>
                          this.handelChangeInput('fromDate', formatted)
                        }
                        // id="datePicker"
                        preSelected={fromDate}
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{TO_DATE}</label>
                      <CPPersianCalendar
                        className={
                          promotion && !promotion.editable ? 'disabled' : ''
                        }
                        placeholder={TO_DATE}
                        onChange={(unix, formatted) =>
                          this.handelChangeInput('toDate', formatted)
                        }
                        id="datePicker"
                        preSelected={toDate}
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{FROM_HOUR}</label>
                      <CPSelect
                        name="fromHour"
                        disabled={promotion ? !promotion.editable : false}
                        defaultValue={fromHour}
                        onChange={value => {
                          this.handelChangeInput('fromHour', value);
                        }}
                      >
                        {fromHourArray}
                      </CPSelect>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{TO_HOUR}</label>
                      <CPSelect
                        disabled={promotion ? !promotion.editable : false}
                        name="toHour"
                        defaultValue={toHour}
                        onChange={value => {
                          this.handelChangeInput('toHour', value);
                        }}
                      >
                        {fromHourArray}
                      </CPSelect>
                    </div>
                    {(promotionType === 3 ||
                      promotionType === 6 ||
                      promotionType === 7) && (
                      <div className="col-lg-6 col-sm-12">
                        <div className="align-margin">
                          <label name="label">{ACCESS}</label>
                          <CPMultiSelect
                            placeholder={ACCESS}
                            onChange={value =>
                              this.handelChangeInput('roleIds', value)
                            }
                            value={roleIds}
                          >
                            {rolesArray}
                          </CPMultiSelect>
                        </div>
                      </div>
                    )}
                    <div className="col-lg-6 col-sm-12">
                      <div className="align-margin">
                        <label name="label">{STATUS}</label>
                        <CPSwitch
                          defaultChecked={active}
                          unCheckedChildren={DEACTIVATE}
                          checkedChildren={ACTIVE}
                          onChange={value =>
                            this.handelChangeInput('active', value)
                          }
                        />
                      </div>
                    </div>
                    {promotionType === 3 && (
                      <div className="col-lg-6 col-sm-12">
                        <div className="align-margin">
                          <CPSwitch
                            defaultChecked={sendAfterSignUp}
                            unCheckedChildren={SEND_AFTER_SIGN_UP}
                            checkedChildren={SEND_AFTER_SIGN_UP}
                            onChange={value =>
                              this.handelChangeInput('sendAfterSignUp', value)
                            }
                          >
                            {STATUS}
                          </CPSwitch>
                        </div>
                      </div>
                    )}
                    {promotionType === 7 && (
                      <div className="col-lg-6 col-sm-12">
                        <div className="align-margin">

                          <label name="label">{INVITED_FRIEND}</label>
                          <CPSwitch
                            defaultChecked={usedForReagent}
                            unCheckedChildren={DEACTIVATE}
                            checkedChildren={ACTIVE}
                            onChange={value =>
                              this.handelChangeInput('usedForReagent', value)
                            }
                          />
                        </div>
                      </div>
                    )}
                  </div>
                  <div className="text-left">
                    <CPButton icon="save" onClick={this.saveForm}>
                      {FORM_SAVE}
                    </CPButton>{' '}
                    <CPButton
                      icon="save"
                      className="btn-primary"
                      onClick={this.saveFormAndContinue}
                    >
                      {FORM_SAVE_AND_CONTINUE}
                    </CPButton>
                  </div>
                </CPPanel>
              </div>
              {false &&
                promotionType !== 'DiscountCode' && (
                  <div className="col-lg-6 col-sm-12">
                    <CPPanel header={PUBLIC_APPLY}>
                      <div className="row">
                        <div className="col-lg-6 col-sm-12">
                          <label>{ACTION_ON_VENDOR_BRANCH}</label>
                          <CPSwitch
                            defaultChecked={actionOnVendorBranch}
                            size="small"
                            onChange={value =>
                              this.handelChangeInput(
                                'actionOnVendorBranch',
                                value,
                              )
                            }
                          />
                        </div>
                      </div>
                      {actionOnVendorBranch && (
                        <div className="row">
                          <div className="col-lg-6 col-sm-12">
                            <label>{ACTION_ON_CATEGORY}</label>
                            <CPSwitch
                              defaultChecked={actionOnCategory}
                              size="small"
                              onChange={value =>
                                this.handelChangeInput(
                                  'actionOnCategory',
                                  value,
                                )
                              }
                            />
                          </div>
                        </div>
                      )}
                      {actionOnCategory &&
                        actionOnVendorBranch && (
                          <div>
                            <div className="row">
                              <div className="col-lg-6 col-sm-12">
                                <label>{DISCOUNT_SELLER}</label>
                                <CPInput
                                  placeholder={DISCOUNT_SELLER}
                                  value={vendorBranchAmount}
                                  onChange={value => {
                                    this.handelChangeInput(
                                      'vendorBranchAmount',
                                      value.target.value,
                                    );
                                  }}
                                />
                              </div>
                              <div className="col-lg-6 col-sm-12">
                                <label>{DISCOUNT_SELLER_TYPE}</label>
                                <CPSelect
                                  name="vendorBranchAmountType"
                                  defaultValue="Percent"
                                  onChange={value =>
                                    this.handelChangeInput(
                                      'vendorBranchAmountType',
                                      value,
                                    )
                                  }
                                >
                                  <Option key="Percent">{PERCENT}</Option>
                                  <Option key="Amount">{TOMAN}</Option>
                                </CPSelect>
                              </div>
                              {vendorBranchAmountType === 'Percent' && (
                                <div className="col-sm-12">
                                  <label>{MAX_DISCOUNT_SELLER}</label>
                                  <CPInput
                                    placeholder={MAX_DISCOUNT_SELLER}
                                    value={vendorBranchMaxAmount}
                                    onChange={value => {
                                      this.handelChangeInput(
                                        'vendorBranchMaxAmount',
                                        value.target.value,
                                      );
                                    }}
                                  />
                                </div>
                              )}
                            </div>
                            <div className="row">
                              <div className="col-lg-6 col-sm-12">
                                <label>{DISCOUNT_CENTER}</label>
                                <CPInput
                                  placeholder={DISCOUNT_CENTER}
                                  value={boAmount}
                                  onChange={value => {
                                    this.handelChangeInput(
                                      'boAmount',
                                      value.target.value,
                                    );
                                  }}
                                />
                              </div>
                              <div className="col-lg-6 col-sm-12">
                                <label>{DISCOUNT_CENTER_TYPE}</label>
                                <CPSelect
                                  name="boAmountType"
                                  defaultValue="Percent"
                                  onChange={value =>
                                    this.handelChangeInput(
                                      'boAmountType',
                                      value,
                                    )
                                  }
                                >
                                  <Option key="Percent">{PERCENT}</Option>
                                  <Option key="Amount">{TOMAN}</Option>
                                </CPSelect>
                              </div>
                              {boAmountType === 'Percent' && (
                                <div className="col-sm-12">
                                  <label>{MAX_DISCOUNT_CENTER}</label>
                                  <CPInput
                                    placeholder={MAX_DISCOUNT_CENTER}
                                    value={boMaxAmount}
                                    onChange={value => {
                                      this.handelChangeInput(
                                        'boMaxAmount',
                                        value.target.value,
                                      );
                                    }}
                                  />
                                </div>
                              )}
                            </div>
                            <div className="row">
                              <div className="col-lg-6 col-sm-12">
                                <CPSwitch
                                  defaultChecked={groupActionActive}
                                  unCheckedChildren={DEACTIVATE}
                                  checkedChildren={ACTIVE}
                                  onChange={value =>
                                    this.handelChangeInput(
                                      'groupActionActive',
                                      value,
                                    )
                                  }
                                >
                                  {STATUS}
                                </CPSwitch>
                              </div>
                            </div>
                          </div>
                        )}
                      {actionOnVendorBranch && (
                        <div className="text-left">
                          <CPButton icon="save" onClick={this.saveGroupAction}>
                            {FORM_SAVE}
                          </CPButton>{' '}
                        </div>
                      )}
                    </CPPanel>
                  </div>
                )}
            </div>
          </div>
        </div>
      </HotKeys>
    );
  }
}
const mapStateToProps = state => ({
  promotion: state.singlePromotion.data
    ? state.singlePromotion.data.items[0]
    : null,
  promotionTypes: state.promotionTypeList.data
    ? state.promotionTypeList.data.items
    : [],
  roles: state.role.data ? state.role.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      promotionPostRequest: promotionPostActions.promotionPostRequest,
      promotionPutRequest: promotionPutActions.promotionPutRequest,
      promotionGroupActionPostRequest:
        promotionGroupActionPostActions.promotionGroupActionPostRequest,
      promotionDiscountCodeListRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PromotionInfo));
