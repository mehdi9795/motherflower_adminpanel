import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select } from 'antd';
import moment from 'moment-jalaali';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CategoryPromotionDetails.css';
import {
  CATEGORY_NAME,
  DELETE_CONFIRM,
  DISCOUNT_CENTER,
  DISCOUNT_CENTER_TYPE,
  DISCOUNT_SELLER,
  DISCOUNT_SELLER_TYPE,
  FORM_SAVE,
  MAX_DISCOUNT_CENTER,
  MAX_DISCOUNT_SELLER,
  NO,
  PERCENT,
  SELECT_CATEGORY_FOR_PROMOTION,
  SET_USER_ROLE,
  STATUS,
  TOMAN,
  YES,
  CATEGORY,
  DEACTIVATE,
  ACTIVE,
  PRODUCT_LIST_CODE,
  PRODUCT_LIST_NAME,
  ACTIVE_IN_PROMOTION,
  CHANGE_STATUS_PRODUCT_IN_PROMOTION,
  PICKUP_DELIVERY_DAY_LIMIT,
} from '../../../../Resources/Localization';
import CPList from '../../../CP/CPList';

import CPPopConfirm from '../../../CP/CPPopConfirm';
import Link from '../../../Link';
import CPSwitch from '../../../CP/CPSwitch';
import {
  VendorBranchPromotionDetailDto,
  VendorBranchPromotionDto,
} from '../../../../dtos/samDtos';
import vendorBranchPromotionDetailsDeleteActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionDetailsDelete';
import vendorBranchPromotionDetailsPostActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionDetailsPost';
import vendorBranchPromotionDetailsPutActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionDetailsPut';
import CPModal from '../../../CP/CPModal';
import CPButton from '../../../CP/CPButton';
import CPTreeSelect from '../../../CP/CPTreeSelect';
import categoryListActions from '../../../../redux/catalog/actionReducer/category/CategoryList';
import { CategoryDto, ProductDto } from '../../../../dtos/catalogDtos';
import CPInput from '../../../CP/CPInput';
import CPSelect from '../../../CP/CPSelect';
import CPInputNumber from '../../../CP/CPInputNumber';
import { getCategoryPromotionDetailsApi } from '../../../../services/samApi';
import { getDtoQueryString } from '../../../../utils/helper';
import CPCheckbox from '../../../CP/CPCheckbox';
import CPPersianCalendar from '../../../CP/CPPersianCalendar';
import {
  BaseGetDtoBuilder,
  BaseListCRUDDtoBuilder,
  BaseListGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import { CATEGORY_PROMOTION_DETAILS_LIST_REQUEST } from '../../../../constants';

const { Option } = Select;

class CategoryPromotionDetails extends React.Component {
  static propTypes = {
    categoryPromotionDetailsLoading: PropTypes.bool,
    vendorBranchPromotionId: PropTypes.string,
    categoriesPromotionDetails: PropTypes.arrayOf(PropTypes.object),
    categoryPromotionDetailsCount: PropTypes.number,
    categories: PropTypes.arrayOf(PropTypes.object),
    loginData: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    loginData: {},
    categoryPromotionDetailsLoading: false,
    vendorBranchPromotionId: 0,
    categoriesPromotionDetails: [],
    categoryPromotionDetailsCount: 0,
    categories: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      visibleCategoryFullModal: false,
      visibleCategoryEditModal: false,
      selectedCategoryIds: [],
      vendorBranchAmountType: 'Percent',
      boAmountType: 'Percent',
      vendorBranchAmount: null,
      vendorBranchMaxAmount: null,
      boAmount: null,
      boMaxAmount: null,
      active: false,
      categoryPromotionDetailsId: '',
      visibleModalProduct: false,
      productCode: '',
      categoryRowId: 0,
      productName: '',
      productId: 0,
      productActive: false,
      errorMessage: '',
      limitDay: '',
    };
    this.updatePrice = this.updatePrice.bind(this);
    this.searchProduct = this.searchProduct.bind(this);
  }

  onDeleteCategoryPromotionDetails = id => {
    const { vendorBranchPromotionId } = this.props;
    /**
     * category promotion details list reloded
     */

    const jsonList = new BaseListGetDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Category',
        }),
      ])
      .includes(['Category'])
      .buildJson();

    const deleteData = {
      id,
      listDto: jsonList,
      type: CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsDeleteRequest(deleteData);
  };

  categoryPromotionDetailsAddClick = () => {
    const { categoriesPromotionDetails } = this.props;
    const categoryPromotionIds = [];

    categoriesPromotionDetails.map(item =>
      categoryPromotionIds.push(item.categoryDto.id),
    );

    /**
     * get all category for first time without category exist
     */

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new CategoryDto({
          makeTree: true,
          removeChildtoParentList: categoryPromotionIds,
        }),
      )
      .buildJson();
    this.props.actions.categoryListRequest(jsonList);

    this.setState({
      visibleCategoryFullModal: true,
      selectedCategoryIds: [],
      vendorBranchAmountType: 'Percent',
      boAmountType: 'Percent',
      vendorBranchAmount: null,
      vendorBranchMaxAmount: null,
      boAmount: null,
      boMaxAmount: null,
      active: false,
    });
  };

  handleCancelvendorBranchFullModal = () => {
    this.setState({
      visibleCategoryFullModal: false,
    });
  };

  handleCancelcategoryEditModal = () => {
    this.setState({
      visibleCategoryEditModal: false,
    });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });

    if (inputName === 'boAmountType') {
      this.setState({
        boAmount: null,
        boMaxAmount: null,
      });
    }
    if (inputName === 'vendorBranchAmountType') {
      this.setState({
        vendorBranchAmount: null,
        vendorBranchMaxAmount: null,
      });
    }
  };

  insertCategoryPromotionDetails = () => {
    const { vendorBranchPromotionId } = this.props;
    const {
      vendorBranchAmountType,
      boAmountType,
      vendorBranchAmount,
      vendorBranchMaxAmount,
      boAmount,
      boMaxAmount,
      active,
      selectedCategoryIds,
      limitDay,
    } = this.state;

    const vendorBranchPromotionDetails = [];

    selectedCategoryIds.map(item => {
      const json = new VendorBranchPromotionDetailDto({
        categoryDto: new CategoryDto({ id: item }),
        vendorBranchPromotionDto: new VendorBranchPromotionDto({
          id: vendorBranchPromotionId,
        }),
        active,
        boMaxAmount,
        boAmountType,
        boAmount,
        vendorBranchAmount,
        vendorBranchAmountType,
        vendorBranchMaxAmount,
        stringPickupDeliveryDeadline: limitDay !== '' ? limitDay : undefined,
        vendorBranchPromotionDetailType: 'Category',
      });
      vendorBranchPromotionDetails.push(json);
      return null;
    });

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos(vendorBranchPromotionDetails)
      .build();

    /**
     * category promotion details list reloded
     */

    const jsonList = new BaseListGetDtoBuilder()
      .dtos(
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Category',
        }),
      )
      .includes(['Category'])
      .buildJson();

    const postData = {
      data: jsonCrud,
      listDto: jsonList,
      type: CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPostRequest(postData);

    this.setState({ visibleCategoryFullModal: false });
  };

  updateCategoryPromotionDetails = () => {
    const { vendorBranchPromotionId, promotionType } = this.props;
    const {
      vendorBranchAmountType,
      boAmountType,
      vendorBranchAmount,
      vendorBranchMaxAmount,
      boAmount,
      boMaxAmount,
      active,
      categoryPromotionDetailsId,
      limitDay,
    } = this.state;

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          active,
          boMaxAmount,
          boAmountType,
          boAmount,
          vendorBranchAmount,
          vendorBranchAmountType,
          vendorBranchMaxAmount,
          stringPickupDeliveryDeadline: limitDay !== '' ? limitDay : undefined,
          id: categoryPromotionDetailsId,
        }),
      ])
      .build();

    /**
     * category promotion details list reloded
     */

    const jsonList = new BaseListGetDtoBuilder()
      .dtos(
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Category',
        }),
      )
      .includes(['Category'])
      .buildJson();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
      type: CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPutRequest(putData);

    this.setState({ visibleCategoryEditModal: false });
  };

  showCategoryEditModal(record) {
    this.setState({
      vendorBranchAmountType: record.vendorBranchAmountType,
      boAmountType: record.boAmountType,
      vendorBranchAmount: record.vendorBranchAmount,
      vendorBranchMaxAmount: record.vendorBranchMaxAmount,
      boAmount: record.bOAmount,
      boMaxAmount: record.bOMaxAmount,
      active: record.active,
      categoryPromotionDetailsId: record.key,
      visibleCategoryEditModal: true,
      limitDay: record.limitDay,
      // limitDay: moment(record.limitDay),
    });
  }

  async updatePrice(id) {
    const { vendorBranchPromotionId } = this.props;

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          applyJob: true,
          id,
        }),
      ])
      .build();

    /**
     * category promotion details list reloded
     */

    const jsonList = new BaseListGetDtoBuilder()
      .dtos(
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Category',
        }),
      )
      .includes(['Category'])
      .buildJson();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
      type: CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPutRequest(putData);
  }

  changeStatusVendorBranchCategoryPromotion = (value, record) => {
    const { vendorBranchPromotionId } = this.props;

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          vendorBranchAmountType: record.vendorBranchAmountType,
          boAmountType: record.boAmountType,
          vendorBranchAmount: record.vendorBranchAmount,
          vendorBranchMaxAmount: record.vendorBranchMaxAmount,
          boAmount: record.bOAmount,
          boMaxAmount: record.bOMaxAmount,
          active: value,
          id: record.key,
        }),
      ])
      .build();

    /**
     * category promotion details list reloded
     */
    const jsonList = new BaseListGetDtoBuilder()
      .dtos(
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Category',
        }),
      )
      .includes(['Category'])
      .buildJson();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
      type: CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPutRequest(putData);
  };

  showModalProduct = id => {
    this.setState({
      errorMessage: '',
      productName: '',
      productActive: false,
      productId: 0,
      visibleModalProduct: true,
      categoryRowId: id,
      productCode: '',
    });
  };

  handleCancelModalProduct = () => {
    this.setState({ visibleModalProduct: false });
  };

  async searchProduct() {
    const { loginData } = this.props;
    const { productCode, categoryRowId } = this.state;

    if (productCode) {
      const jsonList = new BaseListGetDtoBuilder()
        .dtos(
          new VendorBranchPromotionDetailDto({
            productDto: new ProductDto({ code: productCode }),
            id: categoryRowId,
          }),
        )
        .includes(['Category'])
        .buildJson();

      const response = await getCategoryPromotionDetailsApi(
        loginData.token,
        getDtoQueryString(jsonList),
      );

      if (response.status === 200) {
        this.setState({
          productName: response.data.items[0].productDto.name,
          productActive: response.data.items[0].active,
          productId: response.data.items[0].productDto.id,
        });
      } else {
        this.setState({
          errorMessage: response.data.errorMessage,
          productName: '',
          productActive: false,
          productId: 0,
        });
      }
    } else {
      this.setState({
        errorMessage: 'لطفا کد محصول را وارد کنید.',
        productName: '',
        productActive: false,
        productId: 0,
      });
    }
  }

  updateStatusCategoryPromotion = () => {
    const { vendorBranchPromotionId } = this.props;

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          productDto: new ProductDto({ id: this.state.productId }),
          id: this.state.categoryRowId,
          active: this.state.productActive,
        }),
      ])
      .build();

    /**
     * category promotion details list reloded
     */

    const jsonList = new BaseListGetDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Category',
        }),
      ])
      .includes(['Category'])
      .build();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
      type: CATEGORY_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPutRequest(putData);
    this.setState({ visibleModalProduct: false });
  };

  render() {
    const {
      categoryPromotionDetailsLoading,
      categoriesPromotionDetails,
      categoryPromotionDetailsCount,
      categories,
      promotionType,
    } = this.props;
    const {
      vendorBranchAmountType,
      boAmountType,
      vendorBranchAmount,
      vendorBranchMaxAmount,
      boAmount,
      boMaxAmount,
      active,
      selectedCategoryIds,
      productCode,
      productActive,
      productName,
      errorMessage,
      limitDay,
    } = this.state;

    const categoryPromotionDetailsDataSource = [];
    const columns = [
      { title: CATEGORY_NAME, dataIndex: 'categoryName', key: 'categoryName', width: 250 },
      {
        title: DISCOUNT_SELLER,
        dataIndex: 'discountSeller',
        key: 'discountSeller',
        width: 200,
      },
      {
        title: DISCOUNT_CENTER,
        dataIndex: 'districtCenter',
        key: 'districtCenter',
        width: 200,
      },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 100,
        render: (text, record) => (
          <div>
            <CPSwitch
              defaultChecked={record.active}
              checkedChildren={ACTIVE}
              unCheckedChildren={DEACTIVATE}
              disabled={record.expired}
              onChange={value => {
                this.changeStatusVendorBranchCategoryPromotion(value, record);
              }}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: '',
        width: 100,
        render: (text, record) => (
          <div>{record.expired && <label>منقضی شد</label>}</div>
        ),
      },
      {
        title: '',
        dataIndex: '',
        render: (text, record) => (
          <div>
            {record.applyJob &&
              promotionType !== 'DiscountCode' &&
              promotionType !== 'DiscountGroupCode' && (
                <CPButton onClick={() => this.updatePrice(record.key)}>
                  بروزرسانی
                </CPButton>
              )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: '',
        render: (text, record) => (
          <div>
            {record.applyJob &&
              promotionType !== 'DiscountCode' &&
              promotionType !== 'DiscountGroupCode' && (
                <CPButton onClick={() => this.showModalProduct(record.key)}>
                  محصولات
                </CPButton>
              )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            {!record.expired && (
              <CPPopConfirm
                title={DELETE_CONFIRM}
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() =>
                  this.onDeleteCategoryPromotionDetails(record.key)
                }
              >
                <Link to="#" className="delete_action">
                  <i className="cp-trash" />
                </Link>
              </CPPopConfirm>
            )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            {!record.expired && (
              <span
                onClick={() => this.showCategoryEditModal(record)}
                className="edit_action"
              >
                <i className="cp-edit" />
              </span>
            )}
          </div>
        ),
      },
    ];
    /**
     * maping categories Promotion Details for list data source
     */

    categoriesPromotionDetails.map(item => {
      let sellerAmountDescription = '';
      if (item.vendorBranchAmount > 0) {
        const value = item.vendorBranchAmount;
        if (item.vendorBranchAmountType === 'Amount') {
          sellerAmountDescription = `${
            item.stringVendorBranchAmount
          }  ${TOMAN} `;
        } else {
          sellerAmountDescription = `${value} ${PERCENT}`;
          if (item.vendorBranchMaxAmount > 0)
            sellerAmountDescription = `${sellerAmountDescription} 
              تا سقف
               ${item.stringVendorBranchMaxAmount} 
              تومان`;
        }
      }

      let centerAmountDescription = '';
      if (item.boAmount > 0) {
        const value = item.boAmount;
        if (item.boAmountType === 'Amount') {
          centerAmountDescription = `${item.stringBoAmount} ${TOMAN}`;
        } else {
          centerAmountDescription = `${value} ${PERCENT}`;
          if (item.boMaxAmount > 0)
            centerAmountDescription = `${centerAmountDescription} 
              تا سقف
               ${item.stringBoMaxAmount} 
              تومان`;
        }
      }

      const obj = {
        key: item.id,
        discountSeller: sellerAmountDescription,
        districtCenter: centerAmountDescription,
        active: item.active,
        vendorBranchAmountType: item.vendorBranchAmountType,
        boAmountType: item.boAmountType,
        bOAmount: item.boAmount,
        categoryName: item.categoryDto ? item.categoryDto.description : '',
        vendorBranchAmount: item.vendorBranchAmount,
        bOMaxAmount: item.boMaxAmount,
        vendorBranchMaxAmount: item.vendorBranchMaxAmount,
        applyJob: item.applyJob,
        expired: item.expired,
        limitDay: item.stringPickupDeliveryDeadline,
      };
      return categoryPromotionDetailsDataSource.push(obj);
    });

    return (
      <div>
        <CPList
          data={categoryPromotionDetailsDataSource}
          columns={columns}
          count={categoryPromotionDetailsCount}
          loading={categoryPromotionDetailsLoading}
          onAddClick={this.categoryPromotionDetailsAddClick}
          hideSearchBox
          hideAdd={
            categoriesPromotionDetails.length > 0
              ? categoriesPromotionDetails[0].expired
              : false
          }
        />
        <CPModal
          visible={this.state.visibleCategoryFullModal}
          handleCancel={this.handleCancelvendorBranchFullModal}
          title={SELECT_CATEGORY_FOR_PROMOTION}
          handleOk={this.insertCategoryPromotionDetails}
        >
          <div className="row">
            <div className="col-sm-12">
              <label>{CATEGORY}</label>
              <CPTreeSelect
                model={categories}
                checkBox
                onChange={value => {
                  this.handelChangeInput('selectedCategoryIds', value);
                }}
                values={selectedCategoryIds}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4 col-sm-12">
              <label>{DISCOUNT_SELLER_TYPE}</label>
              <CPSelect
                name="vendorBranchAmountType"
                value={vendorBranchAmountType}
                onChange={value =>
                  this.handelChangeInput('vendorBranchAmountType', value)
                }
              >
                <Option key="Percent">{PERCENT}</Option>
                <Option key="Amount">{TOMAN}</Option>
              </CPSelect>
            </div>
            <div className="col-lg-4 col-sm-12">
              <label>{DISCOUNT_SELLER}</label>
              <CPInputNumber
                value={vendorBranchAmount}
                onChange={value => {
                  this.handelChangeInput('vendorBranchAmount', value);
                }}
                format={
                  vendorBranchAmountType === 'Percent' ? 'Percent' : 'Currency'
                }
              />
            </div>
            {vendorBranchAmountType === 'Percent' && (
              <div className="col-lg-4 col-sm-12">
                <label>{MAX_DISCOUNT_SELLER}</label>
                <CPInputNumber
                  value={vendorBranchMaxAmount}
                  onChange={value => {
                    this.handelChangeInput('vendorBranchMaxAmount', value);
                  }}
                  format="Currency"
                />
              </div>
            )}
          </div>

          <div className="row">
            <div className="col-lg-4 col-sm-12">
              <label>{DISCOUNT_CENTER_TYPE}</label>
              <CPSelect
                name="boAmountType"
                value={boAmountType}
                onChange={value =>
                  this.handelChangeInput('boAmountType', value)
                }
              >
                <Option key="Percent">{PERCENT}</Option>
                <Option key="Amount">{TOMAN}</Option>
              </CPSelect>
            </div>
            <div className="col-lg-4 col-sm-12">
              <label>{DISCOUNT_CENTER}</label>
              <CPInputNumber
                value={boAmount}
                onChange={value => {
                  this.handelChangeInput('boAmount', value);
                }}
                format={boAmountType === 'Percent' ? 'Percent' : 'Currency'}
              />
            </div>
            {boAmountType === 'Percent' && (
              <div className="col-lg-4 col-sm-12">
                <label>{MAX_DISCOUNT_CENTER}</label>
                <CPInputNumber
                  value={boMaxAmount}
                  onChange={value => {
                    this.handelChangeInput('boMaxAmount', value);
                  }}
                  format="Currency"
                />
              </div>
            )}
          </div>

          <div className="row">
            <div className="col-lg-4 col-sm-12">
              <label>{PICKUP_DELIVERY_DAY_LIMIT}</label>
              <CPPersianCalendar
                placeholder={PICKUP_DELIVERY_DAY_LIMIT}
                onChange={(unix, formatted) =>
                  this.handelChangeInput('limitDay', formatted)
                }
                preSelected={limitDay}
              />
            </div>
            <div className="col-lg-4 col-sm-12">
              <CPSwitch
                defaultChecked={active}
                unCheckedChildren={DEACTIVATE}
                checkedChildren={ACTIVE}
                onChange={value => this.handelChangeInput('active', value)}
              >
                {STATUS}
              </CPSwitch>
            </div>
          </div>
        </CPModal>
        <CPModal
          visible={this.state.visibleCategoryEditModal}
          handleCancel={this.handleCancelcategoryEditModal}
          title={SET_USER_ROLE}
          showFooter={false}
        >
          <div>
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{DISCOUNT_SELLER}</label>
                <CPInputNumber
                  value={vendorBranchAmount}
                  onChange={value => {
                    this.handelChangeInput('vendorBranchAmount', value);
                  }}
                  format={
                    vendorBranchAmountType === 'Percent'
                      ? 'Percent'
                      : 'Currency'
                  }
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{DISCOUNT_SELLER_TYPE}</label>
                <CPSelect
                  name="vendorBranchAmountType"
                  value={vendorBranchAmountType}
                  onChange={value =>
                    this.handelChangeInput('vendorBranchAmountType', value)
                  }
                >
                  <Option key="Percent">{PERCENT}</Option>
                  <Option key="Amount">{TOMAN}</Option>
                </CPSelect>
              </div>
              {vendorBranchAmountType === 'Percent' && (
                <div className="col-lg-6 col-sm-12">
                  <label>{MAX_DISCOUNT_SELLER}</label>
                  <CPInputNumber
                    value={vendorBranchMaxAmount}
                    onChange={value => {
                      this.handelChangeInput('vendorBranchMaxAmount', value);
                    }}
                    format="Currency"
                  />
                </div>
              )}
            </div>
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{DISCOUNT_CENTER}</label>
                <CPInputNumber
                  value={boAmount}
                  onChange={value => {
                    this.handelChangeInput('boAmount', value);
                  }}
                  format={boAmountType === 'Percent' ? 'Percent' : 'Currency'}
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{DISCOUNT_CENTER_TYPE}</label>
                <CPSelect
                  name="boAmountType"
                  value={boAmountType}
                  onChange={value =>
                    this.handelChangeInput('boAmountType', value)
                  }
                >
                  <Option key="Percent">{PERCENT}</Option>
                  <Option key="Amount">{TOMAN}</Option>
                </CPSelect>
              </div>
              {boAmountType === 'Percent' && (
                <div className="col-lg-6 col-sm-12">
                  <label>{MAX_DISCOUNT_CENTER}</label>
                  <CPInputNumber
                    value={boMaxAmount}
                    onChange={value => {
                      this.handelChangeInput('boMaxAmount', value);
                    }}
                    format="Currency"
                  />
                </div>
              )}
            </div>
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{PICKUP_DELIVERY_DAY_LIMIT}</label>
                <CPPersianCalendar
                  placeholder={PICKUP_DELIVERY_DAY_LIMIT}
                  onChange={(unix, formatted) =>
                    this.handelChangeInput('limitDay', formatted)
                  }
                  value={limitDay}
                  preSelected={limitDay}
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{STATUS}</label>
                <CPSwitch
                  defaultChecked={active}
                  unCheckedChildren={DEACTIVATE}
                  checkedChildren={ACTIVE}
                  onChange={value => this.handelChangeInput('active', value)}
                >
                  {STATUS}
                </CPSwitch>
              </div>
            </div>
            <div className="row">
              <CPButton
                icon="save"
                type="primary"
                style={{ margin: '0 auto' }}
                onClick={this.updateCategoryPromotionDetails}
              >
                {FORM_SAVE}
              </CPButton>
            </div>
          </div>
        </CPModal>
        <CPModal
          visible={this.state.visibleModalProduct}
          handleCancel={this.handleCancelModalProduct}
          title={CHANGE_STATUS_PRODUCT_IN_PROMOTION}
          showFooter={false}
        >
          <div>
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{PRODUCT_LIST_CODE}</label>
                <CPInput
                  placeholder={PRODUCT_LIST_CODE}
                  value={productCode}
                  onChange={value => {
                    this.handelChangeInput('productCode', value.target.value);
                  }}
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  className={s.btnSearch}
                  onClick={this.searchProduct}
                />
              </div>
            </div>
            {productName ? (
              <div>
                <div className="row">
                  <div className="col-sm-12">
                    <label
                      className={s.detailProduct}
                    >{`${PRODUCT_LIST_NAME}: ${productName}`}</label>
                  </div>
                  <div className="col-sm-12">
                    <label className={s.detailProduct}>
                      {ACTIVE_IN_PROMOTION}{' '}
                    </label>
                    <CPCheckbox
                      checked={productActive}
                      onChange={value => {
                        this.handelChangeInput(
                          'productActive',
                          value.target.checked,
                        );
                      }}
                    />
                  </div>
                </div>
                <div className="row">
                  <CPButton
                    icon="save"
                    type="primary"
                    className={s.btnSubmit}
                    onClick={this.updateStatusCategoryPromotion}
                  >
                    {FORM_SAVE}
                  </CPButton>
                </div>
              </div>
            ) : (
              <label className={s.error}>{errorMessage}</label>
            )}
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  categoriesPromotionDetails: state.categoryPromotionDetailsList
    ? state.categoryPromotionDetailsList.data.items
    : [],
  categoryPromotionDetailsCount: state.categoryPromotionDetailsList.data
    ? state.categoryPromotionDetailsList.data.count
    : [],
  categoryPromotionDetailsLoading: state.categoryPromotionDetailsList.loading,
  categories: state.categoryList.data ? state.categoryList.data.items : [],
  loginData: state.login.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      categoryListRequest: categoryListActions.categoryListRequest,
      vendorBranchPromotionDetailsPostRequest:
        vendorBranchPromotionDetailsPostActions.vendorBranchPromotionDetailsPostRequest,
      vendorBranchPromotionDetailsPutRequest:
        vendorBranchPromotionDetailsPutActions.vendorBranchPromotionDetailsPutRequest,
      vendorBranchPromotionDetailsDeleteRequest:
        vendorBranchPromotionDetailsDeleteActions.vendorBranchPromotionDetailsDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CategoryPromotionDetails));
