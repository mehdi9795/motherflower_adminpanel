import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PromotionDiscountCode.css';
import {
  ACTIVE,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_DISCOUNT_CODE,
  CREATE_DATE,
  DEACTIVATE,
  DISCOUNT_CODE,
  DISCOUNT_CODE_COUNT,
  DISCOUNT_CODE_FREE_QUOTA,
  DISCOUNT_CODE_USED_QUOTA,
  FORM_SAVE,
  NO,
  STATUS,
  YES,
} from '../../../../Resources/Localization';
import CPList from '../../../CP/CPList';

import CPPopConfirm from '../../../CP/CPPopConfirm';
import Link from '../../../Link';
import CPSwitch from '../../../CP/CPSwitch';
import CPButton from '../../../CP/CPButton';
import CPInput from '../../../CP/CPInput';
import promotionDiscountCodeDelete from '../../../../redux/sam/actionReducer/promotion/PromotionDiscountCodeDelete';
import promotionDiscountCodePost from '../../../../redux/sam/actionReducer/promotion/PromotionDiscountCodePost';
import promotionDiscountCodePut from '../../../../redux/sam/actionReducer/promotion/PromotionDiscountCodePut';
import promotionDiscountCodeList from '../../../../redux/sam/actionReducer/promotion/PromotionDiscountCodeList';
import { DiscountCodeDto, PromotionDto } from '../../../../dtos/samDtos';
import CPCheckbox from '../../../CP/CPCheckbox';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPPagination from '../../../CP/CPPagination';

class PromotionDiscountCode extends React.Component {
  static propTypes = {
    promotionDiscountCodeLoading: PropTypes.bool,
    promotionDiscountCodes: PropTypes.arrayOf(PropTypes.object),
    promotionDiscountCodeCount: PropTypes.number,
    promotion: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    promotion: null,
    promotionDiscountCodeLoading: false,
    promotionDiscountCodeCount: 0,
    promotionDiscountCodes: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      code: '',
      active: true,
      persianSearchFromDate: '',
      persianSearchToDate: '',
      discountCodeCount: '0',
      changeStatusDiscountCode: false,
      discountCode: '',
      status: true,
      id: 0,
      isEdit: false,
      currentPage: 1,
      pageSize: 10,
    };
    this.insertDiscountCode = this.insertDiscountCode.bind(this);
  }

  componentWillMount() {
    this.getPromotion();
  }

  getPromotion = () => {
    const { promotion } = this.props;
    const { currentPage, pageSize } = this.state;

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DiscountCodeDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
      )
      .all(true)
      .pageIndex(currentPage - 1)
      .pageSize(pageSize)
      .buildJson();

    this.props.actions.promotionDiscountCodeListRequest(jsonList);
  };

  onDelete = id => {
    const { promotion } = this.props;
    /**
     * reload discount code after insert
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, removeChildtoParentList}}
     */

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DiscountCodeDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
      )
      .all(true)
      .buildJson();

    const deleteData = {
      id,
      listDto: jsonList,
    };

    this.props.actions.promotionDiscountCodeDeleteRequest(deleteData);
  };

  insertDiscountCode() {
    const { discountCodeCount, discountCode, status, id } = this.state;
    const { promotion } = this.props;

    /**
     * reload discount code after insert
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, removeChildtoParentList}}
     */

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DiscountCodeDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
      )
      .all(true)
      .buildJson();

    if (id === 0) {
      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new DiscountCodeDto({
            promotionDto: new PromotionDto({ id: promotion.id }),
            active: status,
            code: discountCode,
            freeQuota: discountCodeCount,
          }),
        )
        .build();

      const postData = {
        data: jsonCrud,
        listDto: jsonList,
      };

      this.props.actions.promotionDiscountCodePostRequest(postData);
    } else {
      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new DiscountCodeDto({
            active: status,
            code: discountCode,
            freeQuota: discountCodeCount,
            id,
          }),
        )
        .build();

      const putData = {
        data: jsonCrud,
        listDto: jsonList,
      };

      this.props.actions.promotionDiscountCodePutRequest(putData);
    }
  }

  changeStatusDiscountCode = (inputName, value, record) => {
    const { promotion } = this.props;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new DiscountCodeDto({
          active: value,
          id: record.key,
        }),
      )
      .build();
    /**
     * reload discount code after insert
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, removeChildtoParentList}}
     */

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DiscountCodeDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
      )
      .all(true)
      .buildJson();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
    };

    this.props.actions.promotionDiscountCodePutRequest(putData);
  };

  // <editor-fold dsc="CPPersianCalendar">
  changeFromDate = (unix, formatted) => {
    this.setState({
      persianSearchFromDate: formatted,
    });
  };

  changeToDate = (unix, formatted) => {
    this.setState({
      persianSearchToDate: formatted,
    });
  };
  // </editor-fold>

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  showDetail = (e, record) => {
    e.preventDefault();
    this.setState({
      isEdit: true,
      status: record.active,
      id: record.key,
      discountCode: record.code,
      discountCodeCount: record.freeQuota.toString(),
    });
  };

  onChangePagination = page => {
    this.setState({ currentPage: page }, () => this.getPromotion());
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () =>
      this.props.actions.userListRequest(this.prepareContainer()),
    );
  };

  render() {
    const {
      promotionDiscountCodeLoading,
      promotionDiscountCodes,
      promotionDiscountCodeCount,
    } = this.props;
    const { discountCodeCount, status, discountCode, isEdit, currentPage } = this.state;

    const columns = [
      {
        title: DISCOUNT_CODE,
        dataIndex: 'code',
        key: 'discountCode',
        width: 100,
      },
      {
        title: CREATE_DATE,
        dataIndex: 'createDate',
        key: 'createDate',
        width: 100,
      },
      {
        title: DISCOUNT_CODE_FREE_QUOTA,
        dataIndex: 'freeQuota',
        key: 'freeQuota',
        width: 150,
      },
      {
        title: DISCOUNT_CODE_USED_QUOTA,
        dataIndex: 'usedQuota',
        key: 'usedQuota',
        width: 150,
      },
      {
        title: '',
        dataIndex: 'operationOne1',
        width: 130,
        render: (text, record) => (
          <div>
            <CPSwitch
              disabled
              defaultChecked={record.active}
              checkedChildren={ACTIVE}
              unCheckedChildren={DEACTIVATE}
              onChange={value => {
                this.changeStatusDiscountCode(
                  'changeStatusDiscountCode',
                  value,
                  record,
                );
              }}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_DISCOUNT_CODE}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <Link to="#" className="delete_action">
                <i className="cp-trash" />
              </Link>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to="#"
              className="edit_action"
              onClick={e => this.showDetail(e, record)}
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    const discountCodeArray = [];

    if (promotionDiscountCodes) {
      promotionDiscountCodes.map(item => {
        const obj = {
          key: item.id,
          active: item.active,
          code: item.code,
          createDate: item.persianCreatedOnUtc,
          freeQuota: item.freeQuota,
          usedQuota: item.usedQuota,
        };
        discountCodeArray.push(obj);
        return null;
      });
    }
    return (
      <div>
        <CPList
          data={discountCodeArray}
          columns={columns}
          count={promotionDiscountCodeCount}
          loading={promotionDiscountCodeLoading}
          hideAdd
          pagination={false}
          hideSearchBox
        />
        <CPPagination
          total={promotionDiscountCodeCount}
          current={currentPage}
          onChange={this.onChangePagination}
          onShowSizeChange={this.onShowSizeChange}
          defaultPageSize={10}
          pageSizeOptions={['10', '20', '30', '50', '80', '100']}
        />

        {(discountCodeArray.length === 0 || isEdit) && (
          <div>
            <div className="row">
              <div className="col-lg-4 col-sm-12">
                <label>{DISCOUNT_CODE}</label>
                <CPInput
                  disabled={isEdit}
                  placeholder={DISCOUNT_CODE}
                  value={discountCode}
                  onChange={value => {
                    this.handelChangeInput('discountCode', value.target.value);
                  }}
                />
              </div>
              <div className="col-lg-4 col-sm-12">
                <label>{DISCOUNT_CODE_FREE_QUOTA}</label>
                <CPInput
                  placeholder={DISCOUNT_CODE_COUNT}
                  value={discountCodeCount}
                  onChange={value => {
                    this.handelChangeInput(
                      'discountCodeCount',
                      value.target.value,
                    );
                  }}
                />
              </div>
              <div className="col-md-4 col-sm-12">
                <label>{STATUS}</label>
                <CPCheckbox
                  checked={status}
                  onChange={value => {
                    this.handelChangeInput('status', value.target.checked);
                  }}
                />
              </div>
            </div>

            <div className="row">
              <div className="col-sm-12 align-margin">
                <CPButton
                  icon="save"
                  type="primary"
                  onClick={this.insertDiscountCode}
                >
                  {FORM_SAVE}
                </CPButton>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  promotion: state.singlePromotion.data
    ? state.singlePromotion.data.items[0]
    : null,
  promotionDiscountCodes: state.promotionDiscountCodeList.data
    ? state.promotionDiscountCodeList.data.items
    : null,
  promotionDiscountCodeCount: state.promotionDiscountCodeList.data
    ? state.promotionDiscountCodeList.data.count
    : null,
  promotionDiscountCodeLoading: state.promotionDiscountCodeList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      promotionDiscountCodeListRequest:
        promotionDiscountCodeList.promotionDiscountCodeListRequest,
      promotionDiscountCodePostRequest:
        promotionDiscountCodePost.promotionDiscountCodePostRequest,
      promotionDiscountCodeDeleteRequest:
        promotionDiscountCodeDelete.promotionDiscountCodeDeleteRequest,
      promotionDiscountCodePutRequest:
        promotionDiscountCodePut.promotionDiscountCodePutRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PromotionDiscountCode));
