import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PromotionForm.css';
import {
  DISCOUNT_CODE,
  DISCOUNT_CODE_WALLET,
  PROMOTION_DETAILS,
  PROMOTION_INFO,
} from '../../../../Resources/Localization';
import PromotionInfo from '../PromotionInfo/PromotionInfo';
import PromotionDetails from '../PromotionDetails/PromotionDetails';
import PromotionDiscountCode from '../PromotionDiscountCode';
import PromotionDiscountGroupCode from '../PromotionDiscountGroupCode';

class PromotionForm extends React.Component {
  static propTypes = {
    promotion: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    promotion: null,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    const { promotion } = this.props;

    const isEdit = promotion && promotion.id;

    return (
      <div className={s.root}>
        <Tabs>
          <TabList>
            <Tab>{PROMOTION_INFO}</Tab>
            {isEdit &&
              promotion.promotionType !== 'DiscountWalletCode' && (
                <Tab>{PROMOTION_DETAILS}</Tab>
              )}
            {isEdit &&
              promotion.promotionType === 'DiscountCode' && (
                <Tab>{DISCOUNT_CODE}</Tab>
              )}
            {isEdit &&
              (promotion.promotionType === 'DiscountGroupCode' ||
                promotion.promotionType === 'DiscountWalletCode') && (
                <Tab>
                  {promotion.promotionType === 'DiscountGroupCode'
                    ? DISCOUNT_CODE
                    : DISCOUNT_CODE_WALLET}
                </Tab>
              )}
          </TabList>
          <TabPanel>
            <PromotionInfo />
          </TabPanel>
          {isEdit &&
            promotion.promotionType !== 'DiscountWalletCode' && (
              <TabPanel>
                <PromotionDetails />
              </TabPanel>
            )}
          {isEdit &&
            promotion.promotionType === 'DiscountCode' && (
              <TabPanel>
                <PromotionDiscountCode />
              </TabPanel>
            )}
          {isEdit &&
            (promotion.promotionType === 'DiscountGroupCode' ||
              promotion.promotionType === 'DiscountWalletCode') && (
              <TabPanel>
                <PromotionDiscountGroupCode />
              </TabPanel>
            )}
        </Tabs>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  promotion: state.singlePromotion.data
    ? state.singlePromotion.data.items[0]
    : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PromotionForm));
