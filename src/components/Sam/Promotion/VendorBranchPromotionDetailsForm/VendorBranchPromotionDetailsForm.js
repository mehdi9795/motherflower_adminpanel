import React from 'react';
import PropTypes from 'prop-types';
import { Tabs } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorBranchPromotionDetailsForm.css';
import {
  PRODUCT_LIST_CATEGORY,
  PRODUCTS,
} from '../../../../Resources/Localization';
import CategoryPromotionDetails from '../CategoryPromotionDetails';
import ProductPromotionDetails from '../ProductPromotionDetails';
import childCategoryListActions from '../../../../redux/catalog/actionReducer/category/ChildCategoryList';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import productTypesListActions from '../../../../redux/catalog/actionReducer/productType/List';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import {BaseGetDtoBuilder} from "../../../../dtos/dtoBuilder";

const { TabPane } = Tabs;

class VendorBranchPromotionDetailsForm extends React.Component {
  static propTypes = {
    query: PropTypes.objectOf(PropTypes.object).isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  componentWillMount() {
    /**
     * get all product types for first time
     */
    this.props.actions.productTypesListRequest(new BaseGetDtoBuilder().all(true).buildJson());

    /**
     * get all child categories for first time
     */
    this.props.actions.childCategoryListRequest(new BaseGetDtoBuilder().dto(new CategoryDto({
      justChild: true,
      published: true,
    })).buildJson());

    /**
     * get all vendor branch for first time
     */
    this.props.actions.vendorBranchListRequest(new BaseGetDtoBuilder().all(true).buildJson());
  }

  render() {
    const { query } = this.props;
    return (
      <div className={s.root}>
        <Tabs animated>
          {query.promotionType !== 'DiscountSpecialProductDelivery' &&
            query.promotionType !== 'DiscountSpecialProductPickup' && (
              <TabPane tab={PRODUCT_LIST_CATEGORY} key="1">
                <CategoryPromotionDetails
                  vendorBranchPromotionId={query.id}
                  promotionType={query.promotionType}
                />
              </TabPane>
            )}
          <TabPane tab={PRODUCTS} key="2">
            <ProductPromotionDetails
              vendorBranchPromotionId={query.id}
              vendorBranchId={query.vendorBranchId}
              promotionType={query.promotionType}
            />
          </TabPane>
        </Tabs>
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      childCategoryListRequest:
        childCategoryListActions.childCategoryListRequest,
      productTypesListRequest: productTypesListActions.productTypesListRequest,
      vendorBranchListRequest: vendorBranchListActions.vendorBranchListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchPromotionDetailsForm));
