import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductPromotionDetails.css';
import {
  ACTIVE,
  AGENT_NAME,
  ALL_TYPES,
  ALL_VENDORS,
  CATEGORY_NAME,
  DEACTIVATE,
  DELETE_CONFIRM,
  DISCOUNT_CENTER,
  DISCOUNT_CENTER_TYPE,
  DISCOUNT_SELLER,
  DISCOUNT_SELLER_TYPE,
  MAX_DISCOUNT_CENTER,
  MAX_DISCOUNT_SELLER,
  NO,
  PICKUP_DELIVERY_DAY_LIMIT,
  PERCENT,
  PRODUCT_LIST_CATEGORY,
  PRODUCT_LIST_CODE,
  PRODUCT_LIST_NAME,
  PRODUCT_PRODUCT_TYPE,
  SELECT_PRODUCT_FOR_PROMOTION,
  SET_USER_ROLE,
  STATUS,
  TOMAN,
  YES,
  PRODUCT_LIST_CODE_VENDOR,
  SELECT,
  CONFIRMATION,
  ID,
} from '../../../../Resources/Localization';
import CPList from '../../../CP/CPList';

import CPPopConfirm from '../../../CP/CPPopConfirm';
import CPSwitch from '../../../CP/CPSwitch';
import {
  VendorBranchPromotionDetailDto,
  VendorBranchPromotionDto,
} from '../../../../dtos/samDtos';
import vendorBranchPromotionDetailsDeleteActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionDetailsDelete';
import vendorBranchPromotionDetailsPostActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionDetailsPost';
import vendorBranchPromotionDetailsPutActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionDetailsPut';
import CPModal from '../../../CP/CPModal';
import CPButton from '../../../CP/CPButton';
import childCategoryListActions from '../../../../redux/catalog/actionReducer/category/ChildCategoryList';
import productListActions from '../../../../redux/catalog/actionReducer/product/ProductList';
import {
  CategoryDto,
  ProductCategoryDto,
  ProductDto,
  ProductSpecificationAttributeDto,
  ProductTypeDto,
} from '../../../../dtos/catalogDtos';
import CPInput from '../../../CP/CPInput';
import CPSelect from '../../../CP/CPSelect';
import { AgentDto, VendorBranchDto } from '../../../../dtos/vendorDtos';
import CPMultiSelect from '../../../CP/CPMultiSelect';
import CPInputNumber from '../../../CP/CPInputNumber';
import CPPanel from '../../../CP/CPPanel';
import CPPersianCalendar from '../../../CP/CPPersianCalendar';
import {
  BaseGetDtoBuilder,
  BaseListCRUDDtoBuilder,
  BaseListGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import { PRODUCT_PROMOTION_DETAILS_LIST_REQUEST } from '../../../../constants';
import CPPagination from '../../../CP/CPPagination';
import { treeModel } from '../../../../utils/model';
import CPTree from '../../../CP/CPTree';
import CPAvatar from '../../../CP/CPAvatar';
import { getProductSpecificationAttributeApi } from '../../../../services/catalogApi';
import {
  getDtoQueryString,
  pressEnterAndCallApplySearch,
} from '../../../../utils/helper';
import CPCarousel from '../../../CP/CPCarousel';

const { Option } = Select;

class ProductPromotionDetails extends React.Component {
  static propTypes = {
    productPromotionDetailsLoading: PropTypes.bool,
    vendorBranchPromotionId: PropTypes.string,
    productsPromotionDetails: PropTypes.arrayOf(PropTypes.object),
    productPromotionDetailsCount: PropTypes.number,
    products: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    vendorBranchId: PropTypes.string,
    promotionType: PropTypes.string,
    productCount: PropTypes.number,
    productLoading: PropTypes.bool,
    categoryChildren: PropTypes.arrayOf(PropTypes.object),
    productTypes: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
  };
  static defaultProps = {
    productsPromotionDetails: [],
    productPromotionDetailsCount: 0,
    productCount: 0,
    products: [],
    categoryChildren: [],
    productTypes: [],
    vendorBranches: [],
    productPromotionDetailsLoading: false,
    productLoading: false,
    vendorBranchPromotionId: '0',
    vendorBranchId: '0',
    promotionType: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      visibleProductFullModal: false,
      visibleProductEditModal: false,
      selectedProductIds: [],
      vendorBranchAmountType: 'Percent',
      boAmountType: 'Percent',
      vendorBranchAmount: null,
      vendorBranchMaxAmount: null,
      boAmount: null,
      boMaxAmount: null,
      active: false,
      categoryPromotionDetailsId: '',
      vendorId: props.vendorBranchId,
      productTypeId: '0',
      childCategorySelected: [],
      name: ``,
      code: ``,
      statusProduct: true,
      limitDay: '',
      pageSize: 10,
      currentPage: 1,
      visibleCategoryModal: false,
      categoryArray: [],
      categorySelectedIds: [],
      categorySelectedArray: [],
      visibleImageModal: false,
      dataImage: [],
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentWillMount() {
    /**
     * get all child categories for search product list popup
     */
    const jsonCategoryDto = new BaseGetDtoBuilder()
      .dto(new CategoryDto({ makeTree: true, published: true }))
      .buildJson();
    this.props.actions.childCategoryListRequest(jsonCategoryDto);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  onDeleteProductPromotionDetails = id => {
    const { vendorBranchPromotionId } = this.props;
    /**
     * category promotion details list reloded
     */
    const jsonList = new BaseListGetDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Product',
        }),
      ])
      .includes(['Product'])
      .buildJson();

    const deleteData = {
      id,
      listDto: jsonList,
      type: PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsDeleteRequest(deleteData);
  };

  onSelectedProducts = selectedRowKeys => {
    this.setState({ selectedProductIds: selectedRowKeys });
  };

  productPromotionDetailsAddClick = () => {
    const { vendorBranchId, productsPromotionDetails } = this.props;
    const productPromotionIds = [];
    productsPromotionDetails.map(item =>
      productPromotionIds.push(item.productDto.id),
    );

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new ProductDto({
          published: true,
          vendorDto: new AgentDto({
            vendorBranchDtos: [new VendorBranchDto({ id: vendorBranchId })],
          }),
        }),
      )
      .notExpectedIds(productPromotionIds)
      .includes([
        'productCategoryDtos',
        'vendorDto',
        'vendorBranches',
        'imageDtos',
      ])
      .pageSize(10)
      .pageIndex(0)
      .buildJson();
    this.props.actions.productListRequest(jsonList);

    this.setState({
      visibleProductFullModal: true,
      selectedProductIds: [],
      vendorBranchAmountType: 'Percent',
      boAmountType: 'Percent',
      vendorBranchAmount: null,
      vendorBranchMaxAmount: null,
      boAmount: null,
      boMaxAmount: null,
      active: false,
      productTypeId: '0',
      childCategorySelected: [],
      name: ``,
      code: ``,
      statusProduct: true,
    });
  };

  handleCancelvendorBranchFullModal = () => {
    this.setState({
      visibleProductFullModal: false,
    });
  };

  handleCancelproductEditModal = () => {
    this.setState({
      visibleProductEditModal: false,
    });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });

    if (inputName === 'boAmountType') {
      this.setState({
        boAmount: null,
        boMaxAmount: null,
      });
    }
    if (inputName === 'vendorBranchAmountType') {
      this.setState({
        vendorBranchAmount: null,
        vendorBranchMaxAmount: null,
      });
    }
  };

  showImageModal = (image, key) => {
    const dataImage = [];
    const obj = {
      small: `${image}`,
      large: `${image}`,
      key: image,
      // srcSet: srcSetArray, TODO: It was commented on the error
    };
    dataImage.push(obj);
    // dataImage.push(obj);
    this.setState({ visibleImageModal: true, dataImage });
  };

  handleCancelImageModal = () => {
    this.setState({ visibleImageModal: false });
  };

  /**
   * chnage child category value when select option of comboBox category
   * @param value
   */
  // categoryChange = value => {
  //   this.setState({
  //     childCategorySelected: value,
  //   });
  // };

  categoryChange = value => {
    this.setState({
      categorySelectedIds: value,
    });
  };

  /**
   * for search product
   * @param value
   */
  async submitSearch() {
    this.props.actions.productListRequest(this.prepareContainer());
  }

  /**
   * for submit search in product
   */
  prepareContainer = () => {
    const {
      productTypeId,
      vendorId,
      statusProduct,
      name,
      code,
      categorySelectedIds,
      currentPage,
      pageSize,
    } = this.state;
    const productCategoryDtos = [];

    categorySelectedIds.map(categoryId => {
      productCategoryDtos.push(
        new ProductCategoryDto({
          categoryDto: new CategoryDto({
            id: categoryId,
          }),
        }),
      );
      return null;
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new ProductDto({
          productTypeDto: new ProductTypeDto({ id: productTypeId }),
          vendorDto: new AgentDto({
            vendorBranchDtos: [new VendorBranchDto({ id: vendorId })],
          }),
          published: statusProduct,
          productCategoryDtos,
          name,
          id: code,
        }),
      )
      .includes(['vendorDto', 'vendorBranches', 'productCategoryDtos'])
      .pageSize(pageSize)
      .pageIndex(currentPage - 1)
      .buildJson();
    return jsonList;
  };

  insertProductPromotionDetails = () => {
    const { vendorBranchPromotionId } = this.props;
    const {
      vendorBranchAmountType,
      boAmountType,
      vendorBranchAmount,
      vendorBranchMaxAmount,
      boAmount,
      boMaxAmount,
      active,
      selectedProductIds,
      limitDay,
    } = this.state;

    const vendorBranchPromotionDetails = [];

    selectedProductIds.map(item => {
      const json = new VendorBranchPromotionDetailDto({
        productDto: new ProductDto({ id: item }),
        vendorBranchPromotionDto: new VendorBranchPromotionDto({
          id: vendorBranchPromotionId,
        }),
        active,
        boMaxAmount,
        boAmountType,
        boAmount,
        vendorBranchAmount,
        vendorBranchAmountType,
        vendorBranchMaxAmount,
        stringPickupDeliveryDeadline: limitDay !== '' ? limitDay : undefined,
        vendorBranchPromotionDetailType: 'Product',
      });
      vendorBranchPromotionDetails.push(json);
      return null;
    });

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos(vendorBranchPromotionDetails)
      .build();

    /**
     * product promotion details list reloded
     */

    const jsonList = new BaseListGetDtoBuilder()
      .dtos(
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Product',
        }),
      )
      .includes(['Product'])
      .buildJson();

    const postData = {
      data: jsonCrud,
      listDto: jsonList,
      type: PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPostRequest(postData);

    this.setState({ visibleProductFullModal: false });
  };

  updateCategoryPromotionDetails = () => {
    const { vendorBranchPromotionId } = this.props;
    const {
      vendorBranchAmountType,
      boAmountType,
      vendorBranchAmount,
      vendorBranchMaxAmount,
      boAmount,
      boMaxAmount,
      active,
      categoryPromotionDetailsId,
      limitDay,
    } = this.state;

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          active,
          boMaxAmount,
          boAmountType,
          boAmount,
          vendorBranchAmount,
          vendorBranchAmountType,
          vendorBranchMaxAmount,
          stringPickupDeliveryDeadline: limitDay !== '' ? limitDay : undefined,
          id: categoryPromotionDetailsId,
        }),
      ])
      .build();

    /**
     * product promotion details list reloded
     */
    const jsonList = new BaseListGetDtoBuilder()
      .dtos(
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Product',
        }),
      )
      .includes(['Product'])
      .buildJson();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
      type: PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPutRequest(putData);

    this.setState({ visibleProductEditModal: false });
  };

  showProductEditModal(record) {
    this.setState({
      vendorBranchAmountType: record.vendorBranchAmountType,
      boAmountType: record.boAmountType,
      vendorBranchAmount: record.vendorBranchAmount,
      vendorBranchMaxAmount: record.vendorBranchMaxAmount,
      boAmount: record.bOAmount,
      boMaxAmount: record.bOMaxAmount,
      active: record.active,
      categoryPromotionDetailsId: record.key,
      limitDay: record.limitDay, // moment(record.limitDay),
      visibleProductEditModal: true,
    });
  }

  async updatePrice(id) {
    const { vendorBranchPromotionId } = this.props;

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          applyJob: true,
          id,
        }),
      ])
      .build();
    /**
     * product promotion details list reloded
     */
    const jsonList = new BaseListGetDtoBuilder()
      .dtos(
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Product',
        }),
      )
      .includes(['Product'])
      .buildJson();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
      type: PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPutRequest(putData);
  }

  changeStatusVendorBranchProductPromotion = (value, record) => {
    const { vendorBranchPromotionId } = this.props;

    const jsonCrud = new BaseListCRUDDtoBuilder()
      .dtos([
        new VendorBranchPromotionDetailDto({
          vendorBranchAmountType: record.vendorBranchAmountType,
          boAmountType: record.boAmountType,
          vendorBranchAmount: record.vendorBranchAmount,
          vendorBranchMaxAmount: record.vendorBranchMaxAmount,
          boAmount: record.bOAmount,
          boMaxAmount: record.bOMaxAmount,
          active: value,
          id: record.key,
        }),
      ])
      .build();
    /**
     * product promotion details list reloded
     */
    const jsonList = new BaseListGetDtoBuilder()
      .dtos(
        new VendorBranchPromotionDetailDto({
          vendorBranchPromotionDto: new VendorBranchPromotionDto({
            id: vendorBranchPromotionId,
          }),
          vendorBranchPromotionDetailType: 'Product',
        }),
      )
      .includes(['Product'])
      .buildJson();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
      type: PRODUCT_PROMOTION_DETAILS_LIST_REQUEST,
    };

    this.props.actions.vendorBranchPromotionDetailsPutRequest(putData);
  };

  onChangePagination = page => {
    this.setState({ currentPage: page }, () =>
      this.props.actions.productListRequest(this.prepareContainer()),
    );
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () =>
      this.props.actions.productListRequest(this.prepareContainer()),
    );
  };

  traverseTree(node) {
    if (node.children && node.children.length > 0) {
      let tempItem = {};
      node.children.map(item => {
        tempItem = item;
        tempItem.link = `/category/edit/${item.id}`;
        return this.traverseTree(tempItem);
      });
    }
  }

  showCategoryModal = () => {
    this.setState({
      visibleCategoryModal: true,
      categoryIds: this.state.categorySelectedIds,
    });
  };

  handleCancelCategoryModal = () => {
    this.setState({ visibleCategoryModal: false });
  };

  handleOkCategoryModal = () => {
    const { categoryIds, categoryArray } = this.state;
    const ids = [];
    categoryIds.map(item => ids.push(item.split('*')[0]));
    this.setState({
      visibleCategoryModal: false,
      categorySelectedArray: categoryArray,
      categorySelectedIds: ids,
    });
  };

  onCheckCategory = (checkedKeys, info) => {
    const categories = [];
    info.checkedNodes.map(item =>
      categories.push(
        <Option key={item.key.split('*')[0]}>{item.key.split('*')[1]}</Option>,
      ),
    );
    this.setState({ categoryIds: checkedKeys, categoryArray: categories });
  };

  render() {
    const {
      productPromotionDetailsLoading,
      productsPromotionDetails,
      productPromotionDetailsCount,
      products,
      productCount,
      productLoading,
      categoryChildren,
      productTypes,
      vendorBranches,
      promotionType,
    } = this.props;
    const {
      vendorBranchAmountType,
      boAmountType,
      vendorBranchAmount,
      vendorBranchMaxAmount,
      boAmount,
      boMaxAmount,
      active,
      selectedProductIds,
      vendorId,
      productTypeId,
      categorySelectedIds,
      name,
      code,
      statusProduct,
      limitDay,
      currentPage,
      visibleCategoryModal,
      categoryIds,
      categorySelectedArray,
      visibleImageModal,
      dataImage,
    } = this.state;

    const productPromotionDetailsDataSource = [];
    const productPopupDataSource = [];
    const columns = [
      {
        title: '',
        dataIndex: 'url',
        width: 50,
        render: (text, record) => (
          <div onClick={() => this.showImageModal(record.imgUrl, record.key)}>
            <CPAvatar src={record.imgUrl} />
          </div>
        ),
      },
      {
        title: ID,
        dataIndex: 'productId',
        key: 'productId',
        width: 200,
      },
      {
        title: PRODUCT_LIST_NAME,
        dataIndex: 'productName',
        key: 'productName',
        width: 250,
      },
      {
        title: CATEGORY_NAME,
        dataIndex: 'categoryName',
        key: 'categoryName',
        width: 250,
      },
      {
        title: DISCOUNT_SELLER,
        dataIndex: 'discountSeller',
        key: 'discountSeller',
        width: 200,
      },
      {
        title: DISCOUNT_CENTER,
        dataIndex: 'districtCenter',
        key: 'districtCenter',
        width: 200,
      },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 100,
        render: (text, record) => (
          <CPSwitch
            defaultChecked={record.active}
            checkedChildren={ACTIVE}
            unCheckedChildren={DEACTIVATE}
            disabled={record.expired}
            onChange={value => {
              this.changeStatusVendorBranchProductPromotion(value, record);
            }}
          />
        ),
      },
      {
        title: '',
        dataIndex: '',
        width: 100,
        render: (text, record) => (
          <div>{record.expired && <label>منقضی شد</label>}</div>
        ),
      },
      {
        title: '',
        dataIndex: '',
        render: (text, record) => (
          <div>
            {record.applyJob &&
              promotionType !== 'DiscountCode' &&
              promotionType !== 'DiscountGroupCode' && (
                <CPButton onClick={() => this.updatePrice(record.key)}>
                  بروزرسانی
                </CPButton>
              )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <CPPopConfirm
            title={DELETE_CONFIRM}
            okText={YES}
            cancelText={NO}
            okType="primary"
            onConfirm={() => this.onDeleteProductPromotionDetails(record.key)}
          >
            {!record.expired && (
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            )}
          </CPPopConfirm>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            {!record.expired && (
              <CPButton
                onClick={() => this.showProductEditModal(record)}
                className="edit_action"
              >
                <i className="cp-edit" />
              </CPButton>
            )}
          </div>
        ),
      },
    ];
    const columnsProductsPopup = [
      {
        title: '',
        dataIndex: 'url',
        width: 50,
        render: (text, record) => (
          <div onClick={() => this.showImageModal(record.imgUrl, record.key)}>
            <CPAvatar src={record.imgUrl} />
          </div>
        ),
      },
      {
        title: ID,
        dataIndex: 'key',
        key: 'key',
      },
      {
        title: PRODUCT_LIST_NAME,
        dataIndex: 'productName',
        key: 'productName',
      },
      { title: CATEGORY_NAME, dataIndex: 'categoryName', key: 'categoryName' },
    ];
    treeModel.items = [];
    categoryChildren.map(item => {
      const treeNodeModel = {
        id: item.id,
        name: `${item.code}-${item.name}`,
        children: item.children,
      };

      return treeModel.items.push(treeNodeModel);
    });
    treeModel.allowLastLeafSelect = true;

    categoryChildren.map(item => this.traverseTree(item));

    /**
     * maping products Promotion Details for list data source
     */
    productsPromotionDetails.map(item => {
      let sellerAmountDescription = '';
      if (item.vendorBranchAmount > 0) {
        if (item.vendorBranchAmountType === 'Amount') {
          sellerAmountDescription = `${
            item.stringVendorBranchAmount
          }  ${TOMAN} `;
        } else {
          sellerAmountDescription = `${item.vendorBranchAmount} ${PERCENT}`;
          if (item.vendorBranchMaxAmount > 0)
            sellerAmountDescription = `${sellerAmountDescription} 
              تا سقف
               ${item.stringVendorBranchMaxAmount} 
              تومان`;
        }
      }

      let centerAmountDescription = '';
      if (item.boAmount > 0) {
        const value = item.boAmount;
        if (item.boAmountType === 'Amount') {
          centerAmountDescription = `${item.stringBoAmount} ${TOMAN}`;
        } else {
          centerAmountDescription = `${value} ${PERCENT}`;
          if (item.boMaxAmount > 0)
            centerAmountDescription = `${centerAmountDescription} 
              تا سقف
               ${item.stringBoMaxAmount} 
              تومان`;
        }
      }

      const obj = {
        key: item.id,
        discountSeller: sellerAmountDescription,
        districtCenter: centerAmountDescription,
        active: item.active,
        vendorBranchAmountType: item.vendorBranchAmountType,
        boAmountType: item.boAmountType,
        bOAmount: item.boAmount,
        productName: item.productDto ? item.productDto.name : '',
        categoryName:
          item.productDto &&
          item.productDto.productCategoryDtos &&
          item.productDto.productCategoryDtos.length &&
          item.productDto.productCategoryDtos[0].categoryDto
            ? item.productDto.productCategoryDtos[0].categoryDto.description
            : '',
        vendorBranchAmount: item.vendorBranchAmount,
        bOMaxAmount: item.boMaxAmount,
        vendorBranchMaxAmount: item.vendorBranchMaxAmount,
        productCode: item.productDto ? item.productDto.code : '',
        productId: item.productDto ? item.productDto.id : '',
        applyJob: item.applyJob,
        expired: item.expired,
        limitDay: item.stringPickupDeliveryDeadline,
        imgUrl:
          item.productDto &&
          item.productDto.imageDtos &&
          item.productDto.imageDtos.length > 0
            ? `${item.productDto.imageDtos[0].url}`
            : ` `,
      };
      return productPromotionDetailsDataSource.push(obj);
    });

    const vendorBranchArray = [];
    const productTypeArray = [];
    const CategoryChildrenArray = [];

    if (products)
      products.map(item => {
        const obj = {
          key: item.id,
          productName: item.name,
          categoryName: item.productCategoryDtos
            ? item.productCategoryDtos[0].categoryDto.description
            : '',
          productCode: item.code,
          imgUrl:
            item.imageDtos && item.imageDtos.length > 0
              ? `${item.imageDtos[0].url}`
              : ` `,
        };
        return productPopupDataSource.push(obj);
      });

    const rowSelection = {
      selectedProductIds,
      onChange: this.onSelectedProducts,
    };

    /**
     * map vendorBranches for comboBox Datasource
     */
    vendorBranches.map(item =>
      vendorBranchArray.push(
        <Option key={item.id.toString()} value={item.id.toString()}>
          {item.name}
        </Option>,
      ),
    );

    /**
     * map child Category for comboBox Datasource
     */
    categoryChildren.map(item =>
      CategoryChildrenArray.push(
        <Option key={item.id}>{item.description}</Option>,
      ),
    );
    /**
     * map product Types for comboBox Datasource
     */
    productTypes.map(item =>
      productTypeArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <div>
        <CPList
          data={productPromotionDetailsDataSource}
          columns={columns}
          count={productPromotionDetailsCount}
          loading={productPromotionDetailsLoading}
          onAddClick={this.productPromotionDetailsAddClick}
          hideSearchBox
          hideAdd={
            productsPromotionDetails.length > 0
              ? productsPromotionDetails[0].expired
              : false
          }
        />
        <CPModal
          visible={this.state.visibleProductFullModal}
          handleCancel={this.handleCancelvendorBranchFullModal}
          handleOk={this.insertProductPromotionDetails}
          title={SELECT_PRODUCT_FOR_PROMOTION}
          className="max_modal"
        >
          <div className="row">
            <div className="col-sm-12">
              <CPList
                data={productPopupDataSource}
                columns={columnsProductsPopup}
                count={productCount}
                loading={productLoading}
                rowSelection={rowSelection}
                withCheckBox
                hideAdd
                pagination={false}
              >
                <div className="searchBox">
                  <div className={s.searchFildes}>
                    <div className="row">
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <CPInput
                          name="name"
                          label={PRODUCT_LIST_NAME}
                          value={name}
                          onChange={value => {
                            this.handelChangeInput('name', value.target.value);
                          }}
                        />
                      </div>
                      <div className="col-lg-8 col-md-6 col-sm-12">
                        <label name="label">{PRODUCT_LIST_CATEGORY}</label>
                        <div className="col-sm-12 selectCategory">
                          <CPMultiSelect
                            placeholder="انتخاب گروه محصول"
                            onChange={this.categoryChange}
                            value={categorySelectedIds}
                          >
                            {categorySelectedArray}
                          </CPMultiSelect>
                          <CPButton onClick={this.showCategoryModal}>
                            {SELECT}
                          </CPButton>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <CPInput
                          name="code"
                          label={ID}
                          value={code}
                          onChange={value => {
                            this.handelChangeInput('code', value.target.value);
                          }}
                        />
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <label>{AGENT_NAME}</label>
                        <CPSelect
                          disabled
                          showSearch
                          value={vendorId}
                          onChange={value => {
                            this.handelChangeInput('vendorId', value);
                          }}
                        >
                          <Option key={0}>{ALL_VENDORS}</Option>
                          {vendorBranchArray}
                        </CPSelect>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <label>{PRODUCT_PRODUCT_TYPE}</label>
                        <CPSelect
                          value={productTypeId}
                          onChange={value => {
                            this.handelChangeInput('productTypeId', value);
                          }}
                        >
                          <Option key={0}>{ALL_TYPES}</Option>
                          {productTypeArray}
                        </CPSelect>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="align-margin">
                          <CPSwitch
                            defaultChecked={statusProduct}
                            checkedChildren={ACTIVE}
                            unCheckedChildren={DEACTIVATE}
                            onChange={value => {
                              this.handelChangeInput('statusProduct', value);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={s.field}>
                    <CPButton
                      size="large"
                      shape="circle"
                      type="primary"
                      icon="search"
                      onClick={this.submitSearch}
                    />
                  </div>
                </div>
              </CPList>
              <CPPagination
                total={productCount}
                current={currentPage}
                onChange={this.onChangePagination}
                onShowSizeChange={this.onShowSizeChange}
                defaultPageSize={10}
              />
            </div>
            <div className="col-sm-12">
              <div className={s.modalContent}>
                <div className="row">
                  <div className="col-sm-4">
                    <label>{PICKUP_DELIVERY_DAY_LIMIT}</label>
                    <CPPersianCalendar
                      placeholder={PICKUP_DELIVERY_DAY_LIMIT}
                      onChange={(unix, formatted) =>
                        this.handelChangeInput('limitDay', formatted)
                      }
                      preSelected={limitDay}
                    />
                  </div>
                  <div className="col-sm-4">
                    <label>{STATUS}</label>
                    <CPSwitch
                      defaultChecked={active}
                      checkedChildren={ACTIVE}
                      unCheckedChildren={DEACTIVATE}
                      onChange={value =>
                        this.handelChangeInput('active', value)
                      }
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-4 col-md-6 col-sm-12">
                    <CPPanel header={DISCOUNT_SELLER}>
                      <div className="row">
                        <div className="col-sm-12">
                          <label>{DISCOUNT_SELLER_TYPE}</label>
                          <CPSelect
                            name="vendorBranchAmountType"
                            value={vendorBranchAmountType}
                            onChange={value =>
                              this.handelChangeInput(
                                'vendorBranchAmountType',
                                value,
                              )
                            }
                          >
                            <Option key="Percent">{PERCENT}</Option>
                            <Option key="Amount">{TOMAN}</Option>
                          </CPSelect>
                        </div>
                        <div className="col-sm-12">
                          <label>{DISCOUNT_SELLER}</label>
                          <CPInputNumber
                            value={vendorBranchAmount}
                            onChange={value => {
                              this.handelChangeInput(
                                'vendorBranchAmount',
                                value,
                              );
                            }}
                            format={
                              vendorBranchAmountType === 'Percent'
                                ? 'Percent'
                                : 'Currency'
                            }
                          />
                        </div>
                        {vendorBranchAmountType === 'Percent' && (
                          <div className="col-sm-12">
                            <label>{MAX_DISCOUNT_SELLER}</label>
                            <CPInputNumber
                              value={vendorBranchMaxAmount}
                              onChange={value => {
                                this.handelChangeInput(
                                  'vendorBranchMaxAmount',
                                  value,
                                );
                              }}
                              format="Currency"
                            />
                          </div>
                        )}
                      </div>
                    </CPPanel>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-12">
                    <CPPanel header={DISCOUNT_CENTER}>
                      <div className="row">
                        <div className="col-sm-12">
                          <label>{DISCOUNT_CENTER_TYPE}</label>
                          <CPSelect
                            name="boAmountType"
                            value={boAmountType}
                            onChange={value =>
                              this.handelChangeInput('boAmountType', value)
                            }
                          >
                            <Option key="Percent">{PERCENT}</Option>
                            <Option key="Amount">{TOMAN}</Option>
                          </CPSelect>
                        </div>
                        <div className="col-sm-12">
                          <label>{DISCOUNT_CENTER}</label>
                          <CPInputNumber
                            value={boAmount}
                            onChange={value => {
                              this.handelChangeInput('boAmount', value);
                            }}
                            format={
                              boAmountType === 'Percent'
                                ? 'Percent'
                                : 'Currency'
                            }
                          />
                        </div>
                        {boAmountType === 'Percent' && (
                          <div className="col-sm-12">
                            <label>{MAX_DISCOUNT_CENTER}</label>
                            <CPInputNumber
                              value={boMaxAmount}
                              onChange={value => {
                                this.handelChangeInput('boMaxAmount', value);
                              }}
                              format="Currency"
                            />
                          </div>
                        )}
                      </div>
                    </CPPanel>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </CPModal>

        <CPModal
          visible={this.state.visibleProductEditModal}
          handleCancel={this.handleCancelproductEditModal}
          handleOk={this.updateCategoryPromotionDetails}
          title={SET_USER_ROLE}
        >
          <div>
            <div className="row">
              <div className="col-md-4 col-sm-12">
                <label>{DISCOUNT_SELLER_TYPE}</label>
                <CPSelect
                  name="vendorBranchAmountType"
                  value={vendorBranchAmountType}
                  onChange={value =>
                    this.handelChangeInput('vendorBranchAmountType', value)
                  }
                >
                  <Option key="Percent">{PERCENT}</Option>
                  <Option key="Amount">{TOMAN}</Option>
                </CPSelect>
              </div>
              <div className="col-md-4 col-sm-12">
                <label>{DISCOUNT_SELLER}</label>
                <CPInputNumber
                  value={vendorBranchAmount}
                  onChange={value => {
                    this.handelChangeInput('vendorBranchAmount', value);
                  }}
                  format={
                    vendorBranchAmountType === 'Percent'
                      ? 'Percent'
                      : 'Currency'
                  }
                />
              </div>
              {vendorBranchAmountType === 'Percent' && (
                <div className="col-md-4 col-sm-12">
                  <label>{MAX_DISCOUNT_SELLER}</label>
                  <CPInputNumber
                    value={vendorBranchMaxAmount}
                    onChange={value => {
                      this.handelChangeInput('vendorBranchMaxAmount', value);
                    }}
                    format="Currency"
                  />
                </div>
              )}
            </div>

            <div className="row">
              <div className="col-md-4 col-sm-12">
                <label>{DISCOUNT_CENTER_TYPE}</label>
                <CPSelect
                  name="boAmountType"
                  value={boAmountType}
                  onChange={value =>
                    this.handelChangeInput('boAmountType', value)
                  }
                >
                  <Option key="Percent">{PERCENT}</Option>
                  <Option key="Amount">{TOMAN}</Option>
                </CPSelect>
              </div>
              <div className="col-md-4 col-sm-12">
                <label>{DISCOUNT_CENTER}</label>
                <CPInputNumber
                  value={boAmount}
                  onChange={value => {
                    this.handelChangeInput('boAmount', value);
                  }}
                  format={boAmountType === 'Percent' ? 'Percent' : 'Currency'}
                />
              </div>
              {boAmountType === 'Percent' && (
                <div className="col-md-4 col-sm-12">
                  <label>{MAX_DISCOUNT_CENTER}</label>
                  <CPInputNumber
                    value={boMaxAmount}
                    onChange={value => {
                      this.handelChangeInput('boMaxAmount', value);
                    }}
                    format="Currency"
                  />
                </div>
              )}
            </div>

            <div className="row">
              <div className="col-lg-4 col-sm-12">
                <label>{PICKUP_DELIVERY_DAY_LIMIT}</label>
                <CPPersianCalendar
                  placeholder={PICKUP_DELIVERY_DAY_LIMIT}
                  onChange={(unix, formatted) =>
                    this.handelChangeInput('limitDay', formatted)
                  }
                  preSelected={limitDay}
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{STATUS}</label>
                <CPSwitch
                  defaultChecked={active}
                  unCheckedChildren={DEACTIVATE}
                  checkedChildren={ACTIVE}
                  onChange={value => this.handelChangeInput('active', value)}
                >
                  {STATUS}
                </CPSwitch>
              </div>
            </div>
          </div>
        </CPModal>
        <CPModal
          visible={visibleCategoryModal}
          handleCancel={this.handleCancelCategoryModal}
          handleOk={this.handleOkCategoryModal}
          className="max_modal"
          title="انتخاب دسته بندی"
          textSave={CONFIRMATION}
        >
          <CPTree
            model={treeModel}
            onCheck={this.onCheckCategory}
            checkable
            checkedKeys={categoryIds}
            defaultExpandAll
          />
        </CPModal>
        <CPModal
          visible={visibleImageModal}
          handleCancel={this.handleCancelImageModal}
          showFooter={false}
        >
          <CPCarousel data={dataImage} width={700} height={700} />
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  productsPromotionDetails: state.productPromotionDetailsList.data
    ? state.productPromotionDetailsList.data.items
    : null,
  productPromotionDetailsCount: state.productPromotionDetailsList.data
    ? state.productPromotionDetailsList.data.count
    : null,
  productPromotionDetailsLoading: state.productPromotionDetailsList.loading,
  products: state.productList.data ? state.productList.data.items : [],
  productCount: state.productList.data ? state.productList.data.count : 0,
  productLoading: state.productList.loading,
  categoryChildren: state.childCategoryList.data
    ? state.childCategoryList.data.items
    : [],
  productTypes: state.productTypesList.data
    ? state.productTypesList.data.items
    : [],
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorBranchPromotionDetailsPostRequest:
        vendorBranchPromotionDetailsPostActions.vendorBranchPromotionDetailsPostRequest,
      vendorBranchPromotionDetailsPutRequest:
        vendorBranchPromotionDetailsPutActions.vendorBranchPromotionDetailsPutRequest,
      vendorBranchPromotionDetailsDeleteRequest:
        vendorBranchPromotionDetailsDeleteActions.vendorBranchPromotionDetailsDeleteRequest,
      childCategoryListRequest:
        childCategoryListActions.childCategoryListRequest,
      productListRequest: productListActions.productListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductPromotionDetails));
