import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select, message } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PromotionDiscountGroupCode.css';
import {
  ACTIVE,
  ALL,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_DISCOUNT_CODE,
  CREATE_DATE,
  DEACTIVATE,
  DISCOUNT_CODE,
  DISCOUNT_CODE_COUNT,
  DISCOUNT_CODE_COUNT_WALLET,
  DISCOUNT_CODE_WALLET,
  FORM_SAVE,
  FROM_DATE,
  NO,
  NOT_RESERVED,
  NOT_USED,
  PRICE_DISCOUNT_CODE_WALLET,
  RESERVE,
  RESERVED,
  STATUS,
  TO_DATE,
  USE,
  USED,
  USER_NAME,
  YES,
} from '../../../../Resources/Localization';
import CPList from '../../../CP/CPList';

import CPPopConfirm from '../../../CP/CPPopConfirm';
import Link from '../../../Link';
import CPSwitch from '../../../CP/CPSwitch';
import CPButton from '../../../CP/CPButton';
import CPInput from '../../../CP/CPInput';
import promotionDiscountCodeDelete from '../../../../redux/sam/actionReducer/promotion/PromotionDiscountCodeDelete';
import promotionDiscountCodePost from '../../../../redux/sam/actionReducer/promotion/PromotionDiscountCodePost';
import promotionDiscountCodePut from '../../../../redux/sam/actionReducer/promotion/PromotionDiscountCodePut';
import promotionDiscountCodeList from '../../../../redux/sam/actionReducer/promotion/PromotionDiscountCodeList';
import walletPromotionPutActions from '../../../../redux/sam/actionReducer/promotion/WalletPromotionPut';
import {
  DiscountCodeDto,
  PromotionDto,
  WalletPromotionDto,
} from '../../../../dtos/samDtos';
import CPPersianCalendar from '../../../CP/CPPersianCalendar';
import CPCheckbox from '../../../CP/CPCheckbox';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPSelect from '../../../CP/CPSelect';
import CPInputNumber from "../../../CP/CPInputNumber";
import {pressEnterAndCallApplySearch} from "../../../../utils/helper";
import CPPagination from "../../../CP/CPPagination";

const { Option } = Select;

class PromotionDiscountGroupCode extends React.Component {
  static propTypes = {
    promotionDiscountCodeLoading: PropTypes.bool,
    promotionDiscountCodes: PropTypes.arrayOf(PropTypes.object),
    promotionDiscountCodeCount: PropTypes.number,
    promotion: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    promotion: null,
    promotionDiscountCodeLoading: false,
    promotionDiscountCodeCount: 0,
    promotionDiscountCodes: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      code: '',
      active: true,
      persianSearchFromDate: '',
      persianSearchToDate: '',
      discountCodeCount: '0',
      changeStatusDiscountCode: false,
      discountCode: '',
      status: true,
      discountCodeFromDate: '',
      discountCodeToDate: '',
      fromDate: '',
      toDate: '',
      id: 0,
      searchUsedQuota: 0,
      searchReservedUser: 0,
      discountCodeWalletPrice:
        props.promotion &&
        props.promotion.promotionType === 'DiscountWalletCode' &&
        props.promotion.walletPromotionDtos &&
        props.promotion.walletPromotionDtos.length > 0
          ? props.promotion.walletPromotionDtos[0].amount
          : 0,
      currentPage: 1,
      pageSize: 10,
    };
    this.submitSearch = this.submitSearch.bind(this);
    this.insertDiscountCode = this.insertDiscountCode.bind(this);
  }

  componentWillMount() {
    const searchData = this.searchFields();

    this.props.actions.promotionDiscountCodeListRequest(searchData);
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  onDelete = id => {
    /**
     * reload discount code after insert
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, removeChildtoParentList}}
     */
    const listRefresh = this.searchFields();

    const deleteData = {
      id,
      listDto: listRefresh,
    };

    this.props.actions.promotionDiscountCodeDeleteRequest(deleteData);
  };

  searchFields() {
    const {
      code,
      active,
      fromDate,
      toDate,
      searchUsedQuota,
      searchReservedUser,
      currentPage,
      pageSize,
    } = this.state;
    const { promotion } = this.props;

    let used = null;
    let reserved = null;

    if (searchUsedQuota === 1) used = true;
    else if (searchUsedQuota === 2) used = false;

    if (searchReservedUser === 1) reserved = true;
    else if (searchReservedUser === 2) reserved = false;

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DiscountCodeDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
          code,
          active,
          persianSearchFromDateTimeUtc: fromDate,
          persianSearchToDateTimeUtc: toDate,
          searchUsedQuota: used,
          searchReservedUser: reserved,
        }),
      )
      .includes(['reservedUserDto'])
      .pageIndex(currentPage - 1)
      .pageSize(pageSize)
      .buildJson();
    return jsonList;
  }

  /**
   * for search product
   * @param value
   */
  async submitSearch() {
    const searchData = this.searchFields();

    this.props.actions.promotionDiscountCodeListRequest(searchData);
  }

  insertDiscountCode() {
    const {
      discountCodeCount,
      id,
      status,
      discountCodeFromDate,
      discountCodeToDate,
      discountCodeWalletPrice,
    } = this.state;

    const { promotion } = this.props;

    /**
     * reload discount code after insert
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, removeChildtoParentList}}
     */

    if (
      promotion.promotionType === 'DiscountWalletCode' &&
      discountCodeWalletPrice <= 0
    ) {
      message.error('لطفا مبلغ کد تخفیف کیف پول را وارد کنید!', 7);
      return;
    }

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DiscountCodeDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
          active: true,
        }),
      )
      .buildJson();

    if (id === 0) {
      const postDiscountCode = {
        dto: new DiscountCodeDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
        count: parseInt(discountCodeCount, 0),
      };

      const postData = {
        data: postDiscountCode,
        listDto: jsonList,
      };

      this.props.actions.promotionDiscountCodePostRequest(postData);
    } else {
      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new DiscountCodeDto({
            active: status,
            id,
            freeQuota: discountCodeCount,
            persianSearchFromDateTimeUtc:
              discountCodeFromDate !== '' ? discountCodeFromDate : undefined,
            persianSearchToDateTimeUtc:
              discountCodeToDate !== '' ? discountCodeToDate : undefined,
          }),
        )
        .build();

      const putData = {
        data: jsonCrud,
        listDto: jsonList,
      };

      this.props.actions.promotionDiscountCodePutRequest(putData);
    }
    this.setState({
      status: true,
      isEdit: false,
    });
  }

  changeStatusDiscountCode = (inputName, value, record) => {
    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new DiscountCodeDto({
          active: value,
          id: record.key,
        }),
      )
      .build();
    /**
     * reload discount code after insert
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, removeChildtoParentList}}
     */
    const listRefresh = this.searchFields();

    const putData = {
      data: jsonCrud,
      listDto: listRefresh,
    };

    this.props.actions.promotionDiscountCodePutRequest(putData);
  };

  // <editor-fold dsc="CPPersianCalendar">
  changeFromDate = (unix, formatted) => {
    this.setState({
      persianSearchFromDate: formatted,
    });
  };

  changeToDate = (unix, formatted) => {
    this.setState({
      persianSearchToDate: formatted,
    });
  };
  // </editor-fold>

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  showDetail = (e, record) => {
    e.preventDefault();
    this.setState({
      isEdit: true,
      status: record.active,
      id: record.key,
      discountCode: record.code,
      discountCodeFromDate: record.persianFromDateTimeUtc,
      discountCodeToDate: record.persianToDateTimeUtc,
      discountCodeCount: record.freeQuota.toString(),
    });
  };

  changePriceDiscountCodeWallet = () => {
    const { discountCodeWalletPrice } = this.state;
    const { promotion } = this.props;

    if (discountCodeWalletPrice <= 0) {
      message.error('لطفا مبلغ کد تخفیف کیف پول را وارد کنید!', 7);
      return;
    }

    const walletCrud = new BaseCRUDDtoBuilder()
      .dto(
        new WalletPromotionDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
          amount: discountCodeWalletPrice,
        }),
      )
      .build();

    this.props.actions.walletPromotionPutRequest(walletCrud);
  };


  onChangePagination = page => {
    this.setState({ currentPage: page }, () => this.submitSearch());
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () =>
      this.props.actions.userListRequest(this.prepareContainer()),
    );
  };

  render() {
    const {
      promotionDiscountCodeLoading,
      promotionDiscountCodes,
      promotionDiscountCodeCount,
      promotion,
    } = this.props;
    const {
      discountCodeCount,
      code,
      active,
      fromDate,
      toDate,
      discountCodeFromDate,
      discountCodeToDate,
      discountCode,
      status,
      isEdit,
      searchUsedQuota,
      searchReservedUser,
      discountCodeWalletPrice,
      currentPage,
    } = this.state;

    const columns = [
      {
        title:
          promotion.promotionType === 'DiscountGroupCode'
            ? DISCOUNT_CODE
            : DISCOUNT_CODE_WALLET,
        dataIndex: 'code',
        key: 'discountCode',
        width: 200,
      },
      {
        title: USER_NAME,
        dataIndex: 'userName',
        key: 'userName',
        width: 200,
      },
      {
        title: RESERVED,
        dataIndex: 'reserved',
        key: 'reserved',
        width: 130,
        render: (text, record) => (
          <div>
            <CPSwitch disabled defaultChecked={record.reserved} />
          </div>
        ),
      },
      {
        title: USED,
        dataIndex: 'used',
        key: 'used',
        width: 130,
        render: (text, record) => (
          <div>
            <CPSwitch disabled defaultChecked={record.used} />
          </div>
        ),
      },
      {
        title: CREATE_DATE,
        dataIndex: 'createDate',
        key: 'createDate',
        width: 100,
      },
      {
        title: STATUS,
        dataIndex: 'operationOne1',
        width: 130,
        render: (text, record) => (
          <div>
            <CPSwitch
              disabled
              defaultChecked={record.active}
              checkedChildren={ACTIVE}
              unCheckedChildren={DEACTIVATE}
              onChange={value => {
                this.changeStatusDiscountCode(
                  'changeStatusDiscountCode',
                  value,
                  record,
                );
              }}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_DISCOUNT_CODE}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <Link to="#" className="delete_action">
                <i className="cp-trash" />
              </Link>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <Link
              to="#"
              className="edit_action"
              onClick={e => this.showDetail(e, record)}
            >
              <i className="cp-edit" />
            </Link>
          </div>
        ),
      },
    ];

    const discountCodeArray = [];

    if (promotionDiscountCodes) {
      promotionDiscountCodes.map(item => {
        const obj = {
          key: item.id,
          active: item.active,
          code: item.code,
          createDate: item.persianCreatedOnUtc,
          freeQuota: item.freeQuota,
          persianFromDateTimeUtc: item.persianFromDateTimeUtc,
          persianToDateTimeUtc: item.persianToDateTimeUtc,
          used: item.usedQuota > 0,
          reserved: !!item.reservedUserDto,
          userName: item.reservedUserDto
            ? item.reservedUserDto.displayName
            : '',
        };
        discountCodeArray.push(obj);
        return null;
      });
    }
    return (
      <div>
        <CPList
          data={discountCodeArray}
          columns={columns}
          count={promotionDiscountCodeCount}
          loading={promotionDiscountCodeLoading}
          hideAdd
          pagination={false}
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-2 col-md-6 col-sm-12">
                  <CPInput
                    name="name"
                    label={
                      promotion.promotionType === 'DiscountGroupCode'
                        ? DISCOUNT_CODE
                        : DISCOUNT_CODE_WALLET
                    }
                    value={code}
                    onChange={value => {
                      this.handelChangeInput('code', value.target.value);
                    }}
                  />
                </div>
                <div className="col-lg-2 col-md-6 col-sm-12">
                  <label>{RESERVE}</label>
                  <CPSelect
                    name="name"
                    value={searchReservedUser}
                    onChange={value => {
                      this.handelChangeInput('searchReservedUser', value);
                    }}
                  >
                    <Option value={0}>{ALL}</Option>
                    <Option value={1}>{RESERVED}</Option>
                    <Option value={2}>{NOT_RESERVED}</Option>
                  </CPSelect>
                </div>
                <div className="col-lg-2 col-md-6 col-sm-12">
                  <label>{USE}</label>
                  <CPSelect
                    name="name"
                    value={searchUsedQuota}
                    onChange={value => {
                      this.handelChangeInput('searchUsedQuota', value);
                    }}
                  >
                    <Option value={0}>{ALL}</Option>
                    <Option value={1}>{USED}</Option>
                    <Option value={2}>{NOT_USED}</Option>
                  </CPSelect>
                </div>
                <div className="col-lg-2 col-md-6 col-sm-12">
                  <label>{FROM_DATE}</label>
                  <CPPersianCalendar
                    placeholder={FROM_DATE}
                    onChange={(unix, formatted) =>
                      this.handelChangeInput('fromDate', formatted)
                    }
                    preSelected={fromDate}
                  />
                </div>
                <div className="col-lg-2 col-md-6 col-sm-12">
                  <label>{TO_DATE}</label>
                  <CPPersianCalendar
                    placeholder={TO_DATE}
                    onChange={(unix, formatted) =>
                      this.handelChangeInput('toDate', formatted)
                    }
                    preSelected={toDate}
                  />
                </div>
                <div className="col-lg-2 col-md-6 col-sm-12">
                  <div className="align-center">
                    <CPSwitch
                      defaultChecked={active}
                      checkedChildren={ACTIVE}
                      unCheckedChildren={DEACTIVATE}
                      onChange={value => {
                        this.handelChangeInput('active', value);
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CPList>

        <CPPagination
          total={promotionDiscountCodeCount}
          current={currentPage}
          onChange={this.onChangePagination}
          onShowSizeChange={this.onShowSizeChange}
          defaultPageSize={10}
          pageSizeOptions={['10', '20', '30', '50', '80', '100']}
        />
        <div>
          {isEdit && <label>{discountCode}</label>}
          <div className="row">
            <div className="col-lg-3 col-sm-12">
              <label>
                {promotion.promotionType === 'DiscountGroupCode'
                  ? DISCOUNT_CODE_COUNT
                  : DISCOUNT_CODE_COUNT_WALLET}
              </label>
              <CPInput
                value={discountCodeCount}
                onChange={value => {
                  this.handelChangeInput(
                    'discountCodeCount',
                    value.target.value,
                  );
                }}
              />
            </div>
            {isEdit && (
              <div className="col-lg-3 col-sm-12">
                <label>{FROM_DATE}</label>
                <CPPersianCalendar
                  placeholder={FROM_DATE}
                  // format="jYYYY/jMM/jDD"
                  onChange={(unix, formatted) =>
                    this.handelChangeInput('discountCodeFromDate', formatted)
                  }
                  // id="datePicker"
                  preSelected={discountCodeFromDate}
                />
              </div>
            )}
            {isEdit && (
              <div className="col-lg-3 col-sm-12">
                <label>{TO_DATE}</label>
                <CPPersianCalendar
                  placeholder={TO_DATE}
                  onChange={(unix, formatted) =>
                    this.handelChangeInput('discountCodeToDate', formatted)
                  }
                  // preSelected={discountCodeToDate}
                  preSelected={discountCodeToDate}
                />
              </div>
            )}
            <div className="col-lg-3 col-sm-12">
              <label>{STATUS}</label>
              <CPCheckbox
                checked={status}
                onChange={value => {
                  this.handelChangeInput('status', value.target.checked);
                }}
              />
            </div>

            {promotion.promotionType === 'DiscountWalletCode' && (
              <div className="col-lg-3 col-sm-12">
                <label>{PRICE_DISCOUNT_CODE_WALLET}</label>
                <CPInputNumber
                  value={discountCodeWalletPrice}
                  onChange={value =>
                    this.handelChangeInput('discountCodeWalletPrice', value)
                  }
                  format="Currency"
                />
              </div>
            )}
          </div>

          <div className="row">
            <div className="col-sm-6 align-margin">
              <CPButton
                icon="save"
                type="primary"
                onClick={this.insertDiscountCode}
              >
                {FORM_SAVE}
              </CPButton>
            </div>
            <div className="col-sm-6 align-margin">
              <CPButton
                icon="save"
                type="primary"
                onClick={this.changePriceDiscountCodeWallet}
              >
                {FORM_SAVE}
              </CPButton>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  promotion: state.singlePromotion.data
    ? state.singlePromotion.data.items[0]
    : null,
  promotionDiscountCodes: state.promotionDiscountCodeList.data
    ? state.promotionDiscountCodeList.data.items
    : null,
  promotionDiscountCodeCount: state.promotionDiscountCodeList.data
    ? state.promotionDiscountCodeList.data.count
    : null,
  promotionDiscountCodeLoading: state.promotionDiscountCodeList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      promotionDiscountCodeListRequest:
        promotionDiscountCodeList.promotionDiscountCodeListRequest,
      promotionDiscountCodePostRequest:
        promotionDiscountCodePost.promotionDiscountCodePostRequest,
      promotionDiscountCodeDeleteRequest:
        promotionDiscountCodeDelete.promotionDiscountCodeDeleteRequest,
      promotionDiscountCodePutRequest:
        promotionDiscountCodePut.promotionDiscountCodePutRequest,
      walletPromotionPutRequest:
        walletPromotionPutActions.walletPromotionPutRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PromotionDiscountGroupCode));
