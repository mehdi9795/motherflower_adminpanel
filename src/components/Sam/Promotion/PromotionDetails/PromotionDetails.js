import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PromotionDetails.css';
import {
  ACTIVE,
  AGENT_NAME,
  ALL_CITIES,
  ALL_DISTRICT,
  ALL_VENDORS,
  CITY,
  DEACTIVATE,
  DELETE_CONFIRM,
  DISTRICT,
  NAME,
  NO,
  SELECT_VENDOR_BRANCH,
  STATUS,
  VENDOR_BRANCH_NAME,
  YES,
} from '../../../../Resources/Localization';
import CPList from '../../../CP/CPList';

import CPPopConfirm from '../../../CP/CPPopConfirm';
import Link from '../../../Link';
import CPSwitch from '../../../CP/CPSwitch';
import {
  PromotionDto,
  VendorBranchPromotionDto,
} from '../../../../dtos/samDtos';
import vendorBranchPromotionDeleteActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionDelete';
import vendorBranchPromotionPostActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionPost';
import vendorBranchPromotionPutActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionPut';
import vendorBranchPromotionListActions from '../../../../redux/sam/actionReducer/promotion/VendorBranchPromotionList';
import CPModal from '../../../CP/CPModal';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import { AgentDto, VendorBranchDto } from '../../../../dtos/vendorDtos';
import CPButton from '../../../CP/CPButton';
import CPInput from '../../../CP/CPInput';
import CPSelect from '../../../CP/CPSelect';
import cityListActions from '../../../../redux/common/actionReducer/city/List';
import districtOptionListActions from '../../../../redux/common/actionReducer/district/List';
import vendorListActions from '../../../../redux/vendor/actionReducer/vendor/List';
import { DistrictDto } from '../../../../dtos/commonDtos';
import {
  BaseGetDtoBuilder,
  BaseListGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import {pressEnterAndCallApplySearch} from "../../../../utils/helper";

const { Option } = Select;

class PromotionDetails extends React.Component {
  static propTypes = {
    promotion: PropTypes.objectOf(PropTypes.any),
    vendorBranchPromotionLoading: PropTypes.bool,
    vendorBranchPromotions: PropTypes.arrayOf(PropTypes.object),
    vendorBranchPromotionCount: PropTypes.number,
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    vendorBranchCount: PropTypes.number,
    vendorBranchLoading: PropTypes.bool,
    cities: PropTypes.arrayOf(PropTypes.object),
    vendors: PropTypes.arrayOf(PropTypes.object),
    districtOptions: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    promotion: null,
    districtOptions: [],
    vendorBranchPromotions: [],
    vendorBranches: [],
    cities: [],
    vendors: [],
    vendorBranchLoading: false,
    vendorBranchPromotionLoading: false,
    vendorBranchCount: 0,
    vendorBranchPromotionCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      visibleVendorBranchFullModal: false,
      selectedVendorBranchIds: [],
      searchName: '',
      searchVendorId: '0',
      searchCityId: '0',
      searchDistrictId: '0',
      searchVendorBranchStatus: true,
    };
  }

  componentWillMount() {
    /**
     * get all cities for first time with default clause
     */
    this.props.actions.cityListRequest(
      new BaseGetDtoBuilder().all(true).buildJson(),
    );

    /**
     * get all vendors for first time with where clause
     */
    this.props.actions.vendorListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new AgentDto({
            active: true,
          }),
        )
        .buildJson(),
    );
  }

  onDeleteVendorBranchPromotion = id => {
    const { promotion } = this.props;
    /**
     * reload vendor branch promotion after delete vendor branch
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes}}
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchPromotionDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
      )
      .includes(['VendorBranch', 'Promotion'])
      .buildJson();

    const deleteData = {
      id,
      listDto: jsonList,
    };

    this.props.actions.vendorBranchPromotionDeleteRequest(deleteData);
  };

  onSelectedVendorBranch = selectedRowKeys => {
    this.setState({ selectedVendorBranchIds: selectedRowKeys });
  };

  /**
   * change city for automatic change district
   * @param inputName
   * @param value
   * @returns {Promise<void>}
   */
  async cityChange(inputName, value) {
    this.props.actions.districtOptionListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityId: value,
          }),
        )
        .buildJson(),
    );
    this.setState({
      [inputName]: value,
    });
  }

  changeStatusVendorBranch = (inputName, value, record) => {
    const { promotion } = this.props;
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchPromotionDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
      )
      .includes(['VendorBranch', 'Promotion'])
      .buildJson();

    const data = {
      data: {
        dtos: [new VendorBranchPromotionDto({ active: value, id: record.key })],
      },
      listDto: jsonList,
    };

    this.props.actions.vendorBranchPromotionPutRequest(data);
  };

  insertVendorBranchPromotion = () => {
    const { promotion } = this.props;
    const { selectedVendorBranchIds } = this.state;

    /**
     * reload vendor branch promotion after insert new vendor branch
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes}}
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchPromotionDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
      )
      .includes(['VendorBranch', 'Promotion'])
      .buildJson();

    /**
     * insert vendor branch to vendor branch promotion
     * @type {Array}
     */
    const dtos = [];
    selectedVendorBranchIds.map(item => {
      dtos.push(
        new VendorBranchPromotionDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
          vendorBranchDto: new VendorBranchDto({ id: item }),
        }),
      );

      return null;
    });
    this.props.actions.vendorBranchPromotionPostRequest({
      data: { dtos },
      listDto: jsonList,
    });

    this.setState({ visibleVendorBranchFullModal: false });
  };

  vendorBranchPromotionAddClick = () => {
    const { vendorBranchPromotions } = this.props;
    const {
      searchName,
      searchVendorId,
      searchCityId,
      searchDistrictId,
    } = this.state;
    const vendorBranchPromotionIds = [];

    vendorBranchPromotions.map(item =>
      vendorBranchPromotionIds.push(item.vendorBranchDto.id),
    );

    const jsonListVB = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchDto({
          name: searchName,
          active: true,
          cityId: searchCityId,
          districtId: searchDistrictId,
          vendorId: searchVendorId,
        }),
      )
      .notExpectedIds(vendorBranchPromotionIds)
      .includes(['vendorDto', 'districtDto'])
      .buildJson();

    this.props.actions.vendorBranchListRequest(jsonListVB);

    this.setState({
      visibleVendorBranchFullModal: true,
      selectedVendorBranchIds: [],
    });
  };

  handleCancelvendorBranchFullModal = () => {
    this.setState({
      visibleVendorBranchFullModal: false,
    });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });

    if ([inputName][0] === 'districtId')
      this.setState({
        districtValue: value,
      });
  };

  async updatePrice(record) {
    const { promotion } = this.props;

    const data = {
      data: {
        dtos: [
          new VendorBranchPromotionDto({
            applyJob: true,
            active: record.active,
            id: record.key,
          }),
        ],
      },
      listDto: null,
    };

    this.props.actions.vendorBranchPromotionPutRequest(data);

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchPromotionDto({
          promotionDto: new PromotionDto({ id: promotion.id }),
        }),
      )
      .includes(['VendorBranch', 'Promotion'])
      .buildJson();
    this.props.actions.vendorBranchPromotionListRequest(jsonList);
  }

  render() {
    const {
      vendorBranchPromotions,
      vendorBranchPromotionCount,
      vendorBranchPromotionLoading,
      vendorBranches,
      vendorBranchCount,
      vendorBranchLoading,
      cities,
      vendors,
      districtOptions,
      promotion,
    } = this.props;
    const {
      selectedVendorBranchIds,
      searchName,
      searchVendorId,
      searchCityId,
      searchDistrictId,
      searchVendorBranchStatus,
    } = this.state;
    const vendorBranchPromotionDataSource = [];
    const vendorBranchDataSource = [];
    const columns = [
      { title: NAME, dataIndex: 'name', key: 'name', width: 250 },
      { title: CITY, dataIndex: 'city', key: 'city', width: 100 },
      { title: DISTRICT, dataIndex: 'district', key: 'district', width: 100 },
      { title: AGENT_NAME, dataIndex: 'vendor', key: 'vendor', width: 150 },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 100,
        render: (text, record) => (
          <div>
            <CPSwitch
              defaultChecked={record.enable}
              checkedChildren={ACTIVE}
              unCheckedChildren={DEACTIVATE}
              disabled={record.expired}
              onChange={value => {
                this.changeStatusVendorBranch(
                  'vendorBranchPromotionStatus',
                  value,
                  record,
                );
              }}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: '',
        render: (text, record) => (
          <div>{record.expired && <label>منقضی شد</label>}</div>
        ),
      },
      {
        title: '',
        dataIndex: '',
        render: (text, record) => (
          <div>
            {record.applyJob &&
              promotion.promotionType !== 'DiscountCode' &&
              promotion.promotionType !== 'DiscountGroupCode' && (
                <CPButton onClick={() => this.updatePrice(record)}>
                  بروزرسانی
                </CPButton>
              )}
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <CPPopConfirm
            title={DELETE_CONFIRM}
            okText={YES}
            cancelText={NO}
            okType="primary"
            onConfirm={() => this.onDeleteVendorBranchPromotion(record.key)}
          >
            {!record.expired && (
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            )}
          </CPPopConfirm>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <Link
            to={`/promotion/vendorBranchPromotionDetails?id=${
              record.key
            }&vendorBranchId=${record.vendorBranchId}&promotionId=${
              record.promotionId
            }&promotionType=${record.promotionType}`}
            className="edit_action"
          >
            <i className="cp-edit" />
          </Link>
        ),
      },
    ];
    const columnsVendorBranchPopup = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      { title: CITY, dataIndex: 'city', key: 'city' },
      { title: DISTRICT, dataIndex: 'district', key: 'district' },
      { title: AGENT_NAME, dataIndex: 'vendor', key: 'vendor' },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <CPSwitch disabled size="small" defaultChecked={record.active} />
        ),
      },
    ];

    const vendorDataSource = [];
    const citiesDataSource = [];
    const districtDataSource = [];

    /**
     * maping vendor Branches promotion for list data source
     */
    vendorBranchPromotions.map(item => {
      const obj = {
        key: item.id,
        name: item.vendorBranchDto.name,
        vendor: item.vendorBranchDto ? item.vendorBranchDto.vendorDto.name : '',
        city:
          item.vendorBranchDto &&
          item.vendorBranchDto.districtDto &&
          item.vendorBranchDto.districtDto.cityDto
            ? item.vendorBranchDto.districtDto.cityDto.name
            : '',
        district:
          item.vendorBranchDto && item.vendorBranchDto.districtDto
            ? item.vendorBranchDto.districtDto.name
            : '',
        active: item.vendorBranchDto ? item.vendorBranchDto.active : false,
        enable: item.active,
        vendorBranchId: item.vendorBranchDto ? item.vendorBranchDto.id : 0,
        promotionId: item.promotionDto ? item.promotionDto.id : 0,
        promotionType: item.promotionDto ? item.promotionDto.promotionType : '',
        expired: item.expired,
        applyJob: item.applyJob,
      };
      return vendorBranchPromotionDataSource.push(obj);
    });

    /**
     * maping vendor Branches for list data source full modal
     */
    if (vendorBranches)
      vendorBranches.map(item => {
        const obj = {
          key: item.id,
          name: item.name,
          vendor: item.vendorDto ? item.vendorDto.name : '',
          city:
            item.districtDto && item.districtDto.cityDto
              ? item.districtDto.cityDto.name
              : '',
          district: item.districtDto ? item.districtDto.name : '',
          active: item.active,
        };
        return vendorBranchDataSource.push(obj);
      });

    /**
     * map vendors for comboBox Datasource
     */
    if (vendors)
      vendors.map(item =>
        vendorDataSource.push(<Option key={item.id}>{item.name}</Option>),
      );

    /**
     * map cities for comboBox Datasource
     */
    if (cities)
      cities.map(item =>
        citiesDataSource.push(<Option key={item.id}>{item.name}</Option>),
      );

    /**
     * map district for comboBox Datasource
     */
    if (districtOptions)
      districtOptions.map(item =>
        districtDataSource.push(<Option key={item.id}>{item.name}</Option>),
      );

    const rowSelection = {
      selectedVendorBranchIds,
      onChange: this.onSelectedVendorBranch,
    };

    return (
      <div>
        <CPList
          data={vendorBranchPromotionDataSource}
          columns={columns}
          count={vendorBranchPromotionCount}
          loading={vendorBranchPromotionLoading}
          onAddClick={this.vendorBranchPromotionAddClick}
          hideSearchBox
          hideAdd={
            vendorBranchPromotions && vendorBranchPromotions[0]
              ? vendorBranchPromotions[0].expired
              : false
          }
        />
        <CPModal
          visible={this.state.visibleVendorBranchFullModal}
          handleCancel={this.handleCancelvendorBranchFullModal}
          handleOk={this.insertVendorBranchPromotion}
          title={SELECT_VENDOR_BRANCH}
          className="max_modal"
        >
          <CPList
            data={vendorBranchDataSource}
            count={vendorBranchCount}
            columns={columnsVendorBranchPopup}
            loading={vendorBranchLoading}
            rowSelection={rowSelection}
            selectedIds={selectedVendorBranchIds}
            onChange={this.onSelectedVendorBranch}
            withCheckBox
            hideAdd
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    {' '}
                    <CPInput
                      name="name"
                      label={VENDOR_BRANCH_NAME}
                      value={searchName}
                      onChange={value => {
                        this.handelChangeInput(
                          'searchName',
                          value.target.value,
                        );
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{AGENT_NAME}</label>
                    <CPSelect
                      name="vendorId"
                      defaultValue={searchVendorId}
                      showSearch
                      onChange={value => {
                        this.handelChangeInput('searchVendorId', value);
                      }}
                    >
                      <Option key={0}>{ALL_VENDORS}</Option>
                      {vendorDataSource}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{CITY}</label>
                    <CPSelect
                      name="cityId"
                      defaultValue={searchCityId}
                      showSearch
                      onChange={value => {
                        this.cityChange('searchCityId', value);
                      }}
                    >
                      <Option key={0}>{ALL_CITIES}</Option>
                      {citiesDataSource}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{DISTRICT}</label>
                    <CPSelect
                      defaultValue={searchDistrictId}
                      // value={searchDistrictValue}
                      showSearch
                      onChange={value => {
                        this.handelChangeInput('searchDistrictId', value);
                      }}
                    >
                      <Option key={0}>{ALL_DISTRICT}</Option>
                      {districtDataSource}
                    </CPSelect>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <div className="align-center">
                      <CPSwitch
                        defaultChecked={searchVendorBranchStatus}
                        checkedChildren={ACTIVE}
                        unCheckedChildren={DEACTIVATE}
                        onChange={value => {
                          this.handelChangeInput(
                            'searchVendorBranchStatus',
                            value,
                          );
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="field">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.vendorBranchPromotionAddClick}
                />
              </div>
            </div>
          </CPList>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  vendorBranchPromotions: state.vendorBranchPromotionList.data
    ? state.vendorBranchPromotionList.data.items
    : [],
  vendorBranchPromotionCount: state.vendorBranchPromotionList.data
    ? state.vendorBranchPromotionList.data.count
    : 0,
  vendorBranchPromotionLoading: state.vendorBranchPromotionList.loading,
  promotion: state.singlePromotion.data
    ? state.singlePromotion.data.items[0]
    : null,
  vendorBranches: state.vendorBranchList.data.items,
  vendorBranchCount: state.vendorBranchList.data.count,
  vendorBranchLoading: state.vendorBranchList.loading,
  vendors: state.vendorList.data.items,
  cities: state.cityList.data.items,
  districtOptions: state.districtList.data.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorBranchPromotionPutRequest:
        vendorBranchPromotionPutActions.vendorBranchPromotionPutRequest,
      vendorBranchListRequest: vendorBranchListActions.vendorBranchListRequest,
      vendorBranchPromotionPostRequest:
        vendorBranchPromotionPostActions.vendorBranchPromotionPostRequest,
      vendorBranchPromotionDeleteRequest:
        vendorBranchPromotionDeleteActions.vendorBranchPromotionDeleteRequest,
      cityListRequest: cityListActions.cityListRequest,
      vendorListRequest: vendorListActions.vendorListRequest,
      districtOptionListRequest:
        districtOptionListActions.districtOptionListRequest,
      vendorBranchPromotionListRequest:
        vendorBranchPromotionListActions.vendorBranchPromotionListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PromotionDetails));
