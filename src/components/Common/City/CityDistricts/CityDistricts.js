import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select, message } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CityDistricts.css';
import CPInput from '../../../CP/CPInput/CPInput';
import {
  ACTIVE,
  ALL,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_DISTRICT,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_ZONE,
  DEACTIVATE,
  DISTRICT_EDIT,
  GRADE,
  LATITUDE,
  LONGITUDE,
  MAP,
  NAME,
  NO,
  OFFERED_IN_SEARCH,
  STATUS,
  YES,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import { CityDto, DistrictDto } from '../../../../dtos/commonDtos';
import districtPutActions from '../../../../redux/common/actionReducer/district/Put';
import districtPostActions from '../../../../redux/common/actionReducer/district/Post';
import districtDeleteActions from '../../../../redux/common/actionReducer/district/Delete';
import districtOptionListActions from '../../../../redux/common/actionReducer/district/List';
import CPModal from '../../../CP/CPModal';
import CPMap from '../../../CP/CPMap';
import CPSelect from '../../../CP/CPSelect';
import CPPagination from '../../../CP/CPPagination';
import CPList from '../../../CP/CPList';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import CPSwitch from '../../../CP/CPSwitch';
import CPCheckbox from '../../../CP/CPCheckbox';
import { pressEnterAndCallApplySearch } from '../../../../utils/helper';

const { Option } = Select;

class CityDistricts extends React.Component {
  static propTypes = {
    city: PropTypes.objectOf(PropTypes.any),
    districts: PropTypes.arrayOf(PropTypes.object),
    districtLoading: PropTypes.bool,
    districtCount: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    districts: null,
    city: null,
    districtLoading: false,
    districtCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      districtName: '',
      visibleEditModal: false,
      lat: '',
      lng: '',
      districtGrade: 'VIP',
      id: 0,
      currentPage: 1,
      pageSize: 10,
      orderByDescending: false,
      orderByFields: undefined,
      status: false,
      offered: false,
      searchDistrictGrade: '0',
      searchOffered: '0',
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  onChangePagination = page => {
    this.setState({ currentPage: page }, () =>
      this.submitSearch(page, this.state.pageSize),
    );
  };

  onShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize }, () =>
      this.submitSearch(current, pageSize),
    );
  };

  setLatAndLng = value => {
    this.setState({ lat: value.lat, lng: value.lng });
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  /**
   * for search in district
   * @returns {Promise<void>}
   */
  async submitSearch(currentPage = 1, pageSize = 10) {
    const {
      name,
      orderByDescending,
      orderByFields,
      searchDistrictGrade,
      searchOffered,
    } = this.state;
    const { city } = this.props;

    let offered;
    if (searchOffered === '1') offered = true;
    else if (searchOffered === '2') offered = false;

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictDto({
          name,
          cityDto: new CityDto({ id: city.id }),
          searchGrade:
            searchDistrictGrade === '0' ? undefined : searchDistrictGrade,
          searchOffered: offered,
        }),
      )
      .pageSize(pageSize)
      .pageIndex(currentPage - 1)
      .orderByDescending(orderByDescending)
      .orderByFields(orderByFields)
      .buildJson();
    this.props.actions.districtOptionListRequest(jsonList);
  }

  showEditModal = record => {
    this.setState({
      id: record.key,
      visibleEditModal: true,
      districtName: record.name,
      lat: record.latitude,
      lng: record.longitude,
      districtGrade: record ? record.grade : 'VIP',
      status: record.status,
      offered: record.offered,
    });
  };

  insertDistrict = () => {
    this.setState({
      id: 0,
      districtName: '',
      visibleEditModal: true,
      lat: 35.6891975,
      lng: 51.3889736,
      districtGrade: 'VIP',
    });
  };

  handleCancelModal = () => {
    this.setState({
      visibleEditModal: false,
    });
  };

  handleOkModal = () => {
    const {
      lat,
      lng,
      districtName,
      districtGrade,
      id,
      name,
      currentPage,
      pageSize,
      orderByDescending,
      orderByFields,
      status,
      offered,
    } = this.state;
    const { city } = this.props;
    if (districtName.length > 0) {
      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new DistrictDto({
            id,
            name: districtName,
            lat,
            lng,
            grade: districtGrade,
            cityDto: new CityDto({
              id: city.id,
            }),
            published: status,
            offered,
          }),
        )
        .build();

      /**
       * get all distrcit with city id
       */
      const jsonList = new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            name,
            cityDto: new CityDto({ id: city.id }),
          }),
        )
        .pageSize(pageSize)
        .pageIndex(currentPage - 1)
        .orderByDescending(orderByDescending)
        .orderByFields(orderByFields)
        .buildJson();

      const data = {
        data: jsonCrud,
        listDto: jsonList,
      };
      if (id === 0) this.props.actions.districtPostRequest(data);
      else this.props.actions.districtPutRequest(data);
      this.setState({
        visibleEditModal: false,
        districtGrade: 'VIP',
      });
    } else message.error('نام محله را وارد کنید.', 5);
  };

  handleTableChange = (pagination, filters, sorter) => {
    if (Object.keys(sorter).length > 0) {
      if (sorter.order === 'ascend') {
        this.setState(
          { orderByDescending: false, orderByFields: sorter.field },
          () => this.submitSearch(),
        );
      } else {
        this.setState(
          { orderByDescending: true, orderByFields: sorter.field },
          () => this.submitSearch(),
        );
      }
    }
  };

  onDelete = id => {
    const { city } = this.props;
    const {
      name,
      currentPage,
      pageSize,
      orderByDescending,
      orderByFields,
    } = this.state;

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictDto({
          name,
          cityDto: new CityDto({ id: city.id }),
        }),
      )
      .pageSize(pageSize)
      .pageIndex(currentPage - 1)
      .orderByDescending(orderByDescending)
      .orderByFields(orderByFields)
      .buildJson();

    const data = {
      id,
      listDto: jsonList,
    };
    this.props.actions.districtDeleteRequest(data);
  };
  render() {
    const {
      name,
      districtName,
      visibleEditModal,
      lat,
      lng,
      districtGrade,
      currentPage,
      status,
      offered,
      searchDistrictGrade,
      searchOffered,
    } = this.state;

    const { districts, districtLoading, districtCount } = this.props;
    const columns = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      { title: LATITUDE, dataIndex: 'latitude', key: 'latitude' },
      { title: LONGITUDE, dataIndex: 'longitude', key: 'longitude' },
      { title: GRADE, dataIndex: 'grade', key: 'grade', sorter: true },
      {
        title: STATUS,
        dataIndex: 'operationTwo22',
        width: 50,
        render: (text, record) => (
          <CPSwitch disabled defaultChecked={record.status} />
        ),
      },
      {
        title: OFFERED_IN_SEARCH,
        dataIndex: 'operationTwo222',
        width: 50,
        render: (text, record) => (
          <CPSwitch defaultChecked={record.offered} disabled />
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo2',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.showEditModal(record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </CPButton>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <CPPopConfirm
            title={ARE_YOU_SURE_WANT_TO_DELETE_THE_DISTRICT}
            okText={YES}
            cancelText={NO}
            okType="primary"
            onConfirm={() => this.onDelete(record.key)}
          >
            <CPButton className="delete_action">
              <i className="cp-trash" />
            </CPButton>
          </CPPopConfirm>
        ),
      },
    ];

    const districtArray = [];

    districts.map(item => {
      const obj = {
        key: item.id,
        name: item.name,
        latitude: item.lat,
        longitude: item.lng,
        grade: item.grade,
        status: item.published,
        offered: item.offered,
      };
      districtArray.push(obj);
      return null;
    });

    return (
      <div className={s.mainContent}>
        <CPList
          data={districtArray}
          columns={columns}
          loading={districtLoading}
          pagination={false}
          onAddClick={this.insertDistrict}
          classNameAddButton="addButton"
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <CPInput
                    name="name"
                    label={NAME}
                    value={name}
                    onChange={value => {
                      this.handelChangeInput('name', value.target.value);
                    }}
                  />
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{GRADE}</label>
                  <CPSelect
                    value={searchDistrictGrade}
                    onChange={value => {
                      this.handelChangeInput('searchDistrictGrade', value);
                    }}
                  >
                    <Option value="0" key="0">
                      {ALL}
                    </Option>
                    <Option value="VIP" key="VIP">
                      VIP
                    </Option>
                    <Option value="Gold" key="Gold">
                      Gold
                    </Option>
                    <Option value="Silver" key="Silver">
                      Silver
                    </Option>
                    <Option value="Boronz" key="Boronz">
                      Boronz
                    </Option>
                  </CPSelect>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label>{OFFERED_IN_SEARCH}</label>
                  <CPSelect
                    value={searchOffered}
                    onChange={value => {
                      this.handelChangeInput('searchOffered', value);
                    }}
                  >
                    <Option value="0" key="0">
                      {ALL}
                    </Option>
                    <Option value="1" key="1">
                      {ACTIVE}
                    </Option>
                    <Option value="2" key="2">
                      {DEACTIVATE}
                    </Option>
                  </CPSelect>
                </div>
              </div>
            </div>
            <div className="field">
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.submitSearch}
              />
            </div>
          </div>
        </CPList>
        <CPPagination
          total={districtCount}
          current={currentPage}
          onChange={this.onChangePagination}
          onShowSizeChange={this.onShowSizeChange}
          defaultPageSize={10}
        />
        <CPModal
          visible={visibleEditModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
          title={DISTRICT_EDIT}
          className="max_modal"
        >
          <div className="modalContent">
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{NAME}</label>
                <CPInput
                  name="districtName"
                  value={districtName}
                  onChange={value => {
                    this.handelChangeInput('districtName', value.target.value);
                  }}
                />
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{GRADE}</label>
                <CPSelect
                  value={districtGrade}
                  onChange={value => {
                    this.handelChangeInput('districtGrade', value);
                  }}
                >
                  <Option value="VIP" key="VIP">
                    VIP
                  </Option>
                  <Option value="Gold" key="Gold">
                    Gold
                  </Option>
                  <Option value="Silver" key="Silver">
                    Silver
                  </Option>
                  <Option value="Boronz" key="Boronz">
                    Boronz
                  </Option>
                </CPSelect>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{LATITUDE}</label>
                <CPInput
                  name="latitude"
                  value={lat.toString()}
                  onChange={value => {
                    this.handelChangeInput('lat', value.target.value);
                  }}
                />
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{LONGITUDE}</label>
                <CPInput
                  name="longitude"
                  value={lng.toString()}
                  onChange={value => {
                    this.handelChangeInput('lng', value.target.value);
                  }}
                />
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{STATUS}</label>
                <CPSwitch
                  defaultChecked={status}
                  checkedChildren={ACTIVE}
                  unCheckedChildren={DEACTIVATE}
                  onChange={value => {
                    this.handelChangeInput('status', value);
                  }}
                />
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{OFFERED_IN_SEARCH}</label>
                <CPSwitch
                  defaultChecked={offered}
                  checkedChildren={ACTIVE}
                  unCheckedChildren={DEACTIVATE}
                  onChange={value => {
                    this.handelChangeInput('offered', value);
                  }}
                />
              </div>
              <div className="col-sm-12">
                <label>{MAP}</label>
                <CPMap
                  googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
                  containerElement="500px"
                  loadingElement="100%"
                  mapElement="100%"
                  defaultZoom={12}
                  draggable
                  lat={lat}
                  lng={lng}
                  onDragEnd={value => this.setLatAndLng(value)}
                  searchBox
                />
              </div>
            </div>
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  city: state.singleCity.data ? state.singleCity.data.items[0] : null,
  districts: state.districtList.data ? state.districtList.data.items : [],
  districtCount: state.districtList.data ? state.districtList.data.count : 0,
  districtLoading: state.districtList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      districtOptionListRequest:
        districtOptionListActions.districtOptionListRequest,
      districtPutRequest: districtPutActions.districtPutRequest,
      districtPostRequest: districtPostActions.districtPostRequest,
      districtDeleteRequest: districtDeleteActions.districtDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CityDistricts));
