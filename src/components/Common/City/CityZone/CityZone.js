import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Select } from 'antd';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CityZone.css';
import CPInput from '../../../CP/CPInput/CPInput';
import CardTable from '../../../../components/CP/CPList/CPList';
import {
  ACTIVE,
  AGENT_BRANCHES,
  ALL,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_ZONE,
  ARROW_LEFT,
  ARROW_RIGHT,
  DEACTIVATE,
  DESCRIPTION,
  DISTRICT,
  DISTRICT_ON_MAP,
  DISTRICT_ZONE_MANAGE,
  GRADE,
  MAP,
  NAME,
  NO,
  PRODUCT_SPECIFICATION_ATTRIBUTE_INSERT,
  STATUS,
  WITH_ZONE,
  WITHOUT_ZONE,
  YES,
  ZONE_EDIT,
  ZONE_STATUS,
  ZONE_VENDOR_BRANCH_MANAGE,
  ZONES,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import {
  CityDto,
  DistrictDto,
  DistrictZoneDto,
  ZoneDto,
} from '../../../../dtos/commonDtos';
import districtZoneDeleteActions from '../../../../redux/common/actionReducer/district/DistrictZoneDelete';
import districtZoneListActions from '../../../../redux/common/actionReducer/district/DistrictZoneList';
import districtZoneNotContainListActions from '../../../../redux/common/actionReducer/district/DistrictZoneNotContainList';
import districtZonePostActions from '../../../../redux/common/actionReducer/district/DistrictZonePost';
import districtOptionListActions from '../../../../redux/common/actionReducer/district/List';
import CPModal from '../../../CP/CPModal';
import CPSwitch from '../../../CP/CPSwitch';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import zoneDeleteActions from '../../../../redux/common/actionReducer/zone/Delete';
import zonePostActions from '../../../../redux/common/actionReducer/zone/Post';
import zonePutActions from '../../../../redux/common/actionReducer/zone/Put';
import {
  getDistrictApi,
  getDistrictZoneApi,
} from '../../../../services/commonApi';
import { getDtoQueryString } from '../../../../utils/helper';
import CPMap from '../../../CP/CPMap';
import VendorBranchZone from '../VendorBranchZone';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import {
  VendorBranchDto,
  VendorBranchZoneDto,
} from '../../../../dtos/vendorDtos';
import { getVendorBranchZoneApi } from '../../../../services/vendorApi';
import vendorBranchZoneListActions from '../../../../redux/vendor/actionReducer/vendorBranchZone/List';
import CPSelect from '../../../CP/CPSelect';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder, BaseListCRUDDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPPagination from '../../../CP/CPPagination';

const { Option } = Select;

class CityDistricts extends React.Component {
  static propTypes = {
    city: PropTypes.objectOf(PropTypes.any),
    zones: PropTypes.arrayOf(PropTypes.object),
    zoneLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    districtZoneNotContain: PropTypes.arrayOf(PropTypes.object),
    districtZoneNotContainCount: PropTypes.number,
    districtZoneNotContainLoading: PropTypes.bool,
    districtZones: PropTypes.arrayOf(PropTypes.object),
    districtZoneLoading: PropTypes.bool,
  };

  static defaultProps = {
    districts: null,
    city: null,
    districtZoneNotContain: null,
    districtZoneNotContainCount: 0,
    districtZoneNotContainLoading: false,
    zones: null,
    districtZones: null,
    zoneLoading: false,
    districtZoneLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      visibleEditModal: false,
      visibleFullModal: false,
      visibleMapModal: false,
      visibleVendorBranchModal: false,
      lat: '',
      lng: '',
      description: '',
      status: true,
      id: 0,
      nameNotContain: '',
      nameContain: '',
      zoneId: 0,
      selectedDistrictIdsNotContain: [],
      selectedDistrictIdsContain: [],
      districtZoneStatusType: 'None',
      currentPage: 1,
      pageSize: 10,
    };
    this.submitSearchNotContain = this.submitSearchNotContain.bind(this);
  }

  onDelete = id => {
    const { city } = this.props;
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new ZoneDto({
          cityDto: new CityDto({ id: city.id }),
          active: true,
        }),
      )
      .buildJson();

    const data = {
      id,
      listDto: jsonList,
    };

    this.props.actions.zoneDeleteRequest(data);
  };

  onSelectedDistrictContain = selectedRowKeys => {
    this.setState({ selectedDistrictIdsContain: selectedRowKeys });
  };

  onSelectedDistrictNotContain = selectedRowKeys => {
    this.setState({ selectedDistrictIdsNotContain: selectedRowKeys });
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  /**
   * for search in district
   * @returns {Promise<void>}
   */
  async submitSearchNotContain() {
    const { nameNotContain, zoneId, districtZoneStatusType } = this.state;
    const { city, loginData } = this.props;

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictDto({
          cityDto: new CityDto({ id: city.id }),
          name: nameNotContain,
          expectedZoneIds: false,
          districtZoneDtos: [{ zone: { id: zoneId } }],
          districtZoneStatusType,
        }),
      )
      .includes(['districtZoneDtos'])
      .pageSize(10)
      .pageIndex(0)
      .buildJson();
    const districtZoneNotContain = await getDistrictApi(
      loginData.token,
      getDtoQueryString(jsonList),
    );

    if (districtZoneNotContain.status === 200)
      this.props.actions.districtZoneNotContainListSuccess(
        districtZoneNotContain.data,
      );
  }

  submitSearchContain = () => {
    const { nameContain, zoneId } = this.state;

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictZoneDto({
          district: new DistrictDto({ name: nameContain }),
          zone: new ZoneDto({ id: zoneId }),
        }),
      )
      .buildJson();

    this.props.actions.districtZoneListRequest(jsonList);
  };

  showEditModal(record) {
    this.setState({
      visibleEditModal: true,
      name: record ? record.name : '',
      description: record ? record.description : '',
      status: record ? record.status : true,
      id: record ? record.key : 0,
    });
  }

  async showFullModal(record) {
    const { city } = this.props;
    this.setState({
      zoneId: record.key,
      name: record.name,
      nameNotContain: '',
      nameContain: '',
      visibleFullModal: true,
      pageSize: 10,
      currentPage: 1,
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictZoneDto({
          zone: new ZoneDto({ id: record.key }),
        }),
      )
      .buildJson();

    this.props.actions.districtZoneListRequest(jsonList);

    const jsonListDistrict = new BaseGetDtoBuilder()
      .dto(
        new DistrictDto({
          cityDto: new CityDto({ id: city.id }),
          expectedZoneIds: false,
          districtZoneDtos: [{ zone: { id: record.key } }],
        }),
      )
      .includes(['districtZoneDtos'])
      .pageSize(10)
      .pageIndex(0)
      .buildJson();

    this.props.actions.districtZoneNotContainListRequest(jsonListDistrict);
  }

  async showMapModal(record) {
    const { loginData } = this.props;

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictZoneDto({
          zone: new ZoneDto({ id: record.key }),
        }),
      )
      .buildJson();

    const districtZones = await getDistrictZoneApi(
      loginData.token,
      getDtoQueryString(jsonList),
    );

    if (districtZones.status === 200) {
      this.props.actions.districtZoneListSuccess(districtZones.data);

      this.setState({
        visibleMapModal: true,
      });
    }

  }

  async showVendorBranchModal(record) {
    const { loginData, city } = this.props;
    this.setState({ visibleVendorBranchModal: true, zoneId: record.key });

    /**
     * get all vendor branch zone for first time with default clause
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchZoneDto({
          zoneDto: new ZoneDto({ id: record.key }),
        }),
      )
      .includes(['districtDto', 'vendorDto'])
      .buildJson();

    const vendorBranchZones = await getVendorBranchZoneApi(
      loginData.token,
      getDtoQueryString(jsonList),
    );

    this.props.actions.vendorBranchZoneListSuccess(vendorBranchZones.data);
    const vendorBranchZoneIds = [];

    if (vendorBranchZones.status === 200 && vendorBranchZones.data.items)
      vendorBranchZones.data.items.map(item =>
        vendorBranchZoneIds.push(item.vendorBranchDto.id),
      );

    /**
     * get all vendor branch for first time with default clause
     */
    const jsonListVendorBranch = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchDto({
          // active: true,
          cityId: city.id,
        }),
      )
      .notExpectedIds(vendorBranchZoneIds)
      .includes(['districtDto', 'vendorDto'])
      .buildJson();
    this.props.actions.vendorBranchListRequest(jsonListVendorBranch);
    /**
     * get all district without district zone
     */
    const jsonListDistrict = new BaseGetDtoBuilder()
      .dto(
        new DistrictDto({
          cityDto: new CityDto({ id: city.id }),
          expectedZoneIds: false,
          districtZoneDtos: [{ zone: { id: record.key } }],
        }),
      )
      .buildJson();

    this.props.actions.districtOptionListRequest(jsonListDistrict);
  }

  handleCancelModal = () => {
    this.setState({
      visibleEditModal: false,
    });
  };

  handleCancelFullModal = () => {
    this.setState({
      visibleFullModal: false,
    });
  };

  handleCancelMapModal = () => {
    this.setState({
      visibleMapModal: false,
    });
  };

  handleCancelVendorBranchModal = () => {
    this.setState({
      visibleVendorBranchModal: false,
    });
  };

  handleOkModal = () => {
    const { description, status, name, id } = this.state;
    const { city } = this.props;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new ZoneDto({
          cityDto: new CityDto({ id: city.id }),
          name,
          description,
          active: status,
          id,
        }),
      )
      .build();

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new ZoneDto({
          cityDto: new CityDto({ id: city.id }),
          active: true,
        }),
      )
      .buildJson();

    const data = {
      data: jsonCrud,
      listDto: jsonList,
    };

    if (id > 0) this.props.actions.zonePutRequest(data);
    else this.props.actions.zonePostRequest(data);

    this.setState({ visibleEditModal: false });
  };

  insertDistrictZone = () => {
    const {
      selectedDistrictIdsNotContain,
      zoneId,
      nameContain,
      nameNotContain,
      districtZoneStatusType,
    } = this.state;
    const { city } = this.props;

    const districtZones = [];

    selectedDistrictIdsNotContain.map(item => {
      districtZones.push(
        new DistrictZoneDto({
          district: new DistrictDto({ id: item }),
          zone: new ZoneDto({ id: zoneId }),
        }),
      );
      return null;
    });

    const baseListCRUDDtoTemp = new BaseListCRUDDtoBuilder().dtos(districtZones).build();

    /**
     * get districtNotContain after insert for reload list
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictDto({
          cityDto: new CityDto({ id: city.id }),
          name: nameNotContain,
          expectedZoneIds: false,
          districtZoneDtos: [{ zone: { id: zoneId } }],
          districtZoneStatusType,
        }),
      )
      .includes(['districtZoneDtos'])
      .buildJson();

    /**
     * get districtZone after insert for reload list
     */
    const jsonListDistrictZone = new BaseGetDtoBuilder()
      .dto(
        new DistrictZoneDto({
          district: new DistrictDto({ name: nameContain }),
          zone: new ZoneDto({ id: zoneId }),
        }),
      )
      .buildJson();

    const data = {
      data: baseListCRUDDtoTemp,
      districtListDto: jsonList,
      districtZoneListDto: jsonListDistrictZone,
    };
    this.props.actions.districtZonePostRequest(data);
    this.setState({ selectedDistrictIdsNotContain: [] });
  };

  DeleteDistrictZone = () => {
    const {
      selectedDistrictIdsContain,
      districtZoneStatusType,
      nameContain,
      zoneId,
      nameNotContain,
    } = this.state;
    const districtZoneIds = [];
    const districtIds = [];
    const { city } = this.props;

    selectedDistrictIdsContain.map(item => {
      districtZoneIds.push(item.split('*')[0]);
      districtIds.push(parseInt(item.split('*')[1], 0));
      return null;
    });

    /**
     * get districtNotContain after insert for reload list
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictDto({
          cityDto: new CityDto({ id: city.id }),
          name: nameNotContain,
          expectedZoneIds: false,
          districtZoneDtos: [{ zone: { id: zoneId } }],
          districtZoneStatusType,
        }),
      )
      .includes(['districtZoneDtos'])
      .buildJson();

    /**
     * get districtZone after insert for reload list
     */
    const jsonListDistrictZone = new BaseGetDtoBuilder()
      .dto(
        new DistrictZoneDto({
          district: new DistrictDto({ name: nameContain }),
          zone: new ZoneDto({ id: zoneId }),
        }),
      )
      .buildJson();

    const data = {
      data: districtZoneIds,
      districtListDto: jsonList,
      districtZoneListDto: jsonListDistrictZone,
    };

    this.props.actions.districtZoneDeleteRequest(data);
    this.setState({ selectedDistrictIdsContain: [] });
  };

  onChangePagination = page => {
    const { city } = this.props;
    const { pageSize, currentPage, zoneId } = this.state;
    this.setState({ currentPage: page }, () => {
      const jsonListDistrict = new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityDto: new CityDto({ id: city.id }),
            expectedZoneIds: false,
            districtZoneDtos: [{ zone: { id: zoneId } }],
          }),
        )
        .includes(['districtZoneDtos'])
        .pageSize(pageSize)
        .pageIndex(currentPage - 1)
        .buildJson();

      this.props.actions.districtZoneNotContainListRequest(jsonListDistrict);
    });
  };

  onShowSizeChange = (current, pageSize) => {
    const { city } = this.props;
    const { zoneId } = this.state;
    this.setState({ currentPage: current, pageSize }, () => {
      const jsonListDistrict = new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityDto: new CityDto({ id: city.id }),
            expectedZoneIds: false,
            districtZoneDtos: [{ zone: { id: zoneId } }],
          }),
        )
        .includes(['districtZoneDtos'])
        .pageSize(pageSize)
        .pageIndex(current - 1)
        .buildJson();

      this.props.actions.districtZoneNotContainListRequest(jsonListDistrict);
    });
  };

  render() {
    const {
      name,
      description,
      status,
      visibleEditModal,
      nameNotContain,
      nameContain,
      visibleFullModal,
      selectedDistrictIdsNotContain,
      selectedDistrictIdsContain,
      visibleMapModal,
      visibleVendorBranchModal,
      districtZoneStatusType,
      currentPage,
    } = this.state;
    const {
      city,
      zones,
      zoneLoading,
      districtZoneNotContain,
      districtZoneNotContainCount,
      districtZoneNotContainLoading,
      districtZones,
      districtZoneLoading,
    } = this.props;
    const columns = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      { title: DESCRIPTION, dataIndex: 'description', key: 'description' },
      {
        title: STATUS,
        dataIndex: 'status',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.status} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operation',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.showEditModal(record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </CPButton>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.showFullModal(record)}
              className="edit_action"
            >
              {DISTRICT}
            </CPButton>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationThree',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.showMapModal(record)}
              className="edit_action"
            >
              {MAP}
            </CPButton>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationFour',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.showVendorBranchModal(record)}
              className="edit_action"
            >
              {AGENT_BRANCHES}
            </CPButton>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_ZONE}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
    ];
    const columnsModal = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      { title: GRADE, dataIndex: 'districtGrade', key: 'districtGrade' },
      { title: ZONES, dataIndex: 'zones', key: 'zones' },
    ];
    const districtZoneArray = [];
    const districtZoneNotContainArray = [];
    const zoneArray = [];
    const rowSelectionContain = {
      selectedRowKeys: selectedDistrictIdsContain,
      onChange: this.onSelectedDistrictContain,
    };
    const rowSelectionNotContain = {
      selectedRowKeys: selectedDistrictIdsNotContain,
      onChange: this.onSelectedDistrictNotContain,
    };

    zones.map(item => {
      const obj = {
        key: item.id,
        name: item.name,
        status: item.active,
        description: item.description,
      };
      zoneArray.push(obj);
      return null;
    });

    if (districtZones)
      districtZones.map(item => {
        const obj = {
          key: `${item.id}*${item.district.id}`,
          name: item.district.name,
          lat: item.district.lat,
          lng: item.district.lng,
          districtGrade: item.district.grade,
        };
        districtZoneArray.push(obj);
        return null;
      });

    if (districtZoneNotContain)
      districtZoneNotContain.map(item => {
        const zonesArray = [];
        if (item.districtZoneDtos)
          item.districtZoneDtos.map(item2 => zonesArray.push(item2.zone.name));
        const obj = {
          key: item.id,
          name: item.name,
          districtGrade: item.grade,
          zones: zonesArray.toString(),
        };
        districtZoneNotContainArray.push(obj);
        return null;
      });

    return (
      <div className={s.mainContent}>
        <CardTable
          onAddClick={() => this.showEditModal(null)}
          data={zoneArray}
          columns={columns}
          loading={zoneLoading}
          hideSearchBox
        />
        <CPModal
          visible={visibleEditModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
          title={ZONE_EDIT}
        >
          <div className="modalContent">
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{NAME}</label>
                <CPInput
                  name="name"
                  value={name}
                  onChange={value => {
                    this.handelChangeInput('name', value.target.value);
                  }}
                />
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{DESCRIPTION}</label>
                <CPInput
                  name="description"
                  value={description}
                  onChange={value => {
                    this.handelChangeInput('description', value.target.value);
                  }}
                />
              </div>

              <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="align-center">
                  <CPSwitch
                    defaultChecked={status}
                    checkedChildren={ACTIVE}
                    unCheckedChildren={DEACTIVATE}
                    onChange={value => this.handelChangeInput('status', value)}
                  />
                </div>
              </div>
            </div>
          </div>
        </CPModal>
        <CPModal
          visible={visibleFullModal}
          handleCancel={this.handleCancelFullModal}
          title={`${DISTRICT_ZONE_MANAGE} ${name}`}
          className="max_modal"
          showFooter={false}
        >
          <div className="modalContent">
            <div className="row">
              <div className="col-md-5 col-sm-12">
                <CardTable
                  data={districtZoneNotContainArray}
                  columns={columnsModal}
                  loading={districtZoneNotContainLoading}
                  withCheckBox
                  rowSelection={rowSelectionNotContain}
                  hideAdd
                  pagination={false}
                >
                  <div className="searchBox">
                    <div className={s.searchFildes}>
                      <div className="row">
                        <div className="col-md-6 col-sm-12">
                          <CPInput
                            name="name"
                            label={NAME}
                            value={nameNotContain}
                            onChange={value => {
                              this.handelChangeInput(
                                'nameNotContain',
                                value.target.value,
                              );
                            }}
                          />
                        </div>
                        <div className="col-md-6 col-sm-12">
                          <label>{ZONE_STATUS}</label>
                          <CPSelect
                            value={districtZoneStatusType}
                            onChange={value => {
                              this.handelChangeInput(
                                'districtZoneStatusType',
                                value,
                              );
                            }}
                          >
                            <Option value="None" key="None">
                              {ALL}
                            </Option>
                            <Option value="WithoutZone" key="WithoutZone">
                              {WITHOUT_ZONE}
                            </Option>
                            <Option value="WithZone" key="WithZone">
                              {WITH_ZONE}
                            </Option>
                          </CPSelect>
                        </div>
                      </div>
                    </div>
                    <div className="field">
                      <CPButton
                        size="large"
                        shape="circle"
                        type="primary"
                        icon="search"
                        onClick={this.submitSearchNotContain}
                      />
                    </div>
                  </div>
                </CardTable>
                <CPPagination
                  total={districtZoneNotContainCount}
                  current={currentPage}
                  onChange={this.onChangePagination}
                  onShowSizeChange={this.onShowSizeChange}
                  defaultPageSize={10}
                />
              </div>
              <div className="col-md-2 col-sm-12">
                <div className="row" style={{ marginTop: 200 }}>
                  <div className="col-sm-6">
                    <CPButton onClick={this.insertDistrictZone}>
                      {ARROW_RIGHT}
                    </CPButton>
                  </div>
                  <div className="col-sm-6">
                    <CPButton onClick={this.DeleteDistrictZone}>
                      {ARROW_LEFT}
                    </CPButton>
                  </div>
                </div>
              </div>
              <div className="col-sm-5 col-xs-12">
                <CardTable
                  data={districtZoneArray}
                  columns={columnsModal}
                  loading={districtZoneLoading}
                  withCheckBox
                  rowSelection={rowSelectionContain}
                  hideAdd
                >
                  <div className="searchBox">
                    <div className={s.searchFildes}>
                      <div className="row">
                        <div className="col-md-6 col-sm-12">
                          <CPInput
                            name="name"
                            label={NAME}
                            value={nameContain}
                            onChange={value => {
                              this.handelChangeInput(
                                'nameContain',
                                value.target.value,
                              );
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="field">
                      <CPButton
                        size="large"
                        shape="circle"
                        type="primary"
                        icon="search"
                        onClick={this.submitSearchContain}
                      />
                    </div>
                  </div>
                </CardTable>
              </div>
            </div>
          </div>
        </CPModal>
        <CPModal
          visible={visibleMapModal}
          handleCancel={this.handleCancelMapModal}
          title={DISTRICT_ON_MAP}
          className="max_modal"
          showFooter={false}
        >
          <div className="modalContent">
            <CPMap
              containerElement="500px"
              defaultCenter={{ lat: city.lat, lng: city.lng }}
              defaultZoom={10}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
              mapElement="100%"
              markers={districtZoneArray}
              loadingElement="100%"
            />
          </div>
        </CPModal>
        <CPModal
          visible={visibleVendorBranchModal}
          handleCancel={this.handleCancelVendorBranchModal}
          title={`${ZONE_VENDOR_BRANCH_MANAGE} ${name}`}
          className="max_modal"
          showFooter={false}
        >
          <VendorBranchZone zoneId={this.state.zoneId} />
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  city: state.singleCity.data ? state.singleCity.data.items[0] : null,
  districts: state.districtList.data ? state.districtList.data.items : [],
  districtZones: state.districtZoneList.data
    ? state.districtZoneList.data.items
    : [],
  districtZoneLoading: state.districtZoneList.loading,
  districtZoneNotContain: state.districtZoneNotContainList.data
    ? state.districtZoneNotContainList.data.items
    : [],
  districtZoneNotContainCount: state.districtZoneNotContainList.data
    ? state.districtZoneNotContainList.data.count
    : 0,
  districtZoneNotContainLoading: state.districtZoneNotContainList.loading,
  zones: state.zoneList.data ? state.zoneList.data.items : [],
  zoneLoading: state.zoneList.loading,
  loginData: state.login.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      districtOptionListRequest:
        districtOptionListActions.districtOptionListRequest,
      zonePutRequest: zonePutActions.zonePutRequest,
      zonePostRequest: zonePostActions.zonePostRequest,
      zoneDeleteRequest: zoneDeleteActions.zoneDeleteRequest,
      districtZoneListSuccess: districtZoneListActions.districtZoneListSuccess,
      districtZoneNotContainListRequest:
        districtZoneNotContainListActions.districtZoneNotContainListRequest,
      districtZoneNotContainListSuccess:
        districtZoneNotContainListActions.districtZoneNotContainListSuccess,
      districtZoneListRequest: districtZoneListActions.districtZoneListRequest,
      districtZoneDeleteRequest:
        districtZoneDeleteActions.districtZoneDeleteRequest,
      districtZonePostRequest: districtZonePostActions.districtZonePostRequest,
      vendorBranchListRequest: vendorBranchListActions.vendorBranchListRequest,
      vendorBranchZoneListSuccess:
        vendorBranchZoneListActions.vendorBranchZoneListSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CityDistricts));
