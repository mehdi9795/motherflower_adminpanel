import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorBranchZone.css';
import CPInput from '../../../CP/CPInput/CPInput';
import {
  ACTIVE,
  ADD_TO_ZONE,
  AGENT_NAME,
  ALL,
  ALL_CITIES,
  ALL_DISTRICT,
  ALL_VENDORS,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR_BRANCH,
  CITY,
  DEACTIVATE,
  DISTRICT,
  GRADE,
  NAME,
  NO,
  STATUS,
  VENDOR_BRANCH_NAME,
  YES,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import CPSwitch from '../../../CP/CPSwitch';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import CPList from '../../../CP/CPList';
import {
  VendorBranchDto,
  VendorBranchZoneDto,
} from '../../../../dtos/vendorDtos';
import { DistrictDto, ZoneDto } from '../../../../dtos/commonDtos';
import CPSelect from '../../../CP/CPSelect';
import vendorBranchZoneDeleteActions from '../../../../redux/vendor/actionReducer/vendorBranchZone/Delete';
import vendorBranchZonePostActions from '../../../../redux/vendor/actionReducer/vendorBranchZone/Post';
import vendorBranchListActions from '../../../../redux/vendor/actionReducer/vendorBranch/List';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import { pressEnterAndCallApplySearch } from '../../../../utils/helper';

const { Option } = Select;

class VendorBranchZone extends React.Component {
  static propTypes = {
    vendorBranchLoading: PropTypes.bool,
    vendorBranchZoneLoading: PropTypes.bool,
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    vendorBranchZones: PropTypes.arrayOf(PropTypes.object),
    vendors: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    vendorBranchCount: PropTypes.number,
    vendorBranchZoneCount: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    districtOptions: PropTypes.objectOf(PropTypes.any),
    zoneId: PropTypes.number,
    city: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    districtOptions: [],
    vendorBranches: [],
    vendorBranchZones: [],
    vendors: [],
    cities: [],
    vendorBranchLoading: false,
    vendorBranchZoneLoading: false,
    vendorBranchCount: 0,
    vendorBranchZoneCount: 0,
    zoneId: 0,
    city: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      vendorId: '0',
      cityId: '0',
      districtId: '0',
      vendorBranchStatus: true,
      districtValue: '0',
      vendorBranchGrade: 'all',
    };

    this.submitSearch = this.submitSearch.bind(this);
  }

  componentDidMount() {
    pressEnterAndCallApplySearch(this.submitSearch);
  }

  onDelete = record => {
    const { vendorBranchZones, zoneId } = this.props;

    /**
     * get all vendor branch zone for first time with default clause
     */
    const baseGetDtoTemp1 = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchZoneDto({
          zoneDto: new ZoneDto({ id: zoneId }),
        }),
      )
      .includes(['districtDto', 'vendorDto'])
      .buildJson();

    const vendorBranchZoneIds = [];

    vendorBranchZones.map(item =>
      vendorBranchZoneIds.push(item.vendorBranchDto.id),
    );
    const index = vendorBranchZoneIds.indexOf(record.vendorBranchId);
    if (index !== -1) vendorBranchZoneIds.splice(index);
    /**
     * get all vendor branch for first time with default clause
     */

    const baseGetDtoTemp2 = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchDto({
          active: true,
        }),
      )
      .notExpectedIds(vendorBranchZoneIds)
      .includes(['districtDto', 'vendorDto'])
      .buildJson();

    const data = {
      id: record.key,
      vendorBranchListDto: baseGetDtoTemp2,
      vendorBranchZoneListDto: baseGetDtoTemp1,
    };

    this.props.actions.vendorBranchZoneDeleteRequest(data);
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });

    if ([inputName][0] === 'districtId')
      this.setState({
        districtValue: value,
      });
  };

  /**
   * change city for automatic change district
   * @param inputName
   * @param value
   * @returns {Promise<void>}
   */
  async cityChange(inputName, value) {
    this.props.actions.districtOptionListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityId: value,
          }),
        )
        .buildJson(),
    );
    this.setState({
      [inputName]: value,
    });
  }

  /**
   * for search in vendor branch
   * @returns {Promise<void>}
   */
  async submitSearch() {
    const {
      vendorBranchStatus,
      name,
      cityId,
      vendorId,
      districtId,
      vendorBranchGrade,
    } = this.state;
    const { city } = this.props;
    /**
     * get vendor branch with where
     */
    const baseGetDtoTemp = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchDto({
          active: vendorBranchStatus,
          name,
          vendorBranchGrade:
            vendorBranchGrade !== 'all' ? vendorBranchGrade : undefined,
          cityId: city.id,
          districtId,
          vendorId,
        }),
      )
      .includes(['districtDto', 'vendorDto'])
      .buildJson();

    this.props.actions.vendorBranchListRequest(baseGetDtoTemp);
  }

  insertToVendorBranchZone = record => {
    const { vendorBranchZones, zoneId } = this.props;

    const baseCRUDDtoTemp = new BaseCRUDDtoBuilder()
      .dto(
        new VendorBranchZoneDto({
          zoneDto: new ZoneDto({ id: zoneId }),
          vendorBranchDto: new VendorBranchDto({ id: record.key }),
        }),
      )
      .build();

    /**
     * get all vendor branch zone for first time with default clause
     */
    const baseGetDtoTemp1 = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchZoneDto({
          id: zoneId,
        }),
      )
      .includes(['districtDto', 'vendorDto'])
      .buildJson();

    const vendorBranchZoneIds = [];

    vendorBranchZones.map(item =>
      vendorBranchZoneIds.push(item.vendorBranchDto.id),
    );
    vendorBranchZoneIds.push(record.key);
    /**
     * get all vendor branch for first time with default clause
     */
    const baseGetDtoTemp2 = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchDto({
          active: true,
        }),
      )
      .notExpectedIds(vendorBranchZoneIds)
      .includes(['districtDto', 'vendorDto'])
      .buildJson();

    const data = {
      data: baseCRUDDtoTemp,
      vendorBranchListDto: baseGetDtoTemp2,
      vendorBranchZoneListDto: baseGetDtoTemp1,
    };

    this.props.actions.vendorBranchZonePostRequest(data);
  };

  render() {
    const {
      vendorBranches,
      vendorBranchZones,
      vendorBranchLoading,
      vendorBranchZoneLoading,
      cities,
      vendors,
      vendorBranchCount,
      vendorBranchZoneCount,
      districtOptions,
    } = this.props;

    const {
      districtValue,
      name,
      vendorId,
      cityId,
      districtId,
      vendorBranchStatus,
      vendorBranchGrade,
    } = this.state;

    const columnsVendorBranchZone = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      { title: CITY, dataIndex: 'city', key: 'city' },
      { title: DISTRICT, dataIndex: 'district', key: 'district' },
      { title: AGENT_NAME, dataIndex: 'vendor', key: 'vendor' },
      {
        title: GRADE,
        dataIndex: 'vendorBranchGrade',
        key: 'vendorBranchGrade',
      },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR_BRANCH}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record)}
            >
              <CPButton className="delete_action" disabled={!record.deletable}>
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
    ];
    const columnsVendorBranch = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      { title: CITY, dataIndex: 'city', key: 'city' },
      { title: DISTRICT, dataIndex: 'district', key: 'district' },
      { title: AGENT_NAME, dataIndex: 'vendor', key: 'vendor' },
      {
        title: GRADE,
        dataIndex: 'vendorBranchGrade',
        key: 'vendorBranchGrade',
      },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton onClick={() => this.insertToVendorBranchZone(record)}>
              {ADD_TO_ZONE}
            </CPButton>
          </div>
        ),
      },
    ];
    const vendorBranchDataSource = [];
    const vendorBranchZoneDataSource = [];

    /**
     * maping vendor Branches for list data source
     */
    if (vendorBranches)
      vendorBranches.map(item => {
        const obj = {
          key: item.id,
          name: item.name,
          vendor: item.vendorDto ? item.vendorDto.name : '',
          city:
            item.districtDto && item.districtDto.cityDto
              ? item.districtDto.cityDto.name
              : '',
          district: item.districtDto ? item.districtDto.name : '',
          active: item.active,
          vendorBranchGrade: item.vendorBranchGrade,
        };
        return vendorBranchDataSource.push(obj);
      });

    /**
     * maping vendor Branch zone for list data source
     */
    if (vendorBranchZones)
      vendorBranchZones.map(item => {
        const obj = {
          key: item.id,
          vendorBranchId: item.vendorBranchDto.id,
          name: item.vendorBranchDto.name,
          vendor: item.vendorBranchDto.vendorDto.name,
          city: item.vendorBranchDto.districtDto.cityDto.name,
          district: item.vendorBranchDto.districtDto.name,
          active: item.vendorBranchDto.active,
          vendorBranchGrade: item.vendorBranchDto.vendorBranchGrade,
          deletable: item.deletable,
        };
        return vendorBranchZoneDataSource.push(obj);
      });

    const vendorDataSource = [];
    const citiesDataSource = [];
    const districtDataSource = [];

    /**
     * map vendors for comboBox Datasource
     */
    if (vendors)
      vendors.map(item =>
        vendorDataSource.push(<Option key={item.id}>{item.name}</Option>),
      );

    /**
     * map cities for comboBox Datasource
     */
    if (cities)
      cities.map(item =>
        citiesDataSource.push(<Option key={item.id}>{item.name}</Option>),
      );

    /**
     * map district for comboBox Datasource
     */
    if (districtOptions)
      districtOptions.items.map(item =>
        districtDataSource.push(<Option key={item.id}>{item.name}</Option>),
      );

    return (
      <div className="modalContent">
        <div className="col-sm-12">
          <CPList
            data={vendorBranchZoneDataSource}
            columns={columnsVendorBranchZone}
            count={vendorBranchZoneCount}
            loading={vendorBranchZoneLoading}
            hideAdd
            hideSearchBox
          />
        </div>
        <div className="col-sm-12">
          <CPList
            data={vendorBranchDataSource}
            columns={columnsVendorBranch}
            count={vendorBranchCount}
            loading={vendorBranchLoading}
            hideAdd
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="name"
                      label={VENDOR_BRANCH_NAME}
                      value={name}
                      onChange={value => {
                        this.handelChangeInput('name', value.target.value);
                      }}
                    />
                  </div>
                  {/* <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{AGENT_NAME}</label>
                    <CPSelect
                      name="vendorId"
                      defaultValue={vendorId}
                      showSearch
                      onChange={value => {
                        this.handelChangeInput('vendorId', value);
                      }}
                    >
                      <Option key={0}>{ALL_VENDORS}</Option>
                      {vendorDataSource}
                    </CPSelect>
                  </div> */}
                  {/* <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{CITY}</label>
                    <CPSelect
                      name="cityId"
                      defaultValue={cityId}
                      showSearch
                      onChange={value => {
                        this.cityChange('cityId', value);
                      }}
                    >
                      <Option key={0}>{ALL_CITIES}</Option>
                      {citiesDataSource}
                    </CPSelect>
                  </div> */}
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{DISTRICT}</label>
                    <CPSelect
                      defaultValue={districtId}
                      value={districtValue}
                      showSearch
                      onChange={value => {
                        this.handelChangeInput('districtId', value);
                      }}
                    >
                      <Option key={0}>{ALL_DISTRICT}</Option>
                      {districtDataSource}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{GRADE}</label>
                    <CPSelect
                      value={vendorBranchGrade}
                      onChange={value => {
                        this.handelChangeInput('vendorBranchGrade', value);
                      }}
                    >
                      <Option value="all" key="all">
                        {ALL}
                      </Option>
                      <Option value="VIP" key="VIP">
                        VIP
                      </Option>
                      <Option value="Gold" key="Gold">
                        Gold
                      </Option>
                      <Option value="Silver" key="Silver">
                        Silver
                      </Option>
                      <Option value="Boronz" key="Boronz">
                        Boronz
                      </Option>
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPSwitch
                      defaultChecked={vendorBranchStatus}
                      checkedChildren={ACTIVE}
                      unCheckedChildren={DEACTIVATE}
                      onChange={value => {
                        this.handelChangeInput('vendorBranchStatus', value);
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="field">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  vendorBranches: state.vendorBranchList.data.items,
  vendorBranchCount: state.vendorBranchList.data.count,
  vendorBranchLoading: state.vendorBranchList.loading,
  vendorBranchZones: state.vendorBranchZoneList.data.items,
  vendorBranchZoneCount: state.vendorBranchZoneList.data.count,
  vendorBranchZoneLoading: state.vendorBranchZoneList.loading,
  vendors: state.vendorList.data.items,
  cities: state.cityList.data.items,
  districtOptions: state.districtList.data,
  city: state.singleCity.data ? state.singleCity.data.items[0] : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorBranchListRequest: vendorBranchListActions.vendorBranchListRequest,
      vendorBranchZonePostRequest:
        vendorBranchZonePostActions.vendorBranchZonePostRequest,
      vendorBranchZoneDeleteRequest:
        vendorBranchZoneDeleteActions.vendorBranchZoneDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchZone));
