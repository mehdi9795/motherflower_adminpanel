import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CityInfo.css';
import CPInput from '../../../CP/CPInput/CPInput';
import {
  ACTIVE,
  CITY_NAME,
  DEACTIVATE,
} from '../../../../Resources/Localization';
import CPSwitch from '../../../CP/CPSwitch';

class CityInfo extends React.Component {
  static propTypes = {
    city: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    categories: null,
    city: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: props.city ? props.city.name : '',
      status: props.city ? props.city.active : true,
    };
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  render() {
    const { name, status } = this.state;

    return (
      <div>
        <div className="tabContent">
          <div className="row">
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <CPInput
                label={CITY_NAME}
                value={name}
                onChange={value =>
                  this.handelChangeInput('name', value.target.value)
                }
              />
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <div className="align-center">
                <CPSwitch
                  defaultChecked={status}
                  checkedChildren={ACTIVE}
                  unCheckedChildren={DEACTIVATE}
                  onChange={value => this.handelChangeInput('status', value)}
                />
              </div>
            </div>
          </div>
        </div>
        {/* <div className={s.textLeft}> */}
        {/* <CPButton icon="save" onClick={this.saveForm}> */}
        {/* {FORM_SAVE} */}
        {/* </CPButton>{' '} */}
        {/* <CPButton icon="form" className="btn-primary" onClick={this.saveFormAndContinue}> */}
        {/* {FORM_SAVE_AND_CONTINUE} */}
        {/* </CPButton> */}
        {/* </div> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  city: state.singleCity.data ? state.singleCity.data.items[0] : null,
});

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CityInfo));
