import React from 'react';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { CITY_INFO, DISTRICT, ZONES } from '../../../../Resources/Localization';
import s from './CityForm.css';
import CityInfo from '../CityInfo';
import CityDistricts from '../CityDistricts';
import CityZone from '../CityZone';

class CityForm extends React.Component {
  static propTypes = {
    city: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    city: null,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    const { city } = this.props;
    const isEdit = city && city.id;
    return (
      <Tabs>
        <TabList>
          <Tab>{CITY_INFO}</Tab>
          {isEdit && <Tab>{DISTRICT}</Tab>}
          {isEdit && <Tab>{ZONES}</Tab>}
        </TabList>
        <TabPanel>
          <CityInfo />
        </TabPanel>
        {isEdit && (
          <TabPanel>
            <CityDistricts />
          </TabPanel>
        )}
        {isEdit && (
          <TabPanel>
            <CityZone city={city} />
          </TabPanel>
        )}
      </Tabs>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CityForm));
