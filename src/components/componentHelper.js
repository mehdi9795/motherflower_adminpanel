import React from 'react';
import { Select } from 'antd';

const { Option } = Select;

export class OptionBuilder {
  constructor(key, value, array = []) {
    this.key = key;
    this.value = value;
    this.array = array;
  }

  buildAndPush() {
    return this.array.push(<Option key={this.key}>{this.value}</Option>);
  }

  build() {
    return <Option key={this.key}>{this.value}</Option>;
  }
}
