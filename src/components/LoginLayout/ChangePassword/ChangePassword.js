import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { setCookie } from '../../../utils/index';
import s from './ChangePassword.css';
import CPInput from '../../CP/CPInput';
import CPButton from '../../CP/CPButton';
import {
  LOGIN,
  CONFIRMATION,
  NEW_PASSWORD,
  REENTER_NEW_PASSWORD,
} from '../../../Resources/Localization';
import { ChangePasswordDto } from '../../../dtos/identityDtos';
import { ChangePasswordApi } from '../../../services/identityApi';
import loginActions from '../../../redux/identity/actionReducer/account/Login';
import CPValidator from '../../CP/CPValidator';
import { BaseCRUDDtoBuilder } from '../../../dtos/dtoBuilder';

class ChangePassword extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirmPassword: '',
      confirmPasswordClass: 'bar',
      showMessage: false,
      showMessageConfirm: false,
    };

    this.changePassword = this.changePassword.bind(this);
    this.validation = [];
  }

  async changePassword() {
    const { confirmPassword, password } = this.state;
    const { signUpData } = this.props;
    if (this.validation.length === 1 && confirmPassword === password) {
      const response = await ChangePasswordApi(
        new BaseCRUDDtoBuilder()
          .dto(
            new ChangePasswordDto({
              userName: signUpData.phone,
              newPassword: password,
              newConfirmPassword: confirmPassword,
            }),
          )
          .build(),
      );
      if (response.data.sessionId) {
        const authenticationCookieExpireHours = 8;
        setCookie(
          'siteToken',
          response.data.sessionId,
          authenticationCookieExpireHours,
        );
        setCookie(
          'siteUserName',
          response.data.userName,
          authenticationCookieExpireHours,
        );
        setCookie(
          'siteDisplayName',
          response.data.displayName,
          authenticationCookieExpireHours,
        );

        const obj = {
          token: response.data.sessionId,
          userName: response.data.userName,
          displayName: response.data.displayName,
        };

        this.props.actions.loginSuccess(obj);
      } else {
        this.props.actions.loginFailure();
      }
    } else {
      this.setState({
        showMessage: this.validation.length !== 1,
        showMessageConfirm: confirmPassword !== password,
      });
    }
  }

  handleInput = (inputName, value) => {
    const { password, confirmPassword } = this.state;
    if (inputName === 'confirmPassword')
      this.setState({
        [inputName]: value,
        confirmPasswordClass:
          value === password ? 'bar_isValid' : 'bar_noValid',
        showMessageConfirm: value !== password,
      });
    else
      this.setState({
        [inputName]: value,
        confirmPasswordClass:
          value === confirmPassword ? 'bar_isValid' : 'bar_noValid',
      });
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
    if (this.validation.length === 3) this.disable = false;
    else this.disable = true;
  };

  render() {
    const {
      password,
      confirmPassword,
      showMessage,
      showMessageConfirm,
      confirmPasswordClass,
    } = this.state;
    return (
      <div className={s.loginForm}>
        <div className={s.formWrapper}>
          <h3 className={s.title}>{LOGIN}</h3>
          <div className={s.formContainer}>
            <div className={s.loginFields}>
              <CPValidator
                minLength={5}
                value={password}
                checkValidation={this.checkValidation}
                showMessage={showMessage}
                name={NEW_PASSWORD}
              >
                <CPInput
                  onChange={value =>
                    this.handleInput('password', value.target.value)
                  }
                  className={s.loginInput}
                  label={NEW_PASSWORD}
                  value={password}
                  type="password"
                  autoFocus
                  loginInput
                  formalInput={false}
                />
              </CPValidator>
              <CPInput
                onChange={value =>
                  this.handleInput('confirmPassword', value.target.value)
                }
                className={s.loginInput}
                label={REENTER_NEW_PASSWORD}
                value={confirmPassword}
                isValidClass={confirmPasswordClass}
                type="password"
                loginInput
                formalInput={false}
              />
              {showMessageConfirm && (
                <p className={s.errorMessage}>
                  رمز عبور جدید با تکرار آن برابر نیست
                </p>
              )}
            </div>
            <CPButton className={s.registerBtn} onClick={this.changePassword}>
              {CONFIRMATION}
            </CPButton>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  signUpData: state.signup.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginSuccess: loginActions.loginSuccess,
      loginFailure: loginActions.loginFailure,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ChangePassword));
