import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './LoginForm.css';
import CPButton from '../../CP/CPButton';
import {
  FORGOT_PASSWORD,
  LOGIN,
  PASSWORD,
  PHONE_NUMBER,
} from '../../../Resources/Localization';
import CPInput from '../../CP/CPInput';
import Link from '../../Link';
import CPIntlPhoneInput from '../../CP/CPIntlPhoneInput';
import CPValidator from '../../CP/CPValidator';
import { AuthenticateDto, PermissionDto } from '../../../dtos/identityDtos';
import LoginActions from '../../../redux/identity/actionReducer/account/Login';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';

class LoginForm extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    changeStep: PropTypes.func,
    loginLoading: PropTypes.bool,
    backUrl: PropTypes.string,
  };

  static defaultProps = {
    changeStep: () => {},
    loginLoading: false,
    backUrl: '/',
  };

  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
      showMessage: false,
      userNameFinal: '',
      isValidPassword: false,
      showMessagePhone: 'none',
      messagePhoneNumber: 'لطفا شماره همراه را صحیح وارد کنید.',
    };

    this.login = this.login.bind(this);
    this.validation = [];
  }

  async login() {
    const { backUrl } = this.props;
    const { userNameFinal, password, captchaNC, isValidPassword } = this.state;
    if (this.validation.length === 1 && isValidPassword) {
      const authenticateDtoTemp = new AuthenticateDto({
        UserName: userNameFinal,
        Password: password,
        nc: captchaNC,
        State: 'True',
      });

      const permissions = new BaseGetDtoBuilder()
        .dto(
          new PermissionDto({
            resourceType: 'UI',
            permissionStatus: 'Deny',
          }),
        )
        .buildJson();

      const data = {
        data: authenticateDtoTemp,
        backUrl: backUrl || '/',
        permissionList: permissions,
        fromLogin: true,
      };

      this.props.actions.loginRequest(data);
    } else {
      this.setState({
        showMessage: true,
        showMessagePhone: this.state.messagePhoneNumber ? 'block' : 'none',
      });
    }
  }

  handleInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        userNameFinal: number.replace(/ /g, ''),
        userName: value,
        isValidPassword: status,
        messagePhoneNumber: status ? '' : 'لطفا شماره همراه را صحیح وارد کنید.',
      });
    }
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
    if (this.validation.length === 3) this.disable = false;
    else this.disable = true;
  };

  forgetPassword = e => {
    e.preventDefault();
    this.props.changeStep('forgetPassword');
    // this.props.changeStep('changePassword');
  };

  render() {
    const { loginLoading } = this.props;

    const {
      password,
      userName,
      showMessage,
      showMessagePhone,
      messagePhoneNumber,
    } = this.state;
    return (
      <div className={s.loginForm}>
        <div className={s.formWrapper}>
          <h3 className={s.title}>{LOGIN}</h3>
          <div className={s.formContainer}>
            <div className={s.loginFields}>
              <div className={s.flagBox}>
                <b className={s.label}>{PHONE_NUMBER}</b>
                <CPIntlPhoneInput
                  value={userName}
                  onChange={this.changeNumber}
                />
                <p
                  className={s.errorMessage}
                  style={{ display: `${showMessagePhone}` }}
                >
                  {messagePhoneNumber}
                </p>
              </div>
              <CPValidator
                isRequired
                value={password}
                checkValidation={this.checkValidation}
                showMessage={showMessage}
                name={PASSWORD}
              >
                <CPInput
                  onChange={value =>
                    this.handleInput('password', value.target.value)
                  }
                  className={s.loginInput}
                  label={PASSWORD}
                  value={password}
                  loginInput
                  formalInput={false}
                  type="password"
                  onPressEnter={this.login}
                />
              </CPValidator>

              <CPButton
                className={s.loginBtn}
                disabled={loginLoading}
                onClick={this.login}
              >
                {loginLoading ? <Spin spinning={loginLoading} /> : LOGIN}
              </CPButton>
              {/* <Link
                to="/"
                onClick={e => this.forgetPassword(e)}
                className={s.forgot}
              >
                {FORGOT_PASSWORD}
              </Link> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  loginLoading: state.login.loginRequest,
  backUrl: state.backUrl.data ? state.backUrl.data[0] : '',
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginRequest: LoginActions.loginRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(LoginForm));
