import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../../../redux/identity/action/account';
import s from './AccountWizard.css';
import LoginForm from '../LoginForm';
import ConfirmationForm from '../ConfirmationForm';
import ForgetPassword from '../ForgetPassword';
import ChangePassword from '../ChangePassword';

class AccountWizard extends React.Component {
  static propTypes = {
    validationFalse: PropTypes.func,
    validationTrue: PropTypes.func,
    isValid: PropTypes.func,
    loading: PropTypes.bool,
    selectedStep: PropTypes.string,
  };

  static defaultProps = {
    loading: false,
    fromCheckout: false,
    validationFalse: () => {},
    validationTrue: () => {},
    isValid: () => {},
    selectedStep: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      stepWizardData: props.selectedStep,
    };
  }

  visibilityHandler = key => {
    const stepData = this.state.stepWizardData;
    return key === stepData;
  };

  changeStep = value => {
    this.setState({ stepWizardData: value });
  };

  render() {
    const { loading, validationFalse, validationTrue, isValid } = this.props;
    return (
      <div className="accountWizard">
        {this.visibilityHandler('login') && (
          <div className={loading ? 'showLogin' : 'hiddenLogin'}>
            <LoginForm
              validationFalse={validationFalse}
              validationTrue={validationTrue}
              isValid={isValid}
              changeStep={this.changeStep}
            />
          </div>
        )}
        {this.visibilityHandler('verification') && (
          <div className={loading ? 'hiddenConfirmation' : 'showConfirmation'}>
            <ConfirmationForm
              validationFalse={validationFalse}
              validationTrue={validationTrue}
              isValid={isValid}
              changeStep={this.changeStep}
            />
          </div>
        )}
        {this.visibilityHandler('forgetPassword') && (
          <div className={loading ? 'hiddenConfirmation' : 'showConfirmation'}>
            <ForgetPassword
              validationFalse={validationFalse}
              validationTrue={validationTrue}
              isValid={isValid}
              changeStep={this.changeStep}
            />
          </div>
        )}
        {this.visibilityHandler('changePassword') && (
          <div className={loading ? 'hiddenConfirmation' : 'showConfirmation'}>
            <ChangePassword
              validationFalse={validationFalse}
              validationTrue={validationTrue}
              isValid={isValid}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actionCreators, dispatch),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(AccountWizard),
);
