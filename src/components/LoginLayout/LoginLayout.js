import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import normalizeCss from 'normalize.css';
import s from './LoginLayout.css';

class LoginLayout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  render() {
    return <div className={s.pageWrapper}>{this.props.children}</div>;
  }
}

export default withStyles(normalizeCss, s)(LoginLayout);
