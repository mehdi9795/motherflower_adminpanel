import React from 'react';
import moment from 'moment';
import { message } from 'antd';
import Countdown from 'react-countdown-moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ConfirmationForm.css';
import CPInput from '../../CP/CPInput';
import CPButton from '../../CP/CPButton';
import {
  CONFIRMATION,
  REMINDING_TIME,
  RESEND,
  ENTER_THE_CONFIRMATION_CODE,
  SENDING_CODE,
} from '../../../Resources/Localization';
import {
  PermissionDto,
  SendVerificationCodeDto,
  UserDto,
  UserVerificationDto,
} from '../../../dtos/identityDtos';
import {
  SendVerificationCodeApi,
  UserVerificationApi,
} from '../../../services/identityApi';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';
import { setCookie } from '../../../utils';
import loginActions from '../../../redux/identity/actionReducer/account/Login';

class ConfirmationForm extends React.Component {
  static propTypes = {
    validationFalse: PropTypes.func,
    isValid: PropTypes.func,
    hideNext: PropTypes.func,
    hidePrev: PropTypes.func,
    fromCheckout: PropTypes.bool,
    signUpData: PropTypes.objectOf(PropTypes.any),
    changeStep: PropTypes.func,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    fromCheckout: false,
    signUpData: {},
    validationFalse: () => {},
    changeStep: () => {},
    isValid: () => {},
    hideNext: () => {},
    hidePrev: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      code1: '',
      code2: '',
      code3: '',
      code4: '',
      btnResend: true,
      btnConfirm: false,
    };
    if (this.props.hideNext) {
      this.props.hideNext();
      this.props.hidePrev();
      this.props.validationFalse();
    }
    this.verify = this.verify.bind(this);
    this.onResend = this.onResend.bind(this);
    this.sendVerivicationCodeHandler();
    this.endDate = moment().add(30, 'seconds');
  }

  async onResend() {
    const { phone, countryCode } = this.props.signUpData;
    this.endDate = moment().add(30, 'seconds');
    this.setState({ btnConfirm: false, btnResend: true });
    this.sendVerivicationCodeHandler();

    const baseCRUDDtoTemp = new BaseCRUDDtoBuilder()
      .dto(
        new SendVerificationCodeDto({
          userDto: new UserDto({ phone, countryCode }),
          verificationType: 'Signup',
          resend: true,
        }),
      )
      .build();

    await SendVerificationCodeApi(baseCRUDDtoTemp);
  }

  async verify() {
    const { code1, code2, code3, code4 } = this.state;
    const { signUpData, backUrl } = this.props;

    const baseCRUDDtoTemp = new BaseCRUDDtoBuilder()
      .dto(
        new UserVerificationDto({
          id: signUpData.id,
          userName: signUpData.phone,
          verificationType: 'SignIn',
          // verificationType: 'ForgetPassword',
          verificationCode: `${code1}${code2}${code3}${code4}`.toString(),
        }),
      )
      .build();

    const permissions = new BaseGetDtoBuilder()
      .dto(
        new PermissionDto({
          resourceType: 'UI',
          permissionStatus: 'Deny',
        }),
      )
      .buildJson();

    const data = {
      data: baseCRUDDtoTemp,
      backUrl: backUrl || '/',
      permissionList: permissions,
    };

    this.props.actions.loginRequest(data);
  }

  handleInput = (inputName, value, keyFocus) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({ [inputName]: value });
      if (keyFocus !== 'submit') {
        const input = document.getElementsByName(keyFocus);
        input[0].focus();
      }
       else {
        // document.getElementById(keyFocus).focus();
        setTimeout(() => this.verify(), 100);
      }
    }
  };

  sendVerivicationCodeHandler = () => {
    const thiz = this;
    setTimeout(() => {
      thiz.setState({ btnConfirm: true, btnResend: false });
    }, 30000);
  };

  render() {
    const { btnResend } = this.state;
    return (
      <div className={s.loginForm}>
        <div className={s.formWrapper}>
          <h3 className={s.title}>{SENDING_CODE}</h3>
          <div className={s.formContainer}>
            <div className={s.confirmationFields}>
              <b className={s.label}>{ENTER_THE_CONFIRMATION_CODE}</b>
              <div className={s.confirmationInputs}>
                <CPInput
                  onChange={value =>
                    this.handleInput('code4', value.target.value, 'submit')
                  }
                  className={s.confirmationInput}
                  value={this.state.code4}
                  maxLength={1}
                  name="code4"
                  loginInput
                  formalInput={false}
                />
                <CPInput
                  onChange={value =>
                    this.handleInput('code3', value.target.value, 'code4')
                  }
                  className={s.confirmationInput}
                  value={this.state.code3}
                  maxLength={1}
                  name="code3"
                  loginInput
                  formalInput={false}
                />
                <CPInput
                  onChange={value =>
                    this.handleInput('code2', value.target.value, 'code3')
                  }
                  className={s.confirmationInput}
                  value={this.state.code2}
                  maxLength={1}
                  name="code2"
                  loginInput
                  formalInput={false}
                />
                <CPInput
                  onChange={value =>
                    this.handleInput('code1', value.target.value, 'code2')
                  }
                  className={s.confirmationInput}
                  value={this.state.code1}
                  maxLength={1}
                  name="code1"
                  autoFocus
                  loginInput
                  formalInput={false}
                />
              </div>
              <CPButton
                className={s.confirmationBtn}
                onClick={this.verify}
                id="submit"
              >
                {CONFIRMATION}
              </CPButton>
            </div>
            <div className={s.reminder}>
              <b className={s.label}>
                {REMINDING_TIME} :{' '}
                <b>
                  <Countdown endDate={this.endDate} />
                </b>
              </b>
              <CPButton
                disabled={btnResend}
                onClick={this.onResend}
                className={s.resendBtn}
              >
                {RESEND}
              </CPButton>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  signUpData: state.signup.data,
  backUrl: state.backUrl.data ? state.backUrl.data[0] : '',
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginRequest: loginActions.loginRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ConfirmationForm));
