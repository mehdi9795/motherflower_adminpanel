import React from 'react';
import ReactExport from 'react-data-export';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

const multiDataSet = [
  {
    columns: ['Headings', 'Text Style', 'Colors'],
    data: [
      [
        { value: 'H1' },
        { value: 'Bold' },
        {
          value: 'Red',
        },
      ],
      [{ value: 'H2' }, { value: 'underline' }, { value: 'Blue' }],
      [{ value: 'H3' }, { value: 'italic' }, { value: 'Green' }],
      [{ value: 'H4' }, { value: 'strike' }, { value: 'Orange' }],
      [{ value: 'H3' }, { value: 'italic' }, { value: 'Green' }],
      [{ value: 'H4' }, { value: 'strike' }, { value: 'Orange' }],
    ],
  },
];

class CPExcel extends React.Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  render() {
    return (
      <div>
        <ExcelFile element={<button>Download Data With Styles</button>}>
          <ExcelSheet dataSet={multiDataSet} name="Organization" />
        </ExcelFile>
      </div>
    );
  }
}

export default CPExcel;
