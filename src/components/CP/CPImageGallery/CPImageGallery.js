import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPImageGallery.css';
import CPImage from '../CPImage';
import CPPermission from "../CPPermission/CPPermission";

class CPImageGallery extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
    onCreate: PropTypes.func,
  };

  static defaultProps = {
    onEdit: () => {},
    onDelete: () => {},
    onCreate: () => {},
    data: null,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  onDelete = value => {
    this.props.onDelete(value);
  };

  onEdit = value => {
    this.props.onEdit(value);
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOkModal = () => {
    this.setState({
      visible: false,
    });
  };

  handleCancelModal = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const { data, onCreate } = this.props;
    return (
      <div className={s.imageGallery}>
        {data.map(item => (
          <CPImage
            imgUrl={`${item.url}`}
            imgTitle={item.tooltip}
            editIcon
            onEdit={this.onEdit}
            deleteIcon
            displayOrder={item.displayOrder}
            onDelete={this.onDelete}
            showModal
            entityId={item.entityId}
            id={item.id}
            isCover={item.isCover}
            key={item.id}
            isShowOnHomePage={item.showOnHomePage}
            status={item.status ? item.status : false}
            link={item.link ? item.link : ''}
            imageId={item.imageId ? item.imageId : 0}
            deleteTitle={item.deleteTitle}
          />
        ))}
        <CPPermission>
          <div name="product-management-image" className={s.cardUpload}>
            <div className={s.upload} onClick={onCreate}>
              <div className={s.content}>
                <i className="cp-upload" />
                <h3>آپلود تصویر</h3>
              </div>
            </div>
          </div>
        </CPPermission>
      </div>
    );
  }
}

export default withStyles(s)(CPImageGallery);
