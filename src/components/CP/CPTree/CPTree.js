import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Tree } from 'antd';
import { showLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPTree.css';
import Link from '../../Link/Link';

const TreeNode = Tree.TreeNode;

class CPTree extends React.Component {
  static propTypes = {
    model: PropTypes.objectOf(PropTypes.any).isRequired,
    checkedKeys: PropTypes.arrayOf(PropTypes.number),
    onSelect: PropTypes.func,
    checkable: PropTypes.bool,
    defaultExpandAll: PropTypes.bool,
    parentDisabled: PropTypes.bool,
    onCheck: PropTypes.func,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    onSelect: () => {},
    onCheck: () => {},
    checkedKeys: [],
    checkable: false,
    defaultExpandAll: false,
    parentDisabled: false,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  getTitle = node => {
    const { model } = this.props;
    let treeNodeTitle = '';
    if (model.allowLink) {
      treeNodeTitle = (
        <Link to={node.link} onClick={this.goToPage}>
          {node.code && `${node.code}-`}
          {node.name}
        </Link>
      );
    } else if (model.allowLastLeafSelect) {
      treeNodeTitle = node.name;
    }
    return treeNodeTitle;
  };

  goToPage = () => {
    this.props.actions.showLoading();
  };

  loop = data =>
    data.map(item => {
      if (item.children && item.children.length) {
        return (
          <TreeNode disableCheckbox={this.props.parentDisabled} key={item.name} title={this.getTitle(item)}>
            {this.loop(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode key={this.props.checkable ? `${item.id}*${item.description}` : item.name} isLeaf title={this.getTitle(item)} />;
    });

  render() {
    const { model, onSelect, checkable, onCheck, checkedKeys, defaultExpandAll } = this.props;
    return (
      <div className={classnames('form-group')}>
        {!checkable && (
          <Tree showLine onSelect={onSelect} defaultExpandAll={defaultExpandAll}>
            {this.loop(model.items)}
          </Tree>
        )}
        {checkable && (
          <Tree checkable onCheck={onCheck} checkedKeys={checkedKeys} defaultExpandAll={defaultExpandAll}>
            {this.loop(model.items)}
          </Tree>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  categories: state.categoryList.data.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CPTree));
