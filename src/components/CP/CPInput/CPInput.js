import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPInput.css';
import { Input } from 'antd';

class CPInput extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    hintText: PropTypes.string,
    fullWidth: PropTypes.bool,
    className: PropTypes.string,
    parentClassName: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onPressEnter: PropTypes.func,
    name: PropTypes.string,
    isValidClass: PropTypes.string,
    value: PropTypes.string,
    type: PropTypes.string,
    maxLength: PropTypes.string,
    autoFocus: PropTypes.bool,
    loginInput: PropTypes.bool,
    formalInput: PropTypes.bool,
    id: PropTypes.string,
  };

  static defaultProps = {
    hintText: null,
    fullWidth: true,
    disabled: false,
    onChange: () => {},
    onBlur: () => {},
    onPressEnter: () => {},
    label: '',
    className: '',
    parentClassName: '',
    name: '',
    isValidClass: 'bar',
    value: '',
    maxLength: '99999999999999999',
    autoFocus: false,
    loginInput: false,
    formalInput: true,
    type: 'text',
    id: '',
  };

  render() {
    const {
      label,
      hintText,
      fullWidth,
      disabled,
      name,
      onChange,
      className,
      isValidClass,
      onBlur,
      value,
      maxLength,
      autoFocus,
      type,
      loginInput,
      formalInput,
      onPressEnter,
      parentClassName,
      id,
    } = this.props;
    return (
      <div
        className={cx('form-group', parentClassName, className, {
          fullWidth,
        })}
      >
        {formalInput && (
          <label className="label" htmlFor="input">
            {label}
          </label>
        )}
        <Input
          className={className}
          disabled={disabled}
          name={name}
          type={type}
          // required="required"
          onChange={onChange}
          placeholder={hintText}
          onBlur={onBlur}
          value={value}
          maxLength={maxLength}
          autoFocus={autoFocus}
          onPressEnter={onPressEnter}
          id={id}
          autocomplete="off"
        />
        {loginInput && (
          <label className="control-label" htmlFor="input">
            {label}
          </label>
        )}
        <i className={isValidClass} />
      </div>
    );
  }
}

export default withStyles(s)(CPInput);
