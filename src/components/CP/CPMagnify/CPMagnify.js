import React from 'react';
import ReactSlick from 'react-slick';
import ReactImageMagnify from 'react-image-magnify';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPMagnify.css';
import Link from '../../Link';

// Data Sample for slider
// const image1 = [
//   { src: '/images/slider/image01.jpg', vw: '500w' },
//   { src: '/images/slider/watch01.jpg', vw: '1426w' },
// ];
//
// image1.map(item => `${item.src} ${item.vw}`).join(', ');
//
// const image2 = [
//   { src: '/images/slider/image02.jpg', vw: '500w' },
//   { src: '/images/slider/watch02.jpg', vw: '1426w' },
// ];
//
// image2.map(item => `${item.src} ${item.vw}`).join(', ');
//
// const data = [
//   {
//     srcSet: image1,
//     small: '/images/slider/image01.jpg',
//     large: '/images/slider/watch01.jpg',
//   },
//   {
//     srcSet: image2,
//     small: '/images/slider/image02.jpg',
//     large: '/images/slider/watch02.jpg',
//   },
// ];
/* <CPMagnify data={data} width={1426} height={2000} /> */

class CPMagnify extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    width: PropTypes.number,
    height: PropTypes.number,
    isFluidWidth: PropTypes.bool,
    shouldHideHintAfterFirstActivation: PropTypes.bool,
    isHintEnabled: PropTypes.bool,
    enlargedImagePosition: PropTypes.string,
    sizes: PropTypes.string,
  };

  static defaultProps = {
    width: null,
    height: null,
    data: [],
    isFluidWidth: true,
    shouldHideHintAfterFirstActivation: false,
    isHintEnabled: true,
    enlargedImagePosition: 'over',
    sizes: '(max-width: 480px) 100vw, (max-width: 1200px) 30vw, 360px',
  };

  render() {
    const slickSettings = {
      customPaging(i) {
        return (
          <Link to="/#">
            <img
              className="img-thumb"
              src={`/images/slider/image0${i + 1}.jpg`}
              alt=""
            />
          </Link>
        );
      },
      dots: true,
      dotsClass: 'slick-dots slick-thumb',
      infinite: true,
      draggable: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      rtl: true,
    };

    const { data } = this.props;

    return (
      <ReactSlick {...slickSettings}>
        {data.map((src, i) => (
          <div key={i}>
            <ReactImageMagnify
              {...{
                largeImage: {
                  src: src.large,
                  width: this.props.width,
                  height: this.props.height,
                },
                smallImage: {
                  isFluidWidth: this.props.isFluidWidth,
                  src: src.small,
                  srcSet: src.srcSet,
                  sizes: this.props.sizes,
                },
                isHintEnabled: this.props.isHintEnabled,
                shouldHideHintAfterFirstActivation: this.props
                  .shouldHideHintAfterFirstActivation,
                enlargedImagePosition: this.props.enlargedImagePosition,
              }}
            />
          </div>
        ))}
      </ReactSlick>
    );
  }
}

export default withStyles(s)(CPMagnify);
