import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { Card, Button } from 'antd';
import s from './EditCard.css';
import CPLoading from '../CPLoading/CPLoading';
import CPButton from '../CPButton/CPButton';
import Link from '../../Link/Link';
import { NO, YES } from '../../../Resources/Localization';
import CPPopConfirm from '../CPPopConfirm';
import CPAlert from '../CPAlert';
import AlertHideActions from '../../../redux/shared/actionReducer/alert/alert';
import CPPopover from '../CPPopover';
import CPPermission from "../CPPermission/CPPermission";

const { Group } = Button;

class EditCard extends React.Component {
  static propTypes = {
    product: PropTypes.objectOf(PropTypes.any),
    children: PropTypes.node.isRequired,
    title: PropTypes.string.isRequired,
    isEdit: PropTypes.bool,
    loading: PropTypes.bool,
    errorAlert: PropTypes.bool,
    showPopHover: PropTypes.bool,
    backUrl: PropTypes.string,
    onDelete: PropTypes.func,
    deleteTitle: PropTypes.string,
    errorMessage: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    renderActions: PropTypes.node,
    flatermission: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    title: 'عنوان کارت',
    isEdit: false,
    loading: false,
    errorAlert: false,
    showPopHover: false,
    backUrl: '',
    deleteTitle: '',
    errorMessage: '',
    onDelete: () => {},
    product: {},
    renderActions: null,
    flatermission: {},
  };

  constructor(props) {
    super(props);
    this.props.actions.AlertHideRequest();
  }

  onDelete = () => {
    this.props.onDelete();
  };

  onCloseErrorMessage = () => {
    this.props.actions.AlertHideRequest();
  };

  render() {
    const {
      title,
      backUrl,
      isEdit,
      deleteTitle,
      loading,
      errorAlert,
      errorMessage,
      showPopHover,
      product,
      renderActions,
    } = this.props;
    /* let renderActions = null;

    if (showPopHover) {
      const href = `/product/create?vendorId=${product && product.vendorDto ? product.vendorDto.id : 0}`;

      renderActions = (
        <div>
          <div>
            <a href={href}>ایجاد محصول</a>
          </div>
        </div>
      );
    } */

    return (
      <CPLoading spinning={loading}>
        <div className={s.editCard}>
          <Card title={title}>
            <div className={s.topbtns}>
              <Group>
                {showPopHover && (
                  <CPPopover content={renderActions}>
                    <CPButton shape="circle">
                      <i className="cp-dot" />
                    </CPButton>
                  </CPPopover>
                )}
                <CPButton shape="circle">
                  <Link id="backUrlEditCard" to={backUrl}>
                    <i className="cp-arrow-left" />
                  </Link>
                </CPButton>
              </Group>
            </div>
            {errorAlert && (
              <CPAlert
                showIcon
                closable
                onClose={this.onCloseErrorMessage}
                type="error"
                message="خطا"
                description={errorMessage}
              />
            )}
            {isEdit && (
              <CPPermission>
              <div className={s.bottombtns} name="product-management-info-delete">
                <Group>
                  <CPPopConfirm
                    title={deleteTitle}
                    okText={YES}
                    cancelText={NO}
                    okType="primary"
                    onConfirm={() => this.onDelete()}
                  >
                    <CPButton shape="circle">
                      <i className="cp-trash" />
                    </CPButton>
                  </CPPopConfirm>
                </Group>
              </div>
              </CPPermission>
            )}
            {this.props.children}
          </Card>
        </div>
      </CPLoading>
    );
  }
}

const mapStateToProps = state => ({
  product:
    state.singleProduct.data && state.singleProduct.data.items
      ? state.singleProduct.data.items[0]
      : null,
  loading: state.editFormLoading.loading,
  errorAlert: state.editFormAlert.loading,
  errorMessage: state.editFormAlert.data,
  flatermission:
    state.permission.data && state.permission.data.items.length > 0
      ? state.permission.data.items[0].flatermission
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      AlertHideRequest: AlertHideActions.editFormAlertHideRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(EditCard));
