import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Card } from 'antd';
import PropTypes from 'prop-types';
import cs from 'classnames';
import s from './SearchBox.css';

class SearchBox extends React.Component {
  static propTypes = {
    children: PropTypes.node,
  };

  static defaultProps = {
    children: '',
  };

  render() {
    return (
      <div className={cs(s.advancedSearch,'advancedSearch')}>
        <Card>{this.props.children}</Card>
      </div>
    );
  }
}

export default withStyles(s)(SearchBox);
