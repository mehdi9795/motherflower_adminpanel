import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Tooltip } from 'antd';
import s from './CPTooltip.css';

class CPTooltip extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    placement: PropTypes.string,
  };

  static defaultProps = {
    title: '',
    placement: 'bottom',
  };

  render() {
    const { title, placement } = this.props;

    return (
      <Tooltip title={title} placement={placement}>
        {this.props.children}
      </Tooltip>
    );
  }
}

export default withStyles(s)(CPTooltip);
