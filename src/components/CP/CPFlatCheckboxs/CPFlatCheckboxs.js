import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Checkbox } from 'antd';
import s from './CPFlatCheckboxs.css';
import CPCheckbox from '../CPCheckbox';

class CPFlatCheckboxs extends React.Component {
  static propTypes = {
    dataSource: PropTypes.arrayOf(PropTypes.object),
    values: PropTypes.arrayOf(PropTypes.number),
    onChange: PropTypes.func,
  };

  static defaultProps = {
    dataSource: [],
    values: [],
    onChange: () => {},
  };

  render() {
    const { onChange, dataSource, values } = this.props;
    return (
      <div>
        {dataSource.map(item => (
          <div>
            <CPCheckbox checked={item.value}>{item.title}</CPCheckbox>
          </div>
        ))}
      </div>
    );
  }
}

export default withStyles(s)(CPFlatCheckboxs);
