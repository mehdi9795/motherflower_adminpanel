import React from 'react';
import { connect } from 'react-redux';
import { Select } from 'antd';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPLeftDrawer.css';
import CPSelect from '../CPSelect';
import {
  AREA,
  BOTTOM,
  CENTER,
  SAVE,
  STATUS,
  SUB_SYSTEM,
  TOP,
} from '../../../Resources/Localization';
import CPCheckbox from '../CPCheckbox';
import CPFlatCheckboxs from '../CPFlatCheckboxs';
import CPMultiSelect from '../CPMultiSelect';
import CPButton from '../CPButton';
import { ReportDto } from '../../../dtos/commonDtos';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
  BaseListCRUDDtoBuilder,
} from '../../../dtos/dtoBuilder';
import reportSettingPostActions from '../../../redux/identity/actionReducer/reportSetting/Post';
import { UserReportSetting } from '../../../dtos/identityDtos';
import { getDtoQueryString } from '../../../utils/helper';

const { Option } = Select;

class CPLeftDrawer extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    id: PropTypes.string,
    reports: PropTypes.arrayOf(PropTypes.object),
    reportSetting: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    className: '',
    id: '',
    reports: [],
    reportSetting: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      subSystem: 'Catalog',
      area: 'Top*Box',
      reportDataSource: props.reports.filter(value =>
        this.reportFiltering(value, 'Catalog', 'Box'),
      ),
      reportIds: [],
    };
  }

  componentDidMount() {
    this.setReportIds();
  }

  setReportIds = () => {
    const reportSelected = [];
    this.props.reportSetting
      .filter(this.reportSettingFiltering)
      .map(item => reportSelected.push(item.reportDto.id.toString()));
    this.setState({
      reportIds: reportSelected,
    });
  };

  reportSettingFiltering = item => {
    const { reportDataSource, area } = this.state;
    let result;
    for (let i = 0; i < reportDataSource.length; i += 1) {
      if (
        item.reportDto.id === reportDataSource[i].id &&
        item.region === area.split('*')[0]
      ) {
        result = item;
        break;
      }
    }
    return result;
  };

  reportFiltering = (item, subSystem, category) => {
    if (category === 'Box')
      return item.category === category && item.subSystem === subSystem;
    return item.category !== 'Box' && item.subSystem === subSystem;
  };

  handelChangeInput = (inputName, value) => {
    this.setState(
      {
        [inputName]: value,
        reportDataSource: this.props.reports.filter(item =>
          this.reportFiltering(
            item,
            inputName === 'subSystem' ? value : this.state.subSystem,
            inputName === 'area'
              ? value.split('*')[1]
              : this.state.area.split('*')[1],
          ),
        ),
      },
      () => {
        if (inputName !== 'reportIds') this.setReportIds();
      },
    );
  };

  saveReportSetting = () => {
    const { reportIds, subSystem, area } = this.state;
    const array = [];
    reportIds.map(item => {
      array.push(
        new UserReportSetting({
          region: area.split('*')[0],
          reportDto: new ReportDto({ id: item, subSystem }),
        }),
      );
      return null;
    });

    const crudDto = new BaseListCRUDDtoBuilder().dtos(array).build();
    const data = {
      data: crudDto,
      listDto: getDtoQueryString(
        new BaseGetDtoBuilder().dto(new ReportDto()).buildJson(),
      ),
    };
    this.props.actions.reportSettingPostRequest(data);
  };

  render() {
    const { className, id } = this.props;
    const { subSystem, area, reportDataSource, reportIds } = this.state;

    const reportArray = [];
    reportDataSource.map(item =>
      reportArray.push(
        <Option className="reportItem" key={item.id} value={item.id.toString()}>
          {item.title}
        </Option>,
      ),
    );

    return (
      <div className={cs(s.drawer, className)} id={id}>
        <div>
          <label>{SUB_SYSTEM}</label>
          <CPSelect
            value={subSystem}
            onChange={value => {
              this.handelChangeInput('subSystem', value);
            }}
          >
            <Option key="Catalog">Catalog</Option>
            <Option key="Identity">Identity</Option>
            <Option key="CMS">Cms</Option>
            <Option key="Common">Common</Option>
            <Option key="SAM">Sam</Option>
            <Option key="Vendor">Vendor</Option>
          </CPSelect>
        </div>
        <div>
          <label>{AREA}</label>
          <CPSelect
            value={area}
            onChange={value => {
              this.handelChangeInput('area', value);
            }}
          >
            <Option key="Top*Box">{TOP}</Option>
            <Option key="Center*Chart">{CENTER}</Option>
            <Option key="Bottom*Box">{BOTTOM}</Option>
          </CPSelect>
        </div>
        <div>
          <label>{STATUS}</label>
          <CPMultiSelect
            value={reportIds}
            onChange={value => {
              this.handelChangeInput('reportIds', value);
            }}
            style={{
              maxWidth: '171px',
              fontSize: '11px !important',
              width: '100%',
            }}
          >
            {reportArray}
          </CPMultiSelect>
        </div>
        <div>
          <CPButton className={s.saveButton} onClick={this.saveReportSetting}>
            {SAVE}
          </CPButton>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  reports: state.reportList.data ? state.reportList.data.items : [],
  reportSetting: state.reportSettingList.data
    ? state.reportSettingList.data.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      reportSettingPostRequest:
        reportSettingPostActions.reportSettingPostRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CPLeftDrawer));
