import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPCounter.css';

class CPCounter extends React.Component {
  static propTypes = {
    value: PropTypes.number,
  };

  static defaultProps = {
    value: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      newValue: 0,
    };

    if (props.value) this.counter(props.value);
  }

  counter = value => {
    let count = 0;
    const counter1 = setInterval(() => {
      count += 1;
      if (value < count) clearInterval(counter1);
      else
      this.setState({ newValue: count });

    }, 500);
  };

  render() {
    const { newValue } = this.state;

    return <span>{newValue}</span>;
  }
}
export default withStyles(s)(CPCounter);
