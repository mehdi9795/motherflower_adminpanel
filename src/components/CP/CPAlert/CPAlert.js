import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Alert } from 'antd';
import s from './CPAlert.css';

class CPAlert extends React.Component {
  static propTypes = {
    closable: PropTypes.bool,
    type: PropTypes.string,
    showIcon: PropTypes.bool,
    message: PropTypes.node,
    description: PropTypes.node,
    onClose: PropTypes.func,
    key: PropTypes.number,
  };

  static defaultProps = {
    closable: false,
    type: '',
    message: '',
    description: '',
    showIcon: false,
    onClose: () => {},
    key: 0,
  };

  render() {
    const {
      closable,
      type,
      showIcon,
      message,
      description,
      onClose,
      key,
    } = this.props;
    return (
      <Alert
        closable={closable}
        onClose={onClose}
        type={type}
        showIcon={showIcon}
        message={message}
        description={description}
        key={key}
      />
    );
  }
}

export default withStyles(s)(CPAlert);
