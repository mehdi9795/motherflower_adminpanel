import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Avatar } from 'antd';
import s from './CPAvatar.css';

class CPAvatar extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    icon: PropTypes.string,
    shape: PropTypes.string,
    size: PropTypes.string,
    src: PropTypes.string,
    style: PropTypes.object,
  };

  static defaultProps = {
    children: '',
    icon: '',
    shape: 'circle',
    size: 'default',
    src: '',
    style: null,
  };

  render() {
    const { icon, shape, size, src, style } = this.props;
    return (
      <Avatar icon={icon} shape={shape} size={size} src={src} style={style} oncl>
        {this.props.children}
      </Avatar>
    );
  }
}

export default withStyles(s)(CPAvatar);
