import React from 'react';
import AvatarEditor from 'react-avatar-editor';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPImageEditor.css';
import CPSlider from '../CPSlider';
import CPUpload from '../CPUpload';
import { DELETE, SAVE } from '../../../Resources/Localization';
import CPButton from '../CPButton';

class CPImageEditor extends React.Component {
  static propTypes = {
    onChangeImage: PropTypes.func,
    onRef: PropTypes.func,
    onSave: PropTypes.func,
    onDelete: PropTypes.func,
    image: PropTypes.string,
  };

  static defaultProps = {
    onChangeImage: () => {},
    onRef: () => {},
    onSave: () => {},
    onDelete: () => {},
    image: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      rotate: 0,
      fileImage: '',
      zoom: 1,
      position: { x: 0.5, y: 0.5 },
      isWeb: true,
    };
  }

  componentDidMount() {
    this.props.onRef(this);
    this.updatePredicate();
  }

  handleImageChange = value => {
    this.setState({ fileImage: value.file });
  };

  changeSliders = (inputName, value) => {
    if (inputName === 'position-x')
      this.setState({ position: { x: value, y: this.state.position.y } });
    else if (inputName === 'position-y')
      this.setState({ position: { x: this.state.position.x, y: value } });
    else this.setState({ [inputName]: value });
  };

  changePosition = value => {
    this.setState({ position: value });
  };

  onClickSave = () => {
    console.log('onSave');
    if (this.editor) {
      const canvas = this.editor.getImage().toDataURL();
      this.props.onChangeImage(canvas);
    }
  };

  setEditorRef = editor => (this.editor = editor);

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 769 });
  };

  render() {
    const { zoom, position, rotate, isWeb, fileImage } = this.state;
    const { image, onDelete, onSave } = this.props;
    return (
      <div>
        {(fileImage || image) && (
          <div className={s.avatarEditor}>
            <AvatarEditor
              ref={this.setEditorRef}
              image={fileImage || image}
              width={isWeb ? 250 : 150}
              height={isWeb ? 250 : 150}
              border={100}
              color={[255, 255, 255, 0.6]} // RGBA
              scale={zoom}
              rotate={rotate}
              position={position}
              onPositionChange={this.changePosition}
              borderRadius={130}
              className="col-sm-12"
            />
            <div className="row">
              <div className={cs(s.zoom, 'col-sm-12 col-lg-6')}>
                <span>زوم</span>
                <CPSlider
                  value={zoom}
                  min={1}
                  max={10}
                  step={0.2}
                  onChange={value => this.changeSliders('zoom', value)}
                />
              </div>
              <div className={cs(s.rotate, 'col-sm-12 col-lg-6')}>
                <span>چرخش</span>
                <CPSlider
                  value={rotate}
                  min={0}
                  max={360}
                  step={1}
                  onChange={value => this.changeSliders('rotate', value)}
                />
              </div>
            </div>
          </div>
        )}
        <div className={s.buttons}>
          <CPUpload
            showFileName={false}
            showImage={false}
            onChange={this.handleImageChange}
          />
          {(fileImage || image) && <CPButton onClick={onSave}>{SAVE}</CPButton>}
          {(fileImage || image) && (
            <CPButton onClick={onDelete}>{DELETE}</CPButton>
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(s)(CPImageEditor);
