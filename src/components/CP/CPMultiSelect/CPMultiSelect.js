import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPMultiSelect.css';

class CPMultiSelect extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    value: PropTypes.arrayOf(PropTypes.string),
    disabled: PropTypes.bool,
    style: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    children: '',
    onChange: () => {},
    placeholder: '',
    value: [],
    disabled: false,
    style: { width: '100%', maxWidth: 'unset' },
  };

  render() {
    const {
      onChange,
      children,
      placeholder,
      value,
      disabled,
      style,
    } = this.props;
    return (
      <Select
        mode="tags"
        style={style}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        disabled={disabled}
      >
        {children}
      </Select>
    );
  }
}

export default withStyles(s)(CPMultiSelect);
