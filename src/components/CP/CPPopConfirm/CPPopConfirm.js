import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Popconfirm } from 'antd';
import s from './CPPopConfirm.css';

class CPPopConfirm extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    cancelText: PropTypes.string,
    okText: PropTypes.string,
    okType: PropTypes.string,
    title: PropTypes.string,
    onCancel: PropTypes.func,
    onConfirm: PropTypes.func,
  };

  static defaultProps = {
    children: '',
    cancelText: '',
    okText: '',
    okType: '',
    title: '',
    onCancel: () => {},
    onConfirm: () => {},
  };

  render() {
    const {
      cancelText,
      okText,
      okType,
      title,
      onCancel,
      onConfirm,
    } = this.props;

    return (
      <Popconfirm
        cancelText={cancelText}
        okText={okText}
        okType={okType}
        title={title}
        onCancel={onCancel}
        onConfirm={onConfirm}
      >
        {this.props.children}
      </Popconfirm>
    );
  }
}

export default withStyles(s)(CPPopConfirm);
