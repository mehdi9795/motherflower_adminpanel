/* eslint-disable class-methods-use-this,react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { TreeSelect } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPTreeSelect.css';

const SHOW_PARENT = TreeSelect.SHOW_PARENT;

class CPTreeSelect extends React.Component {
  static propTypes = {
    model: PropTypes.arrayOf(PropTypes.object),
    checkBox: PropTypes.bool,
    withParentId: PropTypes.bool,
    withCategoryCode: PropTypes.bool,
    onChange: PropTypes.func,
    value: PropTypes.string,
    values: PropTypes.array,
    placeholder: PropTypes.string,
    defaultValue: PropTypes.string,
    className: PropTypes.string,
    parentDisabled: PropTypes.bool,
  };

  static defaultProps = {
    model: [],
    checkBox: false,
    onChange: () => {},
    value: '',
    placeholder: '',
    defaultValue: '',
    values: [],
    withParentId: false,
    withCategoryCode: false,
    parentDisabled: false,
    className: '',
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  traverseTree(node) {
    const { withParentId, parentDisabled, withCategoryCode, widthCategoryDescription } = this.props;
    const child = [];

    if (node.children && node.children.length > 0) {
      node.children.map(item => {
        let finalValue = 0;
        if (withParentId && withCategoryCode) {
          finalValue = `${item.id}*${item.parentCategoryId}*${item.deepCode}*${item.description}`;
        }
        else if (withParentId) {
          finalValue = `${item.id}*${item.parentCategoryId}`;
        } else if (withCategoryCode) {
          finalValue = `${item.id}*${item.deepCode}*${item.description}`;
        } else {
          finalValue = `${item.id}`;
        }
        const childNodeModel = {
          value: finalValue,
          label: item.name,
          disabled: false,
          children: [],
          key: item.id,
        };
        if (item.children && item.children.length > 0) {
          childNodeModel.disabled = parentDisabled;
          childNodeModel.children = this.traverseTree(item);
        }

        return child.push(childNodeModel);
      });
    }
    return child;
  }

  render() {
    const {
      model,
      checkBox,
      onChange,
      value,
      values,
      withParentId,
      parentDisabled,
      withCategoryCode,
      className,
    } = this.props;
    const treeData = [];
    model.map(item => {
      let finalValue = 0;
      if (withParentId && withCategoryCode) {
        finalValue = `${item.id}*${item.parentCategoryId}*${item.deepCode}`;
      }
      else if (withParentId) {
        finalValue = `${item.id}*${item.parentCategoryId}`;
      } else if (withCategoryCode) {
        finalValue = `${item.id}*${item.deepCode}`;
      } else {
        finalValue = `${item.id}`;
      }
      const treeNodeModel = {
        value: finalValue,
        label: item.name,
        disabled: parentDisabled,
        children: [],
        key: item.id,
      };
      treeNodeModel.children = this.traverseTree(item);

      return treeData.push(treeNodeModel);
    });

    // noinspection JSAnnotator
    const tProps = {
      treeData,
      value: checkBox ? values : value,
      defaultValue: '1',
      onChange,
      treeCheckable: checkBox,
      showCheckedStrategy: SHOW_PARENT,
      searchPlaceholder: '',
      // treeCheckStrictly: true,
      treeDefaultExpandAll: parentDisabled,
      style: {
        width: '100%',
      },
      className,
    };

    return (
      <div>
        <TreeSelect {...tProps} />
      </div>
    );
  }
}
export default withStyles(s)(CPTreeSelect);
