import React from 'react';
import PropTypes from 'prop-types';
import { Slider } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPSlider.css';

class CPSlider extends React.Component {
  static propTypes = {
    autoFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    defaultValue: PropTypes.number,
    max: PropTypes.number,
    min: PropTypes.number,
    range: PropTypes.bool,
    step: PropTypes.number,
    value: PropTypes.number,
    vertical: PropTypes.bool,
    onAfterChange: PropTypes.func,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    autoFocus: false,
    disabled: false,
    defaultValue: 0,
    max: 1000,
    min: 0,
    range: false,
    step: 1,
    value: 0,
    vertical: false,
    onAfterChange: () => {},
    onChange: () => {},
  };

  render() {
    const {
      autoFocus,
      disabled,
      defaultValue,
      max,
      min,
      range,
      step,
      value,
      vertical,
      onAfterChange,
      onChange,
    } = this.props;
    return (
      <Slider
        autoFocus={autoFocus}
        defaultValue={defaultValue}
        disabled={disabled}
        max={max}
        min={min}
        range={range}
        step={step}
        value={value}
        vertical={vertical}
        onAfterChange={onAfterChange}
        onChange={onChange}
      />
    );
  }
}

export default withStyles(s)(CPSlider);
