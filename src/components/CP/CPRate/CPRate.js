import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Rate } from 'antd';
import PropTypes from 'prop-types';
import s from './CPRate.css';

class CPRate extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    value: PropTypes.number,
    defaultValue: PropTypes.number,
    onChange: PropTypes.func,
    allowHalf: PropTypes.bool,
    disabled: PropTypes.bool,
    className: PropTypes.string,
  };

  static defaultProps = {
    children: '',
    className: '',
    value: 0,
    defaultValue: 0,
    allowHalf: false,
    disabled: false,
    onChange: () => {},
  };

  render() {
    const {
      value,
      onChange,
      allowHalf,
      defaultValue,
      className,
      disabled,
    } = this.props;
    return (
      <Rate
        style={{ direction: 'rtl' }}
        className={className}
        onChange={onChange}
        value={value}
        allowHalf={allowHalf}
        defaultValue={defaultValue}
        disabled={disabled}
      >
        {this.props.children}
      </Rate>
    );
  }
}

export default withStyles(s)(CPRate);
