import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Pagination } from 'antd';
import s from './CPPagination.css';

class CPPanel extends React.Component {
  static propTypes = {
    showSizeChanger: PropTypes.bool,
    showQuickJumper: PropTypes.bool,
    size: PropTypes.string,
    total: PropTypes.number,
    current: PropTypes.number,
    pageSizeOptions: PropTypes.arrayOf(PropTypes.string),
    defaultPageSize: PropTypes.number,
    onChange: PropTypes.func,
    onShowSizeChange: PropTypes.func,
  };

  static defaultProps = {
    showSizeChanger: true,
    showQuickJumper: true,
    pageSizeOptions: ['10', '20', '30', '40'],
    size: 'small',
    total: 0,
    current: 1,
    defaultPageSize: 20,
    onChange: () => {},
    onShowSizeChange: () => {},
  };

  render() {
    const {
      showQuickJumper,
      showSizeChanger,
      size,
      total,
      onChange,
      current,
      onShowSizeChange,
      defaultPageSize,
      pageSizeOptions,
    } = this.props;
    return (
      <Pagination
        size={size}
        total={total}
        showSizeChanger={showSizeChanger}
        showQuickJumper={showQuickJumper}
        current={current}
        onChange={onChange}
        onShowSizeChange={onShowSizeChange}
        defaultPageSize={defaultPageSize}
        pageSizeOptions={pageSizeOptions}
      />
    );
  }
}

export default withStyles(s)(CPPanel);
