import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Popover } from 'antd';
import s from './CPPopover.css';

class CPPopover extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    placement: PropTypes.string,
    title: PropTypes.string,
    content: PropTypes.node,
    trigger: PropTypes.string,
  };

  static defaultProps = {
    children: '',
    content: '',
    placement: 'bottomLeft',
    title: '',
    trigger: 'click',
  };

  render() {
    const { placement, title, content, trigger } = this.props;

    return (
      <Popover
        content={content}
        trigger={trigger}
        placement={placement}
        title={title}
      >
        {this.props.children}
      </Popover>
    );
  }
}

export default withStyles(s)(CPPopover);
