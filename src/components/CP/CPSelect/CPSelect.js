import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPSelect.css';

class CPSelect extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    onChange: PropTypes.func,
    showSearch: PropTypes.bool,
    value: PropTypes.node,
    defaultValue: PropTypes.string,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    autoFocus: PropTypes.bool,
  };

  static defaultProps = {
    children: '',
    className: '',
    onChange: () => {},
    showSearch: false,
    value: null,
    defaultValue: null,
    defaultActiveFirstOption: false,
    disabled: false,
    autoFocus: false,
  };

  render() {
    const {
      onChange,
      children,
      showSearch,
      value,
      defaultValue,
      disabled,
      className,
      autoFocus,
    } = this.props;
    return value !== null ? (
      <Select
        showSearch={showSearch}
        style={{ width: '100%' }}
        optionFilterProp="children"
        onChange={onChange}
        disabled={disabled}
        value={value}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        className={className}
        autoFocus={autoFocus}
      >
        {children}
      </Select>
    ) : (
      <Select
        showSearch={showSearch}
        style={{ width: '100%' }}
        optionFilterProp="children"
        onChange={onChange}
        disabled={disabled}
        defaultValue={defaultValue}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        className={className}
        autoFocus={autoFocus}
      >
        {children}
      </Select>
    );
  }
}

export default withStyles(s)(CPSelect);
