import React from 'react';
import ReactSlick from 'react-slick';
import ReactImageMagnify from 'react-image-magnify';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPCarousel.css';
import Link from '../../Link';

class CPCarousel extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    width: PropTypes.number,
    height: PropTypes.number,
    isFluidWidth: PropTypes.bool,
    shouldHideHintAfterFirstActivation: PropTypes.bool,
    isHintEnabled: PropTypes.bool,
    enlargedImagePosition: PropTypes.string,
    sizes: PropTypes.string,
  };

  static defaultProps = {
    width: null,
    height: null,
    data: [],
    isFluidWidth: true,
    shouldHideHintAfterFirstActivation: false,
    isHintEnabled: false,
    enlargedImagePosition: 'over',
    sizes: '(max-width: 480px) 100vw, (max-width: 1200px) 30vw, 360px',
  };

  constructor(props) {
    super(props);
    this.state = {
      showSlider: false,
    };
  }

  componentDidMount = () => {
    setTimeout(() => this.setState({ showSlider: true }), 0);
  };

  render() {
    const { data } = this.props;
    const slickSettings = {
      customPaging(i) {
        return (
          <Link to="/#">
            <img className="img-thumb" src={`${data[i].small}`} alt="" />
          </Link>
        );
      },
      dots: true,
      dotsClass: 'slick-dots slick-thumb',
      infinite: true,
      draggable: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          dotsClass: 'slick-dots',
          settings: {
            customPaging(i) {
              return <button>{i + 1}</button>;
            },
          },
        },
        {
          breakpoint: 480,
          dotsClass: 'slick-dots',
          settings: {
            customPaging(i) {
              return <button>{i + 1}</button>;
            },
          },
        },
      ],
    };
    if (!this.state.showSlider) {
      return <div style={{ width: '100%', height: '100%' }} />;
    }
    return (
      <ReactSlick {...slickSettings}>
        {data.map(src => (
          <div key={src.key}>
            <ReactImageMagnify
              {...{
                largeImage: {
                  src: src.large,
                  width: this.props.width,
                  height: this.props.height,
                },
                smallImage: {
                  isFluidWidth: this.props.isFluidWidth,
                  src: src.small,
                  srcSet: src.srcSet,
                  sizes: this.props.sizes,
                },
                isHintEnabled: this.props.isHintEnabled,
                shouldHideHintAfterFirstActivation: this.props
                  .shouldHideHintAfterFirstActivation,
                enlargedImagePosition: this.props.enlargedImagePosition,
              }}
            />
          </div>
        ))}
      </ReactSlick>
    );
  }
}

export default withStyles(s)(CPCarousel);
