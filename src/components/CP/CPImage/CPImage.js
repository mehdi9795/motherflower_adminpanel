import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPImage.css';
import CPCard from '../CPCard';
import CPModal from '../CPModal';
import {
  OVERVIEW,
  DEFAULT,
  NO,
  YES,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT,
  HOME_PAGE,
} from '../../../Resources/Localization';
import CPButton from '../CPButton';
import CPPopConfirm from '../CPPopConfirm';
import CPPermission from '../CPPermission/CPPermission';

class CPImage extends React.Component {
  static propTypes = {
    imgUrl: PropTypes.string,
    imgTitle: PropTypes.string,
    link: PropTypes.string,
    displayOrder: PropTypes.number,
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
    showModal: PropTypes.bool,
    isCover: PropTypes.bool,
    isShowOnHomePage: PropTypes.bool,
    status: PropTypes.bool,
    id: PropTypes.number,
    entityId: PropTypes.number,
    imageId: PropTypes.number,
    deleteTitle: PropTypes.string,
  };

  static defaultProps = {
    imgUrl: '',
    imgTitle: '',
    link: '',
    displayOrder: 0,
    onEdit: () => {},
    onDelete: () => {},
    showModal: true,
    isCover: false,
    isShowOnHomePage: false,
    status: false,
    entityId: 0,
    imageId: 0,
    id: 0,
    deleteTitle: ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }
  onEdit = () => {
    const {
      displayOrder,
      id,
      imgTitle,
      entityId,
      isCover,
      imgUrl,
      isShowOnHomePage,
      link,
      status,
      imageId,
    } = this.props;
    if (typeof this.props.onEdit === 'function') {
      const data = {
        displayOrder,
        id,
        tooltip: imgTitle,
        entityId,
        isCover,
        imgUrl,
        isShowOnHomePage,
        link,
        status,
        imageId,
      };
      this.props.onEdit(data);
    }
  };

  onDelete = () => {
    const { id } = this.props;
    if (typeof this.props.onEdit === 'function') {
      this.props.onDelete(id);
    }
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOkModal = () => {
    this.setState({
      visible: false,
    });
  };

  handleCancelModal = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const {
      imgUrl,
      imgTitle,
      displayOrder,
      showModal,
      isCover,
      isShowOnHomePage,
      deleteTitle,
    } = this.props;

    return (
      <CPCard className={s.cpImage} showModal={showModal}>
        <CPButton className={s.maximize} onClick={this.showModal}>
          <i className="cp-maximize2" />
        </CPButton>
        <img src={imgUrl} title={imgTitle} alt="" />
        {isCover && (
          <div className={s.default}>
            <b>{DEFAULT}</b>
          </div>
        )}
        {isShowOnHomePage && (
          <div className={s.default}>
            <b>{HOME_PAGE}</b>
          </div>
        )}
        <div className={s.cardContent}>
          <b className={s.displayOrder}>ترتیب نمایش : {displayOrder}</b>
          <CPPermission>
            <div name="product-management-image">
              <CPButton className={s.edit} onClick={this.onEdit}>
                <i className="cp-edit" />
              </CPButton>
              {/* <CPButton className={s.delete} onClick={this.onDelete}> */}
              {/* <i className="cp-trash" /> */}
              {/* </CPButton> */}
              <CPPopConfirm
                title={deleteTitle}
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() => this.onDelete()}
              >
                <CPButton className={s.delete}>
                  <i className="cp-trash" />
                </CPButton>
              </CPPopConfirm>
            </div>
          </CPPermission>
        </div>

        <CPModal
          title={OVERVIEW}
          showFooter={false}
          visible={this.state.visible}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
        >
          <img className={s.modalImg} src={imgUrl} alt="" title={imgTitle} />
        </CPModal>
      </CPCard>
    );
  }
}

export default withStyles(s)(CPImage);
