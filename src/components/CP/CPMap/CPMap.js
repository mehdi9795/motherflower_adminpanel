import React from 'react';
import PropTypes from 'prop-types';
import { compose, lifecycle } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPMap.css';

const _ = require('lodash');
const {
  SearchBox,
} = require('react-google-maps/lib/components/places/SearchBox');

class CPMap extends React.Component {
  static propTypes = {
    draggable: PropTypes.bool,
    searchBox: PropTypes.bool,
    lat: PropTypes.number,
    lng: PropTypes.number,
    onDragEnd: PropTypes.func,
    defaultZoom: PropTypes.number,
    defaultCenter: PropTypes.objectOf(PropTypes.number),
    googleMapURL: PropTypes.string,
    loadingElement: PropTypes.string,
    containerElement: PropTypes.string,
    markerTitle: PropTypes.string,
    mapElement: PropTypes.string,
    markers: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    draggable: false,
    searchBox: false,
    lat: 0,
    lng: 0,
    onDragEnd: () => {},
    defaultZoom: 8,
    defaultCenter: { lat: 0, lng: 0 },
    googleMapURL: '',
    loadingElement: '',
    containerElement: '',
    mapElement: '',
    markerTitle: '',
    markers: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      latitude: props.lat,
      longitude: props.lng,
      markerTitle: props.markerTitle,
    };

    this.renderMap();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.lat !== this.props.lat) {
      this.renderMap(nextProps.lat, nextProps.lng, nextProps.markerTitle);
      // this.setState({latitude: nextProps.lat, longitude: nextProps.lng});
    }

    if (nextProps.markers.length > 0)
      this.renderMap(null, null, '', nextProps.markers);
  }

  onDrag = e => {
    this.props.onDragEnd({ lat: e.latLng.lat(), lng: e.latLng.lng() });
    this.setState({ latitude: e.latLng.lat(), longitude: e.latLng.lng() });
  };

  onClick = e => {
    if (this.props.markers.length === 0) {
      // this.setState({ latitude: e.latLng.lat(), longitude: e.latLng.lng() });
      this.onDrag(e);
    }
  };

  renderMap = (
    newLat = null,
    newLng = null,
    newMarkerTitle = '',
    newMarkers = [],
  ) => {
    const {
      draggable,
      lat,
      lng,
      defaultZoom,
      defaultCenter,
      markers,
      searchBox,
    } = this.props;
    const { latitude, longitude, markerTitle } = this.state;

    const markerArray = newMarkers.length > 0 ? newMarkers : markers;

    this.MapWithAMarker = compose(
      lifecycle({
        componentWillMount() {
          const refs = {};
          this.setState({
            onMarkerPositionChanged: e => {
              this.props.onDragEnd(`${e.latLng.lat()}-${e.latLng.lng()}`);
            },
            bounds: null,
            center: {
              lat: newLat || latitude,
              lng: newLng || longitude,
            },
            markers: [],
            onMapMounted: ref => {
              refs.map = ref;
            },
            onBoundsChanged: () => {
              this.setState({
                bounds: refs.map.getBounds(),
                center: refs.map.getCenter(),
              });
            },
            onSearchBoxMounted: ref => {
              refs.searchBox = ref;
            },
            onPlacesChanged: () => {
              const places = refs.searchBox.getPlaces();
              const bounds = new google.maps.LatLngBounds();

              places.forEach(place => {
                if (place.geometry.viewport) {
                  bounds.union(place.geometry.viewport);
                } else {
                  bounds.extend(place.geometry.location);
                }
              });
              const nextMarkers = places.map(place => ({
                position: place.geometry.location,
              }));
              const nextCenter = _.get(
                nextMarkers,
                '0.position',
                this.state.center,
              );

              this.setState({
                center: nextCenter,
                markers: nextMarkers,
              });
              // refs.map.fitBounds(bounds);
            },
          });
        },
      }),
      withScriptjs,
      withGoogleMap,
    )(props => (
      <GoogleMap
        ref={props.onMapMounted}
        onBoundsChanged={props.onBoundsChanged}
        // defaultCenter={defaultCenter}
        defaultZoom={defaultZoom}
        center={defaultCenter.lat === 0 ? props.center : defaultCenter}
        onClick={this.onClick}
      >
        {searchBox && (
          <SearchBox
            ref={props.onSearchBoxMounted}
            bounds={props.bounds}
            controlPosition={google.maps.ControlPosition.TOP_LEFT}
            onPlacesChanged={props.onPlacesChanged}
          >
            <input
              type="text"
              placeholder="نام محله مورد نظر را وارد کنید"
              style={{
                boxSizing: `border-box`,
                border: `1px solid transparent`,
                width: `240px`,
                height: `32px`,
                marginTop: `10px`,
                padding: `0 12px`,
                borderRadius: `3px`,
                boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                fontSize: `14px`,
                outline: `none`,
                textOverflow: `ellipses`,
              }}
            />
          </SearchBox>
        )}
        {lat && lng ? (
          <Marker
            draggable={draggable}
            position={{ lat: newLat || latitude, lng: newLng || longitude }}
            onDragEnd={this.onDrag}
            title={newMarkerTitle || markerTitle}
          />
        ) : (
          markerArray.map(item => (
            <Marker
              draggable={draggable}
              position={{ lat: item.lat, lng: item.lng }}
              onDragEnd={this.onDrag}
              title={item.name}
            />
          ))
        )}
      </GoogleMap>
    ));

    if (newLat && newLng)
      this.setState({ latitude: newLat, longitude: newLng });
  };

  render() {
    const {
      googleMapURL,
      loadingElement,
      containerElement,
      mapElement,
    } = this.props;

    return (
      <div>
        <this.MapWithAMarker
          googleMapURL={googleMapURL}
          loadingElement={<div style={{ height: loadingElement }} />}
          containerElement={<div style={{ height: containerElement }} />}
          mapElement={<div style={{ height: mapElement }} />}
        />
      </div>
    );
  }
}

export default withStyles(s)(CPMap);
