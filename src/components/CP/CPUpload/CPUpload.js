import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import FileBase64 from 'react-file-base64';
import s from './CPUpload.css';
import CPButton from '../CPButton';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_IMAGE,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_TAG,
  IMAGE_UPLOAD,
  NO,
  YES,
} from '../../../Resources/Localization';
import CPPopConfirm from '../CPPopConfirm';

class CPUpload extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    onDelete: PropTypes.func,
    buttonRef: PropTypes.func,
    image: PropTypes.string,
    deleted: PropTypes.bool,
    firstLoad: PropTypes.bool,
    disabled: PropTypes.bool,
    name: PropTypes.string,
  };

  static defaultProps = {
    onChange: () => {},
    onDelete: () => {},
    buttonRef: () => {},
    image: null,
    deleted: false,
    callSelectedImage: false,
    disabled: false,
    firstLoad: false,
    name: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      files: [
        {
          name: '1-40_edited.jpg',
          type: 'image/jpeg',
          size: '79 kB',
          base64: props.image,
          file: {},
        },
      ],
      changeFile: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ changeFile: !nextProps.firstLoad });
  }

  getFiles = files => {
    this.setState({
      files,
      changeFile: true,
    });
    this.props.onChange(files[0]);
  };

  selectFile = () => {
    const files = document.querySelectorAll('input[type=file]');
    files[0].click();
  };

  render() {
    const { files, changeFile } = this.state;
    const { image, deleted, onDelete, name, buttonRef, disabled } = this.props;
    return (
      <div className="clearfix">
        {changeFile &&
          files.length > 0 &&
          files[0].base64 && (
            <div className={s.uploadImg}>
              <img
                alt="preview"
                style={{ width: '125px' }}
                src={files[0].base64}
              />
            </div>
          )}
        {!changeFile &&
          image && (
            <div className={s.uploadImg}>
              {deleted && (
                <CPPopConfirm
                  title={ARE_YOU_SURE_WANT_TO_DELETE_THE_IMAGE}
                  okText={YES}
                  cancelText={NO}
                  okType="primary"
                  onConfirm={onDelete}
                >
                  <CPButton className={[s.deleteButton, 'delete_action']}>
                    <i className="cp-trash" />
                  </CPButton>
                </CPPopConfirm>
              )}
              <img src={image} style={{ width: '125px' }} alt="" />
            </div>
          )}
        <CPButton
          name={name}
          className={s.btn}
          ref={buttonRef}
          onClick={this.selectFile}
          disabled={disabled}
        >
          {IMAGE_UPLOAD}
        </CPButton>
        <div className={s.fileInput}>
          <FileBase64
            ref={input => {
              this.textInput = input;
            }}
            multiple
            onDone={this.getFiles}
          />
        </div>
        {files.length > 0 &&
          files[0].base64 && <div className={s.fileName}>{files[0].name}</div>}
      </div>
    );
  }
}

export default withStyles(s)(CPUpload);
