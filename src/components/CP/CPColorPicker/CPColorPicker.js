import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPColorPicker.css';
import CPPopover from '../CPPopover';

class CPColorPicker extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func,
    multiSelect: PropTypes.bool,
    defaultValues: PropTypes.arrayOf(PropTypes.number),
    defaultValue: PropTypes.number,
  };

  static defaultProps = {
    data: [],
    onChange: () => {},
    multiSelect: false,
    defaultValues: [],
    defaultValue: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      valueCheckBox: [],
    };
  }
  onChangeCheckBox = value => {
    const array = this.state.valueCheckBox;
    const index = array.indexOf(value); // Let's say it's Bob.

    if (index === -1) this.state.valueCheckBox.push(value);
    else {
      array.splice(index, 1);
      this.setState({ valueCheckBox: array });
    }
    this.props.onChange(this.state.valueCheckBox);
  };

  onchangeRadio = value => {
    console.log('sss', value)
    this.setState({ value });
    this.props.onChange(value);
  };

  render() {
    const { data, multiSelect, defaultValue, defaultValues } = this.props;

    return !multiSelect ? (
      <div className={s.wrapp}>
        {data.map(item => (
          <CPPopover content={item.name} key={item.name} placement="top" trigger="hover">
            <input
              type="checkbox"
              onChange={value => this.onchangeRadio(value.target.value)}
              checked={
                this.state.value === item.id.toString() ||
                item.id === defaultValue
              }
              value={item.id}
              // defaultValue={defaultValue}
              id={item.attribute}
              hidden
              key={item.id}
            />
            <label
              className={s.colorSwitch}
              htmlFor={item.attribute}
              style={{ background: item.attribute }}
            />
          </CPPopover>
        ))}
      </div>
    ) : (
      <div className={s.wrapp}>
        {data.map(item => (
          <CPPopover content={item.name} placement="top" trigger="hover">
            <input
              type="checkbox"
              onChange={value => this.onChangeCheckBox(value.target.value)}
              value={item.id}
              id={item.attribute}
              // checked={defaultValues.find(item.id.toString())}
              hidden
              key={item.id}
            />
            <label
              className={s.colorSwitch}
              htmlFor={item.attribute}
              style={{ background: item.attribute }}
            />
          </CPPopover>
        ))}
      </div>
    );
  }
}

export default withStyles(s)(CPColorPicker);
