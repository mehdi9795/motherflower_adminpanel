import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-jalaali';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPDatepickerContent.css';

class CPDatepickerContent extends React.Component {
  static propTypes = {
    date: PropTypes.node.isRequired,
    onSelectDay: PropTypes.func,
    monthExceptions: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    onSelectDay: () => {},
    monthExceptions: {},
  };

  constructor(props) {
    super(props);
    moment.loadPersian({ dialect: 'persian-modern' });
    const format = 'jYYYY/jM/jD';
    const dateTime = moment(props.date, format);
    const firstDayName = moment(
      `${dateTime.jYear()}/${dateTime.jMonth() + 1}/1`,
      'jYYYY/jM/jD',
    ).format('dddd');
    const firstYear = props.from.jYear();
    const lastYear = props.to.jYear();
    this.state = {
      // date: props.date,
      years: dateTime.jYear(),
      month: dateTime.jMonth(),
      day: dateTime.jDate(),
      // monthName: dateTime.format('jMMMM'),
      // firstDayName,
      isLeapYear: moment(
        `${dateTime.jYear()}/12/30`,
        'jYYYY/jMM/jDD',
      ).isValid(), // سال کبیسه
      is31Days: moment(
        `${dateTime.jYear()}/${dateTime.jMonth() + 1}/31`,
        'jYYYY/jMM/jDD',
      ).isValid(),
      firstDayNumber: this.firstDayNumber(firstDayName),
      firstYear,
      lastYear,
    };
  }

  componentWillReceiveProps(nextProps) {
    const dateTime = moment(nextProps.date, 'jYYYY/jM/jD');
    const firstDayName = moment(
      `${dateTime.jYear()}/${dateTime.jMonth() + 1}/1`,
      'jYYYY/jM/jD',
    ).format('dddd');
    this.setState({
      // date: nextProps.date,
      years: dateTime.jYear(),
      month: dateTime.jMonth(),
      day: dateTime.jDate(),
      // monthName: dateTime.format('jMMMM'),
      // firstDayName,
      isLeapYear: moment(
        `${dateTime.jYear()}/12/30`,
        'jYYYY/jMM/jDD',
      ).isValid(), // سال کبیسه
      is31Days: moment(
        `${dateTime.jYear()}/${dateTime.jMonth() + 1}/31`,
        'jYYYY/jMM/jDD',
      ).isValid(),
      firstDayNumber: this.firstDayNumber(firstDayName),
    });
  }

  firstDayNumber(dayName) {
    switch (dayName) {
      case 'شنبه':
        return 1;
      case 'یک‌شنبه':
        return 2;
      case 'دوشنبه':
        return 3;
      case 'سه‌شنبه':
        return 4;
      case 'چهارشنبه':
        return 5;
      case 'پنج‌شنبه':
        return 6;
      case 'جمعه':
        return 7;
    }
  }

  selectDay(i) {
    this.props.onSelectDay(i);
  }

  createRow = index => {
    const { years, month, day } = this.state;
    const rows = [];
    const addOnDay = this.state.is31Days ? 0 : 1;
    let isLeapYear;
    if (this.state.month === 11) {
      if (this.state.isLeapYear) isLeapYear = 0;
      else isLeapYear = 1;
    } else isLeapYear = 0;
    for (let i = index; i < 7 + index; i++) {
      if (i < this.state.firstDayNumber - 1) {
        rows.push(
          <span className={s.emptyCell} key={i + years + month + day} />,
        );
      } else if (i > 29 + this.state.firstDayNumber - addOnDay - isLeapYear) {
        rows.push(
          <span className={s.emptyCell} key={i + years + month + day} />,
        );
      } else {
        rows.push(this.renderDayCell(i - this.state.firstDayNumber + 2));
      }
    }
    return rows;
  };

  renderDayCell(day) {
    const { years, month, firstYear, lastYear } = this.state;
    const { monthExceptions, selectedDay, from, to } = this.props;

    // const dayName = moment(
    //   `${years}/${month + 1}/${day}`,
    //   'jYYYY/jM/jD',
    // ).format('dddd');

    const first = from.format('jDD');
    const last = to.format('jDD');
    const firstMonth = from.format('jM');
    const lastMonth = to.format('jM');

    if (
      (firstYear === years &&
        firstMonth === (month + 1).toString() &&
        day < first) ||
      (lastYear === years && lastMonth === (month + 1).toString() && day > last)
    ) {
      return (
        <span className={s.dayCell} key={month + day}>
          <span className={s.disabled}>{day}</span>
        </span>
      );
    }
    if (
      monthExceptions &&
      monthExceptions.disabledDays &&
      monthExceptions.disabledDays[`${years}/${month + 1}/${day}`]
    ) {
      return (
        <span className={s.dayCell} key={month + day}>
          <span className={s.disabled}>
            {monthExceptions &&
            monthExceptions.calendarEvents &&
            monthExceptions.calendarEvents[`${years}/${month + 1}/${day}`] ? (
              <div>
                <img
                  className={s.imageEvent}
                  src={
                    monthExceptions.calendarEvents[
                      `${years}/${month + 1}/${day}`
                    ].imageUrl
                  }
                  alt=""
                />
                {day}
              </div>
            ) : (
              day
            )}
          </span>
        </span>
      );
    }
    return (
      <span
        className={s.dayCell}
        key={month + day}
        onClick={() => this.selectDay(day)}
      >
        <span
          className={selectedDay.toString() === day.toString() ? s.active : ''}
        >
          {monthExceptions &&
          monthExceptions.calendarEvents &&
          monthExceptions.calendarEvents[`${years}/${month + 1}/${day}`] ? (
            <div>
              <img
                className={s.imageEvent}
                src={
                  monthExceptions.calendarEvents[`${years}/${month + 1}/${day}`]
                    .imageUrl
                }
                alt=""
              />
              {day}
            </div>
          ) : (
            day
          )}
        </span>
      </span>
    );
  }

  renderDaysContent() {
    const { years, month, day } = this.state;
    const content = [];
    for (let i = 0; i < 30 + this.state.firstDayNumber; i += 7) {
      content.push(
        <div key={`${years}/${month}/${day}/${i}`} className={s.daysHeader}>
          {this.createRow(i)}
        </div>,
      );
    }
    return content;
  }

  render() {
    return <div>{this.renderDaysContent()}</div>;
  }
}

export default withStyles(s)(CPDatepickerContent);
