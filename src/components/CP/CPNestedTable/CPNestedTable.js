import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Card, LocaleProvider, Table } from 'antd';
import faIR from 'antd/lib/locale-provider/fa_IR';
import s from './CPNestedTable.css';
import SearchBox from '../SearchBox';

class CPNestedTable extends React.Component {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.object),
    dataSource: PropTypes.arrayOf(PropTypes.object),
    expandedRowRender: PropTypes.func,
    rowClassName: PropTypes.func,
    hideSearchBox: PropTypes.bool,
    bordered: PropTypes.bool,
    loading: PropTypes.bool,
    children: PropTypes.node,
    pagination: PropTypes.bool,
  };

  static defaultProps = {
    columns: [],
    dataSource: [],
    expandedRowRender: () => {},
    rowClassName: () => {},
    hideSearchBox: false,
    bordered: false,
    loading: false,
    pagination: true,
    children: '',
  };

  render() {
    const {
      dataSource,
      columns,
      expandedRowRender,
      hideSearchBox,
      loading,
      rowClassName,
      bordered,
      pagination,
    } = this.props;
    return (
      <LocaleProvider locale={faIR}>
        <Card className={s.cardTable}>
          {!hideSearchBox && <SearchBox>{this.props.children}</SearchBox>}
          <Table
            bordered={bordered}
            columns={columns}
            expandedRowRender={expandedRowRender}
            dataSource={dataSource}
            loading={loading}
            rowClassName={rowClassName}
            pagination={pagination}
            scroll={{ x: 10 }}
          />
        </Card>
      </LocaleProvider>
    );
  }
}

export default withStyles(s)(CPNestedTable);
