import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Modal } from 'antd';
import CPButton from '../CPButton';
import s from './CPConfirmation.css';
import flyImage from '../../../../public/images/fly.jpg';
import confirmImage from '../../../../public/images/confirm.jpg';

class CPConfirmation extends React.Component {
  static propTypes = {
    visible: PropTypes.bool,
    closable: PropTypes.bool,
    handleOk: PropTypes.func,
    handleCancel: PropTypes.func,
    footer: PropTypes.node,
    content: PropTypes.string,
    zIndex: PropTypes.number,
  };

  static defaultProps = {
    visible: false,
    closable: false,
    handleOk: () => {},
    handleCancel: () => {},
    footer: null,
    content: '',
    zIndex: 0,
  };

  handleOk = e => {
    this.props.handleOk(e);
  };

  handleCancel = e => {
    this.props.handleCancel(e);
  };

  render() {
    const {
      closable,
      footer,
      handleOk,
      handleCancel,
      visible,
      content,
      zIndex,
    } = this.props;
    return (
      <Modal
        closable={closable}
        visible={visible}
        footer={footer}
        zIndex={zIndex}
      >
        <div className={s.modalContent}>
          <div className={s.content}>
            <h3>
              <img className={s.logo} src={flyImage} />
              {content}
            </h3>

            <CPButton className={s.yes} onClick={handleOk}>
              بله
            </CPButton>
            <CPButton className={s.no} onClick={handleCancel}>
              خیر
            </CPButton>
          </div>
          <img className={s.background} src={confirmImage} />
        </div>
      </Modal>
    );
  }
}

export default withStyles(s)(CPConfirmation);
