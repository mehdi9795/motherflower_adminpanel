import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import RaisedButton from 'material-ui/RaisedButton';
import s from './CPButton.css';

class CPButton extends React.Component {
  static propTypes = {
    labelPosition: PropTypes.string,
    label: PropTypes.string,
    checked: PropTypes.bool,
    errorText: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    name: PropTypes.string,
    icon: PropTypes.node,
  };

  static defaultProps = {
    label: null,
    labelPosition: null,
    checked: false,
    errorText: null,
    disabled: false,
    onClick: () => {},
    name: null,
    icon: null,
  };

  render() {
    const { label, disabled, onClick, icon } = this.props;
    return (
      <Button
        label={label}
        primary
        className={s.button}
        onClick={onClick}
        icon={icon}
        labelPosition="before"
        disabled={disabled}
      />
    );
  }
}

export default withStyles(s)(CPButton);
