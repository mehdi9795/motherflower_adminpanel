import React from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Button } from 'antd';
import s from './CPButton.css';

class CPButton extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    icon: PropTypes.node,
    type: PropTypes.string,
    size: PropTypes.string,
    shape: PropTypes.string,
    className: PropTypes.string,
    htmlType: PropTypes.node,
    loading: PropTypes.bool,
    style: PropTypes.objectOf(PropTypes.string),
  };

  static defaultProps = {
    children: '',
    disabled: false,
    onClick: () => {},
    icon: '',
    type: 'default', //
    size: 'default',
    className: null,
    shape: null,
    htmlType: 'button',
    loading: false,
    style: null,
  };

  render() {
    const {
      disabled,
      onClick,
      icon,
      type,
      size,
      shape,
      className,
      htmlType,
      loading,
      style,
    } = this.props;
    return (
      <Button
        style={style}
        type={type}
        className={cs(s.button, className)}
        onClick={onClick}
        icon={icon}
        disabled={disabled}
        size={size}
        shape={shape}
        htmlType={htmlType}
        loading={loading}
      >
        {this.props.children}
      </Button>
    );
  }
}

export default withStyles(s)(CPButton);
