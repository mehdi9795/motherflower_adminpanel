import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Modal } from 'antd';
import CPButton from '../CPButton';
import s from './CPModal.css';

class CPModal extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    visible: PropTypes.bool,
    handleOk: PropTypes.func,
    handleCancel: PropTypes.func,
    handleButton: PropTypes.func,
    isButton: PropTypes.bool,
    className: PropTypes.string,
    children: PropTypes.node,
    iconSave: PropTypes.string,
    iconClose: PropTypes.string,
    textSave: PropTypes.string,
    textClose: PropTypes.string,
    typeClose: PropTypes.string,
    typeSave: PropTypes.string,
    showFooter: PropTypes.bool,
    hideOkButton: PropTypes.bool,
    hideCancelButton: PropTypes.bool,
    width: PropTypes.number,
    customButton: PropTypes.node,
  };

  static defaultProps = {
    title: '',
    visible: false,
    handleOk: () => {},
    handleCancel: () => {},
    handleButton: () => {},
    isButton: false,
    className: 'min_modal',
    children: null,
    iconSave: 'save',
    iconClose: 'close-circle-o',
    textSave: 'ذخیره',
    textClose: 'انصراف',
    typeClose: 'primary',
    typeSave: 'primary',
    showFooter: true,
    hideOkButton: false,
    hideCancelButton: false,
    width: 520,
    customButton: '',
  };

  constructor(props) {
    super(props);
    this.state = { visible: this.props.visible || false };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.visible,
    });
  }

  handleOk = e => {
    this.props.handleOk(e);
  };

  handleCancel = e => {
    this.props.handleCancel(e);
  };

  handleButton = e => {
    this.props.handleButton(e);
  };

  render() {
    const {
      title,
      className,
      iconSave,
      iconClose,
      textSave,
      textClose,
      typeClose,
      typeSave,
      showFooter,
      isButton,
      width,
      hideOkButton,
      hideCancelButton,
      customButton,
    } = this.props;

    return (
      <div>
        <Modal
          className={className}
          width={width}
          title={title}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            showFooter && (
              <div key={1} className={s.footerButton}>
                {customButton}
                {!hideOkButton && (
                  <CPButton
                    key="submit"
                    type={typeSave}
                    icon={iconSave}
                    onClick={this.handleOk}
                  >
                    {textSave}
                  </CPButton>
                )}
                {!isButton &&
                  !hideCancelButton && (
                    <CPButton
                      key="back"
                      type={typeClose}
                      icon={iconClose}
                      onClick={this.handleCancel}
                    >
                      {textClose}
                    </CPButton>
                  )}
                {isButton && (
                  <CPButton key="back2" onClick={this.handleButton}>
                    {textClose}
                  </CPButton>
                )}
              </div>
            ),
          ]}
        >
          {this.props.children}
        </Modal>
      </div>
    );
  }
}

export default withStyles(s)(CPModal);
