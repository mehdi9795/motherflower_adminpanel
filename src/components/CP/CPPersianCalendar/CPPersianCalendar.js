import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { DatePicker } from 'react-advance-jalaali-datepicker';
// import DatePicker from 'react-datepicker-fa';
// import { DatePicker } from 'react-persian-datepicker';
import moment from 'moment-jalaali';
import s from './CPPersianCalendar.css';

class CPPersianCalendar extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    format: PropTypes.node,
    id: PropTypes.string,
    value: PropTypes.objectOf(PropTypes.any),
    preSelected: PropTypes.string,
    className: PropTypes.string,
    timePicker: PropTypes.bool,
  };

  static defaultProps = {
    onChange: () => {},
    placeholder: '',
    value: null,
    format: 'jYYYY/jMM/jDD',
    id: 'datePicker',
    preSelected: null,
    className: 'datePicker',
    timePicker: false,
  };

  /* constructor() {
    super();
    moment.loadPersian({ dialect: 'persian-modern' });
    this.state = {};
  } */

  render() {
    const {
      onChange,
      placeholder,
      id,
      className,
      format,
      preSelected,
      timePicker,
      value,
    } = this.props;

    return (
      <div className={s.dateInput}>
        <DatePicker
          onChange={onChange}
          placeholder={placeholder}
          format={format}
          id={id}
          preSelected={preSelected}
          className={className}
        />
        {/* <DatePicker */}
        {/* //placeholder={placeholder} */}
        {/* //isGregorian={false} */}
        {/* value={value} */}
        {/* onChange={onChange} */}
        {/* className={className} */}
        {/* timePicker={timePicker} */}
        {/* id={id} */}

        {/* /> */}
      </div>
    );
  }
}

export default withStyles(s)(CPPersianCalendar);
