/* eslint-disable react/forbid-prop-types,react/prop-types */
import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Table } from 'antd';
import s from './CPTable.css';
import PropTypes from "prop-types";

class CPTable extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    columns: PropTypes.arrayOf(PropTypes.object).isRequired,
    loading: PropTypes.bool,
    onChange: PropTypes.func,
    rowClassName: PropTypes.func,
    pagination: PropTypes.bool,
    scroll: PropTypes.objectOf(PropTypes.any),
    className: PropTypes.string,
  };
  static defaultProps = {
    onChange: () => {},
    rowClassName: () => {},
    loading: false,
    pagination: true,
    scroll: { x: 0 },
    className: '',
  };
  constructor(props) {
    super(props);
    this.state = {
      isWeb: true,
    };
  }

  componentDidMount() {
    this.updatePredicate();
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 1100 });
  };

  render() {
    const { isWeb } = this.state;
    const {
      data,
      columns,
      loading = false,
      rowSelection,
      pagination,
      onChange,
      rowClassName,
      scroll,
      className,
    } = this.props;

    return (
      <Table
        columns={columns}
        dataSource={data}
        pagination={pagination}
        loading={loading}
        rowSelection={rowSelection}
        onChange={onChange}
        rowClassName={rowClassName}
        scroll={scroll}
        className={className}
      />
    );
  }
}

export default withStyles(s)(CPTable);
