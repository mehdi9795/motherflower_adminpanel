import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Dropdown, Icon, Button } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPDropdown.css';
import Link from '../../Link/Link';

const { Item } = Menu;

class CPDropdown extends React.Component {
  static propTypes = {
    disabled: PropTypes.bool,
    placement: PropTypes.oneOf([
      'bottomLeft',
      'bottomCenter',
      'bottomRight',
      'topLeft',
      'topCenter',
      'topRight',
    ]),
    target: PropTypes.oneOf(['_self', '_blank']),
    type: PropTypes.oneOf(['button']),
    linkType: PropTypes.oneOf(['refresh', 'onClick']),
    title: PropTypes.node,
    menu: PropTypes.arrayOf(PropTypes.object).isRequired,
    onClick: PropTypes.func,
  };

  static defaultProps = {
    disabled: false,
    placement: 'bottomRight',
    target: '_self',
    linkType: null,
    type: null,
    title: null,
    onClick: () => {},
  };

  handleMenuClick = e => {
    this.props.onClick(e.key);
  };

  renderMenu = menu => {
    const { target, linkType } = this.props;
    if (linkType === 'refresh') {
      return (
        <Menu>
          {menu.map(item => (
            <Item key={item.value}>
              <a target={target} href={item.href ? item.href : null}>
                {item.image ? <img alt={item.name} src={item.image} /> : null}
                {item.name}
              </a>
            </Item>
          ))}
        </Menu>
      );
    } else if (linkType === 'onClick') {
      return (
        <Menu onClick={this.handleMenuClick}>
          {menu.map(item => (
            <Item key={item.value}>
              {item.image ? <img alt={item.name} src={item.image} /> : null}
              {item.name}
            </Item>
          ))}
        </Menu>
      );
    }
    return (
      <Menu>
        {menu.map(item => (
          <Item key={item.value}>
            <Link to={item.href}>
              {item.image ? <img alt={item.name} src={item.image} /> : null}
              {item.name}
            </Link>
          </Item>
        ))}
      </Menu>
    );
  };

  render() {
    const { disabled, menu, title, placement, type } = this.props;
    return (
      <Dropdown
        placement={placement}
        overlay={this.renderMenu(menu)}
        disabled={disabled}
      >
        {type === 'button' ? (
          <Button style={{ marginLeft: 8 }}>
            <span className="ant-dropdown-link">
              {title} <Icon type="down" />
            </span>
          </Button>
        ) : (
          <span className="ant-dropdown-link">
            {title} <Icon type="down" />
          </span>
        )}
      </Dropdown>
    );
  }
}

export default withStyles(s)(CPDropdown);
