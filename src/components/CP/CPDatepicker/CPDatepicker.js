import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-jalaali';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPDatepicker.css';
import CPDatepickerContent from '../CPDatepickerContent';

class CPDatepicker extends React.Component {
  static propTypes = {
    calendarData: PropTypes.objectOf(PropTypes.any),
    onChange: PropTypes.func,
    selectedDayDefault: PropTypes.string,
  };

  static defaultProps = {
    onChange: () => {},
    selectedDayDefault: '1',
    calendarData: {
      monthExceptions: {},
      persianFromDate: '1397/12/5',
      fromDate: '/Date(-62135596800000+0330)/',
      toDate: '/Date(-62135596800000+0330)/',
      persianToDate: '1398/3/3',
      fromMobile: false,
      id: 0,
      key: 0,
      createdOnUtc: '/Date(-62135596800000+0330)/',
      updatedOnUtc: '/Date(-62135596800000+0330)/',
    },
  };

  constructor(props) {
    super(props);

    moment.loadPersian({ dialect: 'persian-modern' });
    const format = 'jYYYY/jM/jD';
    const dateTime = moment(props.calendarData.persianFromDate, format);
    const firstDateTime = moment(props.calendarData.persianFromDate, format);
    const lastDateTime = moment(props.calendarData.persianToDate, format);
    const years = dateTime.jYear();
    const month = dateTime.jMonth() + 1;
    const firstMonth = month;
    const lastMonth = lastDateTime.jMonth() + 1;
    const firstYear = years;
    const lastYear = lastDateTime.jYear();

    const week = {
      شنبه: 1,
      یک‌شنبه: 2,
      دوشنبه: 3,
      سه‌شنبه: 4,
      چهارشنبه: 5,
      پنج‌شنبه: 6,
      جمعه: 7,
    };
    const mobileWeek = {
      ش: 1,
      ی: 2,
      د: 3,
      س: 4,
      چ: 5,
      پ: 6,
      ج: 7,
    };

    this.state = {
      date: props.calendarData.persianFromDate, // Show current date
      selectedDate: moment(this.props.calendarData.persianFromDate, format),
      selectedDay: props.selectedDayDefault || dateTime.format('jDD'),
      years,
      month,
      firstMonth,
      firstYear,
      lastMonth,
      lastYear,
      day: dateTime.jDate(),
      monthName: dateTime.format('jMMMM'),
      week,
      mobileWeek,
      firstDateTime,
      lastDateTime,
      monthExceptions:
        props.calendarData.monthExceptions[dateTime.format('jYYYY/jM')],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedDayDefault !== this.props.selectedDayDefault)
      this.setState({ selectedDay: nextProps.selectedDayDefault })
  }

  prevMonth = () => {
    let { years, month } = this.state;
    const { day } = this.state;
    if (month - 1 > 0) {
      month -= 1;
    } else {
      month = 12;
      years -= 1;
    }
    const dateTime = moment(`${years}/${month}/${day}`, 'jYYYY/jM/jD');
    this.setState({
      month,
      date: `${years}/${month}/${day}`,
      monthName: dateTime.format('jMMMM'),
      monthExceptions: this.props.calendarData.monthExceptions[
        dateTime.format('jYYYY/jM')
      ],
      years,
    });
  };

  nextMonth = () => {
    let { years, month } = this.state;
    const { day } = this.state;
    if (month + 1 < 13) {
      month += 1;
    } else {
      month = 1;
      years += 1;
    }
    const dateTime = moment(`${years}/${month}/${day}`, 'jYYYY/jM/jD');
    this.setState({
      month,
      date: `${years}/${month}/${day}`,
      monthName: dateTime.format('jMMMM'),
      monthExceptions: this.props.calendarData.monthExceptions[
        dateTime.format('jYYYY/jM')
      ],
      years,
    });
  };

  selectedDayChange = day => {
    const { years, month } = this.state;
    this.setState({
      selectedDate: moment(`${years}/${month}/${day}`, 'jYYYY/jM/jD'),
      selectedDay: day,
    });
    this.props.onChange(`${years}/${month}/${day}`);
  };

  renderNextMonth = () => {
    const { years, month, lastMonth, lastYear } = this.state;
    if (years < lastYear) {
      return <i onClick={this.nextMonth} className="cp-arrow-right" />;
    } else if (years === lastYear && month < lastMonth) {
      return <i onClick={this.nextMonth} className="cp-arrow-right" />;
    }
    return <i className={cs('cp-arrow-right', s.disabled)} />;
  };

  renderPrevMonth = () => {
    const { years, month, firstMonth, firstYear } = this.state;
    if (years > firstYear) {
      return <i onClick={this.prevMonth} className="cp-arrow-left" />;
    } else if (years === firstYear && month > firstMonth) {
      return <i onClick={this.prevMonth} className="cp-arrow-left" />;
    }
    return <i className={cs('cp-arrow-left', s.disabled)} />;
  };

  render() {
    const {
      monthName,
      years,
      week,
      date,
      monthExceptions,
      selectedDay,
      selectedDate,
      firstDateTime,
      lastDateTime,
      mobileWeek,
    } = this.state;

    return (
      <div className={s.cpDatepicker}>
        <div className={s.sidebar}>
          <div className={s.sidebarYear}>{selectedDate.jYear()}</div>
          <div className={s.sidebarWeekDay}>{selectedDate.format('dddd')}،</div>
          <div className={s.sidebarDay}>
            {selectedDate.format('jDD')} {selectedDate.format('jMMMM')}
          </div>
        </div>
        <div className={s.cpDatepickerContent}>
          <div style={{ height: '365px' }}>
            <div className={s.header}>
              {this.renderNextMonth()}
              <div className={s.monthName}>
                {monthName} {years}
              </div>
              {this.renderPrevMonth()}
            </div>
            <span />
            <div className={s.daysHeader}>
              {Object.keys(week).map(item => (
                <span className={s.fullDayName} key={item}>
                  {item}
                </span>
              ))}
              {Object.keys(mobileWeek).map(item => (
                <span className={s.mobileDayName} key={item}>
                  {item}
                </span>
              ))}
            </div>
            <CPDatepickerContent
              date={date}
              from={firstDateTime}
              to={lastDateTime}
              monthExceptions={monthExceptions}
              selectedDay={selectedDay}
              onSelectDay={this.selectedDayChange}
            />
          </div>
          <div className={s.event}>
            {monthExceptions &&
              Object.keys(monthExceptions.calendarEvents).map(key => (
                <div key={key}>
                  <span className={s.eventDay}>
                    {`${moment(key, 'jYYYY/jM/jD').format('jDD')} ${moment(
                      key,
                      'jYYYY/jM/jD',
                    ).format('jMMMM')}`}
                  </span>
                  <span className={s.eventDescription}>
                    {monthExceptions.calendarEvents[key].label}
                  </span>
                </div>
              ))}
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(CPDatepicker);
