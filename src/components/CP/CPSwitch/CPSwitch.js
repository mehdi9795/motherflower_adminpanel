import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Switch } from 'antd';
import s from './CPSwitch.css';

class CPSwitch extends React.Component {
  static propTypes = {
    size: PropTypes.string,
    // checked: PropTypes.bool,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    checkedChildren: PropTypes.node,
    unCheckedChildren: PropTypes.node,
    defaultChecked: PropTypes.bool,
  };

  static defaultProps = {
    size: 'default',
    checked: false,
    disabled: false,
    onChange: () => {},
    checkedChildren: '',
    unCheckedChildren: '',
    defaultChecked: false,
  };

  render() {
    const {
      size,
      // checked,
      onChange,
      disabled,
      checkedChildren,
      unCheckedChildren,
      defaultChecked,
    } = this.props;
    return (
      <Switch
        size={size}
        checked={defaultChecked}
        // defaultChecked={defaultChecked}
        onChange={onChange}
        disabled={disabled}
        checkedChildren={checkedChildren}
        unCheckedChildren={unCheckedChildren}
      />
    );
  }
}

export default withStyles(s)(CPSwitch);
