import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { bindActionCreators } from 'redux';
import cs from 'classnames';
import { connect } from 'react-redux';
import { Card, LocaleProvider } from 'antd';
import faIR from 'antd/lib/locale-provider/fa_IR';
import s from './CPList.css';
import CPTable from '../CPTable/CPTable';
import SearchBox from '../SearchBox';
import CPButton from '../CPButton';
import CPPermission from '../CPPermission/CPPermission';

class CPList extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    columns: PropTypes.arrayOf(PropTypes.object).isRequired,
    flatPermission: PropTypes.objectOf(PropTypes.any).isRequired,
    count: PropTypes.number,
    loading: PropTypes.bool,
    children: PropTypes.node,
    onAddClick: PropTypes.func,
    onChange: PropTypes.func,
    rowClassName: PropTypes.func,
    width: PropTypes.node,
    searchBoxName: PropTypes.string,
    hideSearchBox: PropTypes.bool,
    hideAdd: PropTypes.bool,
    showTopAdd: PropTypes.bool,
    rowSelection: PropTypes.objectOf(PropTypes.any),
    withCheckBox: PropTypes.bool,
    pagination: PropTypes.bool,
    scroll: PropTypes.objectOf(PropTypes.any),
    classNameAddButton: PropTypes.string,
    className: PropTypes.string,
  };
  static defaultProps = {
    data: [],
    columns: [],
    onAddClick: () => {},
    onChange: () => {},
    rowClassName: () => {},
    count: 0,
    loading: false,
    children: '',
    width: '',
    searchBoxName: '',
    hideSearchBox: false,
    hideAdd: false,
    showTopAdd: false,
    rowSelection: {},
    withCheckBox: false,
    pagination: true,
    scroll: { x: 0 },
    classNameAddButton: '',
    className: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      hiddenSearcBox: this.props.hideSearchBox,
    };
  }

  componentWillMount() {
    const { flatPermission } = this.props;
    const { searchBoxName } = this.props;
    Object.keys(flatPermission).map(item => {
      if (flatPermission[item].resourceValue === searchBoxName) {
        this.setState({ hiddenSearcBox: true });
      }
    });
  }

  render() {
    const {
      data,
      count,
      columns,
      loading = false,
      onAddClick,
      width,
      hideAdd,
      showTopAdd,
      rowSelection,
      withCheckBox,
      pagination,
      onChange,
      rowClassName,
      scroll,
      classNameAddButton,
      className,
    } = this.props;
    const { hiddenSearcBox } = this.state;
    return (
      <LocaleProvider locale={faIR}>
        <Card className={s.cardTable}>
          {!hiddenSearcBox && <SearchBox>{this.props.children}</SearchBox>}
          {showTopAdd && (
            <CPPermission>
              <div name="product-list-create">
                <div className="row" style={{ position: 'relative' }}>
                  <CPButton
                    onClick={onAddClick}
                    type="primary"
                    className={cs(
                      s.createRecord,
                      classNameAddButton,
                      s.topAddButton,
                    )}
                    shape="circle"
                  >
                    <i className="cp-add" />
                  </CPButton>
                </div>
              </div>
            </CPPermission>
          )}
          {withCheckBox && (
            <CPTable
              width={width}
              data={data}
              count={count}
              columns={columns}
              loading={loading}
              rowSelection={rowSelection}
              pagination={pagination}
              onChange={onChange}
              rowClassName={rowClassName}
              scroll={scroll}
              className={className}
            />
          )}
          {!withCheckBox && (
            <CPTable
              width={width}
              data={data}
              count={count}
              columns={columns}
              loading={loading}
              pagination={pagination}
              onChange={onChange}
              rowClassName={rowClassName}
              scroll={scroll}
              className={className}
            />
          )}
          {!hideAdd && (
            <CPPermission>
              <div name="product-list-create">
                <CPButton
                  onClick={onAddClick}
                  type="primary"
                  className={cs(s.createRecord, classNameAddButton)}
                  shape="circle"
                >
                  <i className="cp-add" />
                </CPButton>
              </div>
            </CPPermission>
          )}
        </Card>
      </LocaleProvider>
    );
  }
}

const mapStateToProps = state => ({
  flatPermission:
    state.permission.data && state.permission.data.items.length > 0
      ? state.permission.data.items[0].flatermission
      : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CPList));
