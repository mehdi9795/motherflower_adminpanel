import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Upload, Icon } from 'antd';
import s from './CPDragger.css';
import { CLICK_OR_DRAG } from '../../../Resources/Localization';

const Dragger = Upload.Dragger;

class CPDragger extends React.Component {
  static propTypes = {
    accept: PropTypes.string,
    action: PropTypes.string,
    name: PropTypes.string,
    onChange: PropTypes.func,
    beforeUpload: PropTypes.func,
    onRemove: PropTypes.func,
  };

  static defaultProps = {
    accept: '',
    action: '',
    name: '',
    onChange: () => {},
    onRemove: () => {},
    beforeUpload: () => {},
  };

  render() {
    const {
      accept,
      action,
      name,
      onChange,
      onRemove,
      beforeUpload,
    } = this.props;
    return (
      <Dragger
        accept={accept}
        action={action}
        name={name}
        onChange={onChange}
        onRemove={onRemove}
        beforeUpload={beforeUpload}
      >
        <p>
          <i className="cp-upload" />
        </p>
        <p>{CLICK_OR_DRAG}</p>
      </Dragger>
    );
  }
}

export default withStyles(s)(CPDragger);
