import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Radio } from 'antd';
import cs from 'classnames';
import s from './CPRadio.css';
import CPPopover from '../CPPopover';

const { Group } = Radio;

class CPRadio extends React.Component {
  static propTypes = {
    model: PropTypes.arrayOf(PropTypes.object).isRequired,
    disabled: PropTypes.bool,
    customRadio: PropTypes.bool,
    onChange: PropTypes.func,
    defaultValue: PropTypes.string,
  };

  static defaultProps = {
    children: '',
    disabled: false,
    customRadio: false,
    name: '',
    onChange: () => {},
    size: 'default',
    defaultValue: 0,
  };

  loop = (data, disabled) =>
    data.map(item => (
      <Radio disabled={disabled} value={item.value} key={item.value}>
        {item.name}
      </Radio>
    ));

  loopCustom = (data, onChange, defaultValue) =>
    data.map(item => (
      <CPPopover content={item.name} key={item.value} placement="top" trigger="hover">
        <input
          type="checkbox"
          onChange={onChange}
          checked={item.value === defaultValue}
          value={item.value}
          // defaultValue={defaultValue}
          id={item.value}
          hidden
        />
        <label
          className={cs(s.colorSwitch, item.value === defaultValue ? s.selectedStyle : s.defaultStyle)}
          htmlFor={item.value}
        >{item.name}</label>
      </CPPopover>
    ));

  render() {
    const { model, disabled, onChange, defaultValue, customRadio } = this.props;
    return !customRadio ? (
      <Group value={defaultValue} onChange={onChange}>
        {this.loop(model, disabled)}
      </Group>
    ) : (
      <div>
        {this.loopCustom(model, onChange, defaultValue)}

      </div>
    );
  }
}

export default withStyles(s)(CPRadio);
