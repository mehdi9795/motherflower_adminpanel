import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Badge } from 'antd';
import s from './CPBadge.css';

class CPBadge extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    count: PropTypes.node,
    status: PropTypes.string,
    text: PropTypes.string,
  };

  static defaultProps = {
    children: '',
    count: '',
    status: '',
    text: '',
  };

  render() {
    const { count, status, text } = this.props;
    return (
      <Badge count={count} status={status} text={text}>
        {this.props.children}
      </Badge>
    );
  }
}

export default withStyles(s)(CPBadge);
