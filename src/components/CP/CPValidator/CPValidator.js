import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './CPValidator.css';

const cx = require('classnames');

class CPValidator extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    value: PropTypes.string,
    name: PropTypes.string,
    minLength: PropTypes.number,
    validationEmail: PropTypes.bool,
    isRequired: PropTypes.bool,
    showMessage: PropTypes.bool,
    checkValidation: PropTypes.func,
  };

  static defaultProps = {
    children: '',
    value: '',
    name: '',
    showMessage: false,
    minLength: 0,
    validationEmail: false,
    isRequired: false,
    checkValidation: () => {},
  };

  constructor(props) {
    super(props);
    this.childrenWithClassName = '';
    this.errorMessage = '';
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.value !== nextProps.value ||
      nextProps.value.length > 0 ||
      nextProps.showMessage
    ) {
      const re = /^([a-zA-Z0-9])([a-zA-Z0-9\._])*@(([a-zA-Z0-9])+(\.))+([a-zA-Z]{2,4})+$/;
      if (
        nextProps.validationEmail &&
        !re.test(nextProps.value)
        // !/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(nextProps.value)
      ) {
        this.validation(
          nextProps,
          'bar_noValid',
          'لطفا ایمیل را صحیح وارد کنید',
          'false',
        );
      } else if (nextProps.minLength > nextProps.value.length) {
        this.validation(
          nextProps,
          'bar_noValid',
          `${nextProps.name} حداقل باید ${nextProps.minLength} کاراکتر باشد.`,
          'false',
        );
      } else if (nextProps.isRequired && nextProps.value.length === 0) {
        this.validation(
          nextProps,
          'bar_noValid',
          `${nextProps.name} اجباری است.`,
          'false',
        );
      } else {
        this.validation(nextProps, 'bar_isValid', ``, 'true');
      }
    } else this.validation(nextProps, 'bar', ``, 'false');
  }

  validation = (props, className, message, isValid) => {
    const child = React.cloneElement(props.children, {
      isValidClass: cx(props.children.props.isValidClass, className),
      isValid: cx(props.children.props.isValid, isValid),
    });
    if (props.showMessage) this.errorMessage = message;
    this.childrenWithClassName = child;
    props.checkValidation(props.name, isValid);
  };

  render() {
    return this.childrenWithClassName ? (
      <div>
        {this.childrenWithClassName}
        <p className={s.errorMessage}>{this.errorMessage}</p>
      </div>
    ) : (
      <div>{this.props.children}</div>
    );
  }
}

export default withStyles(s)(CPValidator);
