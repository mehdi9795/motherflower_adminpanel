import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPPermission.css';

const cx = require('classnames');

class CPPermission extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    flatermission: PropTypes.objectOf(PropTypes.any),
    tag: PropTypes.string,
  };

  static defaultProps = {
    flatermission: {},
    tag: 'div',
  };

  filtering(child) {
    const { flatermission } = this.props;
    try {
      Object.keys(flatermission).map(item => {
        if (flatermission[item].resourceValue === child.props.name) {
          if (flatermission[item].permissionAttribute === 'Readonly') {
            throw React.cloneElement(child, {
              className: cx(child.props.className, 'disabled'),
            });
          } else if (flatermission[item].permissionAttribute === 'Hide') {
            throw React.cloneElement(child, {
              className: cx(child.props.className, 'hide'),
            });
          }
        }
        return null;
      });
      throw child;
    } catch (e) {
      return e;
    }
  }

  render() {
    const { children, tag } = this.props;

    const childrenWithProps = React.Children.map(children, child =>
      this.filtering(child),
    );

    return tag === 'div' ? (
      <div className={childrenWithProps[0].props.className}>
        {childrenWithProps[0].props.children}
      </div>
    ) : (
      <span className={childrenWithProps[0].props.className}>
        {childrenWithProps[0].props.children}
      </span>
    );
  }
}

const mapStateToProps = state => ({
  flatermission:
    state.permission.data && state.permission.data.items.length > 0
      ? state.permission.data.items[0].flatermission
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CPPermission));
