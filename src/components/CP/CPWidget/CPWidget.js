import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPWidget.css';
import CPCounter from '../CPCounter';

class CPWidget extends React.Component {
  static propTypes = {
    icon: PropTypes.string,
    label: PropTypes.string,
    content: PropTypes.string,
    unit: PropTypes.string,
    value: PropTypes.number,
  };

  static defaultProps = {
    icon: '',
    label: '',
    content: '',
    unit: '',
    value: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      newValue: 0,
    };

    if (props.value) this.counter(props.value);
  }

  counter = value => {
    let count = 0;
    const couner1 = setInterval(() => {
      count += 1;
      // this.setState({ newValue: this.comma(count)})
      if (value <= count) clearInterval(couner1);
      return count;
    }, 0.000005);
  };

  comma = num => {
    const str = num.toString().split('.');
    if (str[0].length >= 4) {
      str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    if (str[1] && str[1].length >= 4) {
      str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
  };

  render() {
    const { icon, label, content, unit, value } = this.props;

    return (
      <div className="col-lg-3 col-sm-6 col-xs-12 container">
        <div>
          <i className={icon} />
          <label>{label}</label>
          {content && <div>{content}</div>}
          {value !== 0 && (
            <div>
              {value
                .toString(10)
                .replace(/\D/g, '0')
                .split('')
                .map(item => (
                  <CPCounter
                    key={Math.random().toString()}
                    value={parseInt(item, 0)}
                  />
                ))}{' '}
              <span>{unit}</span>
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default withStyles(s)(CPWidget);
