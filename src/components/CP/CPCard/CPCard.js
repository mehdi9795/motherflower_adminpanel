import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Card } from 'antd';
import s from './CPCard.css';

class CPCard extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    title: PropTypes.node,
    className: PropTypes.string,
  };

  static defaultProps = {
    children: '',
    title: '',
    className: '',
  };

  render() {
    const { title, className } = this.props;
    return (
      <Card className={className} title={title}>
        {this.props.children}
      </Card>
    );
  }
}

export default withStyles(s)(CPCard);
