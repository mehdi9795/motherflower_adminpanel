import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HotKeys from 'react-hot-keys';
import { Select, message } from 'antd';
import moment from 'moment-jalaali';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorBranchInfo.css';
import districtOptionListActions from '../../../../redux/common/actionReducer/district/List';
import vendorBranchPostActions from '../../../../redux/vendor/actionReducer/vendorBranch/Post';
import vendorBranchDeleteActions from '../../../../redux/vendor/actionReducer/vendorBranch/Delete';
import vendorBranchPutActions from '../../../../redux/vendor/actionReducer/vendorBranch/Put';
import {
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  INFO_BASIC,
  VENDOR_BRANCH_CODE,
  VENDOR_BRANCH_NAME,
  AGENT_NAME,
  DESCRIPTION,
  PICKUP,
  DELIVERY,
  CONTACT_INFO,
  CITY,
  DISTRICT,
  ADDRESS,
  PHONE,
  FAX,
  DISTRICT_SELECTED,
  CITY_SELECTED,
  CATEGORY,
  VENDOR_SELECTED,
  ACTIVE,
  DEACTIVATE,
  GRADE,
  FROM_HOUR,
  TO_HOUR,
  MAP,
  COVERED_CITIES,
  IMAGE_UPLOAD,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import CPInput from '../../../CP/CPInput';
import CPTextArea from '../../../CP/CPTextArea';
import history from '../../../../history';
import CPPanel from '../../../CP/CPPanel/CPPanel';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPMultiSelect from '../../../CP/CPMultiSelect/CPMultiSelect';
import CPSwitch from '../../../CP/CPSwitch';
import { CityDto, DistrictDto } from '../../../../dtos/commonDtos';
import {
  VendorBranchCitiesDto,
  VendorBranchDto,
} from '../../../../dtos/vendorDtos';
import CPInputMask from '../../../CP/CPInputMask';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import CPTimepicker from '../../../CP/CPTimepicker';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
  BaseListCRUDDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPMap from '../../../CP/CPMap';
import { getDtoQueryString, showNotification } from '../../../../utils/helper';
import { getNearestLocationsApi } from '../../../../services/commonApi';
import CPUpload from '../../../CP/CPUpload';
import { ImageDto } from '../../../../dtos/cmsDtos';

const { Option } = Select;

class VendorBranchInfo extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    vendors: PropTypes.arrayOf(PropTypes.object),
    districtOptions: PropTypes.arrayOf(PropTypes.object),
    categories: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    vendorId: PropTypes.string,
    loginData: PropTypes.objectOf(PropTypes.any),
  };
  static defaultProps = {
    data: null,
    vendors: [],
    categories: [],
    cities: [],
    districtOptions: [],
    vendorId: '',
    loginData: {},
  };
  constructor(props) {
    super(props);
    const coveredCityIds = [];
    if (props.data && props.data.vendorBranchCityDtos)
      props.data.vendorBranchCityDtos.map(item =>
        coveredCityIds.push(item.cityDto.id.toString()),
      );

    this.state = {
      selectedCategories: [],
      code: props.data ? props.data.code : '',
      name: props.data ? props.data.name : '',
      deliveryEnabled: props.data ? props.data.deliveryEnabled : true,
      pickupEnabled: props.data ? props.data.pickupEnabled : true,
      description: props.data ? props.data.description : '',
      active: props.data ? props.data.active : true,
      address: props.data ? props.data.address : '',
      phone: props.data ? props.data.phone : '',
      fax: props.data ? props.data.fax : '',
      cityId: props.data ? props.data.cityId.toString() : '0',
      districtId: props.data && props.data.districtId
        ? `${props.data.districtId.toString()}*${props.data.districtDto.lat}*${
            props.data.districtDto.lng
          }`
        : '0',
      lat: props.data ? props.data.lat : 35.6891975,
      lng: props.data ? props.data.lng : 51.3889736,
      vendorId: props.data ? props.data.vendorId.toString() : '0',
      vendorBranchGrade: props.data ? props.data.vendorBranchGrade : 'VIP',
      closingHour: props.data
        ? moment(props.data.closingHour, 'HH:mm')
        : moment('23:0', 'HH:mm'),
      openingHour: props.data
        ? moment(props.data.openingHour, 'HH:mm')
        : moment('10:0', 'HH:mm'),
      finalClosingHour: '23:0',
      finalOpeningHour: '10:0',
      coveredCities: coveredCityIds,
      pictureId: props.data ? props.data.pictureId : 0,
      url:
        props.data && props.data.imageDtos && props.data.imageDtos.length > 0
          ? `${props.data.imageDtos[0].url}`
          : '',
      binariesImage: '',
      extention: '',
    };
    this.setLatAndLng = this.setLatAndLng.bind(this);
  }

  componentWillMount() {
    const { data, vendorId, categories } = this.props;
    const isEdit = data && data.id;
    const { selectedCategories } = this.state;

    if (data && data.districtId)
      this.cityChange(
        'cityId',
        data.cityId.toString(),
        `${data.districtId.toString()}*${data.districtDto.lat}*${
          data.districtDto.lng
        }`,
      );

    if (isEdit) {
      if (data.vendorCategories) {
        data.vendorCategories.map(item =>
          this.state.selectedCategories.push(item.categoryDto.id.toString()),
        );
      }
    } else if (!isEdit) {
      if (vendorId) this.setState({ vendorId });

      /**
       * map secondary Category for comboBox Datasource for default value
       */
      categories.map(item => {
        if (!isEdit) {
          selectedCategories.push(item.id.toString());
        }
        return null;
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.districtDataSource = [];
    /**
     * get district for comboBox dataSource with cityId
     * @type {string}
     */

    if (nextProps.districtOptions) {
      nextProps.districtOptions.map(item =>
        this.districtDataSource.push(
          <Option key={`${item.id}*${item.lat}*${item.lng}`}>
            {item.name}
          </Option>,
        ),
      );
    }
  }

  /**
   * set value category multi select to state
   * @param value
   */
  onChangeCategorySelect = value => {
    this.setState({
      selectedCategories: value,
    });
  };

  async setLatAndLng(value) {
    const { loginData } = this.props;
    this.setState({ lat: value.lat, lng: value.lng });
    const response = await getNearestLocationsApi(
      loginData,
      getDtoQueryString(
        JSON.stringify({
          dto: { lat: value.lat, lng: value.lng },
          fromCache: true,
        }),
      ),
    );

    if (
      response.status === 200 &&
      response.data.items &&
      response.data.items.length > 0
    ) {
      this.cityChange(
        'cityId',
        response.data.items[0].cityDto.id.toString(),
        `${response.data.items[0].id.toString()}*${
          response.data.items[0].lat
        }*${response.data.items[0].lng}`,
      );
    } else {
      showNotification(
        'error',
        '',
        'محله مورد نظر تحت پوشش نیست',
        15,
        'errorBox',
      );
    }
  }

  /**
   * change city for automatic change district
   * @param inputName
   * @param value
   * @returns {Promise<void>}
   */
  async cityChange(inputName, value, districtId) {
    this.props.actions.districtOptionListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityDto: new CityDto({ id: value }),
          }),
        )
        .buildJson(),
    );

    this.setState({
      [inputName]: value,
      districtId: districtId !== undefined ? districtId : '0',
    });
  }

  /**
   * set dto with state for post and put data
   * @returns {{dto: {name: *, code: *, deliveryEnabled: *, pickupEnabled: *, description: *, active: *, address: *, phone: *, fax: *, cityId: *, districtId: *, lat: *, lng: *, vendorId: *, vendorCategories: Array, id: number}}}
   */
  prepareContainer = () => {
    const {
      name,
      code,
      deliveryEnabled,
      pickupEnabled,
      description,
      active,
      address,
      phone,
      fax,
      cityId,
      districtId,
      vendorId,
      selectedCategories,
      vendorBranchGrade,
      finalOpeningHour,
      finalClosingHour,
      lat,
      lng,
      binariesImage,
      extention,
    } = this.state;
    const { data } = this.props;

    const vendorCategory = [];
    selectedCategories.map(item =>
      vendorCategory.push({
        categoryDto: { id: item },
        vendorBranchDto: { id: data && data.id ? data.id : 0 },
      }),
    );

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new VendorBranchDto({
          id: data && data.id ? data.id : 0,
          active,
          address,
          vendorBranchGrade,
          code,
          name,
          vendorId,
          description,
          vendorCategories: vendorCategory,
          cityId,
          districtId: districtId.split('*')[0],
          phone,
          fax,
          deliveryEnabled,
          pickupEnabled,
          openingHour: finalOpeningHour,
          closingHour: finalClosingHour,
          lat,
          lng,
          imageDtos: binariesImage
            ? [
                new ImageDto({
                  extention,
                  binaries: binariesImage.split(',')[1],
                }),
              ]
            : null,
        }),
      )
      .build();

    return jsonCrud;
  };

  /**
   * save data with post and put - redirect to list page
   */
  saveForm = () => {
    const { data } = this.props;
    const { pickupEnabled, deliveryEnabled, coveredCities } = this.state;
    if (!pickupEnabled && !deliveryEnabled) {
      message.error(
        'حداقل یکی از گزینه های دلیوری (Delivery) یا پیکاپ (Pickup) باید فعال باشد.',
        5,
      );
    } else if (data && data.id) {
      const covered = [];
      coveredCities.map(item => {
        const crud = new VendorBranchCitiesDto({
          cityDto: new CityDto({ id: item }),
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
        });
        covered.push(crud);
        return null;
      });
      const putData = {
        data: this.prepareContainer(),
        status: 'save',
        coveredCities: new BaseListCRUDDtoBuilder().dtos(covered).build(),
      };
      this.props.actions.vendorBranchPutRequest(putData);
    } else {
      // this.postData(this.prepareContainer());
      const postData = { data: this.prepareContainer(), status: 'save' };
      this.props.actions.vendorBranchPostRequest(postData);
    }
  };

  /**
   * save data with post and put - redirect to edit page
   */
  saveFormAndContinue = () => {
    const { data } = this.props;
    const { pickupEnabled, deliveryEnabled, coveredCities } = this.state;
    if (!pickupEnabled && !deliveryEnabled) {
      message.error(
        'حداقل یکی از گزینه های دلیوری (Delivery) یا پیکاپ (Pickup) باید فعال باشد.',
        5,
      );
    } else if (data && data.id) {
      const covered = [];
      coveredCities.map(item => {
        const crud = new VendorBranchCitiesDto({
          cityDto: new CityDto({ id: item }),
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
        });
        covered.push(crud);
        return null;
      });
      const putData = {
        data: this.prepareContainer(),
        status: 'saveAndContinue',
        coveredCities: new BaseListCRUDDtoBuilder().dtos(covered).build(),
      };
      this.props.actions.vendorBranchPutRequest(putData);
    } else {
      // this.postData(this.prepareContainer(), true);
      const postData = {
        data: this.prepareContainer(),
        status: 'saveAndContinue',
      };
      this.props.actions.vendorBranchPostRequest(postData);
    }
  };

  redirect = id => {
    history.push(`/vendor-branch/edit/${id}`);
  };

  onChangeTimePicker = (inputName, time, timeString) => {
    if (inputName === 'openingHour')
      this.setState({ [inputName]: time, finalOpeningHour: timeString });
    else this.setState({ [inputName]: time, finalClosingHour: timeString });
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    if (inputName === 'districtId') {
      this.setState({
        [inputName]: value,
        lat: parseFloat(value.split('*')[1]),
        lng: parseFloat(value.split('*')[2]),
      });
    } else this.setState({ [inputName]: value });
  };

  handleImageChange = value => {
    this.setState({
      binariesImage: value.base64,
      extention: value.type.split('/')[1],
    });
  };

  render() {
    const { vendors, categories, cities, data } = this.props;
    const {
      districtId,
      code,
      name,
      deliveryEnabled,
      active,
      address,
      cityId,
      description,
      fax,
      phone,
      pickupEnabled,
      selectedCategories,
      vendorBranchGrade,
      closingHour,
      openingHour,
      lat,
      lng,
      coveredCities,
      url,
    } = this.state;

    // const isEdit = data && data.id;
    const vendorArray = [];
    const categoryArray = [];
    const cityArray = [];
    /**
     * map vendors for comboBox Datasource
     */
    vendors.map(item =>
      vendorArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map secondary Category for comboBox Datasource
     */
    categories.map(item => {
      categoryArray.push(<Option key={item.id}>{item.name}</Option>);
      return null;
    });

    /**
     * map city for comboBox Datasource
     */
    cities.map(item =>
      cityArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <CPPermission>
          <div name="vendorBranch-management-info">
            <div className="tabContent">
              <div className="row">
                <div className="col-lg-6 col-sm-12">
                  <CPPanel header={INFO_BASIC}>
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{VENDOR_BRANCH_CODE}</label>
                        <CPInput
                          name="code"
                          value={code}
                          placeholder={VENDOR_BRANCH_CODE}
                          onChange={value =>
                            this.handelChangeInput('code', value.target.value)
                          }
                        />
                      </div>
                      <div className="col-lg-6 col-sm-12">
                        <label>{VENDOR_BRANCH_NAME}</label>
                        <CPInput
                          name="name"
                          value={name}
                          placeholder={VENDOR_BRANCH_NAME}
                          onChange={value =>
                            this.handelChangeInput('name', value.target.value)
                          }
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{AGENT_NAME}</label>
                        <CPSelect
                          name="vendorId"
                          showSearch
                          value={this.state.vendorId}
                          onChange={value =>
                            this.handelChangeInput('vendorId', value)
                          }
                        >
                          <Option key="0">{VENDOR_SELECTED}</Option>
                          {vendorArray}
                        </CPSelect>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{GRADE}</label>
                        <CPSelect
                          value={vendorBranchGrade}
                          onChange={value => {
                            this.handelChangeInput('vendorBranchGrade', value);
                          }}
                        >
                          <Option value="VIP" key="VIP">
                            VIP
                          </Option>
                          <Option value="Gold" key="Gold">
                            Gold
                          </Option>
                          <Option value="Silver" key="Silver">
                            Silver
                          </Option>
                          <Option value="Boronz" key="Boronz">
                            Boronz
                          </Option>
                        </CPSelect>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{CATEGORY}</label>
                        <CPMultiSelect
                          onChange={this.onChangeCategorySelect}
                          placeholder=""
                          value={selectedCategories}
                        >
                          {categoryArray}
                        </CPMultiSelect>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{FROM_HOUR}</label>
                        <CPTimepicker
                          onChange={(time, timeString) =>
                            this.onChangeTimePicker(
                              'openingHour',
                              time,
                              timeString,
                            )
                          }
                          value={openingHour}
                        >
                          {categoryArray}
                        </CPTimepicker>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{TO_HOUR}</label>
                        <CPTimepicker
                          onChange={(time, timeString) =>
                            this.onChangeTimePicker(
                              'closingHour',
                              time,
                              timeString,
                            )
                          }
                          value={closingHour}
                        >
                          {categoryArray}
                        </CPTimepicker>
                      </div>
                    </div>
                    {data &&
                      data.id && (
                        <div className="row">
                          <div className="col-sm-12">
                            <label>{COVERED_CITIES}</label>
                            <CPMultiSelect
                              placeholder={COVERED_CITIES}
                              onChange={value =>
                                this.handelChangeInput('coveredCities', value)
                              }
                              value={coveredCities}
                            >
                              {cityArray}
                            </CPMultiSelect>
                          </div>
                        </div>
                      )}
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{DESCRIPTION}</label>
                        <CPTextArea
                          name="description"
                          value={description}
                          placeholder={DESCRIPTION}
                          onChange={value =>
                            this.handelChangeInput(
                              'description',
                              value.target.value,
                            )
                          }
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <CPSwitch
                          defaultChecked={deliveryEnabled}
                          checkedChildren={DELIVERY}
                          unCheckedChildren={DELIVERY}
                          onChange={value =>
                            this.handelChangeInput('deliveryEnabled', value)
                          }
                        />
                        <CPSwitch
                          defaultChecked={pickupEnabled}
                          checkedChildren={PICKUP}
                          unCheckedChildren={PICKUP}
                          onChange={value =>
                            this.handelChangeInput('pickupEnabled', value)
                          }
                        />
                        <CPSwitch
                          defaultChecked={active}
                          checkedChildren={ACTIVE}
                          unCheckedChildren={DEACTIVATE}
                          onChange={value =>
                            this.handelChangeInput('active', value)
                          }
                        />
                      </div>
                      <div className="col-sm-12">
                        <label>{IMAGE_UPLOAD}</label>
                        <CPUpload
                          onChange={this.handleImageChange}
                          image={`${url}`}
                        />
                      </div>
                    </div>
                  </CPPanel>
                </div>
                <div className="col-lg-6 col-sm-12">
                  <CPPanel header={CONTACT_INFO}>
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{CITY}</label>
                        <CPSelect
                          name="cityId"
                          showSearch
                          value={cityId}
                          onChange={value => {
                            this.cityChange('cityId', value);
                          }}
                        >
                          <Option key="0">{CITY_SELECTED}</Option>
                          {cityArray}
                        </CPSelect>
                      </div>
                      <div className="col-lg-6 col-sm-12">
                        <label>{DISTRICT}</label>
                        <CPSelect
                          value={districtId}
                          // value={districtId}
                          showSearch
                          onChange={value => {
                            this.handelChangeInput('districtId', value);
                          }}
                        >
                          <Option key="0">{DISTRICT_SELECTED}</Option>
                          {this.districtDataSource}
                        </CPSelect>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{PHONE}</label>
                        <CPInputMask
                          onChange={value => {
                            this.handelChangeInput('phone', value);
                          }}
                          placeholder={PHONE}
                          value={phone}
                          prefix={<i className="cp cp-mobile" />}
                          mask="0\999 999 99 99"
                        />
                      </div>
                      <div className="col-lg-6 col-sm-12">
                        <label>{FAX}</label>
                        <CPInputMask
                          onChange={value => {
                            this.handelChangeInput('fax', value);
                          }}
                          placeholder={FAX}
                          value={fax}
                          prefix={<i className="cp cp-mobile" />}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{ADDRESS}</label>
                        <CPInput
                          placeholder={ADDRESS}
                          value={address}
                          onChange={value => {
                            this.handelChangeInput(
                              'address',
                              value.target.value,
                            );
                          }}
                        />
                      </div>
                      <div className="col-sm-12">
                        <label>{MAP}</label>
                        <CPMap
                          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
                          containerElement="500px"
                          loadingElement="100%"
                          mapElement="100%"
                          defaultZoom={14}
                          draggable
                          lat={lat || 35.6891975}
                          lng={lng || 51.3889736}
                          onDragEnd={value => this.setLatAndLng(value)}
                          searchBox
                        />
                      </div>
                    </div>
                  </CPPanel>
                </div>
              </div>
            </div>
            <div className={s.textLeft}>
              <CPButton
                icon="form"
                className="btn-primary"
                onClick={this.saveFormAndContinue}
              >
                {FORM_SAVE_AND_CONTINUE}
              </CPButton>
              <CPButton icon="save" onClick={this.saveForm}>
                {FORM_SAVE}
              </CPButton>{' '}
            </div>
          </div>
        </CPPermission>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  vendors: state.vendorList.data.items,
  cities: state.cityList.data.items,
  categories: state.secondaryLevelList.data.items,
  data:
    state.singleVendorBranch.data && state.singleVendorBranch.data.items
      ? state.singleVendorBranch.data.items[0]
      : null,
  districtOptions: state.districtList.data ? state.districtList.data.items : [],
  loginData: state.login.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      districtOptionListRequest:
        districtOptionListActions.districtOptionListRequest,
      vendorBranchPostRequest: vendorBranchPostActions.vendorBranchPostRequest,
      vendorBranchPutRequest: vendorBranchPutActions.vendorBranchPutRequest,
      vendorBranchDeleteRequest:
        vendorBranchDeleteActions.vendorBranchDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchInfo));
