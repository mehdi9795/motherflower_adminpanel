import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select, message } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorBranchOffShift.css';
import vendorBranchOffShiftPostActions from '../../../../redux/vendor/actionReducer/vendorBranchOffShift/Post';
import vendorBranchOffShiftDeleteActions from '../../../../redux/vendor/actionReducer/vendorBranchOffShift/Delete';
import {
  STATUS,
  FROM_DATE,
  TO_DATE,
  FROM_HOUR,
  TO_HOUR,
  DELIVERY,
  PICKUP,
  CLOSE,
  QUESTION_DELETE_OFF_SHIFT,
  YES,
  NO,
} from '../../../../Resources/Localization';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPList from '../../../CP/CPList/CPList';
import CPModal from '../../../CP/CPModal';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import {
  VendorBranchDto,
  VendorBranchOffShiftsDto,
} from '../../../../dtos/vendorDtos';
import CPPersianCalendar from '../../../CP/CPPersianCalendar';
import CPButton from '../../../CP/CPButton';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class VendorBranchOffShift extends React.Component {
  static propTypes = {
    vendorBranchOffShifts: PropTypes.arrayOf(PropTypes.object),
    vendorBranchOffShiftLoading: PropTypes.bool,
    vendorBranchOffShiftCount: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    data: null,
    vendorBranchOffShifts: null,
    vendorBranchOffShiftLoading: false,
    vendorBranchOffShiftCount: 0,
  };
  constructor(props) {
    super(props);
    this.state = {
      visibleOffShiftModal: false,
      toHour: '22',
      fromHour: '7',
      fromDate: '',
      toDate: '',
      offShiftStatus: 'Delivery',
      disabled: true,
      toHourArray: [],
    };
    this.insertOffShift = this.insertOffShift.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    for (let i = 8; i < 23; i += 1) {
      this.state.toHourArray.push(<Option key={`${i}`}>{i}</Option>);
    }
  }

  async onDelete(id) {
    const { data } = this.props;

    /**
     * get Vendor branch off shift for first time with vendorBranchId and offStatus
     * @type {string}
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchOffShiftsDto({
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
        }),
      )
      .buildJson();

    const deleteData = { id, dto: jsonList };

    this.props.actions.vendorBranchOffShiftDeleteRequest(deleteData);
  }

  async insertOffShift() {
    const { toHour, fromHour, fromDate, toDate, offShiftStatus } = this.state;
    if (fromDate === '') {
      message.error('لطفا تاریخ شروع را انتخاب کنید');
    } else {
      this.setState({
        visibleOffShiftModal: false,
      });

      const { data } = this.props;

      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new VendorBranchOffShiftsDto({
            persianFromDateTimeUtc: fromDate,
            persianToDateTimeUtc: toDate || fromDate,
            fromHour,
            toHour,
            offStatus: offShiftStatus,
            vendorBranchDto: new VendorBranchDto({ id: data.id }),
          }),
        )
        .build();

      /**
       * get Vendor branch off shift for first time with vendorBranchId and offStatus
       * @type {string}
       */

      const jsonList = new BaseGetDtoBuilder()
        .dto(
          new VendorBranchOffShiftsDto({
            vendorBranchDto: new VendorBranchDto({ id: data.id }),
          }),
        )
        .buildJson();

      const postData = {
        data: jsonCrud,
        dto: jsonList,
      };

      this.props.actions.vendorBranchOffShiftPostRequest(postData);
    }
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });

    if (inputName === 'fromHour') {
      this.setState({ toHourArray: [] });
      const toArray = [];
      for (let i = parseInt(value, 0) + 1; i < 23; i += 1) {
        toArray.push(<Option key={`${i}`}>{i}</Option>);
      }

      this.setState({ toHourArray: toArray, toHour: '22' });
    }

    if (inputName === 'fromDate') {
      this.setState({ disabled: false });
    }
  };

  showModalOffShift = () => {
    this.setState({
      visibleOffShiftModal: true,
    });
  };

  handleCancelModal = () => {
    this.setState({
      visibleOffShiftModal: false,
      toHour: '22',
      fromHour: '7',
      fromDate: '',
      toDate: '',
      offShiftStatus: 'Delivery',
      disabled: true,
      toHourArray: [],
    });
  };

  render() {
    const {
      vendorBranchOffShifts,
      vendorBranchOffShiftLoading,
      vendorBranchOffShiftCount,
    } = this.props;
    const {
      fromHour,
      toHour,
      toDate,
      fromDate,
      offShiftStatus,
      disabled,
      toHourArray,
    } = this.state;

    const columnsVendorBranchOffShift = [
      {
        title: FROM_DATE,
        dataIndex: 'persianFromDateTimeUtc',
        key: 'fromDateTimeUtc',
      },
      {
        title: TO_DATE,
        dataIndex: 'persianToDateTimeUtc',
        key: 'toDateTimeUtc',
      },
      {
        title: FROM_HOUR,
        dataIndex: 'fromHour',
        key: 'fromHour',
      },
      {
        title: TO_HOUR,
        dataIndex: 'toHour',
        key: 'toHour',
      },
      { title: STATUS, dataIndex: 'offStatus', key: 'offStatus' },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <CPPopConfirm
            title={QUESTION_DELETE_OFF_SHIFT}
            okText={YES}
            cancelText={NO}
            okType="primary"
            onConfirm={() => this.onDelete(record.key)}
          >
            <CPButton className="delete_action">
              <i className="cp-trash" />
            </CPButton>
          </CPPopConfirm>
        ),
      },
    ];
    let i = 7;
    const fromHourArray = [];
    for (i = 7; i < 23; i += 1) {
      fromHourArray.push(<Option key={`${i}`}>{i}</Option>);
    }

    return (
      <CPPermission>
        <div name="vendorBranch-management-offShift">
          <CPList
            data={vendorBranchOffShifts}
            columns={columnsVendorBranchOffShift}
            count={vendorBranchOffShiftCount}
            loading={vendorBranchOffShiftLoading}
            onAddClick={this.showModalOffShift}
            hideSearchBox
          />
          <CPModal
            visible={this.state.visibleOffShiftModal}
            handleOk={this.insertOffShift}
            handleCancel={this.handleCancelModal}
          >
            <div className="row">
              <div className="col-sm-12">
                <label>{STATUS}</label>
                <CPSelect
                  name="offShiftStatus"
                  defaultValue={offShiftStatus}
                  onChange={value => {
                    this.handelChangeInput('offShiftStatus', value);
                  }}
                >
                  <Option key="Delivery">{DELIVERY}</Option>
                  <Option key="Pickup">{PICKUP}</Option>
                  <Option key="Close">{CLOSE}</Option>
                </CPSelect>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{FROM_DATE}</label>
                <CPPersianCalendar
                  placeholder={FROM_DATE}
                  // format="jYYYY/jMM/jDD"
                  onChange={(unix, formatted) =>
                    this.handelChangeInput('fromDate', formatted)
                  }
                  preSelected={fromDate}
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{FROM_HOUR}</label>
                <CPSelect
                  name="fromHour"
                  disabled={disabled}
                  defaultValue={fromHour}
                  onChange={value => {
                    this.handelChangeInput('fromHour', value);
                  }}
                >
                  {fromHourArray}
                </CPSelect>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{TO_DATE}</label>
                <CPPersianCalendar
                  placeholder={TO_DATE}
                  // format="jYYYY/jMM/jDD"
                  onChange={(unix, formatted) =>
                    this.handelChangeInput('toDate', formatted)
                  }
                  preSelected={toDate}
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{TO_HOUR}</label>
                <CPSelect
                  disabled={disabled}
                  name="toHour"
                  defaultValue={toHour}
                  onChange={value => {
                    this.handelChangeInput('toHour', value);
                  }}
                >
                  {toHourArray}
                </CPSelect>
              </div>
            </div>
          </CPModal>
        </div>
      </CPPermission>
    );
  }
}

const mapStateToProps = state => ({
  vendorBranchOffShiftLoading: state.vendorBranchOffShiftList.loading,
  vendorBranchOffShifts: state.vendorBranchOffShiftList.data
    ? state.vendorBranchOffShiftList.data.items
    : [],
  vendorBranchOffShiftCount: state.vendorBranchOffShiftList.data
    ? state.vendorBranchOffShiftList.data.count
    : 0,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorBranchOffShiftDeleteRequest:
        vendorBranchOffShiftDeleteActions.vendorBranchOffShiftDeleteRequest,
      vendorBranchOffShiftPostRequest:
        vendorBranchOffShiftPostActions.vendorBranchOffShiftPostRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchOffShift));
