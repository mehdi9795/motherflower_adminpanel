import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Select } from 'antd';
import HotKeys from 'react-hot-keys';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import s from './VendorBranchShippingTariff.css';
import {
  YES,
  NO,
  PROMOTION_LIST,
  CATEGORY,
  AGENT_NAME,
  COUNT,
  VALUE,
  CITY,
  ZONE,
  TOMAN,
  ALL,
  DISTRICT,
  NAME,
  WITH_ZONE,
  ZONE_STATUS,
  WITHOUT_ZONE,
  ARROW_LEFT,
  ARROW_RIGHT,
  ZONES,
  GRADE,
} from '../../../../Resources/Localization';
import CPButton from '../../../../components/CP/CPButton';
import CardTable from '../../../../components/CP/CPList/CPList';
import CPSelect from '../../../../components/CP/CPSelect/CPSelect';
import Link from '../../../../components/Link/Link';
import CPPopConfirm from '../../../../components/CP/CPPopConfirm';
import {
  CityDto,
  DistrictDto,
  DistrictZoneDto,
  ZoneDto,
} from '../../../../dtos/commonDtos';
import zoneListActions from '../../../../redux/common/actionReducer/zone/List';
import { VendorBranchShippingTariffsDto } from '../../../../dtos/samDtos';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import { getDtoQueryString } from '../../../../utils/helper';
import vbShippingTariffListActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/List';
import vbShippingTariffPutActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/Put';
import vbShippingTariffDeleteActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/Delete';
import { getZoneApi } from '../../../../services/commonApi';
import vbShippingTariffPostActions from '../../../../redux/sam/actionReducer/vendorBranchShippingTariff/Post';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import CPModal from '../../../CP/CPModal';
import districtZoneListActions from '../../../../redux/common/actionReducer/district/DistrictZoneList';

const { Option } = Select;

class VendorBranchShippingTariff extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    shippingTariffLoading: PropTypes.bool,
    shippingTariffCount: PropTypes.number,
    cities: PropTypes.arrayOf(PropTypes.object),
    categories: PropTypes.arrayOf(PropTypes.object),
    zones: PropTypes.arrayOf(PropTypes.object),
    shippingTariffs: PropTypes.arrayOf(PropTypes.object),
    districtZones: PropTypes.arrayOf(PropTypes.object),
    districtZoneLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    data: null,
    shippingTariffs: [],
    categories: [],
    cities: [],
    zones: [],
    shippingTariffLoading: false,
    shippingTariffCount: 0,
    districtZones: null,
    districtZoneLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      categoryId: '0',
      cityId:
        props.cities && props.cities[0] ? props.cities[0].id.toString() : '0',
      vendorBranchId: props.data ? props.data.id : '0',
      zoneId: '0',
      visibleFullModal: false,
      pageSize: 10,
      currentPage: 1,
      selectedDistrictIdsContain: [],
    };
    this.handelChangeInput = this.handelChangeInput.bind(this);
    this.submitSearch = this.submitSearch.bind(this);
  }

  async componentDidMount() {
    const { cityId } = this.state;
    const { loginData } = this.props;
    const listzone = new BaseGetDtoBuilder()
      .dto(
        new ZoneDto({
          cityDto: new CityDto({
            id: cityId,
          }),
          active: true,
        }),
      )
      .buildJson();

    const response = await getZoneApi(
      loginData.token,
      getDtoQueryString(listzone),
    );

    if (response.status === 200) {
      this.props.actions.zoneListSuccess(response.data);
      this.submitSearch();
    } else this.props.actions.zoneListFailure();
  }

  async submitSearch() {
    const { zoneId, categoryId, vendorBranchId } = this.state;

    const listJson = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchShippingTariffsDto({
          zoneDto: zoneId === '0' ? undefined : new ZoneDto({ id: zoneId }),
          vendorBranchDto: new VendorBranchDto({ id: vendorBranchId }),
          categoryDto:
            categoryId === '0'
              ? undefined
              : new CategoryDto({ id: categoryId }),
        }),
      )
      .includes(['zoneDto', 'vendorBranchDto', 'categoryDto'])
      .buildJson();

    this.props.actions.vbShippingTariffListRequest(listJson);
  }

  /**
   * set value inputs into state
   * @param inputName
   * @param value
   */
  async handelChangeInput(inputName, value) {
    const { loginData } = this.props;

    this.setState({ [inputName]: value });
    if (inputName === 'cityId') {
      const cityList = new BaseGetDtoBuilder()
        .dto(
          new ZoneDto({
            cityDto: new CityDto({ id: value }),
            active: true,
          }),
        )
        .buildJson();

      const response = await getZoneApi(
        loginData.token,
        getDtoQueryString(cityList),
      );

      if (response.status === 200) {
        this.props.actions.zoneListSuccess(response.data);
        this.setState({
          zoneId: '0',
        });
      }
    }
  }

  async showFullModal(record) {
    this.setState({
      visibleFullModal: true,
      pageSize: 10,
      currentPage: 1,
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new DistrictZoneDto({
          zone: new ZoneDto({ id: record.zoneId }),
        }),
      )
      .buildJson();

    this.props.actions.districtZoneListRequest(jsonList);
  }

  handleCancelModal = () => {
    this.setState({
      visibleFullModal: false,
    });
  };

  render() {
    const {
      categories,
      cities,
      zones,
      shippingTariffLoading,
      shippingTariffCount,
      shippingTariffs,
      districtZones,
      districtZoneLoading,
    } = this.props;
    const {
      categoryId,
      zoneId,
      cityId,
      visibleFullModal,
      pageSize,
      currentPage,
    } = this.state;
    const columns = [
      {
        title: AGENT_NAME,
        dataIndex: 'vendorBranch',
        key: 'vendorBranch',
        width: 250,
      },
      { title: CATEGORY, dataIndex: 'category', key: 'category', width: 250 },
      { title: ZONE, dataIndex: 'zone', key: 'zone', width: 150 },
      {
        title: COUNT,
        dataIndex: 'count',
        key: 'count',
        width: 70,
      },
      {
        title: VALUE,
        dataIndex: 'stringAmount',
        key: 'stringAmount',
        width: 200,
      },
      ,
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.showFullModal(record)}
              className="edit_action"
            >
              {DISTRICT}
            </CPButton>
          </div>
        ),
      },
    ];
    const categoryArray = [];
    const cityArray = [];
    const zoneArray = [];
    const shippingTariffDataSource = [];
    const districtZoneArray = [];

    const columnsModal = [{ title: NAME, dataIndex: 'name', key: 'name' }];

    if (shippingTariffs)
      shippingTariffs.map(item => {
        const obj = {
          category: item.categoryDto.name,
          vendorBranch: item.vendorBranchDto.name,
          zone: item.zoneDto.name,
          count: item.qty,
          amount: item.shippingAmount,
          stringAmount: `${item.stringShippingAmount} ${TOMAN}`,
          key: item.id,
          categoryId: item.categoryDto.id.toString(),
          vendorBranchId: item.vendorBranchDto.id.toString(),
          zoneId: item.zoneDto.id.toString(),
          cityId: item.zoneDto.cityDto.id.toString(),
        };
        shippingTariffDataSource.push(obj);
        return null;
      });

    /**
     * map category for comboBox Datasource
     */
    categories.map(item =>
      categoryArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map city for comboBox Datasource
     */
    cities.map(item =>
      cityArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map zone for comboBox Datasource
     */
    zones.map(item =>
      zoneArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    if (districtZones)
      districtZones.map(item => {
        const obj = {
          key: `${item.id}*${item.district.id}`,
          name: item.district.name,
          lat: item.district.lat,
          lng: item.district.lng,
          districtGrade: item.district.grade,
        };
        districtZoneArray.push(obj);
        return null;
      });

    return (
      <HotKeys keyName="shift+n" onKeyDown={this.redirectToCreatePage}>
        <div className={s.mainContent}>
          <h3>{PROMOTION_LIST}</h3>
          <CardTable
            data={shippingTariffDataSource}
            count={shippingTariffCount}
            columns={columns}
            loading={shippingTariffLoading}
            hideAdd
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{CATEGORY}</label>
                    <CPSelect
                      value={categoryId}
                      onChange={value => {
                        this.handelChangeInput('categoryId', value);
                      }}
                    >
                      <Option key={0}>{ALL}</Option>
                      {categoryArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{CITY}</label>
                    <CPSelect
                      value={cityId}
                      onChange={value => {
                        this.handelChangeInput('cityId', value);
                      }}
                    >
                      {cityArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{ZONE}</label>
                    <CPSelect
                      value={zoneId}
                      onChange={value => {
                        this.handelChangeInput('zoneId', value);
                      }}
                    >
                      <Option key={0}>{ALL}</Option>
                      {zoneArray}
                    </CPSelect>
                  </div>
                </div>
              </div>
              <div className="field">
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CardTable>
          <CPModal
            visible={visibleFullModal}
            handleCancel={this.handleCancelModal}
            title={DISTRICT}
            className="max_modal"
            showFooter={false}
          >
            <div className="modalContent">
              <div className="row">
                <div className="col-sm-12">
                  <CardTable
                    data={districtZoneArray}
                    columns={columnsModal}
                    loading={districtZoneLoading}
                    hideAdd
                    hideSearchBox
                  />
                </div>
              </div>
            </div>
          </CPModal>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.secondaryLevelList.data.items,
  cities: state.cityList.data.items,
  zones: state.zoneList.data.items,
  shippingTariffs: state.vbShippingTariffList.data.items,
  shippingTariffCount: state.vbShippingTariffList.data.count,
  shippingTariffLoading: state.vbShippingTariffList.loading,
  loginData: state.login.data,
  districtZones: state.districtZoneList.data
    ? state.districtZoneList.data.items
    : [],
  districtZoneLoading: state.districtZoneList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vbShippingTariffListRequest:
        vbShippingTariffListActions.vbShippingTariffListRequest,
      zoneListSuccess: zoneListActions.zoneListSuccess,
      zoneListFailure: zoneListActions.zoneListFailure,
      districtZoneListRequest: districtZoneListActions.districtZoneListRequest,
      hideLoading,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchShippingTariff));
