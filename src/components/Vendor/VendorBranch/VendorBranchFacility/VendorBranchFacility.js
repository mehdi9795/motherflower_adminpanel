import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorBranchFacility.css';
import { SAVE } from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import CPSwitch from '../../../CP/CPSwitch';
import { VendorFcilityDto } from '../../../../dtos/vendorDtos';
import vendorFacilityPostActions from '../../../../redux/vendor/actionReducer/facility/VendorFacilityPost';
import CPPermission from '../../../CP/CPPermission/CPPermission';

class VendorBranchFacility extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    facilities: PropTypes.arrayOf(PropTypes.object).isRequired,
    vendorFacilities: PropTypes.arrayOf(PropTypes.object).isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    data: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      selectedFacilitiesId: [],
    };
    this.insertVendorFacility = this.insertVendorFacility.bind(this);
    this.facilityArray = [];
  }

  /**
   * change selectedFacilitiesId state when facility change to true/false
   */
  handleFacilityChange = value => {
    const temp = this.state.selectedFacilitiesId;
    const indexof = temp.indexOf(value);
    if (indexof >= 0) {
      temp.splice(indexof, 1);
    } else {
      temp.push(value);
    }
  };

  async insertVendorFacility() {
    const items = [];
    const selectedIds = this.state.selectedFacilitiesId;

    selectedIds.map(item =>
      items.push(
        new VendorFcilityDto({
          vendorBranchId: this.props.data.id,
          facilityId: item,
        }),
      ),
    );

    const container = { items };

    this.props.actions.vendorFacilityPostRequest(container);
  }

  render() {
    const { facilities, vendorFacilities } = this.props;
    const facilityArray = [];

    if (facilities)
      facilities.map(item => {
        if (vendorFacilities.find(x => x.facilityId === item.id)) {
          this.state.selectedFacilitiesId.push(item.id);
          facilityArray.push(
            <div className={s.switch} key={item.id}>
              <CPSwitch
                defaultChecked
                checkedChildren={item.name}
                unCheckedChildren={item.name}
                onChange={() => this.handleFacilityChange(item.id)}
              />
            </div>,
          );
        } else {
          facilityArray.push(
            <div className={s.switch} key={item.id}>
              <CPSwitch
                defaultChecked={false}
                checkedChildren={item.name}
                unCheckedChildren={item.name}
                onChange={() => this.handleFacilityChange(item.id)}
              />
            </div>,
          );
        }
        return facilityArray;
      });

    return (
      <CPPermission>
        <div name="vendorBranch-management-facility">
          {facilityArray}
          <div className="textLeft">
            <CPButton
              icon="save"
              type="primary"
              onClick={this.insertVendorFacility}
            >
              {SAVE}
            </CPButton>
          </div>
        </div>
      </CPPermission>
    );
  }
}

const mapStateToProps = state => ({
  facilities: state.facilityList.data.items,
  vendorFacilities: state.vendorFacilityList.data.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorFacilityPostRequest:
        vendorFacilityPostActions.vendorFacilityPostRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchFacility));
