import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  CATEGORY_NAME,
  COMMISSION_VALUE,
  DAY,
  HOUR,
  MINUTE,
  PRODUCT_PREPARATION_TIME_MINUTE,
} from '../../../../Resources/Localization';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPList from '../.././../CP/CPList/CPList';
import CPModal from '../../../CP/CPModal';
import productTimePutActions from '../../../../redux/catalog/actionReducer/productTime/Put';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import { PreparingProductTimeDto } from '../../../../dtos/catalogDtos';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

class VendorBranchProductTime extends React.Component {
  static propTypes = {
    productTimes: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    productTimeLoading: PropTypes.bool,
    productTimeCount: PropTypes.number,
  };
  static defaultProps = {
    data: null,
    productTimeLoading: false,
    productTimeCount: 0,
    productTimes: [],
  };
  constructor(props) {
    super(props);
    this.state = {
      visibleEditModal: false,
      takesTimeDay: '0',
      takesTimeHour: '0',
      takesTimeMinute: '1 ',
      id: 0,
    };
    this.putData = this.putData.bind(this);
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  showEditModal = record => {
    let time = '0';
    let status = '0';
    if (record.day !== '0') time = record.day;
    if (record.hours !== '0') {
      time = record.hours;
      status = '1';
    }
    if (record.minute !== '0') {
      time = record.minute;
      status = '2';
    }

    this.setState({
      timeValue: time,
      status,
      id: record.key,
      visibleEditModal: true,
    });
  };

  handleOkModal = () => {
    this.setState({
      visibleEditModal: false,
    });
    this.putData();
  };

  handleCancelModal = () => {
    this.setState({
      visibleEditModal: false,
    });
  };

  async putData() {
    const { data } = this.props;
    const { timeValue, id } = this.state;
    // let finalTime = '';
    // if (status === '0') finalTime = `${timeValue}:0:0`;
    // if (status === '1') finalTime = `0:${timeValue}:0`;
    // if (status === '2') finalTime = `0:0:${timeValue}`;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new PreparingProductTimeDto({
          id,
          takesTime: `0:0:${timeValue}`,
        }),
      )
      .build();

    /**
     * get product time for reload list after editing
     * @type {string}
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new PreparingProductTimeDto({
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
        }),
      )
      .buildJson();

    const putData = {
      data: jsonCrud,
      listDto: jsonList,
    };

    this.props.actions.productTimePutRequest(putData);
  }

  render() {
    const { productTimes, productTimeCount, productTimeLoading } = this.props;
    const { timeValue, status } = this.state;
    const productTimeArray = [];
    const takesTimeDayArray = [];
    const takesTimeHourArray = [];
    const takesTimeMinuteArray = [];

    /**
     * map commission for data table commission
     */

    if (productTimes) {
      productTimes.map(item => {
        const day = item.takesTime.split(':')[0];
        const hours = item.takesTime.split(':')[1];
        const minute = item.takesTime.split(':')[2];
        let time = '';
        if (day !== '0') {
          time = `${day} ${DAY}`;
        } else if (hours !== '0') time = `${hours} ${HOUR}`;
        else if (minute !== '0') time = `${minute} ${MINUTE}`;

        const obj = {
          key: item.id,
          categoryName: item.categoryDto.name,
          time,
          day,
          hours,
          minute,
        };
        return productTimeArray.push(obj);
      });
    }

    for (let i = 15; i <= 90; i += 15) {
      takesTimeMinuteArray.push(<option key={`${i}`}>{i}</option>);
    }
    takesTimeMinuteArray.push(<option key={`${120}`}>{120}</option>);
    takesTimeMinuteArray.push(<option key={`${180}`}>{180}</option>);
    takesTimeMinuteArray.push(<option key={`${240}`}>{240}</option>);

    const columns = [
      { title: CATEGORY_NAME, dataIndex: 'categoryName', key: 'categoryName' },
      {
        title: COMMISSION_VALUE,
        dataIndex: 'time',
        key: 'time',
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <span
            onClick={() => this.showEditModal(record)}
            className="edit_action"
          >
            <i className="cp-edit" />
          </span>
        ),
      },
    ];

    return (
      <CPPermission>
        <div name="vendorBranch-management-productTime">
          <CPList
            data={productTimeArray}
            columns={columns}
            count={productTimeCount}
            loading={productTimeLoading}
            onAddClick={this.vendorBranchAddClick}
            hideSearchBox
            hideAdd
          />
          <CPModal
            visible={this.state.visibleEditModal}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
          >
            <div className="modalContent">
              <div className="row">
                <div className="col-sm-6">
                  <label>{PRODUCT_PREPARATION_TIME_MINUTE}</label>
                  <CPSelect
                    name="timeValue"
                    value={timeValue}
                    onChange={value => {
                      this.handelChangeInput('timeValue', value);
                    }}
                  >
                    {takesTimeMinuteArray}
                  </CPSelect>
                </div>
              </div>
            </div>
          </CPModal>
        </div>
      </CPPermission>
    );
  }
}

const mapStateToProps = state => ({
  productTimes: state.productTimeList.data.items,
  productTimeCount: state.productTimeList.data.count,
  productTimeLoading: state.productTimeList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      productTimePutRequest: productTimePutActions.productTimePutRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VendorBranchProductTime);
