import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import commissionPostActions from '../../../../redux/sam/actionReducer/commission/Post';
import commissionPutActions from '../../../../redux/sam/actionReducer/commission/Put';
import commissionDeleteActions from '../../../../redux/sam/actionReducer/commission/Delete';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_COMMISSION,
  COMMISSION_TYPE,
  COMMISSION_VALUE,
  DISCOUNT_TYPE,
  DISCOUNT_VALUE,
  FROM_PRICE,
  NO,
  PERCENT,
  TO_PRICE,
  TOMAN,
  YES,
} from '../../../../Resources/Localization';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPList from '../.././../CP/CPList/CPList';
import CPModal from '../../../CP/CPModal';
import CPButton from '../../../CP/CPButton';
import CPInputNumber from '../../../CP/CPInputNumber/CPInputNumber';
import { VendorBranchCommissionDto } from '../../../../dtos/samDtos';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class VendorBranchCommission extends React.Component {
  static propTypes = {
    commissions: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    commissionLoading: PropTypes.bool,
    commissionCount: PropTypes.number,
    data: PropTypes.objectOf(PropTypes.any),
  };
  static defaultProps = {
    data: null,
    commissionLoading: false,
    commissionCount: 0,
    commissions: [],
  };
  constructor(props) {
    super(props);
    this.state = {
      visibleCommissionModal: false,
      commissionType: '0',
      commissionValue: '0',
      discountType: '0',
      discountValue: '0',
      commissionId: 0,
      commissions: [],
      fromPrice: '',
      toPrice: '',
    };
  }

  onDelete = id => {
    const { data } = this.props;
    /**
     * get commission for first time with vendorBranchId
     * @type {string}
     */
    const commissionData = {
      id,
      listDto: new BaseGetDtoBuilder()
        .dto(
          new VendorBranchCommissionDto({
            vendorBranchId: data.id,
          }),
        )
        .buildJson(),
    };

    this.props.actions.commissionDeleteRequest(commissionData);
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  showEditModalCommission = value => {
    const commission = value;
    this.setState({
      commissionType: commission.commissionType,
      commissionValue: commission.commissionValue,
      fromPrice: commission.fromPrice,
      toPrice: commission.toPrice,
      discountType: commission.discountType,
      discountValue: commission.discountValue,
      commissionId: commission.key,
    });

    this.setState({
      visibleCommissionModal: true,
    });
  };

  showPostModalCommission = () => {
    this.setState({
      commissionType: 'Percent',
      commissionValue: 0,
      fromPrice: 0,
      toPrice: 0,
      discountType: 'Percent',
      discountValue: 0,
      commissionId: 0,
    });

    this.setState({
      visibleCommissionModal: true,
    });
  };

  handleOkModal = () => {
    this.setState({
      visibleCommissionModal: false,
    });
    this.putOrPostDataCommission();
  };

  handleCancelModal = () => {
    this.setState({
      visibleCommissionModal: false,
    });
  };

  putOrPostDataCommission() {
    const { data } = this.props;
    const {
      commissionType,
      commissionValue,
      discountType,
      discountValue,
      commissionId,
      fromPrice,
      toPrice,
    } = this.state;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new VendorBranchCommissionDto({
          vendorBranchId: data.id,
          id: commissionId,
          valueType: commissionType,
          value: commissionValue,
          discountType,
          discount: discountValue,
          fromPrice,
          toPrice,
        }),
      )
      .build();

    /**
     * get commission for first time with vendorBranchId
     * @type {string}
     */

    const commissionData = {
      data: jsonCrud,
      listDto: new BaseGetDtoBuilder()
        .dto(
          new VendorBranchCommissionDto({
            vendorBranchId: data.id,
          }),
        )
        .buildJson(),
    };

    if (commissionId === 0) {
      this.props.actions.commissionPostRequest(commissionData);
    } else {
      this.props.actions.commissionPutRequest(commissionData);
    }
  }

  render() {
    const { commissionLoading, commissionCount, commissions } = this.props;
    const {
      commissionType,
      commissionValue,
      discountType,
      fromPrice,
      toPrice,
      discountValue,
    } = this.state;
    const commissionArray = [];

    /**
     * map commission for data table commission
     */

    if (commissions) {
      commissions.map(item => {
        const obj = {
          key: item.id,
          commissionValue: item.value,
          commissionValueType: `${item.stringValue} ${
            item.valueType === 'Percent' ? 'درصد' : 'تومان'
          }`,
          commissionType: item.valueType,
          discountValueType: `${item.stringDiscount} ${
            item.discountType === 'Percent' ? 'درصد' : 'تومان'
          }`,
          discountValue: item.discount,
          discountType: item.discountType,
          stringFromPrice: item.stringFromPrice,
          stringToPrice: item.stringToPrice,
          fromPrice: item.fromPrice,
          toPrice: item.toPrice,
        };
        return commissionArray.push(obj);
      });
    }
    const columnsCommission = [
      {
        title: FROM_PRICE,
        dataIndex: 'stringFromPrice',
        key: 'stringFromPrice',
      },
      {
        title: TO_PRICE,
        dataIndex: 'stringToPrice',
        key: 'stringToPrice',
      },
      {
        title: COMMISSION_VALUE,
        dataIndex: 'commissionValueType',
        key: 'commissionValueType',
      },
      {
        title: DISCOUNT_VALUE,
        dataIndex: 'discountValueType',
        key: 'discountValueType',
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_COMMISSION}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <CPButton
            onClick={() => this.showEditModalCommission(record)}
            className="edit_action"
          >
            <i className="cp-edit" />
          </CPButton>
        ),
      },
    ];

    return (
      <CPPermission>
        <div name="vendorBranch-management-commission">
          <CPList
            data={commissionArray}
            columns={columnsCommission}
            count={commissionCount}
            loading={commissionLoading}
            onAddClick={this.showPostModalCommission}
            hideSearchBox
          />
          <CPModal
            visible={this.state.visibleCommissionModal}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
          >
            <div className="modalContent">
              <div className="row">
                <div className="col-lg-4 col-sm-12">
                  <label>{FROM_PRICE}</label>
                  <CPInputNumber
                    name="fromPrice"
                    label={FROM_PRICE}
                    value={fromPrice}
                    onChange={value => {
                      this.handelChangeInput('fromPrice', value);
                    }}
                    format="Currency"
                  />
                </div>
                <div className="col-lg-4 col-sm-12">
                  <label>{TO_PRICE}</label>
                  <CPInputNumber
                    name="toPrice"
                    label={TO_PRICE}
                    value={toPrice}
                    onChange={value => {
                      this.handelChangeInput('toPrice', value);
                    }}
                    format="Currency"
                  />
                </div>

                <div className="col-lg-9 col-sm-12">
                  <label>{COMMISSION_TYPE}</label>
                  <CPSelect
                    name="commissionType"
                    value={commissionType}
                    onChange={value => {
                      this.handelChangeInput('commissionType', value);
                    }}
                  >
                    <Option key="Percent">{PERCENT}</Option>
                    <Option key="Amount">{TOMAN}</Option>
                  </CPSelect>
                </div>
                <div className="col-lg-3 col-sm-12">
                  <label>{COMMISSION_VALUE}</label>
                  <CPInputNumber
                    name="commissionValue"
                    label={COMMISSION_VALUE}
                    value={commissionValue}
                    onChange={value => {
                      this.handelChangeInput('commissionValue', value);
                    }}
                    format={
                      commissionType === 'Percent' ? 'Percent' : 'Currency'
                    }
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-9 col-sm-12">
                  <label>{DISCOUNT_TYPE}</label>
                  <CPSelect
                    name="discountType"
                    value={discountType}
                    onChange={value => {
                      this.handelChangeInput('discountType', value);
                    }}
                  >
                    <Option key="Percent">{PERCENT}</Option>
                    <Option key="Amount">{TOMAN}</Option>
                  </CPSelect>
                </div>
                <div className="col-lg-3 col-sm-12">
                  <label>{DISCOUNT_VALUE}</label>
                  <CPInputNumber
                    name="discountValue"
                    label={DISCOUNT_VALUE}
                    value={discountValue}
                    format={discountType === 'Percent' ? 'Percent' : 'Currency'}
                    onChange={value => {
                      this.handelChangeInput('discountValue', value);
                    }}
                  />
                </div>
              </div>
            </div>
          </CPModal>
        </div>
      </CPPermission>
    );
  }
}

const mapStateToProps = state => ({
  commissions: state.commissionList.data.items,
  commissionCount: state.commissionList.data.count,
  commissionLoading: state.commissionList.loading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      commissionPutRequest: commissionPutActions.commissionPutRequest,
      commissionPostRequest: commissionPostActions.commissionPostRequest,
      commissionDeleteRequest: commissionDeleteActions.commissionDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VendorBranchCommission);
