import React from 'react';
import { hideLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import s from './VendorBranchForm.css';
import commissionListActions from '../../../../redux/sam/actionReducer/commission/List';
import facilityListActions from '../../../../redux/vendor/actionReducer/facility/FacilityList';
import vendorFacilityListActions from '../../../../redux/vendor/actionReducer/facility/VendorFacilityList';
import vendorBranchOffShiftListActions from '../../../../redux/vendor/actionReducer/vendorBranchOffShift/List';
import vendorBranchUserListActions from '../../../../redux/vendor/actionReducer/vendorBranchUser/List';
import {
  VENDOR_BRANCH_FACILITY,
  VENDOR_BRANCH_INFO,
  VENDOR_BRANCH_COMMISSION,
  VENDOR_BRANCH_OFF_SHIFT,
  VENDOR_BRANCH_USERS,
  VENDOR_BRANCH_PREPARING_PRODUCT_TIME, VENDOR_BRANCH_SHIPPING_TARIFF,
} from '../../../../Resources/Localization';
import VendorBranchInfo from '../VendorBranchInfo/VendorBranchInfo';
import VendorBranchFacility from '../VendorBranchFacility';
import VendorBranchCommission from '../VendorBranchCommission';
import VendorBranchOffShift from '../VendorBranchOffShift/VendorBranchOffShift';
import VendorBranchUser from '../VendorBranchUser/VendorBranchUser';
import {
  VendorBranchDto,
  VendorBranchFacilitiesDto,
  VendorBranchUserDto,
  VendorBranchOffShiftsDto,
} from '../../../../dtos/vendorDtos';
import { VendorBranchCommissionDto } from '../../../../dtos/samDtos';
import VendorBranchProductTime from '../VendorBranchProductTime';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import VendorBranchShippingTariff from "../VendorBranchShippingTariff";

class VendorBranchForm extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    tabSelected: PropTypes.string,
    vendorId: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    data: null,
    vendorId: '',
    tabSelected: '',
  };
  constructor(props) {
    super(props);
    this.state = {
      tabSelected: props.tabSelected ? props.tabSelected - 1 : 0,
    };
    this.dropdownOffShift = [];
  }

  async componentWillMount() {
    const { data } = this.props;
    const isEdit = data && data.id;

    if (isEdit) {
      /**
       * get all facility for first time
       * @type {string}
       */
      this.props.actions.facilityListRequest(
        new BaseGetDtoBuilder().all(true).buildJson(),
      );

      /**
       * get vendor facility for first time with vendorBranchId
       * @type {string}
       */
      this.props.actions.vendorFacilityListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchFacilitiesDto({
              vendorBranchId: data.id,
            }),
          )
          .buildJson(),
      );

      /**
       * get commission for first time with vendorBranchId
       * @type {string}
       */
      this.props.actions.commissionListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchCommissionDto({
              vendorBranchId: data.id,
            }),
          )
          .buildJson(),
      );

      /**
       * get Vendor branch off shift for first time with vendorBranchId and offStatus
       * @type {string}
       */
      this.props.actions.vendorBranchOffShiftListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchOffShiftsDto({
              vendorBranchDto: new VendorBranchDto({ id: data.id }),
            }),
          )
          .buildJson(),
      );

      /**
       * get Vendor branch user for first time with vendorBranchId
       * @type {string}
       */
      this.props.actions.vendorBranchUserListRequest(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchUserDto({
              vendorBranchDto: new VendorBranchDto({ id: data.id }),
            }),
          )
          .includes(['Users', 'Roles'])
          .buildJson(),
      );
    }
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  render() {
    const { data, vendorId } = this.props;
    const { tabSelected } = this.state;
    const isEdit = data && data.id;

    return (
      <Tabs defaultIndex={tabSelected}>
        <TabList>
          <Tab>{VENDOR_BRANCH_INFO}</Tab>
          {isEdit && <Tab>{VENDOR_BRANCH_FACILITY}</Tab>}
          {isEdit && <Tab>{VENDOR_BRANCH_COMMISSION}</Tab>}
          {isEdit && <Tab>{VENDOR_BRANCH_USERS}</Tab>}
          {isEdit && <Tab>{VENDOR_BRANCH_OFF_SHIFT}</Tab>}
          {isEdit && <Tab>{VENDOR_BRANCH_PREPARING_PRODUCT_TIME}</Tab>}
          {isEdit && <Tab>{VENDOR_BRANCH_SHIPPING_TARIFF}</Tab>}
        </TabList>
        <TabPanel>
          <VendorBranchInfo vendorId={vendorId} />
        </TabPanel>
        {isEdit && (
          <TabPanel>
            <VendorBranchFacility data={data} />
          </TabPanel>
        )}
        {isEdit && (
          <TabPanel>
            <VendorBranchCommission data={data} />
          </TabPanel>
        )}
        {isEdit && (
          <TabPanel>
            <VendorBranchUser data={data} />
          </TabPanel>
        )}
        {isEdit && (
          <TabPanel>
            <VendorBranchOffShift data={data} />
          </TabPanel>
        )}
        {isEdit && (
          <TabPanel>
            <VendorBranchProductTime data={data} />
          </TabPanel>
        )}
        {isEdit && (
          <TabPanel>
            <VendorBranchShippingTariff data={data} />
          </TabPanel>
        )}
      </Tabs>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      commissionListRequest: commissionListActions.commissionListRequest,
      vendorBranchOffShiftListRequest:
        vendorBranchOffShiftListActions.vendorBranchOffShiftListRequest,
      facilityListRequest: facilityListActions.facilityListRequest,
      vendorFacilityListRequest:
        vendorFacilityListActions.vendorFacilityListRequest,
      vendorBranchUserListRequest:
        vendorBranchUserListActions.vendorBranchUserListRequest,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchForm));
