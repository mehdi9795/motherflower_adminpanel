import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorBranchUser.css';
import {
  STATUS,
  EMAIL,
  PHONE,
  USER_FULL_NAME,
  USER_USER_ROLE,
  CHANGE_USER_ROLE,
  SELECT_USER,
  EDIT_USER_ROLE,
  NO,
  YES,
  SELECT_OF_EXIST_USER,
  USER_CREATE,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR_BRANCH,
} from '../../../../Resources/Localization';
import CPList from '../../../CP/CPList/CPList';
import CPSwitch from '../../../CP/CPSwitch';
import CPModal from '../../../CP/CPModal';
import CPButton from '../../../CP/CPButton';
import CPMultiSelect from '../../../CP/CPMultiSelect/CPMultiSelect';
import vendorBranchUserListActions from '../../../../redux/vendor/actionReducer/vendorBranchUser/List';
import vendorBranchUserPostActions from '../../../../redux/vendor/actionReducer/vendorBranchUser/Post';
import vendorBranchUserPutActions from '../../../../redux/vendor/actionReducer/vendorBranchUser/Put';
import vendorBranchUserDeleteActions from '../../../../redux/vendor/actionReducer/vendorBranchUser/Delete';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import Link from '../../../Link/Link';
import history from '../../../../history';
import {
  AgentDto,
  VendorBranchDto,
  VendorBranchUserDto,
} from '../../../../dtos/vendorDtos';
import { UserDto } from '../../../../dtos/identityDtos';
import vendorUserListActons from '../../../../redux/vendor/actionReducer/vendor/VendorUserList';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import roleActions from '../../../../redux/identity/actionReducer/role/Role';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class VendorBranchUser extends React.Component {
  static propTypes = {
    vendorBranchUserLoading: PropTypes.bool,
    vendorBranchUsers: PropTypes.arrayOf(PropTypes.object),
    roles: PropTypes.arrayOf(PropTypes.object),
    data: PropTypes.objectOf(PropTypes.any),
    vendorBranchUserCount: PropTypes.number,
    popupUserLoading: PropTypes.bool,
    popupUsers: PropTypes.arrayOf(PropTypes.object),
    popupUserCount: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    data: null,
    roles: null,
    vendorBranchUserLoading: false,
    vendorBranchUsers: null,
    vendorBranchUserCount: 0,
    popupUserLoading: false,
    popupUsers: null,
    popupUserCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      visibleQuestionModal: false,
      visibleUserFullModal: false,
      visibleEditModal: false,
      selectedRoleIds: [],
      name: '',
      phone: '',
      email: '',
      isShowDiv: false,
      userId: 0,
      vendorBranchUserId: 0,
    };
    this.insertUserRole = this.insertUserRole.bind(this);
    this.defaultSelectedRoleIds = [];
    this.updateUserRole = this.updateUserRole.bind(this);
    this.reloadVendorBranchUser = this.reloadVendorBranchUser.bind(this);
    this.createNewUser = this.createNewUser.bind(this);
    this.closeQuestionModal = this.closeQuestionModal.bind(this);
  }

  async componentWillMount() {
    /**
     * get all role
     */
    this.props.actions.roleListRequest(
      new BaseGetDtoBuilder()
        .all(true)
        .disabledCount(true)
        .buildJson(),
    );
  }

  onChangeRoleSelect = value => {
    this.setState({
      selectedRoleIds: value,
    });
  };

  async onDelete(id) {
    const { data } = this.props;

    /**
     * get Vendor branch user for first time with vendorBranchId
     * @type {string}
     */

    const jsonListVBUser = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchUserDto({
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
        }),
      )
      .includes(['Users', 'Roles'])
      .buildJson();

    /**
     * get Vendor user for popup Add user list
     * @type {string}
     */

    const jsonListVBUser2 = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchUserDto({
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
          vendorDto: new AgentDto({ id: data.vendorId }),
        }),
      )
      .includes(['Roles'])
      .buildJson();

    const deleteData = {
      id,
      vendorBranchUserdto: jsonListVBUser,
      vendorUserDto: jsonListVBUser2,
    };
    this.props.actions.vendorBranchUserDeleteRequest(deleteData);
  }

  /**
   * call post data for sending data - insert user role for vendor branch
   * @param containert
   * @returns {Promise<void>}
   */
  async insertUserRole() {
    const { selectedRoleIds, userId } = this.state;
    const { data } = this.props;
    const roleDtoIds = [];

    /**
     * get Vendor branch user for first time with vendorBranchId
     * @type {string}
     */

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchUserDto({
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
        }),
      )
      .includes('Users', 'Roles')
      .buildJson();

    /**
     * get Vendor user for popup Add user list
     * @type {string}
     */

    const jsonList2 = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchUserDto({
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
          vendorDto: new AgentDto({ id: data.vendorId }),
        }),
      )
      .includes(['Roles'])
      .buildJson();

    /**
     * fill dto vendor branch user for post data
     */
    selectedRoleIds.map(item => roleDtoIds.push({ id: parseInt(item, 0) }));

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new VendorBranchUserDto({
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
          userDto: new UserDto({ id: userId }),
          roleDtos: roleDtoIds,
        }),
      )
      .build();

    const postData = {
      data: jsonCrud,
      status: 'fromVendor',
      vendorBranchUserDto: jsonList,
      vendorUserDto: jsonList2,
    };
    this.props.actions.vendorBranchUserPostRequest(postData);

    this.setState({ visibleUserFullModal: false });
  }

  async updateUserRole() {
    const { data } = this.props;

    const { vendorBranchUserId, selectedRoleIds } = this.state;
    const roleDtoIds = [];

    /**
     * get Vendor branch user for first time with vendorBranchId
     * @type {string}
     */

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchUserDto({
          vendorBranchDto: new VendorBranchDto({ id: data.id }),
        }),
      )
      .includes(['Users', 'Roles'])
      .buildJson();

    /**
     * fill dto vendor branch user for post data
     */
    selectedRoleIds.map(item => roleDtoIds.push({ id: parseInt(item, 0) }));

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new VendorBranchUserDto({
          id: vendorBranchUserId,
          roleDtos: roleDtoIds,
        }),
      )
      .build();

    const putData = {
      data: jsonCrud,
      vendorBranchUserdto: jsonList,
    };
    this.props.actions.vendorBranchUserPutRequest(putData);
  }

  async reloadVendorBranchUser() {
    const { data } = this.props;

    /**
     * get Vendor branch user for first time with vendorBranchId
     * @type {string}
     */
    this.props.actions.vendorBranchUserListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new VendorBranchUserDto({
            vendorBranchDto: new VendorBranchDto({ id: data.id }),
          }),
        )
        .includes(['Users', 'Roles'])
        .buildJson(),
    );
  }

  /**
   * handling modals
   */
  // <editor-fold dsc="handling modals">
  showQuestionModal = () => {
    this.setState({
      visibleQuestionModal: true,
    });
  };

  showFullModal = () => {
    const { data } = this.props;
    this.setState({
      visibleQuestionModal: false,
      visibleUserFullModal: true,
      selectedRoleIds: this.defaultSelectedRoleIds,
      name: '',
      email: '',
      phone: '',
      isShowDiv: false,
      userId: 0,
    });
    /**
     * get Vendor user for popup Add user list
     * @type {string}
     */
    this.props.actions.vendorUserListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new VendorBranchUserDto({
            vendorBranchDto: new VendorBranchDto({ id: data.id }),
            vendorDto: new AgentDto({ id: data.vendorId }),
          }),
        )
        .includes(['Roles'])
        .buildJson(),
    );
  };

  async createNewUser() {
    this.setState({
      visibleQuestionModal: false,
    });

    const { data } = this.props;
    history.push(
      `/user/create?vendorBranchId=${data.id}&vendorId=${data.vendorId}`,
    );
  }

  closeQuestionModal() {
    this.setState({
      visibleQuestionModal: false,
    });
  }

  handleCancelUserFullModal = () => {
    this.setState({
      visibleUserFullModal: false,
      isShowDiv: false,
    });
  };

  handleOkEditModal = () => {
    this.updateUserRole();
    this.setState({
      visibleEditModal: false,
    });
  };

  handleCancelEditModal = () => {
    this.setState({
      visibleEditModal: false,
    });
  };

  showEditModal(record) {
    this.setState({
      visibleEditModal: true,
      selectedRoleIds: record.userRoleIds,
      vendorBranchUserId: record.key,
    });
  }

  // </editor-fold>

  /**
   * when click user select show div in popup
   * @param item
   */
  userSelect(item) {
    this.setState({
      // selectedRoleIds: this.roleselected,
      name: item.fullName,
      email: item.email,
      phone: item.phone,
      isShowDiv: true,
      userId: item.key,
    });
  }

  render() {
    const {
      vendorBranchUserLoading,
      vendorBranchUsers,
      vendorBranchUserCount,
      popupUserLoading,
      popupUsers,
      popupUserCount,
      roles,
    } = this.props;
    const { selectedRoleIds, name, phone, email, isShowDiv } = this.state;
    const vendorBranchUsersArray = [];
    const usersArray = [];
    const RoleArray = [];
    const columnsVendorBranchUser = [
      {
        title: EMAIL,
        dataIndex: 'email',
        key: 'email',
        width: 250,
      },
      {
        title: PHONE,
        dataIndex: 'phone',
        key: 'phone',
        width: 150,
      },
      {
        title: USER_FULL_NAME,
        dataIndex: 'fullName',
        key: 'fullName',
        width: 200,
      },
      {
        title: USER_USER_ROLE,
        dataIndex: 'userRole',
        key: 'userRole',
        width: 150,
      },
      {
        title: STATUS,
        dataIndex: 'active',
        width: 50,
        render: (text, record) => (
          <CPSwitch disabled size="small" defaultChecked={record.active} />
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <CPPopConfirm
            title={ARE_YOU_SURE_WANT_TO_DELETE_THE_VENDOR_BRANCH}
            okText={YES}
            cancelText={NO}
            okType="primary"
            onConfirm={() => this.onDelete(record.key)}
          >
            <Link to="#" className="delete_action">
              <i className="cp-trash" />
            </Link>
          </CPPopConfirm>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <CPButton
            onClick={() => this.showEditModal(record)}
            className="edit_action"
          >
            <i className="cp-edit" />
          </CPButton>
        ),
      },
    ];
    const columnsUsersModal = [
      { title: USER_FULL_NAME, dataIndex: 'fullName', key: 'firstName' },
      { title: EMAIL, dataIndex: 'email', key: 'email' },
      { title: PHONE, dataIndex: 'phone', key: 'phone' },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <CPSwitch disabled size="small" defaultChecked={record.active} />
        ),
      },
      {
        title: '',
        dataIndex: 'select',
        render: (text, record) => (
          <CPButton onClick={() => this.userSelect(record)}>
            {SELECT_USER}
          </CPButton>
        ),
      },
    ];

    /**
     * map vendor branch user for list
     */
    vendorBranchUsers.map(item => {
      const roleNames = [];
      const roleUserIds = [];
      item.roleDtos.map(role => {
        if (role.roleStatus === 'Driver' || role.roleStatus === 'Seller') {
          roleNames.push(role.name);
          roleUserIds.push(role.id.toString());
        }
        if (role.roleStatus === 'VendorAdmin') roleNames.push(role.name);
        return null;
      });

      const obj = {
        key: item.id,
        email: item.userDto.email,
        phone: item.userDto.phone,
        fullName: `${item.userDto.firstName} ${item.userDto.lastName}`,
        active: item.userDto.active,
        userRole: roleNames.join(),
        userRoleIds: roleUserIds,
      };

      return vendorBranchUsersArray.push(obj);
    });

    /**
     * map popup user for popup modal full user list
     */
    if (popupUsers)
      popupUsers.map(item => {
        const obj = {
          key: item.id,
          fullName: `${item.firstName}  ${item.lastName}`,
          email: item.email,
          phone: item.phone,
          active: item.active,
        };
        return usersArray.push(obj);
      });

    /**
     * maping role for multi select popup
     * @type {Array}
     */
    this.defaultSelectedRoleIds = [];
    if (roles)
      roles.map(item => {
        if (item.roleStatus === 'Driver' || item.roleStatus === 'Seller') {
          RoleArray.push(<Option key={item.id}>{item.name}</Option>);
          this.defaultSelectedRoleIds.push(item.id.toString());
        }
        return null;
      });

    return (
      <CPPermission>
        <div name="vendorBranch-management-user">
          <CPList
            data={vendorBranchUsersArray}
            columns={columnsVendorBranchUser}
            count={vendorBranchUserCount}
            loading={vendorBranchUserLoading}
            onAddClick={this.showQuestionModal}
            hideSearchBox
          />
          <CPModal
            visible={this.state.visibleEditModal}
            handleOk={this.handleOkEditModal}
            handleCancel={this.handleCancelEditModal}
            title={EDIT_USER_ROLE}
          >
            <div className="row">
              <div className="col-sm-12">
                <label>{USER_USER_ROLE}</label>
                <CPMultiSelect
                  onChange={this.onChangeRoleSelect}
                  placeholder=""
                  value={selectedRoleIds}
                >
                  {RoleArray}
                </CPMultiSelect>
              </div>
            </div>
          </CPModal>
          <CPModal
            visible={this.state.visibleQuestionModal}
            handleOk={this.showFullModal}
            handleButton={this.createNewUser}
            handleCancel={this.closeQuestionModal}
            isButton
            iconClose=""
            iconSave=""
            textClose={USER_CREATE}
            textSave={SELECT_OF_EXIST_USER}
          >
            <h3>{CHANGE_USER_ROLE}</h3>
          </CPModal>
          <CPModal
            visible={this.state.visibleUserFullModal}
            handleOk={this.insertUserRole}
            handleCancel={this.handleCancelUserFullModal}
            title={SELECT_USER}
            className="max_modal"
          >
            <CPList
              data={usersArray}
              count={popupUserCount}
              columns={columnsUsersModal}
              loading={popupUserLoading}
              hideSearchBox
              hideAdd
            />
            <div className={s.wrapper}>
              {isShowDiv && (
                <div className="col-lg-4 col-sm-12">
                  <div className={s.info}>
                    <div className="col-xs-12">
                      <b>{USER_FULL_NAME} :</b>
                      <small>{name}</small>
                    </div>
                    <div className="col-xs-12">
                      <b>{EMAIL} :</b>
                      <small>{email}</small>
                    </div>
                    <div className="col-xs-12">
                      <b>{PHONE} :</b>
                      <small>{phone}</small>
                    </div>
                    <div className="col-xs-12">
                      <label>{USER_USER_ROLE}</label>
                      <CPMultiSelect
                        onChange={this.onChangeRoleSelect}
                        placeholder=""
                        value={selectedRoleIds}
                      >
                        {RoleArray}
                      </CPMultiSelect>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </CPModal>
        </div>
      </CPPermission>
    );
  }
}

const mapStateToProps = state => ({
  vendorBranchUserLoading: state.vendorBranchUserList.loading,
  vendorBranchUsers: state.vendorBranchUserList.data.items,
  vendorBranchUserCount: state.vendorBranchUserList.data.count,
  popupUserLoading: state.vendorUserList.loading,
  popupUsers: state.vendorUserList.data.items,
  popupUserCount: state.vendorUserList.data.count,
  roles: state.role.data.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      roleListRequest: roleActions.roleListRequest,
      vendorBranchUserListRequest:
        vendorBranchUserListActions.vendorBranchUserListRequest,
      vendorBranchUserDeleteRequest:
        vendorBranchUserDeleteActions.vendorBranchUserDeleteRequest,
      vendorBranchUserPostRequest:
        vendorBranchUserPostActions.vendorBranchUserPostRequest,
      vendorBranchUserPutRequest:
        vendorBranchUserPutActions.vendorBranchUserPutRequest,
      vendorUserListRequest: vendorUserListActons.vendorUserListRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorBranchUser));
