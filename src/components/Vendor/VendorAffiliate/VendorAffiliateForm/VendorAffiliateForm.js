import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { VENDOR_INFO } from '../../../../Resources/Localization';
import VendorAffiliateInfo from '../VendorAffiliateInfo';
import s from './VendorAffiliateForm.css';

class VendorForm extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    return (
      <Tabs>
        <TabList>
          <Tab>{VENDOR_INFO}</Tab>
        </TabList>
        <TabPanel>
          <VendorAffiliateInfo />
        </TabPanel>
      </Tabs>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorForm));
