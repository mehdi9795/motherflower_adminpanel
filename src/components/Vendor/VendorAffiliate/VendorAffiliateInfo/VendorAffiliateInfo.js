import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import HotKeys from 'react-hot-keys';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorAffiliateInfo.css';
import {
  DESCRIPTION,
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  ADDRESS,
  ZONE,
  CITY,
  PHONE_NUMBER,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import CPInput from '../../../CP/CPInput';
import { VendorAffiliateDto } from '../../../../dtos/vendorDtos';
import vendorAffiliatePutActions from '../../../../redux/vendor/actionReducer/vendorAffiliate/Put';
import CPSelect from '../../../CP/CPSelect';
import CPMap from '../../../CP/CPMap';
import { CityDto } from '../../../../dtos/commonDtos';
import { BaseCRUDDtoBuilder } from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class VendorInfo extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    cities: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    data: {},
    cities: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      fullName: props.data ? props.data.fullName : null,
      phone: props.data ? props.data.phone : null,
      vendorName: props.data ? props.data.vendorName : null,
      description: props.data ? props.data.description : null,
      instagram: props.data ? props.data.instagram : 0,
      address: props.data ? props.data.address : '',
      cityId: props.data && props.data.cityDto ? props.data.cityDto.id.toString() : 0,
      district: props.data ? props.data.district : '',
      lat: props.data ? props.data.lat : '',
      lng: props.data ? props.data.lng : '',
      id: props.data ? props.data.id : 0,
    };
  }

  preparingContainer = () => {
    const {
      cityId,
      district,
      fullName,
      phone,
      instagram,
      vendorName,
      address,
      description,
      lat,
      lng,
      id,
    } = this.state;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new VendorAffiliateDto({
          cityDto: new CityDto({ id: cityId }),
          district,
          fullName,
          phone,
          instagram,
          vendorName,
          description,
          address,
          lat,
          lng,
          id,
        }),
      )
      .build();

    return jsonCrud;
  }

  saveForm = () => {
    const { data } = this.props;

    const postData = {
      data: this.preparingContainer(),
      status: 'save',
    };

    if (data && data.id) {
      this.props.actions.vendorAffiliatePutRequest(postData);
    }
  };

  saveFormAndContinue = () => {
    const { data } = this.props;

    const postData = {
      data: this.preparingContainer(),
      status: 'saveAndContinue',
    };

    if (data && data.id) {
      this.props.actions.vendorAffiliatePutRequest(postData);
    }
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handleInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  render() {
    const { cities } = this.props;
    const {
      cityId,
      district,
      fullName,
      phone,
      instagram,
      vendorName,
      address,
      description,
      lat,
      lng,
    } = this.state;
    const citiesDataSource = [];

    /**
     * map cities for comboBox Datasource
     */
    cities.map(item =>
      citiesDataSource.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <div className="row">
          <div className="col-md-6 col-sm-12">
            <div className="row">
              <div className="col-md-6 col-sm-12">
                <CPInput
                  label="نام و نام خانوادگی ثبت کننده"
                  onChange={value =>
                    this.handleInput('fullName', value.target.value)
                  }
                  value={fullName}
                />
              </div>
              <div className="col-md-6 col-sm-12">
                <CPInput
                  label={PHONE_NUMBER}
                  onChange={value =>
                    this.handleInput('phone', value.target.value)
                  }
                  value={phone}
                />
              </div>
              <div className="col-md-6 col-sm-12">
                <CPInput
                  label="اینستاگرام"
                  onChange={value =>
                    this.handleInput('instagram', value.target.value)
                  }
                  value={instagram}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 col-sm-12">
                <CPInput
                  label="نام گلفروشی"
                  onChange={value =>
                    this.handleInput('vendorName', value.target.value)
                  }
                  value={vendorName}
                />
              </div>
              <div className="col-md-6 col-sm-12">
                <label>{CITY}</label>
                <CPSelect
                  onChange={value => this.handleInput('cityId', value)}
                  defaultValue={cityId}
                  showSearch
                >
                  {citiesDataSource}
                </CPSelect>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <CPInput
                  label={ZONE}
                  onChange={value =>
                    this.handleInput('district', value.target.value)
                  }
                  value={district}
                />
              </div>
            </div>
          </div>
          <div className="col-sm-12">
            <CPInput
              label={ADDRESS}
              onChange={value =>
                this.handleInput('address', value.target.value)
              }
              value={address}
            />
          </div>
          <div className="col-sm-12">
            <CPInput
              label={DESCRIPTION}
              onChange={value =>
                this.handleInput('description', value.target.value)
              }
              value={description}
            />
          </div>
          <div className="col-xs-12">
            <label>مکان یابی روی نقشه</label>
            <CPMap
              containerElement="300px"
              defaultCenter={{ lat: 35.6891975, lng: 51.3889736 }}
              defaultZoom={10}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
              mapElement="100%"
              loadingElement="100%"
              draggable
              onDragEnd={value => this.setLatAndLng(value)}
              lat={lat}
              lng={lng}
            />
          </div>
          <div className="col-sm-12">
            <div className={s.textLeft}>
              <CPButton
                icon="save"
                className="btn-primary"
                onClick={this.saveFormAndContinue}
              >
                {FORM_SAVE_AND_CONTINUE}
              </CPButton>
              <CPButton icon="save" onClick={this.saveForm}>
                {FORM_SAVE}
              </CPButton>
            </div>
          </div>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  data:
    state.singleVendorAffiliate.data &&
    state.singleVendorAffiliate.data.items &&
    state.singleVendorAffiliate.data.items[0]
      ? state.singleVendorAffiliate.data.items[0]
      : {},
  cities: state.cityList.data ? state.cityList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorAffiliatePutRequest:
        vendorAffiliatePutActions.vendorAffiliatePutRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorInfo));
