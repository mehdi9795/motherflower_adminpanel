import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select, message } from 'antd';
import HotKeys from 'react-hot-keys';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorInfo.css';
import {
  VENDOR_NAME,
  EMAIL,
  DESCRIPTION,
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  PRODUCT_PRODUCT_TYPE,
  ACTIVE,
  DEACTIVATE,
  IMAGE_UPLOAD,
  VENDOR_CODE,
} from '../../../../Resources/Localization';
import { AGENTS } from '../../../../Resources/Urls';
import CPButton from '../../../CP/CPButton';
import CPInput from '../../../CP/CPInput';
import CPMultiSelect from '../../../CP/CPMultiSelect/CPMultiSelect';
import CPSwitch from '../../../CP/CPSwitch';
import CPTextArea from '../../../CP/CPTextArea';
import history from '../../../../history';
import { PostData } from '../../../../utils/index';
import { AgentDto, AgentProductTypeDto } from '../../../../dtos/vendorDtos';
import { ProductTypeDto } from '../../../../dtos/catalogDtos';
import vendorAgentPostActions from '../../../../redux/vendor/actionReducer/vendor/Post';
import vendorAgentPutActions from '../../../../redux/vendor/actionReducer/vendor/Put';
import CPUpload from '../../../CP/CPUpload';
import { ImageDto } from '../../../../dtos/cmsDtos';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class VendorInfo extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    productType: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    data: {},
    productType: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      name: props.data ? props.data.name : null,
      code: props.data ? props.data.code : null,
      email: props.data ? props.data.email : null,
      description: props.data ? props.data.description : null,
      pictureId: props.data ? props.data.pictureId : 0,
      url:
        props.data && props.data.imageDtos && props.data.imageDtos.length > 0
          ? `${props.data.imageDtos[0].url}`
          : '',
      active: props.data ? props.data.active : true,
      vendorProductTypeDtos:
        props.productType && props.productType.length > 0
          ? [props.productType[0].id.toString()]
          : [],
      binariesImage: '',
      extention: '',
    };
    this.postData = this.postData.bind(this);
  }

  componentWillMount() {
    const { data } = this.props;
    const isEdit = data && data.id;
    /**
     * map Vendor product type Dto for comboBox Datasource
     */

    if (isEdit) {
      this.setState({ vendorProductTypeDtos: [] });
      const vendorProductTypeSelectedIds = [];
      data.vendorProductTypeDtos.map(item =>
        vendorProductTypeSelectedIds.push(item.productTypeDto.id.toString()),
      );
      this.setState({ vendorProductTypeDtos: vendorProductTypeSelectedIds });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;

    if (data) {
      this.setState({
        name: data.name,
        code: data.code,
        email: data.email,
        description: data.description,
        pictureId: data.pictureId,
        active: true,
      });
    }
  }

  /**
   * set value category multi select to state
   * @param value
   */
  onChangeProductTypeSelect = value => {
    if (value.length >= 1)
      this.setState({
        vendorProductTypeDtos: value,
      });
    else message.warning('حداقل باید یک نوع محصول انتخاب شده باشد.', 5);
  };

  saveForm = () => {
    const { data } = this.props;
    const {
      name,
      code,
      email,
      description,
      vendorProductTypeDtos,
      active,
      binariesImage,
      extention,
    } = this.state;
    const productTypeIds = [];

    vendorProductTypeDtos.map(item => {
      productTypeIds.push(
        new AgentProductTypeDto({
          productTypeDto: new ProductTypeDto({ id: item }),
        }),
      );
      return null;
    });

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new AgentDto({
          name,
          code,
          email,
          description,
          active,
          id: data ? data.id : 0,
          vendorProductTypeDtos: productTypeIds,
          imageDtos: binariesImage
            ? [
                new ImageDto({
                  extention,
                  binaries: binariesImage.split(',')[1],
                }),
              ]
            : null,
        }),
      )
      .build();

    const postData = {
      data: jsonCrud,
      status: 'save',
      listDto: new BaseGetDtoBuilder()
        .dto(new AgentDto({ active: true }))
        .includes(['imageDtos'])
        .buildJson(),
    };

    if (data && data.id) {
      this.props.actions.vendorAgentPutRequest(postData);
    } else {
      this.props.actions.vendorAgentPostRequest(postData);
    }
  };

  saveFormAndContinue = () => {
    const { data } = this.props;
    const {
      name,
      code,
      email,
      description,
      vendorProductTypeDtos,
      active,
      binariesImage,
      extention,
    } = this.state;
    const productTypeIds = [];

    vendorProductTypeDtos.map(item => {

      productTypeIds.push(
        new AgentProductTypeDto({
          productTypeDto: new ProductTypeDto({ id: item }),
        }),
      );
      return null;
    });

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new AgentDto({
          name,
          code,
          email,
          description,
          active,
          id: data ? data.id : 0,
          vendorProductTypeDtos: productTypeIds,
          imageDtos: binariesImage
            ? [
                new ImageDto({
                  extention,
                  binaries: binariesImage.split(',')[1],
                }),
              ]
            : null,
        }),
      )
      .build();

    const postData = {
      data: jsonCrud,
      status: 'saveAndContinue',
      listDto: new BaseGetDtoBuilder()
        .dto(new AgentDto({ active: true }))
        .includes(['imageDtos'])
        .buildJson(),
    };

    if (data && data.id) {
      this.props.actions.vendorAgentPutRequest(postData);
    } else {
      this.props.actions.vendorAgentPostRequest(postData);
    }
  };

  redirect = id => {
    history.push(`/vendor/edit/${id}`);
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  handleImageChange = value => {
    this.setState({
      binariesImage: value.base64,
      extention: value.type.split('/')[1],
    });
  };

  async postData(container) {
    const data = await PostData(`${AGENTS}`, container);
    if (data) {
      return data;
    }
    return data;
  }

  prepareSelectedProductTypes = () => {
    const { data } = this.props;
    const selectedProductTypes = [];
    if (data) {
      data.vendorProductTypeDtos.map(item => {
        if (item.productTypeDto) {
          selectedProductTypes.push(item.productTypeDto.id);
        }
        return null;
      });
    }
    return selectedProductTypes;
  };

  render() {
    const { productType } = this.props;
    const {
      code,
      name,
      email,
      description,
      vendorProductTypeDtos,
      active,
      url,
    } = this.state;
    const productTypeArray = [];

    /**
     * map Product type for comboBox Datasource
     */
    productType.map(item =>
      productTypeArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <div className="row">
          <div className="col-md-6 col-sm-12">
            <div className="row">
              <div className="col-md-6 col-sm-12">
                <CPInput
                  label={VENDOR_CODE}
                  value={code}
                  onChange={value =>
                    this.handelChangeInput('code', value.target.value)
                  }
                />
              </div>
              <div className="col-md-6 col-sm-12">
                <CPInput
                  label={VENDOR_NAME}
                  value={name}
                  onChange={value =>
                    this.handelChangeInput('name', value.target.value)
                  }
                />
              </div>
              <div className="col-md-6 col-sm-12">
                <CPInput
                  label={EMAIL}
                  value={email}
                  onChange={value =>
                    this.handelChangeInput('email', value.target.value)
                  }
                />
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 col-sm-12">
                <label>{PRODUCT_PRODUCT_TYPE}</label>
                <CPMultiSelect
                  onChange={this.onChangeProductTypeSelect}
                  placeholder=""
                  value={vendorProductTypeDtos}
                >
                  {productTypeArray}
                </CPMultiSelect>
              </div>
              <div className="col-md-6 col-sm-12">
                <div className="align-center">
                  <CPSwitch
                    defaultChecked={active}
                    unCheckedChildren={DEACTIVATE}
                    checkedChildren={ACTIVE}
                    onChange={value => this.handelChangeInput('active', value)}
                  />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <label>{DESCRIPTION}</label>
                <CPTextArea
                  placeholder={DESCRIPTION}
                  value={description}
                  onChange={value =>
                    this.handelChangeInput('description', value.target.value)
                  }
                />
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12">
            <div className="col-sm-12">
              <label>{IMAGE_UPLOAD}</label>
              <CPUpload
                className={s.uploadImg}
                onChange={this.handleImageChange}
                image={`${url}`}
              />
            </div>
          </div>
          <div className="col-sm-12">
            <div className={s.textLeft}>
              <CPButton
                icon="save"
                className="btn-primary"
                onClick={this.saveFormAndContinue}
              >
                {FORM_SAVE_AND_CONTINUE}
              </CPButton>
              <CPButton icon="save" onClick={this.saveForm}>
                {FORM_SAVE}
              </CPButton>
            </div>
          </div>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  productType:
    state.productTypesList && state.productTypesList.data
      ? state.productTypesList.data.items
      : [],
  data:
    state.singleVendorAgent.data &&
    state.singleVendorAgent.data.items &&
    state.singleVendorAgent.data.items[0]
      ? state.singleVendorAgent.data.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorAgentPostRequest: vendorAgentPostActions.vendorAgentPostRequest,
      vendorAgentPutRequest: vendorAgentPutActions.vendorAgentPutRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorInfo));
