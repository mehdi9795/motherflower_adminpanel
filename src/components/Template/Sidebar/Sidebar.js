/* eslint-disable css-modules/no-undef-class */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Menu } from 'antd';
import s from './Sidebar.css';
import Link from '../../Link/Link';
import * as actionCreators from '../../../redux/identity/action/user';
import history from '../../../history';

const SubMenu = Menu.SubMenu;
const MenuItem = Menu.Item;

class Sidebar extends React.Component {
  static propTypes = {
    permissions: PropTypes.arrayOf(PropTypes.object).isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    permissions: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
    };
  }

  onClick = e => {
    e.preventDefault();
  };

  goToPage = (e, url) => {
    e.preventDefault();
    this.props.actions.showLoading();
    history.push(url);
  };

  render() {
    const { permissions } = this.props;
    return (
      <Menu
        className={s.sidebar}
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1', 'sub2', 'sub3', 'sub4', 'sub5']}
        mode="inline"
        inlineCollapsed={this.state.collapsed}
      >
        {permissions.map(item => (
          <SubMenu
            key={item.id}
            title={
              <span>
                <i className={item.attribute} />
                <span>
                  <Link to="/" onClick={e => this.onClick(e)}>
                    {item.resourceTitle}
                  </Link>
                </span>
              </span>
            }
          >
            {item.children.map(child => (
              <MenuItem key={child.id}>
                <Link
                  to={child.resourceValue}
                  onClick={e => this.goToPage(e, child.resourceValue)}
                >
                  {child.resourceTitle}
                </Link>
              </MenuItem>
            ))}
          </SubMenu>
        ))}
      </Menu>
    );
  }
}

const mapStateToProps = state => ({
  permissions:
    state.permission.data &&
    state.permission.data.items &&
    state.permission.data.items.length > 0
      ? state.permission.data.items[0].menuPermissionDto
      : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({
    showLoading,
  }, dispatch),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(Sidebar),
);
