import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Menu, Dropdown, Avatar, Tabs } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import s from './Navigation.css';
import Link from '../../Link/Link';
import history from '../../../history';
import getNotificationActions from '../../../redux/notification/actionReducer/notification/GetNotification';
import messageDetailsActions from '../../../redux/notification/actionReducer/message/Details';
import logoutActions from '../../../redux/identity/actionReducer/account/Logout';
import CPButton from '../../CP/CPButton';
import { MESSAGES } from '../../../Resources/Localization';
import { getDtoQueryString } from '../../../utils/helper';
import { MessageDto } from '../../../dtos/notificationDtos';
import {
  GetNotificationApi,
  PutNotificationApi,
} from '../../../services/notificationApi';
import { getCookie } from '../../../utils';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';

const { TabPane } = Tabs;

class Navigation extends React.Component {
  static propTypes = {
    avatarImg: PropTypes.string.isRequired,
    notifications: PropTypes.arrayOf(PropTypes.object),
    loginData: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    avatarImg: '/images/avatar.jpg',
    notifications: [],
    loginData: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      countNotification: 0,
    };
    this.getNewMessage = this.getNewMessage.bind(this);
    this.goToMessageList = this.goToMessageList.bind(this);
    this.interval = null;
  }

  componentDidMount() {
    const { loginData } = this.props;
    this.getNewMessage();
    this.interval = setInterval(() => {
      if (loginData.token) {
        this.getNewMessage();
      }
    }, 30000);
  }

  async getNewMessage() {
    const token = getCookie('token');

    const json = new BaseGetDtoBuilder()
      .dto(
        new MessageDto({
          messageSearchMode: 'Inbox',
          searchIsRead: false,
        }),
      )
      .buildJson();

    const counter = 0;
    const response = await GetNotificationApi(token, getDtoQueryString(json));

    if (response.status === 200) {
      this.props.actions.getNotificationSuccess(response.data);
      this.setState({ countNotification: response.data.items.length });
      /* response.data.items
        .slice(0)
        .reverse()
        .map(item => {
          counter += 1;
          if (counter <= 5)
            showNotification(
              'info',
              <Link to="/#" onClick={e => this.goToMessageList(item, e)}>
                {' '}
                {item.title}
              </Link>,
              item.subject,
              9,
            );
          return null;
        }); */
    }
  }

  async goToMessageList(record, e) {
    e.preventDefault();
    const token = getCookie('token');
    await PutNotificationApi(token, { id: record.id });
    this.props.actions.messageDetailsSuccess(record);
    history.push(`/message/list`);
  }

  logOut = () => {
    clearInterval(this.interval);
    history.push('/login');
    this.props.actions.logoutRequest();
  };

  render() {
    const { avatarImg, loginData, notifications } = this.props;
    const data = [];
    notifications.map(item => {
      const obj = {
        date: item.stringCreatedOnUtc,
        name: item.title,
        link: '/message/list',
        text: item.subject,
        key: item.id,
        record: item,
      };
      data.push(obj);
      return null;
    });

    const notification = (
      <Menu>
        <div className={s.notification}>
          <Tabs defaultActiveKey="1" animated>
            <TabPane tab={MESSAGES} key="1">
              <ul className={s.messageList}>
                {data.map(item => (
                  <li key={item.key}>
                    <Link
                      to="/#"
                      onClick={e => this.goToMessageList(item.record, e)}
                      className={s.message}
                    >
                      <Avatar src={avatarImg} />
                      <span className={s.content}>
                        <Link
                          className={s.name}
                          onClick={e => this.goToMessageList(item.record, e)}
                          to="/#"
                        >
                          {item.name}
                        </Link>
                        <p>{item.text}</p>
                      </span>
                    </Link>
                  </li>
                ))}
              </ul>
              {/* <Link to="/#" className={s.more}>
                بیشتر ...
              </Link> */}
            </TabPane>
          </Tabs>
        </div>
      </Menu>
    );

    const menu = (
      <Menu>
        <div className={s.menu}>
          <div className={s.firstRow}>
            <Avatar src={avatarImg} />
            <div className={s.userInfo}>
              <Link to="/profile">پروفایل من</Link>
              <b>{decodeURIComponent(getCookie('displayName'))}</b>
              <b>
                {loginData.userName && loginData.userName.replace('+98', '0')}
              </b>
            </div>
          </div>
          <div className={s.secondRow}>
            {/*<CPButton className={s.btn}>
              <Link to="/changePassword">
                <i className="cp-locked" />تغییر رمز عبور
              </Link>
            </CPButton>*/}
            <CPButton className={s.btn} onClick={this.logOut}>
              <i className="cp-logout" />خروج
            </CPButton>
          </div>
        </div>
      </Menu>
    );

    return (
      <div className={s.root}>
        <ul className={s.userMenu}>
          <li>
            <Dropdown overlay={notification} trigger={['click']}>
              <Link className={s.link} to="#">
                <i className="cp-bell" />
                {this.state.countNotification > 0 && (
                  <span className={s.count}>
                    {this.state.countNotification}
                  </span>
                )}
              </Link>
            </Dropdown>
          </li>
          <li>
            <Dropdown overlay={menu} trigger={['click']}>
              <Link className="ant-dropdown-link" to="#">

                <Avatar src={loginData.imageUser || avatarImg} />
              </Link>
            </Dropdown>
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  notifications: state.notificationList.data
    ? state.notificationList.data.items
    : [],
  loginData: state.login.data ? state.login.data : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getNotificationSuccess: getNotificationActions.getNotificationSuccess,
      logoutRequest: logoutActions.logoutRequest,
      messageDetailsSuccess: messageDetailsActions.messageDetailsSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Navigation));
