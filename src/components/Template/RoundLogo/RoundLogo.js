/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RoundLogo.css';
import logoUrl from './logo.png';
import logoUrl2 from './logo-white.png';
import Link from '../../Link/Link';

class RoundLogo extends React.Component {
  render() {
    return (
      <div className={s.logoWrapper}>
        <i className={s.lineRight} />
        <Link className={s.mainLogo} to="/">
          <img className={s.logoColored} src={logoUrl} alt="Mother Flower" />
          <img className={s.logoWhite} src={logoUrl2} alt="Mother Flower" />
        </Link>
        <i className={s.lineLeft} />
      </div>
    );
  }
}

export default withStyles(s)(RoundLogo);
