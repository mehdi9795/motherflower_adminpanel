import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import LoadingBar from 'react-redux-loading-bar';
import s from './Header.css';
import Navigation from '../Navigation/Navigation';

class Header extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <LoadingBar />
        <div className={s.container}>
          <Navigation />
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Header);
