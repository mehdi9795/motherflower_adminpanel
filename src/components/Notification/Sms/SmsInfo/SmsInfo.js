import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HotKeys from 'react-hot-keys';
import { bindActionCreators } from 'redux';
import { Select, message } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SmsInfo.css';
import CPInput from '../../../CP/CPInput/CPInput';
import CPButton from '../../../CP/CPButton/CPButton';
import {
  ACTIVE,
  ALL,
  BLOCK,
  CITY,
  CONFIRMATION,
  CREATE_DATE,
  DATE,
  DEACTIVATE, DEVICE_TYPE,
  EMAIL,
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  HOUR,
  IMAGE_UPLOAD,
  LOGIN_HOUR,
  NO,
  OFFLINE,
  ONLINE,
  PHONE,
  PHONE_NUMBER,
  PRODUCT_LIST_CATEGORY,
  REAGENT_USER,
  ROLE_SELECT_PLACEHOLDER,
  SELECT,
  SELECT_STATUS_EVENT,
  SENDING_CODE_DISCOUNT,
  SMS,
  STATUS,
  SUBJECT,
  SYSTEM,
  TYPE,
  UN_WILLINGNESS,
  USER_FIRST_NAME,
  USER_FULL_NAME,
  USER_LAST_NAME,
  USER_ROLE,
  USER_USER_ROLE,
  USERS,
  WAS_NOT_RESPONSE,
  WAS_RESPONSE,
  YES,
} from '../../../../Resources/Localization';
import CPUpload from '../../../CP/CPUpload';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPSelect from '../../../CP/CPSelect';
import CPPersianCalendar from '../../../CP/CPPersianCalendar';
import {
  PushDto,
  SmsBulkRoleDto, SmsBulkUserDto,
  SmsDto,
} from '../../../../dtos/notificationDtos';
import smsPostActions from '../../../../redux/notification/actionReducer/sms/Post';
import smsPutActions from '../../../../redux/notification/actionReducer/sms/Put';
import CPMultiSelect from '../../../CP/CPMultiSelect';
import { RoleDto, UserDto, UserRoleDto } from '../../../../dtos/identityDtos';
import CPTextArea from '../../../CP/CPTextArea';
import CPModal from '../../../CP/CPModal';
import CPList from '../../../CP/CPList';
import CPPagination from '../../../CP/CPPagination';
import CPIntlPhoneInput from '../../../CP/CPIntlPhoneInput';
import userListActions from '../../../../redux/identity/actionReducer/user/List';
import { CityDto } from '../../../../dtos/commonDtos';
import CPSwitch from '../../../CP/CPSwitch';

const { Option } = Select;

class SmsInfo extends React.Component {
  static propTypes = {
    sms: PropTypes.objectOf(PropTypes.any),
    roles: PropTypes.arrayOf(PropTypes.object),
    smsStatus: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    userLoading: PropTypes.bool,
    users: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    userCount: PropTypes.number,
  };

  static defaultProps = {
    userCount: 0,
    userLoading: false,
    users: [],
    cities: [],
    sms: null,
    roles: [],
    smsStatus: [],
  };

  constructor(props) {
    super(props);
    const roleIds = [];
    const userIds = [];
    const userMultiSelectArray = [];
    if (
      props.sms &&
      props.sms.smsBulkRoleDtos &&
      props.sms.smsBulkRoleDtos.length > 0
    )
      props.sms.smsBulkRoleDtos.map(item =>
        roleIds.push(item.roleDto.id.toString()),
      );

    if (
      props.sms &&
      props.sms.smsBulkUserDtos &&
      props.sms.smsBulkUserDtos.length > 0
    )
      props.sms.smsBulkUserDtos.map(item => {
          userIds.push(item.userDto.id.toString());
            userMultiSelectArray.push(<Option key={item.userDto.id.toString()}>{item.userDto.displayName}</Option>);
        }
      );

    this.state = {
      body: props.sms.id ? props.sms.body.replace(/enter/g, '\n') : '',
      status: props.sms.id ? props.sms.statusId.toString() : '0',
      date: props.sms.id ? props.sms.persianDateUtc : '',
      hour: props.sms.id ? props.sms.hour : '',
      hourArray: [],
      id: props.sms.id ? props.sms.id : 0,
      roleIds,
      visibleUserModal: false,
      searchFirstName: '',
      searchLastName: '',
      searchPhoneNumber: '',
      searchRoleIds: [],
      pageSize: 10,
      currentPage: 1,
      searchUserEventStatus: 0,
      searchOffline: '0',
      searchCityId: 0,
      searchUserStatus: true,
      selectedUserIds: [],
      selectedUserIdsFinal: userIds,
      userMultiSelectArray: userMultiSelectArray,
      smsBulkUserTypesId: props.sms.smsBulkUserTypes || [],
    };
    this.userArray = [];
  }

  componentWillMount() {
    for (let i = 7; i <= 24; i += 1) {
      this.state.hourArray.push(<Option key={`${i}`}>{i}</Option>);
    }
  }

  /**
   * set dto with state for post and put data
   * @returns {{dto: {name: *, description: *, parentCategoryId: *, showOnHomePage: *, published: *}}}
   */
  prepareContainer = () => {
    const { body, status, date, hour, id, roleIds, selectedUserIdsFinal, smsBulkUserTypesId } = this.state;
    const roleDtos = [];
    const userDtos = [];
    roleIds.map(item => {
      const dto = new SmsBulkRoleDto({
        roleDto: new RoleDto({
          id: item,
        }),
        smsBulkDto: new RoleDto({
          id,
        }),
      });
      roleDtos.push(dto);
      return null;
    });

    selectedUserIdsFinal.map(item => {
      const dto = new SmsBulkUserDto({
        userDto: new UserDto({
          id: item,
        }),
        smsBulkDto: new UserDto({
          id,
        }),
      });
      userDtos.push(dto);
      return null;
    });

    const jsonList = new BaseCRUDDtoBuilder()
      .dto(
        new SmsDto({
          id,
          body,
          status: id ? status : 'InActive',
          persianDateUtc: date,
          hour,
          smsBulkRoleDtos: roleDtos,
          smsBulkUserDtos: userDtos,
          smsBulkUserTypes: smsBulkUserTypesId.length === 0 ? ['All'] : smsBulkUserTypesId,
        }),
      )
      .build();
    return jsonList;
  };

  /**
   * save data with post and put - redirect to list page
   */
  saveForm = () => {
    const { sms } = this.props;
    const { selectedUserIdsFinal, roleIds } = this.state;
    if (selectedUserIdsFinal.length > 0 || roleIds.length > 0) {
      if (sms && sms.id) {
        this.putData(this.prepareContainer());
      } else {
        this.postData(this.prepareContainer());
      }
    } else
      message.error('حداقل باید یک نقش کاربری یا یک کاربر را انتخاب کنید', 5);
  };

  /**
   * save data with post and put - redirect to edit page
   */
  saveFormAndContinue = () => {
    const { sms } = this.props;
    const { selectedUserIdsFinal, roleIds } = this.state;
    if (selectedUserIdsFinal.length > 0 || roleIds.length > 0) {
      if (sms && sms.id) {
        this.putData(this.prepareContainer(), true);
      } else {
        this.postData(this.prepareContainer(), true);
      }
    } else
      message.error('حداقل باید یک نقش کاربری یا یک کاربر را انتخاب کنید', 5);
  };

  /**
   * call post data for sending data
   * @param containert
   * @returns {Promise<void>}
   */
  async postData(container, saveAndCountinue = false) {
    const data = {
      data: container,
      status: saveAndCountinue ? 'saveAndContinue' : 'save',
    };

    this.props.actions.smsPostRequest(data);
  }

  async putData(container, saveAndCountinue = false) {
    const data = {
      data: container,
      status: saveAndCountinue ? 'saveAndContinue' : 'save',
    };

    this.props.actions.smsPutRequest(data);
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  showAndHideUserModal = value => {
    this.setState({
      visibleUserModal: value,
    });
    if (value) this.submitSearch();
  };

  prepareContainerUserModal = () => {
    const {
      searchFirstName,
      searchLastName,
      phoneFinal,
      searchRoleIds,
      pageSize,
      currentPage,
      selectedUserIdsFinal,
      searchOffline,
      searchCityId,
      searchUserStatus,
      searchUserEventStatus,
    } = this.state;
    const userRoleDtos = [];

    let offline;
    if (searchOffline === '2') offline = true;
    else if (searchOffline === '1') offline = false;

    searchRoleIds.map(roleId => {
      userRoleDtos.push(
        new UserRoleDto({ roleDto: new RoleDto({ id: roleId }) }),
      );
      return null;
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserDto({
          searchFirstName,
          searchLastName,
          userName: phoneFinal,
          userRoleDto: userRoleDtos,
          active: searchUserStatus,
          searchUserEventStatus,
          searchOffline: offline,
          cityDto:
            searchCityId === 0 ? undefined : new CityDto({ id: searchCityId }),
        }),
      )
      .pageIndex(currentPage - 1)
      .pageSize(pageSize)
      .includes(['Roles'])
      .orderByFields(['createdOnUtc'])
      .orderByDescending(true)
      .notExpectedIds(selectedUserIdsFinal)
      .buildJson();

    return jsonList;
  };

  submitSearch = () => {
    this.setState({ currentPage: 1 }, () => {
      this.props.actions.userListRequest(this.prepareContainerUserModal());
    });
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneFinal: number.replace(/ /g, ''),
        searchPhoneNumber: value,
      });
    }
  };

  onSelectedUser = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedUserIds: selectedRowKeys });

    selectedRows.map(item => {
      const index = this.userArray.findIndex(id => id.key === item.key);
      if (index < 0)
        this.userArray.push({ key: item.key, name: item.fullName });
      return null;
    });

    this.userArray.map((item, index) => {
      const index2 = selectedRowKeys.findIndex(id => id === item.key);
      if (index2 < 0) this.userArray.splice(index, 1);
      return null;
    });
  };

  handleOkUserModal = () => {
    const { selectedUserIds } = this.state;
    // this.attributeArray = [];
    const selectedIds = [];
    const selectedArray = [];

    selectedUserIds.map(id => selectedIds.push(id.toString()));
    this.userArray.map(item =>
      selectedArray.push(<Option key={item.key}>{item.name}</Option>),
    );
    this.setState({
      visibleUserModal: false,
      selectedUserIdsFinal: selectedIds,
      userMultiSelectArray: selectedArray,
    });
  };

  render() {
    const { smsStatus, userLoading, users, userCount, cities } = this.props;
    const {
      roleIds,
      body,
      status,
      date,
      hour,
      hourArray,
      id,
      selectedUserIdsFinal,
      visibleUserModal,
      searchFirstName,
      searchLastName,
      searchPhoneNumber,
      searchRoleIds,
      searchEmail,
      searchUserStatus,
      searchUserEventStatus,
      searchOffline,
      searchCityId,
      selectedUserIds,
      userMultiSelectArray,
      smsBulkUserTypesId,
    } = this.state;
    const { roles } = this.props;

    const roleArray = [];

    const columns = [
      {
        title: USER_FULL_NAME,
        dataIndex: 'fullName',
        key: 'firstName',
        width: 150,
      },
      {
        title: '',
        dataIndex: 'alert',
        key: 'alert',
        width: 10,
        render: (text, record) => (
          <div>
            {record.userEventStatusId === 1 && (
              <img src="/images/WasResponsive.png" alt="aa" width={15} />
            )}
            {record.userEventStatusId === 2 && (
              <img src="/images/WasNotResponsive.png" alt="aa" width={15} />
            )}
            {record.userEventStatusId === 3 && (
              <img src="/images/Unwillingness.png" alt="aa" width={15} />
            )}
            {record.userEventStatusId === 4 && (
              <img src="/images/system.png" alt="aa" width={15} />
            )}
            {record.userEventStatusId === 5 && (
              <img src="/images/block.png" alt="aa" width={15} />
            )}
          </div>
        ),
      },
      { title: PHONE, dataIndex: 'phone', key: 'phone', width: 100 },
      {
        title: USER_USER_ROLE,
        dataIndex: 'userRole',
        key: 'userRole',
        width: 150,
      },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
        width: 50,
      },
      {
        title: '',
        dataIndex: 'offline',
        render: (text, record) => (
          <div>
            <CPSwitch
              disabled
              size="small"
              defaultChecked={record.offline}
              checkedChildren={OFFLINE}
              unCheckedChildren={ONLINE}
            />
          </div>
        ),
        width: 80,
      },
    ];
    const cityArray = [];
    const userArray = [];
    const rowSelection = {
      selectedUserIds,
      onChange: this.onSelectedUser,
    };

    if (users)
      users.map(item => {
        const userRoleArray = [];
        item.userRoleDto.map(userRole =>
          userRoleArray.push(userRole.roleDto.name),
        );
        const obj = {
          key: item.id,
          fullName: `${item.firstName}  ${item.lastName}`,
          phone: item.phone.replace('+98', '0'),
          active: item.active,
          userRole: userRoleArray.join(),
          userEventStatusId: item.userEventStatusId,
          isCity: !!item.cityDto,
          offline: item.offline,
        };
        return userArray.push(obj);
      });

    roles.map(item =>
      roleArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    const statusArray = [];
    smsStatus.map(item =>
      statusArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    if (cities)
      cities.map(item =>
        cityArray.push(<Option key={item.id}>{item.name}</Option>),
      );

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <div>
          <div className="tabContent">
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{USER_ROLE}</label>
                <CPMultiSelect
                  value={roleIds}
                  onChange={value => this.handelChangeInput('roleIds', value)}
                >
                  {roleArray}
                </CPMultiSelect>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{SMS}</label>
                <CPTextArea
                  value={body}
                  onChange={value =>
                    this.handelChangeInput('body', value.target.value)
                  }
                />
              </div>
              <div className="col-md-6 col-sm-12">
                <label name="label">{USERS}</label>
                <div className="col-sm-12 selectUser">
                  <CPMultiSelect
                    placeholder="انتخاب کاربران"
                    onChange={value =>
                      this.handelChangeInput('selectedUserIdsFinal', value)
                    }
                    value={selectedUserIdsFinal}
                  >
                    {userMultiSelectArray}
                  </CPMultiSelect>
                  <CPButton onClick={() => this.showAndHideUserModal(true)}>
                    {SELECT}
                  </CPButton>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{DATE}</label>
                <CPPersianCalendar
                  placeholder={DATE}
                  onChange={(unix, formatted) =>
                    this.handelChangeInput('date', formatted)
                  }
                  preSelected={date}
                />
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{HOUR}</label>
                <CPSelect
                  value={hour}
                  onChange={value => this.handelChangeInput('hour', value)}
                >
                  {hourArray}
                </CPSelect>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label name="label">{DEVICE_TYPE}</label>
                  <CPMultiSelect
                    placeholder=""
                    onChange={value =>
                      this.handelChangeInput('smsBulkUserTypesId', value)
                    }
                    value={smsBulkUserTypesId}
                  >
                    <Option key={"Web"}>Web</Option>
                    <Option key={"Android"}>Android</Option>
                    <Option key={"iOS"}>IOS</Option>
                  </CPMultiSelect>
              </div>

              {id !== 0 && (
                <div className="col-lg-3 col-sm-6 col-xs-12">
                  <label>{STATUS}</label>
                  <CPSelect
                    value={status}
                    onChange={value => this.handelChangeInput('status', value)}
                  >
                    {statusArray}
                  </CPSelect>
                </div>
              )}
            </div>
          </div>
          <div className={s.textLeft}>
            <CPButton
              icon="form"
              className="btn-primary"
              onClick={this.saveFormAndContinue}
            >
              {FORM_SAVE_AND_CONTINUE}
            </CPButton>
            <CPButton icon="save" onClick={this.saveForm}>
              {FORM_SAVE}
            </CPButton>
          </div>
        </div>
        <CPModal
          visible={visibleUserModal}
          handleCancel={() => this.showAndHideUserModal(false)}
          handleOk={this.handleOkUserModal}
          className="max_modal"
          title="انتخاب کاربر"
          textSave={CONFIRMATION}
        >
          <CPList
            data={userArray}
            count={userCount}
            columns={columns}
            loading={userLoading}
            pagination={false}
            hideAdd
            withCheckBox
            rowSelection={rowSelection}
          >
            <div className="searchBox">
              <div className={s.searchFildes}>
                <div className="row">
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="email"
                      label={EMAIL}
                      value={searchEmail}
                      onChange={value => {
                        this.handelChangeInput(
                          'searchEmail',
                          value.target.value,
                        );
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{PHONE_NUMBER}</label>
                    <CPIntlPhoneInput
                      value={searchPhoneNumber}
                      onChange={this.changeNumber}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="firstName"
                      label={USER_FIRST_NAME}
                      value={searchFirstName}
                      onChange={value => {
                        this.handelChangeInput(
                          'searchFirstName',
                          value.target.value,
                        );
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <CPInput
                      name="lastName"
                      label={USER_LAST_NAME}
                      value={searchLastName}
                      onChange={value => {
                        this.handelChangeInput(
                          'searchLastName',
                          value.target.value,
                        );
                      }}
                    />
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <label>{USER_USER_ROLE}</label>
                    <CPMultiSelect
                      placeholder={ROLE_SELECT_PLACEHOLDER}
                      value={searchRoleIds}
                      onChange={value =>
                        this.handelChangeInput('searchRoleIds', value)
                      }
                    >
                      {roleArray}
                    </CPMultiSelect>
                  </div>
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <label>
                      {STATUS} {OFFLINE}
                    </label>
                    <CPSelect
                      value={searchOffline}
                      onChange={value =>
                        this.handelChangeInput('searchOffline', value)
                      }
                    >
                      <Option key="0">{ALL}</Option>
                      <Option key="1">{ONLINE}</Option>
                      <Option key="2">{OFFLINE}</Option>
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <label>{SELECT_STATUS_EVENT}</label>
                    <CPSelect
                      value={searchUserEventStatus}
                      onChange={value =>
                        this.handelChangeInput('searchUserEventStatus', value)
                      }
                    >
                      <Option value={0}>{ALL}</Option>
                      <Option value={1}>{WAS_RESPONSE}</Option>
                      <Option value={2}>{WAS_NOT_RESPONSE}</Option>
                      <Option value={3}>{UN_WILLINGNESS}</Option>
                      <Option value={4}>{SYSTEM}</Option>
                      <Option value={5}>{BLOCK}</Option>
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <label>{CITY}</label>
                    <CPSelect
                      value={searchCityId}
                      onChange={value =>
                        this.handelChangeInput('searchCityId', value)
                      }
                    >
                      <Option value={0}>{ALL}</Option>
                      {cityArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-3 col-md-6 col-sm-12">
                    <div className="align-center">
                      <CPSwitch
                        defaultChecked={searchUserStatus}
                        checkedChildren={ACTIVE}
                        unCheckedChildren={DEACTIVATE}
                        onChange={value => {
                          this.handelChangeInput('searchUserStatus', value);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className={s.field}>
                <CPButton
                  size="large"
                  shape="circle"
                  type="primary"
                  icon="search"
                  onClick={this.submitSearch}
                />
              </div>
            </div>
          </CPList>
          {/* <CPPagination */}
          {/* total={userCount} */}
          {/* current={currentPage} */}
          {/* onChange={this.onChangePagination} */}
          {/* onShowSizeChange={this.onShowSizeChange} */}
          {/* defaultPageSize={30} */}
          {/* pageSizeOptions={['10', '20', '30', '50', '80', '100']} */}
          {/* /> */}
        </CPModal>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  sms:
    state.singleSms.data &&
    state.singleSms.data.items &&
    state.singleSms.data.items[0]
      ? state.singleSms.data.items[0]
      : {},
  roles: state.role.data ? state.role.data.items : [],
  users: state.userList.data ? state.userList.data.items : [],
  userCount: state.userList.data ? state.userList.data.count : 0,
  userLoading: state.userList.loading,
  smsStatus: state.smsStatusList.data ? state.smsStatusList.data.items : [],
  cities: state.cityList.data ? state.cityList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      smsPostRequest: smsPostActions.smsPostRequest,
      smsPutRequest: smsPutActions.smsPutRequest,
      userListRequest: userListActions.userListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(SmsInfo));
