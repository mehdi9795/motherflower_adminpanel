import React from 'react';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import SmsInfo from '../SmsInfo/SmsInfo';
import {NOTIFICATION, SMS} from '../../../../Resources/Localization';
import s from './SmsForm.css';

class SmsForm extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    return (
      <Tabs>
        <TabList>
          <Tab>{SMS}</Tab>
        </TabList>
        <TabPanel>
          <SmsInfo />
        </TabPanel>
      </Tabs>
    );
  }
}
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(SmsForm));
