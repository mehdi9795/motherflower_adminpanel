import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HotKeys from 'react-hot-keys';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PushInfo.css';
import CPInput from '../../../CP/CPInput/CPInput';
import CPButton from '../../../CP/CPButton/CPButton';
import {
  DATE,
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  HOUR,
  IMAGE_UPLOAD,
  SMS,
  STATUS,
  SUBJECT,
  TYPE,
} from '../../../../Resources/Localization';
import CPUpload from '../../../CP/CPUpload';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPSelect from '../../../CP/CPSelect';
import CPPersianCalendar from '../../../CP/CPPersianCalendar';
import { PushDto } from '../../../../dtos/notificationDtos';
import { ImageDto } from '../../../../dtos/cmsDtos';
import pushPostActions from '../../../../redux/notification/actionReducer/push/Post';
import pushPutActions from '../../../../redux/notification/actionReducer/push/Put';

const { Option } = Select;

class PushInfo extends React.Component {
  static propTypes = {
    push: PropTypes.objectOf(PropTypes.any),
    pushStatus: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    push: null,
    pushStatus: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      subject: props.push.id ? props.push.subject : '',
      imageUrl:
        props.push.id && props.push.imageDto ? props.push.imageDto.url : '',
      body: props.push.id ? props.push.body : '',
      status: props.push.id ? props.push.statusId.toString() : '',
      date: props.push.id ? props.push.persianDateUtc : '',
      hour: props.push.id ? props.push.hour : '',
      hourArray: [],
      binariesImage: '',
      extention: '',
      entityType: props.push.id ? props.push.entityType : 'Subscription',
      id: props.push.id ? props.push.id : 0,
    };
  }

  componentWillMount() {
    for (let i = 7; i <= 24; i += 1) {
      this.state.hourArray.push(<Option key={`${i}`}>{i}</Option>);
    }
  }

  /**
   * set dto with state for post and put data
   * @returns {{dto: {name: *, description: *, parentCategoryId: *, showOnHomePage: *, published: *}}}
   */
  prepareContainer = () => {
    const {
      subject,
      body,
      status,
      date,
      hour,
      binariesImage,
      extention,
      entityType,
      id,
    } = this.state;
    const { push } = this.props;

    const jsonList = new BaseCRUDDtoBuilder()
      .dto(
        new PushDto({
          entityType: id ? push.entityType : 'Subscription',
          id,
          body,
          subject,
          status: id ? status : 'InActive',
          persianDateUtc: date,
          hour,
          imageDto:
            entityType === 'Subscription' && binariesImage !== ''
              ? new ImageDto({
                  imagetype: 'Push',
                  binaries: binariesImage.split(',')[1],
                  extention,
                })
              : undefined,
        }),
      )
      .build();
    return jsonList;
  };

  /**
   * save data with post and put - redirect to list page
   */
  saveForm = () => {
    const { push } = this.props;
    if (push && push.id) {
      this.putData(this.prepareContainer());
    } else {
      this.postData(this.prepareContainer());
    }
  };

  /**
   * save data with post and put - redirect to edit page
   */
  saveFormAndContinue = () => {
    const { push } = this.props;
    if (push && push.id) {
      this.putData(this.prepareContainer(), true);
    } else {
      this.postData(this.prepareContainer(), true);
    }
  };

  /**
   * call post data for sending data
   * @param containert
   * @returns {Promise<void>}
   */
  async postData(container, saveAndCountinue = false) {
    const data = {
      data: container,
      status: saveAndCountinue ? 'saveAndContinue' : 'save',
      listDto: new BaseGetDtoBuilder().includes(['imageDto']).buildJson(),
    };

    this.props.actions.pushPostRequest(data);
  }

  async putData(container, saveAndCountinue = false) {
    const data = {
      data: container,
      status: saveAndCountinue ? 'saveAndContinue' : 'save',
      listDto: new BaseGetDtoBuilder().includes(['imageDto']).buildJson(),
    };

    this.props.actions.pushPutRequest(data);
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  handleImageChange = value => {
    this.setState({
      binariesImage: value.base64,
      extention: value.type.split('/')[1],
    });
  };

  render() {
    const { pushStatus, push } = this.props;
    const {
      subject,
      imageUrl,
      body,
      status,
      date,
      hour,
      hourArray,
    } = this.state;

    const statusArray = [];
    pushStatus.map(item =>
      statusArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <div>
          <div className="tabContent">
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <CPInput
                  label={SUBJECT}
                  value={subject}
                  onChange={value =>
                    this.handelChangeInput('subject', value.target.value)
                  }
                  autoFocus
                />
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <CPInput
                  label={SMS}
                  value={body}
                  onChange={value =>
                    this.handelChangeInput('body', value.target.value)
                  }
                />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{DATE}</label>
                <CPPersianCalendar
                  placeholder={DATE}
                  onChange={(unix, formatted) =>
                    this.handelChangeInput('date', formatted)
                  }
                  preSelected={date}
                />
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{HOUR}</label>
                <CPSelect
                  value={hour}
                  onChange={value => this.handelChangeInput('hour', value)}
                >
                  {hourArray}
                </CPSelect>
              </div>
            </div>

            {push.id > 0 && (
              <div className="row">
                <div className="col-lg-3 col-sm-6 col-xs-12">
                  <label>{STATUS}</label>
                  <CPSelect
                    value={status}
                    onChange={value => this.handelChangeInput('status', value)}
                  >
                    {statusArray}
                  </CPSelect>
                </div>
                <div className="col-lg-3 col-sm-6 col-xs-12">
                  <label>{TYPE}</label>
                  <CPSelect value={push.entityId} disabled>
                    <Option value={push.entityId}>{push.entityType}</Option>
                  </CPSelect>
                </div>
              </div>
            )}
            <div className="row">
              <div className="col-lg-6 col-sm-6 col-sm-12">
                <label>{IMAGE_UPLOAD}</label>
                <CPUpload
                  onChange={this.handleImageChange}
                  image={`${imageUrl}`}
                  disabled={push.entityType !== 'Subscription' && push.id}
                />
              </div>
            </div>
          </div>
          <div className={s.textLeft}>
            <CPButton
              icon="form"
              className="btn-primary"
              onClick={this.saveFormAndContinue}
            >
              {FORM_SAVE_AND_CONTINUE}
            </CPButton>
            <CPButton icon="save" onClick={this.saveForm}>
              {FORM_SAVE}
            </CPButton>
          </div>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  pushStatus: state.pushStatusList.data ? state.pushStatusList.data.items : [],
  push:
    state.singlePush.data &&
    state.singlePush.data.items &&
    state.singlePush.data.items[0]
      ? state.singlePush.data.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      pushPostRequest: pushPostActions.pushPostRequest,
      pushPutRequest: pushPutActions.pushPutRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PushInfo));
