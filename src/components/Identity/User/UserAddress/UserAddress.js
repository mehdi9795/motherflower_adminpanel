import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select, message } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import CPList from '../../../CP/CPList/CPList';
import s from './UserAddress.css';
import {
  ADDRESS,
  ALL_CITIES,
  ALL_DISTRICT, BANNER_CREATE_IS_SUCCESS,
  CITY,
  DEFINE_THE_EXACT_ADDRESS_ON_THE_MAP,
  DISTRICT,
  ENTER_THE_EXACT_ADDRESS,
  FAX,
  NO,
  PHONE,
  POSTAL_ADDRESS,
  QUESTION_DELETE_USER_ADDRESS,
  RECEIVER_NAME,
  YES,
} from '../../../../Resources/Localization';
import userRoleDeleteActions from '../../../../redux/identity/actionReducer/user/UserRoleDelete';
import CPButton from '../../../CP/CPButton';
import CPModal from '../../../CP/CPModal/CPModal';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPPopConfirm from '../../../CP/CPPopConfirm/CPPopConfirm';
import {
  AddressDto,
  UserDto,
  UserRoleDto,
} from '../../../../dtos/identityDtos';
import vendorBranchUserPostActions from '../../../../redux/vendor/actionReducer/vendorBranchUser/Post';
import CPInput from '../../../CP/CPInput';
import CPInputMask from '../../../CP/CPInputMask';
import { CityDto, DistrictDto } from '../../../../dtos/commonDtos';
import districtOptionListActions from '../../../../redux/common/actionReducer/district/List';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
  BaseListCRUDDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import cityActions from '../../../../redux/common/actionReducer/city/List';
import userAddressActions from '../../../../redux/identity/actionReducer/user/UserAddressList';
import userAddressPostActions from '../../../../redux/identity/actionReducer/user/UserAddressPost';
import userAddressPutActions from '../../../../redux/identity/actionReducer/user/UserAddressPut';
import userAddressDeleteActions from '../../../../redux/identity/actionReducer/user/UserAddressDelete';
import CPIntlPhoneInput from '../../../CP/CPIntlPhoneInput';
import CPValidator from '../../../CP/CPValidator';
import CPMap from '../../../CP/CPMap';
import {
  getCityApi,
  getDistrictApi,
  getNearestLocationsApi,
} from '../../../../services/commonApi';
import { getDtoQueryString } from '../../../../utils/helper';
import { getCookie } from '../../../../utils';
import {
  PostAddressUser,
  PutAddressUser,
} from '../../../../services/identityApi';

const { Option } = Select;

class UserAddress extends React.Component {
  static propTypes = {
    userAddresses: PropTypes.arrayOf(PropTypes.object),
    userAddressCount: PropTypes.number,
    userAddressLoading: PropTypes.bool,
    data: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    cities: PropTypes.arrayOf(PropTypes.object).isRequired,
    districtOptions: PropTypes.objectOf(PropTypes.any),
  };
  static defaultProps = {
    userAddresses: null,
    userAddressCount: 0,
    userAddressLoading: false,
    districtOptions: [],
    data: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      cityId: '0',
      districtId: '0',
      id: 0,
      visibleAddAddressModal: false,
    };
    this.validation = [];
    this.cityChange = this.cityChange.bind(this);
  }

  componentWillMount() {
    /**
     * get all city
     */
    this.props.actions.cityListRequest(
      new BaseGetDtoBuilder()
        .all(true)
        .disabledCount(true)
        .buildJson(),
    );
    this.getAddress();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cities.length > 0 && nextProps.cities !== this.props.cities)
      this.setState({ cityId: nextProps.cities[0].id.toString() }, () =>
        this.cityChange('cityId', this.state.cityId),
      );
  }

  async onDelete(id) {
    const { data } = this.props;
       const listDto = new BaseGetDtoBuilder()
      .dto(
        new AddressDto({
          fromPanel: true,
          userDto: new UserDto({ id: data.id }),
          active: true,
        }),
      )
      .includes(['districtDto'])
      .buildJson();

    const postData = {
      id,
      listDto,
    };

    this.props.actions.userAddressDeleteRequest(postData);
  }

  async PostDataUserAddress() {
    const {
      receiveName,
      phoneNumber,
      cityId,
      districtId,
      content,
      lat,
      lng,
      id,
    } = this.state;
    const { data } = this.props;
    if (this.validation.length === 2) {

      const addressCrud = new BaseCRUDDtoBuilder()
        .dto(
          new AddressDto({
            id,
            content,
            receiverName: receiveName,
            phone: phoneNumber,
            cityId,
            districtId: districtId.split('*')[0],
            lat,
            lng,
            userDto: new UserDto({ id: data.id }),
            fromPanel: true,
          }),
        )
        .build();

      const listDto = new BaseGetDtoBuilder()
        .dto(
          new AddressDto({
            fromPanel: true,
            userDto: new UserDto({ id: data.id }),
            active: true,
          }),
        )
        .includes(['districtDto'])
        .buildJson();

      const data2 = {
        data: addressCrud,
        listDto: listDto,
      };

      if (id === 0) this.props.actions.userAddressPostRequest(data2);
      else  this.props.actions.userAddressPutRequest(data2)

        this.setState({
          phoneNumber: '',
          receiveName: '',
          content: '',
          visibleAddAddressModal: false,
          id: 0,
        });
    } else {
      this.setState({
        errorValid: true,
        messagePhoneNumber: 'لطفا شماره همراه را صحیح وارد کنید.',
      });
    }
  }

  async cityChange(inputName, value, districtId = undefined, lat = undefined, lng = undefined) {
    const token = getCookie('token');
    const response = await getDistrictApi(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new DistrictDto({
              cityDto: new CityDto({ id: value }),
            }),
          )
          .buildJson(),
      ),
    );

    if (response.status === 200) {
      this.props.actions.districtOptionListSuccess(response.data);

      if (response.data.items.length === 0) {
        /**
         * get single city by id
         */
        const responseCity = await getCityApi(
          getDtoQueryString(
            new BaseGetDtoBuilder()
              .dto(
                new CityDto({
                  id: value,
                }),
              )
              .buildJson(),
          ),
        );

        if (responseCity.status === 200) {
          this.setState({
            lat: responseCity.data.items[0].lat,
            lng: responseCity.data.items[0].lng,
          });
        }
      } else {
        this.setState({
          [inputName]: value,
          districtId:
            districtId !== undefined
              ? districtId
              : `${response.data.items[0].id.toString()}*${
                  response.data.items[0].lat
                }*${response.data.items[0].lng}`,
          lat: lat || response.data.items[0].lat,
          lng: lng || response.data.items[0].lng,
        });
      }
    }

    this.setState({
      [inputName]: value,
    });
  }

  getAddress = () => {
    const { data } = this.props;
    this.props.actions.userAddressListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new AddressDto({
            fromPanel: true,
            userDto: new UserDto({ id: data.id }),
            active: true,
          }),
        )
        .includes(['districtDto'])
        .buildJson(),
    );
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
    if (inputName === 'districtId') {
      const position = value.split('*');
      this.setState({
        lat: parseFloat(position[1]),
        lng: parseFloat(position[2]),
      });
    }
  };

  showModalAddAddress = () => {
    const { districtOptions, cities } = this.props;
    this.setState({
      visibleAddAddressModal: true,
      districtId:`${districtOptions[0].id.toString()}*${
        districtOptions[0].lat
            }*${districtOptions[0].lng}`,
      lat: districtOptions[0].lat,
      lng: districtOptions[0].lng,
      cityId: cities[0].id.toString(),
      phoneNumber: '',
      receiveName: '',
      content: '',
      id: 0,
    });
  };

  showEditModal = record => {
    this.cityChange('cityId',record.cityId.toString(),record.districtId);
    setTimeout(() =>
      this.setState({
        visibleAddAddressModal: true,
        districtId: record.districtId,
        cityId: record.cityId.toString(),
        phoneNumber: record.phone,
        receiveName: record.receiverName,
        lat: record.lat,
        lng: record.lng,
        content: record.address,
        id: record.key,
      })
      ,1000);
  }

  handleOkModal = () => {
    this.PostDataUserAddress();
  };

  handleCancelModal = () => {
    this.setState({
      visibleAddAddressModal: false,
      id: 0,
    });
  };

  changeNumber = (status, value, countryCode, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phone: number.replace(/ /g, ''),
        phoneNumber: value,
        showMessagePhone: !status,
        messagePhoneNumber: status ? '' : 'لطفا شماره همراه را صحیح وارد کنید.',
      });
    }
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
  };

  async setLatAndLng(value) {
    const token = getCookie('token');
    this.setState({ lat: value.lat, lng: value.lng });
    const response = await getNearestLocationsApi(
      token,
      getDtoQueryString(
        JSON.stringify({
          dto: { lat: value.lat, lng: value.lng },
          fromCache: true,
        }),
      ),
    );

    if (
      response.status === 200 &&
      response.data.items &&
      response.data.items.length > 0
    ) {
      this.cityChange(
        'cityId',
        response.data.items[0].cityDto.id.toString(),
        `${response.data.items[0].id.toString()}*${
          response.data.items[0].lat
        }*${response.data.items[0].lng}`,
        value.lat,
        value.lng
      );
    } else {
      message.error('محله مورد نظر تحت پوشش نیست', 5);
    }
  }

  render() {
    const {
      userAddresses,
      userAddressCount,
      userAddressLoading,
      cities,
      districtOptions,
    } = this.props;
    const {
      visibleAddAddressModal,
      receiveName,
      phoneNumber,
      cityId,
      districtId,
      content,
      lat,
      lng,
      messagePhoneNumber,
      showMessagePhone,
      errorValid,
    } = this.state;
    const userAddressArray = [];
    const columns = [
      {
        title: ADDRESS,
        dataIndex: 'address',
        key: 'address',
      },
      {
        title: RECEIVER_NAME,
        dataIndex: 'receiverName',
        key: 'receiverName',
      },
      {
        title: PHONE,
        dataIndex: 'phone',
        key: 'phone',
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <span
            onClick={() => this.showEditModal(record)}
            className="edit_action"
          >
            <i className="cp-edit" />
          </span>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={QUESTION_DELETE_USER_ADDRESS}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
    ];
    const citiesDataSource = [];
    const districtDataSource = [];

    /**
     * map userRoles for data source table user role
     */
    if (userAddresses)
      userAddresses.map(item => {
        const obj = {
          key: item.id,
          address: item.content,
          phone: item.phone,
          receiverName: item.receiverName,
          lat: item.lat,
          lng: item.lng,
          districtId: `${item.districtDto.id}*${item.districtDto.lat}*${item.districtDto.lng}`,
          cityId: item.districtDto.cityDto.id,
        };
        userAddressArray.push(obj);

        return userAddressArray;
      });

    /**
     * map cities for comboBox Datasource
     */
    cities.map(item =>
      citiesDataSource.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map district for comboBox Datasource
     */
    districtOptions.map(item =>
      districtDataSource.push(
        <Option key={`${item.id}*${item.lat}*${item.lng}`}>{item.name}</Option>,
      ),
    );
    return (
      <div>
        <CPList
          data={userAddressArray}
          columns={columns}
          count={userAddressCount}
          loading={userAddressLoading}
          onAddClick={this.showModalAddAddress}
          hideSearchBox
        />
        <CPModal
          visible={visibleAddAddressModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
          className="max_modal"
        >
          <div className="row">
            <div className="col-md-6 col-sm-12">
              <h5 className={s.modalTitle}>{ENTER_THE_EXACT_ADDRESS}</h5>
              <div className="row">
                <div className="col-md-6">
                  <CPInput
                    value={phoneNumber}
                    label={PHONE}
                    onChange={value =>
                      this.handelChangeInput('phoneNumber', value.target.value)
                    }
                  />
                </div>
                <div className="col-md-6">
                  <CPValidator
                    value={receiveName}
                    checkValidation={this.checkValidation}
                    minLength={3}
                    showMessage={errorValid}
                    name={RECEIVER_NAME}
                  >
                    <CPInput
                      label={RECEIVER_NAME}
                      value={receiveName}
                      onChange={value =>
                        this.handelChangeInput(
                          'receiveName',
                          value.target.value,
                        )
                      }
                    />
                  </CPValidator>
                </div>
                <div className="col-md-6">
                  <label>{CITY}</label>
                  <CPSelect
                    onChange={value => this.cityChange('cityId', value)}
                    value={cityId}
                    showSearch
                  >
                    {citiesDataSource}
                  </CPSelect>
                </div>
                <div className="col-md-6">
                  <label>{DISTRICT}</label>
                  <CPSelect
                    onChange={value =>
                      this.handelChangeInput('districtId', value)
                    }
                    value={districtId}
                    showSearch
                  >
                    {districtDataSource}
                  </CPSelect>
                </div>
                <div className="col-md-12">
                  <label>{POSTAL_ADDRESS}</label>
                  <CPValidator
                    value={content}
                    checkValidation={this.checkValidation}
                    minLength={10}
                    showMessage={errorValid}
                    name={POSTAL_ADDRESS}
                  >
                    <CPInput
                      value={content}
                      onChange={value =>
                        this.handelChangeInput('content', value.target.value)
                      }
                    />
                  </CPValidator>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-sm-12">
              <h5 className={s.mapTitle}>
                {DEFINE_THE_EXACT_ADDRESS_ON_THE_MAP}
              </h5>
              <CPMap
                containerElement="231px"
                // defaultCenter={{ lat, lng}}
                defaultZoom={15}
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
                mapElement="242px"
                loadingElement="100%"
                draggable
                onDragEnd={value => this.setLatAndLng(value)}
                lat={lat}
                lng={lng}
              />
            </div>
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userAddresses: state.userAddressList.data.items,
  userAddressCount: state.userAddressList.data.count,
  userAddressLoading: state.userAddressList.loading,
  cities: state.cityList.data.items,
  districtOptions: state.districtList.data ? state.districtList.data.items : [],
  data:
    state.singleUser.data &&
    state.singleUser.data.items &&
    state.singleUser.data.items.length > 0
      ? state.singleUser.data.items[0]
      : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      cityListRequest: cityActions.cityListRequest,
      districtOptionListSuccess:
        districtOptionListActions.districtOptionListSuccess,
      userAddressListRequest: userAddressActions.userAddressListRequest,
      userAddressPostRequest: userAddressPostActions.userAddressPostRequest,
      userAddressPutRequest: userAddressPutActions.userAddressPutRequest,
      userAddressDeleteRequest: userAddressDeleteActions.userAddressDeleteRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(UserAddress));
