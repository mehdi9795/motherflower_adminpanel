import React from 'react';
import PropTypes from 'prop-types';
import { Select, message } from 'antd';
import $ from 'jquery';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Event.css';
import {
  SAVE,
  CANCEL,
  YES,
  NO,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT,
  NAME,
  EVENT,
  RELATION,
  DATE,
  DAYS_LEFT,
  EVENT_TYPE,
  PHONE,
  UPDATE_USERNAME,
  CREATE_USERNAME,
  DAY,
  MONTH,
  FARVARDIN,
  ORDIBEHESHT,
  ABAN,
  DEY,
  AZAR,
  MORDAD,
  SHAHRIVAR,
  TIR,
  KHORDAD,
  MEHR,
  BAHMAN,
  ESFAND,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import CPInput from '../../../CP/CPInput';
import CPSelect from '../../../CP/CPSelect';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import userEventListActions from '../../../../redux/identity/actionReducer/user/UserEventList';
import userEventPostActions from '../../../../redux/identity/actionReducer/user/UserEventPost';
import userEventPutActions from '../../../../redux/identity/actionReducer/user/UserEventPut';
import userEventDeleteActions from '../../../../redux/identity/actionReducer/user/UserEventDelete';
import userEventTypeListActions from '../../../../redux/identity/actionReducer/user/UserEventTypeList';
import userRelationShipListActions from '../../../../redux/identity/actionReducer/user/UserRelationShipList';
import CPList from '../../../CP/CPList';
import {
  UserDto,
  UserEvent,
  UserEventType,
  UserRelationShip,
} from '../../../../dtos/identityDtos';
import CPModal from '../../../CP/CPModal';
import CPIntlPhoneInput from '../../../CP/CPIntlPhoneInput';
import { getUserEventApi } from '../../../../services/identityApi';
import { getCookie } from '../../../../utils';
import { getDtoQueryString } from '../../../../utils/helper';
import CPConfirmation from '../../../CP/CPConfirmation';

const { Option } = Select;

class Event extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    userEventTypes: PropTypes.arrayOf(PropTypes.object),
    userEvents: PropTypes.arrayOf(PropTypes.object),
    userRelationShips: PropTypes.arrayOf(PropTypes.object),
    data: PropTypes.objectOf(PropTypes.any),
    userEventLoading: PropTypes.bool,
    userEventCount: PropTypes.number,
  };

  static defaultProps = {
    userEventTypes: [],
    data: [],
    userEvents: [],
    userRelationShips: [],
    userEventLoading: false,
    userEventCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      eventType: '',
      userRelation: '',
      name: '',
      day: 1,
      month: '1',
      id: 0,
      visibleModal: false,
      isValidPhoneNumber: false,
      phoneFinal: '',
      phone: '',
      confirmationContent:
        'شما قبلا این رویداد را انتخاب کرده اید. آیا ادامه می دهید؟',
      visibleConfirmation: false,
    };
    this.onInsert = this.onInsert.bind(this);
  }

  async componentWillMount() {
    this.props.actions.userEventTypeListRequest(JSON.stringify({ dto: {} }));
    this.props.actions.userRelationShipListRequest(
      new BaseGetDtoBuilder()
        .dto({ published: true })
        .orderByFields(['DisplayOrder'])
        .buildJson(),
    );
    this.getUserEvent();
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.userEventTypes.length > 0 &&
      nextProps.userRelationShips.length > 0
    )
      this.setState({
        eventType: nextProps.userEventTypes
          ? nextProps.userEventTypes[0].id.toString()
          : '',
        userRelation: nextProps.userRelationShips
          ? nextProps.userRelationShips[0].id.toString()
          : '',
      });
  }

  async getUserEvent() {
    const { data } = this.props;
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserEvent({
          userDto: new UserDto({ id: data.id }),
          fromPanel: true,
        }),
      )
      .includes(['userEventTypeDto', 'userRelationShipDto'])
      .buildJson();
    this.props.actions.userEventListRequest(jsonList);
  }

  onCancel = () => {
    this.setState({ visibleModal: false });
  };

  onSubmit = () => {
    const {
      eventType,
      userRelation,
      name,
      day,
      month,
      id,
      isValidPhoneNumber,
      phoneFinal,
    } = this.state;
    const { userEventTypes, userRelationShips, data } = this.props;

    // if (!isValidPhoneNumber) {
    //   message.error('لطفا شماره همراه را  درست وارد کنید!', 7);
    //   return;
    // }

    const eventCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserEvent({
          userEventTypeDto: new UserEventType({ id: eventType }),
          userRelationShipDto: new UserRelationShip({ id: userRelation }),
          name,
          eventDay: day,
          eventMonth: month,
          id,
          fromPanel: true,
          phone: phoneFinal,
          userDto: new UserDto({
            id: data.id,
          }),
        }),
      )
      .build();

    const eventList = new BaseGetDtoBuilder()
      .dto(
        new UserEvent({
          userDto: new UserDto({ id: data.id }),
          fromPanel: true,
        }),
      )
      .includes(['userEventTypeDto', 'userRelationShipDto'])
      .buildJson();

    const data2 = {
      data: eventCrud,
      dto: eventList,
    };

    if (id === 0) {
      this.props.actions.userEventPostRequest(data2);
    } else {
      this.props.actions.userEventPutRequest(data2);
    }
    this.setState({
      eventType: userEventTypes ? userEventTypes[0].id.toString() : '',
      userRelation: userRelationShips ? userRelationShips[0].id.toString() : '',
      id: 0,
      name: '',
      day: 1,
      month: '1',
      visibleModal: id === 0,
      isValidPhoneNumber: false,
      phone: '',
      phoneFinal: '',
    });
    // this.onCancel();
  };

  async onInsert() {
    const { data } = this.props;
    const { userRelation, eventType } = this.state;
    const token = getCookie('token');
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserEvent({
          userDto: new UserDto({ id: data.id }),
          fromPanel: true,
          userRelationShipDto: new UserRelationShip({ id: userRelation }),
          userEventTypeDto: new UserEventType({ id: eventType }),
        }),
      )
      .buildJson();

    const response = await getUserEventApi(token, getDtoQueryString(jsonList));
    if (response.status === 200) {
      if (response.data.count === 0) this.onSubmit();
      else
        this.setState({
          visibleConfirmation: true,
        });
    }
  }

  onDelete = id => {
    const { data } = this.props;
    const eventList = new BaseGetDtoBuilder()
      .dto(
        new UserEvent({
          userDto: new UserDto({ id: data.id }),
          fromPanel: true,
        }),
      )
      .includes(['userEventTypeDto', 'userRelationShipDto'])
      .buildJson();

    const data2 = {
      id,
      dto: eventList,
    };

    this.props.actions.userEventDeleteRequest(data2);
  };

  onEdit = record => {
    this.setState({
      name: record.name,
      day: record.eventDay,
      month: record.eventMonth.toString(),
      eventType: record.eventTypeId.toString(),
      userRelation: record.relationShipId.toString(),
      id: record.key,
      visibleModal: true,
      isValidPhoneNumber: true,
      phone: record.phone.replace('+98', '0'),
      phoneFinal: record.phone,
    });
  };

  showForm = () => {
    this.setState({ visibleModal: true });
  };

  handleChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneFinal: number.replace(/ /g, ''),
        phone: value,
        isValidPhoneNumber: status,
      });
    }
  };

  handleOkConfirmation = () => {
    this.onSubmit();
    this.setState({ visibleConfirmation: false });
  };

  handleCancelConfirmation = () => {
    this.setState({ visibleConfirmation: false });
  };

  render() {
    const {
      userRelationShips,
      userEventTypes,
      userEvents,
      userEventLoading,
      userEventCount,
    } = this.props;
    const {
      eventType,
      userRelation,
      name,
      day,
      month,
      visibleModal,
      phone,
      confirmationContent,
      visibleConfirmation,
    } = this.state;

    const userEventTypeDataSource = [];
    const userRelationShipDataSource = [];
    const dayDataSource = [];
    const eventsDataSource = [];

    const columns = [
      {
        title: EVENT,
        dataIndex: 'eventType',
        key: 'eventType',
        width: 200,
      },
      {
        title: RELATION,
        dataIndex: 'relationShip',
        key: 'relationShip',
        width: 250,
      },
      {
        title: DATE,
        dataIndex: 'date',
        key: 'date',
        width: 200,
      },
      {
        title: DAYS_LEFT,
        dataIndex: 'daysLeft',
        key: 'daysLeft',
        width: 200,
      },
      {
        title: NAME,
        dataIndex: 'name',
        key: 'name',
        width: 250,
      },
      {
        title: PHONE,
        dataIndex: 'phone',
        key: 'phone',
        width: 250,
        render: (text, record) => (
          <span>{record.phone && record.phone.replace('+98', '0')}</span>
        ),
      },
      {
        title: CREATE_USERNAME,
        dataIndex: 'userCreated',
        key: 'userCreated',
        width: 250,
      },
      {
        title: UPDATE_USERNAME,
        dataIndex: 'userUpdated',
        key: 'userUpdated',
        width: 250,
      },
      {
        title: '',
        dataIndex: 'Delete',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'Edit',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.onEdit(record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </CPButton>
          </div>
        ),
      },
    ];

    userEvents.map(item => {
      const obj = {
        key: item.id,
        eventType: item.userEventTypeDto.title,
        relationShip: item.userRelationShipDto.title,
        name: item.name,
        date: item.persinaDayMonth,
        daysLeft: item.leftOverDay,
        eventDay: item.eventDay,
        eventMonth: item.eventMonth,
        eventTypeId: item.userEventTypeDto.id,
        relationShipId: item.userRelationShipDto.id,
        phone: item.phone,
        userCreated: item.createdUerDto.displayName,
        userUpdated: item.updatedUerDto.displayName,
      };
      eventsDataSource.push(obj);
      return null;
    });

    /**
     * map district for comboBox Datasource
     */
    userRelationShips.map(item =>
      userRelationShipDataSource.push(
        <Option key={item.id.toString()}>{item.title}</Option>,
      ),
    );

    /**
     * map cities for comboBox Datasource
     */
    userEventTypes.map(item =>
      userEventTypeDataSource.push(
        <Option key={item.id.toString()}>{item.title}</Option>,
      ),
    );

    for (let item = 1; item <= 31; item += 1) {
      dayDataSource.push(
        <Option key={item.toString()}>{item.toString()}</Option>,
      );
    }

    return (
      <div>
        <CPList
          data={eventsDataSource}
          columns={columns}
          onAddClick={this.showForm}
          hideSearchBox
          loading={userEventLoading}
          count={userEventCount}
        />
        <CPModal
          visible={visibleModal}
          handleOk={this.onInsert}
          handleCancel={this.onCancel}
          title={EVENT}
        >
          <div className="row eventForm">
            <div className="col-sm-12 col-lg-6">
              <label>{EVENT_TYPE}</label>
              <CPSelect
                value={eventType}
                onChange={value => this.handleChangeInput('eventType', value)}
                className={s.comboBox}
              >
                {userEventTypeDataSource}
              </CPSelect>
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{RELATION}</label>
              <CPSelect
                value={userRelation}
                onChange={value =>
                  this.handleChangeInput('userRelation', value)
                }
                className={s.comboBox}
              >
                {userRelationShipDataSource}
              </CPSelect>
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{NAME}</label>
              <CPInput
                value={name}
                onChange={value =>
                  this.handleChangeInput('name', value.target.value)
                }
                className={s.inputName}
              />
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{PHONE}</label>
              <CPIntlPhoneInput
                placeholder=""
                value={phone}
                onChange={this.changeNumber}
              />
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{DAY}</label>
              <CPSelect
                value={day}
                onChange={value => this.handleChangeInput('day', value)}
                className={s.comboBox}
                showSearch
              >
                {dayDataSource}
              </CPSelect>
            </div>
            <div className="col-sm-12 col-lg-6">
              <label>{MONTH}</label>
              <CPSelect
                value={month}
                onChange={value => this.handleChangeInput('month', value)}
                className={s.comboBox}
                showSearch
              >
                <Option value="1" key="1">
                  {FARVARDIN}
                </Option>
                <Option value="2" key="2">
                  {ORDIBEHESHT}
                </Option>
                <Option value="3" key="3">
                  {KHORDAD}
                </Option>
                <Option value="4" key="4">
                  {TIR}
                </Option>
                <Option value="5" key="5">
                  {MORDAD}
                </Option>
                <Option value="6" key="6">
                  {SHAHRIVAR}
                </Option>
                <Option value="7" key="7">
                  {MEHR}
                </Option>
                <Option value="8" key="8">
                  {ABAN}
                </Option>
                <Option value="9" key="9">
                  {AZAR}
                </Option>
                <Option value="10" key="10">
                  {DEY}
                </Option>
                <Option value="11" key="11">
                  {BAHMAN}
                </Option>
                <Option value="12" key="12">
                  {ESFAND}
                </Option>
              </CPSelect>
            </div>
          </div>
        </CPModal>
        <CPConfirmation
          visible={visibleConfirmation}
          handleOk={this.handleOkConfirmation}
          handleCancel={this.handleCancelConfirmation}
          content={confirmationContent}
          zIndex={999999}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userEvents: state.userEventList.data ? state.userEventList.data.items : [],
  userEventCount: state.userEventList.data ? state.userEventList.data.count : 0,
  userEventLoading: state.userEventList.loading,
  userEventTypes: state.userEventTypeList.data
    ? state.userEventTypeList.data.items
    : [],
  userRelationShips: state.userRelationShipList.data
    ? state.userRelationShipList.data.items
    : [],
  data:
    state.singleUser.data &&
    state.singleUser.data.items &&
    state.singleUser.data.items.length > 0
      ? state.singleUser.data.items[0]
      : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      userEventPostRequest: userEventPostActions.userEventPostRequest,
      userEventPutRequest: userEventPutActions.userEventPutRequest,
      userEventDeleteRequest: userEventDeleteActions.userEventDeleteRequest,
      userEventTypeListRequest:
        userEventTypeListActions.userEventTypeListRequest,
      userRelationShipListRequest:
        userRelationShipListActions.userRelationShipListRequest,
      userEventListRequest: userEventListActions.userEventListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Event));
