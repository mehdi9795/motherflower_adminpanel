import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select, message } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import CPList from '../../../CP/CPList/CPList';
import s from './UserCardNumber.css';
import {
  ACCOUNT_OWNER_NAME_AND_FAMILY,
  CANCEL,
  ALL_CITIES,
  ALL_DISTRICT,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_CARD_NUMBER,
  BANNER_CREATE_IS_SUCCESS,
  CARD_NUMBER,
  CHOOSE_BANK,
  CITY,
  DEFINE_THE_EXACT_ADDRESS_ON_THE_MAP,
  DISTRICT,
  ENTER_THE_EXACT_ADDRESS,
  FAX,
  NO,
  OPTIONAL,
  PHONE,
  POSTAL_ADDRESS,
  QUESTION_DELETE_USER_ADDRESS,
  RECEIVER_NAME,
  SAVE,
  SHEBA_NUMBER,
  YES,
} from '../../../../Resources/Localization';
import userCardNumberListActions from '../../../../redux/identity/actionReducer/user/UserCardNumberList';
import bankListActions from '../../../../redux/sam/actionReducer/bank/List';
import CPButton from '../../../CP/CPButton';
import CPModal from '../../../CP/CPModal/CPModal';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPPopConfirm from '../../../CP/CPPopConfirm/CPPopConfirm';
import { AddressDto, UserDto } from '../../../../dtos/identityDtos';
import CPInput from '../../../CP/CPInput';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPValidator from '../../../CP/CPValidator';
import CPMap from '../../../CP/CPMap';
import { BankDto, UserBankHistoryDto } from '../../../../dtos/samDtos';
import {
  USER_CARD_NUMBER_DELETE_REQUEST,
  USER_CARD_NUMBER_POST_REQUEST,
  USER_CARD_NUMBER_PUT_REQUEST,
} from '../../../../constants';

const { Option } = Select;

class UserCardNumber extends React.Component {
  static propTypes = {
    userCardNumbers: PropTypes.arrayOf(PropTypes.object),
    userCardNumberCount: PropTypes.number,
    userCardNumberLoading: PropTypes.bool,
    user: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    banks: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    banks: [],
    userCardNumbers: null,
    userCardNumberCount: 0,
    userCardNumberLoading: false,
    user: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      visibleAddCardNumberModal: false,
      bankId: props.banks.length > 0 ? props.banks[0].id : 1,
    };
    this.validation = [];
  }

  componentWillMount() {
    this.getCardNumber();
    this.props.actions.bankListRequest(
      new BaseGetDtoBuilder()
        .dto(
          new BankDto({
            usedForProfile: true,
          }),
        )
        .includes(['bankDto'])
        .buildJson(),
    );
  }

  async onDelete(id) {
    const { user } = this.props;
    const bankAccountList = new BaseGetDtoBuilder()
      .dto(new BankDto({ userDto: new UserDto({ id: user.id }) }))
      .includes(['bankDto'])
      .buildJson();

    const data = {
      id,
      listDto: bankAccountList,
    };

    this.props.userCardNumberDeleteRequest(data);
  }

  getCardNumber = () => {
    const { user } = this.props;

    const bankAccountList = new BaseGetDtoBuilder()
      .dto(new BankDto({ userDto: new UserDto({ id: user.id }) }))
      .buildJson();

    this.props.actions.userCardNumberListRequest(bankAccountList);
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  showModalAddCardNumber = () => {
    this.setState({
      visibleAddCardNumberModal: true,
      name: '',
      sheba: '',
      numberCard: '',
      id: 0,
      bankId: this.props.banks.length > 0 ? this.props.banks[0].id : 1,
    });
  };

  showEditModal = record => {
    setTimeout(
      () =>
        this.setState({
          visibleAddCardNumberModal: true,
          id: record.key,
          name: record.name,
          sheba: record.shabaNo,
          numberCard: record.cardNo,
          bankId: record.bankId,
        }),
      1000,
    );
  };

  handleOkModal = () => {
    const { bankId, name, sheba, numberCard, id } = this.state;
    const { user } = this.props;

    if (numberCard.length === 0) {
      message.error('شماره کارت الزامی است', 5);
      return;
    }
    if (numberCard.length !== 16) {
      message.error('شماره کارت 16 رقمی می باشد.', 5);
      return;
    }
    if (sheba.length !== 0 && sheba.length !== 24) {
      message.error('شماره شبا 24 رقمی می باشد.', 5);
      return;
    }
    if (name.length === 0) {
      message.error('نام و نام خانوادگی الزامی است.', 5);
      return;
    }

    const userBankHistoryCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserBankHistoryDto({
          bankDto: new BankDto({ id: bankId }),
          id,
          name,
          shabaNo: sheba,
          cardNo: numberCard,
          userDto: new UserDto({ id: user.id }),
        }),
      )
      .build();

    const bankAccountList = new BaseGetDtoBuilder()
      .dto(new BankDto({ userDto: new UserDto({ id: user.id }) }))
      .includes(['bankDto'])
      .buildJson();

    const data = {
      data: userBankHistoryCrud,
      listDto: bankAccountList,
    };

    if (id === 0) this.props.userCardNumberPostRequest(data);
    else this.props.userCardNumberPutRequest(data);

    this.setState({
      visibleAddCardNumberModal: false,
    });
  };

  handleCancelModal = () => {
    this.setState({
      visibleAddCardNumberModal: false,
      id: 0,
    });
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
  };

  render() {
    const {
      userCardNumbers,
      userCardNumberCount,
      userCardNumberLoading,
      banks,
    } = this.props;
    const { visibleAddCardNumberModal, numberCard, bankId } = this.state;
    const userCardNumberArray = [];
    const columns = [
      {
        title: 'نام دارنده حساب',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'بانک',
        dataIndex: 'bank',
        key: 'bank',
      },
      {
        title: 'شماره شبا',
        dataIndex: 'shabaNo',
        key: 'shabaNo',
      },
      {
        title: 'شماره کارت',
        dataIndex: 'cardNo',
        key: 'cardNo',
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <span
            onClick={() => this.showEditModal(record)}
            className="edit_action"
          >
            <i className="cp-edit" />
          </span>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_CARD_NUMBER}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
    ];

    /**
     * map userRoles for data source table user role
     */
    if (userCardNumbers)
      userCardNumbers.map(item => {
        const obj = {
          key: item.id,
          name: item.name,
          shabaNo: item.shabaNo,
          cardNo: item.cardNo,
          bankId: item.bankDto.id,
          bank: item.bankDto.name,
        };
        userCardNumberArray.push(obj);

        return userCardNumberArray;
      });

    const bankArray = [];
    banks.map(item =>
      bankArray.push(
        <Option value={item.id} key={item.id}>
          {item.name}
        </Option>,
      ),
    );

    return (
      <div>
        <CPList
          data={userCardNumberArray}
          columns={columns}
          count={userCardNumberCount}
          loading={userCardNumberLoading}
          onAddClick={this.showModalAddCardNumber}
          hideSearchBox
        />
        <CPModal
          visible={visibleAddCardNumberModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
        >
          <div className="row">
            <div className="col-sm-12">
              <label>{CARD_NUMBER}</label>
              <CPInput
                onChange={value =>
                  this.handelChangeInput('numberCard', value.target.value)
                }
                value={numberCard}
                maxLength="16"
                name="code1"
                autoFocus
                type="number"
              />
            </div>
            <div className="col-sm-12">
              <label>
                {SHEBA_NUMBER} <b>{OPTIONAL}</b>
              </label>
              <CPInput
                onChange={value =>
                  this.handelChangeInput('sheba', value.target.value)
                }
                value={this.state.sheba}
                name="sheba"
                maxLength="24"
                type="number"
              />
            </div>
            <div className="col-sm-6">
              <label>{ACCOUNT_OWNER_NAME_AND_FAMILY}</label>
              <CPInput
                onChange={value =>
                  this.handelChangeInput('name', value.target.value)
                }
                value={this.state.name}
              />
            </div>
            <div className="col-sm-6">
              <label>{CHOOSE_BANK}</label>
              <CPSelect
                onChange={value => this.handelChangeInput('bankId', value)}
                value={bankId}
              >
                {bankArray}
              </CPSelect>
            </div>
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userCardNumbers: state.userCardNumberList.data
    ? state.userCardNumberList.data.items
    : [],
  userCardNumberCount: state.userCardNumberList.data
    ? state.userCardNumberList.data.count
    : 0,
  userCardNumberLoading: state.userCardNumberList.loading,
  banks: state.bankList.data ? state.bankList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      userCardNumberListRequest:
        userCardNumberListActions.userCardNumberListRequest,
      bankListRequest: bankListActions.bankListRequest,
    },
    dispatch,
  ),
  dispatch,
  userCardNumberPostRequest: dto =>
    dispatch({ type: USER_CARD_NUMBER_POST_REQUEST, dto }),
  userCardNumberPutRequest: dto =>
    dispatch({ type: USER_CARD_NUMBER_PUT_REQUEST, dto }),
  userCardNumberDeleteRequest: dto =>
    dispatch({ type: USER_CARD_NUMBER_DELETE_REQUEST, dto }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(UserCardNumber));
