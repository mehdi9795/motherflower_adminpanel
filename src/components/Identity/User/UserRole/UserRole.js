import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import CPList from '../../../CP/CPList/CPList';
import s from './UserRole.css';
import {
  DESCRIPTION,
  NAME,
  NO,
  QUESTION_DELETE_USER_ROLE,
  USER_ROLE,
  YES,
} from '../../../../Resources/Localization';
import userRolePostActions from '../../../../redux/identity/actionReducer/user/UserRolePost';
import userRoleDeleteActions from '../../../../redux/identity/actionReducer/user/UserRoleDelete';
import userRoleListActions from '../../../../redux/identity/actionReducer/user/UserRoleList';
import CPButton from '../../../CP/CPButton';
import CPModal from '../../../CP/CPModal/CPModal';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPPopConfirm from '../../../CP/CPPopConfirm/CPPopConfirm';
import { RoleDto, UserDto, UserRoleDto } from '../../../../dtos/identityDtos';
import roleActions from '../../../../redux/identity/actionReducer/role/Role';
import {
  VendorBranchDto,
  VendorBranchUserDto,
} from '../../../../dtos/vendorDtos';
import { postUserRoleApi } from '../../../../services/identityApi';
import vendorBranchUserPostActions from '../../../../redux/vendor/actionReducer/vendorBranchUser/Post';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
  BaseListCRUDDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class UserRole extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    userRoles: PropTypes.arrayOf(PropTypes.object),
    roles: PropTypes.arrayOf(PropTypes.object),
    userRoleCount: PropTypes.number,
    userRolesLoading: PropTypes.bool,
    vendors: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    data: null,
    vendors: [],
    userRoles: null,
    roles: null,
    userRoleCount: 0,
    userRolesLoading: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      roleId: '',
      visibleAddRoleModal: false,
    };
    this.PostDataUserRole = this.PostDataUserRole.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  async onDelete(id) {
    const { data } = this.props;

    /**
     * get all user Role with user id
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserRoleDto({
          userDto: new UserDto({ id: data.id }),
        }),
      )
      .buildJson();

    const postData = {
      data: JSON.stringify([new UserRoleDto({ id })]),
      listDto: jsonList,
    };

    this.props.actions.userRoleDeleteRequest(postData);
  }

  async PostDataUserRole() {
    const { data, vendorBranchId, vendors } = this.props;
    const { roleId } = this.state;

    const jsoncrud = new BaseListCRUDDtoBuilder()
      .dtos([
        new UserRoleDto({
          userDto: new UserDto({ id: data.id }),
          roleDto: new RoleDto({ id: roleId }),
        }),
      ])
      .build();

    /**
     * get all user Role with user id
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserRoleDto({
          userDto: new UserDto({ id: data.id }),
        }),
      )
      .buildJson();

    const userRolePost = await postUserRoleApi(this.props.token, jsoncrud);

    if (userRolePost.status === 200) {
      if (vendorBranchId) {
        const jsonCrudVendorbranchUser = new BaseCRUDDtoBuilder()
          .dto(
            new VendorBranchUserDto({
              roleDtos: [new RoleDto({ id: parseInt(roleId, 0) })],
              userDto: new UserDto({ id: data.id }),
              vendorBranchDto: new VendorBranchDto({
                id: parseInt(vendorBranchId, 0),
              }),
            }),
          )
          .fromIdentity(true)
          .build();
        const postData2 = {
          data: jsonCrudVendorbranchUser,
          status: 'fromIdentity',
          listDto: jsonList,
        };
        this.props.actions.vendorBranchUserPostRequest(postData2);
        document.getElementById('backUrlEditCard').click();
      } else if (data.vendorId) {
        const vendorBranches = vendors.find(
          vendor => vendor.id === data.vendorId,
        ).vendorBranchDtos;

        vendorBranches.map(item => {
          const jsonCrudVendorbranchUser = new BaseCRUDDtoBuilder()
            .dto(
              new VendorBranchUserDto({
                roleDtos: [new RoleDto({ id: parseInt(roleId, 0) })],
                userDto: new UserDto({ id: data.id }),
                vendorBranchDto: new VendorBranchDto({
                  id: parseInt(item.id, 0),
                }),
              }),
            )
            .fromIdentity(true)
            .build();

          const postData2 = {
            data: jsonCrudVendorbranchUser,
            status: 'fromIdentity',
            listDto: jsonList,
          };
          this.props.actions.vendorBranchUserPostRequest(postData2);
          return null;
        });
      } else {
        this.props.actions.userRoleRequest(jsonList);
      }
    } else {
      this.props.actions.userRolePostFailure();
    }
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  showModalAddRole = () => {
    const { userRoles } = this.props;
    const userRoleIds = [];

    userRoles.map(item => userRoleIds.push(item.roleDto.id));
    /**
     * get all role Except user role Id
     */
    this.props.actions.roleListRequest(
      new BaseGetDtoBuilder()
        .all(true)
        .notExpectedIds(userRoleIds)
        .disabledCount(true)
        .buildJson(),
    );

    this.setState({
      visibleAddRoleModal: true,
    });
  };

  handleOkModal = () => {
    this.setState({
      visibleAddRoleModal: false,
    });
    this.PostDataUserRole();
  };

  handleCancelModal = () => {
    this.setState({
      visibleAddRoleModal: false,
    });
  };

  render() {
    const {
      userRoles,
      userRoleCount,
      userRolesLoading,
      roles,
      data,
      vendorBranchId,
    } = this.props;
    const { roleId, visibleAddRoleModal } = this.state;
    const userRoleArray = [];
    const roleArray = [];
    const columns = [
      {
        title: NAME,
        dataIndex: 'name',
        key: 'fromDateTimeUtc',
      },
      {
        title: DESCRIPTION,
        dataIndex: 'description',
        key: 'toDateTimeUtc',
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={QUESTION_DELETE_USER_ROLE}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="cp-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
    ];

    /**
     * map userRoles for data source table user role
     */
    if (userRoles)
      userRoles.map(item => {
        if (!item.roleDto.isDefault) {
          const obj = {
            key: item.id,
            name: item.roleDto.name,
            description: '',
          };
          userRoleArray.push(obj);
        }
        return userRoleArray;
      });
    /**
     * map role for filling combobox Add role
     */
    if (roles)
      roles.map(item => {
        if (vendorBranchId) {
          if (
            item.roleStatus === 'Driver' ||
            item.roleStatus === 'Seller'
            // item.roleStatus === 'VendorAdmin'
          )
            roleArray.push(<Option key={item.id}>{item.name}</Option>);
        } else if (data.vendorId) {
          if (
            // item.roleStatus === 'Driver' ||
          // item.roleStatus === 'Seller' ||
            item.roleStatus === 'VendorAdmin'
          )
            roleArray.push(<Option key={item.id}>{item.name}</Option>);
        } else if (!data.vendorId) {
          if (
            item.roleStatus !== 'Driver' &&
            item.roleStatus !== 'Seller' &&
            item.roleStatus !== 'VendorAdmin'
          )
            roleArray.push(<Option key={item.id}>{item.name}</Option>);
        }
        return null;
      });

    return (
      <div>
        <CPList
          data={userRoleArray}
          columns={columns}
          count={userRoleCount}
          loading={userRolesLoading}
          onAddClick={this.showModalAddRole}
          hideSearchBox
        />
        <CPModal
          visible={visibleAddRoleModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
          title={USER_ROLE}
        >
          <div className="modalContent">
            <div className="row">
              <div className="col-sm-12">
                <label>{USER_ROLE}</label>
                <CPSelect
                  name="roleId"
                  defaultValue={roleId}
                  onChange={value => {
                    this.handelChangeInput('roleId', value);
                  }}
                >
                  {roleArray}
                </CPSelect>
              </div>
            </div>
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userRoles: state.userRoleList.data ? state.userRoleList.data.items : [],
  userRoleCount: state.userRoleList.data.count,
  userRolesLoading: state.userRoleList.loading,
  roles: state.role.data.items,
  data: state.singleUser.data ? state.singleUser.data.items[0] : null,
  token: state.login.data.token,
  vendors: state.vendorList.data ? state.vendorList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      userRolePostFailure: userRolePostActions.userRolePostFailure,
      userRoleDeleteRequest: userRoleDeleteActions.userRoleDeleteRequest,
      roleListRequest: roleActions.roleListRequest,
      userRoleRequest: userRoleListActions.userRoleRequest,
      vendorBranchUserPostRequest:
        vendorBranchUserPostActions.vendorBranchUserPostRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(UserRole));
