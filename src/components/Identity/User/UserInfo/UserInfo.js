import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HotKeys from 'react-hot-keys';
import { bindActionCreators } from 'redux';
import { message, Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserInfo.css';
import {
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  EMAIL,
  USER_FIRST_NAME,
  USER_LAST_NAME,
  CHANGE_PASSWORD,
  PHONE,
  BIRTH_DATE,
  AGENT_NAME,
  NO_AGENT,
  GENDER,
  NEWSLETTER,
  STATUS,
  CHANGE_PASSWORD_IS_SUCCESS,
  CHANGE_PASSWORD_IS_FAILURE,
  PASSWORD,
  ACTIVE,
  DEACTIVATE,
  NAME,
  FROM_DATE,
  TO_DATE,
  SENDING_CODE_DISCOUNT,
  ARE_YOU_SURE_WANT_TO_SEND_DISCOUNT_CODE,
  VERIFY,
  CITY,
  SELECT_STATUS_EVENT,
  WAS_RESPONSE,
  WAS_NOT_RESPONSE,
  UN_WILLINGNESS,
  ENTER_EXPIRE_DATE,
  MONTH_OF_BIRTHDAY,
  DAY_OF_BIRTHDAY,
  REAGENT_USER,
  FARVARDIN,
  ORDIBEHESHT,
  ABAN,
  DEY,
  AZAR,
  MORDAD,
  SHAHRIVAR,
  TIR,
  KHORDAD,
  MEHR,
  BAHMAN,
  ESFAND,
  BLOCK,
  SYSTEM,
  OFFLINE,
  ONLINE,
  LOGIN_HOUR,
  WALLET,
} from '../../../../Resources/Localization';
import userPostActions from '../../../../redux/identity/actionReducer/user/UserPost';
import userPutActions from '../../../../redux/identity/actionReducer/user/UserPut';
import CPButton from '../../../CP/CPButton/CPButton';
import CPPersianCalendar from '../../../CP/CPPersianCalendar/CPPersianCalendar';
import CPInput from '../../../CP/CPInput/CPInput';
import history from '../../../../history';
import CPSelect from '../../../CP/CPSelect/CPSelect';
import CPRadio from '../../../CP/CPRadio/CPRadio';
import CPInputNumber from '../../../CP/CPInputNumber/CPInputNumber';
import { RoleDto, UserDto, UserRoleDto } from '../../../../dtos/identityDtos';
import CPSwitch from '../../../CP/CPSwitch';
import CPIntlPhoneInput from '../../../CP/CPIntlPhoneInput';
import { getCookie } from '../../../../utils';
import { ChangePasswordPutAuthApi } from '../../../../services/identityApi';
import CPModal from '../../../CP/CPModal';
import CPList from '../../../CP/CPList';
import {
  PromotionDto,
  SendDiscountCodeGroupDto,
  VendorBranchPromotionDto,
  WalletDto,
} from '../../../../dtos/samDtos';
import promotionListActions from '../../../../redux/sam/actionReducer/promotion/PromotionList';
import walletPostActions from '../../../../redux/sam/actionReducer/wallet/Post';
import CPConfirmation from '../../../CP/CPConfirmation';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import { CityDto } from '../../../../dtos/commonDtos';
import { postSendToUserApi } from '../../../../services/samApi';
import { showNotification } from '../../../../utils/helper';
import Link from '../../../Link';
import CPValidator from '../../../CP/CPValidator/CPValidator';
import { getDtoQueryString } from '../../../../utils/helper';

const { Option } = Select;

class UserInfo extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    vendors: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    userRoles: PropTypes.arrayOf(PropTypes.object),
    vendorBranchId: PropTypes.string,
    vendorId: PropTypes.string,
    promotionLoading: PropTypes.bool,
    promotionCount: PropTypes.number,
    promotions: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    userWallet: PropTypes.objectOf(PropTypes.any),
  };
  static defaultProps = {
    data: null,
    vendorBranchId: '',
    vendorId: '',
    userRoles: [],
    vendors: [],
    promotions: [],
    cities: [],
    userWallet: {},
    promotionLoading: false,
    promotionCount: 0,
  };
  constructor(props) {
    super(props);
    const { data } = this.props;

    this.state = {
      firstName: data ? data.firstName : '',
      lastName: data ? data.lastName : '',
      email: data && data.email && data.email.length > 0 ? data.email : '',
      phone: data ? data.phone.replace('+98', '0') : '',
      phoneFinal: data ? data.phone : '',
      // dateOfBirth: data ? data.persianDateOfBirth : '',
      monthOfBirthDate: data ? data.monthOfBirthDate.toString() : '0',
      dayOfBirthDate: data ? data.dayOfBirthDate.toString() : '0',
      active: data ? data.active : true,
      gender: data ? data.genderType : 'M',
      password: data ? data.password : '',
      newsLetter: data ? data.newsLetter : false,
      verify: data ? data.verify : false,
      cityId: data && data.cityDto ? data.cityDto.id.toString() : '0',
      vendorId: data
        ? data.vendorId.toString()
        : props.vendorId !== undefined && props.vendorId !== ''
          ? props.vendorId
          : '0',
      disabledVendorID:
        !!(
          data &&
          data.vendorId !== undefined &&
          data.vendorId !== '' &&
          data.vendorId > 0
        ) || !!(props.vendorBranchId > 0),
      visibleConfirm: false,
      countryCode: '',
      imageUrl: data && data.imageDto ? data.imageDto.url : '',
      offline: data ? data.offline : true,
      promotionId: 0,
      isValidPhoneNumber: !!(data && data.phone),
      showMessagePhone: 'none',
      messagePhoneNumber: 'لطفا شماره همراه را صحیح وارد کنید.',
      userEventStatus: data ? data.userEventStatusId : 4,
      persianToDateExpire: '',
      visibleDiscountCodeGroupModal: false,
      promotionTypeId: 0,
      modalWalletVisible: false,
      wallet: false,
    };
    this.postData = this.postData.bind(this);
    this.changePassword = this.changePassword.bind(this);
    this.handleOkConfirm = this.handleOkConfirm.bind(this);

    this.validation = [];
  }

  componentWillReceiveProps(nextProps) {
    const { vendorId } = this.state;
    nextProps.userRoles.map(item => {
      if (
        item.roleDto.roleStatus === 'Driver' ||
        item.roleDto.roleStatus === 'Seller'
      )
        this.setState({
          vendorId: nextProps.vendorId || vendorId,
          disabledVendorID: true,
        });
      return null;
    });
  }

  onChangeInpute = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  async postData(container, saveAndCountinue = false) {
    const { vendorBranchId } = this.props;
    /**
     * get all users
     */

    const jsonListUser = new BaseGetDtoBuilder()
      .dto(
        new UserDto({
          userRoleDto: [new UserRoleDto({ roleDto: new RoleDto({ id: 1 }) })],
          active: true,
        }),
      )
      .includes(['Roles'])
      .buildJson();
    let data = {};

    if (this.props.vendorId) {
      data = {
        data: container,
        listDto: '',
        status: 'backToVendorBranch',
        vendorBranchId,
      };
    } else {
      data = {
        data: container,
        listDto: jsonListUser,
        status: saveAndCountinue ? 'saveAndContinue' : 'save',
        vendorBranchId,
      };
    }
    this.props.actions.userPostRequest(data);
  }
  async putData(container, saveAndCountinue = false) {
    /**
     * get all users
     */

    const jsonListUser = new BaseGetDtoBuilder()
      .dto(
        new UserDto({
          userRoleDto: [new UserRoleDto({ roleDto: new RoleDto({ id: 1 }) })],
          active: true,
        }),
      )
      .includes(['Roles'])
      .buildJson();

    const data = {
      data: container,
      listDto: jsonListUser,
      status: saveAndCountinue ? 'saveAndContinue' : 'save',
    };
    this.props.actions.userPutRequest(data);
  }

  prepareContainer = () => {
    const {
      firstName,
      lastName,
      email,
      phoneFinal,
      dateOfBirth,
      monthOfBirthDate,
      dayOfBirthDate,
      active,
      gender,
      newsLetter,
      vendorId,
      password,
      verify,
      countryCode,
      cityId,
      userEventStatus,
      offline,
    } = this.state;
    const { data } = this.props;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserDto({
          firstName,
          lastName,
          email,
          phone: phoneFinal,
          // persianDateOfBirth: dateOfBirth,
          monthOfBirthDate,
          dayOfBirthDate,
          active,
          genderType: gender,
          newsLetter,
          vendorId,
          cityDto: new CityDto({ id: cityId }),
          password: data && data.id ? undefined : password,
          id: data ? data.id : 0,
          verify,
          countryCode,
          userEventStatus,
          offline,
        }),
      )
      .build();

    return jsonCrud;
  };
  saveForm = () => {
    const { data } = this.props;
    const { isValidPhoneNumber, email } = this.state;

    if (email.length > 0 && this.validation.length !== 1) {
      message.error('لطفا ایمیل را صحیح وارد کنید!', 5);
      return;
    }

    if (isValidPhoneNumber) {
      if (data && data.id) {
        this.putData(this.prepareContainer());
      } else {
        this.postData(this.prepareContainer());
      }
    } else
      this.setState({
        showMessage: true,
        showMessagePhone: this.state.messagePhoneNumber ? 'block' : 'none',
      });
  };

  saveFormAndContinue = () => {
    const { data } = this.props;
    const { isValidPhoneNumber, email } = this.state;

    if (email.length > 0 && this.validation.length !== 1) {
      message.error('لطفا ایمیل را صحیح وارد کنید!', 5);
      return;
    }

    if (isValidPhoneNumber) {
      if (data && data.id) {
        this.putData(this.prepareContainer(), true);
      } else {
        this.postData(this.prepareContainer(), true);
      }
    } else
      this.setState({
        showMessage: true,
        showMessagePhone: this.state.messagePhoneNumber ? 'block' : 'none',
      });
  };

  redirect = id => {
    history.push(`/user/edit/${id}`);
  };

  vendorChange = value => {
    this.setState({
      vendorId: value,
    });
  };

  async changePassword() {
    const { id } = this.props.data;
    const { password } = this.state;
    const token = getCookie('token');
    let response;
    try {
      response = await ChangePasswordPutAuthApi(token, {
        dto: { id, password },
      });
    } catch (error) {
      message.error(CHANGE_PASSWORD_IS_FAILURE, 20);
    }

    if (response.status === 200 && response.data.isSuccess) {
      message.success(CHANGE_PASSWORD_IS_SUCCESS, 10);
      // this.redirect(response.id);
    } else {
      message.error(CHANGE_PASSWORD_IS_FAILURE, 20);
    }
  }

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneFinal: number.replace(/ /g, ''),
        phone: value,
        countryCode: countryData.dialCode,
        isValidPhoneNumber: status,
        messagePhoneNumber: status ? '' : 'لطفا شماره همراه را صحیح وارد کنید.',
      });
    }
  };

  handleCancelModal = () => {
    this.setState({ visibleModal: false });
  };

  showModalDiscount = () => {
    const jsonCrud = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchPromotionDto({
          active: true,
          searchPromotionTypes: [3, 6, 7],
        }),
      )
      .buildJson();

    this.props.actions.promotionListRequest(jsonCrud);

    this.setState({ visibleModal: true });
  };

  async handleOkConfirm() {
    const { promotionId, promotionTypeId, persianToDateExpire } = this.state;
    const { data } = this.props;
    const token = getCookie('token');
    const response = await postSendToUserApi(
      token,
      new SendDiscountCodeGroupDto({
        userId: data.id,
        promotionDto: new PromotionDto({
          id: promotionId,
          promotionType: promotionTypeId,
          persianToDateUtc:
            persianToDateExpire.length > 0 ? persianToDateExpire : undefined,
        }),
      }),
    );

    this.setState({
      visibleConfirm: false,
      visibleDiscountCodeGroupModal: false,
    });
    if (response.status === 200)
      message.success('کد تخفیف با موفقیت ارسال شد', 5);
    else message.error(response.data.errorMessage, 5);
  }

  handleCancelConfirm = () => {
    this.setState({ visibleConfirm: false });
  };

  showModalConfirm = (promotionId, promotionTypeId) => {
    if (promotionTypeId === 6)
      this.setState({ visibleConfirm: true, promotionId, promotionTypeId });
    else
      this.setState({
        visibleDiscountCodeGroupModal: true,
        promotionId,
        promotionTypeId,
      });
  };

  handleCancelDiscountCodeGroupModal = () => {
    this.setState({ visibleDiscountCodeGroupModal: false });
  };

  handleOkDiscountCodeGroupModal = () => {
    const { persianToDateExpire } = this.state;

    if (persianToDateExpire.length > 0) {
      this.setState({ visibleConfirm: true });
    } else
      showNotification('error', '', 'لطفا تاریخ انقضاء را انتخاب کنید.', 7);
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
  };

  walletIncrease = () => {
    const { data } = this.props;
    const { wallet } = this.state;

    const dto = new BaseCRUDDtoBuilder()
      .dto(
        new WalletDto({
          fromPanel: true,
          userDto: new UserDto({ id: data.id }),
          discountCode: 'ihanap',
          payableWalletAmount: wallet,
        }),
      )
      .build();

    const getUser = getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new WalletDto({
            userDto: new UserDto({ id: data.id }),
            fromPanel: true,
          }),
        )
        .buildJson(),
    );

    const data2 = {
      data: dto,
      listDto: getUser,
    };

    this.props.actions.walletPostRequest(data2);
    this.showAndHideModalWallet(false);
  };

  showAndHideModalWallet = value => {
    this.setState({ modalWalletVisible: value, wallet: 0 });
  };

  render() {
    const {
      data,
      vendors,
      promotions,
      promotionLoading,
      promotionCount,
      vendorBranchId,
      cities,
      userWallet,
    } = this.props;
    const {
      firstName,
      lastName,
      email,
      phone,
      password,
      // dateOfBirth,
      monthOfBirthDate,
      dayOfBirthDate,
      vendorId,
      gender,
      active,
      newsLetter,
      disabledVendorID,
      visibleModal,
      visibleConfirm,
      verify,
      cityId,
      imageUrl,
      userEventStatus,
      showMessagePhone,
      messagePhoneNumber,
      persianToDateExpire,
      visibleDiscountCodeGroupModal,
      offline,
      modalWalletVisible,
      wallet,
    } = this.state;
    const isEdit = data && data.id;

    const sexualModel = [
      {
        value: 'M',
        name: 'مرد',
        defaultChecked: gender === 'M',
      },
      {
        value: 'F',
        name: 'زن',
        defaultChecked: gender === 'F',
      },
    ];
    const vendorDataSource = [];
    const cityDataSource = [];

    const columns = [
      { title: NAME, dataIndex: 'name', key: 'name' },
      {
        title: FROM_DATE,
        dataIndex: 'persianFromDateUtc',
        key: 'persianFromDateUtc',
      },
      {
        title: TO_DATE,
        dataIndex: 'persianToDateUtc',
        key: 'persianToDateUtc',
      },
      {
        title: STATUS,
        dataIndex: 'active',
        render: (text, record) => (
          <div>
            <CPSwitch disabled size="small" defaultChecked={record.active} />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operation',
        render: (text, record) => (
          <CPButton
            className="btn-primary"
            onClick={() =>
              this.showModalConfirm(record.id, record.promotionTypeId)
            }
          >
            {SENDING_CODE_DISCOUNT}
          </CPButton>
        ),
      },
    ];

    /**
     * map vendors for comboBox Datasource
     */
    vendors.map(item =>
      vendorDataSource.push(<Option key={item.id}>{item.name}</Option>),
    );

    /**
     * map cities for comboBox Datasource
     */
    cities.map(item =>
      cityDataSource.push(<Option key={item.id}>{item.name}</Option>),
    );

    const days = [];
    for (let i = 1; i <= 31; i += 1) {
      days.push(
        <Option value={i.toString()} key={i.toString()}>
          {i.toString()}
        </Option>,
      );
    }

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <div>
          <div className={s.tabContent}>
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <img src={imageUrl || '/images/avatar.jpg'} />
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <CPInput
                  label={USER_FIRST_NAME}
                  value={firstName}
                  onChange={value =>
                    this.onChangeInpute('firstName', value.target.value)
                  }
                  autoFocus
                />
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <CPInput
                  label={USER_LAST_NAME}
                  value={lastName}
                  onChange={value =>
                    this.onChangeInpute('lastName', value.target.value)
                  }
                />
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                {/* <CPPersianCalendar
                  placeholder={BIRTH_DATE}
                  // onChange={value => this.onChangeInpute('dateOfBirth', value)}
                  onChange={(unix, formatted) =>
                    this.onChangeInpute('dateOfBirth', formatted)
                  }
                  id="datePicker"
                  // value={dateOfBirth}
                  preSelected={dateOfBirth}
                /> */}
                <div className="row">
                  <div className="col-sm-6 col-xs-12">
                    <label>{DAY_OF_BIRTHDAY}</label>
                    <CPSelect
                      value={dayOfBirthDate}
                      onChange={value =>
                        this.onChangeInpute('dayOfBirthDate', value)
                      }
                      showSearch
                    >
                      <Option key="0" value="0">
                        انتخاب روز
                      </Option>
                      {days}
                    </CPSelect>
                  </div>
                  <div className="col-sm-6 col-xs-12">
                    <label>{MONTH_OF_BIRTHDAY}</label>
                    <CPSelect
                      value={monthOfBirthDate}
                      onChange={value =>
                        this.onChangeInpute('monthOfBirthDate', value)
                      }
                      showSearch
                    >
                      <Option key="0" value="0">
                        انتخاب ماه
                      </Option>
                      <Option value="1" key="1">
                        {FARVARDIN}
                      </Option>
                      <Option value="2" key="2">
                        {ORDIBEHESHT}
                      </Option>
                      <Option value="3" key="3">
                        {KHORDAD}
                      </Option>
                      <Option value="4" key="4">
                        {TIR}
                      </Option>
                      <Option value="5" key="5">
                        {MORDAD}
                      </Option>
                      <Option value="6" key="6">
                        {SHAHRIVAR}
                      </Option>
                      <Option value="7" key="7">
                        {MEHR}
                      </Option>
                      <Option value="8" key="8">
                        {ABAN}
                      </Option>
                      <Option value="9" key="9">
                        {AZAR}
                      </Option>
                      <Option value="10" key="10">
                        {DEY}
                      </Option>
                      <Option value="11" key="11">
                        {BAHMAN}
                      </Option>
                      <Option value="12" key="12">
                        {ESFAND}
                      </Option>
                    </CPSelect>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{GENDER}</label>
                <CPRadio
                  model={sexualModel}
                  size="small"
                  onChange={value =>
                    this.onChangeInpute('gender', value.target.value)
                  }
                  defaultValue={gender}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{PHONE}</label>
                <CPIntlPhoneInput value={phone} onChange={this.changeNumber} />
                <p
                  className={s.errorMessage}
                  style={{ display: `${showMessagePhone}` }}
                >
                  {messagePhoneNumber}
                </p>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <CPValidator
                  validationEmail
                  value={email}
                  checkValidation={this.checkValidation}
                  name={EMAIL}
                >
                  <CPInput
                    label={EMAIL}
                    id="emailUser"
                    name="emailUser"
                    value={email}
                    onChange={value =>
                      this.onChangeInpute('email', value.target.value)
                    }
                  />
                </CPValidator>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{AGENT_NAME}</label>
                <CPSelect
                  value={vendorId}
                  disabled={disabledVendorID}
                  onChange={this.vendorChange}
                  showSearch
                >
                  <Option value="0">{NO_AGENT}</Option>
                  {vendorDataSource}
                </CPSelect>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{SELECT_STATUS_EVENT}</label>
                <CPSelect
                  value={userEventStatus}
                  onChange={value =>
                    this.onChangeInpute('userEventStatus', value)
                  }
                >
                  <Option value={0}>{SELECT_STATUS_EVENT}</Option>
                  <Option value={1}>{WAS_RESPONSE}</Option>
                  <Option value={2}>{WAS_NOT_RESPONSE}</Option>
                  <Option value={3}>{UN_WILLINGNESS}</Option>
                  <Option value={4}>{SYSTEM}</Option>
                  <Option value={5}>{BLOCK}</Option>
                </CPSelect>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{CITY}</label>
                <CPSelect
                  value={cityId}
                  onChange={value => this.onChangeInpute('cityId', value)}
                  showSearch
                >
                  <Option key="0">انتخاب شهر</Option>
                  {cityDataSource}
                </CPSelect>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="align-center">
                  <CPSwitch
                    defaultChecked={newsLetter}
                    checkedChildren={NEWSLETTER}
                    unCheckedChildren={NEWSLETTER}
                    onChange={value => {
                      this.onChangeInpute('newsLetter', value);
                    }}
                  />
                  <CPSwitch
                    defaultChecked={active}
                    unCheckedChildren={DEACTIVATE}
                    checkedChildren={ACTIVE}
                    onChange={value => this.onChangeInpute('active', value)}
                  >
                    {STATUS}
                  </CPSwitch>
                  <CPSwitch
                    defaultChecked={verify}
                    unCheckedChildren={VERIFY}
                    checkedChildren={VERIFY}
                    onChange={value => this.onChangeInpute('verify', value)}
                  >
                    {VERIFY}
                  </CPSwitch>
                  <CPSwitch
                    defaultChecked={offline}
                    unCheckedChildren={ONLINE}
                    checkedChildren={OFFLINE}
                    onChange={value => this.onChangeInpute('offline', value)}
                  />
                </div>
              </div>
              {data &&
                data.reagentUserDto && (
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <label>{REAGENT_USER}</label>
                    <Link to={`/user/edit/${data.reagentUserDto.id}`}>
                      {data.reagentUserDto.displayName} ({data.reagentUserDto
                        .phone &&
                        data.reagentUserDto.phone.replace('+98', '0')})
                    </Link>
                  </div>
                )}

              {isEdit && (
                <div className="col-lg-3 col-sm-6 col-xs-12">
                  <label>{LOGIN_HOUR}</label>
                  <div>
                    <img
                      alt=""
                      width={30}
                      src={
                        data.signInFromWeb
                          ? '/images/pc.png'
                          : '/images/mobile.png'
                      }
                    />
                    <span>{data.lastLoginDateTime}</span>
                  </div>
                </div>
              )}
              {isEdit && (
                <div className="col-lg-3 col-sm-6 col-xs-12">
                  <label>{WALLET}</label>
                  <div>
                    {userWallet.stringTotalAmount
                      ? `${userWallet.stringTotalAmount} تومان`
                      : '0'}
                    <CPButton
                      onClick={() => this.showAndHideModalWallet(true)}
                      className={s.wallet}
                    >
                      افزایش اعتبار
                    </CPButton>
                  </div>
                </div>
              )}
            </div>
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <CPInput
                  label={PASSWORD}
                  type="password"
                  value={password}
                  onChange={value =>
                    this.onChangeInpute('password', value.target.value)
                  }
                />
              </div>

              {isEdit && (
                <div className="col-lg-3 col-sm-6 col-xs-12">
                  <div className="align-center">
                    <CPButton
                      icon="save"
                      type="primary"
                      className={s.button}
                      onClick={() => this.changePassword()}
                    >
                      {CHANGE_PASSWORD}
                    </CPButton>

                    <CPButton
                      className={s.button}
                      onClick={this.showModalDiscount}
                    >
                      {SENDING_CODE_DISCOUNT}
                    </CPButton>
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className={s.textLeft}>
            <CPButton
              icon="save"
              className="btn-primary"
              onClick={this.saveFormAndContinue}
            >
              {FORM_SAVE_AND_CONTINUE}
            </CPButton>
            {vendorBranchId === '' && (
              <CPButton icon="save" onClick={this.saveForm}>
                {FORM_SAVE}
              </CPButton>
            )}
          </div>
          <CPModal
            className="max_modal"
            visible={visibleModal}
            handleCancel={this.handleCancelModal}
            showFooter={false}
            title={SENDING_CODE_DISCOUNT}
          >
            <CPList
              data={promotions}
              count={promotionCount}
              columns={columns}
              loading={promotionLoading}
              hideAdd
              hideSearchBox
            />
          </CPModal>
          <CPModal
            visible={visibleDiscountCodeGroupModal}
            handleCancel={this.handleCancelDiscountCodeGroupModal}
            handleOk={this.handleOkDiscountCodeGroupModal}
            title={ENTER_EXPIRE_DATE}
          >
            <div className="col-xs-12 modalBody">
              <label>{ENTER_EXPIRE_DATE}</label>
              <CPPersianCalendar
                placeholder={ENTER_EXPIRE_DATE}
                // onChange={value => this.onChangeInpute('dateOfBirth', value)}
                onChange={(unix, formatted) =>
                  this.onChangeInpute('persianToDateExpire', formatted)
                }
                id="datePicker"
                preSelected={persianToDateExpire}
              />
            </div>
          </CPModal>
          <CPModal
            visible={modalWalletVisible}
            handleCancel={() => this.showAndHideModalWallet(false)}
            handleOk={this.walletIncrease}
            title="افزایش اعتبار"
          >
            <div className="col-xs-12 modalBody">
              <label>افزایش اعتبار (تومان)</label>
              <CPInputNumber
                value={wallet}
                onChange={value => this.onChangeInpute('wallet', value)}
                format="Currency"
              />
            </div>
          </CPModal>
          <CPConfirmation
            visible={visibleConfirm}
            handleOk={this.handleOkConfirm}
            handleCancel={this.handleCancelConfirm}
            content={ARE_YOU_SURE_WANT_TO_SEND_DISCOUNT_CODE}
            zIndex={10000}
          />
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  userRoles: state.userRoleList.data.items,
  vendors: state.vendorList.data.items,
  data:
    state.singleUser.data &&
    state.singleUser.data.items &&
    state.singleUser.data.items.length > 0
      ? state.singleUser.data.items[0]
      : null,
  promotions: state.promotionList.data.items,
  promotionCount:
    state.promotionList.data.items && state.promotionList.data.items.length > 0
      ? state.promotionList.data.count
      : 0,
  promotionLoading: state.promotionList.loading,
  cities: state.cityList.data ? state.cityList.data.items : [],
  userWallet:
    state.walletList.data && state.walletList.data.items && state.walletList.data.items.length > 0
      ? state.walletList.data.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      userPostRequest: userPostActions.userPostRequest,
      userPutRequest: userPutActions.userPutRequest,
      promotionListRequest: promotionListActions.promotionListRequest,
      walletPostRequest: walletPostActions.walletPostRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(UserInfo));
