import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './InvitedUser.css';
import { NAME } from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import CPList from '../../../CP/CPList';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';
import { UserDto } from '../../../../dtos/identityDtos';
import invitedUserListActions from '../../../../redux/identity/actionReducer/user/InvitedUserList';
import Link from "../../../Link";

class InvitedUser extends React.Component {
  static propTypes = {
    invited: PropTypes.arrayOf(PropTypes.object),
    user: PropTypes.objectOf(PropTypes.any),
    invitedLoading: PropTypes.bool,
    invitedCount: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    invited: [],
    user: {},
    invitedLoading: false,
    invitedCount: 0,
  };

  async componentWillMount() {
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new UserDto({
          active: true,
          reagentUserDto: new UserDto({ id: this.props.user.id }),
        }),
      )
      .buildJson();

    this.props.actions.invitedUserListRequest(jsonList);
  }

  render() {
    const { invited, invitedLoading, invitedCount } = this.props;

    const invitedDataSource = [];

    const columns = [
      {
        title: NAME,
        dataIndex: 'name',
        key: 'name',
        width: 200,
        render: (text, record) => (
          <Link to={`/user/edit/${record.key}`}>
            {record.name}
          </Link>
        ),
      },
    ];

    invited.map(item => {
      const obj = {
        key: item.id,
        name: item.displayName,
      };
      invitedDataSource.push(obj);
      return null;
    });

    return (
      <div>
        <CPList
          data={invitedDataSource}
          columns={columns}
          hideAdd
          hideSearchBox
          loading={invitedLoading}
          count={invitedCount}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  invited: state.invitedUserList.data ? state.invitedUserList.data.items : [],
  invitedCount: state.invitedUserList.data
    ? state.invitedUserList.data.count
    : 0,
  invitedLoading: state.invitedUserList.loading,
  user:
    state.singleUser.data &&
    state.singleUser.data.items &&
    state.singleUser.data.items.length > 0
      ? state.singleUser.data.items[0]
      : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      invitedUserListRequest: invitedUserListActions.invitedUserListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(InvitedUser));
