import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { hideLoading } from 'react-redux-loading-bar';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserForm.css';
import {
  ADDRESS, BANK_INFO, EVENT_LIST, INVITED_LIST,
  USER_INFO,
  USER_USER_ROLE,
} from '../../../../Resources/Localization';
import UserInfo from '../UserInfo/UserInfo';
import UserRole from '../UserRole/UserRole';
import Event from "../Event";
import InvitedUser from "../Invited";
import UserAddress from "../UserAddress/UserAddress";
import UserCardNumber from "../UserCardNumber";

class UserForm extends React.Component {
  static propTypes = {
    user: PropTypes.objectOf(PropTypes.any),
    vendorBranchId: PropTypes.string,
    backUrlAndVendorId: PropTypes.string,
    vendorId: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    user: null,
    backUrlAndVendorId: null,
    vendorBranchId: '',
    vendorId: undefined,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    const { user, vendorBranchId, vendorId } = this.props;

    const isEdit = user && user.id;

    return (
      <div className={s.root}>
        <Tabs>
          <TabList>
            <Tab>{USER_INFO}</Tab>
            {isEdit && <Tab>{EVENT_LIST}</Tab>}
            {isEdit && <Tab>{INVITED_LIST}</Tab>}
            {isEdit && <Tab>{USER_USER_ROLE}</Tab>}
            {isEdit && <Tab>{ADDRESS}</Tab>}
            {isEdit && <Tab>{BANK_INFO}</Tab>}
          </TabList>
          <TabPanel>
            <UserInfo vendorBranchId={vendorBranchId} vendorId={vendorId} />
          </TabPanel>
          {isEdit && (
            <TabPanel>
              <Event />
            </TabPanel>
          )}
          {isEdit && (
            <TabPanel>
              <InvitedUser />
            </TabPanel>
          )}
          {isEdit && (
            <TabPanel>
              <UserRole user={user} vendorBranchId={vendorBranchId} />
            </TabPanel>
          )}
           {isEdit && (
             <TabPanel>
              <UserAddress user={user} />
            </TabPanel>
          )}
           {isEdit && (
             <TabPanel>
              <UserCardNumber user={user} />
            </TabPanel>
          )}
        </Tabs>
      </div>
    );
  }
}
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(UserForm));
