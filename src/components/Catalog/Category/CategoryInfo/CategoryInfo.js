import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HotKeys from 'react-hot-keys';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CategoryInfo.css';
import CPInput from '../../../CP/CPInput/CPInput';
import CPButton from '../../../CP/CPButton/CPButton';
import CPTreeSelect from '../../../CP/CPTreeSelect/CPTreeSelect';
import {
  ACTIVE,
  CATEGORY_CODE,
  CATEGORY_NAME,
  DEACTIVATE,
  DESCRIPTION,
  DISPLAY_ORDER,
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  IMAGE_UPLOAD,
  SHOW_ON_MAIN_PAGE,
  STATUS,
} from '../../../../Resources/Localization';
import { CategoryDto } from '../../../../dtos/catalogDtos';
import categoryPostActions from '../../../../redux/catalog/actionReducer/category/CategoryPost';
import categoryPutActions from '../../../../redux/catalog/actionReducer/category/CategoryPut';
import CPSwitch from '../../../CP/CPSwitch';
import CPTextArea from '../../../CP/CPTextArea';
import CPUpload from '../../../CP/CPUpload';
import { ImageDto } from '../../../../dtos/cmsDtos';
import CPInputNumber from '../../../CP/CPInputNumber';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

class CategoryInfo extends React.Component {
  static propTypes = {
    categories: PropTypes.arrayOf(PropTypes.object),
    category: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    categories: null,
    category: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: props.category ? props.category.name : null,
      description: props.category ? props.category.description : null,
      parentCategoryId: props.category
        ? props.category.parentCategoryId.toString()
        : '0',
      showOnHomePage: props.category ? props.category.showOnHomePage : false,
      published: props.category ? props.category.published : true,
      id: props.category ? props.category.id : 0,
      url:
        props.category && props.category.imageDtos.length > 0
          ? props.category.imageDtos[0].url
          : '',
      code: props.category ? props.category.code : '',
      extention: '',
      binariesImage: '',
      displayOrder: props.category ? props.category.displayOrder : 0,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { category } = nextProps;

    if (category) {
      this.setState({
        name: category.name,
        description: category.description,
        parentCategoryId: category.parentCategoryId.toString(),
        showOnHomePage: false,
        published: true,
        code: category.code,
      });
    }
  }

  onChangeTreeSelect = value => {
    this.setState({ parentCategoryId: value });
  };

  /**
   * set dto with state for post and put data
   * @returns {{dto: {name: *, description: *, parentCategoryId: *, showOnHomePage: *, published: *}}}
   */
  prepareContainer = () => {
    const {
      name,
      description,
      parentCategoryId,
      showOnHomePage,
      published,
      id,
      extention,
      binariesImage,
      code,
      displayOrder,
    } = this.state;

    const jsonList = new BaseCRUDDtoBuilder()
      .dto(
        new CategoryDto({
          name,
          code,
          description,
          parentCategoryId,
          showOnHomePage,
          published,
          id,
          displayOrder,
          imageDtos: binariesImage
            ? [
                new ImageDto({
                  extention,
                  binaries: binariesImage.split(',')[1],
                }),
              ]
            : null,
        }),
      )
      .build();
    return jsonList;
  };

  /**
   * save data with post and put - redirect to list page
   */
  saveForm = () => {
    const { category } = this.props;
    if (category && category.id) {
      this.putData(this.prepareContainer());
    } else {
      this.postData(this.prepareContainer());
    }
  };

  /**
   * save data with post and put - redirect to edit page
   */
  saveFormAndContinue = () => {
    const { category } = this.props;
    if (category && category.id) {
      this.putData(this.prepareContainer(), true);
    } else {
      this.postData(this.prepareContainer(), true);
    }
  };

  /**
   * call post data for sending data
   * @param containert
   * @returns {Promise<void>}
   */
  async postData(container, saveAndCountinue = false) {
    const data = {
      data: container,
      status: saveAndCountinue ? 'saveAndContinue' : 'save',
      listDto: new BaseGetDtoBuilder()
        .dto(new CategoryDto({ makeTree: true, published: true }))
        .buildJson(),
    };

    this.props.actions.categoryPostRequest(data);
  }

  async putData(container, saveAndCountinue = false) {
    const data = {
      data: container,
      status: saveAndCountinue ? 'saveAndContinue' : 'save',
      listDto: new BaseGetDtoBuilder()
        .dto(new CategoryDto({ makeTree: true, published: true }))
        .buildJson(),
    };

    this.props.actions.categoryPutRequest(data);
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  handleImageChange = value => {
    this.setState({
      binariesImage: value.base64,
      extention: value.type.split('/')[1],
    });
  };

  render() {
    const { categories } = this.props;
    const {
      name,
      description,
      parentCategoryId,
      showOnHomePage,
      published,
      url,
      code,
      displayOrder,
    } = this.state;
    const model = [];
    /**
     * Add default value for combobox
     */
    model.push({ id: '0', name: 'پدر ندارد', children: [] });
    if (categories) categories.map(item => model.push(item));

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <div>
          <div className="tabContent">
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <CPInput
                  label={CATEGORY_NAME}
                  value={name}
                  onChange={value =>
                    this.handelChangeInput('name', value.target.value)
                  }
                  autoFocus
                />
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>پدر</label>
                <CPTreeSelect
                  model={model}
                  onChange={this.onChangeTreeSelect}
                  value={parentCategoryId}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <CPInput
                  label={CATEGORY_CODE}
                  value={code}
                  onChange={value =>
                    this.handelChangeInput('code', value.target.value)
                  }
                />
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12">
                <label>{DISPLAY_ORDER}</label>
                <CPInputNumber
                  name="displayOrder"
                  value={displayOrder}
                  onChange={value => {
                    this.handelChangeInput('displayOrder', value);
                  }}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{DESCRIPTION}</label>
                <CPTextArea
                  value={description}
                  onChange={value =>
                    this.handelChangeInput('description', value.target.value)
                  }
                  disabled
                />
              </div>
              <div className="col-sm-12">
                <label>{IMAGE_UPLOAD}</label>
                <CPUpload onChange={this.handleImageChange} image={`${url}`} />
              </div>
              <div className="col-sm-12">
                <div className="align-margin">
                  <CPSwitch
                    defaultChecked={showOnHomePage}
                    checkedChildren={SHOW_ON_MAIN_PAGE}
                    unCheckedChildren={SHOW_ON_MAIN_PAGE}
                    onChange={value => {
                      this.handelChangeInput('showOnHomePage', value);
                    }}
                  />
                  <CPSwitch
                    defaultChecked={published}
                    unCheckedChildren={DEACTIVATE}
                    checkedChildren={ACTIVE}
                    onChange={value =>
                      this.handelChangeInput('published', value)
                    }
                  >
                    {STATUS}
                  </CPSwitch>
                </div>
              </div>
            </div>
          </div>
          <div className={s.textLeft}>
            <CPButton
              icon="form"
              className="btn-primary"
              onClick={this.saveFormAndContinue}
            >
              {FORM_SAVE_AND_CONTINUE}
            </CPButton>
            <CPButton icon="save" onClick={this.saveForm}>
              {FORM_SAVE}
            </CPButton>
          </div>
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.categoryList.data ? state.categoryList.data.items : null,
  category:
    state.singleCategory.data && state.singleCategory.data.items
      ? state.singleCategory.data.items[0]
      : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      categoryPostRequest: categoryPostActions.categoryPostRequest,
      categoryPutRequest: categoryPutActions.categoryPutRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CategoryInfo));
