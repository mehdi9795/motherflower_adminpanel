import React from 'react';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import CategoryInfo from '../CategoryInfo/CategoryInfo';
import { CATEGORY_INFO } from '../../../../Resources/Localization';
import s from './CategoryForm.css';

class CategoryForm extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    return (
      <Tabs>
        <TabList>
          <Tab>{CATEGORY_INFO}</Tab>
        </TabList>
        
        <TabPanel>
          <CategoryInfo />
        </TabPanel>
      </Tabs>
    );
  }
}
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(CategoryForm),
);
