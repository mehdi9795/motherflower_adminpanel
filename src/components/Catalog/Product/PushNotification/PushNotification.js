import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PushNotification.css';
import CPInput from '../../../CP/CPInput/CPInput';
import {
  FORM_SAVE,
  INFO_BASIC,
  NOTIFICATION,
  PRODUCT_PRODUCT_TYPE,
  SMS,
  SUBJECT,
  VENDOR_BRANCH,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import CPPanel from '../../../CP/CPPanel';
import { BaseCRUDDtoBuilder } from '../../../../dtos/dtoBuilder';
import { PushDto, PushMetadataDto } from '../../../../dtos/notificationDtos';
import pushPostActions from '../../../../redux/notification/actionReducer/push/Post';
import CPSelect from '../../../CP/CPSelect';
import { Select } from 'antd';

const { Option } = Select;

class PushNotification extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    product: PropTypes.objectOf(PropTypes.any),
    images: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    product: {},
    images: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      vendorBranchId:
        props.product &&
        props.product.vendorBranchProductDtos &&
        props.product.vendorBranchProductDtos.length > 0
          ? props.product.vendorBranchProductDtos[0].vendorBranchDto.id.toString()
          : 0,
    };
  }

  handleChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  saveForm = () => {
    const { subject, sms, vendorBranchId } = this.state;
    const { images, product } = this.props;

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new PushDto({
          subject,
          body: sms,
          pushMetadataDtos: [
            new PushMetadataDto({
              metaKey: 'vendorBranchId',
              metaValue: vendorBranchId,
            }),
            new PushMetadataDto({
              metaKey: 'productId',
              metaValue: product.id,
            }),
          ],
          entityType: 'Product',
          entityId: product.id,
          status: 'InActive',
        }),
      )
      .build();

    const postData = {
      data: jsonCrud,
      listDto: null,
    };

    this.props.actions.pushPostRequest(postData);
  };

  render() {
    const { subject, sms, vendorBranchId } = this.state;
    const { product } = this.props;

    const vendorBranchArray = [];
    product.vendorBranchProductDtos.map(item =>
      vendorBranchArray.push(
        <Option key={item.vendorBranchDto.id.toString()}>
          {item.vendorBranchDto.name}
        </Option>,
      ),
    );

    return (
      <div>
        <div className="tabContent">
          <div className="row">
            <div className="col-lg-6 col-sm-12">
              <CPPanel header={NOTIFICATION} key={1}>
                <div className="row">
                  <div className="col-sm-12">
                    <label>{VENDOR_BRANCH}</label>
                    <CPSelect
                      value={vendorBranchId}
                      onChange={value =>
                        this.handleChangeInput('vendorBranchId', value)
                      }
                    >
                      {vendorBranchArray}
                    </CPSelect>
                  </div>
                  <div className="col-lg-6 col-sm-12">
                    <label>{SUBJECT}</label>
                    <CPInput
                      value={subject}
                      onChange={value =>
                        this.handleChangeInput('subject', value.target.value)
                      }
                    />
                  </div>

                  <div className="col-lg-6 col-sm-12">
                    <label>{SMS}</label>
                    <CPInput
                      value={sms}
                      onChange={value =>
                        this.handleChangeInput('sms', value.target.value)
                      }
                    />
                  </div>
                </div>
              </CPPanel>
            </div>
          </div>
        </div>
        <div className={s.textLeft}>
          <CPButton icon="save" onClick={this.saveForm}>
            {FORM_SAVE}
          </CPButton>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product:
    state.singleProduct.data && state.singleProduct.data.items
      ? state.singleProduct.data.items[0]
      : null,
  images: state.imageList.data ? state.imageList.data.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      pushPostRequest: pushPostActions.pushPostRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(PushNotification));
