import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductImage.css';
import CPImageGallery from '../../../CP/CPImageGallery';
import CPModal from '../../../CP/CPModal';
import CPInput from '../../../CP/CPInput';
import {
  DEFAULT_IMAGE,
  DISPLAY_ORDER,
  HOME_PAGE_IMAGE,
  IMAGE_UPLOAD,
  PRODUCT_PRICE_EDIT,
  TOOLTIP,
} from '../../../../Resources/Localization';
import CPUpload from '../../../CP/CPUpload';
import { ImageDto } from '../../../../dtos/cmsDtos';
import imageDeleteActions from '../../../../redux/cms/actionReducer/image/Delete';
import imagePostActions from '../../../../redux/cms/actionReducer/image/Post';
import imagePutActions from '../../../../redux/cms/actionReducer/image/Put';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import CPCheckbox from '../../../CP/CPCheckbox';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

class ProductPrice extends React.Component {
  static propTypes = {
    product: PropTypes.objectOf(PropTypes.any),
    images: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    images: [],
    product: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      tooltip: '',
      displayOrder: 0,
      isCover: false,
      isShowOnHomePage: false,
      url: '',
      visibleImageModal: false,
      binariesImage: '',
      extention: '',
      id: 0,
      callSelectedImage: false,
      inputElement: null,
    };
    this.inputElement = null;
  }

  onDelete = value => {
    const { product } = this.props;
    /**
     * reload  Product images with productId
     * @type
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(new ImageDto({ entityId: product.id, imagetype: 'Product' }))
      .disabledCount(false)
      .buildJson();

    const putData = {
      id: value,
      listDto: jsonList,
    };

    this.props.actions.imageDeleteRequest(putData);
  };

  onEdit = value => {
    this.setState({
      tooltip: value ? value.tooltip : '',
      displayOrder: value ? value.displayOrder : 0,
      isCover: value ? value.isCover : false,
      isShowOnHomePage: value ? value.isShowOnHomePage : false,
      visibleImageModal: true,
      url: value ? value.imgUrl : '',
      id: value ? value.id : 0,
      binariesImage: '',
    });
  };

  handleImageChange = value => {
    this.setState(
      {
        binariesImage: value.base64,
        extention: value.type.split('/')[1],
      },
      () => {
        if (this.state.id === 0) this.handleOkModal();
      },
    );
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    if (inputName === 'isShowOnHomePage')
      this.setState({ [inputName]: value, isCover: false });
    else if (inputName === 'isCover')
      this.setState({ [inputName]: value, isShowOnHomePage: false });
    else this.setState({ [inputName]: value });
  };

  handleCancelModal = () => {
    this.setState({
      visibleImageModal: false,
      callSelectedImage: false,
    });
  };

  handleOkModal = () => {
    const {
      tooltip,
      displayOrder,
      isCover,
      isShowOnHomePage,
      binariesImage,
      extention,
      id,
    } = this.state;
    const { product } = this.props;

    this.setState({
      visibleImageModal: false,
      callSelectedImage: false,
    });

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new ImageDto({
          binaries: binariesImage.split(',')[1],
          displayOrder,
          entityId: product.id,
          extention,
          id,
          tooltip,
          isCover,
          showOnHomePage: isShowOnHomePage,
          imagetype: 'Product',
        }),
      )
      .build();
    /**
     * reload  Product images with productId
     * @type
     */

    const jsonList = new BaseGetDtoBuilder()
      .dto(new ImageDto({ entityId: product.id, imagetype: 'Product' }))
      .disabledCount(false)
      .buildJson();

    const data = {
      data: jsonCrud,
      listDto: jsonList,
    };
    if (id === 0) {
      this.props.actions.imagePostRequest(data);
    } else {
      this.props.actions.imagePutRequest(data);
    }
  };

  render() {
    const { images } = this.props;
    const {
      tooltip,
      displayOrder,
      isCover,
      url,
      isShowOnHomePage,
    } = this.state;

    return (
      <div>
        <CPImageGallery
          data={images}
          onEdit={this.onEdit}
          onDelete={this.onDelete}
          onCreate={() => this.onEdit(null)}
        />
        <CPModal
          visible={this.state.visibleImageModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
          title={PRODUCT_PRICE_EDIT}
        >
          <div className="modalContent">
            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <label>{TOOLTIP}</label>
                <CPInput
                  name="tooltip"
                  // label={DELIVERY_PRICE}
                  value={tooltip}
                  onChange={value => {
                    this.handelChangeInput('tooltip', value.target.value);
                  }}
                />
              </div>
              <div className="col-lg-6 col-sm-12">
                <label>{DISPLAY_ORDER}</label>
                <CPInput
                  name="displayOrder"
                  // label={PICKUP_PRICE}
                  value={displayOrder.toString()}
                  onChange={value => {
                    this.handelChangeInput('displayOrder', value.target.value);
                  }}
                />
              </div>
              <div className="col-sm-12">
                <label>{IMAGE_UPLOAD}</label>
                {this.state.visibleImageModal && (
                  <CPUpload
                    onChange={this.handleImageChange}
                    image={`${url}`}
                    buttonRef={el => {
                      if (!url && el) el.props.onClick();
                    }}
                  />
                )}
              </div>
              <div className="col-sm-12">
                <div className="align-margin">
                  <div className={s.cover}>
                    <label>{DEFAULT_IMAGE}</label>
                    <CPCheckbox
                      checked={isCover}
                      onChange={value =>
                        this.handelChangeInput('isCover', value.target.checked)
                      }
                    />
                  </div>
                  <div className={s.cover}>
                    <label>{HOME_PAGE_IMAGE}</label>
                    <CPCheckbox
                      checked={isShowOnHomePage}
                      onChange={value =>
                        this.handelChangeInput(
                          'isShowOnHomePage',
                          value.target.checked,
                        )
                      }
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  images: state.imageList.data ? state.imageList.data.items : [],
  product: state.singleProduct.data ? state.singleProduct.data.items[0] : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      imagePutRequest: imagePutActions.imagePutRequest,
      imageDeleteRequest: imageDeleteActions.imageDeleteRequest,
      imagePostRequest: imagePostActions.imagePostRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductPrice));
