import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductPrice.css';
import {
  DELIVERY_PRICE,
  PICKUP_PRICE,
  PRICE,
  PRODUCT_PRICE_EDIT,
  VENDOR_BRANCH_NAME,
  VENDOR_BRANCH,
} from '../../../../Resources/Localization';

import CPList from '../../../CP/CPList';
import CPModal from '../../../CP/CPModal';
import { VendorBranchProductPriceDto } from '../../../../dtos/samDtos';
import { ProductDto } from '../../../../dtos/catalogDtos';
import vbProductPricePutActions from '../../../../redux/sam/actionReducer/vendorBranchProductPrice/Put';
import CPInputNumber from '../../../CP/CPInputNumber';
import CPButton from '../../../CP/CPButton';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import CPConfirmation from '../../../CP/CPConfirmation';
import {
  BaseCRUDDtoBuilder,
  BaseListGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

class ProductPrice extends React.Component {
  static propTypes = {
    productPrices: PropTypes.arrayOf(PropTypes.object),
    product: PropTypes.objectOf(PropTypes.any),
    permission: PropTypes.objectOf(PropTypes.any),
    productPriceCount: PropTypes.number,
    productPriceLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    product: null,
    productPriceCount: 0,
    productPriceLoading: false,
    productPrices: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      visiblePriceModal: false,
      deliveryPrice: '0',
      pickupPrice: '0',
      productPriceId: 0,
      visibleConfirmation: false,
      confirmationContent: '',
      priceObj: {},
    };
  }

  showPriceModal = record => {
    this.setState({
      visiblePriceModal: true,
      deliveryPrice: parseInt(record.deliveryPrice, 0),
      pickupPrice: parseInt(record.pickupPrice, 0),
      productPriceId: record.key,
      b2bPrice: record.b2bPrice,
      priceObj: record.priceObj,
      vendorBranchDeliveryPrice: record.vendorBranchDeliveryPrice,
      vendorBranchPickupPrice: record.vendorBranchPickupPrice,
    });
  };

  handleCancelModal = () => {
    this.setState({
      visiblePriceModal: false,
    });
  };

  handleOkConfirmation = () => {
    this.setState({ visibleConfirmation: false });
    this.saveForm();
  };

  handleCancelConfirmation = () => {
    this.setState({ visibleConfirmation: false });
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    if (inputName === 'b2bPrice') this.setState({ b2bPrice: value });
    else if (inputName === 'deliveryPrice' || inputName === 'pickupPrice')
      this.setState({ deliveryPrice: value, pickupPrice: value });
    else this.setState({ [inputName]: value });
  };

  handleOkModal = () => {
    const { deliveryPrice, pickupPrice, b2bPrice } = this.state;
    if (parseInt(deliveryPrice, 0) >= 1000000 || pickupPrice >= 1000000)
      this.setState({
        confirmationContent:
          'قیمت محصول بیشتر از 1,000,000 تومان است. آیا از قیمت محصول مطمئن هستید؟',
        visibleConfirmation: true,
      });
    else if (parseInt(deliveryPrice, 0) < 10000 || pickupPrice < 10000)
      this.setState({
        confirmationContent:
          'قیمت محصول کمتر از 10,000 تومان است. آیا از قیمت محصول مطمئن هستید؟',
        visibleConfirmation: true,
      });
    else this.saveForm();
  };

  saveForm = () => {
    const {
      deliveryPrice,
      pickupPrice,
      b2bPrice,
      productPriceId,
      priceObj,
      vendorBranchPrice,
      vendorBranchDeliveryPrice,
      vendorBranchPickupPrice,
    } = this.state;
    const { product } = this.props;

    this.setState({
      visiblePriceModal: false,
    });

    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new VendorBranchProductPriceDto({
          deliveryBasePrice: parseInt(deliveryPrice, 0),
          pickupBasePrice: parseInt(pickupPrice, 0),
          // pickupBasePrice: parseInt(deliveryPrice, 0),
          id: productPriceId,
          deliveryPriceAfterDiscount: priceObj.deliveryPriceAfterDiscount,
          deliveryPriceDiscount: priceObj.deliveryPriceDiscount,
          // pickupPriceAfterDiscount: parseInt(deliveryPrice, 0),
          pickupPriceAfterDiscount: priceObj.pickupPriceAfterDiscount,
          pickupPriceDiscount: priceObj.pickupPriceDiscount,
          deliveryBasePriceB2B: parseInt(b2bPrice, 0),
          deliveryPriceAfterDiscountB2B: parseInt(b2bPrice, 0),
          deliveryPriceDiscountB2B: priceObj.deliveryPriceDiscountB2B,
          vendorBranchPickupBasePrice: vendorBranchPickupPrice,
          vendorBranchDeliveryBasePrice: vendorBranchDeliveryPrice,
        }),
      )
      .build();

    /**
     * reload vedor Branch Product Price with productId
     * @type
     */
    const jsonList = new BaseListGetDtoBuilder()
      .dtos([
        new VendorBranchProductPriceDto({
          productDto: new ProductDto({ id: product.id }),
        }),
      ])
      .includes(['VendorBranch'])
      .buildJson();
    const putData = {
      data: jsonCrud,
      listDto: jsonList,
    };

    this.props.actions.vendorBranchProductPricePutRequest(putData);
  };

  render() {
    const {
      productPrices,
      productPriceCount,
      productPriceLoading,
      permission,
    } = this.props;

    const {
      deliveryPrice,
      b2bPrice,
      confirmationContent,
      vendorBranchPickupPrice,
      vendorBranchDeliveryPrice,
    } = this.state;
    const productPriceArray = [];
    const columns = [
      {
        title: VENDOR_BRANCH_NAME,
        dataIndex: 'vendorBranchName',
        key: 'vendorBranchName',
        width: 250,
      },
      {
        title: DELIVERY_PRICE,
        dataIndex: 'stringDeliveryPrice',
        key: 'stringDeliveryPrice',
        width: 200,
      },
      {
        title: PICKUP_PRICE,
        dataIndex: 'stringPickupPrice',
        key: 'stringPickupPrice',
        width: 200,
      },
      // {
      //   title: `${PRICE} b2b`,
      //   dataIndex: 'stringB2bPrice',
      //   key: 'stringB2bPrice',
      //   width: 200,
      // },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.showPriceModal(record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </CPButton>
          </div>
        ),
      },
    ];
    let permissionPrice = true;
    Object.keys(permission).map(item => {
      if (
        item === 'price-list-management' &&
        permission[item].permissionAttribute === 'Hide'
      ) {
        permissionPrice = false;
      }
    });

    productPrices.map(item => {
      if (item.published) {
        const obj = {
          key: item.id,
          vendorBranchName: item.vendorBranchDto.name,
          stringDeliveryPrice: permissionPrice
            ? `${item.stringDeliveryBasePrice}`
            : item.stringVendorBranchDeliveryBasePrice,
          stringPickupPrice: permissionPrice
            ? `${item.stringPickupBasePrice}`
            : item.stringVendorBranchPickupBasePrice,
          deliveryPrice: `${item.deliveryBasePrice}`,
          pickupPrice: `${item.pickupBasePrice}`,
          b2bPrice: `${item.deliveryBasePriceB2B}`,
          stringB2bPrice: `${item.stringDeliveryBasePriceB2B}`,
          priceObj: item,
          stringVendorBranchDeliveryPrice:
            item.stringVendorBranchDeliveryBasePrice,
          vendorBranchDeliveryPrice: item.vendorBranchDeliveryBasePrice,
          stringVendorBranchPickupPrice: item.stringVendorBranchPickupBasePrice,
          vendorBranchPickupPrice: item.vendorBranchPickupBasePrice,
        };
        productPriceArray.push(obj);
      }
      return null;
    });

    return (
      <CPPermission>
        <div name="product-management-price">
          <CPList
            data={productPriceArray}
            count={productPriceCount}
            columns={columns}
            loading={productPriceLoading}
            hideSearchBox
            hideAdd
            pagination={false}
          />
          <CPModal
            visible={this.state.visiblePriceModal}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
            title={PRODUCT_PRICE_EDIT}
          >
            <div className="modalContent">
              <div className="row">
                <CPPermission>
                  <div
                    className="col-lg-6 col-sm-12"
                    name="price-list-management"
                  >
                    <label>{DELIVERY_PRICE}</label>
                    <CPInputNumber
                      value={deliveryPrice}
                      // defaultValue={deliveryPrice}
                      onChange={value =>
                        this.handelChangeInput('deliveryPrice', value)
                      }
                      format="Currency"
                    />
                  </div>
                </CPPermission>
                <CPPermission>
                  <div
                    className="col-lg-6 col-sm-12"
                    name="price-list-management"
                  >
                    <label>{PICKUP_PRICE}</label>
                    <CPInputNumber
                      // value={pickupPrice}
                      // onChange={value =>
                      //   this.handelChangeInput('pickupPrice', value)
                      // }
                      value={deliveryPrice}
                      onChange={value =>
                        this.handelChangeInput('deliveryPrice', value)
                      }
                      format="Currency"
                    />
                  </div>
                </CPPermission>
                <div className="col-lg-6 col-sm-12">
                  <label>{`${DELIVERY_PRICE} ${VENDOR_BRANCH}`}</label>
                  <CPInputNumber
                    value={vendorBranchDeliveryPrice}
                    onChange={value =>
                      this.handelChangeInput('vendorBranchDeliveryPrice', value)
                    }
                    format="Currency"
                  />
                </div>
                <div className="col-lg-6 col-sm-12">
                  <label>{`${PICKUP_PRICE} ${VENDOR_BRANCH}`}</label>
                  <CPInputNumber
                    value={vendorBranchPickupPrice}
                    onChange={value =>
                      this.handelChangeInput('vendorBranchPickupPrice', value)
                    }
                    format="Currency"
                  />
                </div>

                <CPPermission>
                  <div
                    className="col-lg-6 col-sm-12"
                    name="price-list-management"
                  >
                    <label>{`${PRICE} b2b`}</label>
                    <CPInputNumber
                      value={b2bPrice}
                      onChange={value =>
                        this.handelChangeInput('b2bPrice', value)
                      }
                      format="Currency"
                    />
                  </div>
                </CPPermission>
              </div>
            </div>
          </CPModal>
          <CPConfirmation
            visible={this.state.visibleConfirmation}
            handleOk={this.handleOkConfirmation}
            handleCancel={this.handleCancelConfirmation}
            content={confirmationContent}
            zIndex={999999}
          />
        </div>
      </CPPermission>
    );
  }
}

const mapStateToProps = state => ({
  product: state.singleProduct.data ? state.singleProduct.data.items[0] : null,
  productPrices: state.vbProductPriceList.data.items,
  productPriceCount: state.vbProductPriceList.data.count,
  productPriceLoading: state.vbProductPriceList.loading,
  permission:
    state.permission.data && state.permission.data.items.length > 0
      ? state.permission.data.items[0].flatermission
      : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      vendorBranchProductPricePutRequest:
        vbProductPricePutActions.vendorBranchProductPricePutRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductPrice));
