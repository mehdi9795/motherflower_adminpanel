import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Select, message } from 'antd';
import { bindActionCreators } from 'redux';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductSpecificationAttribute.css';
import {
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT_SPECIFICATION_ATTRIBUTE,
  CANCEL,
  COLOR,
  COUNT,
  FEATURE,
  FORM_SAVE,
  NO,
  YES,
} from '../../../../Resources/Localization';

import CPList from '../../../CP/CPList';
import {
  ColorOptionDto,
  ProductDto,
  ProductSpecificationAttributeDto,
  SpecificationAttributeDto,
} from '../../../../dtos/catalogDtos';
import CPSelect from '../../../CP/CPSelect';
import colorOptionListActions from '../../../../redux/catalog/actionReducer/colorOption/List';
import specificationAttributeListActions from '../../../../redux/catalog/actionReducer/specificationAttribute/List';
import singleSpecificationAttributeActions from '../../../../redux/catalog/actionReducer/specificationAttribute/Single';
import productSpecificationAttributeDeleteActions from '../../../../redux/catalog/actionReducer/product/ProductSpecificationAttributeDelete';
import productSpecificationAttributePostActions from '../../../../redux/catalog/actionReducer/product/ProductSpecificationAttributePost';
import productSpecificationAttributePutActions from '../../../../redux/catalog/actionReducer/product/ProductSpecificationAttributePut';
import CPPopConfirm from '../../../CP/CPPopConfirm';
import CPButton from '../../../CP/CPButton';
import CPColorPicker from '../../../CP/CPColorPicker';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import CPCarousel from '../../../CP/CPCarousel';
import CPRadio from '../../../CP/CPRadio';
import ProductPrice from '../ProductPrice';
import CPInput from '../../../CP/CPInput';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class ProductSpecificationAttribute extends React.Component {
  static propTypes = {
    productSpecificationAttributes: PropTypes.arrayOf(PropTypes.object),
    images: PropTypes.arrayOf(PropTypes.object),
    productPrices: PropTypes.arrayOf(PropTypes.object),
    specificationAttributes: PropTypes.arrayOf(PropTypes.object),
    colorOptions: PropTypes.arrayOf(PropTypes.object),
    product: PropTypes.objectOf(PropTypes.any),
    singleSpecificationAttribute: PropTypes.objectOf(PropTypes.any),
    productSpecificationAttributeCount: PropTypes.number,
    productSpecificationAttributeLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    product: null,
    images: null,
    productPrices: null,
    singleSpecificationAttribute: null,
    productSpecificationAttributeCount: 0,
    productSpecificationAttributeLoading: false,
    productSpecificationAttributes: null,
    specificationAttributes: null,
    colorOptions: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      specificationAttributeId: '',
      colorOptionsId: 0,
      customValue: '1',
      isEdit: false,
      isColorSpecificationAttribute: true,
      productSpecificationAttributeId: '',
    };
  }

  componentWillMount() {

    this.props.actions.colorOptionListRequest(new BaseGetDtoBuilder().all(true).buildJson());
    this.props.actions.specificationAttributeListRequest(
      new BaseGetDtoBuilder().all(true).buildJson()
    );
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.singleSpecificationAttribute &&
      nextProps.singleSpecificationAttribute.specificationAttributeType ===
        'Color'
    ) {
      if (!this.state.isEdit)
        this.setState({
          isColorSpecificationAttribute: true,
          // colorOptionsId:
          //   nextProps.colorOptions && nextProps.colorOptions.length > 0
          //     ? nextProps.colorOptions[0].id
          //     : 0,
        });
      else
        this.setState({
          isColorSpecificationAttribute: true,
        });
    } else {
      this.setState({
        isColorSpecificationAttribute: false,
      });
    }
  }

  onDelete = id => {
    const { product } = this.props;
    /**
     * reload specification attribute Product
     * @type
     */
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new ProductSpecificationAttributeDto({
          productDto: new ProductDto({ id: product.id }),
        }),
      )
      .disabledCount(false)
      .buildJson();

    const data = {
      id,
      listDto: jsonList,
    };

    this.props.actions.productSpecificationAttributeDeleteRequest(data);
    this.props.actions.singleSpecificationAttributeSuccess(null);
    this.setState({
      specificationAttributeId: '',
      isColorSpecificationAttribute: false,
      colorOptionsId: 0,
      customValue: '',
      productSpecificationAttributeId: '',
      isEdit: false,
    });
  };

  showForm = record => {
    if (record) {

      this.props.actions.singleSpecificationAttributeRequest(
        new BaseGetDtoBuilder().dto(new SpecificationAttributeDto({
          id: record.specificationAttributeId
        })).buildJson(),
      );

      this.setState({
        specificationAttributeId: record.specificationAttributeId.toString(),
        colorOptionsId:
          record.colorOptionsId.toString() === '0' ? '' : record.colorOptionsId,
        customValue:
          record.customValue.toString() === 'true'
            ? ''
            : record.customValue.length > 1 && record.customValue !== '10'
              ? `${record.customValue}+`
              : record.customValue.toString(),
        productSpecificationAttributeId: record.key,
      });
    } else {
      this.setState({
        specificationAttributeId: '',
        colorOptionsId: 0,
        customValue: '',
        productSpecificationAttributeId: '',
      });
    }

    this.setState({
      isEdit: !!record,
    });
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
    if (inputName === 'specificationAttributeId') {
      this.props.actions.singleSpecificationAttributeRequest(
        new BaseGetDtoBuilder()
          .dto(new SpecificationAttributeDto({ id: value }))
          .buildJson(),
      );
    }
  };

  saveForm = () => {
    const {
      specificationAttributeId,
      colorOptionsId,
      customValue,
      isEdit,
      productSpecificationAttributeId,
      isColorSpecificationAttribute,
    } = this.state;
    const { product } = this.props;
    if (colorOptionsId !== 0 || !isColorSpecificationAttribute) {
      let customValueDescription = '';
      const isMore = customValue.indexOf('+');
      if (isMore !== -1)
        customValueDescription = `بیش از ${parseInt(
          customValue.replace('+', ''),
          0,
        ) - 1}`;
      else if (isColorSpecificationAttribute)
        customValueDescription = customValue;
      else customValueDescription = 'true';

      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new ProductSpecificationAttributeDto({
            specificationAttributeDto: new SpecificationAttributeDto({
              id: specificationAttributeId,
            }),
            colorOptionDto: new ColorOptionDto({ id: colorOptionsId }),
            productDto: new ProductDto({ id: product.id }),
            customValue: isColorSpecificationAttribute
              ? customValue.replace('+', '')
              : 'true',
            customValueDescription,
            id: productSpecificationAttributeId,
          }),
        )
        .build();

      /**
       * reload specification attribute Product
       * @type
       */
      const jsonList = new BaseGetDtoBuilder().dto(new ProductSpecificationAttributeDto({
        productDto: new ProductDto({ id: product.id }),
      })).disabledCount(false).buildJson();
      const data = {
        data: jsonCrud,
        listDto: jsonList,
      };
      if (isEdit) {
        this.props.actions.productSpecificationAttributePutRequest(data);
      } else {
        this.props.actions.productSpecificationAttributePostRequest(data);
      }
      this.props.actions.singleSpecificationAttributeSuccess(null);
      this.setState({
        specificationAttributeId: '',
        isColorSpecificationAttribute: false,
        colorOptionsId: 0,
        customValue: '',
        productSpecificationAttributeId: '',
        isEdit: false,
      });
    } else {
      message.error('لطفا رنگ ویژگی را انتخاب کنید.', 7);
    }
  };

  cancelForm = () => {
    this.setState({
      specificationAttributeId: '',
      isColorSpecificationAttribute: false,
      colorOptionsId: 0,
      customValue: '',
      productSpecificationAttributeId: '',
      isEdit: false,
    });
  };

  render() {
    const {
      productSpecificationAttributes,
      productSpecificationAttributeCount,
      productSpecificationAttributeLoading,
      specificationAttributes,
      colorOptions,
      images,
      productPrices,
    } = this.props;
    const {
      specificationAttributeId,
      customValue,
      isColorSpecificationAttribute,
      colorOptionsId,
    } = this.state;

    const productSpecificationAttributeArray = [];
    const specificationAttributeArray = [];
    const colorOptionsArray = [];
    const dataImage = [];

    const radioModel = [];

    for (let i = 1; i <= 10; i += 1) {
      radioModel.push({ name: i.toString(), value: i.toString() });
      if (i === 5) radioModel.push({ name: '5+', value: '6.5+' });
    }
    radioModel.push(
      {
        value: '11+',
        name: '10+',
      },
      {
        value: '16+',
        name: '15+',
      },
      {
        value: '21+',
        name: '20+',
      },
      {
        value: '31+',
        name: '30+',
      },
      {
        value: '41+',
        name: '40+',
      },
      {
        value: '51+',
        name: '50+',
      },
      {
        value: '61+',
        name: '60+',
      },
      {
        value: '71+',
        name: '70+',
      },
      {
        value: '81+',
        name: '80+',
      },
      {
        value: '91+',
        name: '90+',
      },
      {
        value: '101+',
        name: '100+',
      },
      {
        value: '121+',
        name: '120+',
      },
      {
        value: '151+',
        name: '150+',
      },
      {
        value: '181+',
        name: '180+',
      },
      {
        value: '201+',
        name: '200+',
      },
    );
    const columns = [
      {
        title: FEATURE,
        dataIndex: 'name',
        key: 'name',
        width: 250,
        render: (text, record) => (
          <div>
            <label
              className={s.productName}
              onClick={() => this.showForm(record)}
            >
              {record.name}
            </label>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <CPPermission>
            <div name="product-management-spec">
              <CPPopConfirm
                title={
                  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT_SPECIFICATION_ATTRIBUTE
                }
                okText={YES}
                cancelText={NO}
                okType="primary"
                onConfirm={() => this.onDelete(record.key)}
              >
                <CPButton className="delete_action">
                  <i className="cp-trash" />
                </CPButton>
              </CPPopConfirm>
            </div>
          </CPPermission>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <CPPermission>
            <div name="product-management-spec">
              <CPButton
                onClick={() => this.showForm(record)}
                className="edit_action"
              >
                <i className="cp-edit" />
              </CPButton>
            </div>
          </CPPermission>
        ),
      },
    ];

    productSpecificationAttributes.map(item => {
      let name = '';
      if (
        item.specificationAttributeDto.specificationAttributeType === 'Color'
      ) {
        name = `${item.specificationAttributeDto.name} ${
          item.colorOptionDto.name
        } ${item.customValueDescription} ${item.customValue ? ' عدد' : ''}`;
      } else if (
        item.specificationAttributeDto.specificationAttributeType === 'CheckBox'
      ) {
        name = `${item.specificationAttributeDto.name} ${
          item.customValue === 'true' ? ' دارد' : 'ندارد'
        }`;
      }
      const obj = {
        key: item.id,
        name,
        specificationAttributeId: item.specificationAttributeDto.id,
        colorOptionsId: item.colorOptionDto.id,
        customValue: item.customValue,
      };
      productSpecificationAttributeArray.push(obj);
      return null;
    });

    if (specificationAttributes) {
      specificationAttributes.map(item =>
        specificationAttributeArray.push(
          <Option key={item.id}>{item.name}</Option>,
        ),
      );
    }
    if (colorOptions) {
      colorOptions.map(item =>
        colorOptionsArray.push(<option key={item.id}>{item.name}</option>),
      );
    }
    if (images) {
      images.map(item => {
        const srcSetArray = [];
        srcSetArray.push({ src: `${item.url}`, vw: '500w' });
        srcSetArray.push({ src: `${item.url}`, vw: '1426w' });
        const obj = {
          small: `${item.url}`,
          large: `${item.url}`,
          key: item.id,
          // srcSet: srcSetArray, TODO: It was commented on the error
        };
        dataImage.push(obj);
        return null;
      });
    }

    return (
      <CPPermission>
        <div className="row" name="product-management-specification">
          <div className="col-lg-6 col-sm-12">
            <div className="col-sm-12">
              <ProductPrice productPrices={productPrices} />
            </div>
            <CPPermission>
              <div
                name="product-management-spec"
                className="col-sm-12 attributeDiv"
              >
                <div className="col-sm-12">
                  <label>{FEATURE}</label>
                  <CPSelect
                    showSearch
                    value={specificationAttributeId}
                    onChange={value => {
                      this.handelChangeInput('specificationAttributeId', value);
                    }}
                  >
                    {specificationAttributeArray}
                  </CPSelect>
                </div>
                {isColorSpecificationAttribute && (
                  <div className="col-sm-12">
                    <label>{COLOR}</label>
                    <CPColorPicker
                      data={colorOptions || []}
                      defaultValue={colorOptionsId}
                      onChange={value =>
                        this.handelChangeInput(
                          'colorOptionsId',
                          parseInt(value, 0),
                        )
                      }
                    />
                  </div>
                )}
                {isColorSpecificationAttribute && (
                  <div className="col-sm-12">
                    <label>{COUNT}</label>
                    <CPRadio
                      model={radioModel}
                      defaultValue={customValue}
                      customRadio
                      onChange={value =>
                        this.handelChangeInput(
                          'customValue',
                          value.target.value,
                        )
                      }
                    />
                  </div>
                )}
                <div className={cs('col-sm-12', s.saveButton)}>
                  <CPButton
                    icon="save"
                    disabled={specificationAttributeId === ''}
                    onClick={this.saveForm}
                  >
                    {FORM_SAVE}
                  </CPButton>
                  {specificationAttributeId !== '' && (
                    <CPButton icon="cancel" onClick={this.cancelForm}>
                      {CANCEL}
                    </CPButton>
                  )}
                </div>
              </div>
            </CPPermission>
            <div className="col-sm-12">
              <CPList
                data={productSpecificationAttributeArray}
                count={productSpecificationAttributeCount}
                columns={columns}
                loading={productSpecificationAttributeLoading}
                hideSearchBox
                hideAdd
                pagination={false}
              />
            </div>
          </div>
          <div className={cs('col-lg-6 col-sm-12')}>
            <div className={cs('col-lg-10', s.centerCarousel)}>
              <CPCarousel data={dataImage} width={700} height={700} />
            </div>
          </div>
        </div>
      </CPPermission>
    );
  }
}

const mapStateToProps = state => ({
  product: state.singleProduct.data ? state.singleProduct.data.items[0] : null,
  productSpecificationAttributes: state.productSpecificationAttributeList.data
    ? state.productSpecificationAttributeList.data.items
    : null,
  productSpecificationAttributeCount: state.productSpecificationAttributeList
    .data
    ? state.productSpecificationAttributeList.data.count
    : 0,
  productSpecificationAttributeLoading:
    state.productSpecificationAttributeList.loading,
  colorOptions: state.colorOptionList.data
    ? state.colorOptionList.data.items
    : null,
  specificationAttributes: state.specificationAttributeList.data
    ? state.specificationAttributeList.data.items
    : null,
  singleSpecificationAttribute:
    state.singleSpecificationAttribute.data &&
    state.singleSpecificationAttribute.data.items
      ? state.singleSpecificationAttribute.data.items[0]
      : null,
  images: state.imageList.data ? state.imageList.data.items : null,
  productPrices: state.vbProductPriceList.data.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      colorOptionListRequest: colorOptionListActions.colorOptionListRequest,
      specificationAttributeListRequest:
        specificationAttributeListActions.specificationAttributeListRequest,
      singleSpecificationAttributeRequest:
        singleSpecificationAttributeActions.singleSpecificationAttributeRequest,
      productSpecificationAttributePutRequest:
        productSpecificationAttributePutActions.productSpecificationAttributePutRequest,
      productSpecificationAttributePostRequest:
        productSpecificationAttributePostActions.productSpecificationAttributePostRequest,
      productSpecificationAttributeDeleteRequest:
        productSpecificationAttributeDeleteActions.productSpecificationAttributeDeleteRequest,
      singleSpecificationAttributeSuccess:
        singleSpecificationAttributeActions.singleSpecificationAttributeSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductSpecificationAttribute));
