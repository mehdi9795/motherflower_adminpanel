import React from 'react';
import { connect } from 'react-redux';
import HotKeys from 'react-hot-keys';
import { AutoComplete, Select, message } from 'antd';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductInfo.css';
import cs from 'classnames';
import CPInput from '../../../CP/CPInput/CPInput';
import {
  ACTIVE,
  ADMIN_COMMENT,
  DEACTIVATE,
  DESCRIPTION,
  FORM_SAVE,
  FORM_SAVE_AND_CONTINUE,
  INFO_BASIC,
  CATEGORY,
  PRODUCT_LIST_CODE,
  PRODUCT_LIST_NAME,
  PRODUCT_LIST_VENDOR_BRANCHES,
  PRODUCT_PREPARATION_TIME,
  PRODUCT_PRODUCT_TYPE,
  PRODUCT_TAGS,
  RELATED_CATEGORY,
  STATUS,
  VENDOR_NAME,
  DAY_SELECT,
  CATEGORY_CODE,
  VENDOR_CODE,
  PRODUCT_LIST_CODE_VENDOR,
  CATEGORY_DESCRIPTION,
  INFO_COMPLETE,
  MARKED,
  IS,
  IS_NOT,
  ID,
} from '../../../../Resources/Localization';
import CPButton from '../../../CP/CPButton';
import CPPanel from '../../../CP/CPPanel';
import CPTextArea from '../../../CP/CPTextArea';
import CPMultiSelect from '../../../CP/CPMultiSelect';
import CPSelect from '../../../CP/CPSelect';
import CPTreeSelect from '../../../CP/CPTreeSelect';
import CPSwitch from '../../../CP/CPSwitch';
import {
  CategoryDto,
  ProductCategoryDto,
  ProductDto,
  ProductNameSearchDto,
  ProductRelatedCategoryDto,
  ProductTagDto,
  ProductTypeDto,
  TagDto,
  VendorBranchProductDto,
} from '../../../../dtos/catalogDtos';
import { AgentDto, VendorBranchDto } from '../../../../dtos/vendorDtos';
import productDeleteActions from '../../../../redux/catalog/actionReducer/product/ProductDelete';
import productPostActions from '../../../../redux/catalog/actionReducer/product/ProductPost';
import productPutActions from '../../../../redux/catalog/actionReducer/product/ProductPut';
import productNameListActions from '../../../../redux/catalog/actionReducer/product/ProductNameList';
import productTagActions from '../../../../redux/catalog/actionReducer/product/ProductTag';
import relatedCategoryListActons from '../../../../redux/catalog/actionReducer/category/RelatedCategoryList';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import CPAlert from '../../../CP/CPAlert';
import loadingActions from '../../../../redux/shared/actionReducer/loading/loading';
import { getTagApi } from '../../../../services/catalogApi';
import { getDtoQueryString } from '../../../../utils/helper';
import { BaseGetDtoBuilder } from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class ProductInfo extends React.Component {
  static propTypes = {
    categories: PropTypes.arrayOf(PropTypes.object),
    relatedCategories: PropTypes.arrayOf(PropTypes.object),
    productNames: PropTypes.arrayOf(PropTypes.object),
    vendors: PropTypes.arrayOf(PropTypes.object),
    tags: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    productType: PropTypes.arrayOf(PropTypes.object),
    product: PropTypes.objectOf(PropTypes.any),
    singleVendor: PropTypes.objectOf(PropTypes.any),
    vendorIdFromCreate: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    backUrl: PropTypes.string.isRequired,
    loginData: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    product: {},
    categories: [],
    vendors: [],
    tags: [],
    vendorBranches: [],
    productNames: null,
    singleVendor: null,
    productType: null,
    vendorIdFromCreate: '',
    relatedCategories: null,
    loginData: {},
  };

  constructor(props) {
    super(props);
    const vendorBranchSelectedIds = [];
    const tagSelectedIds = [];
    const relatedCategorySelectedIds = [];
    let vendorCode = '';
    let categoryCode = '';
    let categoryDescription = '';

    if (props.product && props.product.vendorBranchProductDtos) {
      props.product.vendorBranchProductDtos.map(item => {
        if (item.published)
          vendorBranchSelectedIds.push(item.vendorBranchDto.id.toString());
      });
      vendorCode =
        props.product.vendorBranchProductDtos[0].vendorBranchDto.vendorDto.code;
    } else {
      vendorCode = props.vendors.find(
        item => item.id.toString() === props.vendorIdFromCreate,
      ).code;
    }
    if (props.product && props.product.productTagDtos) {
      props.product.productTagDtos.map(item =>
        tagSelectedIds.push(item.tagDto.id.toString()),
      );
    }

    if (props.product && props.product.productRelatedCategoryDto) {
      props.product.productRelatedCategoryDto.map(item =>
        relatedCategorySelectedIds.push(item.categoryDto.id.toString()),
      );
    }

    if (props.product && props.product.productCategoryDtos) {
      categoryCode = props.product.productCategoryDtos[0].categoryDto.deepCode;
      categoryDescription =
        props.product.productCategoryDtos[0].categoryDto.description;
    }

    this.state = {
      name: props.product && props.product.id ? props.product.name : '',
      code: props.product ? props.product.code : '',
      vendorCode,
      categoryCode,
      description: props.product ? props.product.description : '',
      adminComment: props.product ? props.product.adminComment : '',
      markAsNew: props.product ? props.product.markAsNew : false,
      takesTime: props.product ? props.product.takesTime : 0,
      takesTimeDay:
        props.product && props.product.takesTime
          ? props.product.takesTime.split(':')[0]
          : '0',
      takesTimeHour:
        props.product && props.product.takesTime
          ? props.product.takesTime.split(':')[1]
          : '0',
      takesTimeMinute:
        props.product && props.product.takesTime
          ? props.product.takesTime.split(':')[2]
          : '30',
      markAsNewStartDateTime: props.product
        ? props.product.markAsNewStartPersianDateTimeUtc
        : null,
      markAsNewEndDateTime: props.product
        ? props.product.markAsNewEndPersianDateTimeUtc
        : null,
      availableStartDateTime: props.product
        ? props.product.availableStartPersianDateTimeUtc
        : null,
      availableEndDateTime: props.product
        ? props.product.availableEndPersianDateTimeUtc
        : null,
      inventoryStatus: props.product ? props.product.inventoryStatus : true,
      inventoryStartDateTime: props.product
        ? props.product.inventoryStartPersianDateTimeUtc
        : null,
      inventoryEndDateTime: props.product
        ? props.product.inventoryEndPersianDateTimeUtc
        : null,
      published: props.product ? props.product.published : false,
      displayOrder: props.product ? props.product.displayOrder : '',
      vendorId:
        props.product && props.product.vendorDto
          ? props.product.vendorDto.id.toString()
          : props.vendorIdFromCreate,
      vendorBranchId: vendorBranchSelectedIds,
      categoryId:
        props.product &&
        props.product.productCategoryDtos &&
        props.product.productCategoryDtos.length > 0
          ? `${props.product.productCategoryDtos[0].categoryDto.id}*${
              props.product.productCategoryDtos[0].categoryDto.parentCategoryId
            }*${props.product.productCategoryDtos[0].categoryDto.deepCode}*${
              props.product.productCategoryDtos[0].categoryDto.description
            }`
          : '',
      tagIds: tagSelectedIds,
      id: props.product ? props.product.id : 0,
      productTypeId:
        props.singleVendor &&
        props.singleVendor.vendorProductTypeDtos &&
        props.singleVendor.vendorProductTypeDtos.length > 0
          ? props.singleVendor.vendorProductTypeDtos[0].productTypeDto.id.toString()
          : '',
      relatedCategoryIds: relatedCategorySelectedIds,
      vendorBranchDisabled: vendorBranchSelectedIds.length === 1,
      dataSourceName:
        props.product && props.product.id
          ? [{ text: props.product.name, value: props.product.name }]
          : [],
      categoryDescription,
      marked: props.product && props.product.id ? props.product.marked : false,
      b2bSupport:
        props.product && props.product.id ? props.product.b2bSupport : false,
    };
    this.handelChangeInput = this.handelChangeInput.bind(this);
  }

  componentWillMount() {
    const { product, vendorBranches } = this.props;

    if (product && product.id) {
      const jsonLsit = new BaseGetDtoBuilder()
        .dto(
          new CategoryDto({
            parentCategoryId:
              product.productCategoryDtos[0].categoryDto.parentCategoryId,
            published: true,
          }),
        )
        .notExpectedIds(product.productCategoryDtos[0].categoryDto.id)
        .buildJson();
      this.props.actions.relatedCategoryListRequest(jsonLsit);
    } else {
      const vendorBranchIds = [];
      vendorBranches.map(item => vendorBranchIds.push(item.id.toString()));
      this.setState({
        vendorBranchId: vendorBranchIds,
        vendorBranchDisabled: vendorBranchIds.length === 1,
      });
    }
  }

  componentDidMount() {
    this.props.actions.loadingHideRequest();
  }

  componentWillReceiveProps(nextProps) {
    const ids = [];
    if (nextProps.relatedCategories)
      nextProps.relatedCategories.map(item => ids.push(item.id.toString()));
    this.setState({ relatedCategoryIds: ids });

    if (nextProps.productNames) {
      const productNameArray = [];
      nextProps.productNames.map(item => {
        productNameArray.push({ value: item, text: item });
        return null;
      });
      this.setState({ dataSourceName: productNameArray });
    }
  }

  onChangeVendorBranchSelect = value => {
    this.setState({ vendorBranchId: value });
  };

  onChangeRelatedCategory = value => {
    this.setState({ relatedCategoryIds: value });
  };

  onChangeTagSelect = value => {
    this.setState({ tagIds: value });
  };

  onSelect = (value, option) => {
    this.setState({ name: option.props.children });
  };

  handleSearch = value => {
    this.setState({ name: value });
    if (value.length > 2) {
      const { product, vendorIdFromCreate } = this.props;

      const jsonList = new BaseGetDtoBuilder()
        .dto(
          new ProductNameSearchDto({
            name: value,
            vendorDto: new AgentDto({
              id:
                product && product.vendorDto
                  ? product.vendorDto.id
                  : vendorIdFromCreate,
            }),
          }),
        )
        .fromCache(true)
        .buildJson();
      this.props.actions.productNameListRequest(jsonList);
    }
  };

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  async handelChangeInput(inputName, value) {
    const { loginData } = this.props;
    if (inputName === 'categoryId') {
      this.setState({
        [inputName]: value,
        relatedCategoryIds: [],
        categoryCode: value.split('*')[2],
        categoryDescription: value.split('*')[3],
      });

      const jsonList = new BaseGetDtoBuilder()
        .dto(
          new CategoryDto({
            parentCategoryId: value.split('*')[1],
            published: true,
            justChild: true,
          }),
        )
        .notExpectedIds([value.split('*')[0]])
        .buildJson();
      this.props.actions.relatedCategoryListRequest(jsonList);
      /**
       * get all tags with true status
       */

      const tagData = await getTagApi(
        loginData.token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new TagDto({
                published: true,
                categoryTagDtos: { categoryDto: { id: value.split('*')[0] } },
              }),
            )
            .buildJson(),
        ),
      );

      if (tagData.status === 200) {
        this.props.actions.productTagSuccess(tagData.data);

        const tagIds = [];
        if (tagData.data && tagData.data.items)
          tagData.data.items.map(item => tagIds.push(item.id.toString()));
        this.setState({ tagIds });
      }
    } else {
      this.setState({ [inputName]: value });
    }
  }

  prepareContainer = () => {
    const {
      name,
      code,
      description,
      adminComment,
      markAsNew,
      takesTimeDay,
      // takesTimeHour,
      // takesTimeMinute,
      markAsNewStartDateTime,
      markAsNewEndDateTime,
      availableStartDateTime,
      availableEndDateTime,
      inventoryStatus,
      inventoryStartDateTime,
      inventoryEndDateTime,
      published,
      vendorId,
      vendorBranchId,
      categoryId,
      tagIds,
      id,
      productTypeId,
      relatedCategoryIds,
      marked,
      b2bSupport,
    } = this.state;

    const tagIdsArray = [];
    const vendorBranchIdsArray = [];
    const relatedCategoryIdsArray = [];

    tagIds.map(item => {
      tagIdsArray.push(new ProductTagDto({ tagDto: new TagDto({ id: item }) }));
      return null;
    });

    vendorBranchId.map(item => {
      vendorBranchIdsArray.push(
        new VendorBranchProductDto({
          vendorBranchDto: new VendorBranchDto({ id: item }),
        }),
      );
      return null;
    });
    if (relatedCategoryIds.length > 0) {
      relatedCategoryIds.map(item => {
        relatedCategoryIdsArray.push(
          new ProductRelatedCategoryDto({
            categoryDto: new CategoryDto({ id: item }),
          }),
        );
        return null;
      });
    }

    const json = new BaseGetDtoBuilder()
      .dto(
        new ProductDto({
          id,
          name,
          marked,
          adminComment,
          availableEndPersianDateTimeUtc: availableEndDateTime || '',
          availableStartPersianDateTimeUtc: availableStartDateTime || '',
          code,
          description,
          inventoryEndPersianDateTimeUtc:
            inventoryStatus === true && inventoryEndDateTime
              ? inventoryEndDateTime
              : '',
          inventoryStatus,
          inventoryStartPersianDateTimeUtc:
            inventoryStatus === true && inventoryStartDateTime
              ? inventoryStartDateTime
              : '',
          markAsNew,
          markAsNewEndPersianDateTimeUtc:
            markAsNew === true && markAsNewEndDateTime
              ? markAsNewEndDateTime
              : '',
          markAsNewStartPersianDateTimeUtc:
            markAsNew === true && markAsNewStartDateTime
              ? markAsNewStartDateTime
              : '',
          published,
          takesTime: takesTimeDay !== '0' ? `${takesTimeDay}:0:0` : undefined,
          productCategoryDtos: new ProductCategoryDto({
            categoryDto: new CategoryDto({
              id: categoryId.split('*')[0],
            }),
          }),
          vendorDto: new AgentDto({ id: vendorId }),
          productTagDtos: tagIdsArray,
          vendorBranchProductDtos: vendorBranchIdsArray,
          productTypeDto: new ProductTypeDto({ id: productTypeId }),
          productRelatedCategoryDto: relatedCategoryIdsArray,
          b2bSupport,
        }),
      )
      .build();
    return json;
  };

  saveForm = () => {
    const { categoryId, tagIds, name, code, vendorBranchId } = this.state;
    if (code !== '')
      if (name !== '') {
        if (tagIds.length > 0)
          if (categoryId.split('*')[0] > 0)
            if (vendorBranchId.length > 0) {
              const { product } = this.props;
              const container = this.prepareContainer();
              const isEdit = product && product.id;
              if (isEdit) this.putData(container, false);
              else this.postData(container, false);
            } else {
              message.error('حداقل باید یک شعبه انتخاب شود.', 5);
            }
          else {
            message.error('دسته بندی را انتخاب کنید.', 5);
          }
        else message.error('حداقل باید یک تگ انتخاب شود.', 5);
      } else {
        message.error('نام محصول را وارد کنید.', 5);
      }
    else message.error('کد محصول را وارد کنید.');
  };

  saveFormAndContinue = () => {
    const { categoryId, tagIds, name, code, vendorBranchId } = this.state;
    if (code !== '')
      if (name !== '') {
        if (tagIds.length > 0)
          if (categoryId.split('*')[0] > 0)
            if (vendorBranchId.length > 0) {
              const container = this.prepareContainer();
              const { product } = this.props;
              const isEdit = product && product.id;

              if (isEdit) this.putData(container, true);
              else this.postData(container, true);
            } else {
              message.error('حداقل باید یک شعبه انتخاب شود.', 5);
            }
          else {
            message.error('دسته بندی را انتخاب کنید.', 5);
          }
        else message.error('حداقل باید یک تگ انتخاب شود.', 5);
      } else {
        message.error('نام محصول را وارد کنید.', 5);
      }
    else message.error('کد محصول را وارد کنید.');
  };

  postData(container, saveAndContinue) {
    const { backUrl } = this.props;
    const jsonList = new BaseGetDtoBuilder()
      .dto(new ProductDto({ published: true }))
      .includes([
        'vendorDto',
        'vendorBranches',
        'productCategoryDtos',
        'imageDtos',
      ])
      .buildJson();
    const data = {
      data: container,
      listDto: jsonList,
      status: saveAndContinue ? 'saveAndContinue' : 'save',
      backUrl,
    };

    this.props.actions.productPostRequest(data);
  }

  putData(container, saveAndContinue) {
    const { backUrl } = this.props;
    const jsonList = new BaseGetDtoBuilder()
      .dto(new ProductDto({ published: true }))
      .includes([
        'vendorDto',
        'vendorBranches',
        'productCategoryDtos',
        'imageDtos',
      ])
      .buildJson();

    const data = {
      data: container,
      listDto: jsonList,
      status: saveAndContinue ? 'saveAndContinue' : 'save',
      backUrl,
    };

    this.props.actions.productPutRequest(data);
  }

  render() {
    const {
      categories,
      vendors,
      tags,
      vendorBranches,
      productType,
      relatedCategories,
      product,
    } = this.props;
    const {
      name,
      code,
      description,
      adminComment,
      // markAsNew,
      takesTimeDay,
      // takesTimeHour,
      // takesTimeMinute,
      // markAsNewStartDateTime,
      // markAsNewEndDateTime,
      // availableStartDateTime,
      // availableEndDateTime,
      // inventoryStatus,
      // inventoryStartDateTime,
      // inventoryEndDateTime,
      published,
      vendorId,
      vendorBranchId,
      categoryId,
      tagIds,
      productTypeId,
      relatedCategoryIds,
      vendorBranchDisabled,
      dataSourceName,
      categoryCode,
      vendorCode,
      categoryDescription,
      marked,
      b2bSupport,
    } = this.state;
    const vendorArray = [];
    const tagArray = [];
    const vendorBranchArray = [];
    const productTypeArray = [];
    const relatedCategoryArray = [];
    const notExist = [];

    /**
     * map vendors for comboBox Datasource
     */
    vendors.map(item =>
      vendorArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    tags.map(item =>
      tagArray.push(<Option key={item.id}>{item.title}</Option>),
    );

    vendorBranches.map(item =>
      vendorBranchArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    productType.map(item =>
      productTypeArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    if (relatedCategories) {
      relatedCategories.map(item =>
        relatedCategoryArray.push(<Option key={item.id}>{item.name}</Option>),
      );
    }
    if (product && product.vendorBranchProductDtos) {
      product.vendorBranchProductDtos.map(item => {
        if (!item.inventoryStatus) {
          const text = product.adminComment;
          notExist.push(
            <CPAlert
              type="error"
              description={text}
              key={item.vendorBranchDto.id}
            />,
          );
        }
        return null;
      });
    }

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.saveFormAndContinue}>
        <CPPermission>
          <div name="product-management-info">
            <div className="tabContent">
              <div className="row">
                <div className="col-lg-6 col-sm-12">
                  <CPPanel header={INFO_BASIC} key={1}>
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{CATEGORY}</label>
                        <CPTreeSelect
                          model={categories}
                          onChange={value =>
                            this.handelChangeInput('categoryId', value)
                          }
                          withParentId
                          withCategoryCode
                          value={categoryId}
                          parentDisabled
                        />
                      </div>
                      <div className="col-lg-6 col-sm-12">
                        <label>{CATEGORY_DESCRIPTION}</label>
                        <CPInput value={categoryDescription} disabled />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{PRODUCT_LIST_NAME}</label>
                        <AutoComplete
                          value={name}
                          className={s.areaInput}
                          dataSource={dataSourceName}
                          onSelect={this.onSelect}
                          onSearch={this.handleSearch}
                          placeholder={PRODUCT_LIST_NAME}
                        />
                      </div>
                      <CPPermission>
                        <div
                          name="product-list-code"
                          className="col-lg-6 col-sm-12"
                        >
                          <label>{PRODUCT_LIST_CODE}</label>
                          <div className={s.productCode}>
                            <CPInput
                              name="code"
                              value={code}
                              placeholder={PRODUCT_LIST_CODE}
                              onChange={value =>
                                this.handelChangeInput(
                                  'code',
                                  value.target.value,
                                )
                              }
                              parentClassName={s.productCodeSection1}
                            />
                            <CPInput
                              name="p"
                              value="P"
                              disabled
                              parentClassName={s.productCodeSectionP}
                            />
                            <CPInput
                              name="categoryCode"
                              value={categoryCode}
                              placeholder={CATEGORY_CODE}
                              disabled
                              parentClassName={s.productCodeSection2}
                            />
                            <CPInput
                              name="vendorCode"
                              value={vendorCode}
                              placeholder={VENDOR_CODE}
                              disabled
                              parentClassName={s.productCodeSection1}
                            />
                          </div>
                        </div>
                      </CPPermission>
                    </div>
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{ID}</label>
                        <CPInput
                          name="code"
                          value={product ? product.id.toString() : '0'}
                          placeholder={PRODUCT_LIST_CODE}
                          onChange={value =>
                            this.handelChangeInput('code', value.target.value)
                          }
                          disabled
                        />
                      </div>
                      <CPPermission>
                        <div
                          className={cs('col-lg-6 col-sm-12', s.status)}
                          name="product-management-info-active"
                        >
                          <div className="align-margin">
                            <label>{STATUS}</label>
                            <CPSwitch
                              defaultChecked={published}
                              unCheckedChildren={DEACTIVATE}
                              checkedChildren={ACTIVE}
                              onChange={value =>
                                this.handelChangeInput('published', value)
                              }
                            />
                          </div>
                          <div className="align-margin">
                            <label>{MARKED}</label>
                            <CPSwitch
                              defaultChecked={marked}
                              unCheckedChildren={IS_NOT}
                              checkedChildren={IS}
                              onChange={value =>
                                this.handelChangeInput('marked', value)
                              }
                            />
                          </div>
                          <div className="align-margin">
                            <label>B2B</label>
                            <CPSwitch
                              defaultChecked={b2bSupport}
                              unCheckedChildren={DEACTIVATE}
                              checkedChildren={ACTIVE}
                              onChange={value =>
                                this.handelChangeInput('b2bSupport', value)
                              }
                            />
                          </div>
                        </div>
                      </CPPermission>
                    </div>
                    {productType.length > 1 && (
                      <div className="row">
                        <div className="col-sm-12">
                          <label>{PRODUCT_PRODUCT_TYPE}</label>
                          <CPSelect
                            name="productTypeId"
                            value={productTypeId}
                            onChange={value =>
                              this.handelChangeInput('productTypeId', value)
                            }
                          >
                            {productTypeArray}
                          </CPSelect>
                        </div>
                      </div>
                    )}
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{PRODUCT_TAGS}</label>
                        <CPMultiSelect
                          onChange={this.onChangeTagSelect}
                          placeholder=""
                          value={tagIds}
                        >
                          {tagArray}
                        </CPMultiSelect>
                      </div>
                    </div>
                    {notExist}
                  </CPPanel>
                </div>
                <div className="col-lg-6 col-sm-12">
                  <CPPanel header={INFO_COMPLETE} key={1}>
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{VENDOR_NAME}</label>
                        <CPSelect
                          name="vendorId"
                          disabled
                          value={vendorId}
                          onChange={value =>
                            this.handelChangeInput('vendorId', value)
                          }
                        >
                          {vendorArray}
                        </CPSelect>
                      </div>
                      <div className="col-lg-6 col-sm-12">
                        <label>{PRODUCT_LIST_VENDOR_BRANCHES}</label>
                        <CPMultiSelect
                          onChange={this.onChangeVendorBranchSelect}
                          placeholder={PRODUCT_LIST_VENDOR_BRANCHES}
                          value={vendorBranchId}
                          // disabled={vendorBranchDisabled}
                        >
                          {vendorBranchArray}
                        </CPMultiSelect>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{DESCRIPTION}</label>
                        <CPTextArea
                          name="description"
                          autoSize
                          value={description}
                          onChange={value =>
                            this.handelChangeInput(
                              'description',
                              value.target.value,
                            )
                          }
                          className="textArea"
                        />
                      </div>
                      {/* <div className="col-sm-12">
                      <label>{ADMIN_COMMENT}</label>
                      <CPTextArea
                        name="adminComment"
                        autosize
                        value={adminComment}
                        onChange={value =>
                          this.handelChangeInput(
                            'adminComment',
                            value.target.value,
                          )
                        }
                      />
                    </div> */}
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <label>{PRODUCT_PREPARATION_TIME}</label>
                        <CPSelect
                          name="takesTimeDay"
                          value={takesTimeDay}
                          onChange={value =>
                            this.handelChangeInput('takesTimeDay', value)
                          }
                        >
                          <Option key="0">{DAY_SELECT}</Option>
                          <Option key="1">1</Option>
                          <Option key="2">2</Option>
                          <Option key="3">3</Option>
                        </CPSelect>
                      </div>
                    </div>
                    {relatedCategories &&
                      relatedCategories.length > 0 && (
                        <div className="row">
                          <div className="col-sm-12">
                            <label>{RELATED_CATEGORY}</label>
                            <CPMultiSelect
                              onChange={this.onChangeRelatedCategory}
                              placeholder={RELATED_CATEGORY}
                              value={relatedCategoryIds}
                            >
                              {relatedCategoryArray}
                            </CPMultiSelect>
                          </div>
                        </div>
                      )}
                  </CPPanel>
                </div>
                {/* <div className="col-lg-6 col-sm-12">
                <CPPanel header={SETTING}>
                  <div className="row">
                    <div className="col-sm-12">
                      <div className="align-margin">
                        <CPSwitch
                          defaultChecked={inventoryStatus}
                          unCheckedChildren="ناموجود"
                          checkedChildren="موجود"
                          onChange={value => {
                            this.handelChangeInput('inventoryStatus', value);
                          }}
                        />
                      </div>
                    </div>
                  </div>
                  {inventoryStatus && (
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{FROM_DATE}</label>
                        <CPPersianCalendar
                          placeholder={FROM_DATE}
                          onChange={(unix, formatted) =>
                            this.handelChangeInput(
                              'inventoryStartDateTime',
                              formatted,
                            )
                          }
                          preSelected={inventoryStartDateTime}
                        />
                      </div>
                      <div className="col-lg-6 col-sm-12">
                        <label>{TO_DATE}</label>
                        <CPPersianCalendar
                          placeholder={TO_DATE}
                          onChange={(unix, formatted) =>
                            this.handelChangeInput(
                              'inventoryEndDateTime',
                              formatted,
                            )
                          }
                          preSelected={inventoryEndDateTime}
                        />
                      </div>
                    </div>
                  )}

                  <div className="row">
                    <div className="col-sm-12">
                      <div className="align-margin">
                        <CPSwitch
                          defaultChecked={markAsNew}
                          unCheckedChildren="محصول ویژه"
                          checkedChildren="محصول ویژه"
                          onChange={value => {
                            this.handelChangeInput('markAsNew', value);
                          }}
                        />
                      </div>
                    </div>
                  </div>

                  {markAsNew && (
                    <div className="row">
                      <div className="col-lg-6 col-sm-12">
                        <label>{FROM_DATE}</label>
                        <CPPersianCalendar
                          placeholder={FROM_DATE}
                          onChange={(unix, formatted) =>
                            this.handelChangeInput(
                              'markAsNewStartDateTime',
                              formatted,
                            )
                          }
                          preSelected={markAsNewStartDateTime}
                        />
                      </div>
                      <div className="col-lg-6 col-sm-12">
                        <label>{TO_DATE}</label>
                        <CPPersianCalendar
                          placeholder={TO_DATE}
                          onChange={(unix, formatted) =>
                            this.handelChangeInput(
                              'markAsNewEndDateTime',
                              formatted,
                            )
                          }
                          preSelected={markAsNewEndDateTime}
                        />
                      </div>
                    </div>
                  )}

                  <div className="row">
                    <div className="col-sm-12">
                      <h4>نمایش محصول</h4>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{FROM_DATE}</label>
                      <CPPersianCalendar
                        placeholder={FROM_DATE}
                        onChange={(unix, formatted) =>
                          this.handelChangeInput(
                            'availableStartDateTime',
                            formatted,
                          )
                        }
                        preSelected={availableStartDateTime}
                      />
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <label>{TO_DATE}</label>
                      <CPPersianCalendar
                        placeholder={TO_DATE}
                        onChange={(unix, formatted) =>
                          this.handelChangeInput(
                            'availableEndDateTime',
                            formatted,
                          )
                        }
                        preSelected={availableEndDateTime}
                      />
                    </div>
                  </div>
                </CPPanel>
              </div> */}
              </div>
            </div>
            <div className={s.textLeft}>
              <CPButton
                icon="form"
                className="btn-primary"
                onClick={this.saveFormAndContinue}
              >
                {FORM_SAVE_AND_CONTINUE}
              </CPButton>
              <CPButton icon="save" onClick={this.saveForm}>
                {FORM_SAVE}
              </CPButton>
            </div>
          </div>
        </CPPermission>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  relatedCategories: state.relatedCategoryList.data
    ? state.relatedCategoryList.data.items
    : null,
  categories: state.categoryList.data ? state.categoryList.data.items : [],
  vendors: state.vendorList.data ? state.vendorList.data.items : [],
  tags: state.productTag.data ? state.productTag.data.items : [],
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
  product:
    state.singleProduct.data && state.singleProduct.data.items
      ? state.singleProduct.data.items[0]
      : null,
  singleVendor:
    state.singleVendorAgent.data && state.singleVendorAgent.data.items
      ? state.singleVendorAgent.data.items[0]
      : null,
  productType:
    state.productTypesList && state.productTypesList.data
      ? state.productTypesList.data.items
      : [],
  productNames: state.productNameList.data ? state.productNameList.data : [],
  loginData: state.login.data,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      productPostRequest: productPostActions.productPostRequest,
      productPutRequest: productPutActions.productPutRequest,
      productDeleteRequest: productDeleteActions.productDeleteRequest,
      relatedCategoryListRequest:
        relatedCategoryListActons.relatedCategoryListRequest,
      productNameListRequest: productNameListActions.productNameListRequest,
      loadingHideRequest: loadingActions.editFormLoadingHideRequest,
      productTagSuccess: productTagActions.productTagSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductInfo));
