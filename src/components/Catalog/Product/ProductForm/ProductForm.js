import React from 'react';
// import { Tabs } from 'antd';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import {
  FEATURE,
  IMAGES,
  NOTIFICATION,
  PRICING,
  PRODUCT_INFO, PRODUCT_MANAGEMENT_IN_VENDOR_BRANCH,
} from '../../../../Resources/Localization';
import s from './ProductForm.css';
import ProductInfo from '../ProductInfo';
import ProductSpecificationAttribute from '../ProductSpecificationAttribute';
import ProductImage from '../ProductImage';
import PushNotification from '../PushNotification';
import { getCookie } from '../../../../utils';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import ProductArrange from "../ProductArrange";
// const { TabPane } = Tabs;

class ProductForm extends React.Component {
  static propTypes = {
    vendorId: PropTypes.string,
    product: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    backUrl: PropTypes.string.isRequired,
  };

  static defaultProps = {
    vendorId: '',
    product: null,
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    const { vendorId, product, backUrl } = this.props;
    const isEdit = product && product.id;

    return (
      <Tabs>
        <TabList>
          <Tab>{PRODUCT_INFO}</Tab>

          {isEdit && (
            <Tab>
              <CPPermission>
                <span name="product-management-notification">
                  {`${PRODUCT_MANAGEMENT_IN_VENDOR_BRANCH}`}
                </span>
              </CPPermission>
            </Tab>
          )}
          {isEdit && <Tab>{IMAGES}</Tab>}
          {isEdit && <Tab>{`${FEATURE} و ${PRICING}`}</Tab>}

          {isEdit && (
            <Tab>
              <CPPermission>
                <span name="product-management-notification">
                  {`${NOTIFICATION}`}
                </span>
              </CPPermission>
            </Tab>
          )}
        </TabList>

        <TabPanel>
          <ProductInfo vendorIdFromCreate={vendorId} backUrl={backUrl} />
        </TabPanel>

        {isEdit && (
          <CPPermission>
            <div name="product-management-notification">
              <TabPanel>
                <ProductArrange />
              </TabPanel>
            </div>
          </CPPermission>
        )}
        {isEdit && (
          <TabPanel>
            <ProductImage />
          </TabPanel>
        )}
        {isEdit && (
          <TabPanel>
            <ProductSpecificationAttribute />
          </TabPanel>
        )}
        {isEdit && (
          <CPPermission>
            <div name="product-management-notification">
              <TabPanel>
                <PushNotification />
              </TabPanel>
            </div>
          </CPPermission>
        )}
      </Tabs>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductForm));
