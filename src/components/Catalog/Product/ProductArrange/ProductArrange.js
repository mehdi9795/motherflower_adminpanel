import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductArrange.css';
import {
  ACTIVE,
  DEACTIVATE,
  DELIVERY_PRICE,
  DISPLAY_ORDER,
  EXIST,
  MARK_AS_NEW,
  NOT_EXIST,
  PICKUP_PRICE,
  PRODUCT_MANAGEMENT_IN_VENDOR_BRANCH,
  PRODUCT_PRICE_EDIT,
  SOLD_COUNT,
  STATUS,
  VENDOR_BRANCH_NAME,
  VIEWED_COUNT,
} from '../../../../Resources/Localization';

import CPList from '../../../CP/CPList';
import CPModal from '../../../CP/CPModal';
import { VendorBranchProductPriceDto } from '../../../../dtos/samDtos';
import {
  ProductDto,
  VendorBranchProductDto,
} from '../../../../dtos/catalogDtos';
import vbProductPricePutActions from '../../../../redux/sam/actionReducer/vendorBranchProductPrice/Put';
import CPInputNumber from '../../../CP/CPInputNumber';
import CPButton from '../../../CP/CPButton';
import CPPermission from '../../../CP/CPPermission/CPPermission';
import singleProductActions from '../../../../redux/catalog/actionReducer/product/SingleProduct';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
  BaseListGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';
import CPSwitch from '../../../CP/CPSwitch';
import { getCookie } from '../../../../utils';
import {
  getProductApi,
  putVendorBranchProductApi,
} from '../../../../services/catalogApi';
import { getDtoQueryString } from '../../../../utils/helper';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';

class ProductArrange extends React.Component {
  static propTypes = {
    product: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    product: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      listLoading: false,
      viewedCount: 0,
      soldCount: 0,
      markAsNew: false,
      published: false,
      inventoryStatus: false,
      vendorBranchProductId: 0,
      vendorBranchId: 0,
    };

    this.handleOkModal = this.handleOkModal.bind(this);
  }

  showModal = record => {
    this.setState({
      visibleModal: true,
      viewedCount: record.viewedCount,
      soldCount: record.soldCount,
      markAsNew: record.markAsNew,
      published: record.published,
      inventoryStatus: record.inventoryStatus,
      vendorBranchProductId: record.key,
      vendorBranchName: record.vendorBranchName,
      vendorBranchId: record.vendorBranchId,
    });
  };

  async handleOkModal() {
    const {
      viewedCount,
      soldCount,
      markAsNew,
      published,
      inventoryStatus,
      vendorBranchProductId,
      vendorBranchId,
    } = this.state;
    const { product } = this.props;

    this.setState({ listLoading: true });
    const token = getCookie('token');

    const crudVBProduct = new BaseCRUDDtoBuilder()
      .dto(
        new VendorBranchProductDto({
          inventoryStatus,
          published,
          markAsNew,
          soldCount,
          viewedCount,
          id: vendorBranchProductId,
          vendorBranchDto: new VendorBranchDto({ id: vendorBranchId }),
          productDto: new ProductDto({ id: product.id }),
        }),
      )
      .build();

    const response = await putVendorBranchProductApi(token, crudVBProduct);

    if (response.status === 200) {
      const vendorBranchProductDtosNew = [];

      product.vendorBranchProductDtos.map(item => {
        if (item.id === vendorBranchProductId) {
          const vendorBranchNew = {
            createdOnUtc: item.createdOnUtc,
            key: item.key,
            id: item.id,
            updatedOnUtc: item.updatedOnUtc,
            vendorBranchDto: item.vendorBranchDto,
            markAsNew,
            published,
            soldCount,
            viewedCount,
            inventoryStatus,
          };
          vendorBranchProductDtosNew.push(vendorBranchNew);
        } else vendorBranchProductDtosNew.push(item);
        return null;
      });

      product.vendorBranchProductDtos = vendorBranchProductDtosNew;

      this.setState({
        visibleModal: false,
        listLoading: false,
      });
    } else
      this.setState({
        visibleModal: false,
        listLoading: false,
      });
  }

  /**
   * set value input to state
   * @param inputName
   * @param value
   */
  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  handleCancelModal = () => {
    this.setState({
      visibleModal: false,
    });
  };

  render() {
    const { product } = this.props;

    const {
      listLoading,
      viewedCount,
      soldCount,
      markAsNew,
      published,
      inventoryStatus,
      vendorBranchName,
    } = this.state;

    const vendorBranchProductArray = [];
    const columns = [
      {
        title: VENDOR_BRANCH_NAME,
        dataIndex: 'vendorBranchName',
        key: 'vendorBranchName',
      },
      {
        title: VIEWED_COUNT,
        dataIndex: 'viewedCount',
        key: 'viewedCount',
      },
      {
        title: SOLD_COUNT,
        dataIndex: 'soldCount',
        key: 'soldCount',
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              checkedChildren={MARK_AS_NEW}
              unCheckedChildren={MARK_AS_NEW}
              defaultChecked={record.markAsNew}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo1',
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              unCheckedChildren={DEACTIVATE}
              checkedChildren={ACTIVE}
              defaultChecked={record.published}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo2',
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              unCheckedChildren={NOT_EXIST}
              checkedChildren={EXIST}
              defaultChecked={record.inventoryStatus}
            />
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo3',
        width: 50,
        render: (text, record) => (
          <div>
            <CPButton
              onClick={() => this.showModal(record)}
              className="edit_action"
            >
              <i className="cp-edit" />
            </CPButton>
          </div>
        ),
      },
    ];
    if (product && product.vendorBranchProductDtos)
      product.vendorBranchProductDtos.map(item => {
        const obj = {
          key: item.id,
          viewedCount: item.viewedCount,
          soldCount: item.soldCount,
          markAsNew: item.markAsNew,
          published: item.published,
          inventoryStatus: item.inventoryStatus,
          vendorBranchName: item.vendorBranchDto.name,
          vendorBranchId: item.vendorBranchDto.id,
        };
        vendorBranchProductArray.push(obj);
        return null;
      });

    return (
      <CPPermission>
        <div name="product-management-price">
          <CPList
            data={vendorBranchProductArray}
            count={
              product && product.vendorBranchProductDtos
                ? product.vendorBranchProductDtos.length
                : 0
            }
            columns={columns}
            loading={listLoading}
            hideSearchBox
            hideAdd
            pagination={false}
          />
          <CPModal
            visible={this.state.visibleModal}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
            title={`${PRODUCT_MANAGEMENT_IN_VENDOR_BRANCH} - ${vendorBranchName}`}
          >
            <div className="modalContent">
              <div className="row">
                <div className="col-lg-6 col-sm-12">
                  <label>{VIEWED_COUNT}</label>
                  <CPInputNumber
                    value={viewedCount}
                    onChange={value =>
                      this.handelChangeInput('viewedCount', value)
                    }
                  />
                </div>
                <div className="col-lg-6 col-sm-12">
                  <label>{SOLD_COUNT}</label>
                  <CPInputNumber
                    value={soldCount}
                    onChange={value =>
                      this.handelChangeInput('soldCount', value)
                    }
                  />
                </div>
                <div className="col-lg-6 col-md-12">
                  <label>{MARK_AS_NEW}</label>
                  <CPSwitch
                    defaultChecked={markAsNew}
                    checkedChildren={MARK_AS_NEW}
                    unCheckedChildren={MARK_AS_NEW}
                    onChange={value => {
                      this.handelChangeInput('markAsNew', value);
                    }}
                  />
                </div>
                <div className="col-lg-6 col-md-12">
                  <label>{STATUS}</label>
                  <CPSwitch
                    defaultChecked={published}
                    checkedChildren={ACTIVE}
                    unCheckedChildren={DEACTIVATE}
                    onChange={value => {
                      this.handelChangeInput('published', value);
                    }}
                  />
                </div>
                <div className="col-lg-6 col-md-12">
                  <label>{EXIST}</label>
                  <CPSwitch
                    defaultChecked={inventoryStatus}
                    checkedChildren={EXIST}
                    unCheckedChildren={NOT_EXIST}
                    onChange={value => {
                      this.handelChangeInput('inventoryStatus', value);
                    }}
                  />
                </div>
              </div>
            </div>
          </CPModal>
        </div>
      </CPPermission>
    );
  }
}

const mapStateToProps = state => ({
  product:
    state.singleProduct.data &&
    state.singleProduct.data.items &&
    state.singleProduct.data.items.length > 0
      ? state.singleProduct.data.items[0]
      : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      singleProductSuccess: singleProductActions.singleProductSuccess,
      singleProductFailure: singleProductActions.singleProductFailure,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductArrange));
