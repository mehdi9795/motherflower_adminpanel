import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import InventoryManagementInfo from '../InventoryManagementInfo';
import { INVENTORY_MANAGEMENT } from '../../../../Resources/Localization';
import s from './InventoryManagementForm.css';

class InventoryManagementForm extends React.Component {
  render() {
    return (
      <Tabs>
        <TabList>
          <Tab>{INVENTORY_MANAGEMENT}</Tab>
        </TabList>
        <TabPanel>
          <InventoryManagementInfo />
        </TabPanel>
      </Tabs>
    );
  }
}

export default withStyles(s)(InventoryManagementForm);
