import React from 'react';
import { Select, message } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HotKeys from 'react-hot-keys';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './InventoryManagementInfo.css';
import CPButton from '../../../CP/CPButton/CPButton';
import {
  ACTIVE,
  AGENT_BRANCHES,
  ALLOW_FILTERING,
  CODE,
  CONFIRMATION,
  DEACTIVATE,
  NAME,
  PRODUCT_LIST_CATEGORY,
  PRODUCT_LIST_CODE,
  PRODUCT_LIST_NAME,
  PRODUCT_SPECIFICATION_ATTRIBUTE,
  PRODUCT_VIEW,
  SELECT,
  SELECT_PRODUCT_FOR_PROMOTION,
  SHOW_ON_PRODUCT_PAGE,
  STATUS,
  TYPE,
} from '../../../../Resources/Localization';
import {
  SpecificationAttributeDto,
  InventoryManagementDto,
  ProductDto,
  CategoryDto,
  ProductCategoryDto,
  VendorBranchProductDto,
  SpecificationAttributeDtos,
} from '../../../../dtos/catalogDtos';
import { VendorBranchDto } from '../../../../dtos/vendorDtos';
import CPSwitch from '../../../CP/CPSwitch';
import CPMultiSelect from '../../../CP/CPMultiSelect';
import specificationAttributeListActions from '../../../../redux/catalog/actionReducer/specificationAttribute/List';
import CPAvatar from '../../../CP/CPAvatar';
import CPModal from '../../../CP/CPModal';
import CPList from '../../../CP/CPList';
import CPInput from '../../../CP/CPInput';
import inventoryManagementPostActions from '../../../../redux/catalog/actionReducer/inventoryManagement/Post';
import inventoryManagementPutActions from '../../../../redux/catalog/actionReducer/inventoryManagement/Put';
import CPPagination from '../../../CP/CPPagination';
import productListActions from '../../../../redux/catalog/actionReducer/product/ProductList';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../../dtos/dtoBuilder';

const { Option } = Select;

class InventoryManagementInfo extends React.Component {
  static propTypes = {
    specificationAttributes: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    specificationAttributeCount: PropTypes.number,
    specificationAttributeLoading: PropTypes.bool,
    inventory: PropTypes.objectOf(PropTypes.any),
    categoryChildren: PropTypes.arrayOf(PropTypes.object),
    products: PropTypes.arrayOf(PropTypes.object),
    productCount: PropTypes.number,
    productLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    inventory: {},
    vendorBranches: [],
    specificationAttributes: [],
    specificationAttributeCount: 0,
    specificationAttributeLoading: false,
    categoryChildren: [],
    products: [],
    productCount: 0,
    productLoading: false,
  };

  constructor(props) {
    super(props);
    this.attributeArray = [];

    this.state = {
      visibleModal: false,
      attributeSelectedIds: [],
      vendorBranchSelectedIds: [],
      status: props.inventory ? props.inventory.inventoryStatus : false,
      selectedRows: [],
      searchName: '',
      currentPage: 1,
      pageSize: 10,
      searchChildCategorySelected: [],
      searchProductCode: '',
      searchProductName: '',
      productPageSize: 10,
      productCurrentPage: 1,
      hideProductList: true,
      productSelectedIds: [],
    };

    if (props.inventory && props.inventory.id) {
      props.inventory.specificationAttributeDtos.map(item => {
        this.state.attributeSelectedIds.push(item.id.toString());
        this.attributeArray.push(<Option key={item.id}>{item.name}</Option>);
        return null;
      });

      props.inventory.vendorBranchDtos.map(item => {
        this.state.vendorBranchSelectedIds.push(item.id.toString());
        return null;
      });

      props.inventory.vendorBranchProductDtos.map(item => {
        this.state.productSelectedIds.push(item.id.toString());
        return null;
      });
    }
  }

  onSelectedAttributes = (selectedRowKeys, selectedRows) => {
    this.setState({ attributeSelectedIds: selectedRowKeys, selectedRows });
  };

  onSelectedProduct = selectedRowKeys => {
    this.setState({ productSelectedIds: selectedRowKeys });
  };

  onChangePagination = page => {
    const { pageSize } = this.state;
    const selectedIds = [];
    this.attributeArray.map(item => selectedIds.push(item.key));

    this.setState({ currentPage: page }, () =>
      this.props.actions.specificationAttributeListRequest(
        new BaseGetDtoBuilder()
          .notExpectedIds(selectedIds)
          .pageIndex(page - 1)
          .pageSize(pageSize)
          .includes(['imageDtos'])
          .buildJson(),
      ),
    );
  };

  onShowSizeChange = (current, pageSize) => {
    const { currentPage } = this.state;
    const selectedIds = [];
    this.attributeArray.map(item => selectedIds.push(item.key));

    this.setState({ currentPage: current, pageSize }, () =>
      this.props.actions.specificationAttributeListRequest(
        new BaseGetDtoBuilder()
          .notExpectedIds(selectedIds)
          .pageIndex(currentPage - 1)
          .pageSize(pageSize)
          .includes(['imageDtos'])
          .buildJson(),
      ),
    );
  };

  onChangeProductPagination = page => {
    this.setState({ productCurrentPage: page }, () =>
      this.props.actions.productListRequest(this.prepareContainerListProduct()),
    );
  };

  onShowSizeChangeProduct = (current, pageSize) => {
    this.setState(
      { productCurrentPage: current, productPageSize: pageSize },
      () =>
        this.props.actions.specificationAttributeListRequest(
          this.prepareContainerListProduct(),
        ),
    );
  };

  handleCancelModal = () => {
    this.setState({ visibleModal: false });
  };

  handleOkModal = () => {
    const { selectedRows } = this.state;
    // this.attributeArray = [];
    selectedRows.map(item =>
      this.attributeArray.push(<Option key={item.key}>{item.name}</Option>),
    );
    this.setState({ visibleModal: false });
  };

  showModal = () => {
    const selectedIds = [];
    this.attributeArray.map(item => selectedIds.push(item.key));

    this.props.actions.specificationAttributeListRequest(
      new BaseGetDtoBuilder().notExpectedIds(selectedIds).pageIndex(0).pageSize(10).includes(['imageDtos']).buildJson(),
    );

    this.setState({ visibleModal: true, currentPage: 1 });
  };

  submitSearchAttribute = () => {
    const { searchName, searchAttributeIds } = this.state;

    this.props.actions.specificationAttributeListRequest(
      new BaseGetDtoBuilder()
        .dto(new SpecificationAttributeDto({ name: searchName }))
        .pageIndex(0)
        .pageSize(10)
        .notExpectedIds(searchAttributeIds)
        .includes(['imageDtos'])
        .buildJson(),
    );
  };

  confirmation = () => {
    const { inventory } = this.props;
    const {
      attributeSelectedIds,
      vendorBranchSelectedIds,
      productSelectedIds,
      status,
    } = this.state;
    if (attributeSelectedIds.length > 0 || productSelectedIds.length > 0) {
      const vendorBranchArray = [];
      const attributeArray = [];
      const productDtoArray = [];

      attributeSelectedIds.map(item => {
        // const specificationAttributeDtoTemp = new SpecificationAttributeDto();
        // specificationAttributeDtoTemp.id = item;
        attributeArray.push(new SpecificationAttributeDto({ id: item }));
        return null;
      });

      vendorBranchSelectedIds.map(item => {
        vendorBranchArray.push(new VendorBranchDto({ id: item }));
        return null;
      });

      productSelectedIds.map(item => {
        productDtoArray.push(new VendorBranchProductDto({ id: item }));
        return null;
      });

      const jsonCrud = new BaseCRUDDtoBuilder()
        .dto(
          new InventoryManagementDto({
            inventoryStatus: status,
            vendorBranchDtos: vendorBranchArray,
            SpecificationAttributeDtos: attributeArray,
            id: inventory ? inventory.id : undefined,
            vendorBranchProductDtos:
              productDtoArray.length > 0 ? productDtoArray : undefined,
          }),
        )
        .build();

      if (inventory && inventory.id)
        this.props.actions.inventoryManagementPutRequest(jsonCrud);
      else this.props.actions.inventoryManagementPostRequest(jsonCrud);
    } else {
      message.success(
        'حداقل باید یک ویژگی محصول یا یک محصول را انتخاب کنید.',
        10,
      );
    }
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
    if (inputName === 'vendorBranchSelectedIds')
      this.setState({ hideProductList: true });
  };

  prepareContainerListProduct = () => {
    const {
      vendorBranchSelectedIds,
      attributeSelectedIds,
      productCurrentPage,
      productPageSize,
      searchChildCategorySelected,
      searchProductName,
      searchProductCode,
    } = this.state;
    const attributeArray = [];

    attributeSelectedIds.map(item => {
      attributeArray.push(
        new SpecificationAttributeDtos({
          specificationAttributeDto: new SpecificationAttributeDto({
            id: item,
          }),
        }),
      );
      return null;
    });

    const productCategoryDtos = [];

    searchChildCategorySelected.map(categoryId => {
      const productCategoryDtoTemp = new ProductCategoryDto();
      const categoryDtoTemp = new CategoryDto();
      categoryDtoTemp.id = categoryId;
      productCategoryDtoTemp.categoryDto = categoryDtoTemp;
      productCategoryDtos.push(productCategoryDtoTemp);
      return null;
    });

    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new ProductDto({
          // id: vendorBranchSelectedIds[0],
          vendorBranchProductDtos: [
            {
              vendorBranchDto: new VendorBranchDto({
                id: vendorBranchSelectedIds[0],
              }),
            },
          ],
          name: searchProductName,
          code: searchProductCode,
          productSpecificationAttributeDtos: attributeArray,
          published: true,
          productCategoryDtos,
        }),
      )
      .includes(['productCategoryDtos', 'imageDtos', 'vendorBranchProductDtos'])
      .pageSize(productPageSize)
      .pageIndex(productCurrentPage - 1)
      .buildJson();
    return jsonList;
  };

  productView = () => {
    this.setState({ productCurrentPage: 1, hideProductList: false }, () => {
      this.props.actions.productListRequest(this.prepareContainerListProduct());
    });
  };

  renderAttributeModal = () => {
    const {
      visibleModal,
      attributeSelectedIds,
      searchName,
      currentPage,
    } = this.state;
    const {
      specificationAttributeLoading,
      specificationAttributes,
      specificationAttributeCount,
    } = this.props;

    const specificationAttributeArray = [];
    const columns = [
      {
        title: '',
        dataIndex: 'imageUrl',
        render: (text, record) => <CPAvatar src={record.imageUrl} />,
      },
      {
        title: NAME,
        dataIndex: 'name',
      },
      {
        title: TYPE,
        dataIndex: 'typeId',
      },
      {
        title: ALLOW_FILTERING,
        dataIndex: 'allowFiltering',
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              defaultChecked={record.allowFiltering}
            />
          </div>
        ),
      },
      {
        title: SHOW_ON_PRODUCT_PAGE,
        dataIndex: 'showOnProductPage',
        render: (text, record) => (
          <div>
            <CPSwitch
              size="small"
              disabled
              defaultChecked={record.showOnProductPage}
            />
          </div>
        ),
      },
    ];

    specificationAttributes.map(item => {
      const obj = {
        name: item.name,
        allowFiltering: item.allowFiltering,
        key: item.id.toString(),
        showOnProductPage: item.showOnProductPage,
        displayOrder: item.displayOrder,
        imageUrl:
          item.imageDtos && item.imageDtos.length > 0
            ? item.imageDtos[0].url
            : '',
        imageId:
          item.imageDtos && item.imageDtos.length > 0
            ? item.imageDtos[0].id
            : '',
        typeId: item.specificationAttributeType,
      };
      specificationAttributeArray.push(obj);
      return null;
    });

    const rowSelection = {
      attributeSelectedIds,
      onChange: this.onSelectedAttributes,
    };

    return (
      <CPModal
        visible={visibleModal}
        handleCancel={this.handleCancelModal}
        handleOk={this.handleOkModal}
        title={SELECT_PRODUCT_FOR_PROMOTION}
        className="max_modal"
      >
        <div className="row">
          <div className="col-sm-12">
            <CPList
              data={specificationAttributeArray}
              columns={columns}
              count={specificationAttributeCount}
              loading={specificationAttributeLoading}
              rowSelection={rowSelection}
              withCheckBox
              hideAdd
              pagination={false}
            >
              <div className="searchBox">
                <div className={s.searchFildes}>
                  <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <CPInput
                        label={NAME}
                        value={searchName}
                        onChange={value => {
                          this.handelChangeInput(
                            'searchName',
                            value.target.value,
                          );
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className={s.field}>
                  <CPButton
                    size="large"
                    shape="circle"
                    type="primary"
                    icon="search"
                    onClick={this.submitSearchAttribute}
                  />
                </div>
              </div>
            </CPList>
            <CPPagination
              total={specificationAttributeCount}
              current={currentPage}
              onChange={this.onChangePagination}
              onShowSizeChange={this.onShowSizeChange}
            />
          </div>
        </div>
      </CPModal>
    );
  };

  renderProductList = () => {
    const {
      productCount,
      products,
      productLoading,
      categoryChildren,
      inventory,
    } = this.props;
    const {
      productCurrentPage,
      searchChildCategorySelected,
      searchProductName,
      searchProductCode,
      productSelectedIds,
    } = this.state;

    const columns = [
      {
        title: '',
        dataIndex: 'imgUrl',
        render: (text, record) => <CPAvatar src={record.imgUrl} />,
      },
      { title: CODE, dataIndex: 'code', key: 'code' },
      {
        title: PRODUCT_LIST_CATEGORY,
        dataIndex: 'category',
        key: 'category',
      },
      { title: PRODUCT_LIST_NAME, dataIndex: 'name', key: 'name' },
    ];
    const productArray = [];
    const CategoryChildrenArray = [];

    if (!inventory) {
      products.map(item => {
        const product = {
          key: item.vendorBranchProductDtos[0].id,
          code: item.code,
          name: item.name,
          category: item.productCategoryDtos[0].categoryDto.description,
          imgUrl:
            item.imageDtos && item.imageDtos.length > 0
              ? `${item.imageDtos[0].url}`
              : ` `,
        };
        return productArray.push(product);
      });
    } else {
      inventory.vendorBranchProductDtos.map(item => {
        const product = {
          key: item.id,
          code: item.productDto.code,
          name: item.productDto.name,
          category:
            item.productDto.productCategoryDtos[0].categoryDto.description,
          imgUrl:
            item.productDto.imageDtos && item.productDto.imageDtos.length > 0
              ? `${item.productDto.imageDtos[0].url}`
              : ` `,
        };
        return productArray.push(product);
      });
    }

    /**
     * map child Category for comboBox Datasource
     */
    categoryChildren.map(item =>
      CategoryChildrenArray.push(
        <Option key={item.id}>{item.description}</Option>,
      ),
    );

    const rowSelection = {
      productSelectedIds,
      onChange: this.onSelectedProduct,
    };

    return (
      <div>
        <CPList
          data={productArray}
          count={productCount}
          columns={columns}
          loading={productLoading}
          pagination={!!inventory}
          withCheckBox={!inventory}
          hideAdd
          rowSelection={rowSelection}
          hideSearchBox={!!inventory}
        >
          <div className="searchBox">
            <div className={s.searchFildes}>
              <div className="row">
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <CPInput
                    label={PRODUCT_LIST_NAME}
                    value={searchProductName}
                    onChange={value => {
                      this.handelChangeInput(
                        'searchProductName',
                        value.target.value,
                      );
                    }}
                  />
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <CPInput
                    label={PRODUCT_LIST_CODE}
                    value={searchProductCode}
                    onChange={value => {
                      this.handelChangeInput(
                        'searchProductCode',
                        value.target.value,
                      );
                    }}
                  />
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                  <label name="label">{PRODUCT_LIST_CATEGORY}</label>
                  <CPMultiSelect
                    placeholder="انتخاب گروه محصول"
                    onChange={value =>
                      this.handelChangeInput(
                        'searchChildCategorySelected',
                        value,
                      )
                    }
                    value={searchChildCategorySelected}
                  >
                    {CategoryChildrenArray}
                  </CPMultiSelect>
                </div>
              </div>
            </div>
            <div className={s.field}>
              <CPButton
                size="large"
                shape="circle"
                type="primary"
                icon="search"
                onClick={this.productView}
              />
            </div>
          </div>
        </CPList>
        {!inventory && (
          <CPPagination
            total={productCount}
            current={productCurrentPage}
            onChange={this.onChangeProductPagination}
            onShowSizeChange={this.onShowSizeChangeProduct}
          />
        )}
      </div>
    );
  };

  render() {
    const { vendorBranches, inventory } = this.props;
    const {
      attributeSelectedIds,
      vendorBranchSelectedIds,
      status,
      hideProductList,
    } = this.state;

    const vendorBranchArray = [];
    vendorBranches.map(item =>
      vendorBranchArray.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <HotKeys keyName="shift+s" onKeyDown={this.confirmation}>
        <div>
          <div className="tabContent">
            <div className="row">
              <div className="col-lg-4 col-md-6 col-sm-12">
                <label>{PRODUCT_SPECIFICATION_ATTRIBUTE}</label>
                <div className="col-sm-12 attributeSearch">
                  <CPMultiSelect
                    placeholder={PRODUCT_SPECIFICATION_ATTRIBUTE}
                    value={attributeSelectedIds}
                    onChange={value =>
                      this.handelChangeInput('attributeSelectedIds', value)
                    }
                    disabled={!!(inventory && inventory.id)}
                  >
                    {this.attributeArray}
                  </CPMultiSelect>
                  <CPButton
                    disabled={!!(inventory && inventory.id)}
                    onClick={this.showModal}
                  >
                    {SELECT}
                  </CPButton>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>{AGENT_BRANCHES}</label>
                <CPMultiSelect
                  placeholder={AGENT_BRANCHES}
                  value={vendorBranchSelectedIds}
                  onChange={value =>
                    this.handelChangeInput('vendorBranchSelectedIds', value)
                  }
                  disabled={!!(inventory && inventory.id)}
                >
                  {vendorBranchArray}
                </CPMultiSelect>
              </div>
              <div className="col-lg-2 col-md-6 col-sm-12">
                <label>{STATUS}</label>
                <CPSwitch
                  defaultChecked={status}
                  checkedChildren={ACTIVE}
                  unCheckedChildren={DEACTIVATE}
                  onChange={value => {
                    this.handelChangeInput('status', value);
                  }}
                />
              </div>
            </div>
          </div>
          <div className={s.textLeft}>
            {vendorBranchSelectedIds.length === 1 &&
              !inventory && (
                <CPButton className={s.btnView} onClick={this.productView}>
                  {PRODUCT_VIEW}
                </CPButton>
              )}
            <CPButton icon="save" type="primary" onClick={this.confirmation}>
              {CONFIRMATION}
            </CPButton>
          </div>
          {vendorBranchSelectedIds.length > 0 &&
            !hideProductList &&
            this.renderProductList()}
          {inventory &&
            inventory.vendorBranchProductDtos.length > 0 &&
            this.renderProductList()}
          {this.renderAttributeModal()}
        </div>
      </HotKeys>
    );
  }
}

const mapStateToProps = state => ({
  specificationAttributes: state.specificationAttributeList.data.items,
  specificationAttributeCount: state.specificationAttributeList.data.count,
  specificationAttributeLoading: state.specificationAttributeList.loading,
  vendorBranches: state.vendorBranchList.data
    ? state.vendorBranchList.data.items
    : [],
  inventory:
    state.inventoryManagementList.data &&
    state.inventoryManagementList.data.items &&
    state.inventoryManagementList.data.items.length > 0
      ? state.inventoryManagementList.data.items[0]
      : null,
  products: state.productList.data ? state.productList.data.items : [],
  productCount: state.productList.data.count,
  productLoading: state.productList.loading,
  categoryChildren: state.childCategoryList.data
    ? state.childCategoryList.data.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      specificationAttributeListRequest:
        specificationAttributeListActions.specificationAttributeListRequest,
      inventoryManagementPostRequest:
        inventoryManagementPostActions.inventoryManagementPostRequest,
      inventoryManagementPutRequest:
        inventoryManagementPutActions.inventoryManagementPutRequest,
      productListRequest: productListActions.productListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(InventoryManagementInfo));
