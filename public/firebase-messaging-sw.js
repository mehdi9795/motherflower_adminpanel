importScripts('https://www.gstatic.com/firebasejs/5.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.2/firebase-messaging.js');
// const React = require('react');

const config = {
  messagingSenderId: '358354824957',
};
firebase.initializeApp(config);
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(payload => {
  const notif = JSON.parse(payload.data.custom_notification);

  const title = notif.title;
  let link = '';
  if (notif.type === 'Message')
    link = `/message/list?messageId=${notif.meta_data.messageId}&mode=${
      notif.meta_data.mode
    }`;
  else if (notif.type === 'NewBill') link = '/order/list';
  const options = {
    body: notif.body,
    icon: notif.large_icon,
    vibrate: [200, 100, 200, 100, 200, 100, 200],
    tag: link,
  };

  return self.registration.showNotification(title, options);
});

self.addEventListener('notificationclick', event => {
  event.notification.close();
  console.log('event.notification', event.notification);

  // This looks to see if the current is already open and
  // focuses if it is
  event.waitUntil(
    clients
      .matchAll({
        type: 'window',
      })
      .then(clientList => {
        for (const i = 0; i < clientList.length; i++) {
          const client = clientList[i];
          if (client.url === '/' && 'focus' in client) return client.focus();
        }
        if (clients.openWindow)
          return clients.openWindow(event.notification.tag);
        return null;
      }),
  );
});
